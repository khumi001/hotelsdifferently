<!DOCTYPE html>
<html>
    <!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    
    <title>HotelsDifferently - Providing luxury accommodation for a discounted price! </title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=EDGE" />-->
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta content="Home" name="description"/>
    <meta content="Home" name="keywords"/>
    <meta name="google-site-verification" content="ZBUbYCT9lCwyszTdzXzftAqkYQS8o5O6qP8eGRaiQXk" />
    <meta name="p:domain_verify" content="a29fc0e0d9ae02190ff346cc33d9b2b3"/>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <!--<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>-->
    <link href="https://hotelsdifferently.com/public/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="https://hotelsdifferently.com/public/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="https://hotelsdifferently.com/public/affilliate_theme/stylesheets/style.css" rel="stylesheet" type="text/css">
    <link href="https://hotelsdifferently.com/public/affilliate_theme/stylesheets/custom.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="https://hotelsdifferently.com/public/assets/plugins/data-tables/DT_bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="https://hotelsdifferently.com/public/assets/plugins/bootstrap-toastr/toastr.min.css"/>
   <!--<link rel="stylesheet" type="text/css" href="https://hotelsdifferently.com/public/assets/plugins/bootstrap-datepicker/css/datepicker.css"/>-->
    <!-- END GLOBAL MANDATORY STYLES -->
    <!--[if IE 8]>
         <link rel="stylesheet" type="text/css" href="https://hotelsdifferently.com/public/affilliate_theme/stylesheets/ie.css" />
    <![endif]-->
    <!--[if IE]>
        <link rel="stylesheet" type="text/css" href="https://hotelsdifferently.com/public/affilliate_theme/stylesheets/ie.css" />
    <![endif]-->

    <!-- Begin From Controller-->
    
     
    <!-- End From Controller -->


    <!-- BEGIN THEME STYLES -->
    <link rel="stylesheet" href="https://hotelsdifferently.com/public/affilliate_theme/plugins/bootstrap-select/bootstrap-select.min.css" />
    <link href="https://hotelsdifferently.com/public/assets/stylesheets/plugins.css" rel="stylesheet" type="text/css"/>
    <link href="https://hotelsdifferently.com/public/assets/stylesheets/custom.css" rel="stylesheet" type="text/css"/>
    <!-- END THEME STYLES -->
    <script type="text/javascript">
        var baseurl = "https://hotelsdifferently.com/";
    </script>


    <link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->    <body>
        <!--[if gte IE 9]>
  <style type="text/css">
    .gradient {
       filter: none;
    }
  </style>
<![endif]-->
<header>
   <div class="row">
       <div class="col-md-2 margin-top-12">
           <form action="#" method="post" id="newsletter_form" class="newsletter-form">
               <div class="form-group">
                    <label class="control-label">Signup for Newsletter</label>
                    <input type="text" class="form-control" name="newsletter" id="newsletter" value="" />
                    <input type="submit" name="btn_submit" value="submit" />
               </div>
           </form>
       </div> 
       <div class="col-md-8 text-center logo">
               <a href="https://hotelsdifferently.com/"><img alt="logo" src="https://hotelsdifferently.com/public/affilliate_theme/img/logo.png"></a>
       </div>
       <div class="col-md-2 margin-top-12">
           <a href="#" style="visibility: hidden;" class="call_link">800.289.0900</a>
                       <div class="pages_links margin-top-10">
                              <a href="https://hotelsdifferently.com/login" class="main_links login_link">Login</a>
                              <div class="sign_up main_links">Signup
                    <ul>
                        <li><a href="https://hotelsdifferently.com/front/home/member_signup">Member Signup</a></li>
                        <li><a href="https://hotelsdifferently.com/front/home/affiliate_signup">Affiliate Signup</a></li>
                    </ul>
               </div>
               <a href="https://hotelsdifferently.com/user/legal_corner/faqs" class="main_links">FAQ</a>
           </div>
                  </div>
   </div>
    
</header>
<div aria-hidden="false" role="dialog" class="modal fade in" id="newsletter_modal" style="display: none;">
    <div class="success_login_main_cont margin-top-20">
        <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="fa fa-times"></i></button>
            <!--<h4 class="modal-title">Registration Successfully</h4>-->
            <h3 class="form-title">NEWSLETTER</h3>
        </div>
        <div style="" class="">
            <p style="padding: 10px; margin-bottom: 0px;">
                Thank you for signing up for our newsletter. We sent you an activation link to the email address you entered.
                &numsp;&numsp;&numsp;
                Does it seem like it has been taking too long and you still have not received the activation link? 
                &numsp;&numsp;&numsp;&numsp;&numsp;&numsp;&numsp;&numsp;&numsp;&numsp;&numsp;
                Please check your SPAM / JUNK folder or try again.
            </p>
        </div>
        <div style="padding: 3px; margin-top: 0px;" class="modal-footer">
            <div style="text-align:center">
                <button type="button" style="padding: 7px 20px;" data-dismiss="modal" class="btn default_btn clickme">OK</button>
            </div>
        </div>
    </div> 
</div>
<div aria-hidden="false" role="dialog" class="modal fade in" id="newsletter_modal_error" style="display: none;">
    <div class="success_login_main_cont margin-top-20">
        <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="fa fa-times"></i></button>
            <!--<h4 class="modal-title">Registration Successfully</h4>-->
            <h3 class="form-title">NEWSLETTER</h3>
        </div>
        <div style="" class="">
            <p style="padding: 10px; margin-bottom: 0px;">
                Your email address is already enrolled for our newsletter.
            </p>
        </div>
        <div style="padding: 3px; margin-top: 0px;" class="modal-footer">
            <div style="text-align:center">
                <button type="button" style="padding: 7px 20px;" data-dismiss="modal" class="btn default_btn clickme">OK</button>
            </div>
        </div>
    </div> 
</div>        
        <div class="clear"></div>
        <div id="result_container">
            <div class="page_container">
                                                 <style>
    .tooltip-inner {
        max-width: 600px;
        /* If max-width does not work, try using width instead */
        width: 600px; 
    }
    .pricacy-width{
        width: 100%; overflow: auto; height: 200px;
    }
    a.tooltips {
  position: relative;
  display: inline;
}
.username1 span{
    visibility: hidden;
    display: none;
}
.email1 span{
    visibility: hidden;
    display: none;
}

.tooltips span {
  position: absolute;
  
  color: #FFFFFF;
  background: #000000;
  height: 30px;
  line-height: 30px;
  text-align: center;
  visibility: hidden;
  border-radius: 6px;
}
.tooltips span::after {
  border-left: 8px solid transparent;
  border-right: 8px solid transparent;
  border-top: 8px solid #000000;
  content: "";
  height: 0;
  left: 50%;
  margin-left: -8px;
  position: absolute;
  top: 86%;
  width: 0;
}
.username1.tooltips span {
  bottom: 38px;
  display: block;
  left: 0;
  margin-left: 16px;
  opacity: 0.8;
  visibility: visible;
  z-index: 999;
  width:155px;
}
.email1.tooltips span {
  bottom: 38px;
  display: block;
  left: 0;
  margin-left: 16px;
  opacity: 0.8;
  visibility: visible;
  z-index: 999;
  width:255px;
}
</style>

<div class="width-row-700 margin-top-10">
    <div class="main_cont">
        <div class="pagetitle margin-bottom-20">
            <h1>Payment Success</h1>
        </div>
        <div class="row">
            <div class="col-md-12" id="form_wizard_1">
                <p>Thank you for booking with <b>HotelsDifferently!<sup>sm</sup></b> We are delighted that you decided to choose us for your next getaway. As soon as we secure your reservation, we will send you an email notification.</p>
            </div>
        </div>
    </div>
</div>


                    </div>
                </div>   
            </div>
        </div>   
        <!-- BEGIN FOOTER -->
<footer>
    <div class="width-row row padding-left-right-20 margin-bottom-15">
        <div class="col-md-5 foot_wid">
             <h2 class="foot_title">Information</h2>
            <div class="policy_link">
                <a href="https://hotelsdifferently.com/user/legal_corner/aboutus">About Us</a>
                <a href="https://hotelsdifferently.com/user/legal_corner/member">Members</a>
                <a href="https://hotelsdifferently.com/user/legal_corner/affiliateprogram">Affiliate Program</a>
                <a href="https://hotelsdifferently.com/user/legal_corner/contactus">Contact Us</a>
            </div>
        </div>
        <div class="col-md-5 foot_wid">
            <h2 class="foot_title">Legal Corner</h2>
             <div class="policy_link">
                <a href="https://hotelsdifferently.com/user/legal_corner/terms_condition">Terms and Conditions</a>
                <a href="https://hotelsdifferently.com/user/legal_corner/privacy_policy">Privacy Policy</a>
                <a href="https://hotelsdifferently.com/user/legal_corner/property_policy">Intellectual Property Policy</a>
                <a href="https://hotelsdifferently.com/user/legal_corner/dmca_Policy">DMCA Policy</a>
                <a href="https://hotelsdifferently.com/user/legal_corner/bitcoin_policy">Bitcoin Policy</a>
                <a href="https://hotelsdifferently.com/user/legal_corner/security_statement">Security Statement</a>
            </div>
        </div>
        <div class="col-md-2 foot_wid">
             <h2 class="foot_title">Follow Us On</h2>
             <div class="sc_links">
                 <a href="https://www.facebook.com/hotelsdifferently" target="_blank" class="fb_link">Facebook</a>
                 <a href="https://twitter.com/hotelsdifferent" class="tw_link" target="_blank">Twitter</a>
                 <a href="https://plus.google.com/115490121712099690825/" target="_blank" class="gp_link">Google+</a>
                 <a href="https://www.pinterest.com/hotelsdifferent/" class="pn_link" target="_blank">Pinterest</a>
             </div>
        </div>
<!--        <div class="foot_4">
            
        </div>-->
        <div class="clear"></div>
    </div>
    <div class="width-row copy-right" style="padding: 10px 0 0 8px;">
        <span style="float:left; margin: 3px 0 0;"> &#169; Copyright 2014-2015 Hotels Differently All Rights Reserved.</span>
       <div class="" style="float:right;">
            <span style=""><img alt="comodo" src="https://hotelsdifferently.com/public/assets/img/comodo.png"></span>
            <span style=""><img width="40" height="40" alt="paypal" src="https://hotelsdifferently.com/public/assets/img/paypal_without_bg.png"></span>
        </div>
       <div style="clear: both"></div>
    </div>
</footer>
<!-- END FOOTER -->
        <!--<script src="https://hotelsdifferently.com/public/assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>-->
<!-- BEGIN PAGE LEVEL PLUGINS -->
   <!--[if IE]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->      
            
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="https://hotelsdifferently.com/public/assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="https://hotelsdifferently.com/public/assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<!-- /IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="https://hotelsdifferently.com/public/affilliate_theme/plugins/jquery-ui/jquery-ui.js" type="text/javascript"></script>
<script src="https://hotelsdifferently.com/public/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="https://hotelsdifferently.com/public/assets/plugins/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
<script type="text/javascript" src="https://hotelsdifferently.com/public/assets/plugins/bootstrap-toastr/toastr.min.js"></script>
<script src="https://hotelsdifferently.com/public/assets/plugins/gritter/js/jquery.gritter.js" type="text/javascript"></script>
<script src="https://hotelsdifferently.com/public/assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>


<script src="https://hotelsdifferently.com/public/assets/javascripts/core/app.js" type="text/javascript"></script>
<script src="https://hotelsdifferently.com/public/assets/javascripts/core/datatable.js" type="text/javascript"></script>
<script type="text/javascript" src="https://hotelsdifferently.com/public/assets/plugins/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="https://hotelsdifferently.com/public/assets/plugins/data-tables/DT_bootstrap.js"></script>
<!--<script type="text/javascript" src="https://hotelsdifferently.com/public/assets/javascripts/common_function.js"></script>-->
<script type="text/javascript" src="https://hotelsdifferently.com/public/assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
<script type="text/javascript" src="https://hotelsdifferently.com/public/affilliate_theme/plugins/ajaxfileupload.js"></script>
<script type="text/javascript" src="https://hotelsdifferently.com/public/affilliate_theme/plugins/jquery.form.min.js"></script>
<script type="text/javascript" src="https://hotelsdifferently.com/public/affilliate_theme/javascripts/affiliate_common.js"></script>
<script src="https://hotelsdifferently.com/public/assets/plugins/bootstrap-toastr/toastr.min.js" type="text/javascript"></script>


             

     


             <script id="remove_js" src="https://hotelsdifferently.com/public/assets/javascripts/core/datatable_apply.js" type="text/javascript"></script>
            <!--<script id="remove_js" src="https://hotelsdifferently.com/public/assets/javascripts/common_function.js" type="text/javascript"></script>-->
<!-- END PAGE LEVEL SCRIPTS -->

<script>
jQuery(document).ready(function() {    
   App.init();
    
});
</script>    </body>
</html>
