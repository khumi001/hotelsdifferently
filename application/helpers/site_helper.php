<?php
function get_country_name_by_id($id)
{
    $CI =& get_instance();
    $res = $CI->db->where('id' , $id)->get("countries")->result();
    if(count($res) > 0)
    {
        return $res[0]->name;
    }
    else
    {
        return false;
    }
}
function get_referer_email($id)
{
    $CI =& get_instance();
    $res = $CI->db->where('int_glcode' , $id)->get("dz_user")->result();
    if(count($res) > 0)
    {
        return $res[0]->var_email;
    }
    else
    {
        return "None";
    }
}
function isUserLogin()
{
    $CI =& get_instance();
    if(isset($CI->session->userdata['valid_user']['id']))
    {
        return true;
    }
    else
    {
        return false;
    }
}

function isSkipLogin()
{
    $CI =& get_instance();
    if(isset($CI->session->userdata['skipLogin']))
    {
        return true;
    }
    else
    {
        return false;
    }
}

function isLogin()
{
    $CI =& get_instance();
    if(isset($CI->session->userdata['valid_user']['id']) || isset($CI->session->userdata['valid_adminuser']['id']))
    {
        return true;
    }
    else
    {
        return false;
    }
}

function isAdminLogin()
{
    $CI =& get_instance();
    if(isset($CI->session->userdata['valid_adminuser']['id']))
    {
        return true;
    }
    else
    {
        return false;
    }
}

?>