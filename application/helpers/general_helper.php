<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

function signUpActivationEmail($user){
    $ci =& get_instance();
    if($user['chr_user_type'] == 'U')
        $url = base_url() . 'front/home/activation/' . $user['code'];
    else if($user['chr_user_type'] == 'EN') {
        // we have a separate view for entity email too front/entity/sub_entity_signup_email... changes there too id you chagne any thing here
        $key = "-ABCDEFGHIJKLMNOPQRSTUV";
        $code = base64_encode($user['int_glcode'].$key);
        $url = base_url() . 'front/entity/activation/' . $code;
    }
    // update user
    $ci->db->where('int_glcode', $user['int_glcode']);
    $query = $ci->db->update('dz_user', array('var_accountid'=>$user['code']));

    $ci->load->library('email');
    $ci->email->set_mailtype("html");
    $ci->email->from(NOREPLY, SITE_NAME);
    $ci->email->to($user['var_email']);
    $ci->email->subject('Welcome to '.SITE_NAME.'!');
    $mail_body = '<p style="color:#fff;margin-bottom:10px;">Dear <strong style="color:#fff;">'.$user['var_fname']." ".$user['var_lname'].'</strong>,</p>';
    $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Thank you for using <strong style="color:#fff;"><a href="'.SITE_NAME.'" style="color:#fff; text-decoration:none;">'.SITE_NAME.'<sup>sm</sup></a></strong>. We hope that you will find our website very useful and that it will help you achieve your dream vacation for less money.</p>';
    $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Please activate your account by clicking <a style="color: white" href=' . $url . '><u><b>HERE</b></u></a>.</p>';
    $mail_body .= '<p style="color: #fff !important; margin-bottom:10px;" class="txt-alternative">Alternatively, you can also copy and paste the following into your browser’s URL: <br><a style="color: white" href=' . $url . '>' . $url . '</a></p>';
    $mail_body .= '<p style="color:#fff;margin-bottom:10px;">If you feel that you received this message by mistake, do nothing and your email address will not be added to our database!</p>';
    $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Thank you for choosing us and please always give us a try before booking your next trip!</p>';
    $mail_body .='<p style="color:#fff;"><i>Sincerely,</i><br/><strong style="color:#fff;"><a href="'.SITE_NAME.'" style="color:#fff; text-decoration:none;"><b>'.SITE_NAME.'</b><sup>sm</sup></a></strong><br><i style="font-size:10px;">Making hotels happen for YOU!<sup>sm</sup></i></p>';
    $data['mail_body'] = $mail_body;
    $message = $ci->load->view('admin/account/genral_emailtemplate', $data, TRUE);
    $ci->email->message($message);
    $sendit = $ci->email->send();
}

function couponcode($numno1 = 4, $numno2 = 4) {
    $listno = '0123456789';
    return str_shuffle(
        substr(str_shuffle($listno), 0, $numno1) .
        substr(str_shuffle($listno), 0, $numno2)
    );
}

function get_current_class($action, $page) {

    if (!is_array($page))
        $page = array($page);
    if (in_array($action, $page)) {
        return 'current';
    }
}

function get_all_contries()
{
    $ci =& get_instance();
    return $ci->db->order_by('name', 'asc')->get('countries')->result();
}

function get_cities_id($id)
{
    $ci =& get_instance();
    $ci->db->order_by('name', 'asc')->where('country_id', $id);
    $res = $ci->db->get('states')->result();

    $r1 = '';
    foreach ($res as $re) {
        $r1 .= '<option value="' . $re->name . '">' . $re->name . '</option>';
    }
    return $r1;
}

function is_user_login()
{
    // function to restrict when membership gets expired
    $restrict = array("tourico_api-ViewSearchResult","tourico_api-ViewSearchResultInMap","tourico_api-ShowQuotePage");
    $ci =& get_instance();
    $request = $ci->router->fetch_class()."-".$ci->router->fetch_method();
    if($ci->session->userdata['valid_user']['var_usertype'] =='U' || $ci->session->userdata['valid_affiliate']['var_usertype'] =='AF' || $ci->session->userdata['valid_user']['var_usertype'] =='TF' || $ci->session->userdata['valid_user']['var_usertype'] =='EN')
    {
        if($ci->session->userdata['valid_user']['var_usertype'] =='U'){
            $res = $ci->db->where('int_glcode' , $ci->session->userdata['valid_user']['id'])->get("dz_user")->row();
            if($res->is_subscribed == '1'){
                return true;
            }elseif($res->is_subscribed == '0'){
                redirect("/user/myaccount/enrolling");
            }elseif($res->is_subscribed == '2'){
                redirect("/user/myaccount/verification");
            }elseif($res->is_subscribed == '3'){
                //echo $ci->router->fetch_class()." \n";echo $ci->router->fetch_method();exit;
                if(in_array($request,$restrict))
                    redirect("/user/myaccount/expired");
                else
                    return true;
            }
            elseif($res->is_subscribed == '4'){
                redirect("/user/myaccount/unverified");
            }
        }else if($ci->session->userdata['valid_user']['var_usertype'] =='EN'){
            $res = $ci->db->where('int_glcode' , $ci->session->userdata['valid_user']['id'])->get("dz_user")->row();
            if(in_array($request,$restrict) && $res->entity_parent_account == 0){
                // its master, restrict the search and booking
                return false;
            }else {
                if ($res->is_subscribed == '1') {
                    return true;
                } elseif ($res->is_subscribed == '3') {
                    if (in_array($request, $restrict))
                        redirect("/user/myaccount/entity_expired");
                    else
                        return true;
                }
            }
        }else {
            return true;
        }
    }
    else
    {
        return false;
    }
}

// for users that do not require login - such as sub entity sign up
function checkUserMembershipStatus($id=0,$data){
    $restrict = array("entity-entity_sub_signup");
    $ci =& get_instance();
    $res = $ci->db->where('int_glcode' , $id)->get("dz_user")->row();
    if($res->is_subscribed == '1'){
        return true;
    }elseif($res->is_subscribed == '3'){
        $request = $ci->router->fetch_class()."-".$ci->router->fetch_method();
        if(in_array($request,$restrict))
            redirect($data['expiryPage']);
        else
            return true;
    }
}

function regular_login_check()
{

    $ci =& get_instance();
    if($ci->session->userdata['valid_user']['var_usertype'] =='U' || $ci->session->userdata['valid_affiliate']['var_usertype'] =='AF' || $ci->session->userdata['valid_user']['var_usertype'] =='TF' || $ci->session->userdata['valid_user']['var_usertype'] =='EN')
    {
        return true;
    }
    else
    {
        return false;
    }
}

function masterLoginCheck(){
    // master entity
    $ci =& get_instance();
    if($ci->session->userdata['valid_user']['var_usertype'] =='EN'){
        $res = $ci->db->where('int_glcode' , $ci->session->userdata['valid_user']['id'])->get("dz_user")->row();
        if($res->entity_parent_account == 0){
            return true;
        }
    }

    redirect("/");
}

function regularUserAndSubEntityCheck(){
    $ci =& get_instance();
    if($ci->session->userdata['valid_user']['var_usertype'] =='EN'){
        $res = $ci->db->where('int_glcode' , $ci->session->userdata['valid_user']['id'])->get("dz_user")->row();
        if($res->entity_parent_account != 0){
            return true;
        }
    }else if($ci->session->userdata['valid_user']['var_usertype'] =='U'){
        return true;
    }

    redirect("/");
}

function regularUser_SubEntity_TP_Check(){
    $ci =& get_instance();
    if($ci->session->userdata['valid_user']['var_usertype'] =='EN'){
        $res = $ci->db->where('int_glcode' , $ci->session->userdata['valid_user']['id'])->get("dz_user")->row();
        if($res->entity_parent_account != 0){
            return true;
        }
    }else if($ci->session->userdata['valid_user']['var_usertype'] =='U'){
        return true;
    }else if($ci->session->userdata['valid_user']['var_usertype'] =='TF'){
        return true;
    }

    redirect("/");
}

function TP_Check(){
    $ci =& get_instance();
    if($ci->session->userdata['valid_user']['var_usertype'] =='TF'){
        return true;
    }

    redirect("/");
}

function regularUserCheck(){
    $ci =& get_instance();
    if($ci->session->userdata['valid_user']['var_usertype'] =='U'){
        return true;
    }

    redirect("/");
}

function get_default_tab($action, $page) {
    if ($action == $page) {
        return 'default-tab';
    }
}

function date_formating($datestr = "now", $format = "D, M j") {
    if ($datestr == '') {
        return false;
    }
    $time = strtotime($datestr);
    return dateformat($time, $format);
}

function dateformat($time, $fmt = 'D, M jS Y') {
    $newdate = date($fmt, $time);
    return $newdate;
}

function date_add_and_format($datestr = "now", $day = 7, $format = "D, M j") {
    if ($datestr == '')
        return false;

    $time = strtotime($datestr);
    $time = strtotime("+$day day", $time);
    return dateformat($time, $format);
}

function buisiness_day_count($day, $h, $count = 4) {
    if ($day == 4)
        $c = 2;
    elseif ($day == 5 or $day == 6)
        $c = 1;
    else
        $c = 0;

    if ($h > 17 and ($day == 1 or $day == 2 or $day == 3 or $day == 4 or $day == 5)){
        $c++;
    }
    $count+=$c;
    return $count;
}

function active_day_check() {
    $i = 0;
    if (date('G') < 16)
        $i = 1;
    return $i;
}



function mdytomysql($date) {
    $tmparr = explode('/', $date);
    if ($tmparr) {
        $date = $tmparr['2'] . '-' . $tmparr['0'] . '-' . $tmparr['1'];
    }
    return $date;
}

function getfile($filename, $error_img = 'preview_1.jpg') {
    if (!$filename) {
        $filename = $error_img;
    }
    $url_info = parse_url($filename);

    if (!isset($url_info['scheme'])) {
        if (file_exists(IMG_PATH . $filename)) {
            $filename = CDN_IMG_PATH . $filename;
        } else {
            $filename = CDN_IMG_PATH . $error_img;
        }
    }
    return $filename;
}

function file_info($file, $info = 'extension') {
    if ($file != '') {
        $path_info = pathinfo($file);
        if (!isset($path_info['filename'])) {
            $path_info['filename'] = substr($path_info['basename'], 0, strlen($path_info['basename']) - strlen($path_info['extension']) - 1);
        }
        if (isset($path_info[$info])) {
            return $path_info[$info];
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function thumb_ext_arr() {
    $arr = array('jpeg', 'jpg', 'png');
    return $arr;
}

function splitcustomsize($str) {
    $tmp = explode(',', $str);
    if ($tmp) {
        if (!isset($tmp[0]) or $tmp[0] == 0)
            $tmp[0] = 0;
        if (!isset($tmp[1]))
            $tmp[1] = 0;
        if (!isset($tmp[2]))
            $tmp[2] = 0;
        $arr['width'] = $tmp[0];
        $arr['height'] = $tmp[1];
        if (isset($tmp[2]))
            $arr['price'] = $tmp[2];
    }
    return $arr;
}

function getsizeinfo($size, $txt = 'size') {
    $tmp = splitcustomsize($size);

    if ($tmp) {
        if ($txt == 'size') {
            $str = $tmp['width'] . 'x' . $tmp['height'];
        } elseif ($txt == 'price') {
            $str = price($tmp['price']);
        }
    }

    return $str;
}

function price($str) {
    return '$' . decnum($str);
}

function decnum($str) {
    if (!$str) {
        $str = '0.00';
    }
    return number_format($str, 2, '.', '');
}



function jencode($arr) {
    return json_encode($arr);
}

function jdecode($json, $assoc = TRUE) {
    $json = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $json);
    return json_decode($json, $assoc);
}

function cleanshiptxt($txt) {
    return ucfirst(strtolower(str_replace('_', ' ', $txt)));
}

function yes_no($val) {
    $str = 'No';
    if ($val) {
        $str = 'Yes';
    }
    return $str;
}

function na($con) {
    if ($con == '')
        return "None";
    else
        return $con;
}

function nanum($con) {
    if ($con == '')
        return "0";
    else {
        return round($con, 2);
    }
}




function addcomma($str) {
    return addsap($str, ',');
}

function addbreak($str) {
    return addsap($str);
}

function addsap($str, $sap = '<br />') {
    if ($str)
        $str.=$sap;
    return $str;
}



function page_heading($status = '') {
    $status = str_replace('_', ' ', $status);
    return (ucwords($status));
}


function post_vals($fld = array()) {
    $arr = array();
    foreach ($fld as $val) {
        if (isset($_POST[$val])) {
            $arr[$val] = $_POST[$val];
        }
    }
    return $arr;
}

function random_time() {
    return date("dmyhms");
}

function random_num() {
    return rand(10000000000, 99999999999);
}

function remove_keys($arr, $keys) {
    foreach ($keys as $val) {
        unset($arr[$val]);
    }
    return $arr;
}

function get_cp_amount($amount, $type, $order = 0) {
    if ($type == '%') {
        if (!$order) {
            $amount/=100;
        } else {
            $amount*=100;
        }
    }
    return $amount;
}


function maxval($str, $len) {
    if (strlen($str) > $len) {
        $str = substr($str, 0, $len) . '..';
    }
    return $str;
}


function filter_array_values($array) {
    foreach ($array as $key => $value) {
        $array[$key] = mysql_real_escape_string($value);
    }
    return $array;
}

function minimize_str($str) {
    $str = preg_replace('/[^a-zA-Z0-9]/', '', $str);
    $str = strtolower($str);
    return $str;
}

function array_values_recursive($array) {
    $arrayValues = array();

    foreach ($array as $value) {
        if (is_scalar($value) OR is_resource($value)) {
            $arrayValues[] = $value;
        } elseif (is_array($value)) {
            $arrayValues = array_merge($arrayValues, array_values_recursive($value));
        } elseif (is_object($value)) {
            $arrayValues = array_merge($arrayValues, array_values_recursive($value));
        }
    }
    return $arrayValues;
}

function multiarray_keys($ar) {
    $keys = array();
    foreach ($ar as $k => $v) {
        $keys[] = $k;
        if (is_array($ar[$k]))
            $keys = array_merge($keys, multiarray_keys($ar[$k]));
    }
    return $keys;
}

function array_push_key(&$array, $key, $value) {
    $array[$key] = $value;
}


function array_non_empty_items($input) {
    if (!is_array($input)) {
        return $input;
    }
    $non_empty_items = array();
    foreach ($input as $key => $value) {
        if ($value != '') {
            $items = array_non_empty_items($value);
            if ($items != '') {
                $non_empty_items[$key] = $items;
            }
        }
    }
    if (count($non_empty_items) > 0)
        return $non_empty_items;
    else
        return false;
}



