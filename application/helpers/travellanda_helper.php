<?php 
Class TravellandaAPICore {

    private $username = '530d0dfb649a4d0ad09cc26ebc23d9b3';
    private $password = '9GxElVREySGw';
    private $Currency = 'USD';
	private $MAIN_HOST = "http://xmldemo.travellanda.com/xmlv1";
	
	function GetCountries()
	{
		$request ='<Request>
						<Head>
							<Username>'.$this->username.'</Username>
							<Password>'.$this->password.'</Password>
							<RequestType>GetCountries</RequestType>
						</Head>
						<Body/>
					</Request>';
		$response = $this->SendPostRequestToTravellanda($this->MAIN_HOST, $request);
		return $response;
	}
	
	function GetCities($CountryCode)
	{
		$CountryCodeStr = "";
		if($CountryCode != "")
		{
			$CountryCodeStr = '<CountryCode>'.$CountryCode.'</CountryCode>';
		}
		$request ='<Request>
						<Head>
							<Username>'.$this->username.'</Username>
							<Password>'.$this->password.'</Password>
							<RequestType>GetCities</RequestType>
						</Head>
						<Body>
							'.$CountryCodeStr.'
						</Body>
					</Request>';
		$response = $this->SendPostRequestToTravellanda($this->MAIN_HOST, $request);
		return $response;
	}
	
	function GetHotels($CountryCode = "" , $CityId = "")
	{
		if(!empty($CityId))
		{
			$request ='<Request>
						<Head>
							<Username>'.$this->username.'</Username>
							<Password>'.$this->password.'</Password>
							<RequestType>GetHotels</RequestType>
						</Head>
						<Body>
							<CityId>'.$CityId.'</CityId>
						</Body>
					</Request>';
		}
		else
		{
			$request ='<Request>
						<Head>
							<Username>'.$this->username.'</Username>
							<Password>'.$this->password.'</Password>
							<RequestType>GetHotels</RequestType>
						</Head>
						<Body>
							<CountryCode>'.$CountryCode.'</CountryCode>
						</Body>
					</Request>';
		}
		$response = $this->SendPostRequestToTravellanda($this->MAIN_HOST, $request);
		return $response;
	}
	
	function GetHotelDetails($HotelIds)
	{
		$HotelIdsxml = "";
        //$i=0;
		foreach($HotelIds as $singid)
		{
            /*if($i>200){
                continue;
            }*/
			$HotelIdsxml .= "<HotelId>".$singid."</HotelId>";
            //$i++;
		}
		$request ='<Request>
						<Head>
							<Username>'.$this->username.'</Username>
							<Password>'.$this->password.'</Password>
							<RequestType>GetHotelDetails</RequestType>
						</Head>
						<Body>
							<HotelIds>'.$HotelIdsxml.'</HotelIds>
						</Body>
					</Request>';
		$response = $this->SendPostRequestToTravellanda($this->MAIN_HOST, $request);
       // echo '<pre>';print_r($response);exit;
		return $response;
	}
	
	//print_r($reslt->HotelSearch("101848", "2016-11-24" , "2016-11-25" , 1 , array(array("NumAdults"=>1,"Children"=>array(2)))));
	function HotelSearch($CityId , $CheckInDate , $CheckOutDate , $Rooms , $Room , $HotelId = "" , $CityIds = "" , $Nationality = "FR" , $AvailableOnly = 0  )
	{
            //echo $CityId;
		$HotelIdxml = "";
		$CityIdxml = "";
		if(!empty($HotelId))
		{
			$HotelIdxml = "<HotelId>".$HotelId."</HotelId>";
		}
		else
		{
			$CityIdxml = "<CityId>".$CityId."</CityId>";
            //$CityIdxml .= "<CityId>".$CityId."</CityId>";
		}
		$CheckInDatexml = 	"<CheckInDate>".date("Y-m-d", strtotime($CheckInDate))."</CheckInDate>";
		$CheckOutDate 	= 	"<CheckOutDate>".date("Y-m-d", strtotime($CheckOutDate))."</CheckOutDate>";
		$roomsxml = "";
		for($i = 0 ; $i < $Rooms ; $i++)
		{
			if(isset($Room[$i]['NumAdults']) && isset($Room[$i]['Children']))
			{
				$Children = $Room[$i]['Children'];
				$childernxml = "";
				foreach($Children as $singchild)
				{
					$childernxml .= "<ChildAge>".$singchild."</ChildAge>";
				}
				$roomsxml .= '<Room>
					<NumAdults>'.$Room[$i]['NumAdults'].'</NumAdults>
					<Children>
						'.$childernxml.'
					</Children>
				</Room>';
			}
			else if(isset($Room[$i]['NumAdults']))
			{
				$roomsxml .= '<Room>
					<NumAdults>'.$Room[$i]['NumAdults'].'</NumAdults>
				</Room>';
			}
		}
		$request ='<Request>
						<Head>
							<Username>'.$this->username.'</Username>
							<Password>'.$this->password.'</Password>
							<RequestType>HotelSearch</RequestType>
						</Head>
						<Body>
							'.$HotelIdxml.$CityIdxml.$CheckInDatexml.$CheckOutDate.'
							<Rooms>
								'.$roomsxml.'
							</Rooms>
							<Nationality>'.$Nationality.'</Nationality>
							<Currency>'.$this->Currency.'</Currency>
							<AvailableOnly>'.$AvailableOnly.'</AvailableOnly>
						</Body>
					</Request>';
        //echo '<pre>';print_r($request);
		$response = $this->SendPostRequestToTravellanda($this->MAIN_HOST, $request);
        //echo '<pre>';print_r($response);exit;
		return $response;
	}
	
	function HotelPolicies($OptionId)
	{
		$request ='<Request>
						<Head>
							<Username>'.$this->username.'</Username>
							<Password>'.$this->password.'</Password>
							<RequestType>HotelPolicies</RequestType>
						</Head>
						<Body>
							<OptionId>'.$OptionId.'</OptionId>
						</Body>
					</Request>';
        //echo '<pre>';print_r($request);
		$response = $this->SendPostRequestToTravellanda($this->MAIN_HOST, $request);
        //echo '<pre>';print_r($response);exit;
		return $response;
	}
	
	/*$Rooms = array(
		array(
			"RoomId" => $RoomId ,
			"PaxNames"=> array(
				"AdultName" => array(
					array(
						"Title" => $Title,
						"FirstName" => $FirstName,
						"LastName" => $LastName,
					),
				),
				"ChildName" => array(
					array(
						"FirstName" => $FirstName,
						"LastName" => $LastName,
					)
				)
			)
		)
	)*/
	
	function HotelBooking($OptionId , $Rooms , $YourReference = "XMLTEST")
	{
		$OptionIdxml = "<OptionId>".$OptionId."</OptionId>";
		$YourReferencexml = "<YourReference>".$YourReference."</YourReference>";
		$roomsxml = "";
		foreach($Rooms as $singroom)
		{
			$RoomId = $singroom['RoomId'];
			$PaxNames = $singroom['PaxNames'];
			$AdultName = $singroom['PaxNames']['AdultName'];
			$adultxml = "";
			foreach($AdultName as $singadultname)
			{
				$Title 		= $singadultname['Title'];
				$FirstName 	= $singadultname['FirstName'];
				$LastName 	= $singadultname['LastName'];
				$adultxml .= '
					<AdultName>
						<Title>'.$Title.'</Title>
						<FirstName>'.$FirstName.'</FirstName>
						<LastName>'.$LastName.'</LastName>
					</AdultName>
				';
			}
			$ChildName = $singroom['PaxNames']['ChildName'];
			$childxml = "";
			foreach($ChildName as $singchildname)
			{
				$FirstName 	= $singchildname['FirstName'];
				$LastName 	= $singchildname['LastName'];
				$childxml .= '
					<ChildName>
						<FirstName>'.$FirstName.'</FirstName>
						<LastName>'.$LastName.'</LastName>
					</ChildName>
				';
			}
			$roomsxml .= '<Room>
				<RoomId>'.$RoomId.'</RoomId>
				<PaxNames>
					'.$adultxml.$childxml.'
				</PaxNames>
			</Room>';
		}
		$request ='<Request>
						<Head>
							<Username>'.$this->username.'</Username>
							<Password>'.$this->password.'</Password>
							<RequestType>HotelBooking</RequestType>
						</Head>
						<Body>
							'.$OptionIdxml.$YourReferencexml.'
							<Rooms>
								'.$roomsxml.'
							</Rooms>
						</Body>
					</Request>';
        //echo '<pre>';print_r($request);exit;
		$response = $this->SendPostRequestToTravellanda($this->MAIN_HOST, $request);
		return $response;
	}

    function CancelTLBooking($bookingRef)
    {
        $request ='<Request>
						<Head>
							<Username>'.$this->username.'</Username>
							<Password>'.$this->password.'</Password>
							<RequestType>HotelBookingCancel</RequestType>
						</Head>
						<Body>
							<BookingReference>'.$bookingRef.'</BookingReference>
						</Body>
					</Request>';
        $response = $this->SendPostRequestToTravellanda($this->MAIN_HOST, $request);
        return $response;
    }
	
	function SendPostRequestToTravellanda($url, $request) 
	{
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
                    "xml=" . $request);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300);
        $data = curl_exec($ch);
        curl_close($ch);
        //echo '<pre>';print_r($data);exit;
        $array_data = json_decode(json_encode(simplexml_load_string($data)), true);
        //echo '<pre>';print_r($array_data);exit;
		return $array_data;
    }
}


/***************
 * Direct functions
 */

function getRoomDetailByRoomTypeId($hotel,$roomTypeId){
    foreach ($hotel[0]['room_type_list'] as $val) {
        //echo '<pre>';print_r($val);exit;
        // change boardbases key
        foreach ($val['occupancy_list'] as $rk=>$room) {
            $val['occupancy_list'][$rk]['available_list'][0]['boardbases'] = $val['occupancy_list'][$rk]['available_list'][0]['BoardBases'];
            unset($val['occupancy_list'][$rk]['available_list'][0]['BoardBases']);
        }

        if($val['RoomTypeId'] == $roomTypeId){
            $room_type_list[] = $val;
            $hotel[0]['room_type_list'] = $room_type_list;
            continue;
        }
    }
    // add new params for detail page
    $hotel[0]['hotel_address'] = $hotel[0]['location']['address'];

return $hotel;
}
?>