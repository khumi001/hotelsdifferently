<?php
function tcpdf($id, $saveServer = false)
{
    $CI =& get_instance();
    $res = $CI->db->where('id', $id)->get('book_hotal')->result();
    //echo '<pre>';print_r($res);exit;
    if (count($res) > 0) {
        $res = $res[0];
        $reservatDet = '';
        $booing_detail = $CI->db->where('book_id', $id)->get('booking_detail')->result();
        $confimArray = array();
        if (count($booing_detail) > 0) {
            $reservatDet = '<table>
					<tr style="font-size:10px; font-weight:bold;">
						<th>Check-in:</th>
						<th>Check-out:</th>
						<th width="140">RoomType:</th>
						<th>Adults:</th>
						<th>Children:</th>
						<th>Confirmation#:</th>
					</tr>';
            foreach ($booing_detail as $bd) {
                $confimArray[] = $bd->confirm_no;
                $reservatDet .= '<tr style="font-size:10px; font-weight:normal;">
							<td>' . date(DISPLAY_DATE_FORMAT, strtotime($res->checkIn)) . '</td>
							<td>' . date(DISPLAY_DATE_FORMAT, strtotime($res->checkOut)) . '</td>
							<td width="140">' . $bd->roomType . '</td>
							<td>' . $bd->numOfAdults . '</td>
							<td>' . $bd->numOfChildren . '</td>
							<td>' . $bd->confirm_no . '</td>
						</tr>';
            }
            $reservatDet .= '</table>';
        }
        $payment_info = unserialize($res->payment_info);
        $supplemnts = unserialize($res->supplements);
        $supText = "";
        if (!empty($supplemnts) && count($supplemnts) != 0) {
            $supText = "Supplements paid by customer directly to the hotel<br>";
            foreach ($supplemnts as $supp) {
                $supText .= $supp['suppName'] . ':  <span>$' . $supp['publishPrice'] . '</span><br>';
            }
            $supText .= 'This amount does not include local taxes and should be paid directly to the hotel upon check out';
        }
        $cancel_pol = unserialize($res->cancel_pol);
        //echo '<pre>';print_r($cancel_pol);exit;
        $cpol = "";
        foreach ($cancel_pol as $cp) {
            $cpol .= $cp['message'];
        }
        $supplementStr = array();
        foreach ($booing_detail as $key => $bd) {
            $roomSupplement = unserialize($bd->RoomExtraInfo)['SelectedSupplements'];
            $supplementStr1 = "";
            if (isset($roomSupplement['Supplement']) && count($roomSupplement['Supplement']) == 1) {
                $supplementStr1 .= $roomSupplement['Supplement']['@attributes']['suppName'] . " $" . $roomSupplement['Supplement']['@attributes']['price'] . "<br>";
            } else if (isset($roomSupplement['Supplement']) && count($roomSupplement['Supplement']) > 1) {
                for ($si = 0; $si < count($roomSupplement['Supplement']); $si++) {
                    $supplementStr1 .= $roomSupplement['Supplement'][$si]['@attributes']['suppName'] . " $" . $roomSupplement['Supplement'][$si]['@attributes']['price'] . "<br>";
                }
            }
            $supplementStr[] = $supplementStr1;
        }

        $rName = "<br>";
        $srbu = "<br>";
        $room_pref_string = "<br>";
        $reomprefst = explode("@", $payment_info['room_pref_string']);
        $roomwisebrek = array();
        $roomwisebrekTax = array();
        $addsup = "";
        if (count($payment_info['roomInfo']) > 0) {
            foreach ($payment_info['roomInfo'] as $key => $singleUser) {
                $room_pref_string .= $confimArray[$key - 1] . ' - ' . $reomprefst[$key - 1] . "<br>";
                $rName .= $confimArray[$key - 1] . ' - ' . $singleUser['firstname'] . ' ' . $singleUser['lastname'] . "<br>";
                $srbu .= $confimArray[$key - 1] . ' - ' . $singleUser['splReq'] . "<br>";
                $roombrde = explode('|', $singleUser['optradio']);
                $allbreakdo = explode('-', $roombrde[4]);
                unset($allbreakdo[count($allbreakdo) - 1]);
                $roomwisebrek[] = $allbreakdo;
                $roomwisebrekTax[] = $roombrde[5];
                $addsup = $singleUser['additionalSupp'];
            }
        }


        $boardbases = "";
        if (isset($payment_info['roomInfo'])) {
            foreach ($payment_info['roomInfo'] as $key => $roomInfo) {
                if (isset($roomInfo['boardbase']) && !empty($roomInfo['boardbase'])) {
                    $boardbases .= "<br/><b>Room " . $key . "</b><br/>";
                    $borba = explode('|', $roomInfo['boardbase'][0]);
                    $boardbases .= $borba[2];
                }
            }
        }
        if (empty($boardbases)) {
            $boardbases = "<br>N/A";
        }

        $breakdown = "";
        $check_in = $res->checkIn;
        foreach ($roomwisebrek as $key => $singcheckrbd) {

            $rtax = $roomwisebrekTax[$key];

            $roomno = $key + 1;
            $inner = "";
            foreach ($singcheckrbd as $key1 => $singinner) {
                //$singinner = (float)$singinner + (float)$rtax;
                $singinner = (float)$singinner;
                $inner .= '
					<tr>
						<td colspan="6" style="font-size:10px;font-weight:normal;"><b>' . date(DISPLAY_DATE_FORMAT, strtotime($check_in . ' + ' . $key1 . ' days')) . ':</b> $' . number_format($singinner,2) . '</td>
					</tr>
					';

            }
            $breakdown .= '
					<tr>
						<td colspan="6" style="font-size:10px; font-weight:bold;">Room ' . $roomno . '</td>
					</tr>
					
						' . $inner . '
					
				';
        }

        /*<tr>
                <td colspan="6" style="font-size:10px; font-weight:bold;">Room 1</td>
            </tr>
            <tr>
                <td colspan="5" style="font-size:10px;font-weight:normal;">Wednesday, November 02,2016</td>
                <td style="font-size:10px; font-weight:bold;">$37.03</td>
            </tr>*/

        $price = substr($payment_info['total_room_cost'], 1, strlen($payment_info['total_room_cost']));
        //$taxes = substr($payment_info['total_tax_fees'],1,strlen($payment_info['total_tax_fees']));

        $payment_info['total_room_acost'] = '$' . $price;

        $total_supp_amount = substr($payment_info['total_supp_amount'], 1, strlen($payment_info['total_supp_amount']));
        //07-12-2016
        $included_supp_fees = substr($payment_info['included_supp_fees'], 1, strlen($payment_info['included_supp_fees']));
        $coupons = $res->coupons;
        if ($price < 0) {
            $price = 0;
        }
        //07-12-2016
        $price = $price + $total_supp_amount + $included_supp_fees;//+ $taxes
        //$price = $price + $taxes + $total_supp_amount;

        $coupons = '$' . $coupons;
        $price = '$' . number_format($price,2);
        $hotelinfo = unserialize($res->hotel_info);
        $voucher_remarks = "";
        if (isset($hotelinfo['all_info']['voucher_remarks'])) {
            $voucher_remarks = $hotelinfo['all_info']['voucher_remarks'];
        }
        $latitude = $hotelinfo['all_info']['latitude'];
        $longitude = $hotelinfo['all_info']['longitude'];
        $address = $hotelinfo['all_info']['address'];
        $city = $hotelinfo['all_info']['city'];
        $country = $hotelinfo['all_info']['country'];
        $newaddress = $res->hotelLocation;
        $crdinfo = unserialize($res->cardinfo);
        $html = '
			<table style="width:100%; font-style:normal !important; font-family:Arial, Helvetica, sans-serif;">
				<br>
				<br>
				<br>
				<tr>
					<th style="font-size:10px; margin:0 0 10px;">
						<strong style="text-decoration:underline;">Invoice issued by:</strong> <br>
						<strong>Wholesale Hotels Group.</strong>
						<br>
						<span style="font-size:10px; font-style:normal; font-weight:normal;">3651 Lindell Road D141 <br>Las Vegas, Nevada, 89103<br>United States of America</span>
						<br>
						<strong style="font-size:10px; font-weight:bold;">TAX ID: 45-4230568</strong>
					</th>
					<td style="text-align:right; font-size:10px; font-weight:normal;">
						' . $res->hotelName . '<br>
						' . $newaddress . '<br>
						Invoice issued: ' . date(DISPLAY_DATE_FORMAT_FULL, strtotime($res->date_time)) . '<br>
					</td>
				</tr>
				<br>
				<tr style="font-size:10px;">
					<p style="font-weight:normal;"><b>Billing name: </b>' . $crdinfo['firstname'] . ' ' . $crdinfo['lastname'] . '<br>
					<b>Billing address: </b>' . $crdinfo['street'] . ', ' . $crdinfo['city'] . ' ' . get_country_name_by_id($crdinfo['country']) . '<br>
					<b>Payment type: </b>' . $res->card_brand . ' + **** **** **** ' . $res->lastfour . '</p>
				</tr>
				<br>
				<tr style="text-decoration:underline; font-size:10px; font-weight:bold;">
					<h4 style="font-size:10px;">Reservation Details:</h4>
				</tr>
				
				<tr>
					' . $reservatDet . '
				</tr>
				
				<br>
				
				<tr>
					<td colspan="6" style="font-size:10px;
					font-weight:normal;">Please note that once you paid for your reservation, it is immediately confirmed and your room is guaranteed (with the exception of ON REQUEST bookings). However, every hotel processes and inputs incoming reservations differently, therefore it may happen that you contact the hotel and they may not see your booking until closer to your arrival. <b>Rest assured that your room is secured</b> the moment your reservation processes and a <b>WHotelsGroup.com confirmation number</b> is issued. Should you have any questions or concerns please do not hesitate to let us know via our Contact Us menu or the toll-free number that is listed on your reservation confirmation.</td>
				</tr>
				<br>
				<br>
				
				<tr>
					<td colspan="6" style="font-size:10px; font-weight:bold;">Payment Information: </td>
				</tr>
				<br>
				
				' . $breakdown . '
				<br>
				<tr>
					<td colspan="6" style="font-size:10px;
					font-weight:normal;"><b>Supplement:</b> ' . $payment_info['total_supp_amount'] . '</td>
				</tr>
				<!--07-12-2016-->
				<tr>
					<td colspan="6" style="font-size:10px;
					font-weight:normal;"><b>Included Supplements:</b> ' . $payment_info['included_supp_fees'] . '</td>
				</tr>
				<!--<tr>
					<td colspan="6" style="font-size:10px;
					font-weight:normal;"><b>Taxes:</b> ' . $payment_info['total_tax_fees'] . '</td>
				</tr>-->
				<tr>
					<td colspan="6" style="font-size:10px;
					font-weight:normal;"><b>Coupon:</b> ' . $coupons . '</td>
				</tr>
				<tr>
					<td colspan="6" style="font-size:10px;
					font-weight:normal; color:#ff0000; "><b>Your total charge for our professional services is: ' . $price . '. If room rates are shown, it is so that you can determine what the appropriate cancellation fee would be. </b></td>
				</tr>
				
				<br>
				<br>
				
				<tr>
					<td colspan="6" style="font-size:10px;
					font-weight:normal;"><b>Name on reservation:</b> ' . $rName . '</td>
				</tr>
				<br>
				<tr>
					<td colspan="6" style="font-size:10px;
					font-weight:normal;"><b>Bedding preference (not guaranteed): </b> ' . $room_pref_string . '</td>
				</tr>
				<br>
				<tr>
					<td colspan="6" style="font-size:10px;
					font-weight:normal;"><b>Special request by you (not guaranteed):</b> ' . $srbu . '</td>
				</tr>
				<br>
				
				<tr>
					<td colspan="6" style="font-size:10px;
					font-weight:normal;"><b>Hotel’s message:</b> ' . ((!empty($voucher_remarks) && $voucher_remarks != 'null') ? $voucher_remarks : 'N/A') . '</td>
				</tr>
				<br>
				
				<tr>
					<td colspan="6" style="font-size:10px;
					font-weight:normal;"><b>Included:</b> ' . ((!empty($addsup) && ($addsup != 'null')) ? $addsup : 'N/A') . '</td>
				</tr>
				<br>
				<tr>
					<td colspan="6" style="font-size:10px;
					font-weight:normal;"><b>Boardbases:</b> ' . $boardbases . '</td>
				</tr>
				<br>
				
				<tr>
					<td colspan="6" style="font-size:10px;
					font-weight:normal;"><b>Cancellation Policy:</b> ' . $cpol . '</td>
				</tr>
				<br>

				<tr>
					<td colspan="6" style="font-size:10px;
					font-weight:normal;"><b>Problems? NO PROBLEM! </b>Should you encounter any problems with the hotel, please send us a message immediately via the "<b>Contact Us</b>" page and select "<b>Problems with booking</b>" so that we can rectify any problems you may have experienced or call us at <b>888-287-2307</b>. Those emails enjoy high priority and you can expect a very fast reply.</td>
				</tr>
				<br>
				
			</table>';
        require_once('tcpdf/tcpdf.php');
        require_once('tcpdf/examples/example_003.php');
    } else {
        die("Request Forbiden.");
    }
}

function travelProVoucher($id,$user,$saveServer = false)
{
    $CI =& get_instance();
    $res = $CI->db->where('id', $id)->get('book_hotal')->result();
    $userDetail = $CI->db->where('user_id', $user->int_glcode)->get('travel_professional_detail')->result();
    /*echo '<pre>';print_r($res);
    echo '<pre>';print_r($userDetail);
    echo '<pre>';print_r($user);exit;*/
    //ini_set('display_errors', 1);
    //error_reporting(E_ALL);
    if (count($res) > 0 && count($userDetail)>0) {
        $res = $res[0];
        $userDetail = $userDetail[0];
        // country
        $countryDetail = $CI->db->where('id', $userDetail->country)->get('countries')->row_array();
        $stateDetail = $CI->db->where('id', $userDetail->state)->get('states')->row_array();
        $reservatDet = '';
        $booing_detail = $CI->db->where('book_id', $id)->get('booking_detail')->result();
        $bookingSet = array();
        $confimArray = array();
        $cNumber = '';
        if (count($booing_detail) > 0) {
            $bookingSet = $booing_detail;
            $bookingSet = $bookingSet[0];
            $reservatDet = '<table>
					<tr style="font-size:10px; font-weight:bold;">
						<th>Check-in:</th>
						<th>Check-out:</th>
						<th width="140">RoomType:</th>
						<th>Adults:</th>
						<th>Children:</th>
					</tr>';
            foreach ($booing_detail as $bd) {
                $confimArray[] = $bd->confirm_no;
                $cNumber = $bd->confirm_no;
                $reservatDet .= '<tr style="font-size:10px; font-weight:normal;">
							<td>' . date(DISPLAY_DATE_FORMAT, strtotime($res->checkIn)) . '</td>
							<td>' . date(DISPLAY_DATE_FORMAT, strtotime($res->checkOut)) . '</td>
							<td width="140">' . $bd->roomType . '</td>
							<td>' . $bd->numOfAdults . '</td>
							<td>' . $bd->numOfChildren . '</td>
						</tr>';
            }
            $reservatDet .= '</table>';
        }
        $payment_info = unserialize($res->payment_info);
        $supplemnts = unserialize($res->supplements);
        $supText = "";
        if (!empty($supplemnts) && count($supplemnts) != 0) {
            $supText = "Supplements paid by customer directly to the hotel<br>";
            foreach ($supplemnts as $supp) {
                $supText .= $supp['suppName'] . ':  <span>$' . $supp['publishPrice'] . '</span><br>';
            }
            $supText .= 'This amount does not include local taxes and should be paid directly to the hotel upon check out';
        }
        $cancel_pol = unserialize($res->cancel_pol);
        //echo '<pre>';print_r($cancel_pol);exit;
        $cpol = "";
        foreach ($cancel_pol as $cp) {
            $cpol .= $cp['message'];
        }
        $supplementStr = array();
        foreach ($booing_detail as $key => $bd) {
            $roomSupplement = unserialize($bd->RoomExtraInfo)['SelectedSupplements'];
            $supplementStr1 = "";
            if (isset($roomSupplement['Supplement']) && count($roomSupplement['Supplement']) == 1) {
                $supplementStr1 .= $roomSupplement['Supplement']['@attributes']['suppName'] . " $" . $roomSupplement['Supplement']['@attributes']['price'] . "<br>";
            } else if (isset($roomSupplement['Supplement']) && count($roomSupplement['Supplement']) > 1) {
                for ($si = 0; $si < count($roomSupplement['Supplement']); $si++) {
                    $supplementStr1 .= $roomSupplement['Supplement'][$si]['@attributes']['suppName'] . " $" . $roomSupplement['Supplement'][$si]['@attributes']['price'] . "<br>";
                }
            }
            $supplementStr[] = $supplementStr1;
        }

        $rName = "<br>";
        $srbu = "<br>";
        $room_pref_string = "<br>";
        $reomprefst = explode("@", $payment_info['room_pref_string']);
        $roomwisebrek = array();
        $roomwisebrekTax = array();
        $addsup = "";
        if (count($payment_info['roomInfo']) > 0) {
            foreach ($payment_info['roomInfo'] as $key => $singleUser) {
                $room_pref_string .= $confimArray[$key - 1] . ' - ' . $reomprefst[$key - 1] . "<br>";
                $rName .= $confimArray[$key - 1] . ' - ' . $singleUser['firstname'] . ' ' . $singleUser['lastname'] . "<br>";
                $srbu .= $confimArray[$key - 1] . ' - ' . $singleUser['splReq'] . "<br>";
                $roombrde = explode('|', $singleUser['optradio']);
                $allbreakdo = explode('-', $roombrde[4]);
                unset($allbreakdo[count($allbreakdo) - 1]);
                $roomwisebrek[] = $allbreakdo;
                $roomwisebrekTax[] = $roombrde[5];
                $addsup = $singleUser['additionalSupp'];
            }
        }


        $boardbases = "";
        if (isset($payment_info['roomInfo'])) {
            foreach ($payment_info['roomInfo'] as $key => $roomInfo) {
                if (isset($roomInfo['boardbase']) && !empty($roomInfo['boardbase'])) {
                    $boardbases .= "<br/><b>Room " . $key . "</b><br/>";
                    $borba = explode('|', $roomInfo['boardbase'][0]);
                    $boardbases .= $borba[2];
                }
            }
        }
        if (empty($boardbases)) {
            $boardbases = "<br>N/A";
        }

        $breakdown = "";
        $check_in = $res->checkIn;
        foreach ($roomwisebrek as $key => $singcheckrbd) {

            $rtax = $roomwisebrekTax[$key];

            $roomno = $key + 1;
            $inner = "";
            foreach ($singcheckrbd as $key1 => $singinner) {
                //$singinner = (float)$singinner + (float)$rtax;
                $singinner = (float)$singinner;
                $inner .= '
					<tr>
						<td colspan="6" style="font-size:10px;font-weight:normal;"><b>' . date(DISPLAY_DATE_FORMAT, strtotime($check_in . ' + ' . $key1 . ' days')) . ':</b> $' . number_format($singinner,2) . '</td>
					</tr>
					';

            }
            $breakdown .= '
					<tr>
						<td colspan="6" style="font-size:10px; font-weight:bold;">Room ' . $roomno . '</td>
					</tr>

						' . $inner . '

				';
        }

        /*<tr>
                <td colspan="6" style="font-size:10px; font-weight:bold;">Room 1</td>
            </tr>
            <tr>
                <td colspan="5" style="font-size:10px;font-weight:normal;">Wednesday, November 02,2016</td>
                <td style="font-size:10px; font-weight:bold;">$37.03</td>
            </tr>*/

        $price = substr($payment_info['total_room_cost'], 1, strlen($payment_info['total_room_cost']));
        //$taxes = substr($payment_info['total_tax_fees'],1,strlen($payment_info['total_tax_fees']));

        $payment_info['total_room_acost'] = '$' . $price;

        $total_supp_amount = substr($payment_info['total_supp_amount'], 1, strlen($payment_info['total_supp_amount']));
        //07-12-2016
        $included_supp_fees = substr($payment_info['included_supp_fees'], 1, strlen($payment_info['included_supp_fees']));
        $coupons = $res->coupons;
        if ($price < 0) {
            $price = 0;
        }
        //07-12-2016
        $price = $price + $total_supp_amount + $included_supp_fees;//+ $taxes
        //$price = $price + $taxes + $total_supp_amount;

        $coupons = '$' . $coupons;
        $price = '$' . number_format($price,2);
        $hotelinfo = unserialize($res->hotel_info);
        $voucher_remarks = "";
        if (isset($hotelinfo['all_info']['voucher_remarks'])) {
            $voucher_remarks = $hotelinfo['all_info']['voucher_remarks'];
        }
        $latitude = $hotelinfo['all_info']['latitude'];
        $longitude = $hotelinfo['all_info']['longitude'];
        $address = $hotelinfo['all_info']['address'];
        $city = $hotelinfo['all_info']['city'];
        $country = $hotelinfo['all_info']['country'];
        $newaddress = $res->hotelLocation;
        $crdinfo = unserialize($res->cardinfo);
        $html = '
			<table style="width:100%; font-style:normal !important; font-family:Arial, Helvetica, sans-serif;">
				<br>
				<br>
				<br>
				<tr>
					<th style="font-size:10px; margin:0 0 10px;">
						<strong style="text-decoration:underline;">Invoice issued by:</strong> <br>
						<strong>'.$userDetail->company_name.'</strong>
						<br>
						<span style="font-size:10px; font-style:normal; font-weight:normal;">'.$userDetail->street.'</span>
						<br>
						<span style="font-size:10px; font-style:normal; font-weight:normal;">'.$userDetail->city.', '.@$stateDetail['name'].'</span>
						<br>
						<strong style="font-size:10px; font-weight:bold;">'.@$countryDetail['name'].'</strong>
						<br>
						<strong style="font-size:10px; font-weight:bold;">'.$userDetail->phone_country_code.''.$userDetail->phone_number.''.$userDetail->phone_ex.'</strong>
					</th>
					<td style="text-align:right; font-size:10px; font-weight:normal;">
						' . $res->hotelName . '<br>
						' . $newaddress . '<br>
						Confirmation#: '.$cNumber.'<br>
					</td>
				</tr>
				<br>
				<tr style="text-decoration:underline; font-size:10px; font-weight:bold;">
					<h4 style="font-size:10px;">Reservation Details:</h4>
				</tr>

				<tr>
					' . $reservatDet . '
				</tr>

				<br>

				<tr>
					<td colspan="6" style="font-size:10px;
					font-weight:normal;">Please note that once you paid for your reservation, it is immediately confirmed and your room is guaranteed (with the exception of ON REQUEST bookings). However, every hotel processes and inputs incoming reservations differently, therefore it may happen that you contact the hotel and they may not see your booking until closer to your arrival. <b>Rest assured that your room is secured</b> the moment your reservation processes and a confirmation number is issued.</td>
				</tr>
				<br>
				<br>

				<tr>
					<td colspan="6" style="font-size:10px;
					font-weight:normal;"><b>Name on reservation:</b> ' . $rName . '</td>
				</tr>
				<br>

				<tr>
					<td colspan="6" style="font-size:10px;
					font-weight:normal;"><b>Hotel remarks:</b> ' . ((!empty($voucher_remarks) && $voucher_remarks != 'null') ? $voucher_remarks : 'N/A') . '</td>
				</tr>
				<br>

				<tr>
					<td colspan="6" style="font-size:10px;
					font-weight:normal;"><b>Special request by you (not guaranteed):</b> ' . $srbu . '</td>
				</tr>
				<br>

				<tr>
					<td colspan="6" style="font-size:10px;
					font-weight:normal;"><b>Cancellation Policy:</b> ' . $cpol . '</td>
				</tr>
				<br>

				<tr>
					<td colspan="6" style="font-size:10px;
					font-weight:normal;"><b>CUSTOMER SERVICE: </b>Please don’t hesitate to contact our company at <b>'.$userDetail->phone_country_code.''.$userDetail->phone_number.''.$userDetail->phone_ex.'</b> in case you need assistance with your reservation.</td>
				</tr>
				<br>

			</table>';
        require_once('tcpdf/tcpdf.php');
        require_once('tcpdf/examples/voucher.php');
        //echo $html;exit;
    } else {
        die("Request Forbiden.");
    }
}
?>