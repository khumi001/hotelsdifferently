<?php
//Add due to temp table

// min price for slider
/*function getMinPriceByUniqueSessionID($data)
{

    $first_key = key($data);
    if(count($data)>0){
        return round($data[$first_key][0]);
    }else{
        return false;
    }

}
function getMaxPriceByUniqueSessionID($data)
{
    end($data);
    $last_index = key($data);
    if(count($data)>0){
        return round($data[$last_index][0]);
    }else{
        return false;
    }

}*/
function getMinPriceByUniqueSessionID($uniqueSession)
{
    $CI =& get_instance();
    $res = $CI->db->select('MIN(min_price_sum_in_tax) as min_price_sum_in_tax')->where('session_id' , $uniqueSession)->get('temp_hotels_data')->result();
    if(count($res) > 0)
    {
        return round($res[0]->min_price_sum_in_tax);
    }
    else
    {
        return false;
    }
}
function getMaxPriceByUniqueSessionID($uniqueSession)
{
    $CI =& get_instance();
    $res = $CI->db->select('Max(min_price_sum_in_tax) as min_price_sum_in_tax')->where('session_id' , $uniqueSession)->get('temp_hotels_data')->result();
    if(count($res) > 0)
    {
        return round($res[0]->min_price_sum_in_tax);
    }
    else
    {
        return false;
    }
}
function convetsearlizeInarrayHotels($data = array())
{
    foreach($data as $key => $singarr)
    {
        $data[$key]['aminities'] = unserialize($singarr['aminities']);
        $data[$key]['location'] = unserialize($singarr['location']);
        $data[$key]['room_type_list'] = unserialize($singarr['room_type_list']);
    }
    return $data;
}
function getSortDataByUniqueSessionIDInfo($uniqueSession , $filers = array())
{
    $CI =& get_instance();
    if(isset($filers['stars']) && !empty($filers['stars']))
    {
        $starstr = "(";
        foreach($filers['stars'] as $key => $star)
        {
            if($key == 0)
            {
                $starstr .= " star_level = ". $star;
            }
            else
            {
                $starstr .= " OR star_level = ". $star;
            }

        }
        $starstr .= ")";
        $CI->db->where($starstr);
    }
    if(isset($filers['brand']) && !empty($filers['brand']))
    {
        $brandstr = "(";
        foreach($filers['brand'] as $key => $brand)
        {
            if($key == 0)
            {
                $brandstr .= " hotel_brand_name = '$brand'";
            }
            else
            {
                $brandstr .= " OR hotel_brand_name = '$brand'";
            }

        }
        $brandstr .= ")";
        $CI->db->where($brandstr);
    }
    if(isset($filers['hotel_name']))
    {
        $CI->db->like('hotelName', $filers['hotel_name']);
    }
    if(isset($filers['min_price']))
    {
        $CI->db->where('min_price_sum_in_tax >' , $filers['min_price'] - 1);
    }
    if(isset($filers['max_price']))
    {
        $CI->db->where('min_price_sum_in_tax <' , $filers['max_price'] + 1);
    }
    $res = $CI->db->where('session_id' , $uniqueSession)->get('temp_hotels_data')->result_array();
    return array(
        "total_record" => count($res)
    );
}
function getSortDataByUniqueSessionID($uniqueSession , $page = 1 ,$filers = array() , $sort = "(only_min_price + only_min_price_tax)" , $sorttype = "asc" )
{
    $CI =& get_instance();
    $limit = $CI->config->item('perPageHotel');
    $start = ($page * $limit) - $limit;
    if(isset($filers['stars']) && !empty($filers['stars']))
    {
        $starstr = "(";
        foreach($filers['stars'] as $key => $star)
        {
            if($key == 0)
            {
                $starstr .= " star_level = ". $star;
            }
            else
            {
                $starstr .= " OR star_level = ". $star;
            }

        }
        $starstr .= ")";
        $CI->db->where($starstr);
    }
    if(isset($filers['brand']) && !empty($filers['brand']))
    {
        $brandstr = "(";
        foreach($filers['brand'] as $key => $brand)
        {
            if($key == 0)
            {
                $brandstr .= " hotel_brand_name = '$brand'";
            }
            else
            {
                $brandstr .= " OR hotel_brand_name = '$brand'";
            }

        }
        $brandstr .= ")";
        $CI->db->where($brandstr);
    }
    if(isset($filers['hotel_name']))
    {
        $CI->db->like('hotelName', $filers['hotel_name']);
    }
    if(isset($filers['min_price']))
    {
        $CI->db->where('(min_price_sum_in_tax) >' , $filers['min_price'] - 1);
    }
    if(isset($filers['max_price']))
    {
        $CI->db->where('(min_price_sum_in_tax) <' , $filers['max_price'] + 1);
    }
    $res = $CI->db->limit($limit, $start)->order_by($sort , $sorttype)->where('session_id' , $uniqueSession)->get('temp_hotels_data')->result_array();
    if(count($res) > 0)
    {
        return convetsearlizeInarrayHotels($res);
    }
    else
    {
        return array();
    }
}



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function getRoomConfNumbers($id)
{
    $CI =& get_instance();
    $res = $CI->db->select('confirm_no')->where('book_id' , $id)->get('booking_detail')->result();

    if(count($res) > 0)
    {
        return $res;
    }
    else
    {
        return false;
    }
}
function changePathToSSL($path)
{
    return str_replace("http", "https", $path);
}
function getHotelRoomsCount($id)
{
    $CI =& get_instance();
    $res = $CI->db->where('id' , $id)->get('book_hotal')->result();

    if(count($res) > 0)
    {
        return $res[0]->hotel_bkg;
    }
    else
    {
        return false;
    }
}
function getSourcename($id)
{
    $CI =& get_instance();
    $res = $CI->db->where('id' , $id)->get("dz_booking_source")->result();
    if(count($res) > 0)
    {
        return $res[0]->name;
    }
    else
    {
        return false;
    }
}
function getSourceOfReservation($id)
{
    $CI =& get_instance();
    $res = $CI->db->where('id' , $id)->get("booking_detail")->result();
    if(count($res) > 0)
    {
        return $res[0]->sourceID;
    }
    else
    {
        return false;
    }
}
function getSourcePhone($id)
{
    $CI =& get_instance();
    $res = $CI->db->where('id' , $id)->get("dz_booking_source")->result();
    if(count($res) > 0)
    {
        return $res[0]->phone;
    }
    else
    {
        return false;
    }
}

function getcancelpolicy($id)
{
    $CI =& get_instance();
    $res = $CI->db->where('id' , $id)->get('book_hotal')->result();

    if(count($res) > 0)
    {
        return $res[0]->cancel_pol;
    }
    else
    {
        return false;
    }

}

function getcomrescoupon($id)
{
    $CI =& get_instance();
    $res = $CI->db->where('id' , $id)->get('book_hotal')->result();

    if(count($res) > 0)
    {
        return $res[0]->coupons;
    }
    else
    {
        return false;
    }

}

function getPaymentInfo($id)
{
    $CI =& get_instance();
    $res = $CI->db->where('id' , $id)->get('book_hotal')->result();

    if(count($res) > 0)
    {
        return $res[0]->payment_info;
    }
    else
    {
        return false;
    }

}

function getBookingType($id)
{
    $CI =& get_instance();
    $res = $CI->db->where('id' , $id)->get('book_hotal')->result();

    if(count($res) > 0)
    {
        return $res[0]->Type;
    }
    else
    {
        return false;
    }

}

function xml2array ( $xmlObject, $out = array () )
{
    foreach ( (array) $xmlObject as $index => $node )
        $out[$index] = ( is_object ( $node ) ) ? xml2array ( $node ) : $node;

    return $out;
}



function getBookingLocation($id)
{
    $CI =& get_instance();
    $res = $CI->db->where('id' , $id)->get('book_hotal')->result();

    if(count($res) > 0)
    {
        return $res[0]->Location;
    }
    else
    {
        return false;
    }

}

function getBookinghotelName($id)
{
    $CI =& get_instance();
    $res = $CI->db->where('id' , $id)->get('book_hotal')->result();

    if(count($res) > 0)
    {
        return $res[0]->hotelName;
    }
    else
    {
        return false;
    }

}

function getBookingcardNo($id)
{
    $CI =& get_instance();
    $res = $CI->db->where('id' , $id)->get('book_hotal')->result();

    if(count($res) > 0)
    {
        return $res[0]->lastfour;
    }
    else
    {
        return false;
    }
}

function getBookingcheckIn($id)
{
    $CI =& get_instance();
    $res = $CI->db->where('id' , $id)->get('book_hotal')->result();

    if(count($res) > 0)
    {
        // display date format
        return date(DISPLAY_DATE_FORMAT, strtotime($res[0]->checkIn));
    }
    else
    {
        return false;
    }

}

function getBookingcheckOut($id)
{
    $CI =& get_instance();
    $res = $CI->db->where('id' , $id)->get('book_hotal')->result();

    if(count($res) > 0)
    {
        // display date format
        return date(DISPLAY_DATE_FORMAT, strtotime($res[0]->checkOut));
    }
    else
    {
        return false;
    }
}

function getUserById($id)
{
    $CI =& get_instance();
    $res = $CI->db->where('int_glcode' , $id)->get(' dz_user')->result();
    if(count($res) > 0)
    {
        return $res[0];
    }
    else
    {
        return false;
    }
}

function get_user_purchase_amount($id)
{
    $CI =& get_instance();
    $res = $CI->db->where('uid' , $id)->get('book_hotal')->result();
    if(count($res) > 0)
    {
        $amount = 0;
        foreach($res as $amo)
        {
            $amount = $amount + getBookingperRoomPaid($amo->id);
        }
        return $amount;
    }
    else
    {
        return 0;
    }
}

function getBookingDateTime($id)
{
    $CI =& get_instance();
    $res = $CI->db->where('id' , $id)->get('book_hotal')->result();
    if(count($res) > 0)
    {
        return date(DISPLAY_DATE_FORMAT_FULL, strtotime($res[0]->date_time));
    }
    else
    {
        return false;
    }
}

function getBookingperRoomPaid($id)
{
    $CI =& get_instance();
    $res = $CI->db->where('id' , $id)->get('book_hotal')->result();
//var_dump($res);exit;
    if(count($res) > 0)
    {
        $price = substr($res[0]->paid,1,strlen($res[0]->paid));
        $roomPrice = $price / $res[0]->rooms;
        return number_format((float)$roomPrice, 2, '.', '');
    }
    else
    {
        return false;
    }

}

function getExtraPerperRoom($id)
{
    $CI =& get_instance();
    $res = $CI->db->where('id' , $id)->get('book_hotal')->result();

    if(count($res) > 0)
    {
        return $res[0]->percentage;
    }
    else
    {
        return false;
    }

}

function getCouponperRoomPaid($id)
{
    $CI =& get_instance();
    $res = $CI->db->where('id' , $id)->get('book_hotal')->result();

    if(count($res) > 0)
    {
        $price = $res[0]->coupons;
        $coupons = $price / $res[0]->rooms;
        return number_format((float)$coupons, 2, '.', '');
    }
    else
    {
        return false;
    }

}



Class HotelSearchAPIResponseProcess {

    private $hotelSearchAPICoreObj;
    private $destinationXMLLocation = 'a.xml';
    private $brandListFileLocation = 'brand_list.dat';

    function __construct() {
        $this->hotelSearchAPICoreObj = new HotelSearchAPICore();
    }

    function clean_object($obj) {
        return !empty($obj) ? $obj->__toString() : '';
    }

    function clean_speatialChar($var) {
        return preg_replace('/[^A-Za-z0-9\-]/', '', $var);
    }

    /* function SaveDestinationList()
      {
      $distanceAPIResponse = $this->hotelSearchAPICoreObj->GetDestinationList();
      file_put_contents( $this->destinationXMLLocation, $distanceAPIResponse);
      } */

    function GetDestinationList() {
        $distanceAPIResponse = $this->hotelSearchAPICoreObj->GetDestinationList();
        $simpleXMLELementObj = simplexml_load_string($distanceAPIResponse);
        $continentList = $simpleXMLELementObj->children('http://schemas.xmlsoap.org/soap/envelope/')->Body->children('http://touricoholidays.com/WSDestinations/2008/08/DataContracts')->DestinationResponse->DestinationResult->Continent;
        $destinationList = array();
        $totalContinent = $continentList->count();

        $locationList = array();
        for ($conIndex = 0; $conIndex < $totalContinent; $conIndex++) {
            $aContinent = $continentList[$conIndex];
            $countryList = $aContinent->Country;
            $totalCountry = $countryList->count();
            //var_dump($totalCountry );
            for ($countryIndex = 0; $countryIndex < $totalCountry; $countryIndex++) {
                $aCountry = $countryList[$countryIndex];
                $countryName = $aCountry->attributes()->name->__toString();

                $stateList = $aCountry->State;
                $totalState = count($stateList);
                for ($stateIndex = 0; $stateIndex < $totalState; $stateIndex++) {
                    $aState = $stateList[$stateIndex];
                    $stateName = $aState->attributes()->name->__toString();
                    if (strlen($stateName) > 0)
                        $stateName .= ", ";

                    $cityList = $aState->City;
                    $totalCity = count($cityList);
                    //echo $totalCity . '<br /><br />';
                    for ($cityIndex = 0; $cityIndex < $totalCity; $cityIndex++) {
                        $aCity = $cityList[$cityIndex];
                        $cityAttributes = $aCity->attributes();
                        $cityName = $cityAttributes->name->__toString();
                        //var_dump($cityAttributes);
                        $cityLatitude = $cityAttributes->cityLatitude->__toString();
                        $cityLongitude = $cityAttributes->cityLongitude->__toString();
                        $cityDestinationId = $cityAttributes->destinationId->__toString();
                        $locationList[] = array(
                            'destination_id' => $cityDestinationId,
                            'destination_name' => $cityName . ', ' . $stateName . $countryName,
                            'destination_code' => $cityAttributes->destinationCode->__toString(),
                            'city_name' => $cityName,
                            'location_name' => "",
                            'city_latitude' => $cityLatitude,
                            'city_longitude' => $cityLongitude,
                            'country_name' => $countryName,
                            'parent_destination_id' => '0'
                        );
                        $cityLocationList = $aCity->CityLocation;
                        if (isset($cityLocationList)) {
                            $totalCityLocationList = count($cityLocationList);
                            for ($cityLocationIndex = 0; $cityLocationIndex < $totalCityLocationList; $cityLocationIndex++) {
                                $aCityLocation = $cityLocationList[$cityLocationIndex];
                                $cityLocationAttributes = $aCityLocation->attributes();
                                $cityLocationName = $cityLocationAttributes->location->__toString();

                                $locationList[] = array(
                                    'destination_id' => $cityLocationAttributes->destinationId->__toString(),
                                    'destination_name' => $cityLocationName . ', ' . $cityName . ', ' . $stateName . $countryName,
                                    'destination_code' => $cityLocationAttributes->destinationCode->__toString(),
                                    'city_name' => $cityName,
                                    'location_name' => $cityLocationName,
                                    'city_latitude' => $cityLatitude,
                                    'city_longitude' => $cityLongitude,
                                    'country_name' => $countryName,
                                    'parent_destination_id' => $cityDestinationId
                                );
                            }
                        }
                    }
                }

                /* $destinationList[] = array(
                  'country'   =>  $countryName,
                  'location_list' =>  $locationList
                  ); */
            }
        }
        /* echo "<pre>";
          print_r($destinationList );
          echo "</pre>"; */
        return $locationList;
    }

    function ProcessBrandList() {
        $handle = fopen($this->brandListFileLocation, "r");
        $brandList = array();
        if ($handle) {
            while (($aBrand = fgets($handle)) !== false) {
                $brandList[] = $aBrand;
            }
            fclose($handle);
        }
        return $brandList;
    }

    function GetDestinationDetailsByDestinationCode($destinationId) {
        $destinationList = $this->ProcessDestinationFromXML();
        foreach ($destinationList as $aCountryDestination) {
            foreach ($aCountryDestination['location_list'] as $aDestination) {
                if ($aDestination['destination_id'] == $destinationId)
                    return $aDestination;
            }
        }
        return null;
    }

    function ProcessSearchResult($roomInfoStr, $destinationCode, $hotelCityName, $checkInDate, $checkOutDate, $adultNumber = 1, $hotelLocationName = "", $hotelName = "", $childNumber, $childAges, $StarLevel = 0, $brandName = "", $roomNo = 0)
    {

        //error_reporting(E_ALL);
        //ini_set('display_errors', 1);
        ini_set('memory_limit', '512M');
        $hotel_location_name = "";
        $response = $this->hotelSearchAPICoreObj->SearchHotel($roomInfoStr, $destinationCode, $hotelCityName, $checkInDate, $checkOutDate, $adultNumber, $hotelLocationName, $hotelName, $childNumber, $childAges, $StarLevel);
        file_put_contents("hotel_search_results.xml", $response);
        $simpleXMLELementObj = simplexml_load_string($response);
        //print $simpleXMLELementObj->asXML();exit;
        $response = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $response);


        $xml = new SimpleXMLElement($response);
        $body = $xml->xpath('//sBody')[0];

        $array = json_decode(json_encode((array)$body), TRUE);
        if(isset($array['sFault']))
        {
            return array();
        }
        else
        {
            $hotelList = $simpleXMLELementObj->children('http://schemas.xmlsoap.org/soap/envelope/')->Body->children('http://tourico.com/webservices/hotelv3')->SearchHotelsResponse->SearchHotelsResult->children()->HotelList->Hotel;
        }
        $totalHotel = count($hotelList);
        //echo '<pre>';print_r($hotelList);exit;
        $hotelDetailList = array();
        for ($hotelIndex = 0; $hotelIndex < $totalHotel; $hotelIndex++)
        {
            $aHotel = $hotelList[$hotelIndex];
            $aHotelarr = xml2array($aHotel);
            //echo '<pre>';print_r($aHotelarr);exit;
            /*if($aHotelarr['@attributes']['name'] == 'Radisson Ontario Airport Hotel - Demo'){
                echo '<pre>';print_r($aHotelarr);exit;
            }*/
            //echo '<pre>';print_r($aHotelarr);exit;
            $aHotelAttributes = $aHotel->attributes();
            $roomTypeList = $aHotel->RoomTypes->RoomType;
            $roomTypeCount = count($roomTypeList);
            $roomTypeArr = array();
            $minimunPriceOfHotels = 25000;
            $minimunPriceOfHotels_tax = 25000;

            for ($roomType_index = 0; $roomType_index < $roomTypeCount; $roomType_index++)
            {
                $aRoomType = $roomTypeList[$roomType_index];
                $aRoomTypeArr = xml2array($roomTypeList[$roomType_index]);
                $nights = $aRoomTypeArr['@attributes']['nights'];
                $discount = array();
                if(isset($aRoomTypeArr['Discount']))
                {
                    if(isset($aRoomTypeArr['Discount']['@attributes']))
                    {
                        if(isset($aRoomTypeArr['Discount']['@attributes']['type']))
                        {
                            $discount = array(
                                "discType" => "ProgressivePromotion",
                                "from" => date("m/d/Y", strtotime($aRoomType->Discount->attributes()->from->__toString())),
                                "to" => date("m/d/Y", strtotime($aRoomType->Discount->attributes()->to->__toString())),
                                "type" => $aRoomTypeArr['Discount']['@attributes']['type'],
                                "value" => $aRoomTypeArr['Discount']['@attributes']['value'],
                                "name" => (isset($aRoomTypeArr['Discount']['@attributes']['name'])) ? $aRoomTypeArr['Discount']['@attributes']['name'] : ""
                            );
                        }
                        else
                        {
                            $discount = array(
                                "discType" => "PayStayPromotion",
                                "from" => date("m/d/Y", strtotime($aRoomType->Discount->attributes()->from->__toString())),
                                "to" => date("m/d/Y", strtotime($aRoomType->Discount->attributes()->to->__toString())),
                                "pay" => $aRoomTypeArr['Discount']['@attributes']['pay'],
                                "stay" => $aRoomTypeArr['Discount']['@attributes']['stay']
                            );
                        }
                    }
                }
                $perDayAvailabilityXmlObjList = array();
                if(isset($aRoomTypeArr['AvailabilityBreakdown']['Availability']))
                {
                    $perDayAvailabilityXmlObjList = $aRoomTypeArr['AvailabilityBreakdown']['Availability'];
                }
                $totalAvailability = count($perDayAvailabilityXmlObjList);
                $perDayAvailabilityList = array();
                for ($availabilityIndex = 0; $availabilityIndex < $totalAvailability; $availabilityIndex++)
                {
                    if(isset($perDayAvailabilityXmlObjList['@attributes']))
                    {
                        $anAvailabiity = $perDayAvailabilityXmlObjList;
                    }
                    else
                    {
                        $anAvailabiity = xml2array($perDayAvailabilityXmlObjList[$availabilityIndex]);
                    }
                    $index_status = isset( $anAvailabiity['@attributes']['status'] ) ? $anAvailabiity['@attributes']['status'] : false;
                    $perDayAvailabilityList[] = isset($anAvailabiity['@attributes']['status']) ? $anAvailabiity['@attributes']['status'] : $index_status;
                }

                $xmlOccupancyList = $aRoomType->Occupancies->Occupancy;
                $totalOccupancy = count($xmlOccupancyList);
                $occupancyArr = array();
                $suppArr      = array();
                $unAray = array();
                for ($jindex = 0; $jindex < $totalOccupancy; $jindex++)
                {
                    $eachRoom = $xmlOccupancyList[$jindex]->Rooms;
                    for($tt = 0; $tt < count($eachRoom->Room); $tt++) {
                        $r = (array)$eachRoom->Room[$tt]->attributes()['seqNum'];
                        $anOccupancy = $xmlOccupancyList[$jindex];
                        $BoardBases = array();
                        foreach ($anOccupancy->BoardBases as $key => $sing) {
                            foreach ($sing as $singinner) {
                                $BoardBases[] = array(
                                    "bbId" => $singinner->attributes()->bbId->__toString(),
                                    "bbName" => $singinner->attributes()->bbName->__toString(),
                                    "bbPrice" => $singinner->attributes()->bbPrice->__toString(),
                                    "bbPublishPrice" => $singinner->attributes()->bbPublishPrice->__toString(),
                                );
                            }
                        }
                        $occupancyAttr = $anOccupancy->attributes();
                        $troomtax = $occupancyAttr->taxPublish->__toString();
                        $roomtax = $occupancyAttr->taxPublish->__toString() / $nights;

                        $roomXMlObj = $anOccupancy->Rooms->Room;
                        $roomSeqNo = $roomXMlObj->attributes()->seqNum->__toString();
                        $roomSeqNo = $r[0];
                        $roomeName = 'Room ' . $roomSeqNo;

                        $price_break_down_list = $anOccupancy->PriceBreakdown->Price;
                        $price_break_down_list_count = count($price_break_down_list);
                        $per_day_price = [];
                        for ($per_day_price_index = 0; $per_day_price_index < $price_break_down_list_count; $per_day_price_index++) {
                            $per_day_price[] = $price_break_down_list[$per_day_price_index]->attributes()->value->__toString() - $roomtax;
                        }

                        if (!isset($occupancyArr[$roomeName])) {
                            $occupancyArr[$roomeName] = array('room_sequnce' => $roomSeqNo, 'available_list' => array());
                        }
                        if (($occupancyAttr->avrNightPrice->__toString() - $roomtax) < $minimunPriceOfHotels) {
                            $minimunPriceOfHotels = $occupancyAttr->avrNightPrice->__toString() - $roomtax;
                            $minimunPriceOfHotels_tax = $roomtax;
                        }
                        $occupancyArr[$roomeName]['available_list'][] = array(
                            'per_night_price' => $occupancyAttr->avrNightPrice->__toString() - $roomtax,
                            'per_night_tax' => $roomtax,//$occupancyAttr->taxPublish->__toString(),
                            'max_guest' => $occupancyAttr->maxGuests->__toString(),
                            'max_child' => $occupancyAttr->maxChild->__toString(),
                            'total_price' => $occupancyAttr->occupPrice->__toString() - $troomtax,
                            'total_tax' => $troomtax,
                            'bedding' => $occupancyAttr->bedding->__toString(),
                            'per_day_price' => $per_day_price,
                            'per_day_availability' => $perDayAvailabilityList,
                            'BoardBases' => $BoardBases
                        );
                        $occupancyArr2[]['available_list'][] = array(
                            'per_night_price' => $occupancyAttr->avrNightPrice->__toString() - $roomtax,
                            'per_night_tax' => $roomtax,//$occupancyAttr->taxPublish->__toString(),
                            'max_guest' => $occupancyAttr->maxGuests->__toString(),
                            'max_child' => $occupancyAttr->maxChild->__toString(),
                            'total_price' => $occupancyAttr->occupPrice->__toString() - $troomtax,
                            'total_tax' => $troomtax,
                            'bedding' => $occupancyAttr->bedding->__toString(),
                            'per_day_price' => $per_day_price,
                            'per_day_availability' => $perDayAvailabilityList,
                            'room_name' => $roomeName,
                            'BoardBases' => $BoardBases
                        );
                        $occupancyArr2 = array();
                        $suppXMlObj = $anOccupancy->SelctedSupplements->Supplement;
                        $totalSupp = count($suppXMlObj);
                        foreach ($anOccupancy->SelctedSupplements as $singsuppup) {
                            foreach ($singsuppup as $anSupp) {
                                if (!empty($anSupp) && !in_array($anSupp->attributes()->suppId->__toString(), $unAray)) {
                                    $unAray[] = $anSupp->attributes()->suppId->__toString();
                                    $suppArr[] = array(
                                        'suppId' => $anSupp->attributes()->suppId->__toString(),
                                        'suppName' => (isset($anSupp->attributes()->suppName)) ? $anSupp->attributes()->suppName->__toString() : "",
                                        'supptType' => $anSupp->attributes()->supptType->__toString(),
                                        'suppIsMandatory' => $anSupp->attributes()->suppIsMandatory->__toString(),
                                        'suppChargeType' => $anSupp->attributes()->suppChargeType->__toString(),
                                        'price' => $anSupp->attributes()->price->__toString(),
                                        'publishPrice' => $anSupp->attributes()->publishPrice->__toString(),
                                    );
                                }
                            }
                        }
                    }
                }
                $roomTypeAttr = $aRoomType->attributes();
                $roomTypeArr[] = array(
                    'roomTypeCategory' => $roomTypeAttr->roomTypeCategory->__toString(),
                    'name' => $roomTypeAttr->name->__toString(),
                    'RoomTypeId' => $roomTypeAttr->hotelRoomTypeId->__toString(),
                    'HotelType'=>'TO',
                    'RoomId' => $roomTypeAttr->roomId->__toString(),
                    'isAvailable' => $roomTypeAttr->isAvailable->__toString(),
                    'occupancy_list' => $occupancyArr,
                    'occupancy_list_2' => $occupancyArr2,
                    'discount' => $discount,
                    'supp' => $suppArr
                );

            }
            $aHotelatrarr = $aHotelarr['@attributes'];
            $locations = array();
            if(isset($aHotelarr['Location']['@attributes']))
            {
                $locations = $aHotelarr['Location']['@attributes'];
            }
            $aminities = array();
            $thumbImage = $aHotelatrarr['thumb'];
            if (@getimagesize($thumbImage))
            {

            }
            else
            {
                $thumbImage = "http://www.touricoholidays.com/en/Modules/customizable/images/photo-not-available-small.gif";
            }

            //if (count($roomTypeArr[0]['occupancy_list']) == $roomNo) {
            $hotelDetailList[] = array(
                'hotelId' => (isset($aHotelatrarr['hotelId']) ? $aHotelatrarr['hotelId'] : ""),
                'hotelType' => 'TO',
                'voucher_remarks' => null,//$voucher_remarks,
                'hotelThumbUrl' => (isset($thumbImage) ? changePathToSSL($thumbImage) : ""),
                'hotelName' => (isset($aHotelatrarr['name']) ? $aHotelatrarr['name'] : ""),
                'distanceFromAirport' => (isset($distanceFromAirport) ? $distanceFromAirport : null),
                'distanceFromAirportUnit' => (isset($distanceFromAirportUnit) ? $distanceFromAirportUnit : null),
                'aminities' => (isset($aminities) ? $aminities : null),
                'longitude' => (isset($locations['longitude']) ? $locations['longitude'] : null),
                'latitude' => (isset($locations['latitude']) ? $locations['latitude'] : null),
                'star_level' => (isset($aHotelatrarr['starsLevel']) ? $aHotelatrarr['starsLevel'] : null),
                'min_price' => (isset($aHotelatrarr['minAverPrice']) ? $aHotelatrarr['minAverPrice'] : null),
                'facilities' => (isset($facilities) ? $facilities : null),
                'rooms' => (isset($rooms) ? $rooms : null),
                'sports_entertainment' => (isset($sports_entertainment) ? $sports_entertainment : null),
                'meals' => (isset($meals) ? $meals : null),
                'payment' => (isset($payment) ? $payment : null),
                'location' => (isset($locations) ? $locations : null),
                'hotel_brand_name' => (isset($aHotelatrarr['brandName']) ? $aHotelatrarr['brandName'] : null),
                'only_min_price1' => (isset($aHotelatrarr['minAverPrice']) ? $aHotelatrarr['minAverPrice'] : null),
                'only_min_price' => (isset($minimunPriceOfHotels) ? $minimunPriceOfHotels : null),
                'only_min_price_tax' => (isset($minimunPriceOfHotels_tax) ? $minimunPriceOfHotels_tax : null),
                'a_hotel_destination_id' => (isset($a_hotel_destination_id) ? $a_hotel_destination_id : null),
                'hotel_location_name' => (isset($locations['location']) ? $locations['location'] : null),
                'hotel_location_country' => (isset($locations['countryCode']) ? $locations['countryCode'] : null),
                'hotel_location_state' => (isset($locations['stateCode']) ? $locations['stateCode'] : null),
                'hotel_location_city' => (isset($locations['city']) ? $locations['city'] : null),
                'hotel_location_zip' => (isset($locations['zip']) ? $locations['zip'] : null),
                'room_type_list' => (isset($roomTypeArr) ? $roomTypeArr : null)
            );
            //}
            /*if($aHotelarr['@attributes']['name'] == 'Radisson Ontario Airport Hotel - Demo'){
                echo 1;echo '<pre>';print_r($hotelDetailList);exit;
            }*/
        }
        return $hotelDetailList;
    }


    function ProcessSearchResultForCache($roomInfoStr, $destinationCode, $hotelCityName, $checkInDate, $checkOutDate, $adultNumber = 1, $hotelLocationName = "", $hotelName = "", $childNumber, $childAges, $StarLevel = 0, $brandName = "")
    {
        ini_set('memory_limit', '256M');
        $hotel_location_name = "";
        $response = $this->hotelSearchAPICoreObj->SearchHotel($roomInfoStr, $destinationCode, $hotelCityName, $checkInDate, $checkOutDate, $adultNumber, $hotelLocationName, $hotelName, $childNumber, $childAges, $StarLevel);
        file_put_contents("hotel_search_results.xml", $response);
        $simpleXMLELementObj = simplexml_load_string($response);
        $response = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $response);
        $xml = new SimpleXMLElement($response);
        $body = $xml->xpath('//sBody')[0];
        $array = json_decode(json_encode((array)$body), TRUE);
        if(isset($array['sFault']))
        {
            return array();
        }
        else
        {
            $hotelList = $simpleXMLELementObj->children('http://schemas.xmlsoap.org/soap/envelope/')->Body->children('http://tourico.com/webservices/hotelv3')->SearchHotelsResponse->SearchHotelsResult->children()->HotelList->Hotel;
        }
        $totalHotel = count($hotelList);
        $matchedHotelIdArr = array();
        $hotelIdWithThumbArr = array();
        for ($hotelIndex = 0; $hotelIndex < $totalHotel; $hotelIndex++)
        {
            $aHotel = $hotelList[$hotelIndex];
            $aHotelAttributes = $aHotel->attributes();
            $matchedHotelIdArr[] = $aHotelAttributes->hotelId->__toString();
        }
        if (count($matchedHotelIdArr) <= 0)
        {
            return array();
        }
        $i = 0;
        $j = 0;
        foreach ($matchedHotelIdArr as $matchedHotelIdAry)
        {
            if ($i < 98)
            {
                $newMatchedHotelIdArr[$j][$i] = $matchedHotelIdAry;
            }
            else
            {
                $j++;
                $i = 0;
                $newMatchedHotelIdArr[$j][$i] = $matchedHotelIdAry;
            }
            $i++;
        }
        $hotelDetailList = array();
        foreach ($newMatchedHotelIdArr as $newMatchedHotelIdsingleArr)
        {
            $response = $this->hotelSearchAPICoreObj->GetHotelDetailByHotelId($newMatchedHotelIdsingleArr);
            $simpleXMLELementObj = simplexml_load_string($response);
            file_put_contents("GetHotelDetailByHotelId.xml", $response);
            $hotelList = $simpleXMLELementObj->children('http://schemas.xmlsoap.org/soap/envelope/')->Body->children('http://tourico.com/webservices/hotelv3')->GetHotelDetailsV3Response->GetHotelDetailsV3Result->children()->TWS_HotelDetailsV3->Hotel;
            $totalHotel = count($hotelList);
            for ($hotelIndex = 0; $hotelIndex < $totalHotel; $hotelIndex++)
            {
                $aHotel = $hotelList[$hotelIndex];
                $location = $aHotel->Location;

                $aHotelAttributes = $aHotel->attributes();
                $referencePointAttributes = $aHotel->RefPoints->attributes();

                $hotelId = $aHotelAttributes->hotelID->__toString();
                $distanceFromAirport = "";
                $distanceFromAirportUnit = "";
                if (isset($referencePointAttributes->distance))
                {
                    $distanceFromAirport = $referencePointAttributes->distance->__toString();
                    $distanceFromAirportUnit = $referencePointAttributes->unit->__toString();

                    if ($distanceFromAirportUnit == "Km")
                    {
                        $distanceFromAirport = 0.621371 * floatval($distanceFromAirport);
                        $distanceFromAirportUnit = "Mile";
                    }
                }

                $aminityList = $aHotel->Amenities->Amenity;
                $totalAminty = count($aminityList);
                $aminities = array();
                for ($aminityIndex = 0; $aminityIndex < $totalAminty; $aminityIndex++)
                {
                    $anAminity = $aminityList[$aminityIndex];
                    $anAminityAttributes = $anAminity->attributes();
                    $aminities[] = $anAminityAttributes->name->__toString();
                }
                $hotelDetailList[] = array(
                    'hotelId' => $hotelId,
                    'distanceFromAirport' => $distanceFromAirport,
                    'distanceFromAirportUnit' => $distanceFromAirportUnit,
                    'aminities' => implode(",",$aminities)
                );
            }
        }
        return $hotelDetailList;
    }


    function GethotelVocutreByHotelID($hotelIDs)
    {
        $response = $this->hotelSearchAPICoreObj->GetHotelDetailByHotelId($hotelIDs);
        $simpleXMLELementObj = simplexml_load_string($response);
        $hotelList = $simpleXMLELementObj->children('http://schemas.xmlsoap.org/soap/envelope/')->Body->children('http://tourico.com/webservices/hotelv3')->GetHotelDetailsV3Response->GetHotelDetailsV3Result->children()->TWS_HotelDetailsV3->Hotel;
        $totalHotel = count($hotelList);
        $voucher_remarks = array();
        for ($hotelIndex = 0; $hotelIndex < $totalHotel; $hotelIndex++)
        {
            $aHotel = $hotelList[$hotelIndex];
            $voucher_remarks = $aHotel->Descriptions->VoucherRemark;
            $voucher_remarks = xml2array ($voucher_remarks);
        }
        return $voucher_remarks;
    }

    function GetOneHotelDetailsByHotelIdForHotelDetail($hotelId) {
        $hotelIdArr = array($hotelId);
        $response = $this->hotelSearchAPICoreObj->GetHotelDetailByHotelId($hotelIdArr);

        $simpleXMLELementObj = simplexml_load_string($response);

        $hotelList = $simpleXMLELementObj->children('http://schemas.xmlsoap.org/soap/envelope/')->Body->children('http://tourico.com/webservices/hotelv3')->GetHotelDetailsV3Response->GetHotelDetailsV3Result->children()->TWS_HotelDetailsV3->Hotel;

        $hotel_phone = $hotelList->attributes()->hotelPhone->__toString();
        $starLevel = (float)$hotelList->attributes()->starLevel->__toString();
        $hotel_name = $hotelList->attributes()->name->__toString();
        $city = $hotelList->Location->attributes()->city->__toString();
        $address = $hotelList->Location->attributes()->address->__toString();
        $longitude = $hotelList->Location->attributes()->longitude->__toString();
        $latitude = $hotelList->Location->attributes()->latitude->__toString();

        $descriptionList = $hotelList->Descriptions->LongDescription->Description;
        $descriptionListCount = count($descriptionList);

        $facilities_desc = "";
        $room_desc = "";
        $sports_entertainment_desc = "";
        $meals_desc = "";
        $payment_desc = "";
        $location_desc = "";
        $room_type_facilities_desc = "";


        for ($j = 0; $j < $descriptionListCount; $j++) {
            $a_descList = $descriptionList[$j];
            $category = $a_descList->attributes()->category->__toString();
            if ($category == "Facilities") {
                $facilities_desc = $a_descList->attributes()->value->__toString();
            } else if ($category == "Rooms") {
                $room_desc = $a_descList->attributes()->value->__toString();
            } else if ($category == "Sports/Entertainment") {
                $sports_entertainment_desc = $a_descList->attributes()->value->__toString();
            } else if ($category == "meals") {
                $meals_desc = $a_descList->attributes()->value->__toString();
            } else if ($category == "payment") {
                $payment_desc = $a_descList->attributes()->value->__toString();
            } else if ($category == "location") {
                $location_desc = $a_descList->attributes()->value->__toString();
            } else if ($category == "room_type_facilities") {
                $room_type_facilities_desc = $a_descList->attributes()->value->__toString();
            }
        }

        $amenityList = $hotelList->Amenities->Amenity;
        $amenityListCount = count($amenityList);

        for ($j = 0; $j < $amenityListCount; $j++) {
            $an_amenity = $amenityList[$j];
            $amenities[] = $an_amenity->attributes()->name->__toString();
        }

        $image_list = $hotelList->Media->Images->Image;
        $image_list_count = count($image_list);

        for ($j = 0; $j < $image_list_count; $j++)
        {
            $an_image = $image_list[$j];
            $images[] = $an_image->attributes()->path->__toString();
        }

        $newImages = array();
        foreach($images as $key => $img)
        {
            if (@getimagesize($img))
            {
                $newImages[] = changePathToSSL($img);
            }
        }
        $images = $newImages;
        /*if(count($images) == 0)
        {
            $images = array('http://www.touricoholidays.com/en/Modules/customizable/images/photo-not-available-small.gif');
        }*/

        $totalHotel = count($hotelList);
        if ($totalHotel > 0) {
            $aHotel = $hotelList[0];

            $aHotelAttributes = $aHotel->attributes();
            $referencePointAttributes = $aHotel->RefPoints->attributes();

            $hotelId = $aHotelAttributes->hotelID->__toString();
            /* $hotelThumbUrl = "";
              foreach($hotelIdWithThumbArr as $aHotelIdWIthThumb)
              {
              if($aHotelIdWIthThumb['hotel_id']==$hotelId)
              {
              $hotelThumbUrl = $aHotelIdWIthThumb['thumb_url'];
              break;
              }
              } */

            $hotelName = $aHotelAttributes->name->__toString();

            $hotelLongitude = "";
            if (isset($aHotelAttributes->longitude))
                $hotelLongitude = $aHotelAttributes->longitude->__toString();
            $hotelLatitude = "";
            if (isset($aHotelAttributes->latitude))
                $hotelLatitude = $aHotelAttributes->latitude->__toString();

            $distanceFromAirport = "";
            $distanceFromAirportUnit = "";
            if (isset($referencePointAttributes->distance)) {
                $distanceFromAirport = $referencePointAttributes->distance->__toString();
                $distanceFromAirportUnit = $referencePointAttributes->unit->__toString();
            }

            $aminityList = $aHotel->Amenities->Amenity;
            $totalAminty = count($aminityList);
            $aminities = array();
            //foreach( $aminityList as $anAminity )
            for ($aminityIndex = 1; $aminityIndex < $totalAminty; $aminityIndex++) {
                $anAminity = $aminityList[$aminityIndex];
                $anAminityAttributes = $anAminity->attributes();
                $aminities[] = $anAminityAttributes->name->__toString();
            }
            $hotelDetail = array(
                'hotelId' => $hotelId,
                //'hotelThumbUrl' => $hotelThumbUrl,
                'hotelName' => $hotelName,
                'distanceFromAirport' => $distanceFromAirport,
                'distanceFromAirportUnit' => $distanceFromAirportUnit,
                'aminities' => $aminities,
                'longitude' => $hotelLongitude,
                'latitude' => $hotelLatitude,
                'facilities_desc' => $facilities_desc,
                'room_desc' => $room_desc,
                'sports_entertainment_desc' => $sports_entertainment_desc,
                'meals_desc' => $meals_desc,
                'payment_desc' => $payment_desc,
                'location_desc' => $location_desc,
                'room_type_facilities_desc' => $room_type_facilities_desc,
                'star_level' => $starLevel,
                'hotel_name' => $hotel_name,
                'city' => $city,
                'address' => $address,
                'longitude' => $longitude,
                'latitude' => $latitude,
                'images' => $images,
                'hotel_phone' => $hotel_phone
            );

            return $hotelDetail;
        }
        return false;
    }

    function GetOneHotelDetailsByHotelId($hotelId, $input_info , $roomTypeID)
    {
        $hotelIdArr = array($hotelId);
        $response = $this->hotelSearchAPICoreObj->SearchHotelsById($hotelIdArr, $input_info);
        $brandName = "";
        file_put_contents("one_hotel_detail_by_id.xml", $response);
        $simpleXMLELementObj = simplexml_load_string($response);

        $hotelList = $simpleXMLELementObj->children('http://schemas.xmlsoap.org/soap/envelope/')->Body->children('http://tourico.com/webservices/hotelv3')->SearchHotelsByIdResponse->SearchHotelsByIdResult->children()->HotelList->Hotel;

        $totalHotel = count($hotelList);
        $matchedHotelIdArr = array();
        $hotelIdWithThumbArr = array();


        for ($hotelIndex = 0; $hotelIndex < $totalHotel; $hotelIndex++)
        {
            $aHotel = $hotelList[$hotelIndex];
            $aHotelAttributes = $aHotel->attributes();

            $locationArr = $aHotel->Location;
            $hotel_address = $locationArr->attributes()->address->__toString();
            $roomTypeList = $aHotel->RoomTypes->RoomType;
            $roomTypeCount = count($roomTypeList);

            $roomTypeArr = array();
            for ($roomType_index = 0; $roomType_index < $roomTypeCount; $roomType_index++)
            {
                $aRoomType = $roomTypeList[$roomType_index];
                $hotelRoomTypeId = $aRoomType->attributes()->hotelRoomTypeId->__toString();
                $nights = $aRoomType->attributes()->nights->__toString();
                if($hotelRoomTypeId == $roomTypeID)
                {
                    $perDayAvailabilityXmlObjList = $aRoomType->AvailabilityBreakdown->Availability;
                    $totalAvailability = count($perDayAvailabilityXmlObjList);
                    $perDayAvailabilityList = array();
                    for ($availabilityIndex = 0; $availabilityIndex < $totalAvailability; $availabilityIndex++)
                    {
                        $anAvailabiity = $perDayAvailabilityXmlObjList[$availabilityIndex];
                        $perDayAvailabilityList[] = $anAvailabiity->attributes()->status->__toString();
                    }

                    $xmlOccupancyList = $aRoomType->Occupancies->Occupancy;

                    $totalOccupancy = count($xmlOccupancyList);
                    $occupancyArr = array();
                    $totsupplements = array();
                    for ($jindex = 0; $jindex < $totalOccupancy; $jindex++)
                    {
                        $anOccupancy = $xmlOccupancyList[$jindex];
                        $boardbases = array();
                        foreach($anOccupancy->BoardBases->Boardbase as $boardbase)
                        {
                            $boardbases[] = array(
                                "bbId" => $boardbase->attributes()->bbId->__toString(),
                                "bbName" => $boardbase->attributes()->bbName->__toString(),
                                "bbPrice" => $boardbase->attributes()->bbPrice->__toString(),
                                "bbPublishPrice" => $boardbase->attributes()->bbPublishPrice->__toString(),
                            );
                        }
                        $occupancyAttr = $anOccupancy->attributes();
                        $troomtax = $occupancyAttr->taxPublish->__toString();
                        $roomtax = $occupancyAttr->taxPublish->__toString() / $nights;


                        $roomXMlObj = $anOccupancy->Rooms->Room;
                        $roomXMlObjnew = $anOccupancy->Rooms->Room;
                        $roomarray = array();
                        for($ij = 0 ; $ij < count($roomXMlObjnew) ;$ij++)
                        {
                            $roomXMlObjvar = $roomXMlObjnew[$ij];
                            $numOfAdults = $roomXMlObjvar->AdultNum->__toString();
                            $numOfChild = $roomXMlObjvar->ChildNum->__toString();
                            $roomSeqNo = $roomXMlObjvar->attributes()->seqNum->__toString();
                            $childages = array();
                            if($numOfChild > 0)
                            {
                                if(isset($roomXMlObjvar->ChildAges))
                                {
                                    $childAges = $roomXMlObjvar->ChildAges->ChildAge;
                                    foreach($childAges as $ca)
                                    {
                                        $childages[] = $ca->attributes()->age->__toString();
                                    }
                                }
                            }
                            $roomarray[] = array(
                                'roomSeqNo' => $roomSeqNo,
                                'numOfAdults' => $numOfAdults,
                                'numOfChild' => $numOfChild,
                                'childages' => $childages,
                            );
                        }
                        $roomSeqNo = $roomXMlObj->attributes()->seqNum->__toString();
                        $roomeName = 'Room ' . $roomSeqNo;

                        $price_break_down_list = $anOccupancy->PriceBreakdown->Price;
                        $price_break_down_list_count = count($price_break_down_list);
                        $per_day_price = [];
                        for ($per_day_price_index = 0; $per_day_price_index < $price_break_down_list_count; $per_day_price_index++)
                        {
                            $per_day_price[] = $price_break_down_list[$per_day_price_index]->attributes()->value->__toString() - $roomtax;
                        }

                        $supplemtnts = $anOccupancy->SelctedSupplements->Supplement;
                        $allSupplemtnts = array();
                        $used_ID = array();
                        for($supi = 0 ; $supi < count($supplemtnts) ; $supi++)
                        {
                            if(!in_array($supplemtnts[$supi]->attributes()->suppId->__toString() , $used_ID))
                            {
                                $used_ID[] = $supplemtnts[$supi]->attributes()->suppId->__toString();
                                $SuppAgeGroup = array();
                                if(isset($supplemtnts[$supi]->SuppAgeGroup))
                                {
                                    $SuppAgeGroup = $supplemtnts[$supi]->SuppAgeGroup->SupplementAge;
                                    $supraaaa = array();
                                    if(count($SuppAgeGroup) > 0)
                                    {
                                        foreach($SuppAgeGroup as $singr)
                                        {
                                            $supraaaa[] = xml2array($singr);
                                        }
                                    }
                                    $SuppAgeGroup = $supraaaa;
                                }
                                $allSupplemtnts[] = array(
                                    'suppId' => $supplemtnts[$supi]->attributes()->suppId->__toString(),
                                    'suppName' => $supplemtnts[$supi]->attributes()->suppName->__toString(),
                                    'supptType' => $supplemtnts[$supi]->attributes()->supptType->__toString(),
                                    'suppIsMandatory' => $supplemtnts[$supi]->attributes()->suppIsMandatory->__toString(),
                                    'suppChargeType' => $supplemtnts[$supi]->attributes()->suppChargeType->__toString(),
                                    'price' => $supplemtnts[$supi]->attributes()->price->__toString(),
                                    'publishPrice' => $supplemtnts[$supi]->attributes()->publishPrice->__toString(),
                                    'SuppAgeGroup' => $SuppAgeGroup
                                );
                            }
                        }
                        $totsupplements[] = $allSupplemtnts;
                        if (!isset($occupancyArr[$roomeName]))
                        {
                            $occupancyArr[$roomeName] = array('room_sequnce' => $roomSeqNo, 'available_list' => array());
                        }
                        $occupancyArr[$roomeName]['available_list'][] = array(
                            'per_night_price' => $occupancyAttr->occupPrice->__toString(),
                            'per_night_tax' => $roomtax,//$occupancyAttr->taxPublish->__toString(),
                            'max_guest' => $occupancyAttr->maxGuests->__toString(),
                            'max_child' => $occupancyAttr->maxChild->__toString(),
                            'total_price' => $occupancyAttr->occupPrice->__toString(),
                            'bedding' => $occupancyAttr->bedding->__toString(),
                            'occpNo' => $jindex,
                            'per_day_price' => $per_day_price,
                            'per_day_availability' => $perDayAvailabilityList,
                            'roomDet' => $roomarray,
                            'boardbases' => $boardbases
                        );
                        $occupancyArr2[]['available_list'][] = array(
                            'per_night_price' => $occupancyAttr->occupPrice->__toString(),
                            'per_night_tax' => $roomtax,//$occupancyAttr->taxPublish->__toString(),
                            'max_guest' => $occupancyAttr->maxGuests->__toString(),
                            'max_child' => $occupancyAttr->maxChild->__toString(),
                            'total_price' => $occupancyAttr->occupPrice->__toString(),
                            'bedding' => $occupancyAttr->bedding->__toString(),
                            'occpNo' => $jindex,
                            'per_day_price' => $per_day_price,
                            'per_day_availability' => $perDayAvailabilityList,
                            'room_name' => $roomeName,
                            'roomDet' => $roomarray,
                            'boardbases' => $boardbases
                        );
                    }
                    $_SESSION['Supplemtnts'] = $totsupplements;
                    $roomTypeAttr = $aRoomType->attributes();
                    $roomTypeArr[] = array(
                        'roomTypeCategory' => $roomTypeAttr->roomTypeCategory->__toString(),
                        'name' => $roomTypeAttr->name->__toString(),
                        'RoomTypeId' => $roomTypeAttr->hotelRoomTypeId->__toString(),
                        'RoomId' => $roomTypeAttr->roomId->__toString(),
                        'isAvailable' => $roomTypeAttr->isAvailable->__toString(),
                        'occupancy_list' => $occupancyArr,
                        'Supplemtnts' => $totsupplements,
                        'occupancy_list_2' => $occupancyArr2
                    );
                }
            }

            $grabData = false;
            if (strlen($brandName) > 0) {
                if ($brandName == $aHotelAttributes->brandName->__toString()) {
                    $grabData = true;
                }
            }
            else
            {
                $grabData = true;
            }

            if ($grabData)
            {
                $aHotelLocationAttributes = $aHotel->Location->attributes();
                $hotelLongitude = "";
                if (isset($aHotelLocationAttributes->longitude))
                    $hotelLongitude = $aHotelLocationAttributes->longitude->__toString();
                $hotelLatitude = "";
                if (isset($aHotelLocationAttributes->latitude))
                    $hotelLatitude = $aHotelLocationAttributes->latitude->__toString();
                if (isset($aHotelLocationAttributes->location))
                    $hotel_location_name = $aHotelLocationAttributes->location->__toString();

                $matchedHotelIdArr[] = $aHotelAttributes->hotelId->__toString();
                $hotelIdWithThumbArr[] = array(
                    'hotel_id' => $aHotelAttributes->hotelId->__toString(),
                    'thumb_url' => changePathToSSL($aHotelAttributes->thumb->__toString()),
                    'star_level' => $aHotelAttributes->starsLevel->__toString(),
                    'min_price' => $aHotelAttributes->minAverPrice->__toString() . " " . $aHotelAttributes->currency->__toString(),
                    'longitude' => $hotelLongitude,
                    'latitude' => $hotelLatitude,
                    'brand_name' => $aHotelAttributes->brandName->__toString(),
                    'only_min_price' => $aHotelAttributes->minAverPrice->__toString(),
                    'hotel_location_name' => $hotel_location_name,
                    'room_type_list' => $roomTypeArr
                );
            }
        }



        if (count($matchedHotelIdArr) <= 0)
            return array();
        $response = $this->hotelSearchAPICoreObj->GetHotelDetailByHotelId($matchedHotelIdArr);
        $simpleXMLELementObj = simplexml_load_string($response);


        $hotelList = $simpleXMLELementObj->children('http://schemas.xmlsoap.org/soap/envelope/')->Body->children('http://tourico.com/webservices/hotelv3')->GetHotelDetailsV3Response->GetHotelDetailsV3Result->children()->TWS_HotelDetailsV3->Hotel;

        $totalHotel = count($hotelList);
        $hotelDetailList = array();

        //foreach($hotelList as $aHotel)
        for ($hotelIndex = 0; $hotelIndex < $totalHotel; $hotelIndex++) {
            $aHotel = $hotelList[$hotelIndex];

            $image_list = $aHotel->Media->Images->Image;
            $image_list_count = count($image_list);

            for ($index_image_list = 0; $index_image_list < $image_list_count; $index_image_list++) {
                $image_url[] = $image_list[$index_image_list]->attributes()->path->__toString();
            }

            $description_lists = $aHotel->Descriptions->LongDescription->Description;

            $roomTypeObjList = $aHotel->RoomType;
            $totalRoomType = count($roomTypeObjList);

            $roomTypeIdListWithFaciity = array();

            for ($roomTypeIndex = 0; $roomTypeIndex < $totalRoomType; $roomTypeIndex++) {
                $aRoomType = $roomTypeObjList[$roomTypeIndex];

                $facilityObjectList = $aRoomType->Facilities->Facility;
                $facilityArr = array();
                $totalFaciltiy = count($facilityObjectList);
                for ($facilityIndex = 0; $facilityIndex < $totalFaciltiy; $facilityIndex++) {
                    $aFacility = $facilityObjectList[$facilityIndex];
                    $aFacilityAttr = $aFacility->attributes();
                    $facilityArr[] = array(
                        'facility_id' => $aFacilityAttr->facilityId->__toString(),
                        'name' => $aFacilityAttr->name->__toString()
                    );
                }

                $roomTypeIdXmlObjectList = $aRoomType->HotelRoomTypeIds->HotelRoomTypeId;
                $totalRoomTypeIdList = count($roomTypeIdXmlObjectList);
                for ($roomTypeIdIndex = 0; $roomTypeIdIndex < $totalRoomTypeIdList; $roomTypeIdIndex++) {
                    $anRoomTypeId = $roomTypeIdXmlObjectList[$roomTypeIdIndex];
                    $roomTypeId = $anRoomTypeId->attributes()->ID->__toString();
                    $roomTypeIdListWithFaciity[] = array(
                        'RoomTypeId' => $roomTypeId,
                        'facility_list' => $facilityArr
                    );
                }
            }

            $description_lists_count = count($description_lists);

            for ($description_lists_index = 0; $description_lists_index < $description_lists_count; $description_lists_index++) {
                $category = $description_lists[$description_lists_index]->attributes()->category->__toString();
                $value = $description_lists[$description_lists_index]->attributes()->value->__toString();

                if ($category == "Facilities") {
                    $facilities = $value;
                } else if ($category == "Rooms") {
                    $rooms = $value;
                } else if ($category == "Sports/Entertainment") {
                    $sports_entertainment = $value;
                } else if ($category == "Meals") {
                    $meals = $value;
                } else if ($category == "Payment") {
                    $payment = $value;
                } else if ($category == "Location") {
                    $location = $value;
                }
            }

            $aHotelAttributes = $aHotel->attributes();
            $referencePointAttributes = $aHotel->RefPoints->attributes();

            $hotelId = $aHotelAttributes->hotelID->__toString();

            $a_hotel_destination_id = $aHotel->Location->attributes()->destinationId->__toString();

            $hotelThumbUrl = "";
            $starLevel = "";
            $hotelLongitude = "";
            $minPrice = "";
            foreach ($hotelIdWithThumbArr as $aHotelIdWIthThumb)
            {
                if ($aHotelIdWIthThumb['hotel_id'] == $hotelId)
                {
                    $hotelThumbUrl = $aHotelIdWIthThumb['thumb_url'];
                    $starLevel = $aHotelIdWIthThumb['star_level'];
                    $hotelLongitude = $aHotelIdWIthThumb['longitude'];
                    $hotelLatitude = $aHotelIdWIthThumb['latitude'];
                    $minPrice = $aHotelIdWIthThumb['min_price'];
                    $hotelBrandName = $aHotelIdWIthThumb['brand_name'];
                    $only_min_price = $aHotelIdWIthThumb['only_min_price'];
                    $hotel_location_name = $aHotelIdWIthThumb['hotel_location_name'];
                    $roomTypeList = $aHotelIdWIthThumb['room_type_list'];

                    $roomTypeListWithFacility = array();
                    foreach ($roomTypeList as $aRoomType)
                    {
                        foreach ($roomTypeIdListWithFaciity as $aRoomTypeIdWithFaciity)
                        {
                            if ($aRoomType['RoomTypeId'] == $aRoomTypeIdWithFaciity['RoomTypeId'])
                            {
                                $aRoomType['facilities'] = $aRoomTypeIdWithFaciity['facility_list'];
                                break;
                            }
                        }
                        $roomTypeListWithFacility[] = $aRoomType;
                    }
                    break;
                }
            }
            $hotelName = $aHotelAttributes->name->__toString();
            $distanceFromAirport = "";
            $distanceFromAirportUnit = "";
            if (isset($referencePointAttributes->distance)) {
                $distanceFromAirport = $referencePointAttributes->distance->__toString();
                $distanceFromAirportUnit = $referencePointAttributes->unit->__toString();

                if ($distanceFromAirportUnit == "Km") {
                    $distanceFromAirport = strval(0.621371 * floatval($distanceFromAirport));
                    $distanceFromAirportUnit = "Mile";
                }
            }

            $aminityList = $aHotel->Amenities->Amenity;
            $totalAminty = count($aminityList);
            $aminities = array();
            //foreach( $aminityList as $anAminity )
            for ($aminityIndex = 0; $aminityIndex < $totalAminty; $aminityIndex++) {
                $anAminity = $aminityList[$aminityIndex];
                $anAminityAttributes = $anAminity->attributes();
                $aminities[] = $anAminityAttributes->name->__toString();
            }

            $hotelDetailList[] = array(
                'hotelId' => $hotelId,
                'hotelThumbUrl' => $hotelThumbUrl,
                'hotel_address' => $hotel_address,
                'hotelName' => $hotelName,
                'distanceFromAirport' => $distanceFromAirport,
                'distanceFromAirportUnit' => $distanceFromAirportUnit,
                'aminities' => $aminities,
                'longitude' => $hotelLongitude,
                'latitude' => $hotelLatitude,
                'star_level' => $starLevel,
                'min_price' => $minPrice,
                'image_url' => $image_url,
                'facilities' => ( isset($facilities) ? $facilities : null ),
                'rooms' => ( isset($rooms) ? $rooms : null ),
                'sports_entertainment' => ( isset($sports_entertainment) ? $sports_entertainment : null ),
                'meals' => ( isset($meals) ? $meals : null ),
                'payment' => ( isset($payment) ? $payment : null ),
                'location' => ( isset($location) ? $location : null ),
                'hotel_brand_name' => $hotelBrandName,
                'only_min_price' => $only_min_price,
                'a_hotel_destination_id' => $a_hotel_destination_id,
                'hotel_location_name' => $hotel_location_name,
                'room_type_list' => $roomTypeListWithFacility,
                'voucher_remarks' => $this->GethotelVocutreByHotelID(array($hotelId))
            );
        }
        file_put_contents("search_hotel_by_id_results.txt", json_encode(array("hotel_list" => $hotelDetailList)));

        return $hotelDetailList;
    }
}

Class HotelSearchAPICore {

    private $DESTINATION_API_USERNAME = 'HitelsDif15';
    private $DESTINATION_API_PASSWORD = 'HD@th2015';
    private $DESTINATION_API_ENDPOINT = 'http://destservices.touricoholidays.com/DestinationsService.svc?wsdl';

    private $DESTINATION_API_ACTION = 'http://touricoholidays.com/WSDestinations/2008/08/Contracts/IDestinationContracts/GetDestination';
    private $HOTEL_SEARCH_API_USERNAME = "HDL103";
    private $HOTEL_SEARCH_API_PASSWORD = "111111";

    private $HOTEL_SEARCH_API_ENDPOINT = "http://demo-hotelws.touricoholidays.com/HotelFlow.svc/bas";
    private $HOTEL_SEARCH_BY_DESTINATION_API_ACTION = "http://tourico.com/webservices/hotelv3/IHotelFlow/SearchHotelsByDestinationIds";
    private $HOTEL_DETAILSBYID_API_ACTION = 'http://tourico.com/webservices/hotelv3/IHotelFlow/GetHotelDetailsV3';
    private $HOTEL_SEARCH_API_ACTION = "http://tourico.com/webservices/hotelv3/IHotelFlow/SearchHotels";
    private $SEARCH_HOTEL_BY_ID_API_ACTION = "http://tourico.com/webservices/hotelv3/IHotelFlow/SearchHotelsById";

    function SearchHotel($roomInfoStr, $destinationCode, $hotelCityName, $checkInDate, $checkOutDate, $adultNumber = 1, $hotelLocationName = "", $hotelName = "", $childNumber, $childAges, $StarLevel = '0') {
        // $StarLevel = 0;
        $childAgeStr = "";
        for ($i = 0; $i < $childNumber; $i++) {
            $childAge = $childAges[$i];
            $childAgeStr .= <<<EOM
				<hot1:ChildAge age="{$childAge}"/>
EOM;
        }

        $request = <<<EOM
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:aut="http://schemas.tourico.com/webservices/authentication" xmlns:hot="http://tourico.com/webservices/hotelv3" xmlns:hot1="http://schemas.tourico.com/webservices/hotelv3">
   <soapenv:Header>
      <aut:AuthenticationHeader>
         <aut:LoginName>{$this->HOTEL_SEARCH_API_USERNAME}</aut:LoginName>
         <aut:Password>{$this->HOTEL_SEARCH_API_PASSWORD}</aut:Password>
         <aut:Culture>en_US</aut:Culture>
         <aut:Version>7.123</aut:Version>
      </aut:AuthenticationHeader>
   </soapenv:Header>
   <soapenv:Body>
      <hot:SearchHotels>
         <hot:request>
            <hot1:Destination>{$destinationCode}</hot1:Destination>
            <hot1:HotelCityName>{$hotelCityName}</hot1:HotelCityName>
            <hot1:HotelLocationName>{$hotelLocationName}</hot1:HotelLocationName>
            <hot1:HotelName>{$hotelName}</hot1:HotelName>
            <hot1:CheckIn>{$checkInDate}</hot1:CheckIn>
            <hot1:CheckOut>{$checkOutDate}</hot1:CheckOut>
            <hot1:RoomsInformation>
               {$roomInfoStr}
            </hot1:RoomsInformation>
            <hot1:MaxPrice>0</hot1:MaxPrice>
            <hot1:StarLevel>{$StarLevel}</hot1:StarLevel>
            <hot1:AvailableOnly>false</hot1:AvailableOnly>
            <hot1:PropertyType>NotSet</hot1:PropertyType>
            <hot1:ExactDestination>true</hot1:ExactDestination>
         </hot:request>
      </hot:SearchHotels>
   </soapenv:Body>
</soapenv:Envelope>
EOM;

        file_put_contents("search_hotel_request.xml", $request);
        //echo '<pre>';print_r($request);exit;
        $response = $this->SendPostRequestToTourico($this->HOTEL_SEARCH_API_ENDPOINT, $request, $this->HOTEL_SEARCH_API_ACTION);
        return $response;
    }

    function GetHotelListByDestinationId() {
        $request = <<<EOM
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:aut="http://schemas.tourico.com/webservices/authentication" xmlns:hot="http://tourico.com/webservices/hotelv3" xmlns:hot1="http://schemas.tourico.com/webservices/hotelv3">
   <soapenv:Header>
      <aut:AuthenticationHeader>
         <aut:LoginName>{$this->HOTEL_SEARCH_API_USERNAME}</aut:LoginName>
         <aut:Password>{$this->HOTEL_SEARCH_API_PASSWORD}</aut:Password>
         <aut:Culture>en_US</aut:Culture>
         <aut:Version>7.123</aut:Version>
      </aut:AuthenticationHeader>
   </soapenv:Header>
   <soapenv:Body>
      <hot:SearchHotelsByDestinationIds>
         <hot:request>
            <hot1:DestinationIdsInfo>
               <hot1:DestinationIdInfo id="2009"/>
               <hot1:DestinationIdInfo id="26874"/>
               <hot1:DestinationIdInfo id="8176"/>
            </hot1:DestinationIdsInfo>
            <hot1:CheckIn>2015-11-15</hot1:CheckIn>
            <hot1:CheckOut>2015-11-20</hot1:CheckOut>
            <hot1:RoomsInformation>
               <hot1:RoomInfo>
                  <hot1:AdultNum>2</hot1:AdultNum>
                  <hot1:ChildNum>0</hot1:ChildNum>
                  <!--<hot1:ChildAges>
                     <hot1:ChildAge age="0"/>
                  </hot1:ChildAges>-->
               </hot1:RoomInfo>
            </hot1:RoomsInformation>
            <hot1:MaxPrice>0</hot1:MaxPrice>
            <hot1:StarLevel>0</hot1:StarLevel>
            <hot1:AvailableOnly>true</hot1:AvailableOnly>
            <hot1:PropertyType>NotSet</hot1:PropertyType>
            <hot1:ExactDestination>true</hot1:ExactDestination>
         </hot:request>
      </hot:SearchHotelsByDestinationIds>
   </soapenv:Body>
</soapenv:Envelope>
EOM;

        $response = $this->SendPostRequestToTourico($this->HOTEL_SEARCH_API_ENDPOINT, $request, $this->HOTEL_SEARCH_BY_DESTINATION_API_ACTION);

        return $response;
    }

    function GetHotelDetailByHotelId($hotelIdArr) {
        $hotelListXml = "";
        foreach ($hotelIdArr as $aHotelId)
            $hotelListXml .= '<hot:HotelID id="' . $aHotelId . '"/>';

        $request = <<<EOM
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:aut="http://schemas.tourico.com/webservices/authentication" xmlns:hot="http://tourico.com/webservices/hotelv3">
	<soapenv:Header>
		<aut:AuthenticationHeader>
			<aut:LoginName>{$this->HOTEL_SEARCH_API_USERNAME}</aut:LoginName>
			<aut:Password>{$this->HOTEL_SEARCH_API_PASSWORD}</aut:Password>
			<aut:Culture>en_US</aut:Culture>
			<aut:Version>7.123</aut:Version>
		</aut:AuthenticationHeader>
	</soapenv:Header>
	<soapenv:Body>
		<hot:GetHotelDetailsV3>
		<hot:HotelIds>
            {$hotelListXml}
		</hot:HotelIds>
	</hot:GetHotelDetailsV3>
</soapenv:Body>
</soapenv:Envelope>
EOM;
        $response = $this->SendPostRequestToTourico($this->HOTEL_SEARCH_API_ENDPOINT, $request, $this->HOTEL_DETAILSBYID_API_ACTION);

        return $response;
    }

    function GetDestinationList() {
        $request = <<<EOM
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:dat="http://touricoholidays.com/WSDestinations/2008/08/DataContracts">
   <soapenv:Header>
      <dat:LoginHeader>
         <dat:username>{$this->DESTINATION_API_USERNAME}</dat:username>
         <dat:password>{$this->DESTINATION_API_PASSWORD}</dat:password>
         <dat:culture>en_US</dat:culture>
         <dat:version>7.123</dat:version>
      </dat:LoginHeader>
   </soapenv:Header>
   <soapenv:Body>
      <dat:GetDestination>
         <dat:Destination>
            <!--<dat:Continent>North America</dat:Continent>
            <dat:Country>USA</dat:Country>
            <dat:StatusDate>2010-07-25T00:00:00</dat:StatusDate>-->
         </dat:Destination>
      </dat:GetDestination>
   </soapenv:Body>
</soapenv:Envelope>
EOM;
        $response = $this->SendPostRequestToTourico($this->DESTINATION_API_ENDPOINT, $request, $this->DESTINATION_API_ACTION);

        return $response;
    }

    function SearchHotelsById($hotelIdArr, $input_xml)
    {
        $check_in_date = $input_xml['check_in_date'];
        $check_out_date = $input_xml['check_out_date'];

        $fromDateObj = DateTime::createFromFormat('m/d/Y', $check_in_date);

        if ($fromDateObj != false) {
            $check_in_date = $fromDateObj->format('Y-m-d');
        }

        $fromDateObj = DateTime::createFromFormat('m/d/Y', $check_out_date);

        if ($fromDateObj != false) {
            $check_out_date = $fromDateObj->format('Y-m-d');
        }

        $room_info_xml = $this->GetRoomInfoXmlForHotelSearch($input_xml['number_of_rooms'], $input_xml['child_number'], $input_xml['child_age'], $input_xml['adult_number']);

        $request = <<<EOM
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:aut="http://schemas.tourico.com/webservices/authentication" xmlns:hot="http://tourico.com/webservices/hotelv3" xmlns:hot1="http://schemas.tourico.com/webservices/hotelv3">
   <soapenv:Header>
      <aut:AuthenticationHeader>
         <aut:LoginName>{$this->HOTEL_SEARCH_API_USERNAME}</aut:LoginName>
         <aut:Password>{$this->HOTEL_SEARCH_API_PASSWORD}</aut:Password>
         <aut:Culture>en_US</aut:Culture>
         <aut:Version>7.123</aut:Version>
      </aut:AuthenticationHeader>
   </soapenv:Header>
   <soapenv:Body>
      <hot:SearchHotelsById>
         <hot:request>
            <hot1:HotelIdsInfo>
               <hot1:HotelIdInfo id="{$hotelIdArr[0]}"/>
            </hot1:HotelIdsInfo>
            <hot1:CheckIn>{$check_in_date}</hot1:CheckIn>
            <hot1:CheckOut>{$check_out_date}</hot1:CheckOut>
            <hot1:RoomsInformation>
               $room_info_xml
            </hot1:RoomsInformation>
            <hot1:MaxPrice>0</hot1:MaxPrice>
            <hot1:StarLevel>0</hot1:StarLevel>
            <hot1:AvailableOnly>false</hot1:AvailableOnly>
         </hot:request>
      </hot:SearchHotelsById>
   </soapenv:Body>
</soapenv:Envelope>
EOM;

        file_put_contents("search_hotel_by_id.xml", $request);
        $response = $this->SendPostRequestToTourico($this->HOTEL_SEARCH_API_ENDPOINT, $request, 'http://tourico.com/webservices/hotelv3/IHotelFlow/SearchHotelsById');
        return $response;
    }


    function SearchHotelsByIdTest()
    {

        $request = <<<EOM
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:aut="http://schemas.tourico.com/webservices/authentication" xmlns:hot="http://tourico.com/webservices/hotelv3" xmlns:hot1="http://schemas.tourico.com/webservices/hotelv3">
   <soapenv:Header>
      <aut:AuthenticationHeader>
         <aut:LoginName>{$this->HOTEL_SEARCH_API_USERNAME}</aut:LoginName>
         <aut:Password>{$this->HOTEL_SEARCH_API_PASSWORD}</aut:Password>
         <aut:Culture>en_US</aut:Culture>
         <aut:Version>7.123</aut:Version>
      </aut:AuthenticationHeader>
   </soapenv:Header>
   <soapenv:Body>
      <hot:SearchHotelsById>
         <hot:request>
            <hot1:HotelIdsInfo>
               <hot1:HotelIdInfo id="1296321"/>
            </hot1:HotelIdsInfo>
            <hot1:CheckIn>2016-06-17</hot1:CheckIn>
            <hot1:CheckOut>2016-06-18</hot1:CheckOut>
            <hot1:RoomsInformation>
               <hot1:RoomInfo>
                  <hot1:AdultNum>2</hot1:AdultNum>
                  <hot1:ChildNum>1</hot1:ChildNum>
                  <hot1:ChildAges>
                     <hot1:ChildAge age="10"/>
                  </hot1:ChildAges>
               </hot1:RoomInfo>
            </hot1:RoomsInformation>
            <hot1:MaxPrice>0</hot1:MaxPrice>
            <hot1:StarLevel>0</hot1:StarLevel>
            <hot1:AvailableOnly>true</hot1:AvailableOnly>
         </hot:request>
      </hot:SearchHotelsById>
   </soapenv:Body>
</soapenv:Envelope>
EOM;

        file_put_contents("search_hotel_by_id.xml", $request);
        $response = $this->SendPostRequestToTourico($this->HOTEL_SEARCH_API_ENDPOINT, $request, 'http://tourico.com/webservices/hotelv3/IHotelFlow/SearchHotelsById');
        return $response;
    }

    function SendPostRequestToTourico($url, $request, $actionUri) {
        $ch = curl_init($url);
        $options = array(
            CURLOPT_RETURNTRANSFER => true, // return web page
            CURLOPT_HEADER => false, // don't return headers
            CURLOPT_FOLLOWLOCATION => false, // follow redirects
            CURLOPT_ENCODING => "utf-8", // handle all encodings
            CURLOPT_AUTOREFERER => true, // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 300, // timeout on connect
            CURLOPT_TIMEOUT => 300, // timeout on response
            CURLOPT_POST => 1, // i am sending post data
            CURLOPT_POSTFIELDS => $request, // this are my post vars
            CURLOPT_SSL_VERIFYHOST => 0, // don't verify ssl
            CURLOPT_SSL_VERIFYPEER => false, //
            CURLOPT_VERBOSE => 1,
            CURLOPT_HTTPHEADER => array(
                "Content-type: text/xml;charset=\"utf-8\"",
                "Accept: text/xml",
                "Cache-Control: no-cache",
                'Accept-Encoding: gzip',
                //"Pragma: no-cache",
                "SOAPAction: " . $actionUri,

                //"Content-length: ".strlen($request)
            )
        );
        curl_setopt_array($ch, $options);
        $data = curl_exec($ch);
        $curl_errno = curl_errno($ch);
        $curl_error = curl_error($ch);
        //echo $curl_errno;
        //echo $curl_error;
        curl_close($ch);
        return $data;
    }

    function GetRoomInfoXmlForHotelSearch($roomNo, $numberOfChild, $childAge, $numberOfAdults) {
        $room_info_str = "";
        $childAgeStr = "";
        $roomInfoStr = "";
        $roomNo_count = intval($roomNo);
        $child_age_index = 0;
        //var_dump($roomNo_count);
        for ($i = 0; $i < $roomNo_count; $i++) {
            for ($j = $child_age_index; $j < (intval($numberOfChild[$i]) + $child_age_index); $j++) {

                $childAgeStr .= <<<EOM
                                        <hot1:ChildAge age="{$childAge[$j]}"/>
EOM;
            }

            $child_age_index = $j;

            $roomInfoStr .= <<<EOM
<hot1:RoomInfo>
                  <hot1:AdultNum>{$numberOfAdults[$i]}</hot1:AdultNum>
                  <hot1:ChildNum>{$numberOfChild[$i]}</hot1:ChildNum>
                  <hot1:ChildAges>
				    {$childAgeStr}
                  </hot1:ChildAges>
               </hot1:RoomInfo>
EOM;

            $childAgeStr = "";
        }

        return $roomInfoStr;
    }

}

Class CarSearchAPIResponseProcess {

    function ParseRulesAndRegulationsXmlToArray($car_company_id) {
        $carSearchApiCoreObj = new CarSearchAPICore();
        //$xml_string = file_get_contents('mine_rules_and_regulations.xml');
        $xml_string = $carSearchApiCoreObj->GetRulesAndRestrictions($car_company_id);
        $xmlObj = simplexml_load_string($xml_string);

        $rules = $xmlObj->children('http://schemas.xmlsoap.org/soap/envelope/')->Body->children('http://tourico.com/webservices/')->GetRulesAndRestrictionsResponse->Rules;

        $rules_arr[] = array(
            'carCompanyLogoUrl' => $rules->attributes()->carCompanyLogoUrl->__toString(),
            'carCompanyName' => $rules->attributes()->carCompanyName->__toString()
        );

        $rules_list = $xmlObj->children('http://schemas.xmlsoap.org/soap/envelope/')->Body->children('http://tourico.com/webservices/')->GetRulesAndRestrictionsResponse->Rules->Rule;

        $rules_list_count = $rules_list->count();

        for ($i = 0; $i < $rules_list_count; $i++) {
            $a_rule = $rules_list[$i];

            $rules_arr[] = array(
                'title' => $a_rule->attributes()->title->__toString(),
                'description' => $a_rule->__toString()
            );
        }

        return $rules_arr;
    }

    function ParseSelectStationsXmlToArray($product_id, $pickup_station_id, $drop_off_station_id) {
        $select_stations_obj = new SelectStationAPICore();

        $xml_string = $select_stations_obj->SelectStation($product_id, $pickup_station_id, $drop_off_station_id);

        $xmlObj = simplexml_load_string($xml_string);

        $pick_up = $a_car = $xmlObj->children('http://schemas.xmlsoap.org/soap/envelope/')->Body->children('http://tourico.com/webservices/')->SelectStationsResponse->SearchParameters->PickUp;

        $drop_off = $a_car = $xmlObj->children('http://schemas.xmlsoap.org/soap/envelope/')->Body->children('http://tourico.com/webservices/')->SelectStationsResponse->SearchParameters->DropOff;

        $a_car = $xmlObj->children('http://schemas.xmlsoap.org/soap/envelope/')->Body->children('http://tourico.com/webservices/')->SelectStationsResponse->Car;

        $a_car_station = $xmlObj->children('http://schemas.xmlsoap.org/soap/envelope/')->Body->children('http://tourico.com/webservices/')->SelectStationsResponse->Car->RouteStations->Stations->CarStation;

        $select_station_info[] = array(
            'carThumb' => $a_car->attributes()->carThumb->__toString(),
            'carCompanyName' => $a_car->attributes()->carCompanyName->__toString(),
            'carName' => $a_car->attributes()->carName->__toString(),
            'transmission' => $a_car->attributes()->transmission->__toString(),
            'ac' => $a_car->attributes()->ac->__toString(),
            'type' => $a_car->attributes()->type->__toString(),
            'maxPassengers' => $a_car->attributes()->maxPassengers->__toString(),
            'doors' => $a_car->attributes()->doors->__toString(),
            'luggageLarge' => $a_car->attributes()->luggageLarge->__toString(),
            'luggageSmall' => $a_car->attributes()->luggageSmall->__toString(),
            'currency' => $a_car->attributes()->currency->__toString(),
            'status' => $a_car->attributes()->status->__toString(),
            'car_station_name' => $a_car_station->attributes()->name->__toString(),
            'car_station_address' => $a_car_station->attributes()->address->__toString(),
            'operationHoursPickUp' => $a_car_station->attributes()->operationHoursPickUp->__toString(),
            'operationHoursDropOff' => $a_car_station->attributes()->operationHoursDropOff->__toString(),
            'pick_up_date' => $pick_up->attributes()->date->__toString(),
            'drop_off_date' => $drop_off->attributes()->date->__toString(),
        );

        $car_program = $xmlObj->children('http://schemas.xmlsoap.org/soap/envelope/')->Body->children('http://tourico.com/webservices/')->SelectStationsResponse->Car->ProgramList->CarProgram;
        $car_program_count = count($car_program);

        for ($car_program_index = 0; $car_program_index < $car_program_count; $car_program_index++) {
            $a_car_program_obj = $car_program[$car_program_index];

            $components = $a_car_program_obj->ProgramIncludes->Component;
            $components_count = count($components);

            for ($component_index = 0; $component_index < $components_count; $component_index++) {
                $a_component = $components[$component_index];
                $component_list[] = array(
                    'name' => $a_component->attributes()->name->__toString(),
                    'desc' => $a_component->attributes()->desc->__toString()
                );
            }

            $car_program_values[] = array(
                'program_name' => $a_car_program_obj->attributes()->name->__toString(),
                'price' => $a_car_program_obj->attributes()->price->__toString(),
                'tax' => $a_car_program_obj->attributes()->tax->__toString(),
                'program_includes' => $component_list
            );
        }


        $select_station_info[] = array('car_program_info' => $car_program_values);
        return $select_station_info;
    }

    function ParseCarSearchByAirportXmlToArray($pick_up, $pick_up_date, $drop_off_date, $pick_up_hour, $drop_off_hour, $vehicle_type, $car_company, $total_pax, $drive_country_code = "", $driver_age = 22, $drop_off = "") {
        $carSearchApiCoreObj = new CarSearchAPICore();
        $car_search_by_airport_xml = $carSearchApiCoreObj->SearchCarByAirport($pick_up, $pick_up_date, $drop_off_date, $pick_up_hour, $drop_off_hour, $vehicle_type, $car_company, $total_pax, $drive_country_code = "", $driver_age = 22, $drop_off = "");

        $xmlObj = simplexml_load_string($car_search_by_airport_xml);

        $car_info_list = $xmlObj->children('http://schemas.xmlsoap.org/soap/envelope/')->Body->children('http://tourico.com/webservices/')->SearchCarsByAirportCodeResponse->CarResults->SearchCarInfo;

        $car_list_count = $car_info_list->count();
        for ($i = 0; $i < $car_list_count; $i++) {
            $a_car_info = $car_info_list[$i];
            $a_car_info_attributes = $a_car_info->attributes();

            $car_info_arr[] = array(
                'class' => $a_car_info_attributes->class->__toString(),
                'carName' => $a_car_info_attributes->carName->__toString(),
                'transmission' => $a_car_info_attributes->transmission->__toString(),
                'ac' => $a_car_info_attributes->ac->__toString(),
                'status' => $a_car_info_attributes->status->__toString(),
                'maxPassengers' => $a_car_info_attributes->maxPassengers->__toString(),
                'luggageLarge' => $a_car_info_attributes->luggageLarge->__toString(),
                'luggageSmall' => $a_car_info_attributes->luggageSmall->__toString(),
                'currency' => $a_car_info_attributes->currency->__toString(),
                'status' => $a_car_info_attributes->status->__toString(),
                'productId' => $a_car_info_attributes->productId->__toString(),
                //'minAvgPrice'       => $a_car_info_attributes->minAvgPrice->__toString(),
                'minPrice' => $a_car_info_attributes->minPrice->__toString(),
                'tax' => $a_car_info_attributes->tax->__toString(),
                'doors' => $a_car_info_attributes->doors->__toString(),
                'type' => $a_car_info_attributes->type->__toString(),
                'carThumb' => $a_car_info_attributes->carThumb->__toString(),
                'adaptorCode' => $a_car_info_attributes->adaptorCode->__toString(),
                'carID' => $a_car_info_attributes->carID->__toString(),
                'carCompanyName' => $a_car_info_attributes->carCompanyName->__toString(),
                'carCompanyId' => $a_car_info_attributes->carCompanyId->__toString()
            );
        }

        return $car_info_arr;
    }

    function ParseCarSearchByCityXmlToArray($pick_up, $pick_up_date, $drop_off_date, $pick_up_hour, $drop_off_hour, $vehicle_type, $car_company, $total_pax, $drive_country_code = "", $driver_age = 22, $drop_off = "") {
        $carSearchApiCoreObj = new CarSearchAPICore();
        $car_search_by_city_xml = $carSearchApiCoreObj->SearchCarByCity($pick_up, $pick_up_date, $drop_off_date, $pick_up_hour, $drop_off_hour, $vehicle_type, $car_company, $total_pax, $drive_country_code = "", $driver_age = 22, $drop_off = "");

        //$car_search_by_city_xml = file_get_contents('search_by_city.xml');
        $xmlObj = simplexml_load_string($car_search_by_city_xml);

        $car_list = $xmlObj->children('http://schemas.xmlsoap.org/soap/envelope/')->Body->children('http://tourico.com/webservices/')->SearchCarsResponse->CarResults->Car;

        $car_list_count = count($car_list);
        $car_list_arr = array();

        for ($index = 0; $index < $car_list_count; $index++) {
            $car = $car_list[$index];
            $car_name_obj = $car->attributes();

            $car_station = $car->RouteStations->Stations->CarStation;

            $car_list_arr[] = array(
                'carCompanyId' => $car_name_obj['carCompanyId']->__toString(),
                'carCompanyName' => $car_name_obj['carCompanyName']->__toString(),
                'carName' => $car_name_obj['carName']->__toString(),
                'carID' => $car_name_obj['carID']->__toString(),
                'adaptorCode' => $car_name_obj['adaptorCode']->__toString(),
                'carThumb' => $car_name_obj['carThumb']->__toString(),
                'class' => $car_name_obj['class']->__toString(),
                'transmission' => $car_name_obj['transmission']->__toString(),
                'ac' => $car_name_obj['ac']->__toString(),
                'type' => $car_name_obj['type']->__toString(),
                'maxPassengers' => $car_name_obj['maxPassengers']->__toString(),
                'doors' => $car_name_obj['doors']->__toString(),
                'luggageLarge' => $car_name_obj['luggageLarge']->__toString(),
                'luggageSmall' => $car_name_obj['luggageSmall']->__toString(),
                'currency' => $car_name_obj['currency']->__toString(),
                'status' => $car_name_obj['status']->__toString(),
                'productId' => $car_name_obj['productId']->__toString(),
                'productId_64' => base64_encode($car_name_obj['productId']->__toString()),
                'minAvgPrice' => $car_name_obj['minAvgPrice']->__toString(),
                'minPrice' => $car_name_obj['minPrice']->__toString(),
                'tax' => $car_name_obj['tax']->__toString(),
                'pick_up_station_id' => $car_station->attributes()->id->__toString()
            );
        }

        return $car_list_arr;
    }

    function GetDestinationList() {
        $distanceAPIResponse = $this->hotelSearchAPICoreObj->GetDestinationList();
        $simpleXMLELementObj = simplexml_load_string($distanceAPIResponse);
        $continentList = $simpleXMLELementObj->children('http://schemas.xmlsoap.org/soap/envelope/')->Body->children('http://touricoholidays.com/WSDestinations/2008/08/DataContracts')->DestinationResponse->DestinationResult->Continent;
        $destinationList = array();
        $totalContinent = $continentList->count();

        $locationList = array();
        for ($conIndex = 0; $conIndex < $totalContinent; $conIndex++) {
            $aContinent = $continentList[$conIndex];
            $countryList = $aContinent->Country;
            $totalCountry = $countryList->count();
            //var_dump($totalCountry );
            for ($countryIndex = 0; $countryIndex < $totalCountry; $countryIndex++) {
                $aCountry = $countryList[$countryIndex];
                $countryName = $aCountry->attributes()->name->__toString();

                $stateList = $aCountry->State;
                $totalState = count($stateList);
                for ($stateIndex = 0; $stateIndex < $totalState; $stateIndex++) {
                    $aState = $stateList[$stateIndex];
                    $stateName = $aState->attributes()->name->__toString();
                    if (strlen($stateName) > 0)
                        $stateName .= ", ";

                    $cityList = $aState->City;
                    $totalCity = count($cityList);
                    //echo $totalCity . '<br /><br />';
                    for ($cityIndex = 0; $cityIndex < $totalCity; $cityIndex++) {
                        $aCity = $cityList[$cityIndex];
                        $cityAttributes = $aCity->attributes();
                        $cityName = $cityAttributes->name->__toString();
                        //var_dump($cityAttributes);
                        $cityLatitude = $cityAttributes->cityLatitude->__toString();
                        $cityLongitude = $cityAttributes->cityLongitude->__toString();

                        $locationList[] = array(
                            'destination_id' => $cityAttributes->destinationId->__toString(),
                            'destination_name' => $cityName . ', ' . $stateName . $countryName,
                            'destination_code' => $cityAttributes->destinationCode->__toString(),
                            'city_name' => $cityName,
                            'location_name' => "",
                            'city_latitude' => $cityLatitude,
                            'city_longitude' => $cityLongitude,
                            'country_name' => $countryName
                        );
                        $cityLocationList = $aCity->CityLocation;
                        if (isset($cityLocationList)) {
                            $totalCityLocationList = count($cityLocationList);
                            for ($cityLocationIndex = 0; $cityLocationIndex < $totalCityLocationList; $cityLocationIndex++) {
                                $aCityLocation = $cityLocationList[$cityLocationIndex];
                                $cityLocationAttributes = $aCityLocation->attributes();
                                $cityLocationName = $cityLocationAttributes->location->__toString();

                                $locationList[] = array(
                                    'destination_id' => $cityLocationAttributes->destinationId->__toString(),
                                    'destination_name' => $cityLocationName . ', ' . $cityName . ', ' . $stateName . $countryName,
                                    'destination_code' => $cityLocationAttributes->destinationCode->__toString(),
                                    'city_name' => $cityName,
                                    'location_name' => $cityLocationName,
                                    'city_latitude' => $cityLatitude,
                                    'city_longitude' => $cityLongitude,
                                    'country_name' => $countryName
                                );
                            }
                        }
                    }
                }

                /* $destinationList[] = array(
                  'country'   =>  $countryName,
                  'location_list' =>  $locationList
                  ); */
            }
        }
        /* echo "<pre>";
          print_r($destinationList );
          echo "</pre>"; */
        return $locationList;
    }

}

Class CarSearchAPICore {

    private $CAR_SEARCH_API_USERNAME = "HDL103";
    private $CAR_SEARCH_API_PASSWORD = "111111";
    private $CAR_SEARCH_API_ENDPOINT = "http://demo-carws.touricoholidays.com/CarWebService.svc";
    public $CAR_SEARCH_BY_CITY_API_ACTION = "http://tourico.com/webservices/ICarService/SearchCars";
    public $CAR_SEARCH_BY_AIRPORT_API_ACTION = "http://tourico.com/webservices/ICarService/SearchCarsByAirportCode";

    function GetRulesAndRestrictions($car_company_id) {
        $RULES_AND_RESTRICTION_API_ACTION = "http://tourico.com/webservices/ICarService/GetRulesAndRestrictions";

        $request = <<<EOM
                <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://tourico.com/webservices/">
       <soapenv:Header>
          <web:LoginHeader>
             <web:UserName>{$this->CAR_SEARCH_API_USERNAME}</web:UserName>
             <web:Password>{$this->CAR_SEARCH_API_PASSWORD}</web:Password>
             <web:Culture>en_US</web:Culture>
             <web:Version>7.123</web:Version>
          </web:LoginHeader>
       </soapenv:Header>
       <soapenv:Body>
          <web:GetRulesAndRestrictionsRequest>
             <web:CarCompanyId>{$car_company_id}</web:CarCompanyId>
          </web:GetRulesAndRestrictionsRequest>
       </soapenv:Body>
    </soapenv:Envelope>
EOM;

        $response = $this->SendPostRequestToTourico($this->CAR_SEARCH_API_ENDPOINT, $request, $RULES_AND_RESTRICTION_API_ACTION);
        return $response;
    }

    function CancelCar($res_id) {

        $CANCEL_CAR_ACTION = "http://tourico.com/webservices/ICarService/CancelCar";

        $request = <<<EOM
                <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://tourico.com/webservices/">
       <soapenv:Header>
          <web:LoginHeader>
             <web:UserName>{$this->CAR_SEARCH_API_USERNAME}</web:UserName>
             <web:Password>{$this->CAR_SEARCH_API_PASSWORD}</web:Password>
             <web:Culture>en_US</web:Culture>
             <web:Version>7.123</web:Version>
          </web:LoginHeader>
       </soapenv:Header>
       <soapenv:Body>
          <web:CancelCarRequest>
             <web:ResId>{$res_id}</web:ResId>
          </web:CancelCarRequest>
       </soapenv:Body>
    </soapenv:Envelope>
EOM;

        $response = $this->SendPostRequestToTourico($this->CAR_SEARCH_API_ENDPOINT, $request, $CANCEL_CAR_ACTION);
        return $response;
    }

    function BookCar($record_locator_id, $selected_program, $first_name, $last_name, $age, $special_request = "0", $payment_type, $confirmation_email = "0", $confirmation_logo = "0", $requested_price, $delta_price, $currency, $agent_ref_number = "0", $rdid_ref_number = "0") {

        $BOOK_CAR_API_ACTION = "http://tourico.com/webservices/ICarService/BookCar";

        $request = <<<EOM
                <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://tourico.com/webservices/">
       <soapenv:Header>
          <web:LoginHeader>
             <web:UserName>{$this->CAR_SEARCH_API_USERNAME}</web:UserName>
             <web:Password>{$this->CAR_SEARCH_API_PASSWORD}</web:Password>
             <web:Culture>en_US</web:Culture>
             <web:Version>8</web:Version>
          </web:LoginHeader>
       </soapenv:Header>
       <soapenv:Body>
          <web:BookRequest>
             <web:BookCar>
                <web:recordLocatorId>{$record_locator_id}</web:recordLocatorId>
                <web:SelectedProgram>{$selected_program}</web:SelectedProgram>
                <web:DriverInfo firstName="{$first_name}" middleName="" lastName="{$last_name}" countryCode="US" homePhone="555-999-1111" mobilePhone="111-555-1111" age="{$age}" carCompanyDiscountNumber="" frequentFlyerProgram="" frequentFlyerNumber="" flightInformation=""/>
                <web:SpecialRequest>{$special_request}</web:SpecialRequest>
                <web:PaymentType>{$payment_type}</web:PaymentType>
                <web:ConfirmationEmail>{$confirmation_email}</web:ConfirmationEmail>
                <web:ConfirmationLogo>{$confirmation_logo}</web:ConfirmationLogo>
                <web:RequestedPrice>{$requested_price}</web:RequestedPrice>
                <web:DeltaPrice>{$delta_price}</web:DeltaPrice>
                <web:Currency>{$currency}</web:Currency>
                <web:agentRefNumber>{$agent_ref_number}</web:agentRefNumber>
                <web:rgIdRefNumber>{$rdid_ref_number}</web:rgIdRefNumber>
             </web:BookCar>
          </web:BookRequest>
       </soapenv:Body>
    </soapenv:Envelope>
EOM;

        $response = $this->SendPostRequestToTourico($this->CAR_SEARCH_API_ENDPOINT, $request, $BOOK_CAR_API_ACTION);
        return $response;
    }

    function GetSearchByCityXmlString($pick_up, $pick_up_date, $drop_off_date, $pick_up_hour, $drop_off_hour, $vehicle_type, $car_company, $total_pax, $drive_country_code = "", $driver_age = 22, $drop_off = "") {
        return <<<EOM
                <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://tourico.com/webservices/">
       <soapenv:Header>
          <web:LoginHeader>
             <web:UserName>{$this->CAR_SEARCH_API_USERNAME}</web:UserName>
             <web:Password>{$this->CAR_SEARCH_API_PASSWORD}</web:Password>
             <web:Culture>en_US</web:Culture>
             <web:Version>11</web:Version>
          </web:LoginHeader>
       </soapenv:Header>
       <soapenv:Body>
          <web:SearchCars>
             <web:SearchCarsObj>
                <web:Route>
                   <web:PickUp>{$pick_up}</web:PickUp>
                   <web:DropOff>{$drop_off}</web:DropOff>
                </web:Route>
                <web:PickUpDate>{$pick_up_date}</web:PickUpDate>
                <web:DropOffDate>{$drop_off_date}</web:DropOffDate>
                <web:PickUpHour>{$pick_up_hour}</web:PickUpHour>
                <web:DropOffHour>{$drop_off_hour}</web:DropOffHour>
                <web:VehicleType>{$vehicle_type}</web:VehicleType>
                <web:CarCompany>{$car_company}</web:CarCompany>
                <web:TotalPax>{$total_pax}</web:TotalPax>
                <web:DriverCountryCode>{$drive_country_code}</web:DriverCountryCode>
                <web:DriverAge>{$driver_age}</web:DriverAge>
             </web:SearchCarsObj>
          </web:SearchCars>
       </soapenv:Body>
    </soapenv:Envelope>
EOM;
    }

    function SearchCarByCity($pick_up, $pick_up_date, $drop_off_date, $pick_up_hour, $drop_off_hour, $vehicle_type, $car_company, $total_pax, $drive_country_code = "", $driver_age = 22, $drop_off = "") {
        $request = $this->GetSearchByCityXmlString($pick_up, $pick_up_date, $drop_off_date, $pick_up_hour, $drop_off_hour, $vehicle_type, $car_company, $total_pax, $drive_country_code = "", $driver_age = 22, $drop_off = "");

        $response = $this->SendPostRequestToTourico($this->CAR_SEARCH_API_ENDPOINT, $request, $this->CAR_SEARCH_BY_CITY_API_ACTION);
        return $response;
    }

    function SearchCarByAirport($pick_up, $pick_up_date, $drop_off_date, $pick_up_hour, $drop_off_hour, $vehicle_type, $car_company, $total_pax) {
        $request = <<<EOM
                <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://tourico.com/webservices/">
       <soapenv:Header>
          <web:LoginHeader>
             <web:UserName>{$this->CAR_SEARCH_API_USERNAME}</web:UserName>
             <web:Password>{$this->CAR_SEARCH_API_PASSWORD}</web:Password>
             <web:Culture>en_US</web:Culture>
             <web:Version>8</web:Version>
          </web:LoginHeader>
       </soapenv:Header>
       <soapenv:Body>
          <web:SearchCarsByAirportCode>
             <web:SearchCarsObj>
                <web:Route>
                   <web:PickUp>{$pick_up}</web:PickUp>
                </web:Route>
                <web:PickUpDate>{$pick_up_date}</web:PickUpDate>
                <web:DropOffDate>{$drop_off_date}</web:DropOffDate>
                <web:PickUpHour>{$pick_up_hour}</web:PickUpHour>
                <web:DropOffHour>{$drop_off_hour}</web:DropOffHour>
                <web:VehicleType>{$vehicle_type}</web:VehicleType>
                <web:CarCompany>{$car_company}</web:CarCompany>
                <web:TotalPax>{$total_pax}</web:TotalPax>
             </web:SearchCarsObj>
          </web:SearchCarsByAirportCode>
       </soapenv:Body>
    </soapenv:Envelope>
EOM;

        $response = $this->SendPostRequestToTourico($this->CAR_SEARCH_API_ENDPOINT, $request, $this->CAR_SEARCH_BY_AIRPORT_API_ACTION);
        return $response;
    }

    function SendPostRequestToTourico($url, $request, $actionUri) {
        $ch = curl_init($url);
        $options = array(
            CURLOPT_RETURNTRANSFER => true, // return web page
            CURLOPT_HEADER => false, // don't return headers
            CURLOPT_FOLLOWLOCATION => false, // follow redirects
            CURLOPT_ENCODING => "utf-8", // handle all encodings
            CURLOPT_AUTOREFERER => true, // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 300, // timeout on connect
            CURLOPT_TIMEOUT => 300, // timeout on response
            CURLOPT_POST => 1, // i am sending post data
            CURLOPT_POSTFIELDS => $request, // this are my post vars
            CURLOPT_SSL_VERIFYHOST => 0, // don't verify ssl
            CURLOPT_SSL_VERIFYPEER => false, //
            CURLOPT_VERBOSE => 1,
            CURLOPT_HTTPHEADER => array(
                "Content-type: text/xml;charset=\"utf-8\"",
                "Accept: text/xml",
                "Cache-Control: no-cache",
                'Accept-Encoding: gzip',
                //"Pragma: no-cache",
                "SOAPAction: " . $actionUri,
                //"Content-length: ".strlen($request)
            )
        );
        curl_setopt_array($ch, $options);
        $data = curl_exec($ch);
        $curl_errno = curl_errno($ch);
        $curl_error = curl_error($ch);
        //echo $curl_errno;
        //echo $curl_error;
        curl_close($ch);
        return $data;
    }

}

Class SelectStationAPICore {

    private $CAR_SEARCH_API_USERNAME = "HDL103";
    private $CAR_SEARCH_API_PASSWORD = "111111";
    private $CAR_SEARCH_API_ENDPOINT = "http://demo-carws.touricoholidays.com/CarWebService.svc";
    public $SELECT_STATION_API_ACTION = "http://tourico.com/webservices/ICarService/SelectStations";

    function SelectStation($product_id, $station_id, $drop_off_station_id) {
        $request = <<<EOM
            <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://tourico.com/webservices/">
    <soapenv:Header>
      <web:LoginHeader>
         <web:UserName>{$this->CAR_SEARCH_API_USERNAME}</web:UserName>
         <web:Password>{$this->CAR_SEARCH_API_PASSWORD}</web:Password>
         <web:Culture>en_US</web:Culture>
         <web:Version>8</web:Version>
      </web:LoginHeader>
    </soapenv:Header>
    <soapenv:Body>
      <web:SelectStation>
         <web:SelectStationObj>
            <web:ProductId>{$product_id}</web:ProductId>
            <web:Stations>
               <web:StationId>{$station_id}</web:StationId>
               <web:DropOffStationId>{$drop_off_station_id}</web:DropOffStationId>
            </web:Stations>
         </web:SelectStationObj>
      </web:SelectStation>
    </soapenv:Body>
    </soapenv:Envelope>
EOM;
        $response = $this->SendPostRequestToTourico($this->CAR_SEARCH_API_ENDPOINT, $request, $this->SELECT_STATION_API_ACTION);

        return $response;
    }

    function SendPostRequestToTourico($url, $request, $actionUri) {
        $ch = curl_init($url);
        $options = array(
            CURLOPT_RETURNTRANSFER => true, // return web page
            CURLOPT_HEADER => false, // don't return headers
            CURLOPT_FOLLOWLOCATION => false, // follow redirects
            CURLOPT_ENCODING => "utf-8", // handle all encodings
            CURLOPT_AUTOREFERER => true, // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 300, // timeout on connect
            CURLOPT_TIMEOUT => 300, // timeout on response
            CURLOPT_POST => 1, // i am sending post data
            CURLOPT_POSTFIELDS => $request, // this are my post vars
            CURLOPT_SSL_VERIFYHOST => 0, // don't verify ssl
            CURLOPT_SSL_VERIFYPEER => false, //
            CURLOPT_VERBOSE => 1,
            CURLOPT_HTTPHEADER => array(
                "Content-type: text/xml;charset=\"utf-8\"",
                "Accept: text/xml",
                "Cache-Control: no-cache",
                'Accept-Encoding: gzip',
                //"Pragma: no-cache",
                "SOAPAction: " . $actionUri,
                //"Content-length: ".strlen($request)
            )
        );
        curl_setopt_array($ch, $options);
        $data = curl_exec($ch);
        $curl_errno = curl_errno($ch);
        $curl_error = curl_error($ch);
        //echo $curl_errno;
        //echo $curl_error;
        curl_close($ch);
        return $data;
    }

}

class ActivityAPIResponseProcess {

    function ProcessGetActivityDetailsByDestinationIdResponse($destination_id, $from_date, $to_date, $activity_name) {

        error_reporting(E_ALL);
        ini_set('display_errors', 1);

        $activity_api_obj = new ActivitiesAPICore();
        $activity_xml = $activity_api_obj->SearchActivityByDestinationId($destination_id, $from_date, $to_date, $activity_name);
        $activity_xml_obj = simplexml_load_string($activity_xml);

        $category_list = $activity_xml_obj->children('http://schemas.xmlsoap.org/soap/envelope/')->Body->children('http://tourico.com/webservices/activity')->SearchActivityByDestinationIdsResponse->SearchActivityByDestinationIdsResult->children('http://schemas.tourico.com/webservices/activity')->Categories->Category;
        $category_list_count = count($category_list);

        $category = array();
        for ($i = 0; $i < $category_list_count; $i++) {
            $a_category = $category_list[$i];
            $catergory_name = $a_category->attributes()->categoryName->__toString();
            $catergory_id = $a_category->attributes()->categoryId->__toString();

            $activity_lists = $a_category->Activities->Activity;
            $activity_lists_count = count($activity_lists);

            $activity = array();
            for ($j = 0; $j < $activity_lists_count; $j++) {
                $a_activity = $activity_lists[$j];

                $activity_option_lists = $a_activity->ActivityOptions->ActivityOption;
                $activity_option_lists_count = count($activity_option_lists);

                $activity_option = array();

                for ($k = 0; $k < $activity_option_lists_count; $k++) {
                    $a_activity_option = $activity_option_lists[$k];

                    $availability = $a_activity_option->Availabilities->Availability;
                    $availability = array(
                        'fromDate' => $availability->attributes()->fromDate->__toString(),
                        'toDate' => $availability->attributes()->toDate->__toString(),
                        'maxAdults' => $availability->attributes()->maxAdults->__toString(),
                        'maxChildren' => $availability->attributes()->maxChildren->__toString(),
                        'maxUnits' => $availability->attributes()->maxUnits->__toString(),
                        'adultPrice' => $availability->attributes()->adultPrice->__toString(),
                        'childPrice' => $availability->attributes()->childPrice->__toString(),
                        'unitPrice' => $availability->attributes()->unitPrice->__toString()
                    );

                    $activity_option[] = array(
                        'type' => $a_activity_option->attributes()->type->__toString(),
                        'typeDescription' => $a_activity_option->attributes()->typeDescription->__toString(),
                        'optionId' => $a_activity_option->attributes()->optionId->__toString(),
                        'name' => $a_activity_option->attributes()->name->__toString(),
                        'availability' => $availability
                    );
                }

                $activity[] = array(
                    'description' => $a_activity->attributes()->description->__toString(),
                    'starsLevel' => $a_activity->attributes()->starsLevel->__toString(),
                    'activityId' => $a_activity->attributes()->activityId->__toString(),
                    'currency' => $a_activity->attributes()->currency->__toString(),
                    'maxChildAge' => $a_activity->attributes()->maxChildAge->__toString(),
                    'name' => $a_activity->attributes()->name->__toString(),
                    'provider' => $a_activity->attributes()->provider->__toString(),
                    'bestValue' => $a_activity->attributes()->bestValue->__toString(),
                    'thumbURL' => $a_activity->attributes()->thumbURL->__toString(),
                    'activity' => $activity_option
                );
            }

            $category[] = array(
                'category_name' => $catergory_name,
                'category_id' => $catergory_id,
                'activity' => $activity,
            );
        }

        return $category;
    }

    function ProcessGetActivityDetailsResponse($activity_ids) {
        $activityApiCoreObj = new ActivitiesAPICore();
        $activity_details_xml = $activityApiCoreObj->GetActivityDetails($activity_ids);
        $activity_details_obj = simplexml_load_string($activity_details_xml);

        $activity_details = $activity_details_obj->children('http://schemas.xmlsoap.org/soap/envelope/')->Body->children('http://tourico.com/webservices/activity')->GetActivityDetailsResponse->GetActivityDetailsResult->children('http://schemas.tourico.com/webservices/activity')->ActivitiesDetails->ActivityDetails;

        $location = $activity_details->Location;

        $star_level = $activity_details->attributes()->starsLevel->__toString();
        $description = $activity_details->Description;

        $countryCode = $location->attributes()->countryCode->__toString();
        $stateCode = $location->attributes()->stateCode->__toString();
        $city = $location->attributes()->city->__toString();
        $searchingState = $location->attributes()->searchingState->__toString();
        $searchingCity = $location->attributes()->searchingCity->__toString();
        $location_value = $location->attributes()->location->__toString();
        $address = $location->attributes()->address->__toString();
        $longitude = $location->attributes()->longitude->__toString();
        $latitude = $location->attributes()->latitude->__toString();

        $short_description = $description->ShortDescription->attributes()->desc->__toString();


        $voucher_remarks = $description->LongDescription->VoucherRemarks;
        $description_fragment_lists = $description->LongDescription->Fragments->DescriptionFragment;
        $description_fragment_list_count = count($description_fragment_lists);

        $highlights_general_details = "";
        $inclusion = "";
        $exclusion = "";
        $hour_of_operation = "";
        $location = "";
        $participant_restrictions = "";
        $policies = "";


        for ($des_frag_index = 0; $des_frag_index < $description_fragment_list_count; $des_frag_index++) {
            $a_fragment = $description_fragment_lists[$des_frag_index];

            $type = $a_fragment->attributes()->type->__toString();
            $value = $a_fragment->attributes()->value->__toString();

            if ("Highlights / General details" == $type) {
                $highlights_general_details = $value;
            } else if ("Inclusion" == $type) {
                $inclusion = $value;
            } else if ("Exclusion" == $type) {
                $exclusion = $value;
            } else if ("Hours of operation" == $type) {
                $hour_of_operation = $value;
            } else if ("Location" == $type) {
                $location = $value;
            } else if ("Participant restrictions" == $type) {
                $participant_restrictions = $value;
            } else if ("Policies" == $type) {
                $policies = $value;
            }
        }

        $image_lists = $activity_details_obj->children('http://schemas.xmlsoap.org/soap/envelope/')->Body->children('http://tourico.com/webservices/activity')->GetActivityDetailsResponse->GetActivityDetailsResult->children('http://schemas.tourico.com/webservices/activity')->ActivitiesDetails->ActivityDetails->Media->Images->Image;

        $image_list_count = count($image_lists);
        for ($image_list_index = 0; $image_list_index < $image_list_count; $image_list_index++) {
            $a_image = $image_lists[$image_list_index];
            $images[] = $a_image->attributes()->path->__toString();
        }

        $movie_lists = $activity_details_obj->children('http://schemas.xmlsoap.org/soap/envelope/')->Body->children('http://tourico.com/webservices/activity')->GetActivityDetailsResponse->GetActivityDetailsResult->children('http://schemas.tourico.com/webservices/activity')->ActivitiesDetails->ActivityDetails->Media->Movies->Movie;

        $movie_lists_count = count($movie_lists);
        for ($movie_list_index = 0; $movie_list_index < $movie_lists_count; $movie_list_index++) {
            $a_movie = $movie_lists[$movie_list_index];
            $movies[] = array(
                'youtubeId' => $a_movie->attributes()->youtubeId->__toString(),
                'title' => $a_movie->attributes()->title->__toString(),
            );
        }

        $location_str = $location;
        $location_arr_one_new_line = preg_split("/\n/", $location_str);
        $location_count = count($location_arr_one_new_line);

        $heading = trim($location_arr_one_new_line[0]);

        $location2 = "";
        $duration = "";
        $participant_restrictions = "";
        $policies = "";
        $last_restriction = "";

        for ($location_index = 1; $location_index < $location_count; $location_index++) {
            $value = trim($location_arr_one_new_line[$location_index]);
            $desc = "";
            while (0 != strlen($value)) {
                $desc .= $value . "\n";
                $location_index++;
                $value = trim($location_arr_one_new_line[$location_index]);
            }

            if ("Location" == $heading) {
                $location2 = $desc;
            } else if ("Duration" == $heading) {
                $duration = $desc;
            } else if ("Participant restrictions" == $heading) {
                $participant_restrictions = $desc;
            } else if ("Policies" == $heading) {
                $policies = $desc;
            } else {
                $last_restriction = $desc;
            }
            $location_index++;
            $heading = trim($location_arr_one_new_line[$location_index]);
        }

        $final_activity_details = array(
            'images' => $images,
            'highlights_general_details' => $highlights_general_details,
            'inclusion' => $inclusion,
            'exclusion' => $exclusion,
            'hour_of_operation' => $hour_of_operation,
            'location' => $location2,
            'duration' => $duration,
            'participant_restrictions' => $participant_restrictions,
            'policies' => $policies,
            'last_restriction' => $last_restriction,
            'movies' => $movies,
            'longitude' => $longitude,
            'latitude' => $latitude
        );

        return $final_activity_details;
    }

}

Class ActivitiesAPICore {

    private $DESTINATION_API_USERNAME = 'HitelsDif15';
    private $DESTINATION_API_PASSWORD = 'HD@th2015';
    private $DESTINATION_API_ENDPOINT = 'http://destservices.touricoholidays.com/DestinationsService.svc?wsdl';

    private $DESTINATION_API_ACTION = 'http://touricoholidays.com/WSDestinations/2008/08/Contracts/IDestinationContracts/GetDestination';
    //USER NAME
    private $ACTIVITY_API_USERNAME = "HDL103";
    private $ACTIVITY_API_PASSWORD = "111111";
    //END POINTS
    private $ACTIVITY_API_ENDPOINT = "http://demo-activityws.touricoholidays.com/ActivityBookFlow.svc/bas";
    //ACTIONS
    private $ACTIVITY_BY_DESTINATION_ID_API_ACTION = "http://tourico.com/webservices/activity/IActivityBookFlow/SearchActivityByDestinationIds";
    private $GET_ACTIVITY_DETAILS = "http://tourico.com/webservices/activity/IActivityBookFlow/GetActivityDetails";

    function GetActivityDetails($activity_id) {

        $request = <<<EOM
    <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:aut="http://schemas.tourico.com/webservices/authentication" xmlns:act="http://tourico.com/webservices/activity">
       <soapenv:Header>
          <aut:AuthenticationHeader>
             <aut:LoginName>{$this->ACTIVITY_API_USERNAME}</aut:LoginName>
             <aut:Password>{$this->ACTIVITY_API_PASSWORD}</aut:Password>
             <aut:Culture>en_US</aut:Culture>
             <aut:Version>7.123</aut:Version>
          </aut:AuthenticationHeader>
       </soapenv:Header>
       <soapenv:Body>
          <act:GetActivityDetails>
             <act:ActivitiesIds>
                <act:ActivityId id="{$activity_id}"/>
             </act:ActivitiesIds>
          </act:GetActivityDetails>
       </soapenv:Body>
    </soapenv:Envelope>
EOM;

        $response = $this->SendPostRequestToTourico($this->ACTIVITY_API_ENDPOINT, $request, $this->GET_ACTIVITY_DETAILS);

        return $response;
    }

    function SearchActivityByDestinationId($destination_id, $from_date, $to_date, $activity_name) {

        $optionalParams = '
    		<ActivityName xmlns="http://schemas.tourico.com/webservices/activity">
                <Value>'.$activity_name.'</Value>
            </ActivityName>
		';

        $request = <<<EOM
    <s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">
    <s:Header>
        <h:AuthenticationHeader xmlns:h="http://schemas.tourico.com/webservices/authentication" xmlns="http://schemas.tourico.com/webservices/authentication" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
            <LoginName>{$this->ACTIVITY_API_USERNAME}</LoginName>
            <Password>{$this->ACTIVITY_API_PASSWORD}</Password>
            <Version xsi:nil="true"/>
        </h:AuthenticationHeader>
    </s:Header>
    <s:Body xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><SearchActivityByDestinationIds xmlns="http://tourico.com/webservices/activity">
        <SearchRequest>
            <fromDate xmlns="http://schemas.tourico.com/webservices/activity">{$from_date}</fromDate>
            <toDate xmlns="http://schemas.tourico.com/webservices/activity">{$to_date}</toDate>
            <destinationIds xmlns="http://schemas.tourico.com/webservices/activity">
                <int>{$destination_id}</int>
            </destinationIds>
            {$optionalParams}
        </SearchRequest>
    </SearchActivityByDestinationIds></s:Body></s:Envelope>
EOM;

        $response = $this->SendPostRequestToTourico($this->ACTIVITY_API_ENDPOINT, $request, $this->ACTIVITY_BY_DESTINATION_ID_API_ACTION);

        return $response;
    }

    function SendPostRequestToTourico($url, $request, $actionUri) {
        $ch = curl_init($url);
        $options = array(
            CURLOPT_RETURNTRANSFER => true, // return web page
            CURLOPT_HEADER => false, // don't return headers
            CURLOPT_FOLLOWLOCATION => false, // follow redirects
            CURLOPT_ENCODING => "utf-8", // handle all encodings
            CURLOPT_AUTOREFERER => true, // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 300, // timeout on connect
            CURLOPT_TIMEOUT => 300, // timeout on response
            CURLOPT_POST => 1, // i am sending post data
            CURLOPT_POSTFIELDS => $request, // this are my post vars
            CURLOPT_SSL_VERIFYHOST => 0, // don't verify ssl
            CURLOPT_SSL_VERIFYPEER => false, //
            CURLOPT_VERBOSE => 1,
            CURLOPT_HTTPHEADER => array(
                "Content-type: text/xml;charset=\"utf-8\"",
                "Accept: text/xml",
                "Cache-Control: no-cache",
                'Accept-Encoding: gzip',
                //"Pragma: no-cache",
                "SOAPAction: " . $actionUri,
                //"Content-length: ".strlen($request)
            )
        );
        curl_setopt_array($ch, $options);
        $data = curl_exec($ch);
        $curl_errno = curl_errno($ch);
        $curl_error = curl_error($ch);
        //echo $curl_errno;
        //echo $curl_error;
        curl_close($ch);
        return $data;
    }

}

/////////////////////////////////////cruise api portion

class ProcessCruiseAPI {

    private $cruiseAPICoreObj;

    function __construct() {
        $this->cruiseAPICoreObj = new CruiseAPICore();
    }

    function GetCruiseDestinationList() {
        //$reponse = file_get_contents("response.xml");
        $reponse = $this->cruiseAPICoreObj->GetCruiseDestinationList();
        $smpleXmlObj = simplexml_load_string($reponse);
        $xmlDestinationObjList = $smpleXmlObj->children("http://schemas.xmlsoap.org/soap/envelope/")->Body->children("http://tourico.com/webservices/")->CruiseDestinationsResult->CruiseDestinationsList->CruiseDestination;
        $destinationList = array();
        $totalDestination = count($xmlDestinationObjList);
        for ($index = 0; $index < $totalDestination; $index++) {
            $aDestinationXMLobj = $xmlDestinationObjList[$index];
            $destinationXmlObjAttr = $aDestinationXMLobj->attributes();
            $destinationList[] = array(
                'destination_id' => $destinationXmlObjAttr->Id->__toString(),
                'destination_name' => $destinationXmlObjAttr->Name->__toString(),
                'parent_id' => $destinationXmlObjAttr->ParentId->__toString()
            );
        }

        return $destinationList;
    }

    function GetCruiseLineList() {
        $reponse = $this->cruiseAPICoreObj->GetCruiseLineList();
        //header('Content-type: application/xml');
        //echo $reponse;
        //exit(0);
        $smpleXmlObj = simplexml_load_string($reponse);
        $xmCruiseLineList = $smpleXmlObj->children("http://schemas.xmlsoap.org/soap/envelope/")->Body->children("http://tourico.com/webservices/")->CruiseLinesResult->CruiseLinesList->CruiseLine;

        $cruiseLineList = array();
        $totalDestinationLine = count($xmCruiseLineList);
        for ($index = 0; $index < $totalDestinationLine; $index++) {
            $aXmlCruiseLine = $xmCruiseLineList[$index];
            $aXmlCruiseLineAttr = $aXmlCruiseLine->attributes();
            $cruiseLineList[] = array(
                'cruise_line_id' => $aXmlCruiseLineAttr->CruiseLineId->__toString(),
                'destination_name' => $aXmlCruiseLineAttr->CruiseLineName->__toString()
            );
        }

        return $cruiseLineList;
    }

    function SearchCruise($cruiseDestinationId, $departmingYearMonth, $cruiseLengthRange) {
        $firstDayofMonth = $departmingYearMonth . "-01";
        $dateObj = DateTime::createFromFormat('Y-m-d', $firstDayofMonth);
        $lastDayOfMonth = $dateObj->format('Y-m-t');

        $cruiseLengthRangeParts = explode('-', $cruiseLengthRange);
        $minCruiseLength = $cruiseLengthRangeParts[0];
        $maxCruiseLength = $cruiseLengthRangeParts[1];

        $reponse = $this->cruiseAPICoreObj->SearchCruise($cruiseDestinationId, $firstDayofMonth, $lastDayOfMonth, $minCruiseLength, $maxCruiseLength);
        //header("Content-type: application/xml");
        //echo $reponse;
        //exit(0);
        $smpleXmlObj = simplexml_load_string($reponse);
        $xmCruiseList = $smpleXmlObj->children("http://schemas.xmlsoap.org/soap/envelope/")->Body->children("http://tourico.com/webservices/")->SearchCruiseResults->CruiseList->Cruise;

        $cruiseDataList = array();
        $totalCruise = count($xmCruiseList);
        for ($index = 0; $index < $totalCruise; $index++) {
            $aCruiseObj = $xmCruiseList[$index];

            $segmentsList = $aCruiseObj->Itinerary->SegmentsList->Segment;
            $segments_list_count = count($segmentsList);
            $port_infos = [];
            for ($i = 0; $i < $segments_list_count; $i++) {
                $a_segment = $segmentsList[$i];
                $port_infos[] = array(
                    'port_id' => $a_segment->attributes()->PortId->__toString(),
                    'port_name' => $a_segment->attributes()->PortName->__toString()
                );
            }

            $aCruiseObjAttr = $aCruiseObj->attributes();

            $cruiseLineId = $aCruiseObjAttr->CruiseLineId->__toString();
            $cruiseLineName = $aCruiseObjAttr->CruiseLineName->__toString();
            $cruiseLineLogo = $aCruiseObjAttr->CruiseLineLogo->__toString();
            $shipId = $aCruiseObjAttr->ShipId->__toString();
            $shipName = $aCruiseObjAttr->ShipName->__toString();
            $shipRating = $aCruiseObjAttr->ShipRating->__toString();
            $shipImg = $aCruiseObjAttr->ShipImg->__toString();
            $cruiseLength = $aCruiseObjAttr->CruiseLength->__toString();

            $cruiseItinerary = $aCruiseObj->Itinerary;
            $cruiseItineraryAttr = $cruiseItinerary->attributes();
            $xmlItinerarySegmentObjList = $cruiseItinerary->SegmentsList->Segment;
            $totalItineraySegment = count($xmlItinerarySegmentObjList);
            $itinerarySegmentList = array();

            for ($jindex = 0; $jindex < $totalItineraySegment; $jindex++) {
                $aXmlItinerarySegmentObj = $xmlItinerarySegmentObjList[$jindex];
                $aXmlItinerarySegmentObjAttr = $aXmlItinerarySegmentObj->attributes();

                $serialNo = $aXmlItinerarySegmentObjAttr->Order->__toString();
                $dayNo = $aXmlItinerarySegmentObjAttr->Day->__toString();
                $portId = $aXmlItinerarySegmentObjAttr->PortId->__toString();
                $portName = $aXmlItinerarySegmentObjAttr->PortName->__toString();
                $arrival = $aXmlItinerarySegmentObjAttr->Arrival->__toString();
                $departure = $aXmlItinerarySegmentObjAttr->Departure->__toString();

                $itinerarySegmentList[] = array(
                    'serial_no' => $serialNo,
                    'day_no' => $dayNo,
                    'port_id' => $portId,
                    'port_name' => $portName,
                    'arrival' => $arrival,
                    'departure' => $departure,
                );
            }

            //work on saved port list to build up whole details

            $cruiseItineraryId = $cruiseItineraryAttr->ItineraryId->__toString();
            $cruiseItineraryName = $cruiseItineraryAttr->Name->__toString();

            $sailingDateXmlList = $aCruiseObj->SailingDates->Sailing;
            $sailingList = array();
            $totalSailing = count($sailingDateXmlList);
            for ($jindex = 0; $jindex < $totalSailing; $jindex++) {
                $sailingDateXmlObj = $sailingDateXmlList[$jindex];
                $sailingDateXmlObjAttr = $sailingDateXmlObj->attributes();

                $sailingID = $sailingDateXmlObjAttr->SailingID->__toString();
                $departureDate = $sailingDateXmlObjAttr->Departure->__toString();
                $arrivalDate = $sailingDateXmlObjAttr->Arrival->__toString();
                $insideCabinPrice = $sailingDateXmlObjAttr->IN_PricePublish->__toString();
                $oceanViewPrice = $sailingDateXmlObjAttr->OV_PricePublish->__toString();
                $suitePrice = $sailingDateXmlObjAttr->ST_PricePublish->__toString();
                $balconyPrice = $sailingDateXmlObjAttr->BL_PricePublish->__toString();
                $sailingList = array(
                    'sailing_id' => $sailingID,
                    'deapture_date' => $departureDate,
                    'arrival_date' => $arrivalDate,
                    'inside_cabin_price' => $insideCabinPrice,
                    'ocean_view_price' => $oceanViewPrice,
                    'suit_price' => $suitePrice,
                    'balcony_price' => $balconyPrice
                );
            }

            $cruiseDataList[] = array(
                'cruise_line_id' => $cruiseLineId,
                'cruise_line_name' => $cruiseLineName,
                'cruise_line_logo' => $cruiseLineLogo,
                'ship_name' => $shipName,
                'ship_rating' => $shipRating,
                'ship_image' => $shipImg,
                'cruise_length' => $cruiseLength,
                'cruise_itinerary_id' => $cruiseItineraryId,
                'cruise_itinerary_name' => $cruiseItineraryName,
                'sailing_list' => $sailingList,
                'ship_id' => $shipId,
                'itinerary_segment_list' => $itinerarySegmentList,
            );
        }
        /* echo "<pre>";
          print_r($cruiseDataList);
          echo "</pre>"; */
        return $cruiseDataList;
    }

    function GetShipDetail($shipId) {
        $reponse = $this->cruiseAPICoreObj->GetShipDetails($shipId);
        //header("Content-type: application/xml");
        //echo $reponse;
        //exit(0);

        $smpleXmlObj = simplexml_load_string($reponse);
        $xmlShipObj = $smpleXmlObj->children("http://schemas.xmlsoap.org/soap/envelope/")->Body->children("http://tourico.com/webservices/")->ShipsResult->Ship;

        $shipAttrList = $xmlShipObj->attributes();
        $imageLogo = $shipAttrList->Img->__toString();
        $imageLogoParts = explode('/', $imageLogo);

        $shipLineCode = $imageLogoParts[4];
        $shipCode = $imageLogoParts[6];

        $xmlObjectImageList = $xmlShipObj->PhotoGallery->ShipImage;
        $totalImage = count($xmlObjectImageList);

        $shipPhotoGallery = array();
        for ($index = 0; $index < $totalImage; $index++) {
            $aShipImage = $xmlObjectImageList[$index];
            $aShipImageAttr = $aShipImage->attributes();

            $shipPhotoGallery[] = array(
                'big_image_url' => $aShipImageAttr->BigImg->__toString(),
                'thumb_image_url' => $aShipImageAttr->SmallImg->__toString()
            );
        }

        $xmlObjectAminityList = $xmlShipObj->Amenities->Amenity;
        $totalAminity = count($xmlObjectAminityList);

        $aminityList = array();
        $aminityTypeList = $this->GetAminityType();
        for ($index = 0; $index < $totalAminity; $index++) {
            $anAminity = $xmlObjectAminityList[$index];
            $anAminityAttr = $anAminity->attributes();

            $type = $anAminityAttr->Type->__toString();
            $typeName = $aminityTypeList[$type];
            if (isset($aminityList[$typeName]))
                $aminityList[$typeName][] = $anAminityAttr->Name->__toString();
            else
                $aminityList[$typeName] = array($anAminityAttr->Name->__toString());
        }

        //public area
        $xmlPublicAreaObjectList = $xmlShipObj->PublicAreas->PublicArea;
        $publicAreaList = array();
        $totalPublicArea = count($xmlPublicAreaObjectList);
        for ($index = 0; $index < $totalPublicArea; $index++) {
            $aXmlPublicAreaObj = $xmlPublicAreaObjectList[$index];
            $aXmlPublicAreaObjAttr = $aXmlPublicAreaObj->attributes();

            $aPublicArea = array();
            $aPublicArea['name'] = $aXmlPublicAreaObjAttr->Name->__toString();
            $aPublicArea['deck_no'] = $aXmlPublicAreaObjAttr->DeckNumber->__toString();
            //$imageObj = $aXmlPublicAreaObjAttr->Img;
            if (isset($aXmlPublicAreaObjAttr->Img))
                $aPublicArea['image'] = $aXmlPublicAreaObjAttr->Img->__toString();
            $publicAreaList[] = $aPublicArea;
        }

        //cabin categorylist
        $xmlCabinCategoryList = $xmlShipObj->ShipCabinCategories->ShipCabinCategory;
        $cabinCategoryList = array();
        $totalCabinCategory = count($xmlCabinCategoryList);
        for ($index = 0; $index < $totalCabinCategory; $index++) {
            $aXmlCabinCategory = $xmlCabinCategoryList[$index];
            $aXmlCabinCategoryAttr = $aXmlCabinCategory->attributes();
            $categoryCode = $aXmlCabinCategoryAttr->CategoryCode->__toString();
            $categoryName = $aXmlCabinCategoryAttr->Name->__toString();
            $cabinCategoryList[$categoryCode] = $categoryName;
        }

        //deck list
        $xmlDeckObjectList = $xmlShipObj->Decks->Deck;
        $deckList = array();
        $totalDeck = count($xmlDeckObjectList);
        for ($index = 0; $index < $totalDeck; $index++) {
            $anXmlDeckObj = $xmlDeckObjectList[$index];
            $anXmlDeckObjAttr = $anXmlDeckObj->attributes();

            $xmlDeckCategoryList = $anXmlDeckObj->DeckCategories->DeckCategory;
            $deckCategoryList = array();
            $totalDeckCategory = count($xmlDeckCategoryList);
            for ($jindex = 0; $jindex < $totalDeckCategory; $jindex++) {
                $xmlDeckCategoryObj = $xmlDeckCategoryList[$jindex];
                $anXmlDeckCategoryObjAttr = $xmlDeckCategoryObj->attributes();
                $categoryCode = $anXmlDeckCategoryObjAttr->CategoryCode->__toString();
                $deckCategoryList[] = array(
                    'image' => "http://image2.urlforimages.com/Cruises/{$shipLineCode}/CC/" . $categoryCode . ".gif",
                    'name' => $cabinCategoryList[$categoryCode]
                );
            }

            $deckNumber = $anXmlDeckObjAttr->Number->__toString();

            $deckPublicAreaList = array();
            foreach ($publicAreaList as $aPublicArea) {
                if ($aPublicArea['deck_no'] == $deckNumber)
                    $deckPublicAreaList[] = $aPublicArea;
            }

            $imageDeckNumber = $index + 1;
            if (strlen($imageDeckNumber) < 2)
                $imageDeckNumber = '0' . $imageDeckNumber;
            else
                $imageDeckNumber = $imageDeckNumber;

            $deckList[] = array(
                'deck_number' => $deckNumber,
                'deck_name' => $anXmlDeckObjAttr->Name->__toString(),
                'deck_image' => $anXmlDeckObjAttr->Img->__toString(),
                'location_image' => array(
                    'on' => "http://image2.urlforimages.com/Cruises/{$shipLineCode}/SHIPS/{$shipCode}/SI/{$imageDeckNumber}_on.gif",
                    'off' => "http://image2.urlforimages.com/Cruises/{$shipLineCode}/SHIPS/{$shipCode}/SI/{$imageDeckNumber}_off.gif"
                ),
                'deck_categroy' => $deckCategoryList,
                'public_area' => $deckPublicAreaList
            );
        }
        $shipDetails = array(
            'image_list' => $shipPhotoGallery,
            'aminities' => $aminityList,
            'deck_list' => $deckList
        );
        return $shipDetails;
    }

    function GetAminityType() {
        $aminityType = array(
            1 => 'Cabin Features',
            2 => 'Classes and Services',
            3 => 'Dining',
            4 => 'Recreation',
            5 => 'Ship Features'
        );
        return $aminityType;
    }

    function ProcessPortList() {
        $reponse = $this->cruiseAPICoreObj->GetAllPortList();
        //header("Content-type: application/xml");
        //echo $reponse;
        //exit(0);
        //$reponse = file_get_contents("port_list_res.xml");
        $smpleXmlObj = simplexml_load_string($reponse);
        $xmlPortListObj = $smpleXmlObj->children("http://schemas.xmlsoap.org/soap/envelope/")->Body->children("http://tourico.com/webservices/")->PortsResult->Ports->Port;
        $totalPort = count($xmlPortListObj);
        $portList = array();
        for ($index = 0; $index < $totalPort; $index++) {
            $xmlPortObj = $xmlPortListObj[$index];
            $xmlPortObjAttr = $xmlPortObj->attributes();
            $portList[] = array(
                'port_id' => $xmlPortObjAttr->Id->__toString(),
                'port_name' => $xmlPortObjAttr->Name->__toString(),
                'port_description' => $xmlPortObjAttr->Description->__toString(),
                'port_image' => $xmlPortObjAttr->PortImg->__toString(),
                'port_latitude' => $xmlPortObjAttr->Latitude->__toString(),
                'port_longitude' => $xmlPortObjAttr->Longitude->__toString(),
            );
        }
        return $portList;
    }

    function ProcessCabinCategory() {
        $response = $this->cruiseAPICoreObj->GetCabinCategory();
        header("Content-type: application/xml");
        //echo $response;
        // exit(0);
        //$response = file_get_contents('get_cabin_category.xml');
        $smpleXmlObj = simplexml_load_string($response);
        $xmlCabinCategoryObjList = $smpleXmlObj->children("http://schemas.xmlsoap.org/soap/envelope/")->Body->children("http://tourico.com/webservices/")->CabinCategoryResults->CabinCategoriesList->CabinCategory;
        $totalCabinCategory = count($xmlCabinCategoryObjList);
        $cabinCategoryList = array();
        for ($index = 0; $index < $totalCabinCategory; $index++) {
            $aXmlCabinCategoryObj = $xmlCabinCategoryObjList[$index];
            $aXmlCabinCategoryObjAttr = $aXmlCabinCategoryObj->attributes();

            $cabinCategoryCode = $aXmlCabinCategoryObjAttr->Code->__toString();
            $cabinCategoryName = $aXmlCabinCategoryObjAttr->Name->__toString();
            $cabinCategoryImg = $aXmlCabinCategoryObjAttr->Img->__toString();
            $cabinCategoryDescription = $aXmlCabinCategoryObjAttr->Description->__toString();

            $xmlPriceObjList = $aXmlCabinCategoryObj->Prices->CabinCategoryPrice;
            $priceList = array();
            $totalPrice = count($xmlPriceObjList);
            for ($jindex = 0; $jindex < $totalPrice; $jindex++) {
                $aXmlPriceObj = $xmlPriceObjList[$jindex];
                $aXmlPriceObjAttr = $aXmlPriceObj->attributes();

                $priceList[] = array(
                    'price_title' => $aXmlPriceObjAttr->PriceTitle->__toString(),
                    'price' => $aXmlPriceObjAttr->Price->__toString(),
                    'product_id' => $aXmlPriceObjAttr->ProductID->__toString(),
                    'taxes_included' => $aXmlPriceObjAttr->TaxesIncluded->__toString(),
                    'currency' => $aXmlPriceObjAttr->Currency->__toString()
                );
            }
            $cabinCategoryList[] = array(
                'cabin_category_code' => $cabinCategoryCode,
                'cabin_category_name' => $cabinCategoryName,
                'cabin_category_image' => $cabinCategoryImg,
                'cabin_category_description' => $cabinCategoryDescription,
                'price' => $priceList
            );
        }

        echo '<pre>';
        print_r($cabinCategoryList);
        echo '</pre>';
    }

    function GetCabin() {
        //$response = $this->cruiseAPICoreObj->GetCabin();
        //header("Content-type: application/xml");
        //echo $response;
        //exit(0);

        $response = file_get_contents('get_cabin.xml');
        $smpleXmlObj = simplexml_load_string($response);
        $xmlCabinObjList = $smpleXmlObj->children("http://schemas.xmlsoap.org/soap/envelope/")->Body->children("http://tourico.com/webservices/")->CabinResults->CabinsList->Cabin;
        $totalCabin = count($xmlCabinObjList);
        $cabinList = array();
        for ($index = 0; $index < $totalCabin; $index++) {
            $aCabinObj = $xmlCabinObjList[$index];
            $aCabinObjAttr = $aCabinObj->attributes();
            $cabinList[] = array(
                'cabin_number' => $aCabinObjAttr->CabinNumber->__toString(),
                'deck_number' => $aCabinObjAttr->DeckNumber->__toString(),
                'deck_name' => $aCabinObjAttr->DeckName->__toString(),
                'deck_image' => $aCabinObjAttr->DeckImg->__toString(),
                'deck_is_gurranted' => $aCabinObjAttr->IsGuaranteed->__toString()
            );
        }

        echo '<pre>';
        print_r($cabinList);
        echo '</pre>';
    }

}

class CruiseAPICore {

    private $CRUISE_SEARCH_API_USERNAME = "HDL103";
    private $CRUISE_SEARCH_API_PASSWORD = "111111";
    private $CRUISE_SEARCH_API_ENDPOINT = "http://demo-cruisews.touricoholidays.com/CruiseServiceFlow.svc/bas";
    private $GET_CRUISE_DESTINATION_LIST_API_ACTION = "http://tourico.com/webservices/ICruiseServiceFlow/GetCruiseDestinations";
    private $GET_CRUISE_LINE_LIST_API_ACTION = "http://tourico.com/webservices/ICruiseServiceFlow/GetCruiseLines";
    private $SEARCH_CRUISE_API_ACTION = "http://tourico.com/webservices/ICruiseServiceFlow/Step_1_SearchCruises";
    private $Get_SHIP_DETAIL_API_ACTION = "http://tourico.com/webservices/ICruiseServiceFlow/GetShipDetails";
    private $Get_PORT_LIST_API_ACTION = "http://tourico.com/webservices/ICruiseServiceFlow/GetPortsAll";
    private $GET_CABIN_CATEGORY_ACTION = "http://tourico.com/webservices/ICruiseServiceFlow/Step_2_SelectCabinCategory";
    private $GET_CABIN_ACTION = "http://tourico.com/webservices/ICruiseServiceFlow/Step_3_SelectCabin";

    function GetCruiseDestinationList() {
        $url = "http://demo-cruisews.touricoholidays.com/CruiseServiceFlow.svc/bas";
        $actionUrl = 'http://tourico.com/webservices/ICruiseServiceFlow/GetCruiseDestinations';
        $request = <<<EOM
    <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://tourico.com/webservices/">
       <soapenv:Header>
          <web:LoginHeader>
             <web:UserName>{$this->CRUISE_SEARCH_API_USERNAME}</web:UserName>
             <web:Password>{$this->CRUISE_SEARCH_API_PASSWORD}</web:Password>
             <web:Culture>en_US</web:Culture>
             <web:Version>9</web:Version>
          </web:LoginHeader>
       </soapenv:Header>
       <soapenv:Body>
          <web:GetCruiseDestinations/>
       </soapenv:Body>
    </soapenv:Envelope>
EOM;
        $response = $this->SendPostRequestToTourico($this->CRUISE_SEARCH_API_ENDPOINT, $request, $this->GET_CRUISE_DESTINATION_LIST_API_ACTION);
        return $response;
    }

    function GetCruiseLineList() {
        $request = <<<EOM
    <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://tourico.com/webservices/">
       <soapenv:Header>
          <web:LoginHeader>
             <web:UserName>{$this->CRUISE_SEARCH_API_USERNAME}</web:UserName>
             <web:Password>{$this->CRUISE_SEARCH_API_PASSWORD}</web:Password>
             <web:Culture>en_US</web:Culture>
             <web:Version>9</web:Version>
          </web:LoginHeader>
       </soapenv:Header>
       <soapenv:Body>
          <web:GetCruiseLines/>
       </soapenv:Body>
    </soapenv:Envelope>
EOM;
        $response = $this->SendPostRequestToTourico($this->CRUISE_SEARCH_API_ENDPOINT, $request, $this->GET_CRUISE_LINE_LIST_API_ACTION);
        return $response;
    }

    function SearchCruise($cruiseDestinationId, $departingFrom, $departingTo, $minCruiseLength, $maxCruiseLength) {
        $request = <<<EOM
    <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://tourico.com/webservices/">
       <soapenv:Header>
          <web:LoginHeader>
             <web:UserName>{$this->CRUISE_SEARCH_API_USERNAME}</web:UserName>
             <web:Password>{$this->CRUISE_SEARCH_API_PASSWORD}</web:Password>
             <web:Culture>en_US</web:Culture>
             <web:Version>9</web:Version>
          </web:LoginHeader>
       </soapenv:Header>
       <soapenv:Body>
          <web:Step_1_SearchCruises>
             <web:SearchCruiseParamsList>
                <web:CruiseLineID>0</web:CruiseLineID>
                <web:CruiseDestinationID>{$cruiseDestinationId}</web:CruiseDestinationID>
                <web:MarketCode>US</web:MarketCode>
                <web:Currency>USD</web:Currency>
                <web:DepartingFrom>{$departingFrom}</web:DepartingFrom>
                <web:DepartingTo>{$departingTo}</web:DepartingTo>
                <web:MinCruiseLength>{$minCruiseLength}</web:MinCruiseLength>
                <web:MaxCruiseLength>{$maxCruiseLength}</web:MaxCruiseLength>
                <web:PortID>0</web:PortID>
                <web:ShipID>0</web:ShipID>
             </web:SearchCruiseParamsList>
          </web:Step_1_SearchCruises>
       </soapenv:Body>
    </soapenv:Envelope>
EOM;
        $response = $this->SendPostRequestToTourico($this->CRUISE_SEARCH_API_ENDPOINT, $request, $this->SEARCH_CRUISE_API_ACTION);
        //header("content-type: application/xml");
        //echo $response;
        //exit(0);
        return $response;
    }

    function GetShipDetails($shipId) {
        $request = <<<EOM
    <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://tourico.com/webservices/">
       <soapenv:Header>
          <web:LoginHeader>
             <web:UserName>{$this->CRUISE_SEARCH_API_USERNAME}</web:UserName>
             <web:Password>{$this->CRUISE_SEARCH_API_PASSWORD}</web:Password>
             <web:Culture>en_US</web:Culture>
             <web:Version>9</web:Version>
          </web:LoginHeader>
       </soapenv:Header>
       <soapenv:Body>
          <web:GetShipDetails>
             <web:ShipID>{$shipId}</web:ShipID>
          </web:GetShipDetails>
       </soapenv:Body>
    </soapenv:Envelope>
EOM;
        $response = $this->SendPostRequestToTourico($this->CRUISE_SEARCH_API_ENDPOINT, $request, $this->Get_SHIP_DETAIL_API_ACTION);
        return $response;
    }

    function GetAllPortList() {
        $request = <<<EOM
    <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://tourico.com/webservices/">
       <soapenv:Header>
          <web:LoginHeader>
             <web:UserName>{$this->CRUISE_SEARCH_API_USERNAME}</web:UserName>
             <web:Password>{$this->CRUISE_SEARCH_API_PASSWORD}</web:Password>
             <web:Culture>en_US</web:Culture>
             <web:Version>9</web:Version>
          </web:LoginHeader>
       </soapenv:Header>
       <soapenv:Body>
          <web:GetPortsAll/>
       </soapenv:Body>
    </soapenv:Envelope>
EOM;
        $response = $this->SendPostRequestToTourico($this->CRUISE_SEARCH_API_ENDPOINT, $request, $this->Get_PORT_LIST_API_ACTION);
        return $response;
    }

    function GetCabinCategory($cruiseLineId = "1030965", $cruiseDestinationId = "18", $salingId = "112082", $adultNum = "2", $childNum = "1", $ageList = array("22", "22", "4")) {
        $ageXml = "";
        foreach ($ageList as $anAge)
            $ageXml .= '<web:Age GuestAge="' . $anAge . '"/>';
        $request = <<<EOM
    <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://tourico.com/webservices/">
       <soapenv:Header>
          <web:LoginHeader>
             <web:UserName>{$this->CRUISE_SEARCH_API_USERNAME}</web:UserName>
             <web:Password>{$this->CRUISE_SEARCH_API_PASSWORD}</web:Password>
             <web:Culture>en_US</web:Culture>
             <web:Version>9</web:Version>
          </web:LoginHeader>
       </soapenv:Header>
       <soapenv:Body>
          <web:Step_2_SelectCabinCategory>
             <web:CruiseLineID>{$cruiseLineId}</web:CruiseLineID>
             <web:CruiseDestinationID>{$cruiseDestinationId}</web:CruiseDestinationID>
             <web:SailingID>{$salingId}</web:SailingID>
             <web:MarketCode>US</web:MarketCode>
             <web:Currency>USD</web:Currency>
             <web:AdultsNum>{$adultNum}</web:AdultsNum>
             <web:ChildNum>{$childNum}</web:ChildNum>
             <web:Ages>
                {$ageXml}
             </web:Ages>
             <web:ResponseType>1</web:ResponseType>
          </web:Step_2_SelectCabinCategory>
       </soapenv:Body>
    </soapenv:Envelope>
EOM;
        $response = $this->SendPostRequestToTourico($this->CRUISE_SEARCH_API_ENDPOINT, $request, $this->GET_CABIN_CATEGORY_ACTION);
        return $response;
    }

    function GetCabin($cruiseLineId = "1030965", $sailingId = "112082", $productId = "i0FPll/tSOR8JCijDBcRRwvrLgPBkkow7f/Z8iUVsF6qxLlpLRMoBEcjVMq+dHwNdYBhTsjUTWArt6vyMuUlaUCsGtTdSceOgTn5bpNCfFSymcxCWsTM6neZ7DwcHAUh8JKlls0K5ek=", $adultNum = "2", $childNum = "1", $ageList = array("22", "22", "4")) {
        $ageXml = "";
        foreach ($ageList as $anAge)
            $ageXml .= '<web:Age GuestAge="' . $anAge . '"/>';

        $request = <<<EOM
            <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://tourico.com/webservices/">
       <soapenv:Header>
          <web:LoginHeader>
             <web:UserName>{$this->CRUISE_SEARCH_API_USERNAME}</web:UserName>
             <web:Password>{$this->CRUISE_SEARCH_API_PASSWORD}</web:Password>
             <web:Culture>en_US</web:Culture>
             <web:Version>9</web:Version>
          </web:LoginHeader>
       </soapenv:Header>
       <soapenv:Body>
          <web:Step_3_SelectCabin>
             <web:CruiseLineID>{$cruiseLineId}</web:CruiseLineID>
             <web:SailingID>{$sailingId}</web:SailingID>
             <web:ProductID>{$productId}</web:ProductID>
             <web:ResponseType>1</web:ResponseType>
             <web:AdultsNum>{$adultNum}</web:AdultsNum>
             <web:ChildNum>{$childNum}</web:ChildNum>
             <web:Currency>USD</web:Currency>
             <web:Ages>
                {$ageXml}
             </web:Ages>
          </web:Step_3_SelectCabin>
       </soapenv:Body>
    </soapenv:Envelope>
EOM;
        $response = $this->SendPostRequestToTourico($this->CRUISE_SEARCH_API_ENDPOINT, $request, $this->GET_CABIN_ACTION);
        return $response;
    }

    function SendPostRequestToTourico($url, $request, $actionUri) {
        $ch = curl_init($url);
        $options = array(
            CURLOPT_RETURNTRANSFER => true, // return web page
            CURLOPT_HEADER => false, // don't return headers
            CURLOPT_FOLLOWLOCATION => false, // follow redirects
            CURLOPT_ENCODING => "utf-8", // handle all encodings
            CURLOPT_AUTOREFERER => true, // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 300, // timeout on connect
            CURLOPT_TIMEOUT => 300, // timeout on response
            CURLOPT_POST => 1, // i am sending post data
            CURLOPT_POSTFIELDS => $request, // this are my post vars
            CURLOPT_SSL_VERIFYHOST => 0, // don't verify ssl
            CURLOPT_SSL_VERIFYPEER => false, //
            CURLOPT_VERBOSE => 1,
            CURLOPT_HTTPHEADER => array(
                "Content-type: text/xml;charset=\"utf-8\"",
                "Accept: text/xml",
                "Cache-Control: no-cache",
                'Accept-Encoding: gzip',
                //"Pragma: no-cache",
                "SOAPAction: " . $actionUri,
                //"Content-length: ".strlen($request)
            )
        );
        curl_setopt_array($ch, $options);
        $data = curl_exec($ch);
        $curl_errno = curl_errno($ch);
        $curl_error = curl_error($ch);
        //echo $curl_errno;
        //echo $curl_error;
        curl_close($ch);
        return $data;
    }

}

class ReservationAPIProcess {

    private $resevationAPICoreObj;

    function __construct() {
        $this->resevationAPICoreObj = new ResevationAPICore();
    }
    function clean_object($obj) {
        return !empty($obj) ? $obj->__toString() : '';
    }
    function GetCancellationPoliciesBeforeReservation($hotelId, $roomTypeId, $checkIn, $checkOut)
    {
        $response = $this->resevationAPICoreObj->GetCancellationPoliciesBeforeReservation($hotelId, $roomTypeId, $checkIn, $checkOut);
        file_put_contents("cancellation_policies.xml" , $response);
        $simpleXMLELementObj = simplexml_load_string($response);

        //var_dump($response);var_dump($simpleXMLELementObj);exit;
        $xmlCancelPolicyList = $simpleXMLELementObj->children('http://schemas.xmlsoap.org/soap/envelope/')->Body->children('http://tourico.com/webservices/')->GetCancellationPoliciesResponse->children()->HotelPolicy->RoomTypePolicy->CancelPolicy;
        $xmlCancelPolicyList = $xmlCancelPolicyList->CancelPenalty;
        $cancellationPolicy = array();
        $totalCancellationPolicity = count($xmlCancelPolicyList);
        for ($index = 0; $index < $totalCancellationPolicity; $index++)
        {
            $message = "";
            $xmlCancellationPOlicityDeadlineObj = $xmlCancelPolicyList[$index]->Deadline;
            $xmlCancellationPOlicityDeadlineObjAttr = $xmlCancellationPOlicityDeadlineObj->attributes();
            $offsetDropTime = $xmlCancellationPOlicityDeadlineObjAttr->OffsetDropTime->__toString();
            $offsetUnitMultiplier = $xmlCancellationPOlicityDeadlineObjAttr->OffsetUnitMultiplier->__toString();
            $offsetTimeUnit = $xmlCancellationPOlicityDeadlineObjAttr->OffsetTimeUnit->__toString();
            $amountPercentObj = $xmlCancelPolicyList[$index]->AmountPercent;
            $amoutnPercentArr = $amountPercentObj->attributes();
            $basisType = $amoutnPercentArr->BasisType->__toString();
            $percent = $this->clean_object($amoutnPercentArr->Percent);
            if ($offsetDropTime == "BeforeArrival") {
                if ($offsetUnitMultiplier == "0")
                {
                    $message .= "The cancellation penalty for No Show is ";
                }
                else
                {
                    $offsetUnitMultiplier = $offsetUnitMultiplier + HOURS_ADDITION_TO_TOURICO_POLICY;
                    $message .= "The cancellation rule is <b>" . $offsetUnitMultiplier . "  " . strtolower($offsetTimeUnit) . "s</b> prior to the reservation check-in date (hotel’s own time zone) and cancellation penalty is the ";
                }
            }
            else if ($offsetDropTime == "AfterBooking")
            {
                $message .= "This cancellation rule is effective immediately after the booking is made. ";
            }

            if ($basisType == "FullStay")
            {
                if (isset($amoutnPercentArr->Amount))
                {
                    $amount = $amoutnPercentArr->Amount->__toString();
                    $currencyCode = $amoutnPercentArr->CurrencyCode->__toString();
                    $message .= "and cancellation penalty is fixed at the amount of " . $amount . " (" . $currencyCode . ").";
                }
                else
                {
                    $message .= " <b>total stay amount</b> of the reservation.";
                }
            }

            $xmlCancellationPOlicityAmountObj = $xmlCancelPolicyList[$index]->AmountPercent;
            $xmlCancellationPOlicityAmountObjAttr = $xmlCancellationPOlicityAmountObj->attributes();

            $basisType = $xmlCancellationPOlicityAmountObjAttr->BasisType->__toString();
            if ($basisType == 'Nights')
            {
                $percentagePenalty = $xmlCancellationPOlicityAmountObjAttr->NmbrOfNights->__toString();
                if($xmlCancellationPOlicityDeadlineObjAttr->OffsetUnitMultiplier->__toString() == "0")
                {
                    $message .= '  <b>'.$percentagePenalty.' night stay</b>.';
                }
                else
                {
                    $message .= ' <b>'.$percentagePenalty.' night stay</b>. ';
                }
            }
            else if ($basisType == 'FullStay')
            {
                $percentagePenalty = $xmlCancellationPOlicityAmountObjAttr->Percent->__toString();
            }
            $cancellationPolicy[] = array(
                'offset_time' => $xmlCancellationPOlicityDeadlineObjAttr->OffsetUnitMultiplier->__toString() > 0 ? $xmlCancellationPOlicityDeadlineObjAttr->OffsetUnitMultiplier->__toString() + HOURS_ADDITION_TO_TOURICO_POLICY : $xmlCancellationPOlicityDeadlineObjAttr->OffsetUnitMultiplier->__toString(),
                'offeset_time_unit' => $xmlCancellationPOlicityDeadlineObjAttr->OffsetTimeUnit->__toString(),
                'percentage_penalty' => $percentagePenalty,
                'basis_type' => $basisType,
                'message' => $message
            );
        }
        return $cancellationPolicy;
    }

    function GetCancellationPolicyAfterReseration($reservationId) {
        $response = $this->resevationAPICoreObj->GetCancellationPolicyAfterReseration($reservationId);
        $simpleXMLELementObj = simplexml_load_string($response);
        $xmlCancelPolicyList = $simpleXMLELementObj->children('http://schemas.xmlsoap.org/soap/envelope/')->Body->children('http://tourico.com/webservices/')->GetCancellationPoliciesResponse->children()->HotelPolicy->RoomTypePolicy->CancelPolicy->CancelPenalty;

        $cancellationPolicy = array();
        $totalCancellationPolicity = count($xmlCancelPolicyList);
        for ($index = 0; $index < $totalCancellationPolicity; $index++) {
            $xmlCancellationPOlicityDeadlineObj = $xmlCancelPolicyList[$index]->Deadline;
            $xmlCancellationPOlicityDeadlineObjAttr = $xmlCancellationPOlicityDeadlineObj->attributes();

            $xmlCancellationPOlicityAmountObj = $xmlCancelPolicyList[$index]->AmountPercent;
            $xmlCancellationPOlicityAmountObjAttr = $xmlCancellationPOlicityAmountObj->attributes();

            $basisType = $xmlCancellationPOlicityAmountObjAttr->BasisType->__toString();
            if ($basisType == 'Nights')
                $percentagePenalty = $xmlCancellationPOlicityAmountObjAttr->NmbrOfNights->__toString();
            else if ($basisType == 'FullStay')
                $percentagePenalty = $xmlCancellationPOlicityAmountObjAttr->Percent->__toString();

            $cancellationPolicy[] = array(
                'offset_time' => $xmlCancellationPOlicityDeadlineObjAttr->OffsetUnitMultiplier->__toString(),
                'offeset_time_unit' => $xmlCancellationPOlicityDeadlineObjAttr->OffsetTimeUnit->__toString(),
                'percentage_penalty' => $percentagePenalty,
                'basis_type' => $basisType
            );
        }
        return $cancellationPolicy;
    }

    function GetCancellationFees($reservationId, $date = '') {
        if (strlen($date) <= 0)
            $date = date('Y-m-d');
        $response = $this->resevationAPICoreObj->GetCancellationFees($reservationId, $date);
        //header('Content-type: application/xml');
        //echo $response;
        //exit(0);

        $simpleXMLELementObj = simplexml_load_string($response);
        $xmlCancellationFeeObj = $simpleXMLELementObj->children('http://schemas.xmlsoap.org/soap/envelope/')->Body->children('http://tourico.com/webservices/')->GetCancellationFeeResponse->GetCancellationFeeResult;

        $returnArray = array(
            'cancellation_fee' => $xmlCancellationFeeObj->CancellationFeeValue->__toString(),
            'cancellation_fee_currency' => $xmlCancellationFeeObj->Currency->__toString(),
        );
        var_dump($returnArray);
    }

    function CancellBooking($reservationId) {
        //$response = $this->resevationAPICoreObj->CancellBooking($reservationId);
        //header('Content-type: application/xml');
        //echo $response;
        //exit(0);
        $response = file_get_contents('cancell_api_xml.xml');
        $simpleXMLELementObj = simplexml_load_string($response);
        $cancellationSuccess = $simpleXMLELementObj->children('http://schemas.xmlsoap.org/soap/envelope/')->Body->children('http://tourico.com/webservices/')->CancelReservationResponse->CancelReservationResult->__toString();
        var_dump($cancellationSuccess);
    }

    function GetPriceAndAvail($hotel_id,$roomsInfo,$checkIn,$checkOut,$starLevel,$maxPrice,$roomdID,$roomTypeID)
    {
        $returnResult = array(
            'error' => 0,
            'message' => 'success',
        );
        $response = $this->resevationAPICoreObj->CheckAvailPriceBeforeReserve($hotel_id,$roomsInfo,$checkIn,$checkOut,$starLevel =0,$maxPrice =0);
        $simpleXMLELementObj = simplexml_load_string($response);
        $getPriceAvailRes = $simpleXMLELementObj->children('http://schemas.xmlsoap.org/soap/envelope/')->Body->children('http://tourico.com/webservices/hotelv3')->CheckAvailabilityAndPricesResponse->CheckAvailabilityAndPricesResult->children()->HotelList->Hotel;
        file_put_contents("checkAvailAndPrice.xml", $response);
        $totalRoomTypes =  count($getPriceAvailRes->RoomTypes->RoomType);
        if($totalRoomTypes == 0)
        {
            $returnResult = array(
                'error' => 1,
                'message' => 'not_avail',
            );
        }
        for($totalRoomTypeIndex = 0; $totalRoomTypeIndex < $totalRoomTypes; $totalRoomTypeIndex++)
        {
            $roomeTypeObj = $getPriceAvailRes->RoomTypes->RoomType[$totalRoomTypeIndex];
            if($roomeTypeObj->attributes()->roomId == $roomdID && $roomeTypeObj->attributes()->hotelRoomTypeId == $roomTypeID)
            {
                $availabilityObj = $roomeTypeObj->AvailabilityBreakdown->Availability;
                $totalAvailablity =  count($availabilityObj);
                for($totalAvailablityIndex = 0;$totalAvailablityIndex < $totalAvailablity; $totalAvailablityIndex++)
                {
                    if($availabilityObj[$totalAvailablityIndex]->attributes()->status != true)
                    {
                        $returnResult['error'] = 1;
                        $returnResult['message'] = 'not_avail';
                    }
                }
            }
        }
        return $returnResult;
    }

    function HotelBookProcess($hotelId,$roomTypeId,$checkIn,$checkOut,$roomInfoStr,$contactInfo,$price,$email){
        $fromDateObj = DateTime::createFromFormat('m/d/Y', $checkIn);
        if ($fromDateObj != false)
        {
            $checkIn = $fromDateObj->format('Y-m-d');
        }
        $fromDateObj = DateTime::createFromFormat('m/d/Y', $checkOut);
        if ($fromDateObj != false)
        {
            $checkOut = $fromDateObj->format('Y-m-d');
        }
        $response = $this->resevationAPICoreObj->BookHotel($hotelId,$roomTypeId,$checkIn,$checkOut,$roomInfoStr,$contactInfo,$price,$email);
        $simpleXMLELementObj = simplexml_load_string($response);
        $hotelBookResponse   = $simpleXMLELementObj->children('http://schemas.xmlsoap.org/soap/envelope/')->Body->children('http://tourico.com/webservices/hotelv3/');
        file_put_contents("hotel_book_response.xml", $response);
        $response = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $response);
        $xml = new SimpleXMLElement($response);
        $body = $xml->xpath('//sBody')[0];
        $array = json_decode(json_encode((array)$body), TRUE);
        return $array;
    }

}

class ResevationAPICore {

    private $RESERVATION_API_USERNAME = "HDL103";
    private $RESERVATION_API_PASSWORD = "111111";
    private $RESERVATION_API_ENDPOINT = "http://demo-hotelws.touricoholidays.com/HotelFlow.svc/bas";
    private $GET_HOTEL_CANCELLATION_POLICIES_ACTION = "http://tourico.com/webservices/hotelv3/IHotelFlow/GetCancellationPolicies";
    //private $GET_HOTEL_CANCELLATION_FEE_ACTION = "http://tourico.com/webservices/GetCancellationFee";
    private $GET_HOTEL_CANCELLATION_FEE_ACTION = "http://tourico.com/webservices/hotelv3/IHotelFlow/GetCancellationFee";
    //private $CANCEL_RESERVATION_API_ACTION = "http://tourico.com/webservices/hotelv3/IHotelFlow/CancelReservation";
    private $CANCEL_RESERVATION_API_ENDPOINT = "http://demo-wsnew.touricoholidays.com/ReservationsService.asmx";
    private $CANCEL_RESERVATION_API_ACTION = "http://tourico.com/webservices/CancelReservation";
    //edit by tayyab
    private $HOTEL_BOOK_API_ENDPOINT        = "http://demo-hotelws.touricoholidays.com/HotelFlow.svc/bas";
    private $HOTEL_BOOK_API_ACTION          = "http://tourico.com/webservices/hotelv3/IHotelFlow/BookHotelV3";
    private $CHECK_AVAIL_AND_PRICE_ACTION   = "http://tourico.com/webservices/hotelv3/IHotelFlow/CheckAvailabilityAndPrices";

    function GetCancellationPoliciesBeforeReservation($hotelId, $roomTypeId, $checkIn, $checkOut)
    {
        $request = <<<EOM
        <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://tourico.com/webservices/hotelv3" xmlns:trav="http://schemas.tourico.com/webservices/authentication">
       <soapenv:Header>
          <trav:AuthenticationHeader>
             <trav:LoginName>{$this->RESERVATION_API_USERNAME}</trav:LoginName>
             <trav:Password>{$this->RESERVATION_API_PASSWORD}</trav:Password>
             <trav:Culture>en_US</trav:Culture>
             <trav:Version>7.123</trav:Version>
          </trav:AuthenticationHeader>
       </soapenv:Header>
       <soapenv:Body>
          <web:GetCancellationPolicies>
             <web:hotelId>{$hotelId}</web:hotelId>
             <web:hotelRoomTypeId>{$roomTypeId}</web:hotelRoomTypeId>
             <web:dtCheckIn>{$checkIn}</web:dtCheckIn>
             <web:dtCheckOut>{$checkOut}</web:dtCheckOut>
          </web:GetCancellationPolicies>
       </soapenv:Body>
    </soapenv:Envelope>
EOM;
        $response = $this->SendPostRequestToTourico($this->RESERVATION_API_ENDPOINT, $request, $this->GET_HOTEL_CANCELLATION_POLICIES_ACTION);
        /*echo '<pre>';print_r($request);
        echo '<pre>';print_r($response);exit;*/

        return $response;
    }



    function GetCancellationPolicyAfterReseration($reservationId)
    {

        $request = <<<EOM
    <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://tourico.com/webservices/" xmlns:trav="http://tourico.com/travelservices/">
       <soapenv:Header>
          <web:LoginHeader>
             <trav:username>{$this->RESERVATION_API_USERNAME}</trav:username>
             <trav:password>{$this->RESERVATION_API_PASSWORD}</trav:password>
             <trav:culture>en_US</trav:culture>
             <trav:version>7.123</trav:version>
          </web:LoginHeader>
       </soapenv:Header>
       <soapenv:Body>
          <web:GetCancellationPolicies>
             <web:nResID>{$reservationId}</web:nResID>
          </web:GetCancellationPolicies>
       </soapenv:Body>
    </soapenv:Envelope>
EOM;

        //echo '<pre>';print_r($request);
        $response = $this->SendPostRequestToTourico($this->RESERVATION_API_ENDPOINT, $request, $this->GET_HOTEL_CANCELLATION_POLICIES_ACTION);
        //echo '<pre>';print_r($response);exit;
        file_put_contents("book-cancelplicy-response.xml", $response);
        $response = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $response);
        $xml = new SimpleXMLElement($response);

        $body = $xml->xpath('//soapBody')[0];

        $array = json_decode(json_encode((array)$body), TRUE);

        return $array;

    }


    function GetCancellationFees($reservationId, $date) {
        $request = <<<EOM
         <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:aut="http://schemas.tourico.com/webservices/authentication" xmlns:hot="http://tourico.com/webservices/hotelv3">
   <soapenv:Header>
      <aut:AuthenticationHeader>
         <aut:LoginName>{$this->RESERVATION_API_USERNAME}</aut:LoginName>
         <aut:Password>{$this->RESERVATION_API_PASSWORD}</aut:Password>
         <aut:Culture>en_US</aut:Culture>
         <aut:Version>7.123</aut:Version>
      </aut:AuthenticationHeader>
   </soapenv:Header>
   <soapenv:Body>
      <hot:GetCancellationFee>
         <hot:nResID>{$reservationId}</hot:nResID>
         <hot:clxDate>{$date}</hot:clxDate>
      </hot:GetCancellationFee>
   </soapenv:Body>
</soapenv:Envelope>
EOM;
        /*<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://tourico.com/webservices/" xmlns:trav="http://tourico.com/travelservices/">
       <soapenv:Header>
          <web:LoginHeader>
             <trav:username>{$this->RESERVATION_API_USERNAME}</trav:username>
             <trav:password>{$this->RESERVATION_API_PASSWORD}</trav:password>
             <trav:culture>en_US</trav:culture>
             <trav:version>7.123</trav:version>
          </web:LoginHeader>
       </soapenv:Header>
       <soapenv:Body>
          <web:GetCancellationFee>
             <web:nResID>{$reservationId}</web:nResID>
             <web:clxDate>{$date}</web:clxDate>
          </web:GetCancellationFee>
       </soapenv:Body>
    </soapenv:Envelope>*/
        //echo '<pre>';print_r($request);
        $response = $this->SendPostRequestToTourico($this->RESERVATION_API_ENDPOINT, $request, $this->GET_HOTEL_CANCELLATION_FEE_ACTION);
        //echo '<pre>';print_r($response);exit;
        file_put_contents("get-cancellation-fee.xml", $response);
        return $response;
    }

    function CancellBooking($reservationId) {
        $request = <<<EOM
    <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://tourico.com/webservices/" xmlns:trav="http://tourico.com/travelservices/">
       <soapenv:Header>
          <web:LoginHeader>
             <trav:username>{$this->RESERVATION_API_USERNAME}</trav:username>
             <trav:password>{$this->RESERVATION_API_PASSWORD}</trav:password>
             <trav:culture>en_US</trav:culture>
             <trav:version>7.123</trav:version>
          </web:LoginHeader>
       </soapenv:Header>
       <soapenv:Body>
          <web:CancelReservation>
             <web:nResID>{$reservationId}</web:nResID>
          </web:CancelReservation>
       </soapenv:Body>
    </soapenv:Envelope>
EOM;
        //echo '<pre>';print_r($request);
        $response = $this->SendPostRequestToTourico($this->CANCEL_RESERVATION_API_ENDPOINT, $request, $this->CANCEL_RESERVATION_API_ACTION);
        //echo '<pre>';print_r($response);exit;
        $response = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $response);
        //echo '<pre>';print_r($response);exit;
        $xml = new SimpleXMLElement($response);
        $body = $xml->xpath('//soapBody')[0];
        $array = json_decode(json_encode((array)$body), TRUE);
        return $array;
    }

    function BookHotel($hotelId,$roomTypeId,$checkIn,$checkOut,$roomInfoStr,$contactInfo,$price,$email) {
        $delta = $price * 1/100;
        $request = <<<EOM
   <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:aut="http://schemas.tourico.com/webservices/authentication" xmlns:hot="http://tourico.com/webservices/hotelv3" xmlns:hot1="http://schemas.tourico.com/webservices/hotelv3">
   <soapenv:Header>
      <aut:AuthenticationHeader>
         <aut:LoginName>{$this->RESERVATION_API_USERNAME}</aut:LoginName>
         <aut:Password>{$this->RESERVATION_API_PASSWORD}</aut:Password>
         <aut:Culture>en_US</aut:Culture>
         <aut:Version>7.123</aut:Version>
      </aut:AuthenticationHeader>
   </soapenv:Header>
   <soapenv:Body>
      <hot:BookHotelV3>
         <hot:request>
            <hot1:RecordLocatorId>0</hot1:RecordLocatorId>
            <hot1:HotelId>{$hotelId}</hot1:HotelId>
            <hot1:HotelRoomTypeId>{$roomTypeId}</hot1:HotelRoomTypeId>
            <hot1:CheckIn>{$checkIn}</hot1:CheckIn>
            <hot1:CheckOut>{$checkOut}</hot1:CheckOut>
            <hot1:RoomsInfo>
               {$roomInfoStr}
            </hot1:RoomsInfo>
            <hot1:PaymentType>Obligo</hot1:PaymentType>
            <hot1:AgentRefNumber>123NA</hot1:AgentRefNumber>
            <hot1:ContactInfo>{$contactInfo}</hot1:ContactInfo>
            <hot1:RequestedPrice>{$price}</hot1:RequestedPrice>
            <hot1:DeltaPrice>{$delta}</hot1:DeltaPrice>
            <hot1:Currency>USD</hot1:Currency>
            <hot1:IsOnlyAvailable>false</hot1:IsOnlyAvailable>
            <hot1:ConfirmationEmail>{$email}</hot1:ConfirmationEmail>
            <hot1:ConfirmationLogo>logo.gif</hot1:ConfirmationLogo>
         </hot:request>
      </hot:BookHotelV3>
   </soapenv:Body>
    </soapenv:Envelope>
EOM;
        file_put_contents("book-hotel-request.xml", $request);
        $response = $this->SendPostRequestToTourico($this->HOTEL_BOOK_API_ENDPOINT, $request, $this->HOTEL_BOOK_API_ACTION);
        return $response;
    }

    function CheckAvailPriceBeforeReserve($hotelId,$roomsInfo,$checkIn,$checkOut,$starLevel,$maxPrice)
    {
        $fromDateObj = DateTime::createFromFormat('m/d/Y', $checkIn);
        if ($fromDateObj != false) {
            $check_in_date = $fromDateObj->format('Y-m-d');
        }
        $fromDateObj = DateTime::createFromFormat('m/d/Y', $checkOut);
        if ($fromDateObj != false) {
            $check_out_date = $fromDateObj->format('Y-m-d');
        }
        $request = <<<EOM
    <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:aut="http://schemas.tourico.com/webservices/authentication" xmlns:hot="http://tourico.com/webservices/hotelv3" xmlns:hot1="http://schemas.tourico.com/webservices/hotelv3">
   <soapenv:Header>
      <aut:AuthenticationHeader>
         <aut:LoginName>{$this->RESERVATION_API_USERNAME}</aut:LoginName>
         <aut:Password>{$this->RESERVATION_API_PASSWORD}</aut:Password>
         <aut:Culture>en_US</aut:Culture>
         <aut:Version>7.123</aut:Version>
      </aut:AuthenticationHeader>
   </soapenv:Header>
    <soapenv:Body>
      <hot:CheckAvailabilityAndPrices>
         <hot:request>
            <hot1:HotelIdsInfo>
               <hot1:HotelIdInfo id="{$hotelId}"/>
            </hot1:HotelIdsInfo>
            <hot1:CheckIn>{$check_in_date}</hot1:CheckIn>
            <hot1:CheckOut>{$check_out_date}</hot1:CheckOut>
            <hot1:RoomsInformation>
               {$roomsInfo}
            </hot1:RoomsInformation>
            <hot1:MaxPrice>{$maxPrice}</hot1:MaxPrice>
            <hot1:StarLevel>{$starLevel}</hot1:StarLevel>
            <hot1:AvailableOnly>true</hot1:AvailableOnly>
         </hot:request>
      </hot:CheckAvailabilityAndPrices>
   </soapenv:Body>
</soapenv:Envelope>
EOM;
        file_put_contents("CheckAvailPriceBeforeReserve_request.xml", $request);
        $response = $this->SendPostRequestToTourico($this->HOTEL_BOOK_API_ENDPOINT, $request, $this->CHECK_AVAIL_AND_PRICE_ACTION);
        file_put_contents("CheckAvailPriceBeforeReserve.xml", $response);
        return $response;
    }

    function SendPostRequestToTourico($url, $request, $actionUri) {
        $ch = curl_init($url);
        $options = array(
            CURLOPT_RETURNTRANSFER => true, // return web page
            CURLOPT_HEADER => false, // don't return headers
            CURLOPT_FOLLOWLOCATION => false, // follow redirects
            CURLOPT_ENCODING => "utf-8", // handle all encodings
            CURLOPT_AUTOREFERER => true, // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 300, // timeout on connect
            CURLOPT_TIMEOUT => 300, // timeout on response
            CURLOPT_POST => 1, // i am sending post data
            CURLOPT_POSTFIELDS => $request, // this are my post vars
            CURLOPT_SSL_VERIFYHOST => 0, // don't verify ssl
            CURLOPT_SSL_VERIFYPEER => false, //
            CURLOPT_VERBOSE => 1,
            CURLOPT_HTTPHEADER => array(
                "Content-type: text/xml;charset=\"utf-8\"",
                "Accept: text/xml",
                "Cache-Control: no-cache",
                'Accept-Encoding: gzip',
                //"Pragma: no-cache",
                "SOAPAction: " . $actionUri,
                //"Content-length: ".strlen($request)
            )
        );
        curl_setopt_array($ch, $options);
        $data = curl_exec($ch);
        $curl_errno = curl_errno($ch);
        $curl_error = curl_error($ch);
        //echo $curl_errno;
        //echo $curl_error;
        curl_close($ch);
        return $data;
    }

}

?>
