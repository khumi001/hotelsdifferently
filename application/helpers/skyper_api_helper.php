<?php
Class IataAPIResponseProcess {
    function __construct() {

    }

    function getAirLineName($code)
    {
        $urlData = array(
            "api_key" => 'caf8723c-f976-47d9-8d34-3677c6b03d2f',
            "code" => $code
        );
        $url = http_build_query($urlData);
        $url = "https://iatacodes.org/api/v6/airlines?".$url;
        $jsonData = $this->sendRequest($url);
        $jsonData = json_decode($jsonData);
        return $jsonData->response;
    }

    function sendRequest($url)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url,
            CURLOPT_USERAGENT => 'CurlRequest'
        ));
        $resp = curl_exec($curl);
        curl_close($curl);
        return $resp;
    }
};
Class FlightSearchAPIResponseProcess {
    private $IataAPIResponseProcess;
    function __construct() {
        $this->IataAPIResponseProcess = new IataAPIResponseProcess();
    }

    function getLocalAirLineName()
    {
        $file_path = base_url().'public/airlines.json';
        if( !file_exists( $_SERVER['DOCUMENT_ROOT'].'/public/airlines.json' ) ){
            return array();
        }

        @$json_file = file_get_contents($file_path);//$_SERVER['DOCUMENT_ROOT'].'/public/airlines.json');
        $arr = json_decode($json_file, true);

        $retarr = array();
        if( !empty($arr) && is_array($arr) ){

            foreach($arr as $singarr){
                $retarr[$singarr['id']] = array(
                    "code" => $singarr['id'],
                    "logo" => $singarr['logo'],
                    "name" => $singarr['name']//preg_replace('/[^A-Za-z0-9\-]/', '', $singarr['name'])
                );
            }
        }

        return $retarr;
    }

    function timezoneDoesDST($tzId) {
        $tz = new DateTimeZone($tzId);
        return count($tz->getTransitions(time())) > 0;
    }

    function searchFlights($data = array())
    {

        $pagination_limit = 200;
        error_reporting(E_ALL);
        ini_set('memory_limit', '512M');

        $checkReturn = false;
        $dateFrom 	= date('d/m/Y');
        $dateTo 	= $dateFrom;//date('d/m/Y', strtotime('+1 days'));

        if(isset($data['departure_date']))
        {

            $dateFrom	=	date('d/m/Y', strtotime($data['departure_date']));
            $dateTo 	= 	$dateFrom;//date('d/m/Y', strtotime('+1 days' , strtotime($data['departure_date'];)));


        }
        $typeFlight = "oneway";
        if($data['optradio'] == 2)
        {
            //$typeFlight = "return";
            $typeFlight = "round";
        }

        $page_no = isset($data['page_no']) ? intval($data['page_no']) : 1;

        $urlData = array(
            "v" => 2,
            "sort" => 'price',
            "asc" => 1,
            "locale" => 'en',//'us',
            "curr" => 'USD',
            "dateFrom" => $dateFrom,
            "dateTo" => $dateTo,
            "typeFlight" => $typeFlight,
            "limit"=>$pagination_limit,
            "offset"=>( ($page_no - 1) * $pagination_limit),
            "one_per_date"=>0,
            "oneforcity"=>0,
            "passengers" => 1
        );

        //price filters
        if(isset($data['price_from']))
        {
            $urlData['price_from'] = $data['price_from'];
        }
        if(isset($data['price_to']))
        {
            $urlData['price_to'] = $data['price_to'];
        }

        //Departure time filters
        if(isset($data['dtimefrom']))
        {
            $urlData['dtimefrom'] = $data['dtimefrom'];
        }
        if(isset($data['dtimeto']))
        {
            $urlData['dtimeto'] = $data['dtimeto'];
        }

        //Arrival Time filters
        if(isset($data['atimefrom']))
        {
            $urlData['atimefrom'] = $data['atimefrom'];
        }
        if(isset($data['atimeto']))
        {
            $urlData['atimeto'] = $data['atimeto'];
        }

        //Return flight Departure time filters
        if(isset($data['returndtimefrom']))
        {
            $urlData['returndtimefrom'] = $data['returndtimefrom'];
        }
        if(isset($data['returndtimeto']))
        {
            $urlData['returndtimeto'] = $data['returndtimeto'];
        }

        //Return flight Arrival Time filters
        if(isset($data['returnatimefrom']))
        {
            $urlData['returnatimefrom'] = $data['returnatimefrom'];
        }
        if(isset($data['returnatimeto']))
        {
            $urlData['returnatimeto'] = $data['returnatimeto'];
        }


        if(!empty($data['pnum']))
        {
            $urlData['passengers'] = $data['pnum'];
        }

        if(isset($data['maxstopovers']))
        {
            $urlData['maxstopovers'] = $data['maxstopovers'];
        }

        if(isset($data['sort']))
        {
            $urlData['sort'] = $data['sort'];
        }

        if(isset($data['asc']))
        {
            $urlData['asc'] = $data['asc'];
        }

        if(isset($data['onlyWorkingDays']))
        {
            $urlData['onlyWorkingDays'] = $data['onlyWorkingDays'];
        }

        if(isset($data['onlyWeekends']))
        {
            $urlData['onlyWeekends'] = $data['onlyWeekends'];
        }

        if(isset($data['Flight_fromID']))
        {
            $urlData['flyFrom'] = $data['Flight_fromID'];
        }

        if(isset($data['Flight_toID']))
        {
            $urlData['to'] = $data['Flight_toID'];
        }

        if($data['optradio'] == 2)
        {
            $checkReturn = true;

            $dateFrom	=	date('d/m/Y', strtotime($data['return_date']));
            //$dateTo 	= 	date('d/m/Y', strtotime('+1 days' , strtotime($data['return_date'])));
            $dateTo 	= 	$dateFrom;

            $urlData['returnFrom'] 	= $dateFrom;
            $urlData['returnTo']	= $dateTo;

            //
            //$time_diff = strtotime($data['return_date']) - strtotime($data['departure_date']);
            //$urlData['daysInDestinationTo'] = ($time_diff / 86400);
            //$urlData['daysInDestinationFrom'] = $urlData['daysInDestinationTo'] - 1;//1;

        }
        //echo '<pre>';print_r($urlData);exit;
        $url = http_build_query($urlData);
        $url = "https://api.skypicker.com/flights?".$url;
        $jsonData = $this->sendRequest($url);
        $jsonData = json_decode($jsonData);
        $allFlights = isset($jsonData->data) ? $jsonData->data : array();
        //echo '<pre>';print_r($allFlights );exit;

        $flights = array();
        $airlines = array();

        $min_price = 9999999;
        $max_price = 0;

        $default_time_zone = date_default_timezone_get();
        date_default_timezone_set('Europe/Berlin');

        $daylightSaving = $this->timezoneDoesDST('Europe/Berlin');
        $timeDeduction = 0;
        if($daylightSaving){
            $timeDeduction = 3600;
        }

        if($checkReturn)
        {
            foreach($allFlights as $flight)
            {
                $adate = date('D M d,h:i A', $flight->aTime - $timeDeduction);//date_create();
                $ddate = date('D M d,h:i A', $flight->dTime - $timeDeduction);//date_create();
                $adateUTC = date('D M d,h:i A', $flight->aTimeUTC - $timeDeduction);//date_create();
                $ddateUTC = date('D M d,h:i A', $flight->dTimeUTC - $timeDeduction);//date_create();

                $retadate = null;
                $retddate = null;
                $retadateUTC = null;
                $retddateUTC = null;

                $route = $flight->route;

                $cityFrom = '';
                $cityTo = '';
                $flag = true;

                $departure_airlines = array();
                $return_airlines = array();

                foreach($flight->route as $key => $route)
                {
                    $flight->route[$key]->bags_recheck = isset($flight->route[$key]->bags_recheck_required) ? $flight->route[$key]->bags_recheck_required : 0;

                    $flight->route[$key]->aTimeUTCFormated = date('D M d,h:i A', $route->aTimeUTC - $timeDeduction);
                    $flight->route[$key]->dTimeUTCFormated = date('D M d,h:i A', $route->dTimeUTC - $timeDeduction);
                    $flight->route[$key]->aTimeFormated = date('D M d,h:i A', $route->aTime - $timeDeduction);
                    $flight->route[$key]->dTimeFormated = date('D M d,h:i A', $route->dTime - $timeDeduction);

                    $start_date = new DateTime($route->aTimeUTCFormated);
                    $since_start = $start_date->diff(new DateTime($route->dTimeUTCFormated));

                    $flight->route[$key]->bags_recheck_required = $since_start->h.'h '.$since_start->i.'m';

                    if(!in_array($route->airline , $airlines)){
                        $airlines[] = $route->airline;
                    }

                    if($flag && $route->return){
                        $flag = false;
                        $cityTo = $flight->route[$key]->cityFrom;//getting first city from where return start

                        //getting first arrival time and departure time in return route
                        $retddateUTC = $flight->route[$key]->dTimeUTCFormated;
                        $retddate = $flight->route[$key]->dTimeFormated;
                    }
                    $retadateUTC = $flight->route[$key]->aTimeUTCFormated;
                    $retadate = $flight->route[$key]->aTimeFormated;


                    //getting airlines
                    if($route->return){

                        if( !in_array($route->airline, $return_airlines) ){
                            $return_airlines[] = $route->airline;
                        }

                    }else{

                        if( !in_array($route->airline, $departure_airlines) ){
                            $departure_airlines[] = $route->airline;
                        }
                    }

                }

                $cityFrom = $flight->route[0]->cityFrom;//first city from where departure start

                if($flight->price < $min_price){
                    $min_price = $flight->price;
                }

                if($flight->price > $max_price){
                    $max_price = $flight->price;
                }

                $flights[] = array(
                    "mapIdfrom" => $flight->mapIdfrom,
                    "mapIdto" => $flight->mapIdto,
                    "return_duration" => $flight->return_duration,
                    "aTime" => $adate,
                    "dTime" => $ddate,
                    "aTimeUTC" => $adateUTC,
                    "dTimeUTC" => $ddateUTC,
                    "retaTimeUTC" => $retadateUTC,
                    "retdTimeUTC" => $retddateUTC,
                    "retaTime" => $retadate,
                    "retdTime" => $retddate,
                    "flyTo" => $flight->flyTo,
                    "cityTo" => $cityTo,
                    "flyFrom" => $flight->flyFrom,
                    "cityFrom" => $cityFrom,
                    "fly_duration" => $flight->fly_duration,
                    "price" => $flight->price,
                    "route" => $flight->route,
                    "booking_token" => $flight->booking_token,
                    "nightsInDest" => $flight->nightsInDest,
                    "departure_airlines"=>$departure_airlines,
                    "return_airlines"=>$return_airlines,
                    "guarantee"=>(!empty($flight->guarantee) ? 1 : 0)
                );
            }
        }
        else
        {
            foreach($allFlights as $flight)
            {
                $adate = date('D M d,h:i A', $flight->aTime - $timeDeduction);//date_create();
                $ddate = date('D M d,h:i A', $flight->dTime - $timeDeduction);//date_create();
                $adateUTC = date('D M d,h:i A', $flight->aTimeUTC - $timeDeduction);//date_create();
                $ddateUTC = date('D M d,h:i A', $flight->dTimeUTC - $timeDeduction);//date_create();

                $route = $flight->route;

                foreach($flight->route as $key => $route){

                    $flight->route[$key]->bags_recheck = isset($flight->route[$key]->bags_recheck_required) ? $flight->route[$key]->bags_recheck_required : 0;

                    $flight->route[$key]->aTimeUTCFormated = date('D M d,h:i A', $route->aTimeUTC - $timeDeduction);
                    $flight->route[$key]->dTimeUTCFormated = date('D M d,h:i A', $route->dTimeUTC - $timeDeduction);
                    $flight->route[$key]->aTimeFormated = date('D M d,h:i A', $route->aTime - $timeDeduction);
                    $flight->route[$key]->dTimeFormated = date('D M d,h:i A', $route->dTime - $timeDeduction);

                    $start_date = new DateTime($flight->route[$key]->aTimeUTCFormated );
                    $since_start = $start_date->diff(new DateTime($flight->route[$key]->dTimeUTCFormated ));
                    $flight->route[$key]->bags_recheck_required = $since_start->h.'h '.$since_start->i.'m';
                    if(!in_array($route->airline , $airlines))
                    {
                        $airlines[] = $route->airline;
                    }

                }

                if($flight->price < $min_price){
                    $min_price = $flight->price;
                }

                if($flight->price > $max_price){
                    $max_price = $flight->price;
                }

                $flights[] = array(
                    "mapIdfrom" => $flight->mapIdfrom,
                    "mapIdto" => $flight->mapIdto,
                    "fly_duration" => $flight->fly_duration,
                    "aTime" => $adate,
                    "dTime" => $ddate,
                    "aTimeUTC" => $adateUTC,
                    "dTimeUTC" => $ddateUTC,
                    "flyTo" => $flight->flyTo,
                    "flyFrom" => $flight->flyFrom,
                    "price" => $flight->price,
                    "route" => $flight->route,
                    "booking_token" => $flight->booking_token,
                    "guarantee"=>(!empty($flight->guarantee) ? 1 : 0)
                );
            }
        }
        //echo '<pre>';	print_r($flights);exit;

        return array(
            "flights" => $flights,
            "total_flights" => count($flights),
            "checkReturn" => $checkReturn,
            "pagination_limit"=>$pagination_limit,
            "page_no"=>$page_no,
            'min_price'=>$min_price,
            'max_price'=>$max_price
        );
    }

    function sendRequest($url)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url,
            //CURLOPT_USERAGENT => 'Tayyab Ijaz'
        ));
        $resp = curl_exec($curl);
        curl_close($curl);
        return $resp;
    }
};
?>