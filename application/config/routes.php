<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

//Landing page routes
$route['(:any)-landing'] = "front/home/lp/$1";


$route['default_controller'] = "front/home";
//$route['hdaccess'] = "admin/account";
$route['admin/dashboard'] = "admin/account/dashboard";
$route['admin/login'] = "admin/account/login";
$route['login'] = "front/home/login";
$route['ipban'] = "admin/account/ipban";

//SEO by Jocel 
$route['user/legal-corner/(:any)'] = "user/legal_corner/$1";
$route['user/legal-corner/event-tickets'] = "user/legal_corner/event_tickets";
$route['user/legal-corner/terms-condition'] = "user/legal_corner/terms_condition";
$route['user/legal-corner/privacy-policy'] = "user/legal_corner/privacy_policy";
$route['user/legal-corner/property-policy'] = "user/legal_corner/property_policy";
$route['user/legal-corner/dmca-Policy'] = "user/legal_corner/dmca_Policy";
$route['user/legal-corner/bitcoin-policy'] = "user/legal_corner/bitcoin_policy";
$route['user/legal-corner/security-statement'] = "user/legal_corner/security_statement";
$route['front/home/member-signup'] = "front/home/member_signup";
$route['front/home/affiliate-signup'] = "front/home/affiliate_signup";

$route['travel-professional-signup'] = "front/travel_professional/signup";

///my routing
$route['sync_destination_with_api'] = "front/tourico_api/SynchronizeWithApi";
$route['get_destination_suggestion'] = "front/activities/GetDestinationListBasedOnSuggestion";

//cruise
$route['get_cruise_destination_suggestion'] = "front/tourico_api/GetCruiseDestinationListBasedOnSuggestion";
$route['view_cruise_details'] = "front/tourico_api/GetCruiseDetails";

//hotel search
$route['searchFilterAndMoreResult'] = "front/tourico_api/searchFilterAndMoreResult";
$route['view_search_result_map'] = "front/tourico_api/ViewSearchResultInMap";
$route['view_search_result'] = "front/tourico_api/ViewSearchResult";
$route['view_hotel_search_id'] = "front/tourico_api/ViewHotelSearchResultById";
$route['ajax/view_search_result'] = "front/tourico_api/ViewAjaxSearchResult";
$route['ajax/getCancellationPoliciesBeforeReservation'] = "front/tourico_api/GetCancellationPoliciesBeforeReservationAjax";
$route['ajax/TL_HotelPolicy'] = "front/tourico_api/TL_HotelPolicyAJAX";
//Flights
$route['view_search_flights'] = "front/tourico_api/ViewSearchFlights";
$route['get_flights_data'] = "front/tourico_api/get_flights_data";

//car search
$route['view_car_search_result'] = "front/tourico_api/ViewCarSearchResults";
$route['view_car_details/(:any)/(:any)/(:any)/(:any)'] = "front/tourico_api/$1/$2/$3/$4";
$route['ajax/car_search_ajax'] = "front/tourico_api/GetCarDetailsAjax";


$route['request_quote'] = "front/tourico_api/ShowQuotePage";
$route['book_hotel'] = "front/tourico_api/ShowQuotePageTestAnik";
$route['book_cruise'] = "front/tourico_api/ShowBookPageCruise";
$route['book_car'] = "front/tourico_api/ShowBookPageCar";

$route['activities-listing'] = "front/activities/listing";
$route['view_activity_details'] = "front/activities/view";
$route['add_activity_to_cart'] = "front/activities/add_item_to_cart";
$route['remove_activity_from_cart'] = "front/activities/remove_item_from_cart";
$route['checkout'] = "front/activities/checkout";


// Cruise searches
$route['view_cruise_search_result'] = "front/tourico_api/ViewCruiseSearchResults";
$route['404_override'] = 'my404';


//qoute
$route['downloadinv/(:any)'] = "front/quote_request/view_invoice/$1";
$route['thankyou/(:any)'] = "front/tourico_api/thankyou/$1";
//get stats
$route['states/(:any)'] = "front/pages/states/$1";
$route['admin/enrollment/info/(:any)'] = "admin/enrollment/info/$1";
/*Travel profession page*/
$route['front/travel-professional/signup'] = "front/travel_professional/signup";
$route['front/travel-professional/incentives'] = "front/travel_professional/incentives";
$route['front/travel-professional/account_info'] = "front/travel_professional/account_info";
$route['front/travel-professional/thanks'] = "front/travel_professional/thanks";
$route['front/travel-professional/agent-markup'] = "front/travel_professional/agent_markup";
$route['front/travel-professional/faqs'] = "front/travel_professional/faqs";
/* End of file routes.php */

/*Entity page*/
$route['front/entity/entity-sub-signup/(:any)'] = "front/entity/entity_sub_signup/$1";
/*End Entity page*/

$route['front/home/individual-signup'] = "front/home/individual_signup";

/* Location: ./application/config/routes.php */