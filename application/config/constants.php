<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');


/* End of file constants.php */
/* Location: ./application/config/constants.php */
define('ADMIN_LAYOUT','admin/layout');
define('ADMIN_LAYOUT_LOGIN','admin/layout_login');
define('AFILLIATE_LAYOUT','affiliate/affiliate_layout');
define('LOGIN_LAYOUT','front/login_layout');
define('FRONT_LAYOUT','front/front_layout');
define('USER_LAYOUT','user/user_layout');

// plans
/*define("3-Months Plan",'3monthsplan');
define("THREE_MONTH_PLAN_DAYS",90);
define("SIX_MONTH_PLAN",'6monthsplan');
define("SIX_MONTH_PLAN_DAYS",180);
define("YEAR_PLAN",'1yearplan');
define("YEAR_PLAN_DAYS",365);*/

// admin
define("PASS_TO_DELETE",'bUw#kef5ate$U8a2');
define("STRIPE_SECRET_KEY",'sk_test_pXZu0HXdj2nffgTwk8xbG8Tr');

define('SKIP_CODE',22446688);

// DOMAIN_NAME
define("EMAIL_DOMAIN",'WHotelsGroup.com');
define("DOMAIN_NAME",'www.WHotelsGroup.com');
define("SITE_NAME",'Wholesale Hotels Group');

define("EST_AMERICA_TIME_ZONE",'America/New_York');
// set it to est but can we changes to "America/Los_Angeles" as at many places it was set.
define("AMERICA_LOS_ANGLES_TIME_ZONE",'America/New_York');

define("DISPLAY_DATE_FORMAT",'l, F d, Y');
define("DISPLAY_DATE_FORMAT_FULL",'l, F d, Y h:i A');

define("DAYS_ADDITION_TO_TL_POLICY",'2');
define("HOURS_ADDITION_TO_TOURICO_POLICY",'48');

define("INFO_EMAIL",'info@whotelsgroup.com');

define("SUPPLIER_TECHNICAL_ERROR",'Ooooops…. Sorry! We seem to be having some kind of a technical difficulty cancelling your reservation. Please reach out to us via Contact US menu and write us a detailed message about this issue so that we can resolve this immediately. Thank you!');

// captcha
define('CAPTCHA','6LctDSgUAAAAAC8ZpIIMN420Nk7XXa5GC3QtbHlu');

define('NOREPLY','noreply@softech.website');

define('MAP_KEY','AIzaSyB50mqscR5FPkh_NfGnewTI2vi_N1svBug');

define('SUB_ENTITY_MAX_DAYS',90);