<?php
if($ajax == 'ajax'){
     echo $this->load->view($page);
}else{    
?>
<!DOCTYPE html>
<html lang="en" ng-app="wfApp">
    <!-- header start-->
    <?php echo $this->load->view('front/include/header'); ?>
    <!-- header end-->
    
     <body class="ng-scope" id="myP" ng-controller="mainController">
<!--         <div id="preloader">
            <div class="loader"></div>-->
           <?php echo $this->load->view('front/include/loader'); ?>
        <!--</div>-->
           <div id="container">           
               <!--Page body header start-->
               <?php echo $this->load->view('front/include/bodyheader'); ?>
               <!--Page body header end-->
                 
               
               <!-- Page Data start-->
              <div ng-view></div>
               
               <!-- Page Data End-->
               </div>
         
         <!-- Body Footer start-->
         <?php  echo $this->load->view('front/include/bodyfooter');  ?>
         <!-- Body Footer end-->
               
         <!--- Footer Start-->
         <?php echo $this->load->view('front/include/footer');  ?>
         <!-- Footer End-->
           </div><!-- page-wrap end -->
    </body>
</html>
<?php
}
?>
