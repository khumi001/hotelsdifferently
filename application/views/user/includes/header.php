<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <?php
    $title = empty($var_meta_title) ? "Wholesale Hotels Group – Great travel deals for Corporate Travel and Individuals" : $var_meta_title;
    $description = empty($var_meta_description) ? "Get WHOLESALE HOTEL inventories and deals worldwide for Corporate Travel, Non-Profit travel, Government travel, Travel Agents and Individual users." : $var_meta_description;
    $keywords = empty($var_meta_keyword) ? "Wholesale Hotels Group, WHotelsGroup.com, wholesale hotel deals, corporate travel, government travel, non-profit travel, travel agency travel, travel agency hotels, wholesale hotels, Online Travel Agency, best OTAs" : $var_meta_keyword;
    ?>

    <title><?php echo $title ?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta content="<?php echo $description ?>" name="description"/>
    <meta content="<?php echo $keywords ?>" name="keywords"/>
    <meta name="google-site-verification" content="ZBUbYCT9lCwyszTdzXzftAqkYQS8o5O6qP8eGRaiQXk" />
    <meta name="p:domain_verify" content="a29fc0e0d9ae02190ff346cc33d9b2b3"/>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <!--<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>-->
    <link href="<?php echo base_url(); ?>public/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url(); ?>public/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url(); ?>public/affilliate_theme/stylesheets/style.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>public/affilliate_theme/stylesheets/custom.css" rel="stylesheet" type="text/css">
    <!--<link href="<?php echo base_url(); ?>public/affilliate_theme/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">-->
    <?php
		if(isset($this->page_name) && $this->page_name == "My Coupons")
		{
			?>
			<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/assets/plugins/data-tables/DT_bootstrap.css"/>
			<?php
		}
		else
		{
			?>
			<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.12/datatables.min.css"/>
			<?php
		}
	?>
     <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/assets/plugins/bootstrap-toastr/toastr.min.css"/>
<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/assets/plugins/bootstrap-datepicker/css/datepicker.css"/>-->
    <!-- END GLOBAL MANDATORY STYLES -->
    <?php
    if($contactimpoter == 'yes'){
    ?>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/affilliate_theme/stylesheets/contactimporter.css">
    <?php
    }
    ?>
    <!-- Begin From Controller-->
    <?php
    if (!empty($css)) {
        foreach ($css as $value) {
            ?>
            <link rel="stylesheet" href="<?php echo base_url(); ?>public/affilliate_theme/stylesheets/<?php echo $value; ?>" />
            <?php
        }
    }
    ?>

    <?php
    if (!empty($css_plugin)) {
        foreach ($css_plugin as $value_plugin) {
            ?>
            <link rel="stylesheet" href="<?php echo base_url(); ?>public/affilliate_theme/plugins/<?php echo $value_plugin; ?>" />

            <?php
        }
    }
    ?> 
    <!-- End From Controller -->


    <!-- BEGIN THEME STYLES -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/affilliate_theme/plugins/bootstrap-select/bootstrap-select.min.css" />
    <link href="<?php echo base_url(); ?>public/assets/stylesheets/plugins.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url(); ?>public/assets/stylesheets/custom.css" rel="stylesheet" type="text/css"/>
    <!-- END THEME STYLES -->
    <script type="text/javascript">
        var baseurl = "<?php echo base_url(); ?>";
    </script>

    <link rel="shortcut icon" href="http://hotels.softech.website/public/affilliate_theme/img/logo.png"/>
	<link href="<?php echo base_url(); ?>public/affilliate_theme/stylesheets/responsive.css" rel="stylesheet" type="text/css">
    <style type="text/css">
        .switch {
    cursor:pointer; 
    position: relative; 
    padding-right:10px;
    display: inline-block;
    margin-bottom:5px;
    height: 26px;
}

.switch > .switch-label {
    cursor:pointer;
    display: inline-block;
    position: relative;
    height: 25px;
    width: 58px;
    color: #fff;
    font-size: 10px;
    font-weight: bold;
    line-height: 20px;
    text-align: center;
    background: #B5C1C7;
    border: 2px solid #B5C1C7;
    text-transform: uppercase;
    font-family:Helvetica, Arial, sans-serif;

    -webkit-transition: 0.3s ease-out;
       -moz-transition: 0.3s ease-out;
         -o-transition: 0.3s ease-out;
            transition: 0.3s ease-out;

    -webkit-border-radius: 2px;
       -moz-border-radius: 2px;
         -o-border-radius: 2px;
            border-radius: 2px; 
}
.switch > .switch-label + span{ 
    display:inline-block; 
    padding-left:5px; 
    position:relative; top:-7px; 
}
.switch > .switch-label:before {
    content: attr(data-off);
    position: absolute;
    top: 1px;
    right: 3px;
    width: 33px;
}

.switch > .switch-label:after {
    content:""; 
    margin: 1px;
    width: 19px;
    height: 19px;
    display: block;
    background: #fff;
    -webkit-border-radius: 1px;
    -moz-border-radius: 1px;
    -o-border-radius: 1px;
    border-radius: 1px;
}

.switch > input {
    -webkit-appearance: none;
    position: absolute;
    width: inherit;
    height: inherit;
    opacity: 0;
    left: 0;
    top: 0;
   
}

/* @toggle switch focus state 
-------------------------------------------------------------- */
.switch > input:focus { 
    outline: none; 
}
.switch > input:focus + .switch-label { 
    color: #fff; border-color: #a1a6a9; background:#a1a6a9; 
}
.switch > input:focus + .switch-label:after { 
    background: #fff; 
}

/* @toggle switch normal state 
--------------------------------------------------------------- */
.switch > input:checked + .switch-label {
    border-color: #333;
    background: #333; 
    padding-left: 33px;
    color: white;
}
    .switch.switch-success > input:checked + .switch-label {
        border-color:#4cae4c;
        background:#4cae4c;
    }
    .switch.switch-danger > input:checked + .switch-label {
        border-color:#d43f3a;
        background:#d43f3a;
    }
    .switch.switch-warning > input:checked + .switch-label {
        border-color:#eea236;
        background:#eea236;
    }
    .switch.switch-info > input:checked + .switch-label {
        border-color:#46b8da;
        background:#46b8da;
    }
    .switch.switch-default > input:checked + .switch-label {
        border-color:rgba(0,0,0,0.1);
        background:rgba(0,0,0,0.1);
    }
    .switch.switch-default > input:checked + .switch-label:before {
        color:#888;
    }

.switch > input:checked + .switch-label:before {
    content: attr(data-on);
    left: 1px;
    top:1px;
}

.switch > input:checked + .switch-label:after {
    margin: 1px;
    width: 19px;
    height: 19px;
    background: white;
}



/* @toggle switch normal state focus 
--------------------------------------------------------------------------------- */
.switch-round > .switch-label { 
    -webkit-border-radius: 13px;
       -moz-border-radius: 13px;
         -o-border-radius: 13px;
            border-radius: 13px; 
}
.switch-round > .switch-label + span{ 
    top:-2px; 
}   
.switch-round > .switch-label:before { 
    width: 33px; 
}
.switch-round > .switch-label:after {
    width: 19px;
    color:#B5C1C7;
    content: "\2022";
    font:20px/20px Times, Serif;
    -webkit-border-radius: 13px;
    -moz-border-radius: 13px;
    -o-border-radius: 13px;
    border-radius: 13px;
}

.switch-round > input:checked + .switch-label { 
    padding-left: 33px; 
}
.switch-round > input:checked + .switch-label:after{ 
    color:#333; 
}
    </style>
</head>

<!-- END HEAD -->