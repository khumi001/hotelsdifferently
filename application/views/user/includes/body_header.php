<!--[if gte IE 9]>
<style type="text/css">
    .gradient {
        filter: none;
    }
</style>
<![endif]-->
<header>

    <div class="row">
        <div class="col-md-2 margin-top-12">
            <form action="#" method="post" id="newsletter_form" class="newsletter-form">
                <div class="form-group">
                    <label class="control-label">Signup for Newsletter</label>
                    <input type="text"  class="form-control" name="newsletter" id="newsletter" value="" />
                    <input type="submit" name="btn_submit" value="submit" />
                </div>
            </form>
        </div>
        <div class="col-md-2 clearfix"></div>
        <div class="col-md-4 text-center logo">
            <a href="<?php echo base_url();?>" style="padding:0 0;display: block;"><img style="width:315px;height:118px;" width="316" height="118" alt="cheap hotel booking site" src="<?php echo base_url(); ?>public/affilliate_theme/img/logo.png"></a>
        </div>
        <div class="col-md-4 margin-top-12">
            <a href="#" style="visibility: hidden;" class="call_link">800.289.0900</a>
            <div class="pages_links margin-top-12">




                <?php  if (!isset($this->session->userdata['valid_user'])) {?>
                    <a href="<?php echo base_url(); ?>login" class="main_links login_link">Login</a>
                    <div class="sign_up main_links">Signup
                        <ul>
                            <li><a href="<?php  echo base_url(); ?>front/home/member-signup">Member Signup</a></li>
                            <!--
                        <li><a href="<?php  echo base_url(); ?>front/home/affiliate-signup">Affiliate Signup</a></li>
						-->
                        </ul>
                    </div>
                <?php  }else{?>
                    <a href="<?php echo base_url(); ?>front/home/logout" class="login_link main_links">Logout</a>
                <?php }
                ?>
                <a href="<?php echo base_url(); ?>user/legal-corner/aboutus" class="main_links  newmargin-links">About Us</a>
                <a href="<?php echo base_url(); ?>user/legal-corner/coupons" class="main_links newmargin-links">Coupons</a>
                <a href="<?php echo base_url(); ?>user/legal-corner/travelinsurance" class="main_links newmargin-links" >Travel Insurance</a>
                <a href="<?php echo base_url(); ?>user/legal-corner/faqs" class="main_links">FAQ</a>
                <!-- <a href="<?php /*echo base_url(); */?>blog" class="main_links" target="_blank">BLOG</a> -->
            </div>
        </div>
    </div>

</header>
<div aria-hidden="false" role="dialog" class="modal fade in" id="newsletter_modal" style="display: none;">
    <div class="success_login_main_cont margin-top-20">
        <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="fa fa-times"></i></button>
            <!--<h4 class="modal-title">Registration Successfully</h4>-->
            <h3 class="form-title">NEWSLETTER</h3>
        </div>
        <div style="" class="">
            <p style="padding: 10px; margin-bottom: 0px;">
                Thank you for signing up for our newsletter. We sent you an activation link to the email address you entered.
                &numsp;&numsp;&numsp;
                Does it seem like it has been taking too long and you still have not received the activation link?
                &numsp;&numsp;&numsp;&numsp;&numsp;&numsp;&numsp;&numsp;&numsp;&numsp;&numsp;
                Please check your SPAM / JUNK folder or try again.
            </p>
        </div>
        <div style="padding: 3px; margin-top: 0px;" class="modal-footer">
            <div style="text-align:center">
                <button type="button" style="padding: 7px 20px;" data-dismiss="modal" class="btn default_btn clickme">OK</button>
            </div>
        </div>
    </div>
</div>
<div aria-hidden="false" role="dialog" class="modal fade in" id="newsletter_modal_error" style="display: none;">
    <div class="success_login_main_cont margin-top-20">
        <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="fa fa-times"></i></button>
            <!--<h4 class="modal-title">Registration Successfully</h4>-->
            <h3 class="form-title">NEWSLETTER</h3>
        </div>
        <div style="" class="">
            <p style="padding: 10px; margin-bottom: 0px;">
                Sorry but this email address is already registered in our system!
            </p>
        </div>
        <div style="padding: 3px; margin-top: 0px;" class="modal-footer">
            <div style="text-align:center">
                <button type="button" style="padding: 7px 20px;" data-dismiss="modal" class="btn default_btn clickme">OK</button>
            </div>
        </div>
    </div>
</div>
<div aria-hidden="false" role="dialog" class="modal fade in" id="newsletter_already_user" style="display: none;">
    <div class="success_login_main_cont margin-top-20">
        <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="fa fa-times"></i></button>
            <!--<h4 class="modal-title">Registration Successfully</h4>-->
            <h3 class="form-title">NEWSLETTER</h3>
        </div>
        <div style="" class="">
            <p style="padding: 10px; margin-bottom: 0px;">
                Sorry but this email address is already registered in our system!
            </p>
            <!--            <p style="padding: 10px; margin-bottom: 0px;">
                            Sincerely,<br/>
                            <b>HotelsDifferently</b><SUP>sm</SUP>
                        </p>-->
        </div>
        <div style="padding: 3px; margin-top: 0px;" class="modal-footer">
            <div style="text-align:center">
                <button type="button" style="padding: 7px 20px;" data-dismiss="modal" class="btn default_btn clickme">OK</button>
            </div>
        </div>
    </div>
</div>