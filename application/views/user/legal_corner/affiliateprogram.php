<div class="width-row margin-top-20">
    <div class="main_cont">
        <div class="pagetitle margin-bottom-10">
            <h1><?php echo $this->page_name; ?></h1>
            <p style="margin: 20px 0px; text-align: center;">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/5Ni7XpDppVk?modestbranding=1" frameborder="0" allowfullscreen></iframe>
            </p>
            <!--<p align="center"><strong><u>AFFILIATE GUIDE:</u></strong></p>-->

<!--<p><strong>FOR AFFILIATES: </strong></p>-->

<p>Thank you for your interest in becoming our Affiliate. I am sure you would like to know how our Affiliate program works and also to hear why our Affiliate program is <strong>ONE OF THE BEST</strong> and <strong>MOST REWARDING</strong> right now.<br />
<br />
Any new signups you refer will have your referral number on their future bookings, therefore if you have someone signup once, every time that Member uses that account to make a booking, you will receive commission for as long as they pay for bookings. That way, We not only reward you for referring new clients but We will always reward you:&nbsp; As long as that previous client you referred to Us continues to make bookings with Us from the account they used to sign up from <b><a href="<?php echo base_url();?>affiliate/banner_link">your link</a></b>, you CONTINUE to receive a commission each and every time they make payments. This can provide you a nice paycheck for the lifetime of that specific account. The more signups you refer, the more money you can make!</p>

<p>The major hotel booking sites take advantage of Affiliates but We prefer to consider them as business partners. Once you refer them a new client, the client will no longer use your link to make bookings, he will just keep on going back to the site to which you referred them to make a booking &ndash; and then you will no longer receive any compensation. We, at <b>HotelsDifferently<sup>sm</sup></b> reward you for as long as the referred person makes bookings with us.</p>

<p><strong>Our referral program works the following way: </strong></p>

<p>3.5% commission up to $9,999/month.<br />
4% commission between $10,000-$49,999/month!<br />
4.5% commission ABOVE $50,000/month!<br />
<br />
You have the option whether you wish to be paid via PayPal or WesternUnion.</p>

<p>On top of that, we have annual incentives that we give out to our very best Affiliates (such as luxurious hotel stays and/or flights paid by <b>HotelsDifferently<sup>sm</sup></b>). Be our TOP AFFILIATE, MAKE MONEY and get rewarded with LUXURIOUS GIVEAWAYS!!!</p>

<p>Thank you for your attention and we very much look forward to making money with you.</p>

<p>
    <a href="<?php echo base_url().'user/home'?>"><img src="<?php echo base_url(); ?>public/affilliate_theme/img/logo.png"></a>
</p>

        </div>
    </div>
</div>