<style>
    /*-------------popup1------------*/
    .popup1 .modal-content{width: 350px; margin:auto;}
    .popup1 .modal-content p{padding:15px;color:#299a0b; }

    .popup1 .modal-footer{ margin: 0px;}
    .popup1 p{ margin: 0px;}
    .popup1 .modal-header {
        background: none repeat scroll 0 0 #299a0b;
        color: #fff;
        padding: 7px 10px;
    }
    .popup1 .modal-header{
        border-radius:4px 4px 0 0;
    }
    .popup1 .modal-header h4{
        font-size: 16px;
    }
    .popup1 .modal-content .modal-footer .default_btn {
        padding: 3px 10px;
    }
    /*-------------popup1------------*/

    /*-------------popup2------------*/
    .popup2 .modal-content{width: 350px; margin:auto;}
    .popup2 .modal-content p{padding:15px;color:#000; }

    .popup2 .modal-footer{ margin: 0px;}
    .popup2 p{ margin: 0px;}
    .popup2 .modal-header {
        background: none repeat scroll 0 0 #1D9FF1;
        color: #fff;
        padding: 7px 10px;
    }
    .popup2 .modal-header{
        border-radius:4px 4px 0 0;
    }
    .popup2 .modal-header h4{
        font-size: 16px;
    }
    .popup2 .modal-content .modal-footer .default_btn {
        padding: 3px 10px;
    }
    /*-------------popup2------------*/

    /*-------------popup3------------*/
    .popup3 .modal-content{width: 350px; margin:auto; background:#1D9FF1;}
    .popup3 .modal-content .modal-header .modal-title{color:#1D9FF1;}
    .popup3 .modal-content .modal-header{ background:#fff; border-radius:4px 4px 0 0;}
    .popup3 .modal-content .modal-footer{ background:#fff; border-radius:0 0 4px 4px;}
    .popup3 .modal-content p{padding:15px; color:#fff;}
    .popup3 .modal-content .modal-footer .default_btn{padding:4px 30px; transition:300ms;}
    .popup3 .modal-content .modal-footer .btn-left{float:left; width:50%;}
    .popup3 .modal-content .modal-footer .btn-right{float:right; width:50%;}
    .popup3 .modal-footer{ margin: 0px;}
    .popup3 p{ margin: 0px;}
    /*-------------popup3------------*/

    /*-------------popup4------------*/
    .popup4 .modal-content{width: 350px; margin:auto; background:#fff; overflow:hidden;}
    .popup4 .modal-content .modal-header{ background:#1D9FF1}
    .popup4 .modal-content .modal-header .modal-title{color:#fff;}
    .popup4 .modal-content p{padding:15px; color:#1d9ff1;}
    .popup4 .modal-content .modal-footer .default_btn{padding:4px 30px; background:#ffffff; color: #1d9ff1; transition:300ms;}
    .popup4 .modal-content .modal-footer .default_btn:hover{padding:4px 30px; background:#1D9FF1; color: #fff;}
    .popup4 .modal-content .modal-footer .btn-left{float:left; width:50%;}
    .popup4 .modal-content .modal-footer .btn-right{float:right; width:50%;}
    .popup4 .modal-content .modal-footer {background: none repeat scroll 0 0 #1d9ff1; padding: 10px;}
    .popup4 .modal-footer{ margin: 0px;}
    .popup4 p{ margin: 0px;}
    /*-------------popup4------------*/

</style>

<div class="width-row margin-top-20">
	<div class="width-row" style="display:none;padding: 10px 0 0 8px;">
		<div itemscope itemtype="http://schema.org/Person">
		   <span itemprop="name">Hotels Differently</span>
		   <span itemprop="company">HotelsDifferently LLC</span>
		   <span itemprop="tel">888-287-2307</span>
		</div>
	</div>
    <div class="main_cont">
        <div class="pagetitle margin-bottom-10">
            <h1><?php echo $this->page_name; ?></h1>
        </div> 
        <div class="content_row">

            <form method="post" id="send_mail" class="form-horizontal form-bordered" action="#">

                <div class="form-body">

                    <div class="row">
                        <div class="col-md-12">
							<div class="col-md-12" id="contacterror" style="display:none;color:red;"></div>
						</div>
                        <div class="col-md-6">

                            <div class="form-group">
                                <label class="control-label col-md-12" style="margin-bottom: 36px; margin-top: 18px;text-align: justify;">Thank you for contacting us! To better assist you, please be sure to LOGIN before you submit any questions regarding to your account.</label>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4">Name:</label>
                                <div  class="col-md-8">
                                    <input class="form-control" name="name" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4">Email address:</label>
                                <div  class="col-md-8">
                                    <input class="form-control" name="email_add" value="">
                                </div>
                            </div>
                            <!--<div class="form-group">
                                <label class="control-label col-md-4">Response:</label>
                                <div  class="col-md-8">
                                    <select class="form-control input-xlarge select2me" name="response" placeholder="">
                                        <option value="">Please Select</option>
                                        <option value="yes">Yes</option>
                                        <option value="no">No</option>
                                    </select>
                                </div>
                            </div>-->

                        </div>
                        <div class="col-md-6 text-center">

                            <div class="form-group">
                                <label class="control-label col-md-12" style="text-align: center;">Your Message to us:</label>
                                <div  class="col-md-12">
                                    <textarea class="form-control " id="count_char" name="message" maxlength="1000" style="height:172px;"></textarea>
                                    <p id="counter" class="text-left" style="display: inline-block; float: left;">1000 Characters left</p>
									<script src='https://www.google.com/recaptcha/api.js'></script>
									<div class="g-recaptcha" data-sitekey="<?php echo CAPTCHA;?>"></div>
                                    <button type="submit" class="default_btn" style="padding: 25px 50px; display: inline-block; float: right; margin-top: 2px; position:absolute; right:15px; bottom:-25px;">Send</button>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-12" style="padding-top:40px;">
                            <div class="col-md-4">
                                
                            </div>
                            <div  class="col-md-4 text-center">
                                <h4><b> Our Mailing Address: </b></h4>
                                <b>HotelsDifferently, LLC.</b><br/>
                                3651 Lindell Road, D141<br/>
                                Las Vegas, Nevada, 89103<br/>
                                United States of America<br/>

                            </div>
                            <div class="col-md-4"></div>
                            
                        </div>
                    </div>                
                </div>    
            </form>
        </div>
    </div>    
</div>
<div aria-hidden="false" role="dialog" class="modal fade in" id="contact_modal" style="display: none;">
    <div class="success_login_main_cont margin-top-20">
        <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="fa fa-times"></i></button>
            <!--<h4 class="modal-title">Registration Successfully</h4>-->
            <h3 class="form-title">MESSAGE SENT!</h3>
        </div>
        <div style="" class="">
            <p style="padding: 10px">Thank you! Your message was sent to <b>Hotelsᗡifferently<sup>TM</sup></b> and if you requested a response, then a representative will be in touch with you soon!</p>
        </div>
        <div style="padding: 3px;" class="modal-footer">
            <div style="text-align:center">
                <button type="button" style="padding: 7px 20px;" data-dismiss="modal" class="btn default_btn clickme">OK</button>
            </div>
        </div>
    </div> 
</div>
