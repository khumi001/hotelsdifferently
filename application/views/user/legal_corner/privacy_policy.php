<div class="width-row margin-top-20">
     <div class="main_cont">
        <div class="pagetitle margin-bottom-10">
            <h1><?php echo $this->page_name; ?></h1>
            


<p align="center"><strong>Effective as of <?php echo date(DISPLAY_DATE_FORMAT, strtotime("July 15, 2016"))?></strong></p>

<p>Unless otherwise set forth herein with specificity, the definitions set forth in our TERMS AND CONDITIONS POLICY apply to this Privacy Policy (the “PRIVACY POLICY”). Protecting your private information is our priority. This PRIVACY POLICY applies to <strong><?php echo DOMAIN_NAME;?><sup>sm</sup></strong> and <strong><?php echo SITE_NAME;?></strong> and its parent LLC and governs data collection and usage as set forth herein. The Company Site is a service for those person or persons seeking lower cost hotel stays. By using the Company Site, you consent to the data practices described in this PRIVACY POLICY.</p>

<p>&nbsp;</p>

<p><strong>Section I. PERSONALLY IDENTIFIABLE INFORMATION.</strong></p>

<p><strong>1. For Members or Potential Members:</strong> The following are considered PERSONALLY IDENTIFIABLE INFORMATION:</p>

<p>A. Your First and Last Name</p>

<p>B. Your Email Address</p>

<p>C. Your Phone Number (if you supply one)</p>

<p>D. Your Account Number</p>

<p><strong>2. For Affiliates or Potential Affiliates:</strong> The following are considered PERSONALLY IDENTIFIABLE INFORMATION:</p>

<p>A. Your company name</p>

<p>B. The First and Last Name of any person supplied by you as a point of contact for us.</p>

<p>C. Your Email Address</p>

<p>D. Your Phone Number(s)</p>

<p>E. Your street address but NOT your city, state, zip code or country</p>

<p>F. Your account number</p>

<p>G. Your site description</p>

<p>H. Any information provided in order for us to process payments to you.</p>

<p><br />
<strong>SECTION II. INFORMATION WHICH IS NOT PERSONALLY IDENTIFIABLE INFORMATION</strong></p>

<p>1. <strong>For Members or Potential Members</strong>: The following are NOT considered PERSONALLY IDENTIFIABLE INFORMATION:</p>

<p>A. Any Voluntary Data Collection as set forth in this PRIVACY POLICY and in our TERMS AND CONDITIONS POLICY.</p>

<p>B. Your Password; however your password will be known only to you and Company unless you disclose your password to another which is PROHIBITED under our TERMS AND CONDITIONS POLICY.</p>

<p>C. Any ELECTRONIC COMMUNICATIONS as set forth in <strong>our TERMS AND CONDITIONS POLICY, or any information shared with a Service Provider as set forth in our TERMS AND CONDITIONS POLICY, except</strong> that if <strong>any of the items denominated in Section I(1)(A-D) of this PRIVACY POLICY</strong> are included in such ELECTRONIC COMMUNICATION or with a Service Provider,<strong> then the items in Section I(1)(A-D) of this PRIVACY POLICY are still considered Personally Identifiable Information.</strong></p>

<p>D. Any information you supply to LINKS TO THIRD PARTY SITES/THIRD PARTY SERVICES as set forth in <strong>our TERMS AND CONDITIONS POLICY.</strong></p>

<p>E. Any information you supply in your USE OF COMMUNICATION SERVICES as set forth in <strong>our TERMS AND CONDITIONS POLICY.</strong></p>

<p>F. Any information you supply in INTERACTION BETWEEN USERS as set forth in <strong>our TERMS AND CONDITIONS POLICY.</strong></p>

<p>G. Any information that we need to share with a hotel or to provide you other Service(s) in order to make a Booking for you.</p>

<p>H. Any information as denominated in Section <strong>III(1)(A-B) and Section II(11) of our TERMS AND CONDITIONS POLICY</strong></p>

<p>I. Any information supplied in requesting, approving or denying a Membership.</p>

<p>J. Any information supplied to THIRD PARTY ACCOUNTS as set forth in <strong>our TERMS AND CONDITIONS POLICY.</strong></p>

<p><strong>2. For Affiliates or Potential Affiliates: </strong>The following are <strong>NOT</strong> considered <strong>PERSONALLY IDENTIFIABLE INFORMATION:</strong></p>

<p>A. Your city, state, zip code or country</p>

<p>B. Any Voluntary Data Collection as set forth in this PRIVACY POLICY and in our TERMS AND CONDITIONS POLICY.</p>

<p>C. Your password will be known only to you and Company unless you disclose your password to another which is PROHIBITED under our TERMS AND CONDITIONS POLICY.</p>

<p>D. Any ELECTRONIC COMMUNICATIONS as set forth in <strong>our TERMS AND CONDITIONS POLICY,or any information shared with a Service Provider as set forth in our TERMS AND CONDITIONS POLICY, except</strong> that <strong>if any of the items</strong> denominated in <strong>Section I(2)(A-H)</strong> of this PRIVACY POLICY are included in such ELECTRONIC COMMUNICATION or with a Service Provider, <strong>then the items in Section I(2)(A-H) of this PRIVACY POLICY are still considered Personally Identifiable Information.</strong></p>

<p>E. Any information you supply to LINKS TO THIRD PARTY SITES/THIRD PARTY SERVICES as set forth in <strong>our TERMS AND CONDITIONS POLICY.</strong></p>

<p>F. Any information you supply in INTERACTION BETWEEN USERS as set forth in <strong>our TERMS AND CONDITIONS POLICY.</strong></p>

<p>G. Any information supplied to THIRD PARTY ACCOUNTS as set forth in <strong>our TERMS AND CONDITIONS POLICY.</strong></p>

<p>H. Any information supplied in requesting, approving or denying Affiliate status.</p>

<p><strong>&nbsp;SECTION III. INFORMATION GENERALLY</strong></p>

<p><strong>1. Use of Information, Generally</strong></p>

<p>A. Personally Identifiable Information will never be sold, leased, given, transferred, rented or otherwise provided to another User or to a third party except as otherwise as set forth in Section II, Section III and Section IV(2) of this PRIVACY POLICY.</p>

<p>B. Company may also collect anonymous demographic information, which is not unique to you, such as your city, state and country (for Affiliates), and Voluntary Data. All of our payments to us are processed using Stripe® as our payment processor and Bitcoin exchanger. Stripe® is a registered service mark of Stripe, Inc. Our payments to Affiliates are made by us using either Western Union® orStripe® as chosen by the Affiliate. We reserve the right at any time to change our payment processor or Bitcoin exchanger without prior notice.</p>

<p>We do not engage in any direct credit card/debit card or e-check transactions. Stripe® or any other payment processor we use may ask for your billing address information, including zip code, among other information, to process payment electronically by Members to us. Bitcoin may ask for certain information to protect your transaction. Western Union® may ask for the same or similar information from Affiliates for payments by us to Affiliates. We do NOT have access to your Stripe®, or other payment processor, Western Union® or Bitcoin personal account information.</p>

<p>C. Information about your computer hardware and software may be automatically collected by Company. This information can include: Your IP address, browser type, domain names, access times and referring website addresses. This information is used for the operation of the service, to maintain quality of the service, and to provide general statistics regarding use of the Company website.</p>

<p>D. Please keep in mind that if you directly disclose Personally Identifiable Information or personally sensitive data with anyone OTHER than the Company, this information may be collected and used by others.</p>

<p>E. The Company encourages you to review the privacy statements of websites you choose to link to from Company, if any, so that you can understand how those websites collect, use and share your information. Company is not responsible for the privacy statements or other content on websites outside of the Company’ Site.</p>

<p>&nbsp;<strong>2. Use of Your Personally Identifiable Information by Company</strong></p>

<p>A. Company collects and uses your Personally Identifiable Information to operate its website(s) and deliver the services you have requested.</p>

<p>B. Company may also use your Personally Identifiable Information to inform you of other products or services available from Company and its affiliates. Company may also contact you via surveys to conduct research about your opinion of current services or of potential new services that may be offered. You will not consider this to be SPAM.</p>

<p>C. Company may share data with trusted partners, such as Service Providers, to help perform statistical analysis, send you email, track referrals by Affiliates or to provide customer support. All such third parties are prohibited from using your Personally Identifiable Information except to provide these services to Company, and they are required to maintain the confidentiality of your information.</p>

<p>D. Company may keep track of the websites and pages our Users visit within Company, in order to determine what Company services are the most popular. This data is used to deliver customized content and advertising within Company to customers whose behavior indicates that they are interested in a particular subject area.</p>

<p>E. Company will disclose your Personally Identifiable Information, without notice, only if required to do so by law or in the good faith belief that such action is necessary to: (a) conform to the edicts of the law or comply with legal process served on Company or the site; (b) protect and defend the rights or property of Company; or (c) act under exigent circumstances to protect the personal safety of Users of Company, or the public.</p>

<p><strong>3. Use of Cookies</strong></p>

<p>The Company website may use "cookies" and similar automated tools to help you personalize your online experience. A cookie is a text file that is placed on your hard disk by a web page server. Cookies are uniquely assigned to you, and can only be read by a web server in the domain that issued the cookie to you.</p>

<p>One of the primary purposes of cookies is to provide a convenience feature to save you time. The purpose of a cookie is to tell the web server that you have returned to a specific page. For example, if you personalize Company pages, or register with Company site or services, a cookie helps Company to recall your specific information on subsequent visits. This simplifies the process of recording your Personally Identifiable Information, such as Service preferences, etc. When you return to the same Company website, the information you previously provided can be retrieved, so you can easily use the Company features that you customized.</p>

<p>You have the ability to accept or decline cookies. Most web browsers automatically accept cookies, but you can usually modify your browser setting to decline cookies if you prefer. If you choose to decline cookies, you may not be able to fully experience the interactive features of the Company services or websites you visit.</p>

<p>&nbsp;<strong>4. Security of Your Personally Identifiable Information</strong></p>

<p>Company secures your Personally Identifiable Information from unauthorized access, use or disclosure.</p>

<p>&nbsp;<strong>5. Minors Under Minimum Age.</strong></p>

<p>Company does not knowingly collect personally identifiable information from minors under the Minimum Age.</p>

<p><strong>6. Disconnecting Your Company Account from Third Party Websites</strong></p>

<p>You may be able to connect your Company Account to third party accounts, such as social media. BY CONNECTING YOUR COMPANY ACCOUNT TO A THIRD PARTY ENTITY, YOU ACKNOWLEDGE AND AGREE THAT YOU ARE CONSENTING TO THE CONTINUOUS RELEASE OF INFORMATION ABOUT YOU TO OTHERS (IN ACCORDANCE WITH YOUR PRIVACY SETTINGS ON THOSE THIRD PARTY SITES). IF YOU DO NOT WANT INFORMATION ABOUT YOU, INCLUDING PERSONALLY IDENTIFYING INFORMATION, TO BE SHARED IN THIS MANNER, DO NOT USE THE THIS FEATURE IF WE MAKE IT AVAILABLE. You may disconnect your Account from a third party entity at any time. If we make this function available, Members may learn how to disconnect their Accounts from third-party websites by visiting that third-party website.</p>

<p><strong>7. Opt-Out &amp; Unsubscribe</strong></p>

<p>We respect your privacy and give you an opportunity to opt-out of receiving newsletters and announcements of certain information. Users may opt in or out of our newsletter as outlined in our Frequently Asked Questions. With regard to our newsletter, you must affirmatively choose to opt in; we do NOT automatically send you our Newsletter. Users may opt-out of receiving any or all other communications not directly related to the provision of Company Services by using the Contact Us page of our Site.</p>

<p><strong>8. Changes to this Statement</strong></p>

<p>Company will occasionally update this PRIVACY POLICY, in its sole discretion and without prior notice, to reflect Company and User feedback. Company encourages you to periodically review this PRIVACY POLICY to be informed of how Company is protecting your information. The date of the most current PRIVACY POLICY will always be at the top of this PRIVACY POLICY.</p>

<p><strong>9.</strong>&nbsp;<strong>Contact Information</strong></p>

<p>Company welcomes your questions or comments regarding this PRIVACY POLICY. If you believe that Company has not adhered to this Statement, please contact Company using the Contact Us page on our Site.</p>

<p><strong>10. FAILURE TO ABIDE OR ACCEPT. If you DO NOT AGREE TO ABIDE BY, OR FULLY ACCEPT, and ELECTRONICALLY SIGN this PRIVACY POLICY AND ALL OTHER POLICIES, you MUST IMMEDIATELY EXIT THIS SITE. Links to this PRIVACY POLICY AND ALL OF OUR POLICIES may be found on the Home Page of this Site.</strong></p>

<p><strong>11. ELECTRONIC SIGNATURE ACT. </strong>By entering this Site, you are, pursuant to the Electronic Signature Act as defined in our TERMS AND CONDITIONS, indicating: </p>

<p>A. Your unconditional acceptance and agreement to abide by the this PRIVACY POLICY AND ALL OUR POLICIES and,</p>

<p>B. That you are of MINIMUM AGE, and</p>

<p>C. You are submitting an unsworn declaration, certificate and verification, in writing of your agreement with, and that you will abide by, this PRIVACY POLICY AND ALL OUR POLICIES, that is subscribed by you, as true under penalty of perjury, and dated on, and in the form required, to be in compliance with the Electronic Signature Act.</p>

        </div> 
    </div>
</div>