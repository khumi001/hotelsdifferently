<div class="width-row margin-top-20">
    <div class="main_cont">
        <div class="pagetitle margin-bottom-10">
            <h1><?php echo $this->page_name; ?></h1>
            
         

<p align="center"><strong>Effective As Of <?php echo date(DISPLAY_DATE_FORMAT, strtotime("July 15, 2016"))?></strong></p>

<p><strong>1. Definitions:</strong> Unless otherwise set forth herein with specificity, the definitions set forth in our TERMS AND CONDITIONS POLICY apply to this INTELLECTUAL PRIVACY POLICY (the “INTELLECTUAL PROPERTY POLICY”).  This INTELLECTUAL PROPERTY POLICY applies not only to Intellectual Property (as defined below), but to Proprietary Information (as defined below) and the name of this INTELLECTUAL PROPERTY POLICY in no way limits the scope of the provisions as set forth herein.</p>

<p><strong>2. </strong>By entering our Site, you are deemed to have accepted, and signed this INTELLECTUAL PROPERTY POLICY, and have agreed to, and accepted, and signed, our PRIVACY POLICY, our TERMS AND CONDITIONS POLICY, BITCOIN POLICY, SECURITY STATEMENT and our DMCA POLICY, and any of our other terms and conditions relative to the access, use or viewing of this Site (collectively, “OUR POLICIES”). Please review ALL of the aforementioned policies which are available on our Home Page.</p>

<p><strong>3. </strong>You are granted ONLY, a limited, non-exclusive, non-transferable, revocable at our sole discretion, and without prior notice, license (a) to access and use the Site and (b) to your password, strictly in accordance with OUR POLICIES. As a condition of your use of the Site, you represent and warrant to Company that you will not use the Site for any purpose that is unlawful or prohibited by this INTELLECTUAL PROPERTY POLICY and any other of OUR POLICIES. This Site, and all content therein, unless a specific provision to the contrary has been made by this Company, is Copyrighted under the laws of the United States of America, with ALL RIGHTS RESERVED.</p>

<p><strong>4. </strong>All content included as part of the Site, such as text, graphics, logos, images, as well as the compilation and layout and format thereof, and any software used on the Site, including source code, is the property of Company or its suppliers and/or Service Providers and protected by copyright as set forth in Section 3, above, and other laws that protect Intellectual Property and Proprietary Information as such property and information are defined herein and in Section 5, below. Intellectual Property includes, but is not limited to, copyrighted material, trademarks, services marks and Registered Marks (the “Intellectual Property”). You agree to observe and abide by all copyright and other proprietary notices, legends or other restrictions contained in any such content and will not make any changes thereto.</p>

<p><strong>5. </strong>You will not modify, publish, transmit, reverse engineer, participate in the transfer or sale, create derivative works, or in any way exploit any of the content, in whole or in part, found on the Site. Company content, Intellectual Property and Proprietary Information (as defined below) is not for resale. Your use of the Site does not entitle you to make any unauthorized use of any Intellectual Property or Proprietary Information, and in particular you (a) will not delete or alter any proprietary rights or attribution notices in any content and (b) you will make no use, for any purpose, of our Proprietary Information, including, without limitation, the Service Providers that we use in order to provide you our Services, including Bookings and/or reservations or other information that is Confidential Information or a Trade Secret under the laws of the United States of America ( the “Proprietary Information”). You will use the aforementioned Intellectual Property and Proprietary Information solely for your personal use of our Services, and will make no other use of Intellectual Property and Proprietary Information without the express written permission of Company, and with regard to copyrighted material, of any other copyright owner if applicable. You agree that you do not acquire any ownership rights in Intellectual Property and/or Proprietary Information. We do not grant you any license, express or implied, to the Intellectual Property or to the Proprietary Information of Company or our licensors as set forth in Sections 4 and 5 of this INTELLECTUAL PROPERTY POLICY except as expressly authorized by this INTELLECTUAL PROPERTY POLICY.</p>

<p><strong>6. </strong>In addition, you will not attempt, or actually perform any attempt, whether successful or not, by means of the functionality or capability of any browser or similar software, or any print screen function, or software designed to enable a User to capture a screen image, or similar software, in order to copy, paste, print, edit, download or otherwise endeavor to save, make a pictorial or similar image of, or reproduce any portion of, the Site (the “Unauthorized Reproduction”). In addition, you will not attempt to, or actually perform any attempts, whether successful or not, to circumvent any functionality of this Site which is designed to prevent a User from Unauthorized Reproduction.</p>

<p><strong>7. </strong>Changes to INTELLECTUAL PROPERTY and OTHER POLICIES.</p>

<p>Company reserves the right, in its sole discretion, without prior notice, to change this INTELLECTUAL PROPERY POLICY, under which the Site is offered, and any or all of OUR POLICIES. The most current version of the INTELLECTUAL PROPERTY POLICY and OUR POLICIES will supersede all previous versions. Company encourages you to periodically review OUR POLICIES to stay informed of our updates. You may find links to the most current versions of OUR POLICIES on the Home Page of this Site. You are encouraged to frequently review OUR POLICIES for any changes. The Effective Date of this INTELLECTUAL PROPERTY POLICY is at the top of this page, and the most current version supersedes all prior versions.</p>

<p><strong>8. </strong>TRADEMARKS, SERVICE MARKS, REGISTERED TRADEMARKS.</p>

<p>A. The following are trademark(s), service mark(s), trade name(s), or Registered trademarks (under the laws of the United States and any subdivision thereof), individually or collectively referred to as the “Trademarks”, or a registered internet domain name, registered with an entity accredited to issue domain names:</p>

<p>&nbsp;</p>

<p><b><?php echo SITE_NAME;?>.</b>.</p>
<p><?php echo DOMAIN_NAME;?> &nbsp;(a domain name)</p>

<p>Any of the following are service marks and may appear as part of this graphic and in a different font or front size.</p>


<p>1. The graphic:</p>

<p><img style="display:block; max-width:100%; height:auto;" height="77" src="<?php echo base_url();?>public/affilliate_theme/img/logo.png" width="146" /></p>

<p><strong>2. <?php echo SITE_NAME;?>℠</strong></p>

<p>3. <strong>Where better deals are made for YOU!℠</strong></p>

<!-- <p>4. <img style="display:inline-block; vertical-align:middle; height:auto; max-width:100%;" height="77" src="<?php echo base_url();?>public/affilliate_theme/img/privacy-point-04.png" width="146" />℠</p> -->

<p>7. The following are also service marks</p>


<!--
<p><strong><?php echo SITE_NAME;?><sup>sm</sup></strong><br />
Take the hotel challenge!℠</p>-->


<!-- 
<p><img style="display:block; max-width:100%; height:auto;" src="<?php echo base_url(); ?>public/affilliate_theme/img/banner1.png" width="300" height="100" /></p>

<p><img style="display:block; max-width:100%; height:auto;" src="<?php echo base_url(); ?>public/affilliate_theme/img/banner2.png" width="728" height="90" /></p>

<p><img style="display:block; max-width:100%; height:auto;" src="<?php echo base_url(); ?>public/affilliate_theme/img/banner3.png" width="468" height="60" /></p> -->
                        
                    

<p>On each of the graphics, slight color variations or changes in font styles and/or spacing of words in the graphic are included in the service mark or Trademark or registered mark of each graphic in this INTELLECTUAL PROPERTY POLICY.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>B. No User Interest or Ownership.</p>

<p>User acknowledges that (1) Company owns the Trademarks and the internet domain name(s) as set forth above, and all goodwill associated with or symbolized thereby, (2) User has no ownership right in or to any of the Trademarks or internet domain name(s), and (3) User shall acquire no ownership interest or license in or to any of the Trademarks, or any other trademarks, service marks, trade names, and internet domain name(s) used by Company in this Site. User shall do nothing inconsistent with Company's ownership of the Trademarks, internet domain name(s), and related goodwill and agrees that User shall not use any of the Trademarks or internet domain names used by Company in any manner as a part of any business, corporate or trade name or otherwise.</p>

<p><strong>9. </strong>If you DO NOT AGREE TO ABIDE BY, OR FULLY ACCEPT, and ELECTRONICALLY SIGN this INTELLECTUAL PROPERTY POLICY, AND ALL OF OUR OTHER POLICIES, you MUST IMMEDIATELY EXIT THIS SITE.</p>

<p><strong>10. </strong>By entering this Site, you are, pursuant to the Electronic Signature Act as defined in our TERMS AND CONDITIONS, indicating:</p>

<p>A. Your unconditional acceptance and agreement to abide by the this INTELLECTUAL PROPERTY POLICY AND ALL OTHER POLICIES and,</p>

<p>B. That you are of MINIMUM AGE, and</p>

<p>C. You are submitting an unsworn declaration, certificate and verification, in writing of your agreement with, and that you will abide by, this INTELLECTUAL PROPERTY POLICY AND ALL OTHER POLICIES, that is subscribed by you, as true under penalty of perjury, and dated on, and in the form required, to be in compliance with the Electronic Signature Act.</p>

<p><strong>11</strong>. The foregoing notwithstanding, this INTELLECTUAL PROPERTY POLICY is governed by Section IV(5) of our TERMS AND CONDITIONS and to the extent there is any conflict between this INTELLECTUAL PROPERTY POLICY and our TERMS AND CONDITIONS, our TERMS AND CONDITIONS shall prevail.</p>

        </div> 
    </div>
</div>