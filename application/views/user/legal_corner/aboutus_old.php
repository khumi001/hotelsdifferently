<div class="width-row margin-top-20">
	<div class="width-row" style="display:none;padding: 10px 0 0 8px;">
		<div itemscope itemtype="http://schema.org/Person">
		   <span itemprop="name">Hotels Differently</span>
		   <span itemprop="company">HotelsDifferently LLC</span>
		   <span itemprop="tel">888-287-2307</span>
		</div>
	</div>
    <div class="main_cont">
        <div class="pagetitle margin-bottom-10">
            <h1><?php echo $this->page_name; ?></h1>
            <p></p>
            <p><strong>Who are we and what separates us from other booking sites? </strong>
            <p>
		       We would like to welcome you on our site. <strong>HotelsDifferently<sup>sm</sup></strong> (as its name suggest) does everything in a little bit contrarily as compared to other hotel booking sites. We are not just another listing site or an aggregate booking site that works via referrals.  <strong>HotelsDifferently<sup>sm</sup></strong> is a website that provides you with one of the lowest rates in the market through our contracted wholesale suppliers that provide prices that are not available to the general public. Our special prices on hotels and activities are only available to our registered Members while flights and event tickets are available without registration! <i>Still can’t find the best rates here? </i> Not a problem! Click on the "COUPONS" section and you will be able to access ALL PROMOTIONS that the major hotel booking sites currently offer. Just click on the desired promo and enjoy your savings!
            </p>
            <p style="margin: 0px;">&nbsp;</p>
            <p><strong>Our mission:</strong></p>
            <!--<p style="margin: 0px;">&nbsp;</p>-->
            <p>We are dedicated to provide more for less. In today’s world, everybody is looking for amazing deals and we strive to assist our valued Members with this specifically. We work hard to help everybody find the deals they are looking for and to save money on mostly hotels so that you can spend money on other things. We cannot guarantee that you will always find what you are looking for but our extended network has the potential to 
            help you get a better-quality stay for the same amount compared to other sites or the exact same hotel room for a lower price.</p>
			<p>Customer service and discounted QUALITY stays are the two things we mainly focus on!</p>
            <p><strong style="margin:30px 0px 0px ;display: block;">Our Credentials:</strong></p>
            <p><strong>HotelsDifferently<sup>sm</sup></strong> is an Online Travel Agency, which is owned by <strong>HotelsDifferently, LLC </strong> registered in Nevada, USA. </p>
			<p style="margin: 0px;">&nbsp;</p>
            <!-- <p><strong>Company info:</strong></p> -->
            <!--<p style="margin: 0px;">&nbsp;</p>-->
           <!--  <p><strong>HotelsDifferently<sup>sm</sup></strong> is owned by <strong>HotelsDifferently, LLC</strong> registered in Nevada, USA. </p> -->
           <p style="margin:0 0 30px;">
                <span style="float:left; margin:0 10px 0 0;"><a href="https://www.bbb.org/southern-nevada/business-reviews/online-travel-agency/hotelsdifferently-in-las-vegas-nv-90049254" target="_blank"><img src="<?php echo base_url(); ?>public/assets/img/online-arating.jpg" alt="online-arating" width="70" height="43"></a></span>
            <strong>HotelsDifferently.com</strong> has achieved a stellar <strong> A+ rating</strong>  from the Better Business Bureau in 2017. We pride ourselves in providing an honest and customer-oriented service to ensure a pleasant experience when you choose our company for your travel needs.
            </p>
			<p style="margin:0 0 30px;">
				<span style="float:left; margin:0 10px 0 0;"><img width="70" height="70" src="<?php echo base_url(); ?>public/assets/img/about-us-titan.png"  alt="titan"></span>
			<strong>HotelsDifferently, LLC.</strong> is accredited by  IATAN<sup>TM</sup> (International Airlines Travel Agent Network,<i>  formed about 25 years ago</i>) which is a department of  IATA<sup>TM</sup> <i>(formed in 1945) </i> and has been servicing all aspects of the US Travel and Tourism industry ever since. It offers global recognition and global reach, which no other accreditation program can and its members must undergo and requalify a rigorous screening process including but not limited to financial, sales, licensing, educational and liability requirements annually.
			</p>
            <p style="margin:0 0 30px;">
                <span style="float:left; margin:0 10px 0 0;"><a href="https://www.asta.org/about/memberconf.cfm?navItemNumber=671&verify=900252051" target="_blank"><img width="70" height="43" src="<?php echo base_url(); ?>public/assets/img/asta_logo.jpg"  alt="ASTA"></a></span>
            <strong>HotelsDifferently, LLC.</strong> is a proud member of   ASTA<sup>TM</sup> (American Society of Travel Agents) which is the biggest conglomerate for Travel Professionals in the world. It was formed in 1931 and has been committed to principles of professional conduct, ethics and continued enhancement and improvement in the travel industry since then.
            </p>
			
			<p>
				<span style="float:left; max-width:70px; margin:0 10px 0 0;padding:0 3px;background:#fff;"><a style="display:inline-block; vertical-align:top; max-width:200px;" rel="nofollow" href="http://www.tkqlhce.com/click-8137399-10395450-1443636871000" target="_blank">
<img style="display:block; width:100%; height:auto;" src="<?php echo base_url();?>/public/affilliate_theme/img/travelocity.png" width="120" height="90" alt="travelcity"/></a></span>
				We are also proud to announce that we are an Authorized Affiliate to Travelocity.com. 
			</p>
			<p><br/></p>
			<div style="width: 100%;">
                <div style="width: 28%;float: left;margin-left: 35%;text-align: center;">
                    <p><strong>Our mailing address:</strong></p>
                    <p style="margin: 0;"><strong>HotelsDifferently, LLC.</strong></p>
                    <p style="margin: 0;">3651 Lindell Road, D141</p>
                    <p style="margin: 0;">Las Vegas, Nevada, 89103</p>
                    <p style="margin: 0;">United States of America</p>
                   
                </div>
<!--                <div style="width: 25%;float: right;margin-right: 25%;text-align: center;"><p>
                     <strong>MAILING ADDRESS:</strong></p>
                    <p style="margin: 0;"><strong>Tour-Eco Holidays, Ltd.</strong></p>
                    <p style="margin: 0;">Kemp House, 152 City Road</p>
                    <p style="margin: 0;">London, EC1V 2NX</p>
                    <p style="margin: 0;">United Kingdom</p>
                </div>-->
                <div class="clear"></div>
            </div>
        </div>
    </div>
</div>