<div class="width-row margin-top-20">
    <div class="main_cont">
        <div class="pagetitle margin-bottom-10">
            <h1><?php echo $this->page_name; ?></h1>
<p><strong>I. Definitions: </strong>As used herein, the following terms have the following meanings, and these definitions shall apply, whether the defined term is in all uppercase, all lowercase or capitalized letters, unless the context requires otherwise:</p>

<p style="margin-left:.5in;">A. &ldquo;Applicable Law&rdquo; shall mean 17 USC. &sect; 512, and</p>

<p style="margin-left:.5in;">B. &ldquo;Company&rdquo; shall mean <b><?php echo SITE_NAME;?></b>, the owner and/or publisher of the Site.</p>

<p style="margin-left:.5in;">C. &ldquo;Electronic Signature Act&rdquo; shall mean the Electronic Signature Act as defined in our TERMS AND CONDITIONS.</p>

<p style="margin-left:.5in;">D. &ldquo;Notice&rdquo; shall mean any notice or counter notice permitted or required under Applicable Law</p>

<p style="margin-left:.5in;">E. &ldquo;Site&rdquo; shall mean this website: <strong><?php echo DOMAIN_NAME;?></strong> or any other website or website page wholly owned or published by Company:</p>

<p style="margin-left:1.0in;">(i) to which a User may be redirected as a result of the User selecting a particular function or use from within <strong><?php echo DOMAIN_NAME;?></strong>, or</p>

<p style="margin-left:1.0in;">(ii) which the User enters directly by typing in the website address of such other website or page into their internet browser, or</p>

<p style="margin-left:1.0in;">(iii) which the User enters by copying and pasting such website address of such other website or page in their internet browser, or by means of the User selecting a bookmark the User has created, or</p>

<p style="margin-left:1.0in;">(iv) which the User enters by any other means.</p>

<p style="margin-left:.5in;">F. &ldquo;us&rdquo; or &ldquo;we&rdquo; means the Company</p>

<p style="margin-left:.5in;">G. &ldquo;you&rdquo; or &ldquo;your&rdquo; means the person or entity submitting any Notice to us, relative to the DMCA and/or this policy.</p>

<p style="margin-left:.5in;">H. &ldquo;User&rdquo; shall mean any person who accesses the Site for ANY reason.</p>
<br/>
<p><strong>II. DMCA Policy</strong></p>

<p>A.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong><u>This Site takes protection of intellectual property rights, including copyright, very seriously</u></strong>. As such, this Site complies with the &ldquo;safe harbor&rdquo; provisions of the Applicable Law.&nbsp;</p>

<p>B.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong><u>At the same time, this Site takes very seriously any Notice which is not made in good faith</u></strong>. Under Applicable Law, such Notices are required to be signed and submitted <strong><u>under penalty of perjury</u></strong>. As such, any such Notice NOT MADE IN GOOD FAITH and/or which contains a material false statement and/or misrepresentation will be referred to law enforcement authorities for investigation of possible perjury, and we reserve the right to additionally refer such material to our legal counsel for consideration of appropriate civil sanctions. If you are in doubt as to whether or not you, or the person or entity on whose behalf you are submitting Notice, has had any intellectual property rights, including copyright, we strongly suggest you contact your legal counsel since material misrepresentations and/or false statements, may result in you being liable for damages, including costs and legal fees and/or criminal prosecution.</p>

<p>C.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Pursuant to the Applicable Law, we will respond to written notification, by the methods outlined below, of copyright infringements, or a counter-notice in accordance with the DMCA. If you believe your copyrighted material is being infringed on our Site, please contact us immediately by the means outlined in this policy.</p>

<p>D.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; We reserve the right to share any Notice with a third party in order to investigate the allegations in any Notice</p>
<br/>
<p><strong>III. DMCA Infringement Notice</strong></p>

<p>A.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; In order for us to respond, you must provide us notice in a form that substantively complies with the safe harbor provisions of the Applicable Law.  Your notice of claimed infringement must be written, in English, and include ALL of the following:</p>

<p style="margin-left:.25in;">1.&nbsp;&nbsp;&nbsp; Your electronic or physical signature, if you claim the exclusive ownership to such alleged infringing information. If you are a person authorized to act on behalf of the owner of such an exclusive right that is allegedly infringed, you must provide the full legal name of such owner, provide proof that you are authorized by such owner to act on their behalf, and provide your electronic or physical signature.</p>

<p style="margin-left:.25in;">2.&nbsp;&nbsp;&nbsp; You MUST provide identification of each of the copyrighted work(s) that you are claiming has been infringed and a representative copy of those work(s).</p>

<p style="margin-left:.25in;">3. &nbsp;&nbsp;&nbsp; You must identify, with sufficient information that will allow us to locate the alleged infringed material, that you are seeking to have us disable access to, and/or to remove from our Site. We STRONGLY recommend that you copy the EXACT URL of the webpage or webpages of our Site (located in your browser display as the URL you are viewing) by copying and pasting such URL into your notice to us.</p>

<p style="margin-left:.25in;">4. &nbsp;&nbsp;&nbsp; Your contact information in a form that is adequate for us to contact you, such as an email address, which is preferred and/or a mailing address, and/or a telephone number including country code, and an area code, if applicable.</p>

<p style="margin-left:.25in;">5.&nbsp;&nbsp;&nbsp; YOU MUST INCLUDE a statement that you have a good faith belief that use of the allegedly infringed material, as used in or on our Site, is not authorized. </p>

<p style="margin-left:.25in;">6.&nbsp;&nbsp;&nbsp; YOU MUST INCLUDE, under penalty of perjury, a statement that the information in the Notice is accurate, and that you are the copyright owner or are authorized to act on behalf of the copyright owner.</p>

<p style="margin-left:.25in;">7.&nbsp;&nbsp;&nbsp; If you do not provide a written notice that meets these elements, we will not honor your request and are not required by law to do so.</p>
<br/>
<p>B.&nbsp; Your Notice may be sent:</p>

<p style="margin-left:.25in;">
   
    1. By the Contact Us function of our Site, or</p>

<p style="margin-left:.25in;">2. To our DMCA Agent by mail, or at the fax number listed in #3 below:<br/>
James H. Shewmaker, JD, MBA<br/>
DMCA Agent<br/>
<p style="margin-left:.43in;"><b><?php echo SITE_NAME;?></b> <br/>
			3651 Lindell Road, D141<br/>
                                                                    Las Vegas, Nevada, 89103<br/>
                                                                    United States of America<br/>
</p></p>

<!--<p style="margin-left:.25in;">Sherlock Holmes</p>

<p style="margin-left:.25in;">&nbsp;c/o RHD Group, LLC</p>

<p style="margin-left:.25in;">Number 22B Baker Street</p>

<p style="margin-left:.25in;">London, England</p>

<p style="margin-left:.25in;">Fax: ___________________, or</p> <div style="width: 200px;border: 1px solid #000;margin-left: 300px">The name, address and fax number could also be in Panama-either is fine.</div>-->

<!--<p style="margin-left:.25in;">&nbsp;</p>-->

<p style="margin-left:.25in;">3. By fax at 888-885-5259.</p>

<!--<p style="margin-left:.25in;">&nbsp;</p>-->

<p style="margin-left:.25in;">4. Your notice MUST contain all the information required by 17 U.S.C. 512 et seq., including under penalty of perjury, a statement that the information in the Notice is accurate, and that you are the copyright owner or are authorized to act on behalf of the copyright owner.</p>
<!--<br/>-->
<!--<p>C. For your convenience, we are providing a sample of a notice (infringement notice) under Applicable Law, further below in this policy.</p>-->

<!--<p>&nbsp;</p>-->

<p style="margin-left:.25in;">5. If sending Notice to us by email, you may cut and paste the form into the body of your email. In either event, You will be deemed to have signed the form in compliance with the Electronic Signature Act (as defined below).You may request the appropriate form(s) by any of the methods listed in B (2) or B (3) above. Forms will ONLY be provided as a PDF attachment to the email address you specify. If sending the notice to us by either email, or fax, you must additionally comply with all requirements, including the requisite language, which is provided for you in Section VI, below.</p>

<!--<p style="margin-left:.25in;">6. If sending the Notice to us by fax, your transmission of our form in the fax to us will be deemed to have been signed by you pursuant to the Electronic Signature Act (as defined below).</p>-->
<br/>
<p><strong>Section IV. DMCA Counter Notice</strong></p>

<p>A.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; In order for you to respond to a DMCA Infringement Notice, you must provide us a counter notice in a form that substantively complies with the Applicable Law as set forth in Sections 512(g)(3).  Your counter notice denying any claimed infringement must be written, in English, and include ALL of the following:</p>

<p style="margin-left:.25in;">1.&nbsp;&nbsp;&nbsp; Your electronic or physical signature.</p>

<p style="margin-left:.25in;">2.&nbsp;&nbsp;&nbsp; Your legal name</p>

<p style="margin-left:.25in;">3. &nbsp;&nbsp; Your contact information in a form that is adequate for us to contact you, such as an email address and you MUST INCLUDE a mailing address. It will assist us if you also provide a telephone number, including a country code and an area code, if applicable.</p>

<p style="margin-left:.25in;">4.&nbsp;&nbsp;&nbsp; Identify, with specificity, the material which we have removed and/or to which we have blocked access.</p>

<p style="margin-left:.25in;">5.&nbsp;&nbsp;&nbsp;  (A) If your address that you provide us in Item 3 above, is in the United States, you MUST consent to the jurisdiction and venue of the United States District Court for the Judicial District of the address that you provide to us and must agree that you will accept service of process from the person, or their agent,  who provided us a DMCA Infringement Notice to which you are responding, or (B) If your address that you provide us in Item 3 above, is not in the United States, you MUST consent to the jurisdiction and venue of the United States District Court for the Judicial District of Nevada and must agree that you will accept service of process from the person, or their agent, who provided us a DMCA Infringement Notice to which you are responding.</p>

<p style="margin-left:.25in;">6.&nbsp;&nbsp;&nbsp; YOU MUST INCLUDE, under penalty of perjury, a statement that you believe that the information described in Section 4 above was removed or blocked as a result of misidentification or mistake.</p>

<p style="margin-left:.25in;">7.&nbsp;&nbsp;&nbsp; If you do not provide a written counter notice that meets these elements, we will not honor your request and are not required by law to do so.</p>
<br/>
<p>B. Your counter notice may be sent:</p>

<p style="margin-left:.25in;">1. By using the Contact Us function of our Site </p>

<p style="margin-left:.25in;">
    
<p style="margin-left:.25in;">2. To our DMCA Agent by mail, or at the fax number listed in #3 below:<br>
    James H. Shewmaker, JD, MBA<br/>
DMCA Agent<br/>
<p style="margin-left:.43in;">
    <b><?php echo SITE_NAME;?></b><br/>
    3651 Lindell Road, D141<br/>
                                                                    Las Vegas, Nevada, 89103<br/>
                                                                    United States of America<br/>
</p></p>
<!--<p style="margin-left:.25in;">Sherlock Holmes<div class="con-box1">The name, address and fax number could also be in Panama-either is fine.</div><br>&nbsp;c/o RHD Group, LLC<br>Number 22B Baker Street<br>London, England<br>Fax: ___________________, or</p>-->
<!--
<p style="margin-left:.25in;">&nbsp;c/o RHD Group, LLC</p>

<p style="margin-left:.25in;">Number 22B Baker Street</p> <div class="con-box1">The name, address and fax number could also be in Panama-either is fine.</div>

<p style="margin-left:.25in;">London, England</p>

<p style="margin-left:.25in;">Fax: ___________________, or</p>-->

<!--<p style="margin-left:.25in;">&nbsp;</p>

<p style="margin-left:.25in;">&nbsp;</p>-->

<p style="margin-left:.25in;">3. By fax at 888-885-5259</p>

<!--<p style="margin-left:.25in;">&nbsp;</p>-->

<p style="margin-left:.25in;">4. Your notice MUST contain all the information required by 17 U.S.C. 512 et seq., including under penalty of perjury, a statement that information the material which we have removed and/or to which we have blocked access was removed or blocked as a result of misidentification or mistake.</p>
<p style="margin-left:.25in;">5. If sending Notice to us by email, you may cut and paste the form into the body of your email. You will be deemed to have signed the form in compliance with the Electronic Signature Act (as defined below). You may request the appropriate form(s) by any of the methods listed in B (2) or B (3) above. Forms will ONLY be provided as a PDF attachment to the email address you specify. If sending the notice to us by either email, or fax, you must additionally comply with all requirements, including the requisite language, which is provided for you in Section VI, below.</p>
<p><strong>SECTION V. </strong>The foregoing notwithstanding, this DMCA Policy is governed by our TERMS AND CONDITIONS and to the extent there is any conflict between this DMCA Policy and our TERMS AND CONDITIONS, our TERMS AND CONDITIONS shall prevail.</p>
<p><strong>SECTION VI. </strong>Electronic Signature Act: I am, pursuant to 28 U.S.C. § 1746, Unsworn declarations under perjury, indicating:</p>
<p style="margin-left:.25in;">A. Your unconditional acceptance and agreement to abide by this DMCA policy and all OTHER POLICIES, and</p>
<p style="margin-left:.25in;">B. That you are of MINIMUM AGE, and</p>
<p style="margin-left:.25in;">C. You are submitting an unsworn declaration, certificate and verification, in writing, that you accept and Agree that any Notice and/ or Counter-Notice supplied to us pursuant to the DMCA policy is true, and correct; that you accept and agree to Section D below, and that any Notice or Counter-Notice subscribed by you, is true under penalty of perjury, and dated, in substantially the following form:</p>
<p style="margin-left:.35in;">(1) If executed without the United States: “I declare and verify, under penalty of perjury under the laws of the United States of America that the foregoing is true and correct”. Executed on the date on which you, email or fax any Notice or Counter- Notice to us, and you emailing or faxing us any Notice or Counter-Notice is your binding, legal signature, for the purposes of this declaration and verification, or</p>
<p style="margin-left:.35in;">(2) If executed within the United States, its territories, possessions, or commonwealths: “I declare and verify, under penalty of perjury that the foregoing is true and correct”. Executed on the date on which you, email or fax any Notice or Counter- Notice to us, and you emailing or faxing us any Notice or Counter-Notice is your binding, legal signature, for the purposes of this declaration and verification.</p>
<p style="margin-left:.25in;">D. You agree pursuant, to 15 U.S.C. § 7001 et seq. (ESIGN), or if you are a resident of Nevada, pursuant to NRS 719.010 et seq. (UETA), that you:</p>
<p style="margin-left:.35in;">1. Intend, and if fact, do consent to this electronic transaction, and</p>
<p style="margin-left:.35in;">2. That you and we intend to do business electronically, and</p>
<p style="margin-left:.35in;">3. That you emailing or faxing us any Notice or Counter-Notice shall be, and act as, your electronic signature with regard to this electronic transaction, that your electronic signature is your binding, legal signature, and this transaction is effective as of the date upon which you email or fax any Notice or Counter- Notice to Us and,</p>
<p style="margin-left:.35in;">4. That you understand that you may withdraw your consent at any time by emailing notice of same via our Contact Us function, and that in doing so, your account will immediately be terminated with NO REFUND, and a $25.00 fee will be assessed for withdrawal of your consent.</p>
<p style="margin-left:.35in;">5. That you may request an electronic copy at no charge by using the Contact Us function and putting “Request LAD/Policy Acceptance” in the subject line of the email.</p>
<p style="margin-left:.35in;">6. You may request a paper copy of this electronic transaction by emailing us via the Contact Us function and including in the body of your email to us: Your full legal name, complete mailing address and putting “Request LAD/Policy Acceptance in paper form” in the body of the email. There will be a fee of $25.00 required to process a paper copy.</p>
<p style="margin-left:.35in;">7. You may update your contact information by using the update features on our Site. You will need a computer or like device capable of interacting with our Site, an internet connection, and an internet browser, to enter into this electronic transaction.
</p>
<!--<div class="link-main" >
    <a href="#myModal_autocomplete_infri" data-toggle="modal" class="link1">dmca infringement nitice</a>
    <a href="#myModal_autocomplete_counter" data-toggle="modal" class="link1">dmca counter notice</a>
</div>-->


        </div> 
    </div>
</div>
<div id="myModal_autocomplete_infri" class="modal fade" role="dialog" aria-hidden="true">
        <div class="modal-dialog" style="width:80%;">
            
            <div class="main_cont margin-top-20"> 
                 <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="fa fa-times"></i></button> 
                    <p>infringment!</p>   
                     <div style="text-align:center">
                         <button data-dismiss="modal" class="closeok" type="button">OK</button>
                     </div>
             </div>
    </div>  
</div> 
 <div id="myModal_autocomplete_counter" class="modal fade" role="dialog" aria-hidden="true">
        <div class="modal-dialog" style="width:80%;">
            
            <div class="main_cont margin-top-20"> 
                 <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="fa fa-times"></i></button> 
                    <p>coun!</p>   
                     <div style="text-align:center">
                         <button data-dismiss="modal" class="closeok" type="button">OK</button>
                     </div>
             </div>
    </div>  
</div> 