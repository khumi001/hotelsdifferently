<div class="width-row margin-top-20">
    <div class="main_cont">
        <div class="pagetitle margin-bottom-10">
            <h1><?php echo $this->page_name; ?></h1>

<!--<p align="center"><strong>TERMS AND CONDITIONS</strong></p>-->
            <p align="center">&nbsp;</p>
            <p align="center"><strong>Effective <?php echo date(DISPLAY_DATE_FORMAT, strtotime("May 1, 2015"))?></strong></p>
            <p>&nbsp;</p>


            <p><strong>Agreement between User (as defined below) and <?php echo DOMAIN_NAME;?>.</strong></p>

            <p>Welcome to  <strong><?php echo DOMAIN_NAME;?></strong>. The <strong><?php echo DOMAIN_NAME;?></strong> website (the "Site" as defined below) is comprised of various web pages operated by <strong><?php echo SITE_NAME;?></strong> (the "Company" as defined below). The Site is offered to you (as defined below) conditioned on your (as defined below) acceptance without modification of the terms, conditions, and notices contained herein (the "TERMS AND CONDITIONS POLICY" or the “TERMS AND CONDITIONS” or the “POLICY”). Your use of the Site constitutes your legally binding agreement to all such TERMS AND CONDITIONS. Please read these TERMS AND CONDITIONS carefully. For the good and adequate consideration to enter, view, browse, or use our Site, the adequacy and sufficiency of which you hereby acknowledge, you agree as follows:</p>

            <p>&nbsp;</p>

            <p><strong>SECTION I. DEFINITIONS.</strong></p>

            <p><strong>1. DEFINITIONS</strong>: As used in this POLICY, the following terms have the meanings as set forth in each definition. The word or words for which such definition is provided shall apply regardless of whether such word or words are capitalized, or in all upper case, or in all lower case letters, unless the context requires otherwise. In addition, the definitions in this TERMS AND CONDITIONS apply to our Site (as defined below). If a term is defined in an OTHER POLICY (as defined below), such definition in that OTHER POLICY shall apply only to that OTHER POLICY; if a term is not defined in an OTHER POLICY, but defined in this TERMS AND CONDITIONS, then the definition in this TERMS AND CONDITIONS shall apply to each such OTHER POLICY. For the purposes of this TERMS AND CONDITIONS, and each OTHER POLICY, unless otherwise noted, all references to  <strong><?php echo SITE_NAME;?></strong> include <strong><?php echo DOMAIN_NAME;?></strong>&nbsp;and Company (as defined below). The Site is a service for those person or persons seeking lower cost Hotel (as defined below) stays. By using the Site, you consent to all provisions described in this TERMS AND CONDITIONS.</p>

            <p>A. “Account” means the account established by the Company for either an Affiliate or a Member, as the context may require.</p>

            <p>B. “Account Number” means the unique number assigned to each Member and Affiliate by Company.</p>

            <p>C. “Actual Name” is the legal first and last name of a Member or Affiliate, or the legal name of any Commercial Entity, (i) supplied by a Member during Member registration, sign up or requesting to make a Reservation on the Site (ii) and/or the legal first and last name of a person, other than a Member,  which is supplied by a Member when seeking to book a Reservation, or change a Confirmed Reservation, under a different name than that of a Member, or (iii) supplied by an Affiliate or Potential Affiliate as part of the sign up process for an Affiliate.  Actual Name shall also mean the legal first and last name of a Member, Affiliate or a Commercial Entity, as applicable, when used as part of our “Contact Us” function. A Member shall not use their Actual Name as their Username.</p>

            <p>However, a Member or Affiliate, should they choose to use their Actual Name followed by @ and the domain name of the Affiliate’s or Member’s email provider(s) should be aware, particularly in the case of a Member (or an Affiliate, whose Actual Name the Affiliate does not want publicly disclosed), that the use of the Actual Name in an email address is not considered a best practice for a Member and may not be for an Affiliate, since if a third party is able to discover your email address, they can often surmise your Actual Name without your knowledge and use it for malicious purposes. </p>
			
			<p>The “Actual Name” is for use by the individual or Commercial Entity supplying it and the Company ONLY; it will never be displayed to any other User and it is considered PERSONALLY IDENTIFIABLE INFORMATION and will never be shared with a third party except (1) with a Service Provider to assist in providing services to Company, (2) if a Member used his Actual Name as a Username, which is prohibited, or (3) as required by law.</p>

            <p>D. “Advertisement”, or “Ad”, means an advertisement placed on our Site, with our permission, by a Commercial Advertiser.</p>

            <p>E. “Affiliate” means a person or Commercial Entity, other than a Commercial Advertiser, who has signed up for an Affiliate Account on the Site, whether or not they have posted or otherwise promoted Company’s Services, and also includes such individuals or a Commercial Entity who have promoted Company’s Services. Once a person or entity meets this definition, then, and at all times thereafter, they are considered an Affiliate.</p>

            <p>F. “Affiliate Link” shall mean a website page or other web access provided to an Affiliate to activate access to the Site. </p>

            <p>G. “Affiliate Payment” shall be as defined in Section III(2)(H)</p>

            <p>H. “Alternative Minimum Age” shall mean you are the legal minimum age or older, such that you have the legal capacity to enter into a binding legal contract pursuant to the law of your country of citizenship and your political subdivision thereof, if any, where such age is greater than eighteen (18) years of age.</p>

            <p>I. “Banner” shall mean a Company approved banner type advertisement provided to an Affiliate in order for the Affiliate to place it on one or more of Affiliate’s websites or in other locations as chosen by the Affiliate.</p>

            <p>J. “Cancellation” shall mean a Member, who within the timeframe as set forth in our Cancellation Policy, and in complete compliance with our Cancellation Policy, cancels their Reservation(s).</p>

            <p>K. “Cancellation Policy” shall mean the policy as set forth in Section III(1)(H) of this TERMS AND CONDITIONS.</p>

            <p>L. “Commercial Advertiser” means a person or entity, other than an Affiliate, whose Company has allowed to post an Advertisement for goods or services, other than those provided by Company, on the Site, and which may consist of a link to a website other than a Site of Company, such as a banner advertisement or similar referral to such other website not owned or operated by Company.</p>

            <p>M. “Commercial Entity” shall mean any type of business formation permitted under the laws of the country and any legal subdivision thereof in which an Affiliate’s or Potential Affiliate’s business is organized, including but not limited to, a corporation, any type of partnership, sole proprietorship, “doing business as”, any type of limited liability company or corporation, or trust.</p>

            <p>N. “Company” shall mean <?php echo DOMAIN_NAME;?>, and<strong> <?php echo SITE_NAME;?> </strong> the owner and/or publisher of the Site.</p>

            <p>O. “Company Name” shall mean the name entered by a Potential Affiliate during signing up to become an Affiliate.</p>

            <p>P. “Confirmed Reservation” shall mean that a Member has accepted a Reservation and the Member has paid us in FULL for the amount specified in the Reservation, less any applicable Coupons. No Confirmed Reservation shall be issued if less than the full amount of the Reservation is paid to us by the expiration date and time of the Reservation.</p>

            <p>Q. “Electronic Signature Act” shall have the meaning as set forth in Section IV(16).</p>

            <p>R. “Email Address” shall mean an email address where an Affiliate, Member, Potential Affiliate or Potential Member may be reached by Company staff.</p>

            <p>S. “Hotel” shall mean a hotel, motel, or similar facility which offers lodging to guests for a payment for a single night or longer.</p>

            <p>T. “Home Page” shall mean the initial page seen by Users upon accessing or logging into the Site.</p>

            <p>U. “Member” means a person who enters the Site, other than as an Affiliate or, Commercial Advertiser,  or Potential Member and who has applied for, or signed up with the Site, for a Member Account and been granted a Membership  in order to browse the Site and/or potentially or actually use Company’s Services.</p>

            <p>V. Membership shall mean having become a Member.</p>

            <p>W. “Minimum Age” shall mean either eighteen (18) years of age or older where such age is sufficient to enter into a legally binding contract, or where eighteen (18) years of age is insufficient to enter into a legally binding contract, the Alternative Minimum Age or older.</p>

            <p>X. “OTHER POLICIES” shall mean this Site’s TERMS AND CONDITIONS, INTELLECTUAL PROPERTY POLICY, PRIVACY POLICY, DMCA policy, FAQ (Frequently Asked Questions) and all other terms and conditions of this Site that govern access to, the browsing or viewing thereof, and/or the use of any feature, of this Site.</p>

            <p>Y. “our”, “us”, and “we” mean the Company.</p>

            <p>Z. “PERSONALLY IDENTIFIABLE INFORMATION” shall be as defined in our PRIVACY POLICY.</p>

            <p>A1. “Potential Affiliate” means any person or Commercial Entity who visits this Site in contemplation of becoming an Affiliate of Company and/or who has applied for to become an Affiliate but whose Affiliate status has not been approved. </p>

            <p>A2. “Potential Member” means any person who visits this Site in contemplation of becoming a Member of this Site and/or who has applied for Membership but whose Membership has not been approved.</p>

            <p>A3. “Reservation” shall mean the United States Dollar or Dollar and Cents amount that Company provides in response to a Member’s request(s) for the price of a Hotel stay, based upon the information furnished by the Member to the Company.</p>
			<!-- <p>In the event we decline to issue a Confirmed Reservation within the aforementioned time frame, Member shall promptly be notified by email of such event.</p>
			
			<p>Member shall then have to re-enter the Site and either attempt to Book such Hotel Room at the price currently displayed at such time, choose another Hotel or simply elect not to try and Book a Hotel using the Site. Members should also be aware that credit and debit card issuers may take several days or longer to post our credit to your account, even though we will issue the credit automatically if we decline to issue a Confirmed Reservation. <strong><u>Any delays of the posting of our refund after we initiate the refund are the result of your credit or debit card issuer’s policies and we have no control over how soon our credit may post to your account.</u></strong> We always suggest you call the customer service number found on the back of most debit/credit cards and ask when you can expect your credit to post to your account.</p> -->

            <p>A4. “Rate” shall mean the dollar amount charged by Company to a Member for a Hotel room or rooms for one or more nights.</p>

            <p>A5. “Services” shall mean the provision, by Company, of Reservations and/or bookings for Hotel stays.</p>

            <p>A6. “Service Provider” shall mean a person or entity contracted with Company in order to assist Company in the provision of its Services.  A Service Provider may not use PERSONALLY IDENTIFIABLE INFORMATION for ANY purpose other than the business purposes of Company in order to assist Company in the provision of its Services and such Service Provider is likewise prohibited from any disclosure of any PERSONALLY IDENTIFIABLE INFORMATION to any third party.</p>

            <p>A7. “Site” shall mean this website (<?php echo DOMAIN_NAME;?>sm) or any other website or website page wholly owned or published by Company, but not any website of an Affiliate, Commercial Advertiser or any other third party:</p>

            <p style="margin-left:.5in;">(i) to which a User may be redirected as a result of the User selecting a particular function or use from within <strong><?php echo DOMAIN_NAME;?><sup>sm</sup>,</strong> or</p>

            <p style="margin-left:.5in;">(ii) which the User enters directly by typing in the website address of such other website or page into their internet browser, or</p>

            <p style="margin-left:.5in;">(iii) which the User enters by copying and pasting such website address of such other website or page in their internet browser, or by means of the User selecting a bookmark the User has created, or</p>

            <p>(iv) which the User enters by any other means.</p>

            <p>A8. “THIRD PARTY ACCOUNTS” shall mean accounts which are not operated by Company but which a User may elect to utilize or link their Company account, including, but not limited to, social media platforms and websites.</p>

            <p>A9. “User” shall mean any person who accesses this Site for ANY reason, including, but not limited to, an Affiliate, a Member, a Commercial Advertiser, a Potential Affiliate, a Potential Member, or any other person entering this Site for any purpose, except as otherwise set forth in this Site’s PRIVACY POLICY.</p>

            <p>A10. “Username” is a name chosen by a Member or Potential Member during the process of requesting to become a Member. This MUST be a name OTHER than the Actual Name of the person supplying this information. The Username is necessary to display as part of the Member’s login, for promotional purposes by Company on the Site, and may also be displayed in other features of the Site, and may be visible to other User’s from multiple locations in the Site. An example of a Username might be “TRAVELALOT” or “HOTELHUNTER”. In no event is a Username considered to be a Member’s PERSONALLY IDENTIFIABLE INFORMATION.</p>

            <p>A11. “Voluntary Data Collection” shall be as defined in our PRIVACY POLICY.</p>

            <p>A12. “you” and “your” shall mean any User.</p>
			
			<!-- <p>A13. “Service Provider” shall mean a person or entity contracted with Company in order to assist Company in the provision of its Services.  A Service Provider may not use PERSONALLY IDENTIFIABLE INFORMATION for ANY purpose other than the business purposes of Company in order to assist Company in the provision of its Services and such Service Provider is likewise prohibited from any disclosure of any PERSONALLY IDENTIFIABLE INFORMATION to any third party.</p>
			
			<p>A14. “Site” shall mean this website (<?php echo DOMAIN_NAME;?>sm) or any other website or website page wholly owned or published by Company, but not any website of an Affiliate, Commercial Advertiser or any other third party:</p>
			
			<p style="margin-left:.5in;">(i) to which a User may be redirected as a result of the User selecting a particular function or use from within <strong><?php echo DOMAIN_NAME;?><sup>sm</sup>,</strong> or</p>

            <p style="margin-left:.5in;">(ii) which the User enters directly by typing in the website address of such other website or page into their internet browser, or</p>

            <p style="margin-left:.5in;">(iii) which the User enters by copying and pasting such website address of such other website or page in their internet browser, or by means of the User selecting a bookmark the User has created, or</p>

            <p>(iv) which the User enters by any other means.</p>
			
			<p>A15. “THIRD PARTY ACCOUNTS” shall mean accounts which are not operated by Company but which a User may elect to utilize or link their Company account, including, but not limited to, social media platforms and websites.</p>
			
			<p>A16. “USD” shall mean United States of America Dollar(s).</p>
			
			<p>A17. “User” shall mean any person who accesses this Site for ANY reason, including, but not limited to, an Affiliate, a Member, a Commercial Advertiser, a Potential Affiliate, a Potential Member, or any other person entering this Site for any purpose, except as otherwise set forth in this Site’s PRIVACY POLICY.</p>
			
			<p>A18. “Voluntary Data Collection” shall be as defined in our PRIVACY POLICY.</p>
			
			<p>A19. “you” and “your” shall mean any User.</p> -->
			
			

            <p><strong>SECTION II. GENERAL</strong></p>

            <p><strong>1. TERMS AND CONDITIONS-GENERALLY. </strong>.  In addition to the specific contents of this POLICY and OTHER POLICIES, all provisions as set forth in each and every page, portion, section, html page or subdomain of this Site are also terms and conditions of use under this POLICY.</p>

            <p><br />
                <strong>2. ELECTRONIC SIGNATURE. </strong>By entering our Site, you are deemed to have accepted, and signed this TERMS AND CONDITIONS POLICY AND OUR OTHER POLICIES pursuant to the Electronic Signature Act; see also Section IV(16) of this POLICY.</p>

            <p><br />
                <strong>3. OTHER POLICIES. </strong>Your use of the Site is subject to Company's OTHER POLICIES as well as this POLICY. Please review ALL of these OTHER POLICIES, including, but not limited to, our PRIVACY POLICY, DMCA policy, and INTELLECTUAL PROPERTY POLICY, which also govern the Site and informs Users of our data collection practices and other practices and policies. You may find links to any of these OTHER POLICIES and this POLICY on the Home Page of this Site.</p>

            <p><br />
                <strong>4. ELECTRONIC COMMUNICATIONS. </strong>Visiting the Site or sending emails to Company or receiving an email from Company in response to an email sent by you constitutes electronic communications. You consent to receive electronic communications and you agree that all agreements, notices, disclosures and other communications that we provide to you electronically, via email and on the Site, satisfy any legal requirement that such communications be in writing.</p>

            <p>A. For all Users, if you OPTED IN for our newsletter, you will not consider our newsletter as SPAM. You may opt out of the newsletter at any time by using the unsubscribe hyperlink that you will receive in any newsletter.</p>

            <p>B. For all Members or Potential Members, we may send you emails in order to provide, or to potentially provide, Services to you and related functions. These may include, but are not limited to, activating your Member Account, letting you know that you have used a wrong Account activation link, or that you have previously activated your account , sending you a Reservation, to advise if your request for Membership has been declined, suspended or terminated, to verify any change in Account information such as a change in your email address, password, a forgotten password or other Account information, in response to a request or information you seek by using the Contact Us function of the Site, sending you a Confirmed Reservation, a link for our REFER A FRIEND offering, and other matters related to your Account or changes thereto, and/or our Services, and you will not consider such emails as SPAM. If you OPTED IN for our newsletter, you will not consider our newsletter as SPAM. You may opt out of the newsletter at any time by using the unsubscribe hyperlink that you will receive in any newsletter.</p>

            <p>C. For Affiliates or Potential Affiliates, we may send you emails in order to provide, or to potentially provide, with your benefits and activities as an Affiliate, and related functions. These may include, but are not limited to, activating your Affiliate Account, letting you know that you have used a wrong Account activation link, or that you have previously activated your account , to advise if your request for Affiliate status has been declined, suspended or terminated, to verify any change in Account information such as a change in your email address, password, a forgotten password, payment information or other Account information, in response to a request for information you seek by using the Contact Us function of the Site, sending you a link to activate your Affiliate Status, and other matters related to your Account and/or our Services, and you will not consider such emails as SPAM. You may opt out of the newsletter at any time by using the unsubscribe hyperlink that you will receive in any newsletter.  </p>

            <p>D. For Affiliates and Members, at present we do not intend to send you any emails other than those other than those set forth in Sections II(4)(A) and II(4)(B), above. Should we decide, at a later date, to offer to send you emails for other purposes, such as an opportunity to receive special offers, etc., you will be given an opportunity to Opt In or Out of such emails, and you will not consider any offering to Opt In or Opt Out to be “SPAM”.</p>

            <p><br />
                <strong>5. YOUR ACCOUNT AND YOUR IP ADDRESS. </strong>If you use this Site, you are responsible for maintaining the confidentiality of your Account and password and for restricting access to your computer, and you agree to accept responsibility for all activities that occur under your Account or password. You may not assign, disclose, or otherwise transfer your Account to any other person or entity, nor to allow such other person or entity to access your Account. You acknowledge that Company is not responsible for third party access to your Account that results from theft or misappropriation of your Account.</p>

            <p>Your IP address (a) may be suspended for six (6) hours if you unsuccessfully attempt five times to log into the Site, or (b) in the sole discretion of Company, suspended or permanently banned for activity in contravention of this POLICY, or any OTHER POLICY, (c) for actual or perceived fraud, or (d) any other reason Company deems such suspension or ban is in Company’s best interest.</p>
			<br/>

            <p><strong>6. PERSONS UNDER THE MINIMUM AGE/DENIAL OF ACCESS TO SITES/BYPASSING PROTECTIONS.</strong></p>

            <p>A. Company does not permit the access, viewing or utilization of this Site, or knowingly collect, either online or offline, personal information, from persons under the Minimum Age. You are solely responsible if a person under the Minimum Age accesses your Account, and you indemnify and hold harmless the Company if a person under the Minimum Age accesses your Account.</p>

            <p>B. You represent and warrant to Company that you are at least of Minimum Age, understand and agree to abide by the laws and regulations of the location(s) from which you access this Site</p>

            <p>C.You shall not, by means of “favorites” or “bookmarks” or like functions on any internet browser, or by any manual or automated process, gain access to any portion of our Site that allows you or any other person or entity to bypass agreeing to our TERMS AND CONDITIONS or OTHER POLICIES. If you enter the Site in violation of the foregoing, you or any other person or entity have accepted the TERMS AND CONDITIONS, and all OTHER POLICIES, and your entrance to the Site is your legally binding acceptance and signature of agreement of same, dated as of the date you entered the Site.</p>

            <p>D.Our Site, and the software used therein, are governed by the laws of United States of America, and are subject to its import and export laws. This Site and software may not be exported to any country where such exportation is illegal, or to any country in which its importation is illegal.</p>

            <p>E. You expressly indemnify and hold harmless the Company for any claims of any nature, losses or damages as a result of your failure to abide by the provisions of this Section 6.</p>

            <p><br />
                <strong>7. LINKS TO THIRD PARTY SITES/THIRD PARTY SERVICES</strong></p>

            <p>A. This Site may contain links to other websites ("Linked Sites") provided by Commercial Advertisers, social media providers, or others. The Linked Sites are not under the control of Company and Company is not responsible for the contents of any Linked Site, including without limitation, any link contained within in a Linked Site, or any changes or updates to a Linked Site. Company is providing these Linked Sites to you only as a convenience, and the inclusion of any Linked Site does not imply endorsement by Company of the Lined Site or any association with its operators, except that</p>

            <p style="margin-left:.5in;">&nbsp;(i) with regard to a link within this Site to a Commercial Advertiser to view and/or interact with a Commercial Advertiser’s Advertisement (the “Commercial Advertiser Link”) on this Site does mean that Company may have accepted payment from a Commercial Advertiser for such Advertisement. The Company does not endorse any particular Commercial Advertiser and does not warrant the Advertisement of any Commercial Advertiser, including any links included within a Commercial Advertiser Link. Company does not refer you to any particular Commercial Advertiser, and does not provide any contact information for a Commercial Advertiser other than the contact information provided by a Commercial Advertiser in their Advertisement.</p>

            <p>B. Certain services made available via the Site, including, but not limited to Site hosting and Service Providers, are delivered by third party sites and organizations. By using any product, service or functionality originating from the Site domain, you hereby acknowledge and consent that Company may share such information, and data with any third party with whom Company has a contractual relationship to provide the requested product, service or functionality on behalf of Site Users. Provided however, Site hosting services and other Service Providers are prohibited from using any PERSONALLY IDENTIFIABLE INFORMATION for any purpose other than to provide their services to our Site.</p>

            <p>C. We enter into promotional relationships on a commercial basis with Affiliates to promote or market our Site by a variety of methods. We do not endorse, and we are not responsible or liable for any content of such promotions or the types of promotions entered into on our behalf by a third party.</p>

            <p><br />
                <strong>8. LIMITATIONS. </strong>You are granted ONLY, a non-exclusive, non-transferable, revocable, at our sole  discretion and without prior notice, license to (a) access and use the Site strictly in accordance with this TERMS AND CONDITIONS POLICY and this Site’s OTHER POLICIES and (b) to your Username and Password. As a condition of your use of the Site, you warrant to Company that you will not use the Site for any purpose that is unlawful or prohibited by these TERMS AND CONDITIONS or OTHER POLICIES. You may not use the Site in any manner which could damage, disable, overburden, or impair the Site or interfere with any other party's use and enjoyment of the Site. You may not obtain or attempt to obtain any materials or information through any means not intentionally made available or provided for through the Site.</p>

            <p>We do not grant you any licenses, express or implied, to the intellectual property or proprietary information of Company or our licensors except as expressly authorized by these TERMS AND CONDITIONS or OTHER POLICIES.</p>

            <p>We plan, in the future, to offer access to the Site, other than by internet web browsers, through applications for smart phone, smart watches, tablets, etc. When we do, we will amend this TERMS AND CONDITIONS and OTHER POLICIES as necessary to provide for the use of such applications.</p>

            <p><br />
                <strong>9.&nbsp; MATERIALS PROVIDED TO SITE OR POSTED ON ANY COMPANY WEB PAGE</strong></p>

            <p>A. Company does not claim ownership of the materials you provide to Site (including feedback and suggestions) or post, upload, input or submit to any Company Site or our associated services which is Voluntary Data Collection. However, by posting, uploading, inputting, providing or submitting Voluntary Data Collection, you are granting Company, our affiliated companies and necessary sub licensee’s permission to use Voluntary Data Collection in connection with the operation of their internet businesses including, without limitation, the rights to: copy, distribute, transmit, publicly display, publicly perform, reproduce, edit, translate and reformat Voluntary Data Collection. Voluntary Data Collection DOES NOT INCLUDE PERSONALLY IDENTIFIABLE INFORMATION.</p>

            <p>B. No compensation will be paid with respect to the use of Voluntary Data Collection as provided herein. Company is under no obligation to post or use any Voluntary Data Collection. You may provide and we may remove any Voluntary Data Collection at any time in Company's sole discretion.</p>

            <p>C. By posting, uploading, inputting, providing or submitting Voluntary Data Collection you warrant and represent that you own or otherwise control all of the rights to Voluntary Data Collection as described in this section including, without limitation, all the rights necessary for you to provide, post, upload, input or submit such information.</p>

            <p>D. In no event do we warrant, under any theory of equity or of law, the contents of this Site, whether submitted by an Affiliate, Member, Potential Affiliate, Potential Member or Commercial Advertiser, person or entity promoting our Site by any means. Your choice to interact with, purchase from, or follow a link to another website, or to an Affiliate or Commercial Advertiser is solely your choice and you assume all risks for any such action. Other than for your PERSONALLY IDENTIFIABLE INFORMATION, we make no representation as to the safety, security or otherwise, of the contents of this Site. Other than your PERSONALLY IDENTIFIABLE INFORMATION, you have no right of, or expectation of a right of, privacy of any information you submit to this Site except as otherwise explicitly provided for in this TERMS AND CONDITIONS and OTHER POLICIES.</p>

            <p>E. Our Site may include surveys/questionnaire on its Site for Users, which shall be considered a part of Voluntary Data Collection and shall not be considered “SPAM”.</p>

            <p><br />
                <strong>10. INTERACTION BETWEEN USERS</strong>.</p>

            <p>A. Company does not have access to, and therefore does not monitor, any communications or interactions of whatever nature between Users on this Site, including by and among Affiliates and Members. Affiliates and Members ARE SOLELY responsible for any interactions by and among them, on any other website or by any other means.</p>

            <p>B. You absolutely hold harmless and indemnify Company from any actions or inactions, or damages or losses of any nature as a result of choosing to interact with another User, whether online on another website or by any other means. Company EXPRESSLY disclaims ANY LIABILITY OF ANY NATURE WHATSOEVER AS A RESULT OF ANY INTERACTION BETWEEN USERS, ON ANY OTHER WEBSITE OR BETWEEN USERS BY ANY OTHER MEANS.</p>

            <p><br />
                <strong>11. USER SUBMISSION</strong>: Each User, including, but not limited to, Affiliates and Members:</p>

            <p>A. agrees that:</p>

            <p style="margin-left:.5in;">(i) Company is not responsible and does not represent or warrant the accuracy or veracity of any User Submission. As used in this Section 11, “User Submission” means, with regard to a:</p>

            <p style="margin-left:.5in;">1. Member:  Any content in any Voluntary Data Collection </p>

            <p style="margin-left:.5in;">2. Affiliate: Any content in any Voluntary Data Collection   </p>

            <p style="margin-left:.5in;">3. Commercial Advertisement: Any content in a Commercial Advertisement, including any content in a website owned or operated by a Commercial Advertiser.</p>

            <p style="margin-left:.5in;">4. Any information on any website of any other third party, including social media sites.</p>

            <p>B. agree that no User has any confidential information or proprietary rights with regard to any Voluntary Data Collection, nor shall any User misappropriate any confidential information or proprietary rights of any other person in Voluntary Data Collection or in an Advertisement.</p>

            <p>C. each User is aware that you may be exposed to another User’s submission which may be objectionable, inaccurate, or not useful, and to which such other User may or may not have intellectual or other property rights or proprietary interest in such User submission, and the Company bears no responsibility or liability for such User submissions.</p>

            <p>D. Company does review or monitor User submissions made directly to Company. In addition, the Company reserves the right to delete User submissions, including Commercial Advertisements, so long as it does not conflict with Applicable Law, it is under no obligation to do so.</p>

            <p>E. each User is responsible for their User submission(s) and is solely responsible for any loss, damage or other consequences of such User submission.</p>

            <p>F. a User Submission of one User may not be shared, used, reproduced, copied or otherwise used by another User excepted as permitted in this policy.</p>

            <p>G. No User may attempt to, or actually upload any picture, graphic or other Image to any portion of the Site, by any means. As used herein, “Image” shall mean any photographic, graphic, artwork or similar visualization, whether or not such Image includes or does not include any written or verbal component.</p>

            <p>H. All payments by a Member to the Company are processed through a third party credit/debit card processing service, using SSL or similar encryption technology. We are not responsible for the safety, security or accuracy of any information you provide to, or receive from, this third party provider; however, our commercial terms with such third party shall require that such third party be responsible to you, the Affiliate, for the safety, security, and accuracy of your information and to reverse any charge appearing as being from us that in fact was not submitted by you for payment to us.</p>

            <p>I. All our prices for , for payments to Affiliates and payment by Members, and any refund, if any, are in United States Dollars (USD). Payments to obtain a Confirmed Reservation will be charged to the Member in USD via Stripe® if the Member paid using Stripe® or in USD via Stripe® if the Member paid using Bitcoin. Payments to Affiliates will never be made using Bitcoin. Any foreign transaction fees, currency conversion fees, dynamic currency conversion (DCC) fees, or like fees incurred because the Affiliate’s or Member’s local currency is not USD will be the responsibility of the Affiliate or Member. Further, any and all fees charged by Bitcoin or Stripe® relative to a Member’s use of Bitcoin and Stripe® are the sole responsibility of the Member. Please see Section II(K) below relative to Bitcoin and Stripe®.</p>

            <p>J. No Member or Affiliate shall share his Account number or password with another person or entity, including, but not limited to, another User. Sharing of either an Account number and/or password with any other person will result in a suspension and/or termination of a Member or Affiliate in Company’s sole discretion.</p>
            <p>
                K. This Bitcoin Payment Policy (“Bitcoin Payment Policy”) applies to your use of Bitcoin as your selected payment method. With regard to ANY Bitcoin payment, the following terms and conditions apply, in order of precedence: All Bitcoin Terms and Conditions and policies (“BTC”), all Stripe® Terms and Conditions and polices (“CTC”), and our Terms and Conditions. If you do not agree with any of this Section K or our refund policy with regard to Bitcoin and Stripe®, you MUST NOT use Bitcoin as your payment method. </p>
				
<p style="margin-left:1.0in;">(i). Our acceptance of Bitcoin is through Stripe® as our exchange server. We are not responsible for, or guarantee, Stripe®’s services or the availability of such servics. To complete your payment, you will be re-directed to Stripe®’s website, where you will see the total cost of your Reservation in Bitcoin, based on the Stripe®’s exchange rate. The Bitcoin price for your Reservation will remain valid for ten (10) minutes. If you do not initiate your payment during this time, the Bitcoin exchange rate will be updated and the Bitcoin price for your Reservation may change.</p>

<p style="margin-left:1.0in;">(ii). A very small transaction fee may be added by the Bitcoin network to the total cost in Bitcoin of your Reservation if you are sending Bitcoin from a non-Stripe® wallet. This fee covers the cost of verifying Bitcoin transactions. We have no control over this fee and we do not receive any portion of the fee. </p>

<p style="margin-left:1.0in;">(iii). Bitcoin transactions are final. Once you initiate a Bitcoin transaction, you cannot cancel it; this is the policy of the Bitcoin network over which we have no control. Refunds, if any, are governed by our refund policy. </p>

<!-- <p style="margin-left:1.0in;">Once we receive the full payment in the Bitcoin equivalent to the outstanding USD amount for your Booking, you will receive a Confirmed Reservation.</p> -->

<p style="margin-left:1.0in;">(iv). If you attempt a payment via BitCoin wallet, in an amount of Bitcoins different from the exact cost of the Reservation indicated by Stripe®, your Confirmed Reservation will not complete. If, using a non-Stripe® wallet, you initiate a payment of Bitcoins different from the exact cost (plus transaction fees) of the Reservation indicated by Stripe®, your Confirmed Reservation will not complete; however your payment maybe processed by the Bitcoin network. If that incorrect payment is processed, your payment will be refunded by Stripe®. After an incorrect payment you will be required to re-submit payment for the correct amount in order to receive a Confirmed Reservation and the Bitcoin exchange rate may change (which we do not control). </p>

<p style="margin-left:1.0in;">5. Transactions complete once confirmed. Once a Bitcoin transaction is submitted to the Bitcoin network, it will be unconfirmed for a period of time pending full verification of the transaction by the Bitcoin network. A transaction is not complete until it is fully verified and your Confirmed Reservation will not occur until the transaction is completely verified.</p>
            
            <p><br />
                <strong>12. MEMBERSHIP AND AFFILIATE STATUS. </strong>Membership and Affiliate status are at the sole discretion of Company, and may be denied, or suspended or revoked at any time, with or without prior notice. Further, no individual or Commercial Entity may have more than one Membership, and Users should be aware that Company has and shall use technology to determine if any individual has attempted or succeeded in obtaining more than one Membership. In such event, such individual or Commercial Entity shall have the Membership suspended or revoked, and any or all Coupons forfeited at Company’s sole discretion.</p>

            <p><br />
                <strong>SECTION III. ADDITIONAL TERMS AND CONDITIONS FOR MEMBERS AND AFFILIATES</strong></p>

            <p><strong>1. FOR MEMBERS. </strong>For Members, the following additional terms and conditions apply:</p>

            <p>A. The following information is not proprietary to you, and may, or may not, be considered PERSONALLY IDENTIFIABLE INFORMATION (see our PRIVACY POLICY). The following information will only be seen by you, and if necessary, by Company and/or Service Provider(s): your Email Address, any email directed specifically to you by Company or email sent by you to Company, information you provide to seek a Confirmed Reservation, any use by you of the Site’s Contact Us function, Opting In or Opting Out of our newsletter, My Account, My Info. My Coupons, Refer A Friend, Change Password, Change Email Address, Pending Reservations, Confirmed Reservations, FAQ, any facsimile sent to or from Us, Promo Codes and the Legal Corner and all submenus thereof. Please note the following: We don’t obtain/retain credit/debit card, Bitcoin® or checking account information on our Site; all payments are processed through the payment processor(s). See also that portion of our Site entitled: “<strong>Security Statement /We Care for Your Security</strong>” to help you understand the rigorous standards payment processor(s) are required to meet.</p>

            <p>B. Other than Voluntary Data Collection, Company does not claim ownership of the materials you provide to Site with regard to a request to become a Member.</p>

            <p>C. No compensation will be paid with respect to the use of Voluntary Data Collection as provided herein. Company is under no obligation to post or use any Voluntary Data Collection. You may provide and we may remove any Voluntary Data Collection at any time in Company's sole discretion.</p>

            <p>D. The use of fictitious names, whether by an individual or by Commercial Entities is FORBIDDEN.</p>

            <p>E. Reservations are valid ONLY for forty-eight (48) hours commencing at the time we email the Reservation to you. We are not responsible for any delays or failure of you to receive a Reservation in a timely fashion including, but not limited to, any issues with a third party server, internet connection, or your email system.</p>

	     <!-- <p style="margin-left:35.0pt;">(i) Definitions:</p>

		<p style="margin-left:65.0pt;">1. Cancellation Period shall mean that period of time found for the Hotel you have selected, by looking under the column labeled “Policy” and going beneath that column and clicking upon “Rates and Policies”. YOU SHOULD ALWAYS CLICK UPON RATES AND POLICIES; in addition to the foregoing, it will display room availability for the type of room you have selected; pricing per night (which may vary for different days of the week if you selected a stay of more than one night), and a total cost for a Confirmed Reservation for the date(s) you have selected.</p>
		
		<p style="margin-left:65.0pt;">2. In your Confirmed Reservation from us to you, the Cancellation Period begins at the time your Confirmed Reservation is emailed by us to you, and ends as of the check in date and time of the Hotel at the location of the Hotel designated in your Confirmed Reservation. By way of example, and not limitation, if your Hotel stay begins on August 11, and your Confirmed Reservation states you have a seventy-two (72) hour cancellation period, and the Hotel’s check in time is 11:00AM (all Hotel check in times and dates are the local times and dates where the where the Hotel is located), then you would have seventy two (72) hours prior to 11:00AM on August 11 <strong>(<u>measured by the Hotel’s local date and time</u>)</strong> in order to cancel. <strong>The Cancellation Period is NOT measured by the Member’s local date and time.</strong></p>
		
		<p style="margin-left:65.0pt;">3. Cancellation and Refund Policy shall mean this Section III(1)(E) of this Terms and Conditions.</p>
		
		<p style="margin-left:65.0pt;">4. Chargeback shall mean any attempt of any nature by Member to seek a credit or refund from a third party including, but not limited to, a credit or debit card issuer, processor, and merchant bank or like entity, Bitcoin or a Bitcoin wallet where we have provided the Services in accordance with the Terms and Conditions.</p>
		
		<p style="margin-left:65.0pt;">5. The Confirmed Reservation for a specific Hotel or Hotels provided by us to you is based upon the information you supplied to us in requesting the Confirmed Reservation. When we send you a Confirmed Reservation, the Confirmed Reservation will specify the specific Cancellation Policy applicable to that Confirmed Reservation if it was not listed under the Rates & Policies listing for the Hotel on our Site at the time you Booked your Confirmed Reservation Please, and this is EXTREMELY IMPORTANT; THE CANCELLATION POLICY IS THAT SHOWN WHEN YOU BOOK A CONFIRMED RESERVATION; NOT AT SOME LATER POINT IN TIME (SINCE THESE HOTEL POLICIES CAN CHANGE FROM HOUR TO HOUR OR EVEN MORE FREQUENTLY AS HOTELS RECEIVE MORE BOOKINGS). SO MAKE SURE, IF YOU HAVE STARTED THE PROCESS TO BOOK A HOTEL OR ANY OTHER SERVICE, BUT WERE THEN INTERRUPTED, MAKE SURE YOU CHECK THE CANCELLATION POLICY IMMEDIATELY PRIOR TO CHECKOUT!</p>
		
		<p style="margin-left:65.0pt;">6.“No Show”.Under the cancellation policy on our Site, “No Show” means that if for any reason you are not going to be able to arrive at the Hotel by its stated check in time, it is very important that you call the Hotel directly and advise them that your arrival will be delayed. Each Hotel has its own policy in this regard as to how long they will continue to hold your Confirmed Reservation and what the no show penalty is. In addition, we always recommend that EACH MEMBER always check with a Hotel, even if they do not specify their “No Show” penalty and/or any other Service Provider, e.g., a rental car company as to what their policies are in the event you are unable to arrive by the time the Service Provider has listed as your expected arrival time.</p>
		
		<p style="margin-left:65.0pt;">7. Payment shall mean the payment, in full, irrespective of manner or form used by you to pay us for the Confirmed Reservation.  No Confirmed Reservation shall be issued unless payment in full has been received by us.</p>
		
		<p style="margin-left:65.0pt;">8 Refund shall mean a payment by us, if any, to you, for a cancellation of a Confirmed Reservation where such cancellation by you is in complete compliance with our Terms and Conditions. The Refund amount shall be the full amount paid to us for the Confirmed Reservation subject to SectionIII(E)(ii)(9) immediately below if you used Bitcoin as your payment method. 8 Refund shall mean a payment by us, if any, to you, for a cancellation of a Confirmed Reservation where such cancellation by you is in complete compliance with our Terms and Conditions. The Refund amount shall be the full amount paid to us for the Confirmed Reservation subject to SectionIII(E)(ii)(9) immediately below if you used Bitcoin as your payment method. </p>

	<p style="margin-left:35.0pt;">(ii.)Cancellation, Bitcoin, Refund.</p>

		<p style="margin-left:65.0pt;">1. We reserve the right to cancel your Confirmed Reservation if our Service Provider that provided us the pricing information for your Confirmed Reservation provided us with incorrect pricing information. In such a case, your payment for the Confirmed Reservation will automatically be refunded in USD or its then Bitcoin equivalent and you will be notified by email. In no event shall we be liable in any other manner than to refund your funds paid to us if we cancel said Confirmed Reservation because of incorrect pricing information supplied to us by one of our Service Providers. Please see also Section IV(4).</p>
		
		<p style="margin-left:65.0pt;">2. Whether your Confirmed Reservation qualifies for a refund is governed by this Terms and Conditions policy, regardless of whether you pay with Bitcoin or any other payment method. In order to receive a refund (if we grant one) on a Confirmed Reservation paid for with Bitcoin, you will have to follow certain procedures in order to claim your refund: </p>

			<p style="margin-left:85.0pt;">(a). A valid email address and your Bitcoin wallet address are required for refunds. Due to the nature of the way Bitcoin transactions are handled, we must process refunds related to Bitcoin payments manually. There are two steps you must take:</p>
			
			<p style="margin-left:85.0pt;">1. You must cancel the reservation by logging into your account, and go to “Reservations” and under Reservations click on “Confirm Reservations” and then click on “Cancel”, and</p>
			
			<p style="margin-left:85.0pt;">2. Then contact us, using the Contact Us function and choose the drop down menu labeled “Billing” and provide us your confirmation number for your Confirmed Reservation and your Bitcoin address. If you do not know your Bitcoin address, you will need to contact your Bitcoin wallet issuer.</p>
			
			<p style="margin-left:85.0pt;">(b). If you fail to send us your correct Bitcoin wallet address within (30) days after you cancelled your reservation, we will be unable to refund your Bitcoin.</p>
			
			<p style="margin-left:85.0pt;">(c) Refunds, if any, for Bitcoin transactions are issued for the then current USD value of the amount of Bitcoin you original paid at the time you originally paid, and refunds are issued only as set forth in this Policy. Your refund will be issued in Bitcoin for that USD value, less any applicable fees, including any cancellation fees. Your refund will be converted from USD to Bitcoin based on an exchange rate set by Stripe® at the time we initiate the refund. You acknowledge that if the value of Bitcoin against USD has risen since the time of your purchase, you will receive less in Bitcoin than you might otherwise have received had you paid via a payment method other than Bitcoin (e.g., a debit or credit card).</p>

		<p style="margin-left:65.0pt;">3. No Refund shall be made if the Member stays for a shorter period of time than that set forth in the Confirmed Reservation.</p>
		
		<p style="margin-left:65.0pt;">4. No Refund shall be made if the Member cancels directly with the Hotel.</p>
		
		<p style="margin-left:65.0pt;">5. Under no circumstances shall a Member attempt to receive, or receive, any credit or payment, whether in whole or in part, of any funds paid by us to Hotel. Member understands and agrees that we will take any and all steps necessary, including legal action, and including without limitation, recovery of attorney’s fees, legal costs, other fees and any other damages to which we are entitled, to recover such credit or payment.</p>
		
		<p style="margin-left:65.0pt;">6. Member understands and agrees that we will dispute any Chargeback to the fullest possible legal extent, including but not limited to, the commencement of legal action including without limitation, recovery of attorney’s fees, legal costs, other fees and any other damages to which we are entitled, to recover such credit, or reversal of Payment to the Member and in addition, we may choose, in our sole discretion, to report to the credit/debit card issuer any fraudulent Chargeback.</p>
		
		<p style="margin-left:65.0pt;">7. If a Member is entitled to a refund on a Confirmed Reservation, and used a Coupon toward the payment for that Confirmed Reservation, the Member will be refunded in USD less the USD value of the Coupon used, and the Coupon that was used will be reinstated. By way of example, and not limitation, John had a Confirmed Reservation where the Hotel room rate was $100.00USD and John used a Coupon valued at $20.00USD (with John paying us $80.00USD). John would be refunded $80.00USD (the amount he actually paid us) and John’s Coupon valued at $20.00 would be reinstated for use in the future, regardless of what our API information displays.</p> -->

      <p>F. A reservation becomes a Confirmed Reservation ONLY if you accept both the Reservation and make Payment in full to us within the timeframe set forth in Section III(1)(E) above. If you send an amount less than Payment in full and do not send the remaining balance for the Reservation within the time frame set forth in Section III(1)E above, you will not be entitled to a Confirmed Reservation.</p>
	
	<!-- <p style="margin-left:35.0pt;">(i). Coupons and/or Promo Codes are an amount, in USD, that will be automatically deducted from your Confirmed Reservation if you elect to redeem one of your earned Coupons and/or Promo Codes. No more than one (1) Coupon or Promo Code may be redeemed for any single Confirmed Reservation.</p>

	<p style="margin-left:35.0pt;">(ii). General Terms and Conditions for earning Promo Codes and/or Coupons.</p>

		<p style="margin-left:65.0pt;">1. The ways in which to earn or receive Promo Codes and/or Coupons, the number of Promo Codes and/or Coupons that may be used on any single Confirmed Reservation, the expiration date of Promo Codes and/or Coupons, or the maximum number or dollar amount of Promo Codes and/or Coupons that may be accumulated, may be changed, or discontinued at any time by Company, without prior notice except as otherwise set forth in this SectionIII(1)(F).</p>
		
		<p style="margin-left:65.0pt;">2. Company does place an expiration date on all type(s) of Coupon(s) and/or Promo Code(s). Any changes in expiration date(s) of any Coupon(s) and/or Promo Code(s), including those previously issued will be reflected by either an email to you or a prominent notice on the Site.</p>

		<p style="margin-left:65.0pt;">3. If there is a change by Company in the number of Coupons and/or Promo Codes that may be used on any single Confirmed Reservation, how Coupons and/or Promo Codes may be earned, or the maximum number or dollar amount of Coupons and/or Promo Codes that may be accumulated, any such change(s) will be reflected on the Site.</p>
		
		<p style="margin-left:65.0pt;">4. At the present date and time, there is no maximum number or dollar amount of Coupons and/or Promo Codes that may be accumulated by a Member; all of which may change or be discontinued as set forth herein.</p>
		
		<p style="margin-left:65.0pt;">5. Affiliate Bonus.  A Potential Member MAY receive a Coupon in the amount set forth on the Site as of the date the Potential Member uses an Affiliate Banner or an Affiliate Link to enter the Site and such Potential Member becomes a Member (the “New Member"). The Coupon will be credited to the New Member instantly upon becoming a Member, but such Coupon may be used ONLY for a Confirmed Reservation in the amount of Two Hundred Fifty USD ($250.00) or more Further, a Coupon received as an Affiliate Bonus may not be combined with any other Coupon(s) or Promo Code(s) on any single Confirmed Reservation.</p>

		<p style="margin-left:65.0pt;">6. [Reserved For Future Use Of a Possible Refer a Friend Bonus.]</p>

		<p style="margin-left:65.0pt;">7. Direct Sign Up Bonus. A Member may receive a Coupon in the amount set forth on the Site as of the date the Potential Member accesses the Site directly and such Potential Member becomes a Member. The Coupon will be credited to the Member instantly if this feature is activated in the Company’s sole discretion. Company may change the availability of the Direct Sign Up Bonus at any time without prior notice.</p>
		
		<p style="margin-left:65.0pt;">8. A New Member may receive only an Affiliate Bonus or perhaps a Refer a Friend Bonus or a Direct Sign Up Bonus if Company elects to make such other bonus or bonuses available; a Member shall be entitled to only ONE bonus depending on the method the Potential Member used initially to sign up as a Member.</p>
		
		<p style="margin-left:65.0pt;">9. If a Member uses a Coupon that has a value greater than the amount of the Booking, the Booking will be paid in full; however, the balance remaining on that Coupon will be forfeited and the balance remaining on that Coupon cannot not be used on any other Booking.</p>
		
		<p style="margin-left:65.0pt;">10. Coupons may be applied for the Hotel room rate, or the cost of other Services, but Coupons cannot be applied for any taxes on the Hotel room rate or the taxes on any other Service.</p> -->

            <p>G. Once a Cancellation has been received by us, the previous Reservation made by us to make the Reservation which is being cancelled, is no longer valid. A Member must submit a new request for a Reservation once a legitimate Cancellation is made by the Member and then the Member must accept (make a Reservation and make payment in full) or decline the new Reservation (by not accepting the Reservation and not paying in full for the Reservation); all within the time frames set forth in the Cancellation Policy.</p>
            <p>H. CANCELLATION AND REFUND POLICY:</p>

	<p style="margin-left:35.0pt;">(i). Definitions and Special Bitcoin policy:</p>
      <p style="margin-left:45.0pt;">a. Cancellation Period shall mean that period of time stated in your Confirmed Reservation, such period beginning at the time your Confirmed Reservation is emailed by us to you, and ending as of the check in date and time of the Hotel at the location of the Hotel designated in your Confirmed Reservation. By way of example, and not limitation, if your Hotel stay begins on August 11, your Confirmed Reservation states you have a seven (7) day cancellation period, and the Hotel’s check in time is 11:00AM (all Hotel check in times and dates are the local times at dates where the where the Hotel is located), then you would have seven days, (168 hours) prior to 11:00AM on August 11 (measured by the Hotel’s local date and time) in order to cancel. The Cancellation Period is NOT measured by the Member’s local date and time.</p>  
      <p style="margin-left:45.0pt;">b. Cancellation and Refund Policy shall mean this Section III(1)(H) of this Terms and Conditions.</p>
      <p style="margin-left:45.0pt;">c. Chargeback shall mean any attempt of any nature by Member to seek a credit or refund from a third party including, but not limited to, a credit card issuer, processor, merchant bank or like entity where we have provided Services in accordance with the Terms and Conditions.</p> 
      <p style="margin-left:45.0pt;">d. Reservation-The quote or Reservations for a specific Hotel or Hotels provided by us to you based upon the information you supplied to us in requesting the quote. When we send you a Reservation, the Reservation will specify the specific Cancellation Policy applicable to that Reservation.</p>
      <p style="margin-left:45.0pt;">e. Payment-shall mean the payment, in full, irrespective of manner or form used by you to pay us, in full, for the Reservation.  No Confirmed Reservation shall be issued unless payment in full has been received by us.</p>
      <p style="margin-left:45.0pt;">f. Refund shall mean a payment by us, if any, to you, for a cancellation of a Confirmed Reservation where such cancellation by you is in complete compliance with our Terms and Conditions. The Refund Amount shall be the full amount paid to us for the Confirmed Reservation subject to III(H)(i)g immediately below if you used Bitcoin as your payment method. If a Member paid only a partial payment(s) but never completely paid in full for the Reservation within the required timeframe for a Confirmed Reservation, the amount of the refund, if any, shall be the total amount of partial payments made subject to III(H)(i)g immediately below if you used Bitcoin as your payment method.</p>
      <p style="margin-left:45.0pt;">g. Whether your Confirmed Reservation qualifies for a refund is governed by this Terms and Conditions policy, regardless of whether you pay with Bitcoin or any other payment method. In order to receive a refund (if we grant one) on a Confirmed Reservation paid for with Bitcoin, you will have to follow certain procedures in order to claim your refund: </p>
      <p style="margin-left:50.0pt;">1. A valid email address and a Stripe® account are required for refunds. Your refund will be issued through Stripe®, and you must have (or create) a Stripe® account in order to receive the refund. Stripe will send a refund notification email to the email address that you provided to Stripe®. THE EMAIL ADDRESS OF YOUR Stripe ACCOUNT MUST BE IDENTICAL TO THE ONE YOU HAVE ON FILE WITH US AS A MEMBER AND WHICH YOU USED TO OBTAIN YOUR QUOTE! </p>  
      <p style="margin-left:50.0pt;">2. If you do not have a Stripe® account associated with the email address which you used to obtain your Reservation, then visit www.Stripe.com; establish an account and/or make sure any existing Stripe® account has the same email address as your email address that you used to obtain your Reservation. </p>
      <p style="margin-left:50.0pt;">3. If you fail to access your Stripe® account, making sure you have the identical mail address for your Stripe® account as the one you used to obtain your Reservation, then within thirty (30) days, the funds will be returned to us. If a refund notification email is sent to an email address associated with an existing Stripe® account, the refund will automatically be credited by Stripe® to the associated Stripe® account. You agree that, if you are unable to (1) access the email address associated with the email you used to obtain your Reservation, or (2) to use the Stripe® service or to access a Stripe® account, you will not be able to receive a refund for your Confirmed Reservation even if we have authorized such a refund. </p>
      <p style="margin-left:50.0pt;">4. Refunds, if any, are issued for the USD value as set forth in this Policy. Your refund will be issued by Stripe® in Bitcoin for that USD value, less any applicable fees, including any cancellation fees. Your refund will be converted from USD to Bitcoin based on an exchange rate set by Stripe® at the time we initiate the refund. You acknowledge that if the value of Bitcoin against USD has risen since the time of your purchase, you will receive less Bitcoin you might otherwise have received an alternative payment method. </p> 
      <p style="margin-left:35.0pt;">(ii). Other than as set forth is Section III(1)(H)(iii), below, no Refund shall be made after the termination of the Cancellation Period under any circumstances; any exception thereto shall be in our complete and sole discretion. Notwithstanding the foregoing in this Section III(1)(H)(ii), Company MAY, in its complete and sole discretion, make a Refund, in the form of a Coupon in any amount not to exceed One Hundred Dollars USD ($100.00) if the Cancellation Period was listed as “NON-REFUNDABLE”; the exercise of such sole discretion by Company is a courtesy that the Company may, or may not, elect to extend to a Member.</p>
      <p style="margin-left:35.0pt;">(iii). If between the time we provide a Member with a Reservation, and the Member makes a Payment, and we are unable to book the Hotel for the Member at the rate in the Reservation because the Hotel has increased its rate to us, the Member shall have a choice of a full Refund of the Payment or the option to make an additional payment to cover the cost of such increase, but not both.</p>
      <p style="margin-left:35.0pt;">(iv). No Refund shall be made if the Member stays for a shorter period of time than that set forth in the Confirmed Reservation.</p>
      <p style="margin-left:35.0pt;">(v). No Refund, whether in whole or in part, shall be made if the price charged to us by the Hotel decreases between the time we provide a Member with a Reservation, and the Member makes a Payment.</p>
      <p style="margin-left:35.0pt;">(vi). No Refund shall be made if the Member cancels directly with the Hotel.</p>
	<p style="margin-left:35.0pt;">(vi). No Refund shall be made if the Member cancels directly with the Hotel.</p>
	
	<p style="margin-left:35.0pt;">(vii). Under no circumstances shall a Member attempt to receive, or receive, any credit or payment, whether in whole or in part, of any funds paid by us to Hotel. Member understands and agrees that we will take any and all steps necessary, including legal action, and including without limitation, recovery of attorney’s fees, legal costs, other fees and any other damages to which we are entitled, to recover such credit or payment.</p>
	
	<p style="margin-left:35.0pt;">(viii). Member understands and agrees that we will dispute any Chargeback to the fullest possible legal extent, including but not limited to, the commencement of legal action including without limitation, recovery of attorney’s fees, legal costs, other fees and any other damages to which we are entitled, to recover such credit, or reversal of Payment to the Member and in addition, we may choose, in our sole discretion, to report to the credit/debit card issuer any fraudulent Chargeback. </p>
	
	<p style="margin-left:35.0pt;">(ix). Coupons. Coupons are an amount, in USD, that will be automatically deducted from your Reservation if you elect to redeem one or more of your earned Coupons. No more than one (1) coupon may be redeemed for any single Reservation.</p>
	
	<p style="margin-left:35.0pt;">General Terms and Conditions for earning Coupons.</p>
	<p style="margin-left:45.0pt;">a. The ways in which to earn Coupons, the number of Coupons that may be used on any single Reservation, expiration date of Coupons, or the maximum number or dollar amount of coupons that may be accumulated, may be changed, or discontinued at any time by Company, without prior notice except as otherwise set forth in this Section III(1)(H)(x).</p>
      <p style="margin-left:45.0pt;">b. Company does place an expiration date on all type(s) of Coupon(s). Any changes in expiration date(s) of any Coupon(s), including those previously issued, will be reflected in a change to this Policy.</p>
      <p style="margin-left:45.0pt;">c. If there is a change by Company in the number of Coupons that may be used on any single Reservation, how Coupons may be earned, or the maximum number or dollar amount of coupons that may be accumulated, any such changes will be reflected in a change to this Policy.</p>
      <p style="margin-left:45.0pt;">d. At the present date and time, there is no maximum number or dollar amount of Coupons that may be accumulated by a Member; all of which may change or be discontinued as set forth herein.</p>
      <p style="margin-left:45.0pt;">e. Affiliate Bonus.  A Potential Member shall receive a Coupon in the amount set forth on the Site as of the date the Potential Member uses an Affiliate Banner or an Affiliate Link to enter the Site and such Potential Member becomes a Member (the “New Member"). The Coupon will be credited to the New Member instantly upon becoming a Member, but such Coupon may be used ONLY for a Confirmed Reservation in the amount of One Hundred Fifty USD ($150.00) or more.</p>
      <!--<p style="margin-left:45.0pt;">f. Refer a Friend Bonus. When a Member (the “Referring Member”) uses the Referring Member’s personal link (as part of the Refer a Friend process) to have a third party become a Member (the “New Member”), The New Member will instantly receive a Refer A Friend Coupon in the amount set forth on the Site, but such Coupon may be used ONLY for a Confirmed Reservation in the amount of One Hundred Fifty USD ($150.00) or more. The Coupon will be credited to the New Member instantly. In addition, once the New Member makes their first Confirmed Reservation which is not cancelled, refunded or charged back (the “First New Member Trip”), the Referring Member will receive five percent (5%) of the amount of the New Member’s First New Member Trip. The amount of the First New Member’s Trip is the full amount paid by the New Member to Company [less any Coupon(s)] in order for the New Member to receive a Confirmed Reservation. This five percent (5%) is valid ONLY for the First New Member Trip made by the New Member and not any subsequent trips made by that New Member. The five percent will be in the form of a Coupon to the Referring Member, ninety (90) days AFTER the check-out date of the First New Member Trip. However, if you refer yet a different friend who then also becomes a Member, you will receive the same benefits. By way of example and not limitation: You are a Member whose name is Kate. You give your friend Sam your personal Refer a Friend Link. Sam uses that link to become a Member. Sam instantly receives a Refer A Friend Coupon. Sam then accepts a Reservation in the gross amount of $1,020.00, and applies a Coupon valued at $20.00. Sam the pays the Company $1000.00 and receives a Confirmed Reservation, with a checkout date of 5/1/15. Sam does not cancel, get a refund of, or charge back his Confirmed Reservation. You Kate, then, on 7/29/15, receive a Coupon in the amount of $50.00 (5% of $1,000.00). If Sam makes any additional Confirmed Reservations, you will NOT receive five percent (5%) of any of his subsequent trips.    </p>-->
	<p style="margin-left:45.0pt;"> f. Direct Sign Up Bonus. A Member shall receive a Coupon in the amount set forth on the Site as of the date the Potential Member accesses the Site directly and such Potential Member becomes a Member. The Coupon will be credited to the Member instantly.</p>
	
	<p style="margin-left:45.0pt;">g. A New Member may receive only an Affiliate Bonus, or a Refer a Friend Bonus or a Direct Sign Up Bonus; a Member shall be entitled to only ONE bonus depending on the method the Potential Member used initially to sign up as a Member. However, a User who is already a Member and is a Referring Member as defined above, may receive a Refer A Friend Bonus commission for each new individual that the Referring Member uses his/her link to sign up a New Member as set forth in Section III(1)(H)(x)(f)above.</p>
	
	<!-- <p style="margin-left:35.0pt;">(iv). Company in its sole discretion may use the methodology set forth in either or both Section III(G)(ii) and/or Section III(G)(iii) above.</p>
	
	<p style="margin-left:35.0pt;">(v). Membership Fee Change: If Company has a Membership Fee and elects to change a Membership Fee or to change the Membership Fee or pricing options, or to change the Membership Fee refund policy, such changes, if any, shall be upon not less than thirty (30) days subsequent to the date Company elects to make such change(s) (the “Membership Fee Change Decision Date”). The method and manner of such notice shall be the same as set forth as if such Membership Fee Change was the same as if Company was charging a Membership Fee for the very first time, as set forth in SectionsIII(G)(ii) and/or III(G)(iii) above. </p>
	
	<p style="margin-left:35.0pt;">(vi). Notwithstanding any other language in this Section III(G) to the contrary, nothing herein shall prohibit Company, from time to time, to offer free or discounted Memberships in an amount and duration set in the Company’s sole discretion, to those who have been referred to Company by an Affiliate, or to other Potential Members seeking to become a Member, or a Lapsed Member seeking to once again become a Member, or to current Members for their loyalty to Company as a Member. Company shall, and User accepts, the fact that Company may have to employ various Marketing Strategies in order to provide its Members the best discounts and service that Company can provide to its Members and Affiliates.</p> 
	
<p>H. TRIP INSURANCE. </p>

	<p style="margin-left:35.0pt;">(i) While we do NOT provide trip insurance and ARE NOT AN INSURER, we strongly suggest that you consider purchasing Trip Insurance. At some time in our lives we, and probably you, have had interruptions to your planned travel, be it due to business scheduling issues, family issues or health problems. Trip Insurance is generally very reasonable in its cost and will bring you additional peace of mind should some covered occurrence under Trip Insurance happen to you. </p>
	
	<p style="margin-left:35.0pt;">(ii) You will have, after our Checkout Page, an option that will allow you to click and be connected with one of our Commercial Advertisers who specializes in trip insurance. We never endorse, recommend or warrant the product or service provided by one of our Commercial Advertisers but we conduct our own due diligence, as should you, on any Commercial Advertiser. Of course, you are certainly free to forgo Trip Insurance or use a different company to provide Trip Insurance. If you are contemplating using another company for Trip Insurance we certainly suggest that you check out their customer satisfaction ratings and other due diligence for such other Company. In any event, Trip Insurance from a reputable insurance has been, in our experience, a safety net for a very reasonable price.</p>
	
	<p style="margin-left:35.0pt;">(iii) You should also be aware that if your purchase Trip Insurance from our Commercial Advertiser, we do receive a commission from that Commercial Advertiser for your purchase from our Commercial Advertiser.</p>-->
	
<p><strong>2. FOR AFFILIATES. </strong>For Affiliates, the following additional terms and conditions apply:</p>

<p>A. The following information is not proprietary to you, and may, or may not, be considered PERSONALLY IDENTIFIABLE INFORMATION (see our PRIVACY POLICY).</p>

<p>B. Other than Voluntary Data Collection, Company does not claim ownership of the materials you provide to Site with regard to a request to become an Affiliate.</p>

<p>C. No compensation will be paid with respect to the use of Voluntary Data Collection, as provided herein. Company is under no obligation to post or use any Voluntary Data Collection. You may provide and we may remove any Voluntary Data Collection at any time in Company's sole discretion.</p>

<p>D. While the following information is not proprietary to you, it will only be seen by you, and if necessary, by Company and/or its Service Provider(s): any email directed specifically to you by Company or email sent by you to Company, information you provide to become an Affiliate, except as set forth in or PRIVACY POLICY, any use by you of the Site’s Change Password, Change Email Address, ACCOUNT INFO, PAYOUT PREFERENCE, STATISTICS, PAYOUT HISTORY AFFILIATE GUIDE, CONTACT US, OPTING IN OR OUT of our newsletter,  FAQ and the Legal Corner.</p>

<p>E. The following information is not proprietary to you, is not PERSONALLY IDENTIFIABLE INFORMATION, and may be seen by a Member(s), Users, other Affiliates and the general public: your Banner advertisement and Affiliate Link provided by Company. You may not make any changes to the language or content of the Banner advertisement or Affiliate Link supplied by Company without prior authorization from Company. You will indemnify and hold harmless Company from any claims or any nature or damages or other legal actions for any changes to you make to the Banner we supply to, and/or related in any way to, any legal claims or legal action as a result of any language you add, delete or modify with regard to the Banner or Affiliate Link or adjacent to them as to the Company’s Services.</p>

<p>F. CANCELLATION AND REFUND POLICY: Please refer to Section III(1)(H) of this POLICY so that you are aware of our policy regarding cancellations and refunds to Members. The amount of any refund or chargeback by a Member who made a reservation on our Site through your Banner will be deducted from your Affiliate Payment.</p>

<p>G. You may have as few as one or an unlimited number of websites upon which you may post your Banner or Affiliate Link provided that you are authorized to make such a posting; the Company shall not be responsible for any fees of any nature that you may incur for the posting of your Banner(s), including, but not limited to, payment to a third party to post your Banner and/or Affiliate Link on their website(s), hosting costs, etc. Provided however, you SHALL NOT post your Banner and/or Affiliate Link on any website that has links to any other HOTEL booking sites or similar sites that a reasonable and prudent business person would determine to be in competition with Company.</p>

<p>H. Affiliate Payment:</p>

	<p style="margin-left:35.0pt;">(i). Payment Preference. You may choose to have your Affiliate Payment made either via Stripe or Western Union® and you may change it at any time by going to the Affiliate Home Page, Payout Preferences, “I preferred to be paid”, changing your preference and clicking on “Update” Section. Any fees for sending your Affiliate Payment will be deducted from the amount of your Affiliate Payment, and you will be responsible for fees, if any, that you must pay as the recipient of the Affiliate Payment.</p>
	
	<p style="margin-left:35.0pt;">(ii) Also, under Payment Preference, you may choose the dollar amount you have accumulated at which point you wish to be paid. This Section of the Payment Preference says “Please only send me money once I reach: $X.XX” You may enter any dollar amount from $10.00USD to $2,999.99USD”. Currently, the default value is $100.00USD. You may change the dollar amount at any time by going to the Affiliate Home Page, Payout Preferences, “Please only send me money when I reach $X.XX”, changing your preference and clicking on the “Update” Section.</p>
	
	<p style="margin-left:35.0pt;">(iii). All Affiliate Payments are processed every two (2) weeks.</p>
	
	<p style="margin-left:35.0pt;">(iv). How the amount of Affiliate Payments is determined: Company has a commission plan for Affiliates as follows:</p>
	
		<p style="margin-left:65.0pt;">1.Five percent (5%) commission up to $9,999.99 per month, in Confirmed Reservations as a result of a Member having been directed to our Site by the Affiliate, and which Confirmed Reservation(s) have not been canceled, refunded, or charged back.</p>
		
		<p style="margin-left:65.0pt;">2.Five and half percent (5.5%) commission between $10,000-$49,999.99 per month, in Confirmed Reservations as a result of a Member having been directed to our Site by the Affiliate, and which Confirmed Reservation(s) have not been canceled, refunded, or charged back.</p>
		
		<p style="margin-left:65.0pt;">3.Six percent (6%) commission on $50,000.00 or more, per month, in Confirmed Reservations as a result of a Member having been directed to our Site by the Affiliate, and which Confirmed Reservation(s) have not been canceled, refunded, or charged back.</p>
		
		<p style="margin-left:65.0pt;">4.Commencing on the stated check out date and time of the Hotel of the Confirmed Reservation for a Member, the commission for such Confirmed Reservation is posted to the commission account of the Affiliate (the “Posting Date”). However, to allow for Company to process Cancellations, Refunds and Chargebacks, the Affiliate will not be eligible for payment of such commission until ninety (90) days after the Posting Date, and then will be paid on the first two (2) week payment cycle after the Posting Date.</p>
		
		<p style="margin-left:65.0pt;">5.Unlike other websites, this Site will, upon the first time a Member makes a Confirmed Reservation using the Affiliate’s Banner or Affiliate Link, for that first Confirmed Reservation and for ALL Confirmed Reservations of that Member thereafter, the Affiliate will continue to earn commission, subject to Section III(2)(H)(iv)(1-5) above.</p>
		
		<p style="margin-left:65.0pt;">6.The Pending Transactions Section of the Affiliate portion of the Site will give you an APPROXIMATE date of when any earned commission may be payable.</p>

	<p style="margin-left:35.0pt;">(v). Other.</p>

		<p style="margin-left:65.0pt;">1. Please remember that each new User who, through your Affiliate Link(s), becomes a Member (remember there is no charge for anyone to become a Member at this time) will receive a one-time Twenty Dollar ($20.00) off their first Confirmed Reservation of Two Hundred Fifty Dollars USD ($250.00) or more, unless there is specific language to the contrary in this Policy.</p>
		
		<p style="margin-left:65.0pt;">2. Top Affiliate Award. Commencing on the date the first User successfully becomes an Affiliate, and every 365 days thereafter (the “Award Period”), we may have annual incentives that we give out to our very best Affiliate(s) (the “Top Affiliate”). Such incentives may be as luxurious hotel stays and/or flights or other incentives paid by <strong><?php echo SITE_NAME;?></strong>. The designation as a Top Affiliate award recipient is based upon the highest commission amount(s) earned by our Affiliates in the Award Period. We may in our sole discretion award incentives to more than one Top Affiliate in descending order of commissions earned during an Award Period. Be our TOP AFFILIATE, MAKE MONEY and get rewarded with LUXURIOUS GIVEAWAYS!!! The number of Top Affiliate Awards per Award Period, the dollar value and the type of award are at the SOLE discretion of the Company, and the Top Affiliate Award program may be changed, modified, edited or deleted at any time, by the Company, without prior notice. A Top Affiliate Award recipient may elect to transfer their award to another individual by supplying the necessary information to Company via the Contact Us function of the Site. Provided however, once a Top Affiliate Award recipient makes such a transfer, such transfer is irrevocable and may NOT BE changed. In addition, the Top Affiliate who makes such a transfer shall be responsible for any and all taxes for such a transfer, including, but not limited to, any gift tax (if applicable).</p>
		
		<p style="margin-left:65.0pt;">3. Company. The amount of commission, the amount of dollar ranges for various commission levels, Posting Date, payment cycles, and Affiliate vouchers for travel on our Site for a Member’s first Confirmed Reservation may be changed at any time by Company without prior notice.</p>
		
<p><strong>SECTION IV. APPLICABLE TO ALL USERS AND ALL OTHER POLICIES: EACH AND EVERY TERM, CONDITION AND PROVISION SET FORTH BELOW IS APPLICABLE TO ALL USERS, AND FURTHER EACH AND EVERY TERM, CONDITION AND PROVISION SET FORTH BELOW APPLIES TO, IS INTEGRATED INTO, AND INCORPORATED BY REFERENCE INTO EACH OF THE OTHER POLICIES:</strong></p>

<p><strong>1. THIRD PARTY ACCOUNTS</strong><br/>
You may be able to connect your Company Account to THIRD PARTY ACCOUNTS, including, but not limited to, common and popular social media sites. By connecting your Affiliate Account to your third party account, you acknowledge and agree that you are consenting to the continuous release of information about you to others (in accordance with your privacy settings on those third party sites). If you do not want information about you to be shared in this manner, do not use this feature.
</p>

<p><strong>2. INTERNATIONAL USERS</strong><br/>
The Service is controlled, operated and administered by Company from pursuant to the laws of the United States of America. All transactions are deemed to have occurred, for all purposes, within the United States of America regardless of the citizenship, domicile or location of any User, Member or Affiliate. If you access the Service from a location outside of the United States of America, you are responsible for compliance with all local laws of such locations. You agree that you will not use the Company’s content accessed through the Site in any country or in any manner prohibited by any applicable laws, restrictions or regulations.
</p>

<p><strong>3. INDEMNIFICATION</strong><br/>
You agree to indemnify, defend and hold harmless Company, its officers, directors, employees, agents and third parties, for any losses, costs, liabilities and expenses (including reasonable attorneys' fees) relating to or arising out of your use of or inability to use the Site or services, your violation of any TERMS AND CONDITIONS AND ALL OTHER POLICIES of this Agreement or your violation of any rights of a third party, or your violation of any applicable laws, rules or regulations or violation of any OTHER POLICIES. Company reserves the right, at its own cost, to assume the exclusive defense and control of any matter subject to indemnification by you, in which event you will fully cooperate with Company in asserting any available defenses.
</p>

<p><strong>4. LIABILITY DISCLAIMER</strong><br/>
THE INFORMATION, SOFTWARE, PRODUCTS, AND SERVICES INCLUDED IN OR AVAILABLE THROUGH THE SITE MAY INCLUDE INACCURACIES OR TYPOGRAPHICAL ERRORS. CHANGES ARE PERIODICALLY ADDED TO THE INFORMATION HEREIN. <strong><?php echo SITE_NAME;?></strong>, AND/OR ITS SERVICE PROVIDERS MAY MAKE IMPROVEMENTS AND/OR CHANGES IN THE SITE AT ANY TIME.
</p>

<p><strong><?php echo SITE_NAME;?></strong>, AND/OR ITS SERVICE PROVIDERS MAKE NO REPRESENTATIONS ABOUT THE SUITABILITY, RELIABILITY, AVAILABILITY, TIMELINESS, SAFETY, PRIVACY AND ACCURACY OF THE INFORMATION, SOFTWARE, PRODUCTS, SERVICES AND RELATED GRAPHICS CONTAINED ON THE SITE FOR ANY PURPOSE. TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, ALL SUCH INFORMATION, SOFTWARE, PRODUCTS, SERVICES AND RELATED GRAPHICS ARE PROVIDED "AS IS" WITHOUT WARRANTY OR CONDITION OF ANY KIND. <strong><?php echo SITE_NAME;?></strong>, AND/OR ITS SUPPLIERS HEREBY DISCLAIM ALL WARRANTIES AND CONDITIONS WITH REGARD TO THIS INFORMATION, SOFTWARE, PRODUCTS, SERVICES AND RELATED GRAPHICS, INCLUDING ALL IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT.</p>

<p>TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, IN NO EVENT SHALL <strong><?php echo SITE_NAME;?></strong>, AND/OR ITS SERVICE PROVIDERS BE LIABLE FOR, AND YOU WAIVE AND RELEASE <strong><?php echo SITE_NAME;?></strong>  FROM ANY AND ALL DIRECT, INDIRECT, PUNITIVE, INCIDENTAL, SPECIAL, CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF USE, DATA OR PROFITS, ARISING OUT OF OR IN ANY WAY CONNECTED WITH THE USE OR PERFORMANCE OF THE SITE, YOUR VIOLATION OF THE TERMS AND CONDITIONS OR ANY OTHER POLICY OF THE SITE, INCLUDING ANY COMPANY REMEDIES CONTAINED THEREIN, WITH THE DELAY OR INABILITY TO USE THE SITE OR RELATED SERVICES, THE PROVISION OF OR FAILURE TO PROVIDE SERVICES, OR FOR ANY INFORMATION, SOFTWARE, PRODUCTS, SERVICES AND RELATED GRAPHICS OBTAINED THROUGH THE SITE, OR OTHERWISE ARISING OUT OF THE USE OF THE SITE, WHETHER BASED ON CONTRACT, TORT, NEGLIGENCE, STRICT LIABILITY OR OTHERWISE, EVEN IF <strong><?php echo SITE_NAME;?></strong>, OR ANY OF ITS SERVICE PROVIDERS HAS BEEN ADVISED OF THE POSSIBILITY OF DAMAGES. BECAUSE SOME STATES/JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF LIABILITY FOR CONSEQUENTIAL OR INCIDENTAL DAMAGES, THE ABOVE LIMITATION MAY NOT APPLY TO YOU. IF YOU ARE DISSATISFIED WITH ANY PORTION OF THE SITE, OR WITH ANY OF THESE TERMS AND CONDITIONS AND ALL OTHER POLICIES OF USE, YOUR SOLE AND EXCLUSIVE REMEDY IS TO DISCONTINUE USING THE SITE.</p>

<p>COMPANY IS NOT, AND WILL NOT BE, LIABLE OR RESPONSIBILE FOR ANY THIRD PARTY CONTENT ON THIS SITE.</p>

<p><strong>5. TERMINATION/ACCESS RESTRICTION/APPLICABLE LAWS/JURISDICTION/VENUE/CONTRACT FORMATION/OTHER</strong></p>

<p>A. Company reserves the right, in its sole discretion, to terminate your access to the Site and the related services or any portion thereof at any time, without notice. To the maximum extent permitted by law, this agreement is governed by the laws of the State of Nevada, United States of America, without reference to its choice of laws or conflict of laws provision. The contract formed between User and Company is deemed to have been made and performed in Las Vegas, Nevada, USA. User hereby consents to the exclusive jurisdiction and venue of courts in Nevada in all disputes arising out of or relating to the use of the Site. In any action to enforce the provisions of this TERMS AND CONDITIONS OR ANY OTHER POLICIES, venue and jurisdiction shall lie in the Las Vegas Township Justice Court, or in the Nevada Eighth Judicial District Court, or in the United States District Court, District of Nevada-Las Vegas, and each User hereby irrevocably consents to the jurisdiction of such Courts and waives any argument or assertion of forum non conveniens.</p>

<p>Use of the Site is unauthorized in any jurisdiction that does not give effect to all provisions of these TERMS AND CONDITIONS AND ALL OTHER POLICIES including, without limitation, this provision. </p>

<p>You agree that no joint venture, partnership, employment, or agency relationship exists between you and Company as a result of this Agreement or use of the Site. Company's performance of this Agreement is subject to existing laws and legal process, and nothing contained in this Agreement is in derogation of Company's right to comply with governmental, court and law enforcement requests or requirements relating to your use of the Site or information provided to or gathered by Company with respect to such use. If any part of this Agreement is determined to be invalid or unenforceable pursuant to applicable law including, but not limited to, the warranty disclaimers and liability limitations set forth above, then the invalid or unenforceable provision will be deemed superseded by a valid, enforceable provision that most closely matches the intent of the original provision and the remainder of the Agreement shall continue in effect. </p>

<p>Unless otherwise specified herein, this Agreement constitutes the entire Agreement between the User and Company with respect to the Site and it supersedes all prior or contemporaneous communications and proposals, whether electronic, oral or written, between the User and Company with respect to the Site. A printed version of this Agreement and of any notice given in electronic form shall be admissible in judicial or administrative proceedings based upon or relating to this Agreement to the same extent and subject to the same conditions as other business documents and records originally generated and maintained in printed form. It is the express policy of the Company and a User that this TERMS AND CONDITIONS AND OTHER POLICIES and all related documents are to be written in English, and this English language version, not any translation into any other language, shall be controlling. Neither this TERMS AND CONDITION NOR ANY OTHER POLICIES nor any term hereof may be amended, waived, discharged or terminated other than by Company.</p>

<p>B. Contract Formation:</p>

	<p style="margin-left:35.0pt;">(i). For all Users, you explicitly agree that when you enter this Site, you agree to be legally bound to all TERMS AND CONDITIONS and all OTHER POLICIES, and you entry into the Site is your Electronic Signature and you agree that you have entered into a legal binding contract by entering the site, the consideration for such contract being your ability to view this website in exchange for us being able to share with you certain information about our Services, and for both parties, other good and valuable consideration, the value of which is acknowledged by the parties, .</p>
	
	<p style="margin-left:35.0pt;">(ii). When a User becomes a Member and/or an Affiliate, and you click the checkbox entitled “I hereby accept the Terms and Conditions, Privacy Policy, Intellectual Property Policy”, DMCA Policy and all OTHER POLICIES, you have provided your Electronic Signature and have entered into a legal binding contract agreeing to all TERMS AND CONDITIONS and all OTHER POLICIES, the consideration for such contract being:</p>
	
		<p style="margin-left:65.0pt;">1. A Member having the ability to view the Member portion of this Site, earn and redeem Coupons, Promo Codes, request and receive a Confirmed Reservation and the ability to make a Confirmed Reservation, as well as all other right and privileges as a Member, and our consideration being the ability to provide you a Confirmed Reservation, award Coupons, award Promo Codes and all of our other Services and features available to Members, and for both parties, other good and valuable consideration, the value of which is acknowledged by the parties.</p>
		
		<p style="margin-left:65.0pt;">2. An Affiliate having the ability to view the Affiliate portion of this Site, obtain an Affiliate Link, earn commissions, receive a Banner and Affiliate Link for said Affiliate to place on one or more websites and potentially earn additional rewards for being a Top Affiliate, as well as all other right and privileges as an Affiliate, and our consideration being the ability to provide you an Affiliate Link, a Banner, potentially provide you a reward for being a Top Affiliate and for us to receive income from Users who have become Members based on your referral of the Member to us and for both parties, other good and valuable consideration, the value of which is acknowledged by the parties.</p>
		
		<p style="margin-left:65.0pt;">3.You acknowledge that all Electronic Communications from us to you regarding Confirmed Reservations, Cancellations, Refunds, Commissions, Top Affiliate Incentives and all applicable Site pages are in compliance with all laws and judicial determinations of the United States of America relative to contract law.</p>


<p><strong>6. CHANGES TO TERMS AND CONDITIONS AND ALL OTHER POLICIES.</strong> Company reserves the right, in its sole discretion, without prior notice, to change the TERMS AND CONDITIONS AND ALL OTHER POLICIES, under which Site is offered. The most current version of this TERMS AND CONDITIONS AND ALL OTHER POLICIES will supersede all previous versions of each respective POLICY. Company encourages you to periodically review the TERMS AND CONDITIONS AND ALL OTHER POLICIES to stay informed of our updates. You may find links to the most current TERMS AND CONDITIONS AND ALL OTHER POLICIES on the Home Page of this Site in the Legal Corner Section.</p>

<p><strong>7. DELAYS OR OMISSIONS.</strong> No delay or omission to exercise any right, power or remedy accruing to any Company upon any breach or default of any User under this TERMS AND CONDITIONS OR ANY OTHER POLICIES shall impair any such right, power or remedy of Company, nor shall it be construed to be a waiver of any such breach or default, or an acquiescence therein, or of, or in, any similar breach or default thereafter occurring; nor shall any waiver of any single breach or default be deemed a waiver of any other breach or default theretofore or thereafter occurring. Any waiver, permit, consent or approval of any kind or character on the part of Company of any breach or default under the TERMS AND CONDITIONS OR ANY OTHER POLICIES, or any waiver on the part of Company of any provisions or conditions of the TERMS AND CONDITIONS OR ANY OTHER POLICIES, must be in writing and shall be effective only to the extent specifically set forth in such writing or as provided in the TERMS AND CONDITIONS OR OTHER POLICIES</p>

<p><strong>8. ENFORCEMENT.</strong> User agrees that irreparable damage for which money damages would not be an adequate remedy would occur in the event that any of the provision of this TERMS AND CONDITIONS OR ANY OTHER POLICIES were not performed in accordance with its specific terms or was otherwise breached. It is accordingly agreed that, in addition to any other remedies the Company may have at law or equity, the Company shall be entitled to seek an injunction or injunctions, without the necessity to post bond, to prevent such breaches of this TERMS AND CONDITIONS OR ANY OTHER POLICIES and to enforce specifically the terms hereof.</p>

<p><strong>9. CONSTRUCTION</strong>. The normal rule of construction that any ambiguity or uncertainty in a writing shall be interpreted against the Company in drafting this TERMS AND CONDITIONS OR ANY OTHER POLICIES of, this Site shall not apply to any action on these TERMS AND CONDITIONS OR ANY OTHER POLICIES of this Site.</p>

<p><strong>10. FORCE MAJEURE.</strong> The Company is not liable for failure to perform the Company’s obligations, if any, if such failure is as a result of Acts of God (including fire, flood, earthquake, storm, hurricane or other natural disaster), war, invasion, act of foreign enemies, hostilities (regardless of whether war is declared), civil war, rebellion, revolution, insurrection, military or usurped power or confiscation, terrorist activities, nationalization, government sanction, blockage, embargo, labor dispute, strike, lockout or interruption or failure of electricity, internet, telephone or other utility service (‘Force Majeure’). </p>

<p><strong>11. CONFLICT.</strong> If there is any conflict or ambiguity between this TERMS AND CONDITIONS and any OTHER POLICIES, the terms, conditions and provisions of this TERMS AND CONDITIONS policy shall prevail, provided that such interpretation is consistent with Company’s intent.</p>

<p><strong>12.PAROL EVIDENCE.</strong> No parol evidence may be introduced to contravene or dispute this TERMS AND CONDITIONS OR ANY OTHER POLICIES except such parol evidence may be introduced by Company as to its intent as to the interpretation of this TERMS AND CONDITIONS and any OTHER POLICY.</p>

<p><strong>13. CONTACT US</strong><br/>
	Company welcomes your questions or comments regarding the TERMS AND CONDITIONS AND ALL OTHER POLICIES at the address listed below. <br/>
	<strong><?php echo SITE_NAME;?></strong><br/>
	3651 Lindell Road, D141 <br/>
	Las Vegas, Nevada, 89103 <br/>
	United States of America
</p>

<p><strong>14. FAILURE TO ABIDE OR ACCEPT. If you DO NOT AGREE TO ABIDE BY, OR FULLY ACCEPT, and ELECTRONICALLY SIGN the TERMS AND CONDITIONS AND ALL OTHER POLICIES, you MUST IMMEDIATELY EXIT THIS SITE. Links to these TERMS AND CONDITIONS AND ALL OTHER POLICIES may be found on the Home Page of this Site.</strong></p>

<p><strong>15. ORDER OF PREFERENCE IN INTERPRETATION.</strong></p>

<p>A.  If there is any conflict between any definitions or provisions of this TERMS AND CONDITIONS and any OTHER POLICY, this TERMS AND CONDITIONS POLICY shall prevail.</p>

<p>B.  If there is any conflict between any language in contained within our Site, including but not limited to any FAQ (Frequently Asked Questions), and any definitions or provisions of this TERMS AND CONDITIONS and any OTHER POLICY, then the definitions or provisions of this TERMS AND CONDITIONS and any OTHER POLICY shall prevail over any such language within the Site, in the order of precedence as set forth herein and otherwise in this POLICY.</p>

<p><strong>16. ELECTRONIC SIGNATURE ACT. By entering this Site, you are, pursuant to the Electronic Signature Act, 28 U.S.C. § 1746 indicating: </strong></p>

<p>A. Your unconditional acceptance and agreement to abide by the this TERMS AND CONDITIONS AND ALL OTHER POLICIES and,</p>

<p>B. That you are of MINIMUM AGE, and</p>

<p>C. You are submitting an unsworn declaration, certificate and verification, in writing of your agreement with, and that you will abide by, this TERMS AND CONDITIONS AND ALL OTHER POLICIES, that is subscribed by you, as true under penalty of perjury, and dated, in substantially the following form: </p>

	<p style="margin-left:35.0pt;">(i) If executed without the United States: “I declare and verify, under penalty of perjury under the laws of the United States of America that the foregoing is true and correct”. Executed on the date on which entered this Site, and you accept your entrance to this Site as your binding, legal signature, for the purposes of this declaration and verification, or</p>
	
	<p style="margin-left:35.0pt;">(ii) If executed within the United States, its territories, possessions, or commonwealths: “I declare and verify, under penalty of perjury that the foregoing is true and correct”. Executed on the date on which you entered this Site, and you accept your entrance to this Site as your binding, legal signature, for the purposes of this declaration and verification.</p>
	
<p><strong>17. CHANGES TO THIS POLICY.</strong> Company reserves the right, at any time, and for any reason, in its sole discretion to change this POLICY. The Effective Date of the most recent version of this POLICY will always be displayed at the very beginning of this POLICY, and the POLICY shall be effective as of said date. Users are encouraged to frequently check this POLICY and all OTHER POLICIES of this Site.</p>



<p><strong>18. AIRLINE ITINERARIES, RESERVATION(S), BOOKINGS, PRICING, PURCHASES, CANCELLATIONS AND OTHER ACTIVITIES RELATING TO AIRLINES. </strong>

<p>A. Effective as of December 28, 2016 or at some point thereafter, we are happy to be able to provide you access to what we believe, based on our research, is a Linked Site that may offer you extremely competitive pricing for domestic airfare in the United States AND in addition, we think you may find almost unbelievably low pricing for international travel when compared against other airfare websites. </p>
<p>B. We are providing you the opportunity for access to this airfare website (a “Commercial Advertiser”) as a courtesy to you via  a “Linked Site”. Such Linked Site is hereafter referred to as  the “Airfare Site”</p>
<p>C. The “Airfare Site” is NOT under the control of Company and Company is not responsible for the contents of the Airfare Site, including without limitation, any link contained within the Airfare Site, or any changes or updates to the “Airfare Site”. Company is providing this “Airfare Site” to you only as a convenience for you, and the inclusion of the Airfare Site does not imply endorsement by Company of the Airfare Site  or any association with its operators, except as set forth in Section 18(D) below. </p>
<p> D. The link in our Site to the “Airfare Site” may mean that Company accepted payment or will receive commissions from the “Airfare Site”. The Company does not endorse the “Airfare Site” and does not warrant any information in the “Airfare Site” including any links included within the “Airfare Site”. Company does not provide any contact information for the “Airfare Site” other than the contact information provided by the “Airfare Site”.</p>
<p>E. If you elect to use the link we have provided to the Airfare Site, you should read carefully the terms and conditions, and other polices and procedures of the Airfare Sites, including but not limited to reservations, bookings, cancellations, refunds, disputes and payment (the “Rules”). Once you have chosen to link from our Site to the Airfare Site, you are subject to the aforementioned “Rules” of the “Airfare Site”. </p>
 



            <!--<p>E. Quotes are valid ONLY for forty-eight (24) hours commencing at the time we email the Quote to you. We are not responsible for any delays or failure of you to receive a Quote in a timely fashion including, but not limited to, any issues with a third party server, internet connection, or your email system.</p>

            <p>F. A reservation becomes a Confirmed Reservation ONLY if you accept both the Quote and make Payment in full to us within the timeframe set forth in Section III(1)(E) above. If you send an amount less than Payment in full and do not send the remaining balance for the Quote within the time frame set forth in Section III(1)E above, you will not be entitled to a Confirmed Reservation.</p>

            <p>G. Once a Cancellation has been received by us, the previous Quote made by us to make the Reservation which is being cancelled, is no longer valid. A Member must submit a new request for a Quote once a legitimate Cancellation is made by the Member and then the Member must accept (make a Reservation and make payment in full) or decline the new Quote (by not accepting the Quote and not paying in full for the Reservation); all within the time frames set forth in the Cancellation Policy.</p>

            <p>H. CANCELLATION AND REFUND POLICY:</p>

            <p>(i).&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Definitions and Special Bitcoin policy:</p>

            <p style="margin-left:1.0in;">a. Cancellation Period shall mean that period of time stated in your Confirmed Reservation, such period beginning at the time your Confirmed Reservation is emailed by us to you, and ending as of the check in date and time of the Hotel at the location of the Hotel designated in your Confirmed Reservation. By way of example, and not limitation, if your Hotel stay begins on August 11, your Confirmed Reservation states you have a seven (7) day cancellation period, and the Hotel&rsquo;s check in time is 11:00AM (all Hotel check in times and dates are the local times at dates where the where the Hotel is located), then you would have seven days, (168 hours) prior to 11:00AM on August 11 (measured by the Hotel&rsquo;s local date and time) in order to cancel. The Cancellation Period is NOT measured by the Member&rsquo;s local date and time.</p>

            <p style="margin-left:1.0in;">b. Cancellation and Refund Policy shall mean this Section III(1)(H) of this Terms and Conditions.</p>

            <p style="margin-left:1.0in;">c. Chargeback shall mean any attempt of any nature by Member to seek a credit or refund from a third party including, but not limited to, a credit card issuer, processor, merchant bank or like entity where we have provided Services in accordance with the Terms and Conditions.</p>

            <p style="margin-left:1.0in;">d. Quote-The quote or quotes for a specific Hotel or Hotels provided by us to you based upon the information you supplied to us in requesting the quote. When we send you a Quote, the Quote will specify the specific Cancellation Policy applicable to that Quote.</p>

            <p style="margin-left:1.0in;">e. Payment-shall mean the payment, in full, irrespective of manner or form used by you to pay us, in full, for the Quote.&nbsp; No Confirmed Reservation shall be issued unless payment in full has been received by us.</p>

            <p style="margin-left:1.0in;">f. Refund shall mean a payment by us, if any, to you, for a cancellation of a Confirmed Reservation where such cancellation by you is in complete compliance with our Terms and Conditions. The Refund Amount shall be the full amount paid to us for the Confirmed Reservation  subject to III(H)(i)g immediately below if you used Bitcoin as your payment method. If a Member paid only a partial payment(s) but never completely paid in full for the Quote within the required timeframe for a Confirmed Reservation, the amount of the refund, if any, shall be the total amount of partial payments made subject to III(H)(i)g immediately below if you used Bitcoin as your payment method.</p>
<p style="margin-left:1.0in;"> g. Whether your Confirmed Reservation qualifies for a refund is governed by this Terms and Conditions policy, regardless of whether you pay with Bitcoin or any other payment method. In order to receive a refund (if we grant one) on a Confirmed Reservation paid for with Bitcoin, you will have to follow certain procedures in order to claim your refund: </p>
<p style="margin-left:1.5in;">1. A valid email address and a Stripe® account are required for refunds. Your refund will be issued through Coinbase®, and you must have (or create) a Coinbase® account in order to receive the refund. Coinbase will send a refund notification email to the email address that you provided to Coinbase®. THE EMAIL ADDRESS OF YOUR COINBASE ACCOUNT MUST BE IDENTICAL TO THE ONE YOU HAVE ON FILE WITH US AS A MEMBER AND WHICH YOU USED TO OBTAIN YOUR QUOTE! </p>
<p style="margin-left:1.5in;">2. If you do not have a Coinbase® account associated with the email address which you used to obtain your Quote, then visit www.coinbase.com; establish an account and/or make sure any existing Coinbase® account has the same email address as your email address that you used to obtain your Quote.  </p>
<p style="margin-left:1.5in;">3. If you fail to access your Coinbase® account, making sure you have the identical mail address for your Coinbase® account as the one you used to obtain your Quote, then within thirty (30) days, the funds will be returned to us. If a refund notification email is sent to an email address associated with an existing Coinbase® account, the refund will automatically be credited by Coinbase® to the associated Coinbase® account. You agree that, if you are unable to (1) access the email address associated with the email you used to obtain your Quote, or (2) to use the Coinbase® service or to access a Coinbase® account, you will not be able to receive a refund for your Confirmed Reservation even if we have authorized such a refund. </p>
<p style="margin-left:1.5in;">4. Refunds, if any, are issued for the USD value as set forth in this Policy. Your refund will be issued by Coinbase® in Bitcoin for that USD value, less any applicable fees, including any cancellation fees. Your refund will be converted from USD to Bitcoin based on an exchange rate set by Coinbase® at the time we initiate the refund. You acknowledge that if the value of Bitcoin against USD has risen since the time of your purchase, you will receive less Bitcoin you might otherwise have received had you paid via PayPal®. </p>
            <p style="margin-left:.5in;">(ii). Other than as set forth is Section III(1)(H)(iii), below, no Refund shall be made after the termination of the Cancellation Period under any circumstances; any exception thereto shall be in our complete and sole discretion. Notwithstanding the foregoing in this Section III(1)(H)(ii), Company MAY, in its complete and sole discretion, make a Refund, in the form of a Coupon in any amount not to exceed One Hundred Dollars USD ($100.00) if the Cancellation Period was listed as &ldquo;NON-REFUNDABLE&rdquo;; the exercise of such sole discretion by Company is a courtesy that the Company may, or may not, elect to extend to a Member.</p>

            <p style="margin-left:.5in;">(iii). If between the time we provide a Member with a Quote, and the Member makes a Payment, and we are unable to book the Hotel for the Member at the rate in the Quote because the Hotel has increased its rate to us, the Member shall have a choice of a full Refund of the Payment or the option to make an additional payment to cover the cost of such increase, but not both.</p>

            <p style="margin-left:.5in;">(iv). No Refund shall be made if the Member stays for a shorter period of time than that set forth in the Confirmed Reservation.</p>

            <p style="margin-left:.5in;">(v). No Refund, whether in whole or in part, shall be made if the price charged to us by the Hotel decreases between the time we provide a Member with a Quote, and the Member makes a Payment.</p>

            <p>(vi). No Refund shall be made if the Member cancels directly with the Hotel.</p>

            <p style="margin-left:.5in;">(vii). Under no circumstances shall a Member attempt to receive, or receive, any credit or payment, whether in whole or in part, of any funds paid by us to Hotel. Member understands and agrees that we will take any and all steps necessary, including legal action, and including without limitation, recovery of attorney&rsquo;s fees, legal costs, other fees and any other damages to which we are entitled, to recover such credit or payment.</p>

            <p style="margin-left:.5in;">(viii). Member understands and agrees that we will dispute any Chargeback to the fullest possible legal extent, including but not limited to, the commencement of legal action including without limitation, recovery of attorney&rsquo;s fees, legal costs, other fees and any other damages to which we are entitled, to recover such credit, or reversal of Payment to the Member and in addition, we may choose, in our sole discretion, to report to the credit/debit card issuer any fraudulent Chargeback.</p>

            <p style="margin-left:.5in;">(ix). Coupons. Coupons are an amount, in USD, that will be automatically deducted from your Quote if you elect to redeem one or more of your earned Coupons. No more than one (1) coupon may be redeemed for any single Quote.</p>

            <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (x). General Terms and Conditions for earning Coupons.</p>

            <p style="margin-left:1.0in;">a. The ways in which to earn Coupons, the number of Coupons that may be used on any single Quote, expiration date of Coupons, or the maximum number or dollar amount of coupons that may be accumulated, may be changed, or discontinued at any time by Company, without prior notice except as otherwise set forth in this Section III(1)(H)(x).</p>

            <p style="margin-left:1.0in;">b. Company does place an expiration date on all type(s) of Coupon(s). Any changes in expiration date(s) of any Coupon(s), including those previously issued, will be reflected in a change to this Policy.</p>

            <p style="margin-left:1.0in;">c. If there is a change by Company in the number of Coupons that may be used on any single Quote, how Coupons may be earned, or the maximum number or dollar amount of coupons that may be accumulated, any such changes will be reflected in a change to this Policy.</p>

            <p style="margin-left:1.0in;">d. At the present date and time, there is no maximum number or dollar amount of Coupons that may be accumulated by a Member; all of which may change or be discontinued as set forth herein.</p>

            <p style="margin-left:1.0in;">e. Affiliate Bonus.&nbsp; A Potential Member shall receive a Coupon in the amount set forth on the Site as of the date the Potential Member uses an Affiliate Banner or an Affiliate Link to enter the Site and such Potential Member becomes a Member (the &ldquo;New Member&quot;). The Coupon will be credited to the New Member instantly upon becoming a Member, but such Coupon may be used ONLY for a Confirmed Reservation in the amount of One Hundred Fifty USD ($150.00) or more.</p>

            <p style="margin-left:1.0in;">f. Refer a Friend Bonus. When a Member (the &ldquo;Referring Member&rdquo;) uses the Referring Member&rsquo;s personal link (as part of the Refer a Friend process) to have a third party become a Member (the &ldquo;New Member&rdquo;), The New Member will instantly receive a Refer A Friend Coupon in the amount set forth on the Site, but such Coupon may be used ONLY for a Confirmed Reservation in the amount of One Hundred Fifty USD ($150.00) or more. The Coupon will be credited to the New Member instantly. In addition, once the New Member makes their first Confirmed Reservation which is not cancelled, refunded or charged back (the &ldquo;First New Member Trip&rdquo;), the Referring Member will receive five percent (5%) of the amount of the New Member&rsquo;s First New Member Trip. The amount of the First New Member&rsquo;s Trip is the full amount paid by the New Member to Company [less any Coupon(s)] in order for the New Member to receive a Confirmed Reservation. This five percent (5%) is valid ONLY for the First New Member Trip made by the New Member and not any subsequent trips made by that New Member. The five percent will be in the form of a Coupon to the Referring Member, ninety (90) days AFTER the check-out date of the First New Member Trip. However, if you refer yet a different friend who then also becomes a Member, you will receive the same benefits. By way of example and not limitation: You are a Member whose name is Kate. You give your friend Sam your personal Refer a Friend Link. Sam uses that link to become a Member. Sam instantly receives a Refer A Friend Coupon. Sam then accepts a Quote in the gross amount of $1,020.00, and applies a Coupon valued at $20.00. Sam the pays the Company $1000.00 and receives a Confirmed Reservation, with a checkout date of 5/1/15. Sam does not cancel, get a refund of, or charge back his Confirmed Reservation. You Kate, then, on 7/29/15, receive a Coupon in the amount of $50.00 (5% of $1,000.00). If Sam makes any additional Confirmed Reservations, you will NOT receive five percent (5%) of any of his subsequent trips. &nbsp;&nbsp;&nbsp;</p>

            <p style="margin-left:1.0in;">g. Direct Sign Up Bonus. A Member shall receive a Coupon in the amount set forth on the Site as of the date the Potential Member accesses the Site directly and such Potential Member becomes a Member. The Coupon will be credited to the Member instantly.</p>

            <p style="margin-left:1.0in;">h. A New Member may receive only an Affiliate Bonus, or a Refer a Friend Bonus or a Direct Sign Up Bonus; a Member shall be entitled to only ONE bonus depending on the method the Potential Member used initially to sign up as a Member. However, a User who is already a Member and is a Referring Member as defined above, may receive a Refer A Friend Bonus commission for each new individual that the Referring Member uses his/her link to sign up a New Member as set forth in Section III(1)(H)(x)(f)above.</p>

            <p><br />
                <strong>2. FOR AFFILIATES</strong>. For Affiliates, the following additional terms and conditions apply:</p>

            <p>A. The following information is not proprietary to you, and may, or may not, be considered PERSONALLY IDENTIFIABLE INFORMATION (see our PRIVACY POLICY</p>

            <p>B. Other than Voluntary Data Collection, Company does not claim ownership of the materials you provide to Site with regard to a request to become an Affiliate.</p>

            <p>C. No compensation will be paid with respect to the use of Voluntary Data Collection, as provided herein. Company is under no obligation to post or use any Voluntary Data Collection. You may provide and we may remove any Voluntary Data Collection at any time in Company&#39;s sole discretion.</p>

            <p>D. While the following information is not proprietary to you, it will only be seen by you, and if necessary, by Company and/or its Service Provider(s): any email directed specifically to you by Company or email sent by you to Company, information you provide to become an Affiliate, except as set forth in or PRIVACY POLICY, any use by you of the Site&rsquo;s Change Password, Change Email Address, ACCOUNT INFO, PAYOUT PREFERENCE, STATISTICS, PAYOUT HISTORY AFFILIATE GUIDE, CONTACT US, OPTING IN OR OUT of our newsletter,&nbsp; FAQ and the Legal Corner.</p>

            <p>E. The following information is not proprietary to you, is not PERSONALLY IDENTIFIABLE INFORMATION, and may be seen by a Member(s), Users, other Affiliates and the general public: your Banner advertisement and Affiliate Link provided by Company. You may not make any changes to the language or content of the Banner advertisement or Affiliate Link supplied by Company without prior authorization from Company. You will indemnify and hold harmless Company from any claims or any nature or damages or other legal actions for any changes to you make to the Banner we supply to, and/or related in any way to, any legal claims or legal action as a result of any language you add, delete or modify with regard to the Banner or Affiliate Link or adjacent to them as to the Company&rsquo;s Services.</p>

            <p>F. CANCELLATION AND REFUND POLICY: Please refer to Section III(1)(H) of this POLICY so that you are aware of our policy regarding cancellations and refunds to Members. The amount of any refund or chargeback by a Member who made a reservation on our Site through your Banner will be deducted from your Affiliate Payment.</p>

            <p>G. You may have as few as one or an unlimited number of websites upon which you may post your Banner or Affiliate Link provided that you are authorized to make such a posting; the Company shall not be responsible for any fees of any nature that you may incur for the posting of your Banner(s), including, but not limited to, payment to a third party to post your Banner and/or Affiliate Link on their website(s), hosting costs, etc. Provided however, you SHALL NOT post your Banner and/or Affiliate Link on any website that has links to any other HOTEL booking sites or similar sites that a reasonable and prudent business person would determine to be in competition with Company.</p>

            <p>H. Affiliate Payment:</p>

            <p style="margin-left:.5in;">(i). Payment Preference. You may choose to have your Affiliate Payment made either via PayPal&reg; or Western Union&reg; and you may change it at any time by going to the Affiliate Home Page, Payout Preferences, &ldquo;I preferred to be paid&rdquo;, changing your preference and clicking on &ldquo;Update&rdquo; section. Any fees for sending your Affiliate Payment will be deducted from the amount of your Affiliate Payment, and you will be responsible for fees, if any, that you must pay as the recipient of the Affiliate Payment.</p>

            <p style="margin-left:.5in;">(ii) Also, under Payment Preference, you may choose the dollar amount you have accumulated at which point you wish to be paid. This section of the Payment Preference says &ldquo;Please only send me money once I reach: $X.XX&rdquo; You may enter any dollar amount from $10.00USD to $2,999.99&rdquo;. Currently, the default value is $100.00USD. You may change the dollar amount at any time by going to the Affiliate Home Page, Payout Preferences, &ldquo;Please only send me money when I reach $X.XX&rdquo;, changing your preference and clicking on the &ldquo;Update&rdquo; section.<br />
                (iii). All Affiliate Payments are processed every two (2) weeks.</p>

            <p style="margin-left:.5in;">(iv). How the amount of Affiliate Payments is determined: Company has a commission plan for Affiliates as follows:</p>

            <ol style="list-style-type:lower-alpha;">
                <li>Five percent (5%) commission up to $9,999.99 per month, in Confirmed Reservations as a result of a Member having been directed to our Site by the Affiliate, and which Confirmed Reservation(s) have not been canceled, refunded, or charged back.</li>
                <li>Five and half percent (5.5%) commission between $10,000-$49,999.99 per month, in Confirmed Reservations as a result of a Member having been directed to our Site by the Affiliate, and which Confirmed Reservation(s) have not been canceled, refunded, or charged back.</li>
                <li>Six percent (6%) commission on $50,000.00 or more, per month, in Confirmed Reservations as a result of a Member having been directed to our Site by the Affiliate, and which Confirmed Reservation(s) have not been canceled, refunded, or charged back.</li>
                <li>Commencing on the stated check out date and time of the Hotel of the Confirmed Reservation for a Member, the commission for such Confirmed Reservation is posted to the commission account of the Affiliate (the &ldquo;Posting Date&rdquo;). However, to allow for Company to process Cancellations, Refunds and Chargebacks, the Affiliate will not be eligible for payment of such commission until ninety (90) days after the Posting Date, and then will be paid on the first two (2) week payment cycle after the Posting Date.</li>
                <li>Unlike other websites, this Site will, upon the first time a Member makes a Confirmed Reservation using the Affiliate&rsquo;s Banner or Affiliate Link, for that first Confirmed Reservation and for ALL Confirmed Reservations of that Member thereafter, the Affiliate will continue to earn commission, subject to Section III(2)(H)(iv)(a-d) above.</li>
                <li>The Pending Transactions section of the Affiliate portion of the Site will give you an APPROXIMATE date of when any earned commission.</li>
            </ol>

            <p>(v). Other.</p>

            <p style="margin-left:1.0in;">a. Please remember that each new User who, through your Affiliate Link(s), becomes a Member (remember there is no charge for anyone to become a Member) will receive a one-time Twenty Dollar ($20.00) off their first Confirmed Reservation, subject to the provisions of Section III(1)(H)(x)</p>

            <p style="margin-left:1.0in;">b. Top Affiliate Award. Commencing on the date the first User successfully becomes an Affiliate, and every 365 days thereafter (the &ldquo;Award Period&rdquo;), we may have annual incentives that we give out to our very best affiliate(s) (the &ldquo;Top Affiliate&rdquo;). Such incentives may be as luxurious hotel stays and/or flights paid by <strong><?php echo SITE_NAME;?><sup>sm</sup></strong>. The designation as a Top Affiliate award recipient is based upon the highest commission amount(s) earned by our Affiliates in the Award Period. We may in our sole discretion award incentives to more than one Top Affiliate in descending order of commissions earned during an Award Period. Be our TOP AFFILIATE, MAKE MONEY and get rewarded with LUXURIOUS GIVEAWAYS!!! The number of Top Affiliate Awards per Award Period, the dollar value and the type of award are at the SOLE discretion of the Company, and the Top Affiliate Award program may be changed, modified, edited or deleted at any time, by the Company, without prior notice. A Top Affiliate Award recipient may elect to transfer their award to another individual by supplying the necessary information to Company via the Contact Us function of the Site. Provided however, once a Top Affiliate Award recipient makes such a transfer, such transfer is irrevocable and may NOT BE changed. In addition, the Top Affiliate who makes such a transfer shall be responsible for any and all taxes for such a transfer, including, but not limited to, any gift tax (if applicable).</p>

            <p style="margin-left:1.0in;">c. Company. The amount of commission, the amount of dollar ranges for various commission levels, Posting Date, payment cycles, and Affiliate vouchers for travel on our Site for a Member&rsquo;s first Confirmed Reservation may be changed at any time by Company.<br />
                <br />
                &nbsp;</p>

            <p><strong>SECTION IV. APPLICABLE TO ALL USERS AND ALL OTHER POLICIES: EACH AND EVERY TERM, CONDITION AND PROVISION SET FORTH BELOW IS APPLICABLE TO ALL USERS, AND FURTHER EACH AND EVERY TERM, CONDITION AND PROVISION SET FORTH BELOW APPLIES TO, IS INTEGRATED INTO, AND INCORPORATED BY REFERENCE INTO EACH OF THE OTHER POLICIES:</strong></p>

            <p><br />
                <strong>1. THIRD PARTY ACCOUNTS</strong></p>

            <p>You may be able to connect your Company Account to THIRD PARTY ACCOUNTS, including, but not limited to, common and popular social media sites. By connecting your Affiliate Account to your third party account, you acknowledge and agree that you are consenting to the continuous release of information about you to others (in accordance with your privacy settings on those third party sites). If you do not want information about you to be shared in this manner, do not use this feature.</p>

            <p><br />
                <strong>2. INTERNATIONAL USERS</strong></p>

            <p>The Service is controlled, operated and administered by Company from pursuant to the laws of the United States of America. All transactions are deemed to have occurred, for all purposes, within the United States of America regardless of the citizenship, domicile or location of any User, Member or Affiliate. If you access the Service from a location outside of the United States of America, you are responsible for compliance with all local laws of such locations. You agree that you will not use the Company&rsquo;s content accessed through the Site in any country or in any manner prohibited by any applicable laws, restrictions or regulations.</p>

            <p><br />
                <strong>3. INDEMNIFICATION</strong></p>

            <p>You agree to indemnify, defend and hold harmless Company, its officers, directors, employees, agents and third parties, for any losses, costs, liabilities and expenses (including reasonable attorneys&#39; fees) relating to or arising out of your use of or inability to use the Site or services, your violation of any TERMS AND CONDITIONS AND ALL OTHER POLICIES of this Agreement or your violation of any rights of a third party, or your violation of any applicable laws, rules or regulations or violation of any OTHER POLICIES. Company reserves the right, at its own cost, to assume the exclusive defense and control of any matter subject to indemnification by you, in which event you will fully cooperate with Company in asserting any available defenses.</p>

            <p><br />
                <strong>4. LIABILITY DISCLAIMER</strong></p>

            <p>THE INFORMATION, SOFTWARE, PRODUCTS, AND SERVICES INCLUDED IN OR AVAILABLE THROUGH THE SITE MAY INCLUDE INACCURACIES OR TYPOGRAPHICAL ERRORS. CHANGES ARE PERIODICALLY ADDED TO THE INFORMATION HEREIN. <b><?php echo SITE_NAME;?></b>, AND/OR ITS SUPPLIERS MAY MAKE IMPROVEMENTS AND/OR CHANGES IN THE SITE AT ANY TIME.</p>

            <p><b><?php echo SITE_NAME;?></b>, AND/OR ITS SUPPLIERS MAKE NO REPRESENTATIONS ABOUT THE SUITABILITY, RELIABILITY, AVAILABILITY, TIMELINESS, SAFETY, PRIVACY AND ACCURACY OF THE INFORMATION, SOFTWARE, PRODUCTS, SERVICES AND RELATED GRAPHICS CONTAINED ON THE SITE FOR ANY PURPOSE. TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, ALL SUCH INFORMATION, SOFTWARE, PRODUCTS, SERVICES AND RELATED GRAPHICS ARE PROVIDED &quot;AS IS&quot; WITHOUT WARRANTY OR CONDITION OF ANY KIND. <b><?php echo SITE_NAME;?></b>, AND/OR ITS SUPPLIERS HEREBY DISCLAIM ALL WARRANTIES AND CONDITIONS WITH REGARD TO THIS INFORMATION, SOFTWARE, PRODUCTS, SERVICES AND RELATED GRAPHICS, INCLUDING ALL IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT.</p>

            <p>TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, IN NO EVENT SHALL <b><?php echo SITE_NAME;?></b>, AND/OR ITS SUPPLIERS BE LIABLE FOR, AND YOU WAIVE AND RELEASE <b><?php echo SITE_NAME;?></b> &nbsp;FROM ANY AND ALL DIRECT, INDIRECT, PUNITIVE, INCIDENTAL, SPECIAL, CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF USE, DATA OR PROFITS, ARISING OUT OF OR IN ANY WAY CONNECTED WITH THE USE OR PERFORMANCE OF THE SITE, YOUR VIOLATION OF THE TERMS AND CONDITIONS OR ANY OTHER POLICY OF THE SITE, INCLUDING ANY COMPANY REMEDIES CONTAINED THEREIN, WITH THE DELAY OR INABILITY TO USE THE SITE OR RELATED SERVICES, THE PROVISION OF OR FAILURE TO PROVIDE SERVICES, OR FOR ANY INFORMATION, SOFTWARE, PRODUCTS, SERVICES AND RELATED GRAPHICS OBTAINED THROUGH THE SITE, OR OTHERWISE ARISING OUT OF THE USE OF THE SITE, WHETHER BASED ON CONTRACT, TORT, NEGLIGENCE, STRICT LIABILITY OR OTHERWISE, EVEN IF <b><?php echo SITE_NAME;?></b>, OR ANY OF ITS SUPPLIERS HAS BEEN ADVISED OF THE POSSIBILITY OF DAMAGES. BECAUSE SOME STATES/JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF LIABILITY FOR CONSEQUENTIAL OR INCIDENTAL DAMAGES, THE ABOVE LIMITATION MAY NOT APPLY TO YOU. IF YOU ARE DISSATISFIED WITH ANY PORTION OF THE SITE, OR WITH ANY OF THESE TERMS AND CONDITIONS AND ALL OTHER POLICIES OF USE, YOUR SOLE AND EXCLUSIVE REMEDY IS TO DISCONTINUE USING THE SITE.</p>

            <p>COMPANY IS NOT, AND WILL NOT BE, LIABLE OR RESPONSIBILE FOR ANY THIRD PARTY CONTENT ON THIS SITE.</p>

            <p><br />
                <strong>5. TERMINATION/ACCESS RESTRICTION/APPLICABLE LAWS/JURISDICTION/VENUE/CONTRACT FORMATION/OTHER</strong></p>

            <p>A. Company reserves the right, in its sole discretion, to terminate your access to the Site and the related services or any portion thereof at any time, without notice. To the maximum extent permitted by law, this agreement is governed by the laws of the State of Nevada, United States of America, without reference to its choice of laws or conflict of laws provision. User hereby consents to the exclusive jurisdiction and venue of courts in Nevada in all disputes arising out of or relating to the use of the Site. In any action to enforce the provisions of this TERMS AND CONDITIONS OR ANY OTHER POLICIES, venue and jurisdiction shall lie in the Las Vegas Township Justice Court, or in the Nevada Eighth Judicial District Court, or in the United States District Court, District of Nevada-Las Vegas, and each User hereby irrevocably consents to the jurisdiction of such Courts and waives any argument or assertion of forum non conveniens.</p>

            <p>Use of the Site is unauthorized in any jurisdiction that does not give effect to all provisions of these TERMS AND CONDITIONS AND ALL OTHER POLICIES including, without limitation, this provision. </p>

            <p>You agree that no joint venture, partnership, employment, or agency relationship exists between you and Company as a result of this Agreement or use of the Site. Company's performance of this Agreement is subject to existing laws and legal process, and nothing contained in this Agreement is in derogation of Company's right to comply with governmental, court and law enforcement requests or requirements relating to your use of the Site or information provided to or gathered by Company with respect to such use. If any part of this Agreement is determined to be invalid or unenforceable pursuant to applicable law including, but not limited to, the warranty disclaimers and liability limitations set forth above, then the invalid or unenforceable provision will be deemed superseded by a valid, enforceable provision that most closely matches the intent of the original provision and the remainder of the Agreement shall continue in effect. </p>

            <p>Unless otherwise specified herein, this Agreement constitutes the entire Agreement between the User and Company with respect to the Site and it supersedes all prior or contemporaneous communications and proposals, whether electronic, oral or written, between the User and Company with respect to the Site. A printed version of this Agreement and of any notice given in electronic form shall be admissible in judicial or administrative proceedings based upon or relating to this Agreement to the same extent and subject to the same conditions as other business documents and records originally generated and maintained in printed form. It is the express policy of the Company and a User that this TERMS AND CONDITIONS AND OTHER POLICIES and all related documents are to be written in English, and this English language version, not any translation into any other language, shall be controlling. Neither this TERMS AND CONDITION NOR ANY OTHER POLICIES nor any term hereof may be amended, waived, discharged or terminated other than by Company.</p>

            
            <p>B. Contract Formation:</p>

            <p style="margin-left:45.0pt;">(i). For all Users, you explicitly agree that when you enter this Site, you agree to be legally bound to all TERMS AND CONDITIONS and all OTHER POLICIES, and you entry into the Site is your Electronic Signature and you agree that you have entered into a legal binding contract by entering the site, the consideration for such contract being your ability to view this website in exchange for us being able to share with you certain information about our Services, and for both parties, other good and valuable consideration, the value of which is acknowledged by the parties, .</p>

            <p style="margin-left:45.0pt;">(ii). When a User becomes a Member and/or an Affiliate, and you click the checkbox entitled &ldquo;I hereby accept the Terms and Conditions, Privacy Policy , Intellectuall Property Policy&rdquo;, DMCA Policy and all OTHER POLICIES,  you have provided your Electronic Signature and have entered into a legal binding contract agreeing to all TERMS AND CONDITIONS and all OTHER POLICIES, the consideration for such contract being:</p>

            <ol style="list-style-type:lower-alpha;">
                <li>A Member having the ability to view the Member portion of this Site, earn and redeem Coupons, request and receive Quotes and the ability to make a Confirmed Reservation, as well as all other right and privileges as a Member, and our consideration being the ability to provide you Quotes, award Coupons, provide Confirmed Reservations and all of our other Services and features available to Members, and for both parties, other good and valuable consideration, the value of which is acknowledged by the parties.</li>
                <li>An Affiliate having the ability to view the Affiliate portion of this Site, obtain an Affiliate Link, earn commissions, receive a Banner and Affiliate Link for said Affiliate to place on one or more websites and potentially earn additional rewards for being a Top Affiliate, as well as all other right and privileges as an Affiliate, and our consideration being the ability to provide you an Affiliate Link, a Banner, potentially provide you a reward for being a Top Affiliate and for us to receive income from Users who have become Members based on your referral of the Member to us and for both parties, other good and valuable consideration, the value of which is acknowledged by the parties.</li>
            </ol>

            <ol style="list-style-type:lower-roman;">
                <li value="3">You acknowledge that all Electronic Communications from us to you regarding Quotes, Confirmed Reservations, Cancellations, Refunds, Commissions, Top Affiliate Incentives and all applicable Site pages are in compliance with all laws and judicial determinations of the United States of America relative to contract law.</li>
            </ol>

            <p><br />
                <strong>6. CHANGES TO TERMS AND CONDITIONS AND ALL OTHER POLICIES.</strong> Company reserves the right, in its sole discretion, without prior notice, to change the TERMS AND CONDITIONS AND ALL OTHER POLICIES, under which Site is offered. The most current version of this TERMS AND CONDITIONS AND ALL OTHER POLICIES will supersede all previous versions of each respective POLICY. Company encourages you to periodically review the TERMS AND CONDITIONS AND ALL OTHER POLICIES to stay informed of our updates. You may find links to the most current TERMS AND CONDITIONS AND ALL OTHER POLICIES on the Home Page of this Site.</p>

            <p><br />
                <strong>7. DELAYS OR OMISSIONS</strong>. No delay or omission to exercise any right, power or remedy accruing to any Company upon any breach or default of any User under this TERMS AND CONDITIONS OR ANY OTHER POLICIES shall impair any such right, power or remedy of Company, nor shall it be construed to be a waiver of any such breach or default, or an acquiescence therein, or of, or in, any similar breach or default thereafter occurring; nor shall any waiver of any single breach or default be deemed a waiver of any other breach or default theretofore or thereafter occurring. Any waiver, permit, consent or approval of any kind or character on the part of Company of any breach or default under the TERMS AND CONDITIONS OR ANY OTHER POLICIES, or any waiver on the part of Company of any provisions or conditions of the TERMS AND CONDITIONS OR ANY OTHER POLICIES, must be in writing and shall be effective only to the extent specifically set forth in such writing or as provided in the TERMS AND CONDITIONS OR OTHER POLICIES.</p>

            <p><br />
                <strong>8. ENFORCEMENT</strong>. User agrees that irreparable damage for which money damages would not be an adequate remedy would occur in the event that any of the provision of this TERMS AND CONDITIONS OR ANY OTHER POLICIES were not performed in accordance with its specific terms or was otherwise breached. It is accordingly agreed that, in addition to any other remedies the Company may have at law or equity, the Company shall be entitled to seek an injunction or injunctions, without the necessity to post bond, to prevent such breaches of this TERMS AND CONDITIONS OR ANY OTHER POLICIES and to enforce specifically the terms hereof.</p>

            <p><br />
                <strong>9. CONSTRUCTION</strong>. The normal rule of construction that any ambiguity or uncertainty in a writing shall be interpreted against the Company in drafting this TERMS AND CONDITIONS OR ANY OTHER POLICIES of, this Site shall not apply to any action on these TERMS AND CONDITIONS OR ANY OTHER POLICIES of this Site.</p>

            <p><br />
                <strong>10. FORCE MAJEURE.</strong> The Company is not liable for failure to perform the Company&rsquo;s obligations, if any, if such failure is as a result of Acts of God (including fire, flood, earthquake, storm, hurricane or other natural disaster), war, invasion, act of foreign enemies, hostilities (regardless of whether war is declared), civil war, rebellion, revolution, insurrection, military or usurped power or confiscation, terrorist activities, nationalization, government sanction, blockage, embargo, labor dispute, strike, lockout or interruption or failure of electricity, internet, telephone or other utility service (&lsquo;Force Majeure&rsquo;).&nbsp;</p>

            <p><br />
                <strong>11. CONFLICT</strong>. If there is any conflict or ambiguity between this TERMS AND CONDITIONS and any OTHER POLICIES, the terms, conditions and provisions of this TERMS AND CONDITIONS policy shall prevail, provided that such interpretation is consistent with Company&rsquo;s intent.</p>

            <p><br />
                <strong>12.</strong> <strong>PAROL EVIDENCE</strong>. No parol evidence may be introduced to contravene or dispute this TERMS AND CONDITIONS OR ANY OTHER POLICIES except such parol evidence may be introduced by Company as to its intent as to the interpretation of this TERMS AND CONDITIONS and any OTHER POLICY.</p>

            <p><br />
                <strong>13. CONTACT US</strong></p>

            <p>Company welcomes your questions or comments regarding the TERMS AND CONDITIONS AND ALL OTHER POLICIES at the address listed below. 
                <br/><b><?php echo SITE_NAME;?></b> <br/>
                3651 Lindell Road, D141<br/>
Las Vegas, Nevada, 89103<br/>
United States of America<br/>

            </p>

            <p><br />
                <strong>14. FAILURE TO ABIDE OR ACCEPT. If you DO NOT AGREE TO ABIDE BY, OR FULLY ACCEPT, and ELECTRONICALLY SIGN the TERMS AND CONDITIONS AND ALL OTHER POLICIES, you MUST IMMEDIATELY EXIT THIS SITE. Links to these TERMS AND CONDITIONS AND ALL OTHER POLICIES may be found on the Home Page of this Site.</strong></p>

            <p><br />
                <strong>15. </strong><strong>ORDER OF PREFERENCE IN INTERPRETATION</strong>.</p>

            <p>A.&nbsp; If there is any conflict between any definitions or provisions of this TERMS AND CONDITIONS and any OTHER POLICY, this TERMS AND CONDITIONS POLICY shall prevail.</p>

            <p>B.&nbsp; If there is any conflict between any language in contained within our Site, including but not limited to any FAQ (Frequently Asked Questions), and any definitions or provisions of this TERMS AND CONDITIONS and any OTHER POLICY, then the definitions or provisions of this TERMS AND CONDITIONS and any OTHER POLICY shall prevail over any such language within the Site, in the order of precedence as set forth herein and otherwise in this POLICY.</p>

            <p><br />
                <strong>16. ELECTRONIC SIGNATURE ACT</strong>. By entering this Site, you are, pursuant to the Electronic Signature Act, 28 U.S.C. § 1746 indicating:
</p>

            <p>A. Your unconditional acceptance and agreement to abide by the this TERMS AND CONDITIONS AND ALL OTHER POLICIES and,</p>

            <p>B. That you are of MINIMUM AGE, and</p>

            <p>C. You are submitting an unsworn declaration, certificate and verification, in writing of your agreement with, and that you will abide by, this TERMS AND CONDITIONS AND ALL OTHER POLICIES, that is subscribed by you, as true under penalty of perjury, and dated, in substantially the following form: </p>
<p style="margin-left:45.0pt;">(i) If executed without the United States: “I declare and verify,  under penalty of perjury under the laws of the United States of America that the foregoing is true and correct”. Executed on the date on which entered this Site, and you accept your entrance to this Site as your binding, legal signature, for the purposes of this declaration and verification, or</p>
<p style="margin-left:45.0pt;">(ii) If executed within the United States, its territories, possessions, or commonwealths: “I declare and verify, under penalty of perjury that the foregoing is true and correct”. Executed on the date on which you entered this Site, and you accept your entrance to this Site as your binding, legal signature, for the purposes of this declaration and verification.</p>

            <p>&nbsp;</p>

            <p><strong>17. CHANGES TO THIS POLICY. </strong>Company reserves the right, at any time, and for any reason, in its sole discretion to change this POLICY. The Effective Date of the most recent version of this POLICY will always be displayed at the very beginning of this POLICY, and the POLICY shall be effective as of said date. Users are encouraged to frequently check this POLICY and all OTHER POLICIES of this Site.</p>-->

        </div> 
    </div>
</div>