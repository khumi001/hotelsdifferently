<div class="width-row margin-top-20">
    <div class="main_cont">
        <div class="pagetitle margin-bottom-10">
            <h1><?php echo $this->page_name; ?></h1>
<p>I.  Our Bitcoin Payment Policy (“Bitcoin Payment Policy”) applies to your use of Bitcoin as your selected payment method. With regard to ANY Bitcoin payment, the following terms and conditions apply, in order of precedence: All Bitcoin Terms and Conditions and policies (“BTC”), all Stripe® Terms and Conditions and polices (“CTC”), and our Terms and Conditions. If you do not agree with any of this Bitcoin Payment Policy or our refund policy with regard to Bitcoin and Stripe®, you MUST NOT use Bitcoin as your payment method. </p>

<p>II. All our prices for Reservations and payment by Members, and any refund, if any, are in United States Dollars (USD). Payments to obtain a Confirmed Reservation in Bitcoin will be charged to the Member in USD via Stripe®. Any foreign transaction fees, currency conversion fees, dynamic currency conversion (DCC) fees, or like fees incurred because the Affiliate’s or Member’s local currency is not USD will be the responsibility of the Affiliate or Member.</p>

<p>III. Any and all fees charged by Bitcoin or Stripe® relative to a Member’s use of Bitcoin and Stripe® are the sole responsibility of the Member. Our acceptance of Bitcoin is through Stripe® as our exchange server. We are not responsible for, or guarantee, Stripe®’s services or the availability of such services.  To complete your payment, you will be re-directed to Stripe®’s website, where you will see the total cost of your Reservation in Bitcoin, based on the Stripe®’s exchange rate. The Bitcoin price for your Reservation will remain valid for ten (10) minutes. If you do not initiate your payment during this time, the Bitcoin exchange rate will be updated and the Bitcoin price for your Reservation may change. </p>

<p>IV. A very small transaction fee may be added by the Bitcoin network to the total cost in Bitcoin of your Reservation if you are sending Bitcoin from a non-Stripe® wallet. This fee covers the cost of verifying Bitcoin transactions. We have no control over this fee and we do not receive any portion of the fee. </p>

<p>V. Bitcoin transactions are final. Once you initiate a Bitcoin transaction, you cannot cancel it; this is the policy of the Bitcoin network over which we have no control. Refunds, if any, are governed by our refund policy.</p>

<p>VI. If you attempt a payment via BitCoin wallet, in an amount of Bitcoins different from the exact cost of the Reservation indicated by Stripe®, your Confirmed Reservation will not complete. If, using a non-Stripe® wallet, you initiate a payment of Bitcoins different from the exact cost (plus transaction fees) of the Reservation indicated by Stripe®, your Confirmed Reservation will not complete; however your payment maybe processed by the Bitcoin network. If that incorrect payment is processed, your payment will be refunded by Stripe®. After an incorrect payment you will be required to re-submit payment for the correct amount in order to receive a Confirmed Reservation and the Bitcoin exchange rate may change (which we do not control).</p>

<p>VII. Transactions complete once confirmed. Once a Bitcoin transaction is submitted to the Bitcoin network, it will be unconfirmed for a period of time pending full verification of the transaction by the Bitcoin network. A transaction is not complete until it is fully verified and your Confirmed Reservation will not occur until the transaction is completely verified.</p>

<p>VIII. In order to receive a refund (if we grant one) on a Confirmed Reservation paid for with Bitcoin, you will have to follow certain procedures in order to claim your refund: </p>

<p style="margin-left:.5in;">1. A valid email address and a Stripe® account are required for refunds. Your refund will be issued through Stripe®, and you must have (or create) a Stripe® account in order to receive the refund. Stripe will send a refund notification email to the email address that you provided to Stripe®. THE EMAIL ADDRESS OF YOUR STRIPE ACCOUNT MUST BE IDENTICAL TO THE ONE YOU HAVE ON FILE WITH US AS A MEMBER AND WHICH YOU USED TO OBTAIN YOUR RESERVATION! </p>

<p style="margin-left:.5in;">2. If you do not have a Stripe® account associated with the email address which you used to obtain your Reservation, then visit <a href="https://www.stripe.com/">www.stripe.com;</a> establish an account and/or make sure any existing Stripe® account has the same email address as your email address that you used to obtain your Reservation.  </p>

<p style="margin-left:.5in;">3. If you fail to access your Stripe® account, making sure you have the identical mail address for your Stripe® account as the one you used to obtain your Reservation, then within thirty (30) days, the funds will be returned to us. If a refund notification email is sent to an email address associated with an existing Stripe® account, the refund will automatically be credited by Stripe® to the associated Stripe® account. You agree that, if you are unable to (1) access the email address associated with the email you used to obtain your Reservation, or (2) to use the Stripe® service or to access a Stripe® account, you will not be able to receive a refund for your Confirmed Reservation even if we have authorized such a refund. </strong></p>

<p style="margin-left:.5in;">4. Refunds, if any, are issued for the USD value as set forth in this Policy AND ARE SUBJECT TO OUR REFUND POLICY AS SET FORTH IN OUT TERMS AND CONDITIONS. Your refund will be issued by Stripe® in Bitcoin for that USD value, less any applicable fees, including any cancellation fees. Your refund will be converted from USD to Bitcoin based on an exchange rate set by Stripe® at the time we initiate the refund. You acknowledge that if the value of Bitcoin against USD has risen since the time of your purchase, you will receive less Bitcoin you might otherwise have received had you paid via an alternative payment method. </p>


<br/>
</div>
    </div>
</div>