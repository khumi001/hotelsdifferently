<div class="width-row margin-top-20">
	<div class="main_cont">
		<div class="pagetitle margin-bottom-10">
			<h1><?php echo $this->page_name; ?></h1>
			<div class="coupon-holder" style="padding:20px 0 0;">
				<p><strong>What will happen in the future?</strong></p>
				<p>Honestly? Nobody knows! :-) However, we have a few exciting updates for you in the not so distant future!</p>
		
				<!-- 	<p><strong style="color:red;">IMPLEMENTED:</strong>Our team of developers will implement and launch one of the newest brand names in the airline ticketing industry which will provide you <strong>AMAZING DEALS</strong> throughout the world. You will have the convenience to navigate through our site while doing business directly with them in a secure environment. We are all about savings and what you experience will be second to none! The longer the flight is, the more combinations and search algorithms this engine can provide, essentially getting you plane tickets to your destination for a great discount, compared to other public sites.<span style="text-decoration:underline;">No registration needed!</span></p> -->
				<br/>
				<p><strong>By 1st of April, 2017:</strong> A new feature will be activated on our site, called "Activities". Through the Activities panel, you will be able to search for a wide variety of show tickets, tours, adventures and transportation options. All this for basically wholesale prices!</p>
				<br/>
				<p><strong>By 1st of May, 2017:</strong> Car rentals!!! Our wholesale supplier's car rental inventory will be built-in, allowing you to browse throughout the best savings these networks can bring you.</p>
				<br/>
				<p><strong>By 1st of June, 2017:</strong> <span style="text-decoration:underline;">BIG MILESTONE!!!</span> We will be adding one of the biggest names in the wholesale industry to our network to further broaden our offerings which will, by then exceed 225,000 hotels and you will be able to experience - in many instances - a significant drop in hotel room prices. <strong>YES, even compared to our amazing savings we currently provide!</strong></p>
				<br/>
				<p><strong>By 1st of July, 2017:</strong> WAIT! There is more! Want to check out our discounted cruises? All it takes is a quick search and you will have access to the deepest discounts these cruiseliners are running.</p>
				<br/>
				<p>Thank you for trying out our website and I hope that you will enjoy the benefits and savings <strong>HotelsDifferently</strong><sup>sm</sup> brings to you every day!</p>
				<br/>
				<p>If you are happy with our services, please follow us on the social media links at the bottom of this page.</p>
				<p style="font-style:italic;">Sincerely,</p>
				<p><img style="display:block; max-width:300px; height:auto;" src="/public/affilliate_theme/img/logo.png" alt="" /></p>
			</div>
		</div>
	</div>
</div>
