<div class="row">
    <div class="col-md-12">
            <div class="portlet box blue" id="form_wizard_1">
                    <div class="portlet-title">
                            <div class="caption">
                                    <i class="fa fa-reorder"></i> Affiliate Sign up
<!--                                    <span class="step-title">
                                             Step 1 of 4
                                    </span>-->
                            </div>
                            <div class="tools hidden-xs">
                                    <a href="javascript:;" class="collapse">
                                    </a>
                                    <a href="#portlet-config" data-toggle="modal" class="config">
                                    </a>
                                    <a href="javascript:;" class="reload">
                                    </a>
                                    <a href="javascript:;" class="remove">
                                    </a>
                            </div>
                    </div>
                    <div class="portlet-body form">
                        <form method="post" class="form-horizontal" id="submit_form1">
                                    <div class="form-wizard">
                                            <div class="form-body">
                                                    <ul class="nav nav-pills nav-justified steps">
                                                            <li>
                                                                    <a href="#tab1" data-toggle="tab" class="step">
                                                                            <span class="number">
                                                                                     1
                                                                            </span>
                                                                            <span class="desc">
                                                                                    <i class="fa fa-check"></i> Account Setup
                                                                            </span>
                                                                    </a>
                                                            </li>
                                                            <li>
                                                                    <a href="#tab4" data-toggle="tab" class="step">
                                                                            <span class="number">
                                                                                     2
                                                                            </span>
                                                                            <span class="desc">
                                                                                    <i class="fa fa-check"></i> Confirm
                                                                            </span>
                                                                    </a>
                                                            </li>
                                                    </ul>
                                                    <div id="bar" class="progress progress-striped" role="progressbar">
                                                            <div class="progress-bar progress-bar-success">
                                                            </div>
                                                    </div>
                                                    <div class="tab-content">
                                                            <div class="alert alert-danger display-none">
                                                                    <button class="close" data-dismiss="alert"></button>
                                                                    You have some form errors. Please check below.
                                                            </div>
                                                            <div class="alert alert-success display-none">
                                                                    <button class="close" data-dismiss="alert"></button>
                                                                    Your form validation is successful!
                                                            </div>
                                                            <div class="tab-pane active" id="tab1">
                                                                    <h3 class="block">Provide your account details</h3>                                                                    
                                                                      <div class="form-group">
                                                                                <label class="control-label col-md-3">First name
                                                                                <span class="required">
                                                                                         *
                                                                                </span>
                                                                                </label>
                                                                                <div class="col-md-4">
                                                                                        <input type="text" class="form-control" name="firstname"/>
                                                                                        <span class="help-block">
                                                                                                 Provide your firstname
                                                                                        </span>
                                                                                </div>
                                                                        </div>
                                                                         <div class="form-group">
                                                                                <label class="control-label col-md-3">Last name
                                                                                <span class="required">
                                                                                         *
                                                                                </span>
                                                                                </label>
                                                                                <div class="col-md-4">
                                                                                        <input type="text" class="form-control" name="lastname"/>
                                                                                        <span class="help-block">
                                                                                                 Provide your lastname
                                                                                        </span>
                                                                                </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                                <label class="control-label col-md-3">Email address
                                                                                <span class="required">
                                                                                         *
                                                                                </span>
                                                                                </label>
                                                                                <div class="col-md-4">
                                                                                    <input type="email" class="form-control" name="email"/>
                                                                                        <span class="help-block">
                                                                                                 Provide your email address
                                                                                        </span>
                                                                                </div>
                                                                        </div>
                                                                       <div class="form-group">
                                                                                <label class="control-label col-md-3">Password
                                                                                <span class="required">
                                                                                         *
                                                                                </span>
                                                                                </label>
                                                                                <div class="col-md-4">
                                                                                        <input type="password" class="form-control" name="password" id="submit_form_password"/>
                                                                                        <span class="help-block">
                                                                                                 Provide your password.
                                                                                        </span>
                                                                                </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                                <label class="control-label col-md-3">Confirm Password
                                                                                <span class="required">
                                                                                         *
                                                                                </span>
                                                                                </label>
                                                                                <div class="col-md-4">
                                                                                        <input type="password" class="form-control" name="rpassword"/>
                                                                                        <span class="help-block">
                                                                                                 Confirm your password
                                                                                        </span>
                                                                                </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                                <label class="control-label col-md-3">Company name
                                                                                <span class="required">
                                                                                         *
                                                                                </span>
                                                                                </label>
                                                                                <div class="col-md-4">
                                                                                    <input type="text" class="form-control" name="companyname"/>
                                                                                        <span class="help-block">
                                                                                                 Provide your company name.
                                                                                        </span>
                                                                                </div>
                                                                        </div>
                                                                         <div class="form-group">
                                                                                <label class="control-label col-md-3">Title
                                                                                <span class="required">
                                                                                         *
                                                                                </span>
                                                                                </label>
                                                                                <div class="col-md-4">
                                                                                    <input type="text" class="form-control" name="title"/>
                                                                                        <span class="help-block">
                                                                                                 Provide your title.
                                                                                        </span>
                                                                                </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                                <label class="control-label col-md-3">Country
                                                                                <span class="required">
                                                                                         *
                                                                                </span>
                                                                                </label>
                                                                                <div class="col-md-4">
                                                                                    <input type="text" class="form-control" name="country"/>
                                                                                        <span class="help-block">
                                                                                                 Provide your country.
                                                                                        </span>
                                                                                </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                                <label class="control-label col-md-3">Address
                                                                                <span class="required">
                                                                                         *
                                                                                </span>
                                                                                </label>
                                                                                <div class="col-md-4">
                                                                                    <input type="text" class="form-control" name="address"/>
                                                                                        <span class="help-block">
                                                                                                 Provide your address.
                                                                                        </span>
                                                                                </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                                <label class="control-label col-md-3">City
                                                                                <span class="required">
                                                                                         *
                                                                                </span>
                                                                                </label>
                                                                                <div class="col-md-4">
                                                                                    <input type="text" class="form-control" name="city"/>
                                                                                        <span class="help-block">
                                                                                                 Provide your city.
                                                                                        </span>
                                                                                </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                                <label class="control-label col-md-3">State
                                                                                <span class="required">
                                                                                         *
                                                                                </span>
                                                                                </label>
                                                                                <div class="col-md-4">
                                                                                    <input type="text" class="form-control" name="state"/>
                                                                                        <span class="help-block">
                                                                                                 Provide your state.
                                                                                        </span>
                                                                                </div>
                                                                        </div>
                                                                       <div class="form-group">
                                                                                <label class="control-label col-md-3">Zip
                                                                                <span class="required">
                                                                                         *
                                                                                </span>
                                                                                </label>
                                                                                <div class="col-md-4">
                                                                                    <input type="text" class="form-control" name="zip"/>
                                                                                        <span class="help-block">
                                                                                                 Provide your zip.
                                                                                        </span>
                                                                                </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                                <label class="control-label col-md-3">Phone number
                                                                                <span class="required">
                                                                                         *
                                                                                </span>
                                                                                </label>
                                                                                <div class="col-md-4">
                                                                                    <input type="text" class="form-control" name="phone"/>
                                                                                        <span class="help-block">
                                                                                                 Provide your phone.
                                                                                        </span>
                                                                                </div>
                                                                        </div>
                                                                         <div class="form-group">
                                                                                <label class="control-label col-md-3">Newsletter name
                                                                                <span class="required">
                                                                                         *
                                                                                </span>
                                                                                </label>
                                                                                <div class="col-md-4">
                                                                                    <input type="text" class="form-control" name="newsletter"/>
                                                                                        <span class="help-block">
                                                                                                 Provide your Newsletter.
                                                                                        </span>
                                                                                </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                                <label class="control-label col-md-3">Website URL
                                                                                <span class="required">
                                                                                         *
                                                                                </span>
                                                                                </label>
                                                                                <div class="col-md-4">
                                                                                    <input type="text" class="form-control" name="websiteurl"/>
                                                                                        <span class="help-block">
                                                                                                 Provide your websiteurl.
                                                                                        </span>
                                                                                </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                                <label class="control-label col-md-3">Site description
                                                                                <span class="required">
                                                                                         *
                                                                                </span>
                                                                                </label>
                                                                                <div class="col-md-4">                                                                               
                                                                                    <textarea class="form-control" name="sitedescription" rows="3"></textarea>
                                                                                        <span class="help-block">
                                                                                                 Provide your sitedescription.
                                                                                        </span>
                                                                                </div>
                                                                        </div>                                                                      
                                                                        <div class="form-group">
										<label class="control-label col-md-3">Monthly UNIQUE visitors</label>
										<div class="col-md-4">
											<select class="form-control input-medium select2me" name="visitors" data-placeholder="Select...">
												<option value=""></option>												
                                                                                                <option><5,000</option>
                                                                                                <option>5,001-10,000</option>
                                                                                                <option>10,001-25,000</option>
                                                                                                <option>25,001-50,000</option>
                                                                                                <option>50,001-75,000</option>
                                                                                                <option>75,001-100,000</option>
                                                                                                <option>100,001-250,000</option>
                                                                                                <option>250,001-500,000</option>
                                                                                                <option>500,001-750,000</option>
                                                                                                <option>750,000-1,000,000</option>
                                                                                                <option>>1,000,000</option>
											</select>											
										</div>
									</div>
                                                                        
                                                                    
                                                            </div>
                                                            <div class="tab-pane" id="tab4">
                                                                    <h3 class="block">Confirm your account</h3>
                                                                    <h4 class="form-section">Account</h4>
                                                                    <div class="form-group">
                                                                            <label class="control-label col-md-2"></label>
                                                                            <div class="col-md-8">
                                                                                <div class="form-group">
                                                                                        <!--<label class="col-md-3 control-label">Radio</label>-->
                                                                                        <div class="col-md-9">
                                                                                                <div class="radio-list">
                                                                                                        <label>
                                                                                                        <input type="radio" name="optionsRadios" id="optionsRadios22" value="option1" checked> Privacy Policy </label>
                                                                                                        <label>
                                                                                                        <input type="radio" name="optionsRadios" id="optionsRadios23" value="option2" checked> Terms and Conditions </label>
                                                                                                        <label>
                                                                                                        <input type="radio" name="optionsRadios" id="optionsRadios24" value="option2" checked> Intellectual Property Policy </label>
                                                                                                </div>
                                                                                        </div>
                                                                                </div>
                                                                                <p class="form-control-static">
                                                                                    <b style="text-align: center">TERMS AND CONDITIONS</b><br>
                                                                                    <b style="text-align: center">Policy Effective As Of July 1,2013</b>
                                                                                </p><br>
                                                                                    <p class="form-control-static">
                                                                                        
                                                                                        

    The website www.goibibo.com (the "Site") is published and maintained by Ibibo Group Private Limited ("Company"), a company incorporated and existing in accordance with the laws of India When you access, browse or use this Site, you accept, without limitation or qualification, the terms and conditions set forth herein.
                                                                                    </p>
                                                                                    <p class="form-control-static">
SITE AND IT'S CONTENTS

    This Site is only for your personal use. You shall not distribute exchange, modify, sell or transmit anything you copy from this Site, including but not limited to any text, images, audio and video, for any business, commercial or public purpose
    As long as you comply with the terms of these Terms and Conditions of Use, Company grants you a non-exclusive, non-transferable, limited right to enter, view and use this Site. You agree not to interrupt or attempt to interrupt the operation of this Site in any manner whatsoever.
                                                                                    </p>
                                                                                    <p class="form-control-static">        
LINKS TO THIRD-PARTY WEBSITES

    This Site may contain links to websites operated by parties other than Company. Company does not control such Sites and is not responsible for their contents. Company's inclusion of hyperlinks to such Sites does not imply any endorsement of the material on such Sites or any association with their operators. If you decide to access any of the third party Sites linked to this Site, you do so entirely at your own risk.
                                                                                    </p>                                                                                    
                                                                            </div>
                                                                    </div>
                                                                    
                                                            </div>
                                                    </div>
                                            </div>
                                            <div class="form-actions fluid">
                                                    <div class="row">
                                                            <div class="col-md-12">
                                                                    <div class="col-md-offset-3 col-md-9">
                                                                            <a href="javascript:;" class="btn default button-previous">
                                                                                    <i class="m-icon-swapleft"></i> RESET
                                                                            </a>
                                                                            <a href="javascript:;" class="btn blue button-next">
                                                                                     Continue <i class="m-icon-swapright m-icon-white"></i>
                                                                            </a>
<!--                                                                        <a href="javascript:;" class="btn green button-submit">
                                                                                     Submit <i class="m-icon-swapright m-icon-white"></i>
                                                                            </a>-->
                                                                            <input type="submit" class="btn green button-submit" name="submit" value="NEXT">
                                                                    </div>
                                                            </div>
                                                    </div>
                                            </div>
                                    </div>
                            </form>
                    </div>
            </div>
    </div>
</div>