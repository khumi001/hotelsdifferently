<style>
    .greenbg{
        background: green !important;
    }   
    .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
        border-bottom-width: 2px;
        text-align: center;
    }
    .checkbox{
        font-weight: 200;
    }
</style>

<?php
$paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
//$paypal_id='sriniv_1293527277_biz@inbox.com'
$paypal_id = 'kartikdesai123@gmail.com';
?>
<div class="width-row margin-top-20">
    <div class="main_cont">
        <div class="pagetitle margin-bottom-10">
            <h1><span><?php echo $this->page_name; ?></span>
            </h1>
            <!--<h1></h1>-->
        </div>

        <!--        <div class="filter-box">
        
                </div>-->

        <div class="content_row">

            <div class="success_login_main_cont margin-top-20" style="width: 100%;background: none repeat scroll 0 0 rgba(255, 255, 255, 0);border-radius: 5px; ">
                <div class="modal-header" style="display: none;">
                    <!--<button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="fa fa-times"></i></button>-->          
                    <!--<h3 class="form-title">View Quote ID:  <label class="control-label normalfont  quoteid"><?php echo $datacontent[0]['var_uniq_quoteid']; ?></label></label></h3>-->
                </div>

                <form action='#' method='post' name='form1;'>
                    <div class="portlet-body form">
                        <div class="form-body">
                            <div class='row' style="padding: 0 20px 20px 20px;">
                                <div class='col-md-12' style="color:red;text-align: center;">
                                    <b>CONGRATULATIONS! Your reservation is booked!</b>
                                </div>&nbsp
                                <div class='col-md-12'>
                                    <div class="row">
                                        <span>Thank you for booking with <b>HotelsDifferently!<sup>sm</sup></b> We are delighted that you decided to choose us for your next getaway.</span>
                                    </div>&nbsp
                                    <div class="row">
                                        <span>Your charge will appear on your statement as: <b>HOTELSDIFFERENTLY</b></span>
                                    </div>&nbsp
                                    <div class="row">
                                        <span>Your reservation confirmation number is  <b><?php echo $datacontent[0]['var_transactionid']; ?></b></span>
                                    </div>&nbsp
                                    <div class="row">
                                        <span>Once again, here are the details of your reservation: </span>
                                    </div>
                                    &nbsp&nbsp&nbsp&nbsp
                                </div>
                                <div class='col-md-6'>
                                    <div class="row">
                                        <div class="form-group">
                                            <input type='hidden' name='business' value='kartikdesai123@gmail.com'>
                                            <input type='hidden' name='cmd' value='_xclick'>
                                            <input type='hidden' name='item_name' value='xxx'>
                                            <input type='hidden' name='item_number' value='1'>
                                            <input type='hidden' name='amount' class="paidamount" value='<?php echo $datacontent[0]['var_prize']; ?>'>
                                            <input type='hidden' name='no_shipping' value='5'>
                                            <input type='hidden' name='currency_code' value='USD'>
                                            <input type='hidden' name='custom123' class="replayid" value='<?php echo $replayquoteid; ?>'>
                                            <input type='hidden' name='custom' class="customfield" value='<?php echo $replayquoteid; ?>'>
                                            <input type="hidden" name="notify_url" class="returnurl1" value="<?php echo base_url() . 'front/confirm_account/paymentsuccess/'; ?>">
                                            <input type='hidden' name='cancel_return' value='<?php echo base_url() . 'error'; ?>'>
                                            <input type='hidden' name='return'  class="sendurl" value='<?php echo base_url() . 'user/reservation/paypalsuccess/' . $replayquoteid . '/'; ?>'>
                                            <input type='hidden' name='dataurl' class="returnurl" value='<?php echo base_url() . 'user/reservation/paypalsuccess/' . $replayquoteid . '/'; ?>'>

                                            <label class="control-label  col-md-12">Check-in:  
                                                <label class="control-label normalfont checkin"><?php echo $datacontent[0]['var_checkin']; ?></label></label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="control-label col-md-12">City:  
                                                <label class="control-label  normalfont city"><?php echo $datacontent[0]['var_city']; ?></label></label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="control-label col-md-12">Name of hotel:  
                                                <label class="control-label  normalfont nameofhotel"><?php echo $datacontent[0]['var_NOH']; ?></label></label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="control-label col-md-12">TripAdvisor: 
                                                <label class="control-label normalfont tripadvisor">&nbsp;<?php echo $datacontent[0]['var_tripadvisor']; ?></label></label>
                                        </div>
                                    </div>
                                </div>
                                <div class='col-md-6'>
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="control-label  col-md-12">Check-out: 
                                                <label class="control-label normalfont checkout"><?php echo $datacontent[0]['var_checkout']; ?></label></label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="control-label col-md-12">Quote ID:  
                                                <label class="control-label  normalfont quoteid"><?php echo $datacontent[0]['var_uniq_quoteid']; ?></label></label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="control-label col-md-12">Stars: 
                                                <label class="control-label normalfont stars"><?php echo $datacontent[0]['var_star']; ?></label></label>
                                        </div>
                                    </div>
                                </div>
                                <div  class='col-md-12 '>
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Rooms: 
                                                <label class="control-label normalfont rooms"><?php echo $datacontent[0]['var_room']; ?></label></label>
                                            <label class="control-label col-md-3">Adults: 
                                                <label class="control-label normalfont adults"><?php echo $datacontent[0]['var_adult']; ?></label></label>
                                            <label class="control-label col-md-6">Children: 
                                                <label class="control-label normalfont children"><?php echo $datacontent[0]['var_child']; ?></label></label>
                                        </div>    
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="control-label col-md-12">Cancellation policy: 
                                                <label class="control-label normalfont policy"><?php echo $datacontent[0]['var_policy']; ?></label></label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="control-label col-md-12">Comments: 
                                                <label class="control-label normalfont comments"><?php echo $datacontent[0]['var_comment']; ?></label></label>
                                        </div>
                                    </div>
                                    <!--<div class="form-group border-bottom margin-top-40" style="border-bottom:1px solid #e5e5e5;"></div>-->
                                    <div class="row">
                                        <div class="form-group text-center">
                                            <label class="col-md-6" style="text-align: right;margin-top:17px;font-size: 15px;padding-right: 5px; ">Current price: </label>
                                            <label class="control-label col-md-6  yourquote" style=" margin-top:19px;text-align: left;font-size: 15px;padding: 0px;line-height: 19px">
                                                <?php echo '$' . number_format((float) $datacontent[0]['var_prize'], 2, '.', ','); ?>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="col-md-6" style="text-align: right;font-size: 15px;padding-right: 5px;">coupon used: </label>
                                            <?php if ($datacontent[0]['var_prize'] > 150) { ?>
<!--                                                <div class="scroll col-md-3" style="padding: 0px;">
                                                    <?php foreach ($coupons as $row) { ?>
                                                        <div style="padding: 0px; font-size: 12px !important; width: 100%; <?php ?>" class="col-md-3" style="padding: 0px;">
                                                            <label style="padding: 0px ! important;font-size: 15px;" class="checkbox dis_block">
                                                                <div class="checker" style="display: inline-block;float: left;margin-right: 7px !important;margin-top: 2px !important; ">
                                                                    <span>
                                                                        <input type="checkbox" name="coupans" class="inline-labels" value="<?php echo $row['int_glcode']; ?>" style="margin: 3px 0;">
                                                                    </span>
                                                                </div>
                                                                <?php
                                                                if ($row['var_coupan_type'] == 1) {
                                                                    echo $row['var_couponvalue'] . '% OFF';
                                                                } else {
                                                                    echo '$' . $row['var_couponvalue'] . ' OFF';
                                                                }
                                                                ?>

                                                            </label>
                                                        </div>
                                                    <?php } ?>
                                                </div>-->
                                            <?php } ?>
                                            <!--                                            <div class="col-md-3 nottouse hidden">
                                                                                            <a href="javascript:;" style="color:black;">NOT TO USE COUPONS</a>                                             
                                                                                        </div>-->

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group text-center">
                                            <label class="col-md-6" style="text-align: right;color: red;font-size: 15px;padding-right: 5px;">Paid amount : </label>
                                            <label class="control-label col-md-6 finalamout" style="line-height: 24px;color: red;text-align: left; padding: 0px;font-size: 15px;">
                                                <?php echo '$' . number_format((float) $datacontent[0]['var_prize'], 2, '.', ','); ?>
                                            </label>
                                            <input type="hidden" class="finalamoutinput" value="<?php echo $datacontent[0]['var_prize']; ?>">
                                            <input type="hidden" class="replayquoteid" value="<?php echo $replayquoteid; ?>">

                                        </div>
                                    </div>
                                </div>
                                <div class='col-md-12'>
                                    <div class="row">
                                        <span>Please do not hesitate to let us know if there is anything we can do to make your stay excellent.</span>
                                    </div>
                                </div>
                                <div  class='col-md-12 margin-top-20'>
                                    <div class="form-group">
                                        <div class="col-md-12 text-center">
                                            <span style="text-align: center;">Share your savings with friends and colleagues and make money by referring them to our site:</span>
                                            <a class="fb_link" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo base_url();?>front/home/sharefacebook/<?= $refer_link['var_accountid']; ?>">
                                                <img src="<?php echo base_url();?>public/affilliate_theme/img/fb_icon.png"/>
                                            </a>
                                            <a class="tw_link" target="_blank" href="https://twitter.com/intent/tweet?url=aaaaa.dd&text=<?php echo $title." ".$desc; ?>&original_referer=&related=vvvvvv&via=twitter">
                                                <img src="<?php echo base_url();?>public/affilliate_theme/img/tw_icon.png"/>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div> 
    </div>

    <div id="resubmit_modal_success" class="modal fade" role="dialog" aria-hidden="true">
        <div class="success_login_main_cont margin-top-20" style="width:30%;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                <h3 class="form-title">Pay Success</h3>
            </div>
            <div style="" class="">
                <p style="padding: 10px">Thank you! Your submission has been received and we will be in touch shortly!</p>
            </div>
            <div class="modal-footer text-center" style="padding: 3px;text-align: center;">
                <buttom type='submit' aria-hidden="true" data-dismiss="modal" class='btn default_btn listing_btn resubmitokbutton' style='padding: 7px 30px;'>OK</buttom>
            </div>
        </div>
    </div>

    <div id="resubmit_modal_error" class="modal fade" role="dialog" aria-hidden="true">
        <div class="success_login_main_cont margin-top-20" style="width:30%;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                <h3 class="form-title">Pay Error</h3>
            </div>
            <div style="" class="">
                <p style="padding: 10px">Thank you! Your submission has been received and we will be in touch shortly!</p>
            </div>
            <div class="modal-footer text-center" style="padding: 3px;text-align: center;">
                <buttom type='submit' aria-hidden="true" data-dismiss="modal" class='btn default_btn listing_btn resubmitokbutton' style='padding: 7px 30px;'>OK</buttom>
            </div>
        </div>
    </div>
</div>