<style>
    .greenbg{
        background: green !important;
    }   
    .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
  border-bottom-width: 2px;
  text-align: center;
}
#request_submitted_length.dataTables_length{
	position:absolute;
	top:-155px;
	right:0;
	float:right;
}
.radio{
	padding-left:0;
}
#request_submitted_filter.dataTables_filter{
	position:absolute;
	top:-60px;
	right:0;
}
</style>

<div class="width-row margin-top-20">
<div class="main_cont">
    <div class="pagetitle margin-bottom-10">
        <h1><?php echo $this->page_name; ?></h1>
    </div>
    <div class="form-group col-md-7 hide filter-box">
    </div>
	<div class="text-center">
        <label>Please note that “On request” reservations are subject to the hotel’s approval and are not confirmed until the hotel approves your reservation request. Time submitted is displayed in Eastern Standard Time regardless of Hotel’s or Member’s location.</label>
    </div>
	<div class="form-group col-md-12 hide filter-box">
			<div class="row margin-top-10 filter-box1" style="margin-bottom:15px;">
				<div class="form-group">
					<label class="col-md-1 control-label" style="margin:6px 0 0;">Display:</label>
					<input style="width:12%;float:left;" type="text" id="from" class="date-picker form-control icon-pick date-filter" />
					<input style="width:12%;float:left; margin-left:10px;" type="text" id="to" class="date-picker form-control icon-pick date-filter" />
					<label class="checkbox" style="padding: 0px; margin:6px 0 0;">
						<input type="radio" style="margin-top: -2px;margin-left: 10px; padding-left:0;" id="filter" name="filter"  checked="checked"  class="radio-inline filter" value="">&nbsp;ALL
					</label>
					<label class="checkbox" style="padding: 0px; margin:6px 0 0;""><input type="radio" style="margin-top: -2px;margin-left: 10px; padding-left:0;" id="filter3" name="filter" class="radio-inline filter" value="Pending">&nbsp;PENDING </label> 
					<!--
					<label class="checkbox" style="padding: 0px;  margin:6px 0 0;"><input type="radio" style="margin-top: -2px;margin-left: 10px; padding-left:0;" id="filter1" name="filter" class="radio-inline filter" value="OUTDATED">&nbsp;APPROVED </label>                       
					-->
					<label class="checkbox" style="padding: 0px; margin:6px 0 0;"><input type="radio" style="margin-top: -2px;margin-left: 10px; padding-left:0;" id="filter3" name="filter" class="radio-inline filter" value="Declined">&nbsp;DECLINED </label>
				</div>
				<!--<div class="form-group" style="margin-top:45px;">
					<div class="col-md-11">                
						
					</div>
				</div>-->
			</div>
        </div>
    <div class="content_row">
        <table class="table table-striped table-bordered table-hover" id="request_submitted">
            <thead>
                <tr role="row" class="heading">
                    <th width='20%'  style="">Submitted</th>
                    <th width='20%'  style="">Booking number</th>
                    <th width='12%' style="">Check-in</th>
                    <th width='14%' style="">Check-out</th>
                    <th width='' style="">Rooms</th>
                    <th width='' style="">Hotel Name</th>
                    <th width='' style="">Paid</th>
                    <th width='' style="">Decision</th>
                </tr>
            </thead>
            <tbody>
			</tbody>
        </table>
    </div> 
</div>
</div>
