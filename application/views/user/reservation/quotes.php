<style>
    .greenbg{
        background: green !important;
    }   
    .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
        border-bottom-width: 2px;
        text-align: center;
    }
	.control-label.normalfont.site1 {color: #000;}
.control-label.normalfont.site2{color: #000;}
.site1name {color: #000;}
.site2name{color: #000;}
.control-label.col-md-12.text-center > strong{color: #1ea3f2;}

</style>
<div class="width-row margin-top-20">
    <div class="main_cont">
        <div class="pagetitle margin-bottom-10">
            <h1><?php echo $this->page_name; ?></h1>
        </div>
        <div class="row text-center">
            <label><span style="color:#ff0000;">Your quote expires after 24 hours!</span> Please take action soon to secure your quoted rate! <br> Expired quotes will only have a RESUBMIT button.</label>
        </div>
        <div class="filter-box">
            <!--            <label class="control-label col-md-2" style="padding: 6px 0;">Filter:</label>
                        <div class="col-md-9" style="margin-left: -65px;">
                            <div class="input-group input-large input-daterange" data-date="10-11-2012" data-date-format="mm-dd-yyyy">
                                <input type="text" class="form-control date-picker date-filter" id="from" name="from" value="" placeholder="Date From" style="text-align:left;">
                                <span class="input-group-addon">
                                    to
                                </span>
                                <input type="text" class="form-control to date-picker date-filter" id="to" name="to" value="" placeholder="Date To" style="text-align:left;">
                            </div>
                        </div> 
                        <button type="button" id="reset-btn" class="btn default_btn" style="padding: 5px 20px;">Reset</button>-->
            <div class="row margin-top-10 filter-box1">
                <div class="form-group">
                    <label class="col-md-1 control-label">Display:</label>
                    <div class="col-md-7">                
                        <label class="checkbox">
                            <input type="radio" style="margin-top: -2px;margin-left: 10px" id="filter" name="filter"  class="radio-inline filter" value="">&nbsp; ALL
                        </label>
                        <label class="checkbox">
                            <input type="radio" style="margin-top: -2px;margin-left: 10px" id="filter1" name="filter" checked="checked" class="radio-inline filter" value="A">&nbsp; ACTIVE
                        </label>
                        <label class="checkbox">
                            <input type="radio" style="margin-top: -2px;margin-left: 10px" id="filter3" name="filter" class="radio-inline filter" value="E">&nbsp; EXPIRED 
                        </label>            
                    </div>
                </div>
            </div>     
        </div>

        <div class="content_row">

            <table  class="table table-striped table-bordered table-hover" id="quotes">
                <thead>
                    <tr role="row" class="heading">
                        <th width='10%' style="">Quote ID</th>
                        <th width='10%' style="">Name</th>
                        <th width='10%' style="">City</th>
                        <th width='10%' style="">Hotel</th>
                        <th width='10%' style="">Room</th>
                        <th width='10%' style="">Adult</th>
                        <th width='15%'>Check in</th>
                        <th width='15%'>Check out</th>
                        <th width='' style="">Nights</th>
                        <th width=''></th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div> 
    </div>

    <div id="resubmit_modal" class="modal fade" role="dialog" aria-hidden="true">
        <div class="success_login_main_cont margin-top-20" style="width:30%;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                <h3 class="form-title">Resubmit Quote</h3>
            </div>
            <div style="" class="">
                <p style="padding: 10px">Thank you! Your submission has been received and we will be in touch shortly!</p>
            </div>
            <div class="modal-footer text-center" style="padding: 3px;text-align: center;">
                <buttom type='submit' aria-hidden="true" data-dismiss="modal" class='btn default_btn listing_btn resubmitokbutton' style='padding: 7px 30px;'>OK</buttom>
            </div>

        </div>
    </div>



    <div id="myModal_autocomplete" class="modal fade" role="dialog" aria-hidden="true">
        <div class="success_login_main_cont margin-top-20" style="width: 600px;background: none repeat scroll 0 0 rgba(255, 255, 255, 1);border-radius: 5px; ">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="fa fa-times"></i></button>          
                <h3 class="form-title">Quote ID:  <label class="control-label normalfont  quoteid">XXXXXXXX</label></label></h3>
            </div>
            <form  method="post" id="add_quoterequests" class="form-horizontal">
                <div class="portlet-body form">
                    <div class="form-body">
                        <div class='row' style="padding: 20px;">
                            <div class='col-md-6'>
                                <div class="form-group">
                                    <label class="control-label  col-md-12">Check-in:  
                                        <label class="control-label normalfont checkin">January 06,2015</label></label>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-12">City:  
                                        <label class="control-label  normalfont city">HongKong</label></label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-12">Name of hotel:  
                                        <label class="control-label  normalfont nameofhotel">real hotel</label></label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-12">Address:  
                                        <label class="control-label  normalfont address">address</label></label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-12">Room Type:  
                                        <label class="control-label  normalfont roomtype">xyz</label></label>
                                </div>
                                
                                <div class="form-group">
                                    <label class="control-label col-md-12">TripAdvisor: 
                                        <label class="control-label normalfont tripadvisor">&nbsp;tripadvisor</label></label>
                                </div>



                            </div>
                            <div class='col-md-6'>
                                <div class="form-group">
                                    <label class="control-label  col-md-12">Check-out: 
                                        <label class="control-label normalfont checkout">January 06,2015</label></label>
                                </div>
<!--                                <div class="form-group">
                                    <label class="control-label  col-md-12">Src:
                                        <label class="control-label normalfont src">&nbsp;</label></label>
                                </div>-->
                                <div class="form-group">
                                    <label class="control-label col-md-12">Stars: 
                                        <label class="control-label normalfont stars">3</label></label>
                                </div>

                                <!--                                <div class="form-group">
                                                                    <label class="control-label col-md-12 text-center" style="">QUOTE ID: 
                                                                        <label class="control-label normalfont  quoteid">XXXXXXXX</label></label>
                                                                </div>-->

                            </div>
                            <div  class='col-md-12 '>
                                <div class="form-group">
                                    <label class="control-label col-md-4">Rooms: 
                                        <label class="control-label normalfont rooms">2</label></label>
                                    <label class="control-label col-md-4">Adults: 
                                        <label class="control-label normalfont adults">3</label></label>
                                    <label class="control-label col-md-4">Children: 
                                        <label class="control-label normalfont children">2</label></label>
                                </div>                                    

                                <div class="form-group">
                                    <label class="control-label col-md-12"><span style="float:left;">Cancellation policy: </span>
                                        <span style="margin-left: 6px; float:left; padding: 0;" class="control-label normalfont policy">none</span></label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-12"><span style="float:left;">Comments: </span>
                                        <span style="margin-left: 6px; float:left; padding: 0;" class="control-label normalfont comments">none</span></label>
                                </div>
                                <div class="border-bottom margin-top-40" style="border-bottom:1px solid #e5e5e5;"></div>
                                <div class="form-group  margin-top-20 text-center">
                                    <b style="color: red;font-family: 'Adobe Caslon Pro Bold'; font-size: 17px;">Price comparison: </b>
                                    </div>
                                
                                <div class="form-group">
                                    <label class="control-label col-md-12">
                                        <span class="col-md-6 control-label normalfont " style="text-align: right;padding-right: 3px;"><span class="site1name"></span>:</span>
                                        <label class="col-md-6 control-label normalfont site1" style="padding-left: 0px;">&nbsp;$<span class="site1price"></span></label></label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-12 text-center" style="text-align: center;">
                                        <span class="col-md-6 control-label normalfont " style="text-align: right; padding-right: 3px;"><span class="site2name"></span>:</span>
                                        <label class="control-label col-md-6 normalfont site2" style="padding-left: 0px;">&nbsp;<span class="site2price"></span></label></label>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-md-12 text-center" style="font-weight: 400px;text-align: center;">
                                        <span class="col-md-6 control-label" style="text-align: right;padding-right: 3px;"> HotelsDifferently<sup>sm</sup> price: </span>
                                        <label class="col-md-6 control-label yourquote" style="padding-left: 4px;color: #333; font-weight: 700;font-size: 15px;line-height: 20px; ">$250.55</label></label>

                                        
                                </div>
                                <div class="form-group text-center">
                                 <label class="control-label col-md-12 text-center" style="font-weight: 400px;text-align: center;">
                                     <span class="col-md-12 control-label" style="text-align: center; color: #66b305;padding-right: 3px;font-family: 'Adobe Caslon Pro Bold'; font-size: 15px;">CONGRATULATIONS! Your savings is <span class="youaresaving"></span>!</span> 
                                     <!--<label class="control-label col-md-6 normalfont youaresaving" style="color: #66B305; font-weight: 600;font-size: 30px; line-height: 20px;">$250.55</label>-->
                                     
                                 </label>
                                </div>
                                                                        
                            </div>
                            <div  class='col-md-12 margin-top-20'>
                                <div class="form-group">
                                    <div class="col-md-12 text-center">
                                        <buttom type='submit' aria-hidden="true" data-dismiss="modal" class='btn default_btn listing_btn' style='padding: 7px 30px;'>OK</buttom>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
