

<div class="width-row margin-top-20">
    <div class="main_cont">
        <div class="pagetitle margin-bottom-10">
            <h1><?php echo $this->page_name; ?></h1>
        </div>
        <!--    <div class="faqs_title">Account related questions:</div>-->
        <div class="row text-center margin-bottom-10 margin-top-10">
            <div class="form-group col-md-12 text-center margin-bottom-10">

                <label class="control-label col-md-5" style="padding: 6px 0;width: 37%;"> Questions? Type in a keyword (e.g: password):</label>
                <div class="col-md-7" style="padding: 0 5px;">
                    <input type="text" name="que_search" id="que_search" class="form-control">
                </div>
            </div>
        </div>
        <div id="faqsearch">
            <div class="questions">
                <span>Q 01</span>
                I forgot my password, how do I retrieve it?
            </div>
            <div class="answers">
                No worries! To retrieve your password, please go to the homepage, click on the "Login" button, click on the "Forgot your password?" text, type in your email address then click on the "SUBMIT" button and your password should arrive instantly to your inbox. 
                <br />
                <br />
                Does it seem like it has been taking too long and you still have not received your password? Please check your SPAM/JUNK folder or try again.
            </div>
            <div class="ques_border"></div>

            <div class="questions">
                <span>Q 02</span>
                How do I change my email address?
            </div>
            <div class="answers">
                To change your email address, you first need to login to your account. Once you are logged in, go to "My Account" then click on the "Change Email Address" tab. Once you submit changes, you will receive a notification email to your old email address and an activation link to the new email address. Your new email address will only become active once you confirm it by clicking on the link in the email.
            </div>
            <div class="ques_border"></div>

            <div class="questions">
                <span>Q 03</span>
                How do I change my name in my account?
            </div>
            <div class="answers">
                To change your name, you first need to login to your account. Once you are logged in, go to "My Account" then click on the "My Info" tab. Modify the content of your choice and click on the "Update" button.
            </div>
            <div class="ques_border"></div>

            <div class="questions">
                <span>Q 04</span>
                What payment options are available if I make a booking?
            </div>
            <div class="answers">
                Our payment processor is PayPal<sup>TM</sup>, therefore we accept ANY PAYMENT METHOD that is supported by PayPal<sup>TM</sup>. <b class="bullet_color">WE ARE A TRUSTED, PAYPAL<sup>TM</sup> VERIFIED BUSINESS!</b> Please check our credentials by clicking <a style="font-weight: bold;" href="https://www.paypal.com/us/verified/pal=paypal@hotelsdifferently.com" target="_blank">HERE!</a> PayPal<sup>TM</sup> is a registered mark of PayPal<sup>TM</sup>, Inc.
            </div>
            <div class="ques_border"></div>

            <div class="questions">
                <span>Q 05</span>
                When will I be charged for the reservation I make?
            </div>
            <div class="answers">
                Your charge will commence instantly once you submit your payment and your payment processor will be PayPal<sup>TM</sup>. In rare occasions you are only charged a deposit but you are properly notified should that be the case, however 95% of the reservations must be paid in full and you should be charged immediately.
            </div>
            <div class="ques_border"></div>

            <div class="questions">
                <span>Q 06</span>
                How does the reservation confirmation work?
            </div>
            <div class="answers">
                Once you pay for your reservation, we will send you an email when your reservation is confirmed. Usually, we confirm reservations within 5 minutes to 12 hours. You can retrieve your reservation details from the website, when you go to Reservations - Confirmed reservations.
            </div>
            <div class="ques_border"></div>

            <div class="questions">
                <span>Q 07</span>
                Can I put the reservation onto 2 (or more) separate credit cards or debit cards?
            </div>
            <div class="answers">
                Yes, that is possible but it requires some special handling. Once you have the quote and reservation you would like to book please send us a message while logged into your account. Go to "Contact Us" and select "Billing" from the drop-down then provide the following information: <br />
                <br />
                <ul type="number" class="bullet_color">
                    <li><span class="ul_text">Quote ID</span></li>
                    <li><span class="ul_text">A brief message of your request and specify the amounts: "I would like to split the payments into 2 parts, the following way:
                            <ul type="square" class="bullet_color">
                                <li><span class="ul_text">Payment 1: $300.00</span></li>
                                <li><span class="ul_text">Payment 2: $250.25 </span></li>
                            </ul></span>
                    </li>
                </ul>
                <!--            1.) Quote ID <br />
                            2.) A brief message of your request and specify the amounts: "I would like to split the payments into 2 parts, the following way: <br />
                            <ul type="square">
                                <li>Payment #1: $300.00</li>
                                <li>Payment #2: $ </li>
                            </ul>-->
                Thank you! <br /><br />
                Once we receive your request, we will manually send you 2 invoices with the amounts specified by you. Once you submit ALL of your payments, then we can confirm your reservation. Failure to pay the full amount will result in not being able to confirm your reservation and you might lose the quote/rate you got.

            </div>
            <div class="ques_border"></div>

            <div class="questions">
                <span>Q 08</span>
                How does the reservation work on your Site?
            </div>
            <div class="answers">
                Once you get a quote and accept it, you can proceed to make the payment. Once the reservation is fully paid, we will send you a booking confirmation soon after. Once you receive our confirmation email, your reservation is confirmed.
            </div>
            <div class="ques_border"></div>

            <div class="questions">
                <span>Q 09</span>
                Once my payment has been submitted, how will I know that I really have a confirmed reservation?
            </div>
            <div class="answers">
                You are welcome to call the hotel to verify that your booking went through. Usually, hotels receive our booking within 2 days, therefore it is advisable to wait at least that amount of time after you receive your confirmation email from us to contact the hotel. Should you encounter any problems with the hotel, please send us a message immediately via the "Contact Us" page and select "Problems with booking" so that we can rectify any problems you may have experienced. Those emails that are labeled with "Problems with booking" enjoys high priority and you can expect a very fast reply.
            </div>
            <div class="ques_border"></div>

            <div class="questions">
                <span>Q 10</span>
                What are my check-in and check-out times at my hotel?
            </div>
            <div class="answers">
                Every hotel's check-in and check-out times vary greatly. Please make sure to book your hotel only if you have checked and agreed to the hotel's check-in and check-out times. For more information, please feel free to visit your hotel's website or call them.
            </div>
            <div class="ques_border"></div>

            <div class="questions">
                <span>Q 11</span>
                How much time do I have to make the reservation?
            </div>
            <div class="answers">
                Once you receive your quote, you will have exactly 24 hours to pay for the booking. After 24 hours, your quote expires and you have to request a new quote.
            </div>
            <div class="ques_border"></div>

            <div class="questions">
                <span>Q 12</span>
                I paid for the hotel but I still have not received my reservation confirmation from you. What can I do?
            </div>
            <div class="answers">
                If you do NOT hear from us within 24 hours of booking, please get in touch with us using the "Contact Us" menu and in the drop-down select "Email confirmation issue". When sending us a message, please make sure that you include your quote details, your account number and the payment amount. For your security, please do NOT include any debit or credit card numbers. These types of messages in our system enjoy great priority, therefore you can expect a very fast reply!
            </div>
            <div class="ques_border"></div>

            <div class="questions">
                <span>Q 13</span>
                What are the pet policies of my hotel?   
            </div>
            <div class="answers">
                Every hotel's pet policy varies greatly. Please make sure to book your hotel only if you are aware of its pet policy. For more information, please feel free to visit your hotel's website or call them.
            </div>
            <div class="ques_border"></div>

            <div class="questions">
                <span>Q 14</span>
                How do I get a receipt for my booking?
            </div>
            <div class="answers">
                We use PayPal<sup>TM</sup> as our payment processor and along with your payment, you are supposed to get a receipt that you can print or save immediately and you will also get a confirmation email from <b>HotelsDifferently<sup>sm</sup></b>. If you would like to retrieve receipts for bookings in the past, all you need to do is to log into your PayPal<sup>TM</sup> account where you can retrieve them (if you have one).
            </div>
            <div class="ques_border"></div>

            <div class="questions">
                <span>Q 15</span>
                Can I cancel my reservation? 
            </div>
            <div class="answers">
                The cancellation policy depends on your booking. Please check the booking details in the Reservations - Confirmed Reservations menu. Your cancellation policy will appear in the "Cancellation Policy" column.
            </div>
            <div class="ques_border"></div>

            <div class="questions">
                <span>Q 16</span>
                What if I find a lower rate on another website once I made a booking with you? Can you match it?
            </div>
            <div class="answers">
                Unfortunately, we do not offer such service at this time BUT if your reservation is fully refundable, then you can cancel it, get the refund and book the hotel elsewhere. To double-check your reservation's cancellation policy, please go to Reservations - Confirmed Reservations menu. Your cancellation policy will appear in the "Cancellation Policy" column.
            </div>
            <div class="ques_border"></div>

            <div class="questions">
                <span>Q 17</span>
                What is needed for check-in at the hotel?
            </div>
            <div class="answers">
                Most hotels only require a valid driver's license, government issued ID or a passport. Certain hotels have age restrictions, so please check that to ensure that you will not have a problem during check-in. Should you have any concerns regarding to the hotel's minimum age check-in policy, please do not hesitate to contact them. Also, most hotels also require a deposit which also varies from one hotel to another. Some do not need a deposit; others only accept credit cards while certain hotels accept cash as a deposit as well. It is advisable to check with the hotel beforehand.
            </div>
            <div class="ques_border"></div>

            <div class="questions">
                <span>Q 18</span>
                Is there a loyalty program?
            </div>
            <div class="answers">
                Currently, we do NOT offer a loyalty program; we strictly focus on " what we consider " the most important part of a hotel booking experience: <b class="bullet_color" style="color: red;">THE PRICE!</b> <br />
                <br />
                However, we DO appreciate customer loyalty. Instead of selling "preferred accounts", we offer EXPEDITED SERVICE to those who have spent the most with Us. We provide quotes to people with the highest amount spent first. So if we get 2 quote requests and one account has spent $10,000 with us so far, and the other $500, then we will first send a quote to the Member who has spent $10,000 already. Rest assured, everybody will get their quotes in a timely fashion (that is our policy and it is equally important to us as well).  

            </div>
            <div class="ques_border"></div>

            <div class="questions">
                <span>Q 19</span>
                Is there a status based customer service?
            </div>
            <div class="answers">
                There is no status. We provide priority quotes to people with the highest amount spent first, but rest          
                assured, everybody will get their quotes in a timely fashion (that is our policy and it is equally important 
                for us).

            </div>
            <div class="ques_border"></div>

            <div class="questions">
                <span>Q 20</span>
                HOW CAN YOUR QUOTED RATES BE LOWER (sometimes substantially) THAN ANY OF THE MAJOR LISTING SITES?
            </div>
            <div class="answers">
                We are delighted to help you find the hotel of your dreams for prices lower than any of the big hotel booking sites. Our system is a very complex one. We have an extremely extensive network and database of travel agencies <u>WORLDWIDE</u>; certain rates can only be accessed by local agencies, direct discounted rates are provided from select hotels in certain situations or consolidators. Cancellation policies between HotelsDifferently<sup>sm</sup> quotes and competitor sites may vary. Another explanation is that we do not have hundreds of employees, lavish offices or a 24/7 call center. However, providing the best customer service will always be our priority; therefore we are always within reach via email, thus maintaining a low overhead which is another way we make hotels happen for you at deep discounts! 
            </div>
            <div class="ques_border"></div>

            <div class="questions">
                <span>Q 21</span>
                Will I be able to earn points with my hotel program that I am a Member of?
            </div>
            <div class="answers">
                Every hotel's loyalty program differs greatly and some of them change without any prior notice. It is advisable to ask your hotel's loyalty program to see if you can earn points on your stay if you book your reservation outside of the hotel's own website.
            </div>
            <div class="ques_border"></div>

            <div class="questions">
                <span>Q 22</span>
                I know that there is no refund on non-refundable reservations but what happens if the stay has to be canceled due to extreme circumstances, such as a flight cancellation or an accident?
            </div>
            <div class="answers">
                <p>Unfortunately, these things do happen to some of us. Our recommendation is to use a credit card for the booking that covers these extreme situations, so you are protected in this unlikely event.<br />
                    <br />
                    Another option for you is to purchase travel insurance from a third party insurance company. Please note that we do not deal with or sell any types of insurances and any insurance concerns should be addressed with the insurance company directly.<br />
                    <br />
                    Regardless of your reason it is worth checking with us before you cancel your reservation as <strong><u>EVEN ON NON-REFUNDABLE RESERVATIONS WE ISSUE COURTESY REFUNDS!</u></strong>  So please email our customer service department before you cancel your reservation because we might be able to issue a credit to you. Please note that this is not mandated and it is completely at the sole discretion of <strong>HotelsDifferently<sup>sm</sup></strong>.</p>

            </div>
            <div class="ques_border"></div>

            <div class="questions">
                <span>Q 23</span>
                Where can I see my confirmed reservations?
            </div>
            <div class="answers">
                To retrieve your confirmed reservations, please go to Reservations " Confirmed reservations. 
            </div>
            <div class="ques_border"></div>

            <div class="questions">
                <span>Q 24</span>
                How long does it take until I get the quotes I requested?
            </div>
            <div class="answers">
                We provide priority quotes to people with the highest amount spent first, but rest assured, everybody will get their quotes in a timely fashion (that is our policy and it is equally important to us). Typically you get a quote within 10 minutes to 24 hours but we will do our best to get it to you as quickly as possible!
            </div>
            <div class="ques_border"></div>

            <div class="questions">
                <span>Q 25</span>
                Can I make a reservation for someone else?
            </div>
            <div class="answers">
                Absolutely, just make sure that you put the person's full name when requesting a quote. 
            </div>
            <div class="ques_border"></div>

            <div class="questions">
                <span>Q 26</span>
                Can I make a booking over the phone?
            </div>
            <div class="answers">
                Unfortunately, making a booking over the phone is not an option at this time. We prefer everything to be in writing to avoid any unnecessary disputes and miscommunication. 
            </div>
            <div class="ques_border"></div>

            <div class="questions">
                <span>Q 27</span>
                Can I have more guests check-in than the amount of guests I specified when I made the booking?
            </div>
            <div class="answers">
                The rate you paid for is determined by the number of people on your reservation. If more people check-in than it was originally included in the booking, then the hotel might charge you additional fees. To inquire regarding this potential surcharge, please contact the hotel directly.
            </div>
            <div class="ques_border"></div>

            <div class="questions">
                <span>Q 28</span>
                I accidentally canceled my reservation. What can I do now?
            </div>
            <div class="answers">
                Once a reservation is cancelled, We can NOT reinstate it UNLESS you have not gotten a cancellation confirmation email from Us. If you have not gotten any email from Us about your cancellation, please use the "Contact Us" option and choose "Problem with reservation" to reverse cancellation of your reservation. <br>
                <br />
                If your cancellation request has already gone through and the reservation was refundable, then your refund will be processed automatically. If your reservation was non-refundable, then you are not entitled to a refund.

            </div>
            <div class="ques_border"></div>

            <div class="questions">
                <span>Q 29</span>
                I noticed that another Member booked the exact same hotel, room type for the same period of time, but he was able to receive a lower quote. How is this possible, and can you give me the same quote?
            </div>
            <div class="answers">
                Hotel rates can fluctuate from minute to minute based on many factors, including how many rooms a hotel has unsold at a precise point in time. Therefore, the prices we are able to obtain for you are based upon the precise set of circumstances at the time we book a room for a Member. 
                <br />
                <br />
                For instance, the savings you see a certain Member obtain could be considerably less because that Member booked several weeks prior to your Quote, at a time when the hotel had a large number of unsold rooms. Since then, the hotel could have booked a convention, and has very few rooms left, and increased their prices accordingly. We will always do our best to get you the lowest possible price at the time you accept and pay for your quote. 
            </div>
            <div class="ques_border"></div>

            <div class="questions">
                <span>Q 30</span>
                Can I book directly with the hotel and receive the same rate? 
            </div>
            <div class="answers">
                We are able to provide you a low rate Quote because we have access, due to our buying power, and number of bookings, that are NOT available to the general public. In addition, in some circumstances, we may have packaged amenities (such as a complimentary breakfast) that would not be available to you. So trying to book directly with the hotel in an attempt to get the same low price directly from them; even if it were available could cause you to lose certain amenities. As the old saying goes: "You have to compare apples to apples and oranges to oranges".
            </div>
            <div class="ques_border"></div>

            <!--        <div class="questions">
                        <span>Q 31</span>
                        I noticed there is a Booking Fee of Five Dollars ($5.00) included in my Quote; what is this for?
                    </div>-->
            <!--        <div class="answers">
                       We have a very low margin on rooms that we are able to book for you. The Booking Fee is a very modest fee to help offset the time and resources we expend in finding you the very lowest Quote.
                    </div>
                    <div class="ques_border"></div>-->

            <div class="questions">
                <span>Q 31</span>
                What is the Cancellation Policy? 
            </div>
            <div class="answers">
                The Cancellation Period is fully defined in our Terms and Conditions Policy, but essentially it is the length of time, if any, that is stated in the invoice/payment page and subsequently in the Reservations - Confirmed Reservations menu. Your cancellation policy will appear in the "Cancellation Policy" column. The Cancellation Policy will either tell you (a) that your reservation is non-refundable, or (b) the number of days you have, prior to check-in date and time (using the hotel's date and time) in order to cancel your reservation and receive a refund.
            </div>
            <div class="ques_border"></div>

            <div class="questions">
                <span>Q 32</span>
                When am I entitled to a refund?
            </div>
            <div class="answers">
                <p>You are entitled to a refund as long as (a) your Cancellation Policy was not listed as non-refundable, (b) you cancelled prior to the expiration of the Cancellation Policy for a refundable reservation, and (c) you complied with all of our other Terms and Conditions.<br />
                    <br />
                    Regardless of your reason it is worth checking with us before you cancel your reservation as <strong><u>EVEN ON NON-REFUNDABLE RESERVATIONS WE ISSUE COURTESY REFUNDS!</u></strong> So please email our customer service department before you cancel your reservation because we might be able to issue a credit to you. Please note that this is not mandated and it is completely at the sole discretion of <strong>HotelsDifferently<sup>sm</sup></strong>.</p>
            </div>
            <!--           <br />
                       If I am entitled to a refund, will you keep my Five Dollar ($5.00) Booking Fee?
                       <br />
                       <br />
                       You may be pleasantly surprised to learn that we will INCLUDE your Five Dollar ($5.00) Booking Fee in your refund. We highly value your satisfaction with our Site, and we want you to keep returning to us for your travel needs!
                    </div>-->
            <div class="ques_border"></div>

            <div class="questions">
                <span>Q 33</span>
                What happens if I stay fewer nights at the hotel and want a refund? 
            </div>
            <div class="answers">
                Please understand that the rate we obtained for you for your stay was based, among other things, upon the length of your stay. It is quite possible that if you had initially requested a shorter stay, the rate per night we would have been able to offer you could have been considerably higher.
            </div>
            <div class="ques_border"></div>

            <div class="questions">
                <span>Q 34</span>
                What happens if, between the time I accept your quote and the time you are able to book the room on my behalf, the hotel rate increases or the Cancellation Policy time period decreases or becomes non-refundable? 
            </div>
            <div class="answers">
                This unlikely event could occur, since it may be up to 24 hours between the times we receive your acceptance AND PAYMENT for our quote and when we are able to actually book the room for you. 
                <br />
                <br />
                Hotel rates and room availability can fluctuate from minute to minute based on many factors, including how many rooms a hotel has available at a precise point in time. If such an event occurs, we will give you the option of a full refund of your payment, or you may elect to pay the additional amount by which the room rate has increased or agree to the change in the Cancellation Policy. Since we bear the burden of the possibility of Member's electing to seek a full refund, after we have expended resources in obtaining quotes for you, and attempting to book the room for you, we do not provide ANY refunds if a hotel's price decreases from the time of your acceptance/payment of our quote and the time your room is actually booked. This policy permits us to keep our overhead low, providing you the best possible savings we can offer.
                Please note that cancellation policies between HotelsDifferently<sup>sm</sup> quotes and competitor sites may vary.
            </div>
            <div class="ques_border"></div>

            <div class="questions">
                <span>Q 35</span>
                Can I cancel directly with the hotel that your Site booked for me?
            </div>
            <div class="answers">
                Nothing prohibits you from doing so, but please be aware that you will not receive ANY refund from the hotel. If your Cancellation Policy with us has expired or was non-refundable, we will be unable to provide you a refund. You must notify us in accordance with our Cancellation Policy to be eligible for a refund; cancelling with the hotel without notifying us as required by our Cancellation Policy will preclude you receiving a refund from us. In addition, the hotel will be unable to provide you with a refund (please see the question directly below). </div>
            <div class="ques_border"></div>

            <div class="questions">
                <span>Q 36</span>
                Can I receive a refund directly from the hotel? 
            </div>
            <div class="answers">
                No. We paid the hotel on your behalf, and we are the only entity legally entitled to a refund from the hotel for any reason. The hotel will NOT provide a refund directly to you.
            </div>
            <div class="ques_border"></div>

            <div class="questions">
                <span>Q 37</span>
                What if I am not satisfied with my stay?
            </div>
            <div class="answers">
                If you were dissatisfied by the hotel staff, your room(s), or the hotel otherwise didn't meet your personal standards; it is your responsibility to bring those concerns to the attention of hotel management and your recourse is solely limited to whatever the hotel may or may not offer you, to make amends. Of course, we would welcome you using our Contact Us function after your stay to let us know of any difficulties you encountered, and what, if anything, the hotel did to accommodate you. This will help us in making hotel recommendations to you and other Members in the future.
            </div>
            <div class="ques_border"></div>

            <div class="questions">
                <span>Q 38</span>
                If I am not satisfied with my stay, or your refund policy, may I dispute the charge on my credit card/debit card or any other form of payment? 
            </div>
            <div class="answers">
                While we cannot prohibit a Member from filing a dispute, each Member has, as a condition of booking on our Site has agreed to our Terms and Conditions and all Other Policies, including those related to cancellation and refunds. Where we have provided the Services as promised and in accordance with our Terms and Conditions, we will take all necessary steps to refute any attempt on the part of a Member to dispute their payment to us for any reason, regardless of the form of payment.
                <br />
                <br />
                In addition, we will endeavor to recover our costs, including but not limited to, legal fees, and any lost income or other damages attributable to an attempted chargeback or similar attempt to avoid or recover monies paid to us. 
                <br />
                <br />
                This policy is necessary to keep prices low for all Members so they are not penalized by anyone who is trying to take advantage of our Services without paying for our Services. We know you understand that we must take the appropriate steps if a Member tries to avoid their contractually binding agreement to pay for our Services.
            </div>
            <div class="ques_border"></div>

            <div class="questions">
                <span>Q 39</span>
                Do I have any recourse at all if I am unhappy with my stay? 
            </div>
            <div class="answers">
                Obviously, <b style="color:#ff0000">we want ALL of Our Members to have a thoroughly pleasant stay EACH and EVERY time they book a room with us.</b> We pride ourselves on our Customer Service, we want you to continue to use our Site, and for you to encourage others to do so as well.
                <br />
                <br />
                <b style="color:#ff0000">So, PLEASE contact us if you are dissatisfied or unhappy for any reason; we will work with you to try and find a mutually agreeable resolution.</b> Unfortunately, we can offer no guarantee that our attempts to totally resolve your concerns may prove completely satisfactory to you, but you can be assured that we will work very hard to make you happy. Any resolutions that we may offer are in our sole discretion.
            </div>
            <div class="ques_border"></div>

            <div class="questions">
                <span>Q 40</span>
                What does your newsletter contain?
            </div>
            <div class="answers">
                Our newsletter contains interesting news, such as: <br />
                <ul type="square" class="bullet_color">
                    <li><span class="ul_text">Our biggest savings of the week</span></li>
                    <li><span class="ul_text">Website updates</span></li>
                    <li><span class="ul_text">Coupons that can be used for future bookings!</span></li>
                </ul>
            </div>
            <div class="ques_border"></div>

            <div class="questions">
                <span>Q 41</span>
                Do I need to reconfirm my reservation with the hotel?
            </div>
            <div class="answers">
                If you received your confirmation from us, then it is not necessary to reconfirm your booking; you are already confirmed.
            </div>
            <div class="ques_border"></div>

            <div class="questions">
                <span>Q 42</span>
                What is included in the quoted rate and what else can I be charged for later?
            </div>
            <div class="answers">
                Your quoted rate contains the following: <br />
                <ul type="square" class="bullet_color">
                    <li><span class="ul_text">Room rate</span></li>
                    <li><span class="ul_text">Tax recovery charges</span></li>
                    <li><span class="ul_text">Our service fees</span></li>
                    <li><span class="ul_text">Any adjustments in rate if you used a coupon</span></li>
                </ul>
                You MIGHT be charged by the hotel for the following in addition to your quoted rate (if applicable): <br />
                <ul type="square" class="bullet_color">
                    <li><span class="ul_text">Hospitality or Tourism tax (if applicable)</span></li>
                    <li><span class="ul_text">Resort fees (if applicable) and any applicable taxes</span></li>
                    <li><span class="ul_text">Any fees in connection with hotel services (such as parking, dry cleaning, internet, breakfast, etc.) unless it is specified as "included" in your reservation. The hotel may charge applicable taxes on some fees.</span></li>
                </ul>
            </div>
            <div class="ques_border"></div>

            <div class="questions">
                <span>Q 43</span>
                How do I cancel my reservation?
            </div>
            <div class="answers">
                <p>To cancel your reservation, please go to Reservations - Confirmed Reservations and click on the &quot;Cancel&quot; button. Please note that once a reservation is cancelled, We can NOT reinstate it. If your reservation was refundable, then your refund will be processed automatically. If your reservation was non-refundable, then you are not entitled to a refund.<br />
                    Regardless of your reason it is worth checking with us before you cancel your reservation as <strong><u>EVEN ON NON-REFUNDABLE RESERVATIONS WE ISSUE COURTESY REFUNDS!</u></strong> So please email our customer service department before you cancel your reservation because we might be able to issue a credit to you. Please note that this is not mandated and it is completely at the sole discretion of <strong>HotelsDifferently<sup>sm</sup></strong>.</p>
            </div>
            <div class="ques_border"></div>

            <div class="questions">
                <span>Q 44</span>
                What are the tourism taxes or hospitality taxes?
            </div>
            <div class="answers">
                Certain cities charge a hospitality or tourism tax. This tax is a per person, per night charge that is collected by the hotel and it is not included in your rate.
            </div>
            <div class="ques_border"></div>

            <div class="questions">
                <span>Q 45</span>
                How do I redeem a coupon?
            </div>
            <div class="answers">
                You are welcome to apply any of your coupons on the payment page once you are ready to pay for your quoted reservation. Please note that your signup coupon can only be applied on reservations of $200.00 or above that amount.
            </div>
            <div class="ques_border"></div>

            <div class="questions">
                <span>Q 46</span>
                Can I use multiple coupons for the same reservation?
            </div>
            <div class="answers">
                You can currently use ONE COUPON per reservation. If you would like to use multiple coupons, all you need to do is to request the nights separately and apply them individually.
            </div>
            <div class="ques_border"></div>

            <div class="questions">
                <span>Q 47</span>
                Do you sell gift cards?
            </div>
            <div class="answers">
                No, at this time we do NOT offer such service, sorry.
            </div>
            <div class="ques_border"></div>

            <div class="questions">
                <span>Q 48</span>
                How do I subscribe for the newsletter?
            </div>
            <div class="answers">
                To subscribe for our newsletter, please go to the homepage and on the top left section you will see the newsletter subscription. Enter your email address and then confirm it once you receive the confirmation email. As soon as you confirm it, your email address will be added and you will get our newsletter from then on.
            </div>
            <div class="ques_border"></div>

            <div class="questions">
                <span>Q 49</span>
                How do I unsubscribe from the newsletter?
            </div>
            <div class="answers">
                If you would like to stop receiving newsletters from us, you can easily unsubscribe by clicking on the "unsubscribe" hyperlink that is included at the bottom of our newsletters. An alternative way to unsubscribe is by logging into your account, then going to My Account " My Info and find where it says "Opt-in for newsletter". In the drop-down, select "No" and click on the UPDATE button. Please note that the update might not be effective for a few days.
            </div>
            <div class="ques_border"></div>

            <div class="questions">
                <span>Q 50</span>
                What is my hotel's policy about children?
            </div>
            <div class="answers">
                Most of the hotels worldwide allow children as long as they are accompanied by an adult. To ensure that the hotel of your choice allows children, simply select the number of children in the drop-down when asking for a quote and you will only receive quotes from hotels that allow children at their property. Many hotels will allow children to stay for free or at a reduced rate when they share a room with parents or guardians and use existing bedding. The age at which this applies varies from hotel to hotel. If your child will be the third person in a room designated for two, extra-person charges may apply but you will see that reflected in your quote.
            </div>
            <div class="ques_border"></div>

            <div class="questions">
                <span>Q 51</span>
                Is it possible to change a name on a reservation and do I have to pay to change it?
            </div>
            <div class="answers">
                Most of the time it is allowed and you do not have to pay for that unless the hotel's policy does not allow name changes, but that rarely happens. To change the name on the booking, please first log into your account and go to Contact Us. In the drop-down, please select "Booking changes" and be sure to submit your: <br />
                <br />
                <ul type="umber" class="bullet_color">
                    <li><span class="ul_text"><b>HotelsDifferently<sup>sm</sup></b> confirmation number</span></li>
                    <li><span class="ul_text"><b>HotelsDifferently<sup>sm</sup></b> account number</span></li>
                    <li><span class="ul_text">Email address associated with your account</span></li>
                    <li><span class="ul_text">The name you wish to change your reservation to</span></li>
                </ul>
            </div>
            <div class="ques_border"></div>

            <div class="questions">
                <span>Q 52</span>
                Can you place a hold on my reservation until I pay for it?
            </div>
            <div class="answers">
                Our quotes are valid for 24 hours upon sending you the offer and then your quote expires. If you would like to book the hotel once your quote expires, you will need to request a new quote from us and make the booking within 24 hours. You will be notified once your quote is sent to you via email.
            </div>
            <div class="ques_border"></div>

            <div class="questions">
                <span>Q 53</span>
                How will your charge appear on my credit card statement?
            </div>
            <div class="answers">
                When you make a payment with our credit card processor, your payment will appear as <b>"HOTELSDIFFERENTLY"</b> or <b>"PAYPAL*HOTELSDIFFE"</b>.

            </div>
            <div class="ques_border"></div>

            <div class="questions">
                <span>Q 54</span>
                Can you put my refund onto a credit card other than the one I used to pay for the hotel?
            </div>
            <div class="answers">
                No. For your and our protection, we can only refund your payment in the original form of payment. In case this is impossible due to a technicality, then we can issue your refund via Western Union® (additional fees may apply) or PayPal<sup>TM</sup> (additional fees may apply) but this requires extra time and special handling. If that is the case, please log into your account first and then go to Contact Us and select "Billing" in the drop-down and provide us: <br />
                <br />
                <ul type="number" class="bullet_color">
                    <li><span class="ul_text">Brief description of your situation </span></li>
                    <li><span class="ul_text"><b>HotelsDifferently<sup>sm</sup></b> confirmation number</span></li>
                    <li><span class="ul_text"><b>HotelsDifferently<sup>sm</sup></b> account number</span></li>
                    <li><span class="ul_text">Full name of the person who paid for the booking</span></li>
                    <li><span class="ul_text">Current address where you would like your refund to be mailed</span></li>
                </ul>
                <br />
                and our representative will be in touch after carefully reviewing your situation. <br />
            </div>
            <div class="ques_border"></div>

            <div class="questions">
                <span>Q 55</span>
                My credit card information was compromised elsewhere and the number on my credit card has changed. I want to cancel my reservation and get the refund but how will you process the refund if my credit card number is blocked? 
            </div>
            <div class="answers">
                Most credit card issuers although they change your credit card number to prevent additional fraud, your account number with the credit card issuer does NOT change. When sending a refund to your already blocked credit card, most issuers will automatically adjust that amount to your new credit card number since it belongs to the same account number; therefore getting a refund to your new credit card number should be automated and no problem. You are always welcome to contact the credit card issuer first to ensure that you get your refund promptly as credit card issuers systems vary.
            </div>
            <div class="ques_border"></div>

            <div class="questions">
                <span>Q 56</span>
                How secure is the payment on this website?
            </div>
            <div class="answers">
                Our payment Site is <b>EXTREMELY SECURE!</b> Our address bar has the green bar, which means we implemented SSL + Extended Validation which is currently one of the most secure system out there. For more information about the SSL+EV, please click <a style="font-weight:bold;" href="<?php echo base_url()."user/legal_corner/security_statement";?>" target="_blank">HERE</a> to access our <b>Security Statement.</b> <br/><br/>
                For our payment processor we use PayPal<sup>TM</sup> which is the most trusted payment processor in the world right now and when you submit your highly sensitive payment information, you will be directed to PayPal<sup>TM</sup> 's payment Site to ensure that your payment information is in safe hands. 
<b style="color:#ff0000">We DO NOT maintain any credit or debit card information in our systems; it is maintained ONLY with the payment processor!</b>
            </div>
            <div class="ques_border"></div>

            <div class="questions">
                <span>Q 57</span>
                Can I send a special request to the hotel when I make my reservation?
            </div>
            <div class="answers">
                Yes, absolutely. Here is how you can submit a special request to the hotel that you would like to book:
                Once you are ready to pay for your reservation, click on the "PAY" button under "RESERVATIONS -  QUOTES" and simply enter whatever request you may have for the hotel and we will be sure to forward it to them. Please be aware that special requests are never guaranteed by the hotel and 	<b>HotelsDifferently<sup>sm</sup></b> has no way of making hotels guarantee guest requests.


            </div>
            <div class="ques_border"></div>

            <div class="questions">
                <span>Q 58</span>
                Do you offer travel insurance?
            </div>
            <div class="answers">
                <p>We (<strong>HotelsDifferently<sup>sm</sup></strong>) do NOT offer travel insurance, however you can purchase travel insurance from a third party insurance company. Please note that we do not deal with or sell any types of insurances and any insurance concerns should be addressed with the insurance company directly.</p>
            </div>
            <div class="ques_border"></div>

            <div class="questions">
                <span>Q 59</span>
                What is the minimum legal age for hotel check-in?
            </div>
            <div class="answers">
                Certain hotels have age restrictions, so please check directly with the hotel to ensure that you will not have a problem during check-in. Should you have any concerns regarding to the hotel's minimum age check-in policy, please do not hesitate to contact them.
            </div>
            <div class="ques_border"></div>

            <div class="questions">
                <span>Q 60</span>
                How many rooms can I purchase at once?
            </div>
            <div class="answers">
                The highest amount of rooms that you can purchase is currently set at 9. Should you wish to make a booking for more than 9 rooms, please submit a separate quote request and you can make a separate booking for additional rooms.
            </div>
            <div class="ques_border"></div>
            <div class="questions">
                <span>Q 61</span>
                What if I have to check-out from the hotel earlier?
            </div>
            <div class="answers">
                Every hotel has its own policy when it comes to early check-outs. Please inquire about it with the hotel of your choice.
            </div>
            <div class="ques_border"></div>
        </div>
    </div>
</div>