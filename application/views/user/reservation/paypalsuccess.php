<style>
    .greenbg{
        background: green !important;
    }   
    .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
        border-bottom-width: 2px;
        text-align: center;
    }
</style>
<?php
//print_r($datacontent);
?>
<div class="width-row margin-top-20">
    <div class="main_cont">
        <div class="pagetitle margin-bottom-10">
            <h1><?php echo $this->page_name; ?></h1>
        </div>



        <div class="content_row">

            <div class="success_login_main_cont margin-top-20" style="width: 100%;background: none repeat scroll 0 0 rgba(255, 255, 255, 0);border-radius: 5px; ">
<!--                <p style="text-align: center;color: red;margin-bottom: 15px;">
                    <b>CONGRATULATIONS! Your reservation is booked!</b>
                </p>-->
                <p>
                    Thank you for booking with <b>HotelsDifferently!<sup>sm</sup></b> We are delighted that you decided to choose us for your next getaway.
                </p>
                <p>
                    Your charge will appear on your statement as:
                   <b>PAYPAL*HOTELSDIFFE</b> or<b> HOTELSDIFFERENTLY</b>
                </p>
<!--                <p>
                    Your reservation confirmation number is <b><?php echo $transactionid; ?></b>.

                </p>-->
                <p>
                    As soon as we secure your reservation, we will send you an email notification.
                </p>
                <p>
                    Once again, here are the details of your reservation:
                    </p>
                <!--<form  method="post" id="add_quoterequests" class="form-horizontal">-->
                <!--<form action='https://www.sandbox.paypal.com/cgi-bin/webscr' method='post' name='form1;'>-->
                    <!--<input type="image" src="https://paypal.com/en_US/i/btn/btn_buynowCC_LG.gif" name="submit">-->
                    <div class="portlet-body form">
                        <div class="form-body">
                            <div class='row' style="padding: 20px;">
                                <div class='col-md-6'>
                                    <div class="row">
                                        <div class="form-group">

                                            <label class="control-label  col-md-12">Check-in:  
                                                <label class="control-label normalfont checkin"><?php echo $datacontent[0]['var_checkin']; ?></label></label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="control-label col-md-12">City:  
                                                <label class="control-label  normalfont city"><?php echo $datacontent[0]['var_city']; ?></label></label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="control-label col-md-12">Name of hotel:  
                                                <label class="control-label  normalfont nameofhotel"><?php echo $datacontent[0]['var_NOH']; ?></label></label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="control-label col-md-12"><span style="float:left;">Address: </span>
                                                <label class="control-label normalfont policy" style="float:left; margin-left:6px;"><?php echo nl2br($datacontent[0]['var_address']); ?></label></label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="control-label col-md-12"><span style="float:left;">Room Type: </span>
                                                <label class="control-label normalfont policy" style="float:left; margin-left:6px;"><?php echo nl2br($datacontent[0]['var_room_type']); ?></label></label>
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="control-label col-md-12">TripAdvisor: 
                                                <label class="control-label normalfont tripadvisor">&nbsp;<?php echo $datacontent[0]['var_tripadvisor']; ?></label></label>
                                        </div>
                                    </div>


                                </div>
                                <div class='col-md-6'>
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="control-label  col-md-12">Check-out: 
                                                <label class="control-label normalfont checkout"><?php echo $datacontent[0]['var_checkout']; ?></label></label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="control-label col-md-12" style="">QUOTE ID: 
                                                <label class="control-label normalfont  quoteid"><?php echo $datacontent[0]['var_uniq_quoteid']; ?></label>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="control-label col-md-12">Stars: 
                                                <label class="control-label normalfont stars"><?php echo $datacontent[0]['var_star']; ?></label></label>
                                        </div>
                                    </div>


                                </div>
                                <div  class='col-md-12 '>
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Rooms: 
                                                <label class="control-label normalfont rooms"><?php echo $datacontent[0]['var_room']; ?></label></label>
                                            <label class="control-label col-md-4">Adults: 
                                                <label class="control-label normalfont adults"><?php echo $datacontent[0]['var_adult']; ?></label></label>
                                            <label class="control-label col-md-4">Children: 
                                                <label class="control-label normalfont children"><?php echo $datacontent[0]['var_child']; ?></label></label>
                                        </div>                                    
                                    </div>
                                    
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="control-label col-md-12"><span style="float:left;">Cancellation policy: </span>
                                                <label class="control-label normalfont policy" style="float:left; margin-left:6px;"><?php echo nl2br($datacontent[0]['var_policy']); ?></label></label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="control-label col-md-12"><span style="float:left;">Comments:</span>  
                                                <span class="control-label normalfont comments" style="float:left; margin-left:6px;"><?php echo nl2br($datacontent[0]['var_comment']); ?></span></label>
                                        </div>
                                    </div>
                                    <!--<div class="form-group border-bottom margin-top-40" style="border-bottom:1px solid #e5e5e5;"></div>-->
                                    <div class="row">
                                        <div class="form-group text-center">
                                            <label class="col-md-6" style="text-align: right;margin-top:17px;padding-right: 0px; ">Quoted price: </label>
                                            <label class="control-label col-md-6  yourquote" style=" margin-top:19px;text-align: left;padding-left: 5px;line-height: 17px;">
                                                <?php echo '$'.number_format((float) $datacontent[0]['var_prize'], 2, '.', ','); ?>
                                            </label>
                                        </div>
                                    </div>
                                    <?php if(!empty($coupons)){ ?>
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="col-md-6" style="text-align: right;padding-right: 0px;">Coupon used: </label>
                                            <div class="scroll col-md-6" style="padding-left: 5px;line-height: 22px;">
                                                
                                             <?php       foreach ($coupons as $row) { ?>
                                                    <div style="padding: 0px; width: 100%; <?php
                                                    ?>" class="col-md-3">
                                                        <label style="padding: 0px ! important;" class="checkbox dis_block">

                                                            <?php
                                                            if ($row['var_coupan_type'] == 1) {
                                                                echo $row['var_couponvalue'] . '% OFF';
                                                            } else {
                                                                echo '$' . $row['var_couponvalue'] . ' OFF';
                                                            }
                                                            ?>

                                                        </label>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                  <?php  }else{ echo '';}?>
                                    <div class="row" style="margin-bottom: 20px;">
                                        <div class="form-group text-center">
                                            <label class="col-md-6" style="text-align: right;color: red;padding-right: 0px;">Paid amount: </label>
                                            <label class="control-label col-md-6 finalamout" style="line-height: 22px;color: red;padding-left: 5px;text-align: left; ">
                                                <?php echo '$' .number_format((float) $paidamount, 2, '.', ','); ?>
                                            </label>
                                      
                                        </div>
                                    </div>
<!--                                    <div class="row">
                                         <label class="col-md-12" style="text-align: center;color: red;margin-bottom: 15px;">CONGRATULATIONS! Your reservation is booked! </label>
                                    </div>-->
                                    <div class="row">
                                        <p>Please do not hesitate to let us know if there is anything we can do to make your stay excellent.</p>
                                        </div>
<!--                                    <div class="row sc_links">
                                        Share your savings with friends and colleagues:  <a class="fb_link" href="https://www.facebook.com/sharer/sharer.php?u=http://demo.webeet.net/dealz/front/home/member_signup/534534"></a><a class="tw_link" href="#"></a>
                                    </div>-->
                                    <div class="row sc_links" style="text-align: center;margin-top: 30px;">
                                        Share your savings with friends and colleagues and make money by referring them to our site:  
                                        <a class="fb_link" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo base_url();?>front/home/paymentsharefacebook/<?= $refer_link['var_accountid']; ?>/<?= $facebookamount ?>"></a>
                                        <a class="tw_link" target="_blank" href="https://twitter.com/intent/tweet?url=aaaaa.dd&text=<?php echo $title; ?>&original_referer=&related=vvvvvv&via=twitter"></a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                <!--</form>-->
            </div>
        </div> 
    </div>







</div>