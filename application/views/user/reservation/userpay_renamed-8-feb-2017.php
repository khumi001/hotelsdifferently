<style>
    .greenbg{background: green !important;}   
    .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {border-bottom-width: 2px;text-align: center;}
</style>

<?php
$paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
$paypal_id = 'kartikdesai123@gmail.com';
?>
<div>
    <div class="width-row margin-top-20">

        <div class="main_cont">

            <div class="pagetitle margin-bottom-10">
                <h1>
                    <span><?php echo $this->page_name; ?></span>
                    <span style="text-align: center; display: inline-block; width: 68%">Quote ID:  <label class="control-label normalfont  quoteid"><?php echo $datacontent[0]['var_uniq_quoteid']; ?></label></span>
                </h1>
            </div>

            <div class="content_row">
                <div class="success_login_main_cont margin-top-20" style="width: 100%;background: none repeat scroll 0 0 rgba(255, 255, 255, 0);border-radius: 5px; ">
                    <div class="modal-header" style="display: none;">
                    </div>

                    <form action='https://www.sandbox.paypal.com/cgi-bin/webscr' method='post' name='form1;'>
                        <!--<form action='https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout' method='post' name='form1;'>-->

                        <!--<form method='post' name='form' id="payment_way">-->

                    <!--<input type="image" src="https://paypal.com/en_US/i/btn/btn_buynowCC_LG.gif" name="submit">-->

                        <div class="portlet-body form">

                            <div class="form-body">

                                <div class='row' style="padding: 0 20px 20px 20px;">

                                    <div class='col-md-12' style="color:red;text-align: center;">
                                    </div>

                                    <div class='col-md-6'>

                                        <div class="row">

                                            <div class="form-group">

                                                <input type='hidden' name='cmd' value='_xclick'>
                                                <input type='hidden' name='business' value='paypal@hotelsdifferently.com'>
                                                <input type="hidden" name="lc" value="US">
                                                <input type='hidden' name='item_name' value='Booking'>
                                                <input type='hidden' name='item_number' value='1'>
                                                <input type='hidden' name='amount' class="paidamount" value='<?php echo $datacontent[0]['var_prize']; ?>'>
                                                <input type='hidden' name='currency_code' value='USD'>
                                                <input type="hidden" name="button_subtype" value="products">
                                                <input type="hidden" name="cn" value="Add special instructions to the seller">
                                                <input type="hidden" name="no_shipping" value="2">
                                                <input type="hidden" name="weight_unit" value="lbs">
                                                <input type="hidden" name="bn" value="PP-BuyNowBF:btn_buynowCC_LG.gif:NonHosted">
                                                <input type='hidden' name='custom' class="customfield" value='<?php echo $replayquoteid; ?>'>
                                                <input type="hidden" name="notify_url" class="returnurl1" value="<?php echo base_url() . 'front/confirm_account/paymentsuccess/'; ?>">
                                                <input type='hidden' name='cancel_return' value='<?php echo base_url(); ?>'>
                                                <input type='hidden' name='return'  class="sendurl returnurl" value='<?php echo base_url() . 'user/reservation/paypalsuccess/' . $replayquoteid . '/'; ?>'>
                                                <!--<input type='hidden' name='return'  class="sendurl" value='<?php echo base_url(); ?>'>-->
                                                <input type="hidden" name="landing_page" value="billing">
                                                <input type="hidden" name="USERSELECTEDFUNDINGSOURCE" value="CreditCard">
                                                <!--<input type='hidden' name='dataurl' class="returnurl" value='<?php echo base_url() . 'user/reservation/paypalsuccess/' . $replayquoteid . '/'; ?>'>-->
                                                <!--<input type='hidden' name='payment_paypal' value="https://www.sandbox.paypal.com/cgi-bin/webscr"/>-->
                                                <div  class="replayid" id='<?php echo $replayquoteid; ?>'></div>
                                                <label class="control-label  col-md-12">Check-in:  

                                                    <label class="control-label normalfont checkin"><?php echo $datacontent[0]['var_checkin']; ?></label></label>

                                            </div>

                                        </div>

                                        <div class="row">

                                            <div class="form-group">

                                                <label class="control-label col-md-12">City:  

                                                    <label class="control-label  normalfont city"><?php echo $datacontent[0]['var_city']; ?></label></label>

                                            </div>

                                        </div>

                                        <div class="row">

                                            <div class="form-group">

                                                <label class="control-label col-md-12">Name of hotel:  

                                                    <label class="control-label  normalfont nameofhotel"><?php echo $datacontent[0]['var_NOH']; ?></label></label>

                                            </div>

                                        </div>
                                        <div class="row">

                                            <div class="form-group">

                                                <label class="control-label col-md-12">Address:  

                                                    <label class="control-label  normalfont nameofhotel"><?php echo $datacontent[0]['var_address']; ?></label></label>

                                            </div>

                                        </div>
                                        <div class="row">

                                            <div class="form-group">

                                                <label class="control-label col-md-12">Room Type:  

                                                    <label class="control-label  normalfont nameofhotel"><?php echo $datacontent[0]['var_room_type']; ?></label></label>

                                            </div>

                                        </div>
                                        

                                        <div class="row">

                                            <div class="form-group">

                                                <label class="control-label col-md-12">TripAdvisor: 

                                                    <label class="control-label normalfont tripadvisor">&nbsp;<?php echo $datacontent[0]['var_tripadvisor']; ?></label></label>

                                            </div>

                                        </div>

                                    </div>

                                    <div class='col-md-6'>

                                        <div class="row">

                                            <div class="form-group">

                                                <label class="control-label  col-md-12">Check-out: 

                                                    <label class="control-label normalfont checkout"><?php echo $datacontent[0]['var_checkout']; ?></label></label>

                                            </div>

                                        </div>

                                        <div class="row">

                                            <div class="form-group">

                                                <label class="control-label  col-md-12">&nbsp;

                                                    <label class="control-label normalfont ">&nbsp;</label></label>

                                            </div>

                                        </div>

                                        <div class="row">

                                            <div class="form-group">

                                                <label class="control-label col-md-12">Stars: 

                                                    <label class="control-label normalfont stars"><?php echo $datacontent[0]['var_star']; ?></label></label>

                                            </div>

                                        </div>

                                        <!--                                    <div class="row">
    
                                                                                <div class="form-group">
    
                                                                                    <label class="control-label col-md-12 text-center" style="">QUOTE ID: 
    
                                                                                        <label class="control-label normalfont  quoteid">XXXXXXXX</label></label>
    
                                                                                </div>
    
                                                                            </div>-->

                                    </div>

                                    <div  class='col-md-12 '>

                                        <div class="row">

                                            <div class="form-group">

                                                <label class="control-label col-md-3">Rooms: 

                                                    <label class="control-label normalfont rooms"><?php echo $datacontent[0]['var_room']; ?></label></label>

                                                <label class="control-label col-md-3">Adults: 

                                                    <label class="control-label normalfont adults"><?php echo $datacontent[0]['var_adult']; ?></label></label>

                                                <label class="control-label col-md-6">Children: 

                                                    <label class="control-label normalfont children"><?php echo $datacontent[0]['var_child']; ?></label></label>

                                            </div>    

                                        </div>

                                        <div class="row">

                                            <div class="form-group">

                                                <label class="control-label col-md-12"><span style="float:left;">Cancellation policy: </span>

                                                    <span class="control-label normalfont policy" style="float:left; margin-left:6px;"><?php echo nl2br($datacontent[0]['var_policy']); ?></span></label>

                                            </div>

                                        </div>

                                        <div class="row">

                                            <div class="form-group">

                                                <label class="control-label col-md-12"> <span style="float:left;">Comments:</span> 

                                                    <span class="control-label normalfont comments" style="float:left; margin-left:6px;">

                                                        <?php echo nl2br($datacontent[0]['var_comment']); ?>

                                                    </span>

                                                </label>

                                            </div>

                                        </div>

                                        <!--<div class="form-group border-bottom margin-top-40" style="border-bottom:1px solid #e5e5e5;"></div>-->

                                        <div class="row">

                                            <div class="form-group text-center">

                                                <label class="col-md-6" style="text-align: right;margin-top:17px;font-size: 15px;padding-right: 5px; ">Current price: </label>

                                                <label class="control-label col-md-6  yourquote" style=" margin-top:19px;text-align: left;font-size: 15px;padding: 0px;line-height: 19px">

                                                    <?php echo '$' . number_format((float) $datacontent[0]['var_prize'], 2, '.', ','); ?>

                                                </label>

                                            </div>

                                        </div>

                                        <div class="row">

                                            <div class="form-group">

                                                <label class="col-md-6" style="text-align: right;font-size: 15px;padding-right: 5px;">Available coupons: </label>

                                                <?php if ($datacontent[0]['var_prize'] > 200) { ?>

                                                    <div class="scroll col-md-3" style="padding: 0px;">

                                                        <?php foreach ($coupons as $row) { ?>

                                                            <div style="padding: 0px; font-size: 12px !important; width: 100%; <?php ?>" class="col-md-3" style="padding: 0px;">

                                                                <label style="padding: 0px ! important;font-size: 15px;" class="checkbox dis_block">

                                                                    <div class="checker" style="display: inline-block;float: left;margin-right: 7px !important;margin-top: 2px !important; ">

                                                                        <span>

                                                                            <input type="checkbox" name="coupans" class="inline-labels" value="<?php echo $row['int_glcode']; ?>" style="margin: 3px 0;">

                                                                        </span>

                                                                    </div>

                                                                    <?php
                                                                    if ($row['var_coupan_type'] == 1) {

                                                                        echo $row['var_couponvalue'] . '% OFF';
                                                                    } else {

                                                                        echo '$' . $row['var_couponvalue'] . ' OFF';
                                                                    }
                                                                    ?>



                                                                </label>

                                                            </div>

                                                        <?php } ?>

                                                    </div>

                                                <?php } ?>

                                                <!--                                            <div class="col-md-3 nottouse hidden">
    
                                                                                                <a href="javascript:;" style="color:black;">NOT TO USE COUPONS</a>                                             
    
                                                                                            </div>-->



                                            </div>

                                        </div>

                                        <div class="row">

                                            <div class="form-group text-center">

                                                <label class="col-md-6" style="text-align: right;color: red;font-size: 15px;padding-right: 5px;">Final amount due: </label>

                                                <label class="control-label col-md-6 finalamout" style="line-height: 24px;color: red;text-align: left; padding: 0px;font-size: 15px;">

                                                    <?php echo '$' . number_format((float) $datacontent[0]['var_prize'], 2, '.', ','); ?>

                                                </label>

                                                <input type="hidden" class="finalamoutinput" value="<?php echo $datacontent[0]['var_prize']; ?>">

                                                <input type="hidden" class="replayquoteid" value="<?php echo $replayquoteid; ?>">



                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="form-group text-center">
                                                <p style="margin:20px 0;font-size: 16px;">
                                                    <a href="#ftc-disclaimer-statement" style="color: #000;font-weight: bold;font-size: 16px;"data-toggle="modal" class="">FTC Disclaimer Statement</a>
                                                    <!--<a style="padding:5px 20px; " href="#resubmit_modal" data-toggle="modal" class="" id="3">Resubmit</a>-->
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Mayur Code -->
                                    <!--<a style="padding:5px 20px; " href="#resubmit_modal" data-toggle="modal" class="" id="3">Resubmit</a>-->
                                    <div class="col-md-12 text-center" style="margin-top:10px;">
                                        <div class="col-md-6">
                                            <div class="form-group text-center" style="font-size: 17px;">
                                                <label>Credit Card / Debit Card / PayPal<sup>TM</sup></label>
                                            </div>
                                            <div class="form-group" style="margin-top: 35px;">
                                                <!-- PayPal Logo -->
                                                <table border="0" cellpadding="10" cellspacing="0" align="center">
                                                    <tr>
                                                        <td align="center"></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center">
                                                            <!--<a href="https://www.paypal.com/webapps/mpp/paypal-popup" title="How PayPal Works" onclick="javascript:window.open('https://www.paypal.com/webapps/mpp/paypal-popup','WIPaypal','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1060, height=700'); return false;">-->
                                                            <img style="width: 241px;" src="https://www.paypalobjects.com/webstatic/mktg/logo/AM_mc_vs_dc_ae.jpg" border="0" alt="PayPal Acceptance Mark">
                                                            <!--</a>-->
                                                        </td>
                                                        <td>
                                                            <a href="https://www.paypal.com/us/verified/pal=paypal@hotelsdifferently.com" target="_blank">
                                                                <img style="width: 83px; margin: -1px 0px 0px" src="https://www.paypal.com/en_US/i/icon/verification_seal.gif" border="0" alt="Official PayPal Seal">
                                                            </a>
                                                        </td>
                                                    </tr>

                                                    <?php
                                                    $success_url = "https://www.hotelsdifferently.com/user/reservation/paypalsuccess/" . $replayquoteid;

                                                    $ch = curl_init();

                                                    $nonce = sprintf('%0.0f', round(microtime(true) * 1000000));

                                                    curl_setopt_array($ch, array(
                                                        CURLOPT_URL => "https://api.coinbase.com/v1/buttons",
                                                        CURLOPT_RETURNTRANSFER => true,
                                                        CURLOPT_HTTPHEADER => array(
                                                            "ACCESS_KEY: " . "YBMMFa1QGzPdxxEl",
                                                            "ACCESS_NONCE: " . $nonce,
                                                            "ACCESS_SIGNATURE: " . hash_hmac("sha256", $nonce . "https://api.coinbase.com/v1/buttonsbutton[name]=QUOTE ID: " . $datacontent[0]['var_uniq_quoteid'] . "&button[price_string]=" . number_format((float) $datacontent[0]['var_prize'], 2, '.', ',') . "&button[price_currency_iso]=USD&button[variable_price]=1&button[success_url]=" . $success_url, "HH3YLrRx2AGcSSCFSlyoSgo2GNCzuG1U")
                                                        ),
                                                        CURLOPT_POSTFIELDS => "button[name]=QUOTE ID: " . $datacontent[0]['var_uniq_quoteid'] . "&button[price_string]=" . number_format((float) $datacontent[0]['var_prize'], 2, '.', ',') . "&button[price_currency_iso]=USD&button[variable_price]=1&button[success_url]=" . $success_url,
                                                        CURLOPT_POST => true
                                                    ));

                                                    $results = curl_exec($ch);

                                                    curl_close($ch);



                                                    $json_a = json_decode($results, true);

                                                    $final_code = $json_a['button'][code];
                                                    ?>
                                                </table><!-- PayPal Logo -->
                                            </div>
                                            <div class="form-group" style="margin-top: 18px;">
                                                <input type='submit' class='btn default_btn pay_quote listing_btn' style='padding: 7px 30px;' value="PAY NOW">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group text-center" style="font-size: 17px;">
                                                <label><span style="color: red;">1.5% DISCOUNT</span> with BitCoin!</label>
                                                <p><a href="#paypal-instruction" style="color: #000;font-size: 14px;"data-toggle="modal" class="">Payment instructions</a></p>
                                            </div>
                                            <div class="form-group">

                                                <textarea class="form-control policy_text" name="policy_text" style="width: 300px; height: 100px; margin: 0 auto;"><?php echo $bicoin_policy[0]['var_policy']; ?></textarea>
                                            </div>
                                            <div class="showlink">
                                                <a class="btn default_btn" href="javascript:void(0)" style="display:block; padding: 8px 20px; width: 150px; margin: 0 auto; background: linear-gradient(#3384c3, #2375b4); border: 1px solid #2a68a5; border-radius: 3px; font-size: 13px; font-weight: 500; font-family: Helvetica Neue,Helvetica,Arial,sans-serif; color: #fff;">Pay With Bitcoin</a>
                                            </div>
                                            <div class="form-group hidelink" style="padding-left:35px;display: none;">

                                                <a class="coinbase-button Removedisable_bitton" data-code="<?php echo $final_code ?>" data-button-style="custom_small" href="https://www.coinbase.com/checkouts/<?php echo $final_code ?>">Pay With Bitcoin</a>
                                                <script src="https://www.coinbase.com/assets/button.js" type="text/javascript"></script>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>

            <span style="text-align: right;color: red;font-size: 15px;padding-right: 5px;">* Your signup coupon can only be applied on reservations of $200.00 or more</span>

            </form>

        </div>

    </div> 

    <div id="ftc-disclaimer-statement" class="modal fade" role="dialog" aria-hidden="true">

        <div class="success_login_main_cont margin-top-20" style="width:50%;">

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>

                <h3 class="form-title">FTC Disclaimer Statement</h3>

            </div>

            <div style="" class="">



                <p style="text-align: center;font-size: 15px; font-weight: bold;">DISCLOSURE</p>
                <p style="padding: 10px;">Consistent with the FEDERAL TRADE COMMISSION (FTC) guidelines relative to consumers (Members), we wish to provide you the following disclosure. The purpose of this disclosure is to establish any compensatory affiliation between the Company and its Affiliates, Commercial Advertisers, and Third Party Accounts. For questions about this website, please contact us via our "contact us" feature.</p>

                <p style="padding: 10px;">This website may at times accept forms of cash advertising, sponsorship, paid insertions or other forms of compensation, whether tangible or intangible, from Commercial Advertisers and Third Party Accounts. Such content may or may not be identified as sponsored or paid content. The compensation received, if any may or may not influence our advertising content, if any. However, if we give an opinion on such content, we always give our honest opinions, experiences and beliefs concerning such content. However, we will NEVER recommend any other Commercial Advertiser or Third Party Account, and we may, or MAY NOT have had any experience with their product or services. Nor is all Commercial Advertiser or Third Party Account content provided solely for compensation.<b> Clicking on links or banners from Commercial Advertisers or Third Parties may generate income for us.</b> </p>
                <p style="padding: 10px;">We compensate our Affiliates in cash and other potential means that have cash value for their referrals of you, to our Site, from their website(s) or banners. The views and opinions expressed on this website are ours only. <b>As always, you should do your own due diligence and verify any claims, quotes, statistics about any products or services, including ours, mentioned on this website.</b></p>

            </div>

            <div class="modal-footer text-center" style="padding: 3px;text-align: center;">

                <buttom type='submit' aria-hidden="true" data-dismiss="modal" class='btn default_btn listing_btn' style='padding: 7px 30px;'>OK</buttom>

            </div>



        </div>

    </div>

    <div id="paypal-instruction" class="modal fade" role="dialog" aria-hidden="true">

        <div class="success_login_main_cont margin-top-20" style="width:50%;">

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>

                <h3 class="form-title">Payment Instruction</h3>

            </div>

            <div style="" class="">

                <p> In order to pay with BitCoin please read our BitCoin Payment Policy by scrolling all the way down to the bottom of the policy. Once you scrolled down, the 1.5% OFF BitCoin payment button becomes active and you will be able to checkout immediately thereafter.</p>

                <p>Please note that different browsers may have different bugs that have yet to be fixed by the developers of either the various browsers or the BitCoin payment processor. While they are working on this issue, the following may occur: </p>
                <p><b>PROBLEM #1:</b> Upon clicking on the payment button, you may not see the BitCoin payment popup.</p>
                <p><b>PROBLEM #2:</b> Once you successfully login to your CoinBase� wallet on the popup screen, you might still notice that you are not logged in and cannot perform anything.</p>

                <p><b> SOLUTION:</b> Please fully refresh the page, read our policy by scrolling down and try again. You should be able to perform the payment afterwards.</p>
            </div>

            <div class="modal-footer text-center" style="padding: 3px;text-align: center;">

                <buttom type='submit' aria-hidden="true" data-dismiss="modal" class='btn default_btn listing_btn' style='padding: 7px 30px;'>OK</buttom>

            </div>
        </div>
    </div>
</div>