<style>
    .greenbg{
        background: green !important;
    }
    .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
        border-bottom-width: 2px;
        text-align: center;
    }
    #confirm_reservation tr > td{word-wrap: break-word;}
    #confirm_reservation_length.dataTables_length{
        position:absolute;
        top:-105px;
        right:0;
        float:right;
    }
    .radio{
        padding-left:0;
    }
    #confirm_reservation_filter.dataTables_filter{
        position:absolute;
        top:-60px;
        right:0;
    }
</style>
<div class="width-row margin-top-20">
<div class="main_cont">
    <div class="pagetitle margin-bottom-10">
        <h1><?php echo $this->page_name; ?></h1>
    </div>
    <div class="form-group col-md-12 hide filter-box">
        <div class="row margin-top-10 filter-box1" style="margin-bottom:15px;">
            <div class="form-group">
                <label class="col-md-1 control-label" style="margin:6px 0 0;">Display:</label>
                <input style="width:12%;float:left;" type="text" id="from" class="date-picker form-control icon-pick date-filter" />
                <input style="width:12%;float:left; margin-left:10px;" type="text" id="to" class="date-picker form-control icon-pick date-filter" />
                <label class="checkbox" style="padding: 0px; margin:6px 0 0;">
                    <input type="radio" style="margin-top: -2px;margin-left: 10px; padding-left:0;" id="filter" name="filter"  checked="checked"  class="radio-inline filter" value="">&nbsp;ALL
                </label>
                <label class="checkbox" style="padding: 0px; margin:6px 0 0;""><input type="radio" style="margin-top: -2px;margin-left: 10px; padding-left:0;" id="filter3" name="filter" class="radio-inline filter" value="ACTIVE">&nbsp;ACTIVE </label>
                <label class="checkbox" style="padding: 0px;  margin:6px 0 0;"><input type="radio" style="margin-top: -2px;margin-left: 10px; padding-left:0;" id="filter1" name="filter" class="radio-inline filter" value="OUTDATED">&nbsp;EXPIRED </label>
                <label class="checkbox" style="padding: 0px; margin:6px 0 0;"><input type="radio" style="margin-top: -2px;margin-left: 10px; padding-left:0;" id="filter3" name="filter" class="radio-inline filter" value="CANCEL">&nbsp;CANCELED </label>
            </div>
            <!--<div class="form-group" style="margin-top:45px;">
                <div class="col-md-11">

                </div>
            </div>-->
        </div>
    </div>
    <div class="content_row">
        <!--
        <div id="resDiv" style="dsiplay:none;">

        </div>
        -->
        <button style="display: none;" type="button" class="btn btn-info btn-lg" id="callnow" data-toggle="modal" data-target="#cancelpopup">Open Modal</button>
        <table class="table table-striped table-bordered table-hover"  id="confirm_reservation">
            <thead>
            <tr role="row" class="heading">
                <th width='10%' style="">Date/Time</th>
                <th width='10%' style="">Guest Name</th>
                <th width='10%' style="">Confirmation #</th>
                <th width='10%' style="">Location</th>
                <th width='10%' style="">Hotel Name</th>
                <th width='10%' style="">Room Type</th>
                <th width='10%'>Check in</th>
                <th width='13%'>Check out</th>
                <th width='15%' style="">Paid</th>
                <?php if($this->session->userdata['valid_user']['var_usertype'] == 'TF'){?>
                    <th width='15%' style="">Invoice / Voucher</th>
                <?php }else{?>
                    <th width='15%' style="">Invoice</th>
                <?php }?>
                <th width='5%' style="">Amendment</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<!-- Modal -->
<button data-toggle="modal" data-target="#admincancelpopup1" id="admincancelpopupbtn1" style="display:none;"></button>
<div id="admincancelpopup1" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" id="admincancelpopup1-close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">CANCEL RESERVATION</h4>
            </div>
            <div class="modal-body">
                <!--Before canceling your reservation <b>confirmation # <span id="admincanconfinumber"></span></b> please double-check if you are entitled for a refund or not.-->
                <div id="canadmindynamicadata"></div>
                <br>
                Are you sure you want to cancel your reservation? Once you cancel it, we will not be able to reinstate it!
                <!--<p>
                    Please note that if you used a coupon, then your actual amount for refund may be less and in which case you will be refunded in USD value you paid + coupon used for this reservation will be reinstated.
                </p>-->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="admincancelpopup1-yes" onclick="cancelAdminReser();">Yes</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<button data-toggle="modal" data-target="#admincancelpopup" id="admincancelpopupbtn" style="display:none;"></button>
<div id="admincancelpopup" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">CANCEL RESERVATION</h4>
            </div>
            <div class="modal-body">
                <p>
                    Thank you for your cancelation request! Your reservation cancelation confirmation will be sent to your email within a few minutes and if you are eligible for a refund, it will be processed within 5-7 business days.
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal">Ok</button>
            </div>
        </div>
    </div>
</div>
<button style="display: none;" type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#confirmcancelpopup" id="confirmcancelpopupbtn" >click</button>
<!-- Modal cancel confirmation -->
<div id="confirmcancelpopup" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">CANCEL RESERVATION</h4>
            </div>
            <div class="modal-body">
                <p>
                    Thank you for your cancelation request! Your reservation cancelation confirmation will be sent to your email within a few minutes. Thank you for your patience!
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal">Ok</button>
            </div>
        </div>
    </div>
</div>

<div id="myModal_autocomplete" class="modal fade" role="dialog" aria-hidden="true">
    <div class="success_login_main_cont margin-top-20" style="width: 600px;background: none repeat scroll 0 0 rgba(255, 255, 255, 1);border-radius: 5px; ">
        <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="fa fa-times"></i></button>
            <h3 class="form-title">CONFIRMATION NUMBER:  <label class="control-label normalfont  quoteid">XXXXXXXX</label></label></h3>
        </div>
        <form  method="post" id="add_quoterequests" class="form-horizontal">
            <div class="portlet-body form">
                <div class="form-body">
                    <div class='row' style="padding: 20px;">
                        <div class='col-md-6'>
                            <div class="form-group">
                                <label class="control-label  col-md-12">Check-in:
                                    <label class="control-label normalfont checkin">January 06,2015</label></label>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-12">City:
                                    <label class="control-label  normalfont city">HongKong</label></label>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-12">Name of hotel:
                                    <label class="control-label  normalfont nameofhotel">real hotel</label></label>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-12">TripAdvisor:
                                    <label class="control-label normalfont tripadvisor">&nbsp;tripadvisor</label></label>
                            </div>



                        </div>
                        <div class='col-md-6'>
                            <div class="form-group">
                                <label class="control-label  col-md-12">Check-out:
                                    <label class="control-label normalfont checkout">January 06,2015</label></label>
                            </div>
                            <!--                                <div class="form-group">
                                                                <label class="control-label  col-md-12">Src:
                                                                    <label class="control-label normalfont src">&nbsp;</label></label>
                                                            </div>-->
                            <div class="form-group">
                                <label class="control-label col-md-12">Stars:
                                    <label class="control-label normalfont stars">3</label></label>
                            </div>

                            <!--                                <div class="form-group">
                                                                <label class="control-label col-md-12 text-center" style="">QUOTE ID:
                                                                    <label class="control-label normalfont  quoteid">XXXXXXXX</label></label>
                                                            </div>-->

                        </div>
                        <div  class='col-md-12 '>
                            <div class="form-group">
                                <label class="control-label col-md-4">Rooms:
                                    <label class="control-label normalfont rooms">2</label></label>
                                <label class="control-label col-md-4">Adults:
                                    <label class="control-label normalfont adults">3</label></label>
                                <label class="control-label col-md-4">Children:
                                    <label class="control-label normalfont children">2</label></label>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-12">Cancellation policy:
                                    <label class="control-label normalfont policy">none</label></label>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-12">Comments:
                                    <label class="control-label normalfont comments">none</label></label>
                            </div>
                            <div class="border-bottom margin-top-40" style="border-bottom:1px solid #e5e5e5;"></div>
                            <div class="notcancel">
                                <div class="form-group  margin-top-20 text-center">
                                    <b style="color: red;">Price Comparison: </b>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-12">
                                        <span class="col-md-6 control-label normalfont" style="text-align: right;padding-right: 0px;"><span class="site1name"></span>:</span>
                                        <label class="col-md-6 control-label normalfont site1" style="padding-left: 5px;">&nbsp;$<span class="site1price"></span></label></label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-12 text-center" style="text-align: center;">
                                        <span class="col-md-6 control-label normalfont" style="text-align: right;padding-right: 0px;"><span class="site2name"></span>:</span>
                                        <label class="control-label col-md-6 normalfont site2" style="padding-left: 5px;">&nbsp;$<span class="site2price"></span></label></label>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-md-12 text-center" style="font-weight: 400px;text-align: center;">
                                        <span class="col-md-6 control-label" style="text-align: right;padding-right: 0px;"> <?php echo SITE_NAME;?><sup>sm</sup> price: </span>
                                        <label class="col-md-6 control-label yourquote" style="color: #333; padding-left: 8px;font-weight: 700;font-size: 15px;line-height: 20px; ">$250.55</label></label>
                                </div>
                                <div class="form-group text-center">
                                    <label class="control-label col-md-12 text-center" style="font-weight: 400px;text-align: center;">
                                        <span class="col-md-12 control-label" style="text-align: center; color: #66b305;padding-right: 0px;">CONGRATULATIONS! Your savings is <label class="control-label youaresaving" style="color: #66B305; font-weight: 700;padding-left: 0px;font-size: 13px; line-height: 20px;">$250.55</label></span>
                                    </label>
                                </div>
                            </div>
                            <div class="cancel hidden">
                                <div class="form-group  margin-top-20 text-center">
                                    <b style="color: red;" > Your reservation was canceled by you <span class="canceltime"></span> PST.</b>
                                </div>
                            </div>
                            <div  class='col-md-12 margin-top-20'>
                                <div class="form-group">
                                    <div class="col-md-12 text-center">
                                        <buttom type='submit' aria-hidden="true" data-dismiss="modal" class='btn default_btn listing_btn' style='padding: 7px 30px;'>OK</buttom>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </form>
    </div>
</div>
</div>
<!-- Modal cancel res -->
<div id="cancelpopup" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" id="can_model_close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">CANCEL RESERVATION</h4>
            </div>
            <div class="modal-body" id="can_model_body">
                <p>
                    Are you sure that you would like to cancel your reservation? Once you cancel your reservation it cannot be reversed. Tourico API should show this too:
                    Your refund will be “$XXX.xx” if you cancel the reservation now.
                </p>
            </div>
            <div class="modal-footer" id="can_model_footer">
                <button type="button" class="btn btn-danger" >Yes</button>
                <button type="button" class="btn btn-success" data-dismiss="modal">NO</button>
            </div>
        </div>
    </div>
</div>


<div aria-hidden="false" role="dialog" class="modal fade in" id="cansel_model" style="display: none;">
    <div class="success_login_main_cont margin-top-20" style="width: 600px;background: none repeat scroll 0 0 rgba(255, 255, 255, 1);border-radius: 5px; ">
        <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="fa fa-times"></i></button>
            <h3 class="form-title" style="color:blue">CANCEL reservation</h3>
        </div>
        <form  method="post" id="add_quoterequests" class="form-horizontal">
            <div class="portlet-body form">
                <div class="form-body">
                    <div class='row' style="padding: 20px;">

                        <!--                        <div class='col-md-12' style="text-align: center;">
                                                    <div>
                                                        <label style="color:red; ">Are you sure you would like to CANCEL your existing reservation ?</label>
                                                    </div>
                                                </div>-->
                        <div class='col-md-12'>
                            <div>
                                <label> Your reservation details can be found below: </label>
                            </div>
                        </div>
                        <div  class='col-md-12 '>
                            <div style="" class="" style="padding-top: 16px;">
                                <p style="padding: 10px">
                                    If you click on the <b>CANCEL RESERVATION </b>button, your reservation will be cancelled automatically. Please check to make sure that you are entitled for a refund in case of a cancellation.
                                </p>
                                <p style="padding: 10px">
                                    Is your reservation <b>NON-REFUNDABLE?</b> Regardless of your reason it is worth checking with us before you cancel your reservation as <b>EVEN ON NON-REFUNDABLE RESERVATIONS WE ISSUE COURTESY REFUNDS!</b> So please email our customer service department before you cancel your reservation because we might be able to issue a credit to you. Please note that this is not mandated and it is completely at the sole discretion of <b><?php echo SITE_NAME;?><sup>sm</sup></b>.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="padding: 3px;" class="modal-footer">
                    <div style="text-align:center; margin-top:18px;">
                        <button type="submit" style="padding: 7px 20px;" data-dismiss="modal" data-id="" class="btn red_btn sayyes">CANCEL RESERVATION</button>
                        <a href="<?php echo base_url().'user/contactus'?>" type="button" style="padding: 7px 20px;"  class="btn default_btn clickme">CONTACT US</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
</div>
