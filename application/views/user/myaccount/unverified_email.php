<div class="main" style="background:url('http://hotels.softech.website/public/affilliate_theme/img/home-bg.jpg');background-repeat:no-repeat;background-size:100% 100%;width:547px;margin:auto">
    <div class="m_3275774443844637159containt-main" style="background:rgba(17,125,200,0.61);color:#fff;width:100%">
        <div class="m_first">
            <div class="left" style="margin:auto;text-align:center;float:none">
                <a class="logo" href="http://www.WHotelsGroup.com" style="display:inline-block;margin:10px 0 0 10px" target="_blank">
                    <img src="http://hotels.softech.website/public/affilliate_theme/img/logo.png" style="width:130px" class="CToWUd">
                </a>
            </div>
            <div class="m_clear" style="clear:both"></div>
        </div>
        <div class="top-containt">
            <h1 style="max-width:350px;margin:20px auto"><img style="display:block;width:100%;height:auto" src="http://hotels.softech.website/public/affilliate_theme/img/txt-email.png" alt="" class="CToWUd"></h1>
        </div>
        <div class="top-hotel" style="width:90%;margin:40px auto auto auto;background:#0c5fa7;color:#ffffff">
            <div style="padding-bottom:10px;padding-top:10px;width:90%;margin:auto;font-family:'Open Sans',sans-serif;color:#fff!important;font-size:12px;line-height:16px">
                <p style="color:#fff;margin-bottom:10px">Dear
                    <strong style="color:#fff"><?php echo $user['var_fname']." ".$user['var_lname']?></strong>,
                </p>
                <p style="color:#fff;margin-bottom:10px">

                    We are sorry to let you know but you were NOT able to verify your credit or debit card. It is of paramount importance for us to verify that you are the authorized cardholder in order to fight card fraud which is why we implemented this as a preventative measure. It is just as much for your protection as it is for ours.
                </p>
                <p style="color:#fff;margin-bottom:10px">
                    <strong>What happens next?</strong> <br />
                    We issued a full refund for the membership fee you paid, your account will now remain unverified. You are welcome to try and sign up, using a different credit or debit card and email address for your account. We apologize that we were not able to assist you in obtaining your membership but we hope to get to see you back at another time.
                </p>

                <p style="color:#fff"><i>Sincerely,</i><br>
                    <strong style="color:#fff">
                        <a href="<?php echo base_url(); ?>" style="color:#fff;text-decoration:none" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=<?php echo base_url(); ?>&amp;source=gmail&amp;ust=1491043630695000&amp;usg=AFQjCNGJeZMVLW2F_9PD-Xf6jcoG7wXK-Q">
                            <b><span class="il">Wholesale Hotels Group</span></b>
                        </a>
                    </strong><br>
                </p>

                <p style="color:#fff;margin-bottom:10px">
                    <strong>PS: This inbox is NOT monitored, please do NOT send us any emails here.</strong>
                </p>

                <div class="yj6qo"></div>
                <div class="clear adL" style="clear:both"></div>
            </div>
            <div class="adL"></div>
        </div>
        <div class="adL"></div>
        <div class="adL" style="text-align:center;padding:10px 0"></div>
        <div class="adL"></div>
        <div style="font-size:11px;color:#fff;padding:5px;font-family:'Open Sans',sans-serif;display:block" class="adL">&nbsp;</div>
        <div class="adL"></div>
    </div>
    <div class="adL"></div>
</div>