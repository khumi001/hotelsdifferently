<style type="text/css">
    .image-holder-pay img {
        display: block;
        margin: 25px auto 20px;
        max-width: 71%;
    }
    .image-holder-pay > a {
        display: block;
    }
    .main_cont.payment-sec > h1 {
        font-size: 20px;
        font-weight: bold;
        margin: 0 0 10px;
        text-align: center;
        text-transform: capitalize;
    }
    .member-content {
        font-size: 15px;
        margin: 0 0 5px;
        text-align: center;
        text-transform: capitalize;
    }
    .content-holder-payment {
        display: block;
        margin: 0 auto;
        max-width: 90%;
    }
    .submit-btn-furture {
        background: rgba(0, 0, 0, 0) linear-gradient(to bottom, #71d1ff 0%, #22b3f4 4%, #198fef 100%) repeat scroll 0 0;
        border: medium none;
        border-radius: 6px;
        box-shadow: 0 1px 3px rgba(0, 0, 0, 0.4);
        color: #fff;
        font-family: "FuturaStd-Book";
        font-size: 16px;
        margin: 0 60px 30px 40px;
        padding: 10px 25px;
        text-shadow: 1px 1px 2px #000;
        text-transform: uppercase;
    }
    .submit-btn-furture {
        background: rgba(0, 0, 0, 0) linear-gradient(to bottom, #71d1ff 0%, #22b3f4 4%, #198fef 100%) repeat scroll 0 0;
        border: medium none;
        border-radius: 6px;
        box-shadow: 0 1px 3px rgba(0, 0, 0, 0.4);
        color: #fff;
        font-family: "FuturaStd-Book";
        font-size: 16px;
        margin: 0 60px 30px 40px;
        padding: 10px 25px;
        text-shadow: 1px 1px 2px #000;
        text-transform: uppercase;
        bottom: 3px;
        height: 38px;
        position: relative;
        height: 35px;
        line-height: 13px;
        right: 51px;
        top: 0;
    }
    .form-group.furture-form-style {
        margin: 15px 0 0;
        text-align: center;
    }
    .amound-contet{
        display: block;
        margin: 0 0 20px;
    }
    .form-style {
        border: 1px solid #ddd;
        border-radius: 3px;
        height: 35px;
        padding: 8px;
        width: 93%;
    }
    .verify-page .col-md-3 > label {
        position: relative;
        top: 8px;
    }
</style>
<div id="result_container">
    <div class="page_container">
        <?php
        if(isset($_SESSION['error']))
        {
            ?>
            <div class="alert alert-danger">
                <?php
                echo $_SESSION['error'];
                unset($_SESSION['error']);
                ?>
            </div>
        <?php
        }
        ?>
        <div class="width-row" style="background-color: rgba(255, 255, 255, 0.74) none repeat scroll 0 0;">
            <form action="<?php echo base_url("user/myaccount/verification"); ?>" id="verification-form" method="post">
                <div class="main_cont payment-sec verify-page">
                    <h1>VERIFICATION NEEDED</h1>
                    <div class="clearfix"></div>
                    <div class="content-holder-payment">
                        <p>In order to fight credit and debit card fraud, we have refunded a random amount back to your credit card. Please login to your credit card account and check your statement. You will see a refund from <strong> WholsaleHtLs8882872307.</strong> In case you used a debit card, please login to your online bank account and check the refund there. </p>
                        <p>Please enter the amount here in order to verify that you are the authorized cardholder. </p>
                        <div class="col-md-3">
                            <label>My refund amount was</label>
                        </div>
                        <div class="col-md-4">
                            <span>$</span> <input type="text" id="priceper" required class="form-style" name="amount" />
                        </div>
                        <div class="col-md-5">
                            <button type="submit" class="submit-btn-furture" onclick="submitform()">Submit <i class="fa fa-refresh fa-lg fa-spin loadin_ajax" style="color: #ffffff;display:none;"></i></button>
                        </div>
                        <div class="clearfix"></div>
                        <p><ins>NOTE: </ins>You will have <strong> 3 attempts </strong>to verify this amount. After the third unsuccessful attempt, your account will be closed and a full refund will be initiated. Upon successful verification, your account will be activated <strong> immediately.</strong>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $("#priceper").maskMoney({prefix: '', allowNegative: true, thousands: ',', decimal: '.', affixesStay: false});
    });
</script>