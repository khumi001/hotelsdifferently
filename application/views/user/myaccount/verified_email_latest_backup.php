<style type="text/css">
    .color-red{
        color:#FFF;
        font-weight:bold;
    }
    .bold-font{
        font-weight: bold;
    }
    .verif-txt{
        margin: 17px 0;
    }
    .member-msg{
        margin: 17px 0;
    }
</style>
<div class="main" style="background:url('http://hotels.softech.website/public/affilliate_theme/img/home-bg.jpg');background-repeat:no-repeat;background-size:100% 100%;width:547px;margin:auto">
    <div class="m_3275774443844637159containt-main" style="background:rgba(17,125,200,0.61);color:#fff;width:100%">
        <div class="m_first">
            <div class="left" style="margin:auto;text-align:center;float:none">
                <a class="logo" href="https://www.hotelsdifferently.com/" style="display:inline-block;margin:10px 0 0 10px" target="_blank">
                    <img src="http://hotels.softech.website/public/affilliate_theme/img/logo.png" style="width:130px" class="CToWUd">
                </a>
            </div>
            <div class="m_clear" style="clear:both"></div>
        </div>
        <div class="top-containt">
            <h1 style="max-width:350px;margin:20px auto"><img style="display:block;width:100%;height:auto" src="http://hotels.softech.website/public/affilliate_theme/img/txt-email.png" alt="" class="CToWUd"></h1>
        </div>
        <div class="top-hotel" style="width:90%;margin:40px auto auto auto;background:#0c5fa7;color:#ffffff">
            <div style="padding-bottom:10px;padding-top:10px;width:90%;margin:auto;font-family:'Open Sans',sans-serif;color:#fff!important;font-size:12px;line-height:16px">
                <p style="color:#fff;margin-bottom:10px">Dear
                    <strong style="color:#fff"><?php echo $user['var_fname']." ".$user['var_lname']?></strong>,
                </p>
                <p>
                    Thank you very much for your payment for your annual membership!
                    <span style="display: block;">Your payment of <span class="bold-font">$<?php echo $amount;?></span> for your membership was successfully charged and your charges will appear as <span class="color-red">"WholsaleHtls8882872307"</span> and a receipt has been sent to you as well.</span>
                </p>
                <p>
                    <span style="text-decoration:; font-weight:bold;display: block;">What happens next? </span> Since you used Apple Pay / Android Pay to pay for your membership, no further verification is necessary. You are ready to use your account instantly!
                </p>

                <p  class="member-msg"><b>Would you like to get </b><span class="color-red"> 1 EXTRA MONTH  MEMBERSHIP FOR FREE? </span> Go to MY ACCOUNT – REFER A FRIEND and refer your friends. Every friend you refer to sign up, pay <ins>and</ins> successfully enroll on our site, you get 1 MONTH MEMBERSHIP EXTENSION FREE. Furthermore, you will be sending your friends a discounted link for a membership. Be good to them and be rewarded for it!</p>
                <!-- <p  class="member-msg"><span class="bold-font">Life without limits!</span> Interested in our
                <span class="bold-font">Lifetime Plan? </span> Reach out to our customer service to inquire about pricing!</p> -->
                <p  class="member-msg">We are very much looking forward to a mutually beneficial relationship with you and wish you lots of savings on your bookings!</p>
                <p style="color:#fff;margin-bottom:10px">
                    Thank you for choosing <b>WHotelsGroup.com</b> Where better deals are made for YOU!
                </p>
                <strong><a href="" style="color: #fff;">PS: This inbox is NOT monitored, please do NOT send us any emails here.</a></strong>
            </div>
        </div>
        <div style="font-size:11px;color:#fff;padding:5px;font-family:'Open Sans',sans-serif;display:block" class="adL">&nbsp;</div>
        <div class="adL"></div>
    </div>
    <div class="adL"></div>
</div>