<style>
    .main_cont {
        background: rgba(255, 255, 255, 0.8) none repeat scroll 0 0;
        display: block;
        overflow: hidden;
    }
    .closeok {
        color: #000;
        float: center;
        font-size: 21px;
        font-weight: 700;
        line-height: 1;
        opacity: 0.2;
        text-shadow: 0 1px 0 #fff;
    }
    .full-name-ac{
        display: block;
        margin-top: 8px;
    }
    .pading-form {
        margin: 0 0 15px;
    }
    .margin-bottom-0 {
        margin-top: -15px;
        margin-bottom: 0;
    }
    .form-body.phonefieldholder > .text-sec-col {
        display: block;
        text-align: center;
        margin-bottom: 5px;
    }
    .membr-life {
      position: relative;
      right: 41px;
    }
    .switch > .switch-label {
      background: #b5c1c7 none repeat scroll 0 0;
      border: 2px solid #b5c1c7;
      bottom: 22px;
      color: #fff;
      cursor: pointer;
      display: inline-block;
      font-family: Helvetica,Arial,sans-serif;
      font-size: 10px;
      font-weight: bold;
      height: 25px;
      line-height: 20px;
      position: relative;
      text-align: center;
      text-transform: uppercase;
      transition: all 0.3s ease-out 0s;
      width: 58px;
    }

</style>
<div class="width-row margin-top-20">
<?php

$phpne=  explode('-', $edit_data['var_phone']);
//$exp = explode('-',$phpne[0]);
//print_r($phpne); exit();
?>
<div class="main_cont">
<div class="pagetitle margin-bottom-10">
    <h1><?php echo $this->page_name; ?></h1>
</div>
<div class="row">
    
<form class="form-horizontal form-bordered" method="post" id="edit_info">
<div class="col-md-offset-3 col-md-6">
    <div class="form">
        <div class="form-body">
            <div class="form-group margin-bottom-15">
                <label class="control-label col-md-4">Account Number:</label>
                <div  class="col-md-8">
                    <div style="padding: 7px 11px 0;">
                        <?php echo $edit_data['var_accountid']; ?>
                    </div>
                </div>
            </div>
            <!--
                        <div class="form-group margin-bottom-15">
                            <label class="control-label col-md-4">User name:</label>
                            <div  class="col-md-8">
                                <div style="padding: 7px 13px 0;">
                                    <?php //echo $edit_data['var_username']; ?>
                                </div>
                            </div>
                        </div>
						-->
            <!-- <div class="form-group margin-bottom-15">
                            <label class="control-label col-md-4">First name:</label>
                            <div  class="col-md-8">
                                <input type="text" maxlength="20" name="fname" class="form-control fname" value="<?php echo $edit_data['var_fname']; ?>">
                            </div>
                        </div>
                        <div class="form-group ">
                            <label class="control-label col-md-4">Last name:</label>
                            <div  class="col-md-8">
                                <input type="text" maxlength="20" name="lname" class="form-control fname" value="<?php echo $edit_data['var_lname']; ?>">
                            </div>
                        </div> -->
            <div class="form-group pading-form">
                <label class="control-label col-md-4">Full Name:</label>
                <div  class="col-md-8">
                    <strong class="full-name-ac"><?php echo $edit_data['var_fname']." ".$edit_data['var_lname']; ?></strong>
                </div>
            </div>
            <?php if($this->session->userdata['valid_user']['var_usertype'] =='U'){?>
            <div class="form-group pading-form">
                <label class="control-label col-md-4">Auto-renewal:</label>
                <div  class="col-md-8">
                        <label class="switch switch-primary switch-round">
                            <input id="autoRenewal" name="autoRenewal" value="1" type="checkbox" <?php if($edit_data['autoRenewal']) echo 'checked';?>>
                            <span class="switch-label" data-on="YES" data-off="NO"></span>
                        </label>
                </div>
            </div>
            <?php }?>
            <div class="form-group pading-form">
                <label class="control-label col-md-4">Member since:</label>
                <div  class="col-md-8">
                    <div style="padding: 7px 0px 0;">
                        <?php echo date(DISPLAY_DATE_FORMAT, strtotime($edit_data['entity_activate_time'])); ?>
                    </div>
                </div>
            </div>
            <div class="form-group pading-form">
                <p class="control-label col-md-5" style="padding: 0; position: relative;right: 56px;font-weight:bold;">Membership expiration:</p>
                <div  class="col-md-7">
                    <div class="membr-life" >
                        <strong><?php
                            if(isset($edit_data['endDate'])) {
                                if($edit_data['isLifeTime'])
                                    echo "LIFETIME";
                                else
                                    echo date(DISPLAY_DATE_FORMAT, strtotime($edit_data['endDate']));
                            }
                            else
                                echo "LIFETIME"; ?>
                        </strong>
                    </div>
                </div>
            </div>
            <div class="form-group phonefield pading-form">
                <label class="control-label col-md-4">Phone number: <a class="tooltips" data-placement="top" data-original-title="Optional field"><sup><i class="fa fa-info-circle"></i></sup></a></label>
                <div  class="col-md-2">
                    <input type="text" name="phone1" value="<?php echo $phpne[0]; ?>" maxlength="4" class="form-control" style="width:60px">
                </div>
                <div  class="col-md-3" style="padding-left: 0px;">
                    <input type="text" name="phone2" class="form-control" maxlength="12" value="<?php echo $phpne[1]; ?>" style="width:131px">
                </div>
                <div  class="col-md-3" style="">
                    <input type="text" name="phone3" class="form-control" maxlength="6" value="<?php echo $phpne[2]; ?>">
                </div>
            </div>

            <div class="form-group phonelabel pading-form">
                <label class="control-label col-md-4"></label>
                <div  class="col-md-2" style="text-align: center;font-size: 12px;">
                    <span>Country</span>
                </div>
                <div  class="col-md-3" style="text-align: center;font-size: 12px;">
                    <span>Number</span>
                </div>
                <div  class="col-md-3" style="text-align: center;font-size: 12px;">
                    <span>Extension</span>
                </div>
            </div>
        </div>
    </div>
</div>
<!--<div class="col-md-6">
                <div class="form">
                    <div class="form-body phonefieldholder">
                        <strong class="text-sec-col">Billing Information:</strong>
                        <div class="form-group margin-bottom-15  ">
                            <label class="control-label col-md-4">Street:</label>
                            <div  class="col-md-8">
                                <div style="padding: 7px 11px 0;">
                                   <input type="text" name="" class="form-control"  />
                                </div>
                            </div>
                        </div>
                        <div class="form-group margin-bottom-15 margin-bottom-0 ">
                            <label class="control-label col-md-4">City:</label>
                            <div  class="col-md-8">
                                <div style="padding: 7px 11px 0;">
                                   <input type="text" name="" / class="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group margin-bottom-15 margin-bottom-0 ">
                            <label class="control-label col-md-4">Zip:</label>
                            <div  class="col-md-8">
                                <div style="padding: 7px 11px 0;">
                                   <input type="text" name="" / class="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group margin-bottom-15 margin-bottom-0 ">
                            <label class="control-label col-md-4">State:</label>
                            <div  class="col-md-8">
                                <div style="padding: 7px 11px 0;">
                                   <select class="select2me form-control input-xlarge">
                                       <option>Alabama</option>
                                       <option>New York</option>
                                       <option>Las Vegas</option>
                                   </select>
                                </div>
                            </div>
                        </div>
                         </div>
                        <div class="form-group margin-bottom-15 margin-bottom-0 ">
                            <label class="control-label col-md-4">Country:</label>
                            <div  class="col-md-8">
                                <div style="padding: 7px 11px 0;">
                                    <select class="select2me form-control input-xlarge" name="country">
                                        <option value="">UNITED STATES</option>
                                        <option value="AF" <?php /*if($edit_data['var_country'] == 'AF'){ echo 'selected="selected"'; } */?>>Afghanistan</option>
                                        <option value="AL" <?php /*if($edit_data['var_country'] == 'AL'){ echo 'selected="selected"'; } */?>>Albania</option>
                                        <option value="DZ" <?php /*if($edit_data['var_country'] == 'DZ'){ echo 'selected="selected"'; } */?>>Algeria</option>
                                        <option value="AS" <?php /*if($edit_data['var_country'] == 'AS'){ echo 'selected="selected"'; } */?>>American Samoa</option>
                                        <option value="AD" <?php /*if($edit_data['var_country'] == 'AD'){ echo 'selected="selected"'; } */?>>Andorra</option>
                                        <option value="AO" <?php /*if($edit_data['var_country'] == 'AO'){ echo 'selected="selected"'; } */?>>Angola</option>
                                        <option value="AI" <?php /*if($edit_data['var_country'] == 'AI'){ echo 'selected="selected"'; } */?>>Anguilla</option>
                                        <option value="AQ" <?php /*if($edit_data['var_country'] == 'AQ'){ echo 'selected="selected"'; } */?>>Antarctica</option>
                                        <option value="AR" <?php /*if($edit_data['var_country'] == 'AR'){ echo 'selected="selected"'; } */?>>Argentina</option>
                                        <option value="AM" <?php /*if($edit_data['var_country'] == 'AM'){ echo 'selected="selected"'; } */?>>Armenia</option>
                                        <option value="AW" <?php /*if($edit_data['var_country'] == 'AW'){ echo 'selected="selected"'; } */?>>Aruba</option>
                                        <option value="AU" <?php /*if($edit_data['var_country'] == 'AU'){ echo 'selected="selected"'; } */?>>Australia</option>
                                        <option value="AT" <?php /*if($edit_data['var_country'] == 'AT'){ echo 'selected="selected"'; } */?>>Austria</option>
                                        <option value="AZ" <?php /*if($edit_data['var_country'] == 'AZ'){ echo 'selected="selected"'; } */?>>Azerbaijan</option>
                                        <option value="BS" <?php /*if($edit_data['var_country'] == 'BS'){ echo 'selected="selected"'; } */?>>Bahamas</option>
                                        <option value="BH" <?php /*if($edit_data['var_country'] == 'BH'){ echo 'selected="selected"'; } */?>>Bahrain</option>
                                        <option value="BD" <?php /*if($edit_data['var_country'] == 'BD'){ echo 'selected="selected"'; } */?>>Bangladesh</option>
                                        <option value="BB" <?php /*if($edit_data['var_country'] == 'BB'){ echo 'selected="selected"'; } */?>>Barbados</option>
                                        <option value="BY" <?php /*if($edit_data['var_country'] == 'BY'){ echo 'selected="selected"'; } */?>>Belarus</option>
                                        <option value="BE" <?php /*if($edit_data['var_country'] == 'BE'){ echo 'selected="selected"'; } */?>>Belgium</option>
                                        <option value="BZ" <?php /*if($edit_data['var_country'] == 'BZ'){ echo 'selected="selected"'; } */?>>Belize</option>
                                        <option value="BJ" <?php /*if($edit_data['var_country'] == 'BJ'){ echo 'selected="selected"'; } */?>>Benin</option>
                                        <option value="BM" <?php /*if($edit_data['var_country'] == 'BM'){ echo 'selected="selected"'; } */?>>Bermuda</option>
                                        <option value="BT" <?php /*if($edit_data['var_country'] == 'BT'){ echo 'selected="selected"'; } */?>>Bhutan</option>
                                        <option value="BO" <?php /*if($edit_data['var_country'] == 'BO'){ echo 'selected="selected"'; } */?>>Bolivia</option>
                                        <option value="BA" <?php /*if($edit_data['var_country'] == 'BA'){ echo 'selected="selected"'; } */?>>Bosnia and Herzegowina</option>
                                        <option value="BW" <?php /*if($edit_data['var_country'] == 'BW'){ echo 'selected="selected"'; } */?>>Botswana</option>
                                        <option value="BV" <?php /*if($edit_data['var_country'] == 'BV'){ echo 'selected="selected"'; } */?>>Bouvet Island</option>
                                        <option value="BR" <?php /*if($edit_data['var_country'] == 'BR'){ echo 'selected="selected"'; } */?>>Brazil</option>
                                        <option value="IO" <?php /*if($edit_data['var_country'] == 'TO'){ echo 'selected="selected"'; } */?>>British Indian Ocean Territory</option>
                                        <option value="BN" <?php /*if($edit_data['var_country'] == 'BN'){ echo 'selected="selected"'; } */?>>Brunei Darussalam</option>
                                        <option value="BG" <?php /*if($edit_data['var_country'] == 'BG'){ echo 'selected="selected"'; } */?>>Bulgaria</option>
                                        <option value="BF" <?php /*if($edit_data['var_country'] == 'BF'){ echo 'selected="selected"'; } */?>>Burkina Faso</option>
                                        <option value="BI" <?php /*if($edit_data['var_country'] == 'BI'){ echo 'selected="selected"'; } */?>>Burundi</option>
                                        <option value="KH" <?php /*if($edit_data['var_country'] == 'KH'){ echo 'selected="selected"'; } */?>>Cambodia</option>
                                        <option value="CM" <?php /*if($edit_data['var_country'] == 'CM'){ echo 'selected="selected"'; } */?>>Cameroon</option>
                                        <option value="CA" <?php /*if($edit_data['var_country'] == 'CA'){ echo 'selected="selected"'; } */?>>Canada</option>
                                        <option value="CV" <?php /*if($edit_data['var_country'] == 'CV'){ echo 'selected="selected"'; } */?>>Cape Verde</option>
                                        <option value="KY" <?php /*if($edit_data['var_country'] == 'KY'){ echo 'selected="selected"'; } */?>>Cayman Islands</option>
                                        <option value="CF" <?php /*if($edit_data['var_country'] == 'CF'){ echo 'selected="selected"'; } */?>>Central African Republic</option>
                                        <option value="TD" <?php /*if($edit_data['var_country'] == 'TD'){ echo 'selected="selected"'; } */?>>Chad</option>
                                        <option value="CL" <?php /*if($edit_data['var_country'] == 'CL'){ echo 'selected="selected"'; } */?>>Chile</option>
                                        <option value="CN" <?php /*if($edit_data['var_country'] == 'CN'){ echo 'selected="selected"'; } */?>>China</option>
                                        <option value="CX" <?php /*if($edit_data['var_country'] == 'CX'){ echo 'selected="selected"'; } */?>>Christmas Island</option>
                                        <option value="CC" <?php /*if($edit_data['var_country'] == 'CC'){ echo 'selected="selected"'; } */?>>Cocos (Keeling) Islands</option>
                                        <option value="CO" <?php /*if($edit_data['var_country'] == 'CO'){ echo 'selected="selected"'; } */?>>Colombia</option>
                                        <option value="KM" <?php /*if($edit_data['var_country'] == 'KM'){ echo 'selected="selected"'; } */?>>Comoros</option>
                                        <option value="CG" <?php /*if($edit_data['var_country'] == 'CG'){ echo 'selected="selected"'; } */?>>Congo</option>
                                        <option value="CD" <?php /*if($edit_data['var_country'] == 'CD'){ echo 'selected="selected"'; } */?>>Congo, the Democratic Republic of the</option>
                                        <option value="CK" <?php /*if($edit_data['var_country'] == 'CK'){ echo 'selected="selected"'; } */?>>Cook Islands</option>
                                        <option value="CR" <?php /*if($edit_data['var_country'] == 'CR'){ echo 'selected="selected"'; } */?>>Costa Rica</option>
                                        <option value="CI" <?php /*if($edit_data['var_country'] == 'CI'){ echo 'selected="selected"'; } */?>>Cote d'Ivoire</option>
                                        <option value="HR" <?php /*if($edit_data['var_country'] == 'HR'){ echo 'selected="selected"'; } */?>>Croatia (Hrvatska)</option>
                                        <option value="CU" <?php /*if($edit_data['var_country'] == 'CU'){ echo 'selected="selected"'; } */?>>Cuba</option>
                                        <option value="CY" <?php /*if($edit_data['var_country'] == 'CY'){ echo 'selected="selected"'; } */?>>Cyprus</option>
                                        <option value="CZ" <?php /*if($edit_data['var_country'] == 'CZ'){ echo 'selected="selected"'; } */?>>Czech Republic</option>
                                        <option value="DK" <?php /*if($edit_data['var_country'] == 'DK'){ echo 'selected="selected"'; } */?>>Denmark</option>
                                        <option value="DJ" <?php /*if($edit_data['var_country'] == 'DJ'){ echo 'selected="selected"'; } */?>>Djibouti</option>
                                        <option value="DM" <?php /*if($edit_data['var_country'] == 'DM'){ echo 'selected="selected"'; } */?>>Dominica</option>
                                        <option value="DO" <?php /*if($edit_data['var_country'] == 'DO'){ echo 'selected="selected"'; } */?>>Dominican Republic</option>
                                        <option value="EC" <?php /*if($edit_data['var_country'] == 'EC'){ echo 'selected="selected"'; } */?>>Ecuador</option>
                                        <option value="EG" <?php /*if($edit_data['var_country'] == 'EG'){ echo 'selected="selected"'; } */?>>Egypt</option>
                                        <option value="SV" <?php /*if($edit_data['var_country'] == 'SV'){ echo 'selected="selected"'; } */?>>El Salvador</option>
                                        <option value="GQ" <?php /*if($edit_data['var_country'] == 'GQ'){ echo 'selected="selected"'; } */?>>Equatorial Guinea</option>
                                        <option value="ER" <?php /*if($edit_data['var_country'] == 'ER'){ echo 'selected="selected"'; } */?>>Eritrea</option>
                                        <option value="EE" <?php /*if($edit_data['var_country'] == 'EE'){ echo 'selected="selected"'; } */?>>Estonia</option>
                                        <option value="ET" <?php /*if($edit_data['var_country'] == 'ET'){ echo 'selected="selected"'; } */?>>Ethiopia</option>
                                        <option value="FK" <?php /*if($edit_data['var_country'] == 'FK'){ echo 'selected="selected"'; } */?>>Falkland Islands (Malvinas)</option>
                                        <option value="FO" <?php /*if($edit_data['var_country'] == 'FO'){ echo 'selected="selected"'; } */?>>Faroe Islands</option>
                                        <option value="FJ" <?php /*if($edit_data['var_country'] == 'FJ'){ echo 'selected="selected"'; } */?>>Fiji</option>
                                        <option value="FI" <?php /*if($edit_data['var_country'] == 'FI'){ echo 'selected="selected"'; } */?>>Finland</option>
                                        <option value="FR" <?php /*if($edit_data['var_country'] == 'FR'){ echo 'selected="selected"'; } */?>>France</option>
                                        <option value="GF" <?php /*if($edit_data['var_country'] == 'GF'){ echo 'selected="selected"'; } */?>>French Guiana</option>
                                        <option value="PF" <?php /*if($edit_data['var_country'] == 'PF'){ echo 'selected="selected"'; } */?>>French Polynesia</option>
                                        <option value="TF" <?php /*if($edit_data['var_country'] == 'TF'){ echo 'selected="selected"'; } */?>>French Southern Territories</option>
                                        <option value="GA" <?php /*if($edit_data['var_country'] == 'GA'){ echo 'selected="selected"'; } */?>>Gabon</option>
                                        <option value="GM" <?php /*if($edit_data['var_country'] == 'GM'){ echo 'selected="selected"'; } */?>>Gambia</option>
                                        <option value="GE" <?php /*if($edit_data['var_country'] == 'GE'){ echo 'selected="selected"'; } */?>>Georgia</option>
                                        <option value="DE" <?php /*if($edit_data['var_country'] == 'DE'){ echo 'selected="selected"'; } */?>>Germany</option>
                                        <option value="GH" <?php /*if($edit_data['var_country'] == 'GH'){ echo 'selected="selected"'; } */?>>Ghana</option>
                                        <option value="GI" <?php /*if($edit_data['var_country'] == 'GI'){ echo 'selected="selected"'; } */?>>Gibraltar</option>
                                        <option value="GR" <?php /*if($edit_data['var_country'] == 'GR'){ echo 'selected="selected"'; } */?>>Greece</option>
                                        <option value="GL" <?php /*if($edit_data['var_country'] == 'GL'){ echo 'selected="selected"'; } */?>>Greenland</option>
                                        <option value="GD" <?php /*if($edit_data['var_country'] == 'GD'){ echo 'selected="selected"'; } */?>>Grenada</option>
                                        <option value="GP" <?php /*if($edit_data['var_country'] == 'GP'){ echo 'selected="selected"'; } */?>>Guadeloupe</option>
                                        <option value="GU" <?php /*if($edit_data['var_country'] == 'GU'){ echo 'selected="selected"'; } */?>>Guam</option>
                                        <option value="GT" <?php /*if($edit_data['var_country'] == 'GT'){ echo 'selected="selected"'; } */?>>Guatemala</option>
                                        <option value="GN" <?php /*if($edit_data['var_country'] == 'GN'){ echo 'selected="selected"'; } */?>>Guinea</option>
                                        <option value="GW" <?php /*if($edit_data['var_country'] == 'GW'){ echo 'selected="selected"'; } */?>>Guinea-Bissau</option>
                                        <option value="GY" <?php /*if($edit_data['var_country'] == 'GY'){ echo 'selected="selected"'; } */?>>Guyana</option>
                                        <option value="HT" <?php /*if($edit_data['var_country'] == 'HT'){ echo 'selected="selected"'; } */?>>Haiti</option>
                                        <option value="HM" <?php /*if($edit_data['var_country'] == 'HM'){ echo 'selected="selected"'; } */?>>Heard and Mc Donald Islands</option>
                                        <option value="VA" <?php /*if($edit_data['var_country'] == 'VA'){ echo 'selected="selected"'; } */?>>Holy See (Vatican City State)</option>
                                        <option value="HN" <?php /*if($edit_data['var_country'] == 'HN'){ echo 'selected="selected"'; } */?>>Honduras</option>
                                        <option value="HK" <?php /*if($edit_data['var_country'] == 'HK'){ echo 'selected="selected"'; } */?>>Hong Kong</option>
                                        <option value="HU" <?php /*if($edit_data['var_country'] == 'HU'){ echo 'selected="selected"'; } */?>>Hungary</option>
                                        <option value="IS" <?php /*if($edit_data['var_country'] == 'IS'){ echo 'selected="selected"'; } */?>>Iceland</option>
                                        <option value="IN" <?php /*if($edit_data['var_country'] == 'IN'){ echo 'selected="selected"'; } */?>>India</option>
                                        <option value="ID" <?php /*if($edit_data['var_country'] == 'ID'){ echo 'selected="selected"'; } */?>>Indonesia</option>
                                        <option value="IR" <?php /*if($edit_data['var_country'] == 'IR'){ echo 'selected="selected"'; } */?>>Iran (Islamic Republic of)</option>
                                        <option value="IQ" <?php /*if($edit_data['var_country'] == 'IQ'){ echo 'selected="selected"'; } */?>>Iraq</option>
                                        <option value="IE" <?php /*if($edit_data['var_country'] == 'IE'){ echo 'selected="selected"'; } */?>>Ireland</option>
                                        <option value="IL" <?php /*if($edit_data['var_country'] == 'IL'){ echo 'selected="selected"'; } */?>>Israel</option>
                                        <option value="IT" <?php /*if($edit_data['var_country'] == 'IT'){ echo 'selected="selected"'; } */?>>Italy</option>
                                        <option value="JM" <?php /*if($edit_data['var_country'] == 'JM'){ echo 'selected="selected"'; } */?>>Jamaica</option>
                                        <option value="JP" <?php /*if($edit_data['var_country'] == 'JP'){ echo 'selected="selected"'; } */?>>Japan</option>
                                        <option value="JO" <?php /*if($edit_data['var_country'] == 'JO'){ echo 'selected="selected"'; } */?>>Jordan</option>
                                        <option value="KZ" <?php /*if($edit_data['var_country'] == 'KZ'){ echo 'selected="selected"'; } */?>>Kazakhstan</option>
                                        <option value="KE" <?php /*if($edit_data['var_country'] == 'KE'){ echo 'selected="selected"'; } */?>>Kenya</option>
                                        <option value="KI" <?php /*if($edit_data['var_country'] == 'KI'){ echo 'selected="selected"'; } */?>>Kiribati</option>
                                        <option value="KP" <?php /*if($edit_data['var_country'] == 'KP'){ echo 'selected="selected"'; } */?>>Korea, Democratic People's Republic of</option>
                                        <option value="KR" <?php /*if($edit_data['var_country'] == 'KR'){ echo 'selected="selected"'; } */?>>Korea, Republic of</option>
                                        <option value="KW" <?php /*if($edit_data['var_country'] == 'KW'){ echo 'selected="selected"'; } */?>>Kuwait</option>
                                        <option value="KG" <?php /*if($edit_data['var_country'] == 'KG'){ echo 'selected="selected"'; } */?>>Kyrgyzstan</option>
                                        <option value="LA" <?php /*if($edit_data['var_country'] == 'LA'){ echo 'selected="selected"'; } */?>>Lao People's Democratic Republic</option>
                                        <option value="LV" <?php /*if($edit_data['var_country'] == 'LV'){ echo 'selected="selected"'; } */?>>Latvia</option>
                                        <option value="LB" <?php /*if($edit_data['var_country'] == 'LB'){ echo 'selected="selected"'; } */?>>Lebanon</option>
                                        <option value="LS" <?php /*if($edit_data['var_country'] == 'LS'){ echo 'selected="selected"'; } */?>>Lesotho</option>
                                        <option value="LR" <?php /*if($edit_data['var_country'] == 'LR'){ echo 'selected="selected"'; } */?>>Liberia</option>
                                        <option value="LY" <?php /*if($edit_data['var_country'] == 'LY'){ echo 'selected="selected"'; } */?>>Libyan Arab Jamahiriya</option>
                                        <option value="LI" <?php /*if($edit_data['var_country'] == 'LI'){ echo 'selected="selected"'; } */?>>Liechtenstein</option>
                                        <option value="LT" <?php /*if($edit_data['var_country'] == 'LT'){ echo 'selected="selected"'; } */?>>Lithuania</option>
                                        <option value="LU" <?php /*if($edit_data['var_country'] == 'LU'){ echo 'selected="selected"'; } */?>>Luxembourg</option>
                                        <option value="MO" <?php /*if($edit_data['var_country'] == 'MO'){ echo 'selected="selected"'; } */?>>Macau</option>
                                        <option value="MK" <?php /*if($edit_data['var_country'] == 'MK'){ echo 'selected="selected"'; } */?>>Macedonia, The Former Yugoslav Republic of</option>
                                        <option value="MG" <?php /*if($edit_data['var_country'] == 'MG'){ echo 'selected="selected"'; } */?>>Madagascar</option>
                                        <option value="MW" <?php /*if($edit_data['var_country'] == 'MW'){ echo 'selected="selected"'; } */?>>Malawi</option>
                                        <option value="MY" <?php /*if($edit_data['var_country'] == 'MY'){ echo 'selected="selected"'; } */?>>Malaysia</option>
                                        <option value="MV" <?php /*if($edit_data['var_country'] == 'MV'){ echo 'selected="selected"'; } */?>>Maldives</option>
                                        <option value="ML" <?php /*if($edit_data['var_country'] == 'ML'){ echo 'selected="selected"'; } */?>>Mali</option>
                                        <option value="MT" <?php /*if($edit_data['var_country'] == 'MT'){ echo 'selected="selected"'; } */?>>Malta</option>
                                        <option value="MH" <?php /*if($edit_data['var_country'] == 'MH'){ echo 'selected="selected"'; } */?>>Marshall Islands</option>
                                        <option value="MQ" <?php /*if($edit_data['var_country'] == 'MQ'){ echo 'selected="selected"'; } */?>>Martinique</option>
                                        <option value="MR" <?php /*if($edit_data['var_country'] == 'MR'){ echo 'selected="selected"'; } */?>>Mauritania</option>
                                        <option value="MU" <?php /*if($edit_data['var_country'] == 'MU'){ echo 'selected="selected"'; } */?>>Mauritius</option>
                                        <option value="YT" <?php /*if($edit_data['var_country'] == 'YT'){ echo 'selected="selected"'; } */?>>Mayotte</option>
                                        <option value="MX" <?php /*if($edit_data['var_country'] == 'MX'){ echo 'selected="selected"'; } */?>>Mexico</option>
                                        <option value="FM" <?php /*if($edit_data['var_country'] == 'FM'){ echo 'selected="selected"'; } */?>>Micronesia, Federated States of</option>
                                        <option value="MD" <?php /*if($edit_data['var_country'] == 'MD'){ echo 'selected="selected"'; } */?>>Moldova, Republic of</option>
                                        <option value="MC" <?php /*if($edit_data['var_country'] == 'MC'){ echo 'selected="selected"'; } */?>>Monaco</option>
                                        <option value="MN" <?php /*if($edit_data['var_country'] == 'MN'){ echo 'selected="selected"'; } */?>>Mongolia</option>
                                        <option value="MS" <?php /*if($edit_data['var_country'] == 'MS'){ echo 'selected="selected"'; } */?>>Montserrat</option>
                                        <option value="MA" <?php /*if($edit_data['var_country'] == 'MA'){ echo 'selected="selected"'; } */?>>Morocco</option>
                                        <option value="MZ" <?php /*if($edit_data['var_country'] == 'MZ'){ echo 'selected="selected"'; } */?>>Mozambique</option>
                                        <option value="MM" <?php /*if($edit_data['var_country'] == 'MM'){ echo 'selected="selected"'; } */?>>Myanmar</option>
                                        <option value="NA" <?php /*if($edit_data['var_country'] == 'NA'){ echo 'selected="selected"'; } */?>>Namibia</option>
                                        <option value="NR" <?php /*if($edit_data['var_country'] == 'NR'){ echo 'selected="selected"'; } */?>>Nauru</option>
                                        <option value="NP" <?php /*if($edit_data['var_country'] == 'NP'){ echo 'selected="selected"'; } */?>>Nepal</option>
                                        <option value="NL" <?php /*if($edit_data['var_country'] == 'NL'){ echo 'selected="selected"'; } */?>>Netherlands</option>
                                        <option value="AN" <?php /*if($edit_data['var_country'] == 'AN'){ echo 'selected="selected"'; } */?>>Netherlands Antilles</option>
                                        <option value="NC" <?php /*if($edit_data['var_country'] == 'NC'){ echo 'selected="selected"'; } */?>>New Caledonia</option>
                                        <option value="NZ" <?php /*if($edit_data['var_country'] == 'NZ'){ echo 'selected="selected"'; } */?>>New Zealand</option>
                                        <option value="NI" <?php /*if($edit_data['var_country'] == 'NI'){ echo 'selected="selected"'; } */?>>Nicaragua</option>
                                        <option value="NE" <?php /*if($edit_data['var_country'] == 'NE'){ echo 'selected="selected"'; } */?>>Niger</option>
                                        <option value="NG" <?php /*if($edit_data['var_country'] == 'NG'){ echo 'selected="selected"'; } */?>>Nigeria</option>
                                        <option value="NU" <?php /*if($edit_data['var_country'] == 'NU'){ echo 'selected="selected"'; } */?>>Niue</option>
                                        <option value="NF" <?php /*if($edit_data['var_country'] == 'NF'){ echo 'selected="selected"'; } */?>>Norfolk Island</option>
                                        <option value="MP" <?php /*if($edit_data['var_country'] == 'MP'){ echo 'selected="selected"'; } */?>>Northern Mariana Islands</option>
                                        <option value="NO" <?php /*if($edit_data['var_country'] == 'NO'){ echo 'selected="selected"'; } */?>>Norway</option>
                                        <option value="OM" <?php /*if($edit_data['var_country'] == 'OM'){ echo 'selected="selected"'; } */?>>Oman</option>
                                        <option value="PK" <?php /*if($edit_data['var_country'] == 'PK'){ echo 'selected="selected"'; } */?>>Pakistan</option>
                                        <option value="PW" <?php /*if($edit_data['var_country'] == 'PW'){ echo 'selected="selected"'; } */?>>Palau</option>
                                        <option value="PA" <?php /*if($edit_data['var_country'] == 'PA'){ echo 'selected="selected"'; } */?>>Panama</option>
                                        <option value="PG" <?php /*if($edit_data['var_country'] == 'PG'){ echo 'selected="selected"'; } */?>>Papua New Guinea</option>
                                        <option value="PY" <?php /*if($edit_data['var_country'] == 'PY'){ echo 'selected="selected"'; } */?>>Paraguay</option>
                                        <option value="PE" <?php /*if($edit_data['var_country'] == 'PE'){ echo 'selected="selected"'; } */?>>Peru</option>
                                        <option value="PH" <?php /*if($edit_data['var_country'] == 'PH'){ echo 'selected="selected"'; } */?>>Philippines</option>
                                        <option value="PN" <?php /*if($edit_data['var_country'] == 'PN'){ echo 'selected="selected"'; } */?>>Pitcairn</option>
                                        <option value="PL" <?php /*if($edit_data['var_country'] == 'PL'){ echo 'selected="selected"'; } */?>>Poland</option>
                                        <option value="PT" <?php /*if($edit_data['var_country'] == 'PT'){ echo 'selected="selected"'; } */?>>Portugal</option>
                                        <option value="PR" <?php /*if($edit_data['var_country'] == 'PR'){ echo 'selected="selected"'; } */?>>Puerto Rico</option>
                                        <option value="QA" <?php /*if($edit_data['var_country'] == 'QA'){ echo 'selected="selected"'; } */?>>Qatar</option>
                                        <option value="RE" <?php /*if($edit_data['var_country'] == 'RE'){ echo 'selected="selected"'; } */?>>Reunion</option>
                                        <option value="RO" <?php /*if($edit_data['var_country'] == 'RO'){ echo 'selected="selected"'; } */?>>Romania</option>
                                        <option value="RU" <?php /*if($edit_data['var_country'] == 'RU'){ echo 'selected="selected"'; } */?>>Russian Federation</option>
                                        <option value="RW" <?php /*if($edit_data['var_country'] == 'RW'){ echo 'selected="selected"'; } */?>>Rwanda</option>
                                        <option value="KN" <?php /*if($edit_data['var_country'] == 'KN'){ echo 'selected="selected"'; } */?>>Saint Kitts and Nevis</option>
                                        <option value="LC" <?php /*if($edit_data['var_country'] == 'LC'){ echo 'selected="selected"'; } */?>>Saint LUCIA</option>
                                        <option value="VC" <?php /*if($edit_data['var_country'] == 'VC'){ echo 'selected="selected"'; } */?>>Saint Vincent and the Grenadines</option>
                                        <option value="WS" <?php /*if($edit_data['var_country'] == 'WS'){ echo 'selected="selected"'; } */?>>Samoa</option>
                                        <option value="SM" <?php /*if($edit_data['var_country'] == 'SM'){ echo 'selected="selected"'; } */?>>San Marino</option>
                                        <option value="ST" <?php /*if($edit_data['var_country'] == 'ST'){ echo 'selected="selected"'; } */?>>Sao Tome and Principe</option>
                                        <option value="SA" <?php /*if($edit_data['var_country'] == 'SA'){ echo 'selected="selected"'; } */?>>Saudi Arabia</option>
                                        <option value="SN" <?php /*if($edit_data['var_country'] == 'SN'){ echo 'selected="selected"'; } */?>>Senegal</option>
                                        <option value="SC" <?php /*if($edit_data['var_country'] == 'SC'){ echo 'selected="selected"'; } */?>>Seychelles</option>
                                        <option value="SL" <?php /*if($edit_data['var_country'] == 'SL'){ echo 'selected="selected"'; } */?>>Sierra Leone</option>
                                        <option value="SG" <?php /*if($edit_data['var_country'] == 'SG'){ echo 'selected="selected"'; } */?>>Singapore</option>
                                        <option value="SK" <?php /*if($edit_data['var_country'] == 'SK'){ echo 'selected="selected"'; } */?>>Slovakia (Slovak Republic)</option>
                                        <option value="SI" <?php /*if($edit_data['var_country'] == 'SI'){ echo 'selected="selected"'; } */?>>Slovenia</option>
                                        <option value="SB" <?php /*if($edit_data['var_country'] == 'SB'){ echo 'selected="selected"'; } */?>>Solomon Islands</option>
                                        <option value="SO" <?php /*if($edit_data['var_country'] == 'SO'){ echo 'selected="selected"'; } */?>>Somalia</option>
                                        <option value="ZA" <?php /*if($edit_data['var_country'] == 'ZA'){ echo 'selected="selected"'; } */?>>South Africa</option>
                                        <option value="GS" <?php /*if($edit_data['var_country'] == 'GS'){ echo 'selected="selected"'; } */?>>South Georgia and the South Sandwich Islands</option>
                                        <option value="ES" <?php /*if($edit_data['var_country'] == 'ES'){ echo 'selected="selected"'; } */?>>Spain</option>
                                        <option value="LK" <?php /*if($edit_data['var_country'] == 'LK'){ echo 'selected="selected"'; } */?>>Sri Lanka</option>
                                        <option value="SH" <?php /*if($edit_data['var_country'] == 'SH'){ echo 'selected="selected"'; } */?>>St. Helena</option>
                                        <option value="PM" <?php /*if($edit_data['var_country'] == 'PM'){ echo 'selected="selected"'; } */?>>St. Pierre and Miquelon</option>
                                        <option value="SD" <?php /*if($edit_data['var_country'] == 'SD'){ echo 'selected="selected"'; } */?>>Sudan</option>
                                        <option value="SR" <?php /*if($edit_data['var_country'] == 'SR'){ echo 'selected="selected"'; } */?>>Suriname</option>
                                        <option value="SJ" <?php /*if($edit_data['var_country'] == 'SJ'){ echo 'selected="selected"'; } */?>>Svalbard and Jan Mayen Islands</option>
                                        <option value="SZ" <?php /*if($edit_data['var_country'] == 'SZ'){ echo 'selected="selected"'; } */?>>Swaziland</option>
                                        <option value="SE" <?php /*if($edit_data['var_country'] == 'SE'){ echo 'selected="selected"'; } */?>>Sweden</option>
                                        <option value="CH" <?php /*if($edit_data['var_country'] == 'CH'){ echo 'selected="selected"'; } */?>>Switzerland</option>
                                        <option value="SY" <?php /*if($edit_data['var_country'] == 'SY'){ echo 'selected="selected"'; } */?>>Syrian Arab Republic</option>
                                        <option value="TW" <?php /*if($edit_data['var_country'] == 'TW'){ echo 'selected="selected"'; } */?>>Taiwan, Province of China</option>
                                        <option value="TJ" <?php /*if($edit_data['var_country'] == 'TJ'){ echo 'selected="selected"'; } */?>>Tajikistan</option>
                                        <option value="TZ" <?php /*if($edit_data['var_country'] == 'TZ'){ echo 'selected="selected"'; } */?>>Tanzania, United Republic of</option>
                                        <option value="TH" <?php /*if($edit_data['var_country'] == 'TH'){ echo 'selected="selected"'; } */?>>Thailand</option>
                                        <option value="TG" <?php /*if($edit_data['var_country'] == 'TG'){ echo 'selected="selected"'; } */?>>Togo</option>
                                        <option value="TK" <?php /*if($edit_data['var_country'] == 'TK'){ echo 'selected="selected"'; } */?>>Tokelau</option>
                                        <option value="TO" <?php /*if($edit_data['var_country'] == 'TO'){ echo 'selected="selected"'; } */?>>Tonga</option>
                                        <option value="TT" <?php /*if($edit_data['var_country'] == 'TT'){ echo 'selected="selected"'; } */?>>Trinidad and Tobago</option>
                                        <option value="TN" <?php /*if($edit_data['var_country'] == 'TN'){ echo 'selected="selected"'; } */?>>Tunisia</option>
                                        <option value="TR" <?php /*if($edit_data['var_country'] == 'TR'){ echo 'selected="selected"'; } */?>>Turkey</option>
                                        <option value="TM" <?php /*if($edit_data['var_country'] == 'TM'){ echo 'selected="selected"'; } */?>>Turkmenistan</option>
                                        <option value="TC" <?php /*if($edit_data['var_country'] == 'TC'){ echo 'selected="selected"'; } */?>>Turks and Caicos Islands</option>
                                        <option value="TV" <?php /*if($edit_data['var_country'] == 'TV'){ echo 'selected="selected"'; } */?>>Tuvalu</option>
                                        <option value="UG" <?php /*if($edit_data['var_country'] == 'UG'){ echo 'selected="selected"'; } */?>>Uganda</option>
                                        <option value="UA" <?php /*if($edit_data['var_country'] == 'UA'){ echo 'selected="selected"'; } */?>>Ukraine</option>
                                        <option value="AE" <?php /*if($edit_data['var_country'] == 'AE'){ echo 'selected="selected"'; } */?>>United Arab Emirates</option>
                                        <option value="GB" <?php /*if($edit_data['var_country'] == 'GB'){ echo 'selected="selected"'; } */?>>United Kingdom</option>
                                        <option value="US" <?php /*if($edit_data['var_country'] == 'US'){ echo 'selected="selected"'; } */?>>United States</option>
                                        <option value="UM" <?php /*if($edit_data['var_country'] == 'UM'){ echo 'selected="selected"'; } */?>>United States Minor Outlying Islands</option>
                                        <option value="UY" <?php /*if($edit_data['var_country'] == 'UY'){ echo 'selected="selected"'; } */?>>Uruguay</option>
                                        <option value="UZ" <?php /*if($edit_data['var_country'] == 'UZ'){ echo 'selected="selected"'; } */?>>Uzbekistan</option>
                                        <option value="VU" <?php /*if($edit_data['var_country'] == 'VU'){ echo 'selected="selected"'; } */?>>Vanuatu</option>
                                        <option value="VE" <?php /*if($edit_data['var_country'] == 'VE'){ echo 'selected="selected"'; } */?>>Venezuela</option>
                                        <option value="VN" <?php /*if($edit_data['var_country'] == 'VN'){ echo 'selected="selected"'; } */?>>Viet Nam</option>
                                        <option value="VG" <?php /*if($edit_data['var_country'] == 'VG'){ echo 'selected="selected"'; } */?>>Virgin Islands (British)</option>
                                        <option value="VI" <?php /*if($edit_data['var_country'] == 'VI'){ echo 'selected="selected"'; } */?>>Virgin Islands (U.S.)</option>
                                        <option value="WF" <?php /*if($edit_data['var_country'] == 'WF'){ echo 'selected="selected"'; } */?>>Wallis and Futuna Islands</option>
                                        <option value="EH" <?php /*if($edit_data['var_country'] == 'EH'){ echo 'selected="selected"'; } */?>>Western Sahara</option>
                                        <option value="YE" <?php /*if($edit_data['var_country'] == 'YE'){ echo 'selected="selected"'; } */?>>Yemen</option>
                                        <option value="ZM" <?php /*if($edit_data['var_country'] == 'ZM'){ echo 'selected="selected"'; } */?>>Zambia</option>
                                        <option value="ZW" <?php /*if($edit_data['var_country'] == 'ZW'){ echo 'selected="selected"'; } */?>>Zimbabwe</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>-->
<div class="col-md-12 text-center">
    <button type="submit" class="default_btn margin-top-10" style="padding: 6px 25px;">Update</button>
</div>
</form>
</div>
</div>
</div>
<div aria-hidden="false" role="dialog" class="modal fade in" id="myModal_autocomplete" style="display: none;">
    <div class="success_login_main_cont margin-top-20">
        <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="fa fa-times"></i></button>
            <!--<h4 class="modal-title">Registration Successfully</h4>-->
            <h3 class="form-title">Account Update</h3>
        </div>
        <div style="" class="">
            <p style="padding: 10px">Thank you for keeping your account updated, your changes were successfully saved!</p>
        </div>
        <div style="padding: 3px;" class="modal-footer">
            <div style="text-align:center">
                <button type="button" style="padding: 7px 20px;" data-dismiss="modal" class="btn default_btn clickme">OK</button>
            </div>
        </div>
    </div>
</div>
<div aria-hidden="false" role="dialog" class="modal fade in" id="myModal_autocomplete1" style="display: none;">
    <div class="success_login_main_cont margin-top-20">
        <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="fa fa-times"></i></button>
            <!--<h4 class="modal-title">Registration Successfully</h4>-->
            <h3 class="form-title">Account Update Error</h3>
        </div>
        <div style="" class="">
            <p style="padding: 10px">Something went wrong while updating you account settings. Please try again!</p>
        </div>
        <div style="padding: 3px;" class="modal-footer">
            <div style="text-align:center">
                <button type="button" style="padding: 7px 20px;" data-dismiss="modal" class="btn default_btn clickme">OK</button>
            </div>
        </div>
    </div>
</div>
