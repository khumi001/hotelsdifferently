<style>
    .closeok {
        color: #000;
        float: center;
        font-size: 21px;
        font-weight: 700;
        line-height: 1;
        opacity: 0.2;
        text-shadow: 0 1px 0 #fff;
    }
</style>

<div class="width-row margin-top-20">
    <div class="main_cont">
        <div class="pagetitle margin-bottom-10">
            <h1><?php echo $this->page_name; ?></h1>
        </div>
        <div class="row">
            <form action="#" method="post" class="form-horizontal form-bordered" id="chnge_email">
                <div class="col-md-6 form">
                    <div class="form-group">
                        <label class="control-label col-md-5">Current email address:</label>
                        <div  class="col-md-7">
                            <input type="email" name="email" class="form-control" value="<?php echo $user[0]->var_email; ?>" readonly="true">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-5">New email address:</label>
                        <div  class="col-md-7">
                            <input type="email" name="new_email" id="newemail" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-5">Confirm email address:</label>
                        <div  class="col-md-7">
                            <input type="email" name="confirm_email" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-5"></div>
                        <div class="col-md-7">
                            <button type="submit" class="default_btn" style="padding: 6px 25px;"></i>Update</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

 

<div aria-hidden="false" role="dialog" class="modal fade in" id="exits_modal" style="display: none;">
    <div class="success_login_main_cont margin-top-20">
        <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="fa fa-times"></i></button>
            <!--<h4 class="modal-title">Registration Successfully</h4>-->
            <h3 class="form-title">EMAIL ERROR!</h3>
        </div>
        <div style="" class="">
            <p style="padding: 10px">The new email address you provided is already in our database. Please try again!</p>
        </div>
        <div style="padding: 3px;" class="modal-footer">
            <div style="text-align:center">
                <button type="button" style="padding: 7px 20px;" data-dismiss="modal" class="btn default_btn clickme">OK</button>
            </div>
        </div>
    </div> 
    </div>
<div aria-hidden="false" role="dialog" class="modal fade in" id="myModal_autocomplete" style="display: none;">
    <div class="success_login_main_cont margin-top-20">
        <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="fa fa-times"></i></button>
            <!--<h4 class="modal-title">Registration Successfully</h4>-->
            <h3 class="form-title">Email Change</h3>
        </div>
        <div style="" class="">
            <p style="padding: 10px">Thank you for keeping your account updated. A notification was sent to your previous email address for your records and an activation link was sent out to your new email address for verification purposes. Please check the inbox of your new email address within 48 hours to activate it</p>
        </div>
        <div style="padding: 3px;" class="modal-footer">
            <div style="text-align:center">
                <button type="button" style="padding: 7px 20px;" data-dismiss="modal" class="btn default_btn clickme">OK</button>
            </div>
        </div>
    </div> 
    </div>
