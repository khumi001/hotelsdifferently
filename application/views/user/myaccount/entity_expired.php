<style>
    .enroling-list{
        padding: 0;
    }
    .enroling-list > li {
        margin: 5px 0 10px 20px;
    }
    .submit-btn-furture {
        background: rgba(0, 0, 0, 0) linear-gradient(to bottom, #71d1ff 0%, #22b3f4 4%, #198fef 100%) repeat scroll 0 0;
        border: medium none;
        border-radius: 6px;
        box-shadow: 0 1px 3px rgba(0, 0, 0, 0.4);
        color: #fff;
        font-family: "FuturaStd-Book";
        font-size: 16px;
        margin: 0 60px 30px 40px;
        padding: 10px 25px;
        text-shadow: 1px 1px 2px #000;
        text-transform: uppercase;
    }
    .submit-btn-furture {
        background: rgba(0, 0, 0, 0) linear-gradient(to bottom, #71d1ff 0%, #22b3f4 4%, #198fef 100%) repeat scroll 0 0;
        border: medium none;
        border-radius: 6px;
        box-shadow: 0 1px 3px rgba(0, 0, 0, 0.4);
        color: #fff;
        font-family: "FuturaStd-Book";
        font-size: 16px;
        margin: 0 60px 30px 40px;
        padding: 10px 25px;
        text-shadow: 1px 1px 2px #000;
        text-transform: uppercase;
    }
    .form-group.furture-form-style {
        margin: 15px 0 0;
        text-align: center;
    }
    .underli
</style>
<div class="width-row" style="background-color: rgba(255, 255, 255, 0.74) none repeat scroll 0 0;">
    <div class="main_cont">
        <div class="" style="margin: 10px 0;">
          <p>Dear <strong><?php echo $user['var_fname'].' '.$user['var_lname']?></strong>,</p>
        </div>
        <p>We wanted to thank you for using <strong>Wholesale Hotels Group. </strong> As the saying goes <i>"All good things must come to an end</i>but this particular one does not have to! <strong>Your membership has now expired!</strong></p>
        <p>If you liked our service, please contact your Master Account holder <i>(the person who sent you the corporate signup link originally) </i> to renew your membership. In case your entity personnel decide not to extend your membership, you are then welcome to sign up under your personal email address. Just send us a message via Contact Us, identify your corporate email address and we will send out a discounted membership coupon for you. In case your entity personnel decide to extend your membership, your account will be automatically renewed.</p>

        <p>Don’t wish to extend anymore? Don't worry, you will still be able to log back in to retrieve your existing reservations. We hope to see you back on our site and we thank you in advance for your continued patronage.</p>
        <p>Thank you for choosing <strong> Wholesale Hotels Group.</strong><i> where better deals are made for YOU!</i> </p>
        
    </div>