<div class="main" style="background:url('http://hotels.softech.website/public/affilliate_theme/img/home-bg.jpg');background-repeat:no-repeat;background-size:100% 100%;width:547px;margin:auto">
    <div class="m_3275774443844637159containt-main" style="background:rgba(17,125,200,0.61);color:#fff;width:100%">
        <div class="m_first">
            <div class="left" style="margin:auto;text-align:center;float:none">
                <a class="logo" href="http://www.WHotelsGroup.com" style="display:inline-block;margin:10px 0 0 10px" target="_blank">
                    <img src="http://hotels.softech.website/public/affilliate_theme/img/logo.png" style="width:130px" class="CToWUd">
                </a>
            </div>
            <div class="m_clear" style="clear:both"></div>
        </div>
        <div class="top-containt">
            <h1 style="max-width:350px;margin:20px auto"><img style="display:block;width:100%;height:auto" src="http://hotels.softech.website/public/affilliate_theme/img/txt-email.png" alt="" class="CToWUd"></h1>
        </div>
        <div class="m_clear" style="clear:both"></div>
        <div class="top-hotel" style="width:90%;margin:40px auto auto auto;background:#0c5fa7;color:#ffffff">
            <div style="padding-bottom:10px;padding-top:10px;width:90%;margin:auto;font-family:'Open Sans',sans-serif;color:#fff!important;font-size:12px;line-height:16px">
                <p style="color:#fff;margin-bottom:10px">Dear
                    <strong style="color:#fff"><?php echo $data['var_fname'].' '.$data['var_lname']?></strong>,
                </p>
                <p style="color:#fff;margin-bottom:10px">
                    We wanted to thank you for using  <strong>WHotelsGroup.com</strong> this past year. As the saying goes <i>"All good things must come to an end"</i> but this particular one does not have to! <strong>Your membership has now expired! </strong> Please log back in and renew your membership in order to access the best deals! In order to use all features of our site, please login and renew your expired membership now. Don't worry, you will still be able to log back in to retrieve your existing reservations. We hope to see you back on our site and we thank you in advance for your continued patronage.
                </p>
                <!-- <p style="color:#fff;margin-bottom:10px"><strong>Life without limits!</strong> Interested in our <strong>Lifetime Plan? </strong> Reach out to our customer service to inquire about pricing!
                </p> -->
                <p style="color:#fff;margin-bottom:10px">
                    Thank you for choosing <b>WHotelsGroup.com</b>  <i>Where better deals are made for YOU!</i>
                </p>
                <strong style="color:#fff;margin-bottom:10px">PS: This inbox is NOT monitored, please do NOT send us any emails here.</strong>
                <div class="yj6qo"></div>
                <div class="clear adL" style="clear:both"></div>
            </div>
            <div class="adL"></div>
        </div>
        <div class="adL"></div>
        <div class="adL" style="text-align:center;padding:10px 0"></div>
        <div class="adL"></div>
        <div style="font-size:11px;color:#fff;padding:5px;font-family:'Open Sans',sans-serif;display:block" class="adL">&nbsp;</div>
        <div class="adL"></div>
    </div>
    <div class="adL"></div>
</div>