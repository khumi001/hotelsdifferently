<style type="text/css">
    .payment- {
        background: #fff none repeat scroll 0 0;
        display: block;
        overflow: hidden;
        padding: 20px;
    }

    .btn-red {
        background-color: red;
        border: 1px solid red;
    }

    .margin-bottom {
        display: block;
        padding: 0 0 40px;
    }

    .btn-green {
        background-color: #22b14c;
        border: 1px solid #22b14c;
    }

    .payment- .centent-charge {
        display: block;
        margin: 21px 0 0;
        padding: 15px;
    }

    .submit-btn-furture {
        background: rgba(0, 0, 0, 0) linear-gradient(to bottom, #71d1ff 0%, #22b3f4 4%, #198fef 100%) repeat scroll 0 0;
        border: medium none;
        border-radius: 6px;
        box-shadow: 0 1px 3px rgba(0, 0, 0, 0.4);
        color: #fff;
        font-family: "FuturaStd-Book";
        font-size: 16px;
        margin: 0 60px 30px 40px;
        padding: 10px 25px;
        text-shadow: 1px 1px 2px #000;
        text-transform: uppercase;
    }

    .form-group.furture-form-style {
        text-align: center;
    }
</style>
<div class="container">
<div class="payment-">
<form action="<?php echo base_url("user/myaccount/payment"); ?>" id="payment-form" method="post">
<input type="hidden" id="source_token_3d" name="source_token_3d" value="" />
<input type="hidden" id="last_four" name="lastfour" value="" />
<span class="hidden_div text-center" style="display: block;margin:0 0 20px;">Your information is safe and secure</span>
<span class="payment-errors"></span>
<?php
if(isset($_SESSION['error']))
{
    ?>
    <div class="alert alert-danger">
        <?php
        echo $_SESSION['error'];
        unset($_SESSION['error']);
        ?>
    </div>
<?php
}
?>
<!--<div class="form-group hidden_div margin-bottom" style="display: block;margin">
    <label class="control-label col-sm-3">Card Number:</label>

    <div class="col-sm-9">
        <input data-stripe="number" class="cardInfo incorrect_number form-control input-medium" id="card-number"
               required="" type="text">
    </div>
</div>-->
<?php if($this->session->userdata['membership']['paymentMethod'] == 'newCard'){?>
    <div class="form-group hidden_div margin-bottom" style="display: block;margin">
        <label class="control-label col-sm-3">Card Number:</label>
        <div  class="col-sm-9">
            <input type='text' data-stripe="number" maxlength="16" placeholder="" class="cardInfo incorrect_number form-control input-medium" id='card-number' required>
        </div>
    </div>
<?php }else if($this->session->userdata['membership']['paymentMethod'] == 'oldCard'){?>
    <div class="form-group hidden_div margin-bottom" style="display: block;margin">
        <label class="control-label col-sm-3">Card Number:</label>
        <div  class="col-sm-7">
            <input type='text' data-stripe="number" maxlength="12" placeholder="First 11 digits for AMEX or First 12 digits for VISA/MC" class="cardInfo incorrect_number form-control input-medium" id='card-number' required>
        </div>
        <div class="col-sm-2">
            <strong class="billing-name" style="font-size: 20px; position: relative; bottom: 5px; right: 20px;"><?php echo $userDetail['cardNumber'];?></strong>
        </div>
    </div>
<?php }?>
<div class="form-group hidden_div margin-bottom" style="display: block;">
    <label class="control-label col-sm-3">Expiration:</label>

    <div class="col-sm-9">
        <div class="row">
            <div class="col-sm-6">
                <select class="invalid_number invalid_expiry_month cardInfo select2me form-control input-xlarge"
                        data-stripe="exp_month" id="card-expiry-month" name="data[month]" required="">
                    <option class="" value="-1" selected="selected">month</option>

                    <option class="" value="01">01</option>
                    <option class="" value="02">02</option>
                    <option class="" value="03">03</option>
                    <option class="" value="04">04</option>
                    <option class="" value="05">05</option>
                    <option class="" value="06">06</option>
                    <option class="" value="07">07</option>
                    <option class="" value="08">08</option>
                    <option class="" value="09">09</option>
                    <option class="" value="10">10</option>
                    <option class="" value="11">11</option>
                    <option class="" value="12">12</option>
                </select>
            </div>
            <div class="col-sm-6">
                <select class="invalid_number invalid_expiry_year cardInfo select2me form-control input-xlarge"
                        data-stripe="exp_year" id="card-expiry-year" name="data[year]" required="">
                    <option class="" value="-1" selected="selected">year</option>
                    <option class="" value="2017">2017</option>
                    <option class="" value="2018">2018</option>
                    <option class="" value="2019">2019</option>
                    <option class="" value="2020">2020</option>
                    <option class="" value="2021">2021</option>
                    <option class="" value="2022">2022</option>
                    <option class="" value="2023">2023</option>
                    <option class="" value="2024">2024</option>
                    <option class="" value="2025">2025</option>
                    <option class="" value="2026">2026</option>
                    <option class="" value="2027">2027</option>
                    <option class="" value="2028">2028</option>
                    <option class="" value="2029">2029</option>
                    <option class="" value="2030">2030</option>
                    <option class="" value="2031">2031</option>
                    <option class="" value="2032">2032</option>
                </select>
            </div>
        </div>
    </div>
</div>
<div class="form-group hidden_div margin-bottom" style="display: block;">
    <label class="control-label col-sm-3">Security Code:</label>

    <div class="col-sm-9">
        <input data-stripe="cvc" class="form-control input-medium" id="card-cvc" required="" type="text">
    </div>
</div>
<div class="clearfix"></div>
<h4 class="bill-mrgn-sec hidden_div" style="display: block;">Billing Information</h4>

<div class="form-group hidden_div margin-bottom" style="display: block;">
    <label class="control-label col-sm-3"> Billing Name:</label>

    <div class="col-sm-9">
        <input name="data[fullname]" class="form-control input-medium" id="fullname" value="<?php echo $userDetail['var_fname'] . ' ' . $userDetail['var_lname'] ?>" required="" type="text">
        <!--<strong class="billing-name"><?php /*echo $userDetail['var_fname'] . ' ' . $userDetail['var_lname'] */?></strong>-->
    </div>
</div>
<div class="clearfix"></div>
<div class="form-group hidden_div margin-bottom" style="display: block;">
    <label class="control-label col-sm-3"> Phone:</label>

    <div class="col-sm-9">
        <input name="data[phone]" class="form-control input-medium" id="phone" value="" required="" type="text">
    </div>
</div>
<div class="form-group hidden_div margin-bottom" style="display: block;">
    <label class="control-label col-sm-3">Country:</label>

    <div class="col-sm-9">
        <select data-stripe="address_country" class="select2me form-control input-xlarge" id="country" name="data[country]" id="" required>
            <?php
            foreach($countries as $contr)
            {
                if($contr->id == 192)
                {
                    ?>
                    <option value="<?php echo $contr->id; ?>" selected><?php echo $contr->name; ?></option>
                <?php

                }
                else
                {
                    ?>
                    <option value="<?php echo $contr->id; ?>"><?php echo $contr->name; ?></option>
                <?php
                }

            }
            ?>

        </select>
    </div>
</div>
<div class="form-group hidden_div margin-bottom" style="display: block;">
    <label class="control-label col-sm-3">State:</label>

    <div class="col-sm-9">
        <select data-stripe="address_state" id="state" value='<?php echo (isset($input_data_book_card['state'])) ? $input_data_book_card['state'] : ''; ?>' class="select2me form-control input-xlarge" name="data[state]" id="" required>

            <?php
            echo $states;
            ?>
        </select>
    </div>
</div>
<div class="form-group hidden_div margin-bottom" style="display: block;">
    <label class="control-label col-sm-3">Zip:</label>

    <div class="col-sm-9">
        <input data-stripe="address_zip" id="address-zip" value="" name="data[zip]" class="form-control input-medium"
               required="" type="text">
    </div>
</div>
<div class="form-group hidden_div margin-bottom" style="display: block;">
    <label class="control-label col-sm-3">City:</label>

    <div class="col-sm-9">
        <input data-stripe="address_city" id="address_city" value="" name="data[city]" class="form-control input-medium"
               required="" type="text">
    </div>
</div>
<div class="form-group hidden_div margin-bottom" style="display: block;">
    <label class="control-label col-sm-3">Street:</label>

    <div class="col-sm-9">
        <input data-stripe="address_line1" id="address_line1" value="" name="data[street]"
               class="form-control input-medium" required="" type="text">
    </div>
</div>
<div class="form-group hidden_div margin-bottom" style="display: block;">
    <label class="control-label col-sm-3">Promo code:</label>

    <div class="col-sm-7">
        <input name="code" id="cCode" class="form-control input-medium" type="text">
    </div>
    <div class="col-sm-2">
        <button type="button" id="cCodeB" href="javascript:;" class="btn btn-primary pull-left btn-red btn-green">Update</button>
    </div>
</div>
<div class="col-sm-3">
    <div class="clearfix"></div>
</div>
<div class="col-sm-9">
    <div class="form-group hidden_div margin-bottom" style="display: block; text-align: center;">
        <label class="control-label col-sm-3" style="font-size: 20px;"> Total:</label>

        <div class="col-sm-3">
            <strong class="payAmount billing-name" style="color: red; font-size: 20px;">$<?php echo $this->session->userdata['membership']['amount'];?> </strong>
        </div>
    </div>
</div>
<p class="centent-charge">You will be charged <strong class="payAmount">$<?php echo $this->session->userdata['membership']['amount'];?></strong> and your charge will appear as
    <strong>WholsaleHtLs8882872307.</strong> You hereby agree that there will be <strong>
        <ins> NO REFUNDS</ins>
    </strong> on your membership.</strong>
    You hereby also agree and accept that your membership will <strong>NOT<strong> be activated instantly and that we
            will conduct further verification to ensure that you are the authorized cardholder.</p>
<div class="clearfix"></div>
<div class="form-group furture-form-style">
    <a href="<?php echo base_url("/user/myaccount/membership");?>">
        <button type="button" class="submit-btn-furture " style="background: red;">Back <i
                class="fa fa-refresh fa-lg fa-spin loadin_ajax" style="color: #ffffff;display:none;"></i></button>
    </a>
    <button type="submit" id="goToForm" class="submit submit-btn-furture">Pay Now</button>
</div>

</form>
</div>
</div>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script type="text/javascript" src="https://js.stripe.com/v3/"></script>

<script type="text/javascript">

    // coupon
    $("#cCodeB").click(function() {
        var code = $('#cCode').val();
        if(code == null || code == ""){
            alert("Please provide the code");
            return false;
        }
        $.post("<?php echo base_url(); ?>user/myaccount/retrieveCoupon/",
            {
                code:code
            },
            function (data, status) {
                if (data == 'error') {
                    alert("Invalid code")
                } else {
                    $(".payAmount").text("$"+data);
                }
            });
    });

    Stripe.setPublishableKey('pk_test_ylWaFpVcwMp8ng54Q2zzDqNy');
    var $form = $('#payment-form');
    $(function () {
        $form.submit(function (event) {
            // Disable the submit button to prevent repeated clicks:
            $form.find('.submit').prop('disabled', true);

            // Request a token from Stripe:
            //Stripe.card.createToken($form, stripeTokenResponseHandler);
            var paymentMethod = '<?php echo ($this->session->userdata['membership']['paymentMethod']); ?>';
            if(paymentMethod == 'newCard'){
                cardnum = $('#card-number').val();
                $('#last_four').val(cardnum.substr( cardnum.length - 4 , cardnum.length - 1));
            }else if(paymentMethod == 'oldCard'){
                var last_four = '<?php echo $userDetail["cardNumber"];?>';
                cardnum = $('#card-number').val();
                cardnum = cardnum + "" + last_four;
                $('#last_four').val(last_four);
            }

            //Request a source object from Stripe:
            Stripe.source.create({
                type: 'card',
                card: {
                    number: cardnum,
                    cvc: $('#card-cvc').val(),
                    exp_month: $('#card-expiry-month').val(),
                    exp_year: $('#card-expiry-year').val()
                },
                owner: {
                    address: {
                        line1: $('#address_line1').val(),
                        postal_code: $('#address-zip').val(),
                        city: $('#address_city').val(),
                        country: $('#country').val(),
                        state: $('#state').val()
                    },
                    email: "<?php echo $this->session->userdata['valid_user']['var_email'];?>",
                    //name: "<?php //echo $userDetail['var_fname'].' '.$userDetail['var_lname'];?>",
                    name: $('#fullname').val(),
                    phone: $('#phone').val()
                }
            }, stripeResponseHandler);


            // Prevent the form from being submitted:
            return false;
        });
    });

    function stripeResponseHandler(status, response) {
        if (response.error) { // Problem!
            //alert(response.error);
            // remove highlight if any
            $(".cardInfo").css("border-color","#ccc");
            // highlight fields according to error type
            $("."+response.error.code).css("border-color","red");
            alert("Your card information is not correct");
            // Show the errors on the form
            //$form.find('.payment-errors').text(response.error.message);
            $form.find('button').prop('disabled', false); // Re-enable submission
        } else { // Source was created!

            // Get the source ID:
            var source = response.id;

            //stripe3DSource(source);

            // Insert the source into the form so it gets submitted to the server:
            $form.append($('<input type="hidden" name="source" />').val(source));
            $form.append($('<input type="hidden" name="three_d_secure" />').val(response.card.three_d_secure));
            stripe3DSource(source,response.card.three_d_secure);
            // Submit the form:
            //$form.get(0).submit();
        }
    }

    function stripe3DSource(stripeSource,stripe3DSecure) {
        if (stripe3DSecure == 'not_supported') {
            $('#source_token_3d').val(stripeSource);
            alert('No support');
            $form.get(0).submit();
        } else {
            var customerId = '';
            // create custmer with card source
            $.post("<?php echo base_url(); ?>user/myaccount/createCustomer/",
                {
                    email: "<?php echo $this->session->userdata['valid_user']['var_email'];?>",cardSource:stripeSource
                },
                function(data, status){
                    data = $.parseJSON(data);
                    var card = $('#last_four').val();
                    if(data.status == 0){
                        alert("Something went wrong... please try again");
                    }else {
                        Stripe.source.create({
                            type: 'three_d_secure',
                            //amount: '<?php //echo filter_var($this->session->userdata['membership']['actualPrice'], FILTER_SANITIZE_NUMBER_INT); ?>',
                            amount: data.amount * (100),
                            currency: "usd",
                            three_d_secure: {
                                customer: data.id,
                                card: stripeSource
                            },
                            redirect: {
                                return_url: "<?php echo base_url('user/myaccount/stripecallback?card=') ?>"+card
                            }
                        }, Stripe3DSecureResponseHandler);
                    }
                });
        }
    }

    function Stripe3DSecureResponseHandler(status, response) {

        if (response.error) { // Problem!

            // Show the errors on the form
            $form.find('.payment-errors').text(response.error.message);
            $form.find('button').prop('disabled', false); // Re-enable submission

        } else { // Source was created!

            location.href = response.redirect.url;
            return;
            //var myWindow = window.open(response.redirect.url, "3D Secure Payment", "width=500,height=400");

            //$( "#stripeConfirmationModalBody" ).load( response.redirect.url );
            //$( "#stripeConfirmationModalBody" ).html( '<iframe src="<?php //echo base_url(); ?>/front/tourico_api/load_iframe?url='+response.redirect.url+'"></iframe>' );

            //$('#stripeConfirmation')[0].click();

            var source = response.id;
            var client_secret = response.client_secret;

            Stripe.source.poll(
                source,
                client_secret,
                function (status, src) {

                    if (src.status == 'chargeable') {
                        myWindow.close();
                        $('#source_token_3d').val(src.id);
                        //$('#client_secret').val(src.client_secret);
                        $('#quote_request').submit();
                        //location.href = "<?php echo base_url(); ?>request_quote?stp=cnfrm";
                    } else if (src.status == 'failed') {
                        myWindow.close();
                        location.href = "<?php echo base_url(); ?>request_quote?stp=s&error=failed";
                    } else if (src.status == 'canceled') {
                        myWindow.close();
                        location.href = "<?php echo base_url(); ?>request_quote?stp=s&error=failed";
                    } else if (src.status == 'consumed') {
                        myWindow.close();
                        location.href = "<?php echo base_url(); ?>request_quote?stp=s&error=failed";
                    }

                });
        }
    }

    $('#country').change(function(){
        $('#state').html('');
        $.post("<?php echo base_url(); ?>front/tourico_api/get_cities/"+$(this).val(),
            {
                contryid: $(this).val()
            },
            function(data, status){
                $('#state').html(data);
            });
    });
</script>