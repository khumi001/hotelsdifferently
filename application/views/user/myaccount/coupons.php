<div class="width-row margin-top-20">
<div class="main_cont">
    <div class="pagetitle margin-bottom-10">
        <h1><?php echo $this->page_name; ?></h1>
    </div>
    <div class="text-center">
        <label>On this page you can see your coupons!</label>
    </div>
    <div class="row margin-top-10">
        <div class="form-group couponformgroup">
            <label class="col-md-1 control-label">Display:</label>
            <div class="col-md-11">                
                <label class="checkbox"><input type="radio" id="filter" name="filter" class="radio-inline filter" value="">&nbsp;ALL</label>
                <label class="checkbox"><input type="radio" id="filter1" name="filter" class="radio-inline filter" value="A">&nbsp;ACTIVE</label>
                <label class="checkbox"><input type="radio" id="filter2" name="filter" class="radio-inline filter" value="U">&nbsp;USED</label>
                <label class="checkbox"><input type="radio" id="filter3" name="filter" class="radio-inline filter" value="E">&nbsp;EXPIRED </label>            
            </div>
        </div>
    </div>
    <div class="content_row">
        <table class="table table-striped table-bordered table-hover" id="coupons">
            <thead>
                <tr role="row" class="heading">
                    <th width='10%'>Date</th>
                    <th width='20%'>Coupon code</th>
                    <th width='25%'>Coupon value</th>
                    <th width='15%'>Coupon expiration</th>
                    <th width='15%'>Status</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div> 
</div>
</div>