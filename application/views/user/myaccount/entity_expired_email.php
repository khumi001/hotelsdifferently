<div class="main" style="background:url('http://hotels.softech.website/public/affilliate_theme/img/home-bg.jpg');background-repeat:no-repeat;background-size:100% 100%;width:547px;margin:auto">
    <div class="m_3275774443844637159containt-main" style="background:rgba(17,125,200,0.61);color:#fff;width:100%">
        <div class="m_first">
            <div class="left" style="margin:auto;text-align:center;float:none">
                <a class="logo" href="https://www.WHotelsGroup.com" style="display:inline-block;margin:10px 0 0 10px" target="_blank">
                    <img src="http://hotels.softech.website/public/affilliate_theme/img/logo.png" style="width:130px" class="CToWUd">
                </a>
            </div>
            <div class="m_clear" style="clear:both"></div>
        </div>
        <div class="top-containt">
            <h1 style="max-width:350px;margin:20px auto"><img style="display:block;width:100%;height:auto" src="http://hotels.softech.website/public/affilliate_theme/img/txt-email.png" alt="" class="CToWUd"></h1>
        </div>
        <div class="m_clear" style="clear:both"></div>
        <div class="top-hotel" style="width:90%;margin:40px auto auto auto;background:#0c5fa7;color:#ffffff">
            <div style="padding-bottom:10px;padding-top:10px;width:90%;margin:auto;font-family:'Open Sans',sans-serif;color:#fff!important;font-size:12px;line-height:16px">
                <p style="color:#fff;margin-bottom:10px">Dear
                    <strong style="color:#fff"><?php echo $data['var_fname'].' '.$data['var_lname']?></strong>,
                </p>
                <p style="color:#fff;margin-bottom:10px">
                    We wanted to thank you for using <strong>Wholesale Hotels Group. </strong> As the saying goes <i>"All good things must come to an end</i> but this particular one does not have to! <strong>Your membership has now expired!</strong>
                </p>
                <p style="color:#fff;margin-bottom:10px">
                    If you liked our service, please contact your Master Account holder
                    <i>(the person who sent you the corporate signup link originally) </i>
                    to renew your membership. In case your entity personnel decide not to extend your membership, you are then welcome to sign up under your personal email address. Just send us a message via Contact Us, identify your corporate email address and we will send out a discounted membership coupon for you. In case your entity personnel decide to extend your membership, your account will be automatically renewed.
                </p>
                <p>Don’t wish to extend anymore? Don't worry, you will still be able to log back in to retrieve your existing reservations. We hope to see you back on our site and we thank you in advance for your continued patronage.</p>
                <p>Thank you for choosing <strong> Wholesale Hotels Group.</strong><i> where better deals are made for YOU!</i> </p>
                <div class="yj6qo"></div>
                <div class="clear adL" style="clear:both"></div>
            </div>
            <div class="adL"></div>
        </div>
        <div class="adL"></div>
        <div class="adL" style="text-align:center;padding:10px 0"></div>
        <div class="adL"></div>
        <div style="font-size:11px;color:#fff;padding:5px;font-family:'Open Sans',sans-serif;display:block" class="adL">&nbsp;</div>
        <div class="adL"></div>
    </div>
    <div class="adL"></div>
</div>