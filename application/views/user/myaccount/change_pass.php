<style>
    .closeok {
        color: #000;
        float: center;
        font-size: 21px;
        font-weight: 700;
        line-height: 1;
        opacity: 0.2;
        text-shadow: 0 1px 0 #fff;
    }
	.change-pass-form{
		max-width:700px;
		margin:0 auto;
	}
	.change-pass-page h1{
		text-align:center;
	}
</style>
<div class="width-row margin-top-20 change-pass-page">
    <div class="main_cont">
        <div class="pagetitle margin-bottom-10">
            <h1><?php echo $this->page_name; ?></h1>
        </div>
        <div class="row">
            <form action="#" method="post" class="form-horizontal form-bordered" id="edit_pass">
				<div class="col-md-12" id="error_msg" style="color: red;display:none;margin-bottom:10px;" >
					
				</div>
                <div class="form change-pass-form">
                    <div class="form-group">
                        <label class="control-label col-md-6">Current Password:</label>
                        <div  class="col-md-6">
                            <input type="password" name="old_pass" id="old_pass" class="form-control oldpass" onCopy="return false" onDrag="return false"  onPaste="return false" onDrop="return false" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-6">New Password: <a class="tooltips" data-placement="top" data-original-title="Your password is CaSe SeNsItIvE, must be at least 8 characters and must contain at least one letter and one number."><sup><i class="fa fa-info-circle"></i></sup></a></label>
                        <div  class="col-md-6">
                            <input type="password" name="new_pass" id="newpassword" class="form-control newpass" onCopy="return false" onDrag="return false"  onPaste="return false" onDrop="return false" autocomplete="off" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-6">New Password Confirmation:</label>
                        <div  class="col-md-6">
                            <input type="password" name="confirm_pass" class="form-control confirmpass" onPaste="return false" onDrag="return false" onDrop="return false" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-4"></div>
                        <div class="col-md-8">
                            <button type="submit" class="default_btn" style="padding: 6px 25px;"></i>Update</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div aria-hidden="false" role="dialog" class="modal fade in" id="myModal_autocomplete" style="display: none;">
    <div class="success_login_main_cont margin-top-20">
        <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="fa fa-times"></i></button>
            <!--<h4 class="modal-title">Registration Successfully</h4>-->
            <h3 class="form-title">Password Change Successfully</h3>
        </div>
        <div style="" class="">
            <p style="padding: 10px">Thank you for keeping your account updated. Your password has been successfully changed!</p>
        </div>
        <div style="padding: 3px;" class="modal-footer">
            <div style="text-align:center">
                <button type="button" style="padding: 7px 20px;" data-dismiss="modal" class="btn default_btn clickme">OK</button>
            </div>
        </div>
    </div>
</div>
<div aria-hidden="false" role="dialog" class="modal fade in" id="myModal_autocomplete1" style="display: none;">
    <div class="success_login_main_cont margin-top-20">
        <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="fa fa-times"></i></button>
            <!--<h4 class="modal-title">Registration Successfully</h4>-->
            <h3 class="form-title">Password not match</h3>
        </div>
        <div style="" class="">
            <p style="padding: 10px">Your current password does NOT match our records. Please try again!</p>
        </div>
        <div style="padding: 3px;" class="modal-footer">
            <div style="text-align:center">
                <button type="button" style="padding: 7px 20px;" data-dismiss="modal" class="btn default_btn clickme">OK</button>
            </div>
        </div>
    </div>
</div>
