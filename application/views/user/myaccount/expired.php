<style type="text/css">
    .image-holder-pay img {
        display: block;
        margin: 25px auto 20px;
        max-width: 71%;
    }
    .image-holder-pay > a {
        display: block;
    }
    .main_cont.payment-sec > h1 {
        font-size: 20px;
        font-weight: bold;
        margin: 0 0 10px;
        text-align: center;
        text-transform: capitalize;
    }
    .member-content {
        font-size: 15px;
        margin: 0 0 5px;
        text-align: center;
        text-transform: capitalize;
    }
    .content-holder-payment {
        display: block;
        margin: 0 auto;
        max-width: 90%;
    }
    .submit-btn-furture {
        background: rgba(0, 0, 0, 0) linear-gradient(to bottom, #71d1ff 0%, #22b3f4 4%, #198fef 100%) repeat scroll 0 0;
        border: medium none;
        border-radius: 6px;
        box-shadow: 0 1px 3px rgba(0, 0, 0, 0.4);
        color: #fff;
        font-family: "FuturaStd-Book";
        font-size: 16px;
        margin: 0 60px 30px 40px;
        padding: 10px 25px;
        text-shadow: 1px 1px 2px #000;
        text-transform: uppercase;
    }
    .submit-btn-furture {
        background: rgba(0, 0, 0, 0) linear-gradient(to bottom, #71d1ff 0%, #22b3f4 4%, #198fef 100%) repeat scroll 0 0;
        border: medium none;
        border-radius: 6px;
        box-shadow: 0 1px 3px rgba(0, 0, 0, 0.4);
        color: #fff;
        font-family: "FuturaStd-Book";
        font-size: 16px;
        margin: 0 60px 30px 40px;
        padding: 10px 25px;
        text-shadow: 1px 1px 2px #000;
        text-transform: uppercase;
        bottom: 3px;
        height: 38px;
        position: relative;
        height: 35px;
        line-height: 13px;
        right: 51px;
        top: 0;
    }
    .form-group.furture-form-style {
        margin: 15px 0 0;
        text-align: center;
    }
    .amound-contet{
        display: block;
        margin: 0 0 20px;
    }
    .form-style {
        border: 1px solid #ddd;
        border-radius: 3px;
        height: 35px;
        padding: 8px;
        width: 93%;
    }
    .verify-page .col-md-3 > label {
        position: relative;
        top: 8px;
    }
    .btn-expire-sec.text-center {
      display: block;
      margin: 15px 0 0;
    }
    .btn-expired{
        background: radial-gradient(ellipse at center, #b4ddb4 0%,#83c783 17%,#52b152 33%,#008a00 67%,#005700 83%,#002400 100%);
        border: medium none;
        border-radius: 6px;
        box-shadow: 0 1px 3px rgba(0, 0, 0, 0.4);
        color: #fff;
        font-family: "FuturaStd-Book";
        font-size: 16px;
        padding: 10px 25px;
        text-shadow: 1px 1px 2px #000;
        text-transform: uppercase;
        margin: 0 0 30px;
    }
</style>
<div id="result_container">
    <div class="page_container">
        <div class="width-row" style="background-color: rgba(255, 255, 255, 0.74) none repeat scroll 0 0;">
            <div class="main_cont payment-sec verify-page">
                <!-- <h1>Expired Page</h1> -->
                <div class="clearfix"></div>
                <div class="content-holder-payment">
                    <p>Dear
                        <strong><?php echo $user['var_fname'].' '.$user['var_lname']?></strong>,
                    </p>
                    <p>We wanted to thank you for using <strong>Wholesale Hotels Group.</strong> As the saying goes <i>"All good things must come to an end"</i> but this particular one does not have to!<strong> Your membership has now expired! </strong> </p>
                    <p>If you liked our service, we would like to encourage you to renew your expired membership now. Don't worry, you will still be able to log back in to retrieve your existing reservations. We hope to see you back on our site and we thank you in advance for your continued patronage. </p>
                    <p>Thank you for choosing <strong>Wholesale Hotels Group.</strong> where we are <i>Making hotels happen for YOU!</i> </p>
                    <div class="clearfix"></div>
                </div>
                <div class="btn-expire-sec text-center">
                    <a href="<?php echo base_url("user/myaccount/membership");?>"><button class="btn btn-md btn-expired">RENEW</button></a>
                </div>
            </div>
        </div>
    </div>
</div>