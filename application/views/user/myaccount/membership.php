<?php
$i = 1;
?>
<div class="row" id="log">
<div class="col-md-12 col-sm-12">
    <!-- BEGIN EXAMPLE TABLE PORTLET-->
    <div class="portlet box blue">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-user"></i><?= $Heading ?>
            </div>
        </div>

        <div class="portlet-body">
            <div class="form-group">
                <label class="checkbox" style="padding: 0px; margin:6px 0 0;float: left;">
                    <input type="radio" style="margin-top: -2px;margin-left: 10px; padding-left:0;" id="filter" name="filter"  checked="checked"  class="radio-inline filter" value="-1">&nbsp;ALL
                </label>
                <label class="checkbox" style="padding: 0px; margin:6px 0 0;float: left;"><input type="radio" style="margin-top: -2px;margin-left: 10px; padding-left:0;" id="filter3" name="filter" class="radio-inline filter" value="1">&nbsp;ACTIVE </label>
                <label class="checkbox" style="padding: 0px;  margin:6px 0 0;float: left;"><input type="radio" style="margin-top: -2px;margin-left: 10px; padding-left:0;" id="filter1" name="filter" class="radio-inline filter" value="2">&nbsp;EXPIRED </label>
            </div>
            <div class="clearfix"></div>
            <div id="mainlog">
                <table class="table table-striped table-bordered table-hover" id="sample_2">
                    <thead>
                    <tr>
                        <th>
                            Account ID
                        </th>
                        <th>
                            Date/Time
                        </th>
                        <th>
                            Full name
                        </th>
                        <th>
                            Email
                        </th>
                        <!--<th>
                            Amount
                        </th>-->
                        <th>Expiration</th>
                        <th>Referred By</th>
                        <!--<th>Last 4</th>-->
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody id="resultBox">
                    <?php foreach ($users as $usersData) { ?>
                        <tr id="tr_<?= $usersData->int_glcode ?>">
                            <td><?= $usersData->var_accountid ?></td>
                            <td><?= date(DISPLAY_DATE_FORMAT, strtotime($usersData->startDate)); ?></td>
                            <td><?= $usersData->var_fname . ' ' . $usersData->var_lname ?></td>
                            <td><?= $usersData->var_email ?></td>
                            <!--<td><?/*= "$".$usersData->paid */?></td>-->
                            <td><?= date(DISPLAY_DATE_FORMAT, strtotime($usersData->endDate)); ?></td>

                            <td <?php if($usersData->manuallExtension == 1) echo "style='color: red;'";?> ><?= get_referer_email($usersData->fk_parent_account); ?> <?php if($usersData->manuallExtension == 0){?> <a onclick='return confirm("Are you sure?");' style='background: #000;color: #fff;padding: 2px;' href='<?php echo base_url('admin/regular_users/markedExtension/'.$usersData->int_glcode);?>'>x</a><?php }?></td>
                            <!--<td><?/*= get_referer_email($usersData->fk_parent_account); */?></td>-->
                            <!--<td>
                                    <input   <?php /*if(isSkipLogin()){ */?> readonly="true"  <?php /*} */?> type="text" maxlength="4" value="<?php /*echo $usersData->cardNumber;*/?>" style="width:76px;float:left;" class="priceper" id="reserv_id<?php /*echo $usersData->int_glcode; */?>" />
                                </td>-->
                            <td>
                                <a data-toggle="modal" data-target="#detailModal" onclick="getUserInfo('<?=site_url('admin/regular_users/getUserInfo/'.$usersData->int_glcode)?>','<?=$usersData->int_glcode?>')" href="javascript:void(0)">Details</a>
                                |
                                <!--<button type="button" data-id="<?php /*echo "reserv_id".$usersData->id; */?>" class="btn default btn-xs green"
                                                                                                                                   onClick="addCardNumber('<?php /*echo "reserv_id".$usersData->int_glcode; */?>','<?php /*echo $usersData->int_glcode; */?>')">OK</button>
-->
                                <a data-toggle="modal" data-target="#deleteModal" onClick="setDeleteUid('<?=$usersData->int_glcode?>')" href="javascript:void(0)">Delete</a>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>

        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
<div id="deleteModal" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog" style="width: 800px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Please enter password</h4>
            </div>
            <div class="modal-body">
                <div class="form-group furture-form-style">
                    <label>Password:*</label>
                    <input type="password" class="form-control form-style" id="delete_password" >
                </div>
            </div>
            <div class="modal-footer" >
                <button type="button" class="btn btn-default" id="close_model" data-dismiss="modal" >Close</button>
                <button type="button" class="btn btn-default" onClick="deleteAccount()" >Ok</button>
            </div>
        </div>
    </div>
</div>

<div id="detailModal" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog" style="width: 800px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">User Detail</h4>
            </div>
            <div class="modal-body" id="getUserInfoBox">

            </div>
            <div class="modal-footer" >
                <button type="button" class="btn btn-default" id="close_model" data-dismiss="modal" >Close</button>
                <button data-toggle="modal" data-target="#deleteExtendModal" type="button" class="btn btn-default" >Edit</button>
            </div>
        </div>
    </div>
</div>

<div id="deleteExtendModal" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog" style="width: 800px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Please enter password</h4>
            </div>
            <div class="modal-body">
                <div class="form-group furture-form-style">
                    <label>Password:*</label>
                    <input type="password" class="form-control form-style" id="delete_password_extend" >
                </div>
            </div>
            <div class="modal-footer" >
                <button type="button" class="btn btn-default" id="close_model" data-dismiss="modal" >Close</button>
                <button type="button" class="btn btn-default" onClick="extendCheck()" >Edit</button>
            </div>
        </div>
    </div>
</div>

<div id="editModal" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog" style="width: 800px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">User Edit</h4>
            </div>
            <div class="modal-body" id="model_load_content_edit">

            </div>
            <div class="modal-footer" >
                <button type="button" class="btn btn-default" id="close_model" data-dismiss="modal" >Close</button>
                <button type="button" class="btn btn-default" onClick="validateForm()" >Update</button>
            </div>
        </div>
    </div>
</div>

<script>
    var pass = '<?php echo PASS_TO_DELETE; ?>';
    $(document).on('change', '.filter', function() {
        $('.filter').removeAttr('checked');
        $(this).attr('checked', 'checked');
        var status = $(this).val();
        $('#loading').show();
        $.ajax({
            type: 'post',
            url: "<?php echo base_url();?>" + 'admin/regular_users/getFilterResult/',
            data: {status: status},
            success: function(output) {
                $('#loading').hide();
                var dataTable = $("#sample_2").dataTable();
                dataTable.fnClearTable();
                dataTable.fnDraw();
                dataTable.fnDestroy();
                $("#resultBox").html(output);
                // to reload
                //dataTable.ajax.reload();
                refreshTable();
            }
        });
    });

    var deleteUid;
    var detailUid;
    function setDeleteUid(user_id){
        deleteUid = user_id;
    }

    function extendCheck(){
        if($('#delete_password_extend').val() == pass){
            $('#deleteExtendModal').modal('toggle');
            editUserInfo();
        }else{
            alert('Sorry! Password is wrong.');
        }
    }

    function editUserInfo(){
        var _url = "<?=site_url('admin/regular_users/edit')?>/"+detailUid;
        $.ajax({
            type: "POST",
            url: _url,
            data: {},
            cache: false,
            success: function(result){
                $('#model_load_content_edit').html(result);
                $('#editModal').modal('toggle');
            }
        });
    }

    function deleteAccount(){
        var uid = deleteUid;
        var PASS_TO_DELETE = '<?php echo PASS_TO_DELETE; ?>';
        if($('#delete_password').val() == PASS_TO_DELETE){
            var status = 'R';
            $.ajax({
                type: "POST",
                url: "<?=site_url('admin/regular_users/delete')?>",
                data: {userId:uid},
                cache: false,
                success: function(result){
                    if($.trim(result) == 'success'){
                        $('#tr_'+uid).remove();
                        $('#deleteModal').modal('toggle');
                    }
                },
                error: function (request, status, error) {}
            });
        }else{
            alert('Sorry! Password is wrong.');
        }
    }

    function getUserInfo(_url,uid){
        detailUid = uid;
        $.ajax({
            type: "POST",
            url: _url,
            data: {},
            cache: false,
            success: function(result){
                $('#getUserInfoBox').html(result);
            }
        });
    }

    function addCardNumber(val, userId) {
        $('#loading').show();
        var card = $("#" + val).val();
        if(card == 0 || card == "" || card == null){
            alert("Card value should not be empty or zero");
            return false;
        }
        $.ajax({
            type: 'post',
            url: "<?php echo base_url();?>" + 'admin/regular_users/addCardNumber/' + val,
            data: {card: card, userId: userId},
            success: function (output) {
                $('#loading').hide();
                if (output) {
                    alert("Last four number edited successfully");
                }
                else {
                    alert("Error!");
                }
            }
        });
        //admin_changed
    }
</script>