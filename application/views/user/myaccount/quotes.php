<div class="width-row margin-top-20">
<div class="main_cont">
    <div class="pagetitle margin-bottom-10">
        <h1><?php echo $this->page_name; ?></h1>
    </div>
    <div class="row text-center">
        <label>Your quote expires after 48 hours! Please take action soon to secure your quoted rate! Expired quotes will appear with a RED background color and you will have to resubmit your request. Expired quotes will HAVE QUOTE IDs, but it can only be seen from Admin panel (hide it from Member interface and display it in admin panel).</label>
    </div>
    <div class="form-group col-md-7 hide filter-box">
        <label class="control-label col-md-2" style="padding: 6px 0;">Filter:</label>
        <div class="col-md-9">
            <div class="input-group input-large date-picker input-daterange " data-date="10/11/2012" data-date-format="mm/dd/yyyy">
                <input type="text" class="form-control" id="from" name="from" value="" placeholder="Date From" style="text-align:left;">
                <span class="input-group-addon">
                    to
                </span>
                <input type="text" class="form-control" id="to" name="to" value="" placeholder="Date To" style="text-align:left;">
            </div>
        </div>    
    </div>
    <div class="content_row">
        <table class="table table-striped table-bordered table-hover" id="quotes">
            <thead>
                <tr role="row" class="heading">

                    <th width='15%'>City</th>
                    <th width='15%'>Hotel</th>
                    <th width='15%'>Room type</th>
                    <th width='15%'>Benefits</th>
                    <th width='10%'>Check in</th>
                    <th width='10%'>Check out</th>
                    <th width='5%'>Night s</th>
                    <th width='5%'>Room s</th>
                    <th width='5%'>Adult s</th>
                    <th width='5%'>Children</th>
                    <th width='15%'>Name</th>
                    <th width='15%'></th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div> 
</div>

<div id="myModal_autocomplete" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog" style="width:80%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">View Quote</h4>
            </div>
            <form method="post" id="add_quoterequests" class="form-horizontal"> 
                <div class="portlet-body form">
                    <div class="form-body">
                        
                        <div class="row" style="padding: 5px 20px;">
                            <div class="quoterequest"> 
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">City:</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control city" value="" readonly="true">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Points:</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control points" readonly="true">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Check-in:</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control checkin" readonly="true">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Account ID:</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control accountid" readonly="true">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Check-out:</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control checkout" readonly="true">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Username:</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control username" readonly="true">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Rooms:</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control rooms" readonly="true">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Date of request:</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control dateofrequest" readonly="true">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Adults:</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control adults" readonly="true">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Children:</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control children" readonly="true">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Name on reservation:</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control reservation" readonly="true">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Name of hotel:</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control nameofhotel" readonly="true">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Stars:</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control stars" readonly="true">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Comments:</label>
                                        <div class="col-md-9">
                                            <textarea rows="1" class="form-control comments" readonly="true"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END FORM-->
                    </div>
                </div>
            </form> 
        </div>                                               
        <!--</div>-->
        <!--</div>-->                                            
    </div>
</div>  
</div>