<style type="text/css">

    .color-red{

        color:red;

        font-weight:bold;

    }

    .bold-font{

        font-weight: bold;

    }

    .verif-txt{

        margin: 17px 0;

    }

    .member-msg{

        margin: 17px 0;

    }

</style>

<div class="width-row" style="background-color: rgba(255, 255, 255, 0.74) none repeat scroll 0 0;">

    <div class="main_cont">

        <div class="" style="margin: 10px 0;">
            <p>Dear <?php echo $this->session->userdata['userDetail']['var_fname']." ".$this->session->userdata['userDetail']['var_lname'];?>,</p>
        </div>
        <p>Thank you very much for your payment for your membership! Your payment of <strong> $<?php echo $this->session->userdata['membership']['amount'];?>  </strong>for your membership was successfully charged and your charges will appear as <strong> "WholsaleHtLs8882872307"</strong>. </p>
        <p>
            <span style="text-decoration:; font-weight:bold;">What happens next? </span> Since you used your existing card on file, you don’t have to do anything else. Your account is instantly activated and you can use your account immediately.
        </p>
        <p>Thank you for allowing us to service your travel needs and we hope that you will get to enjoy all the benefits we can provide to you and your friends.</p>

        <p  style="margin: 0 0 60px;">Welcome again to<strong> Wholesale Hotels Group!</strong></p>
    </div>
</div>