<?php
date_default_timezone_set(AMERICA_LOS_ANGLES_TIME_ZONE);
?>
<script>
    var start_date_pt = '<?php echo date('F d, Y H:i:s'); ?>';
    var city_array = new Array;
<?php foreach ($city as $row) { ?>
        //city_array.push('<?php echo trim($row['var_city']) ? trim($row['var_city']) . ',' : " "; ?><?php echo trim($row['var_state']) ? trim($row['var_state']) . ',' : " "; ?><?php echo $row['var_country']; ?>');
<?php } ?>
<?php foreach ($hotels as $row) { ?>
        //city_array.push('<?php echo trim($row['var_hotelname']) ? trim($row['var_hotelname']) . ',' : ''; ?><?php echo trim($row['var_city']) ? trim($row['var_city']) . ',' : ''; ?><?php echo trim($row['var_state']) ? trim($row['var_state']) . ',' : ''; ?><?php echo $row['var_country']; ?>');
<?php } ?>
</script>
<style>
    .select2-default{
        color: #000 !important;
    }
    #quote_request input{
        color: #000 !important;
    }
    .form-control{
        color: #000 !important;
    }
    ::-webkit-input-placeholder{
        color: #000 !important;
    }

    :-moz-placeholder { /* Firefox 18- */
        color: #000 !important;
    }

    ::-moz-placeholder {  /* Firefox 19+ */
        color: #000 !important;
    }

    :-ms-input-placeholder {  
        color: #000 !important;
    }
    .radio input[type="radio"], .radio-inline input[type="radio"], .checkbox input[type="checkbox"], .checkbox-inline input[type="checkbox"]{
        margin-left: 0px !important;
    }
    .datepicker .datepicker-months table{
        width: 150px !important;
    }
    .datepicker .datepicker-years table{
        width: 200px !important;
    }
    .congrat_box .count_rs li{position: relative}
    /*    .count_rs .des::after {
      bottom: -10px;
      color: red;
      content: ",";
      position: absolute;
      right: -13px;
    }*/
    .des { margin-right: 12px !important;}
    .ui-autocomplete a{
        font-size: 12 !important;
    }
    /*.ui-widget-content {
      background: none repeat scroll 0 0 #1b96f0;
      border: 1px solid #aaaaaa;
      color: #222222;
    }
    .ui-menu-item:hover {
      background: none repeat scroll 0 0 #236ba4;
      color: #fff;
    }
    #ui-id-1 a span{
        background-color: #0E2E46 !important;
    }
    #ui-id-1 a{
        color: #fff !important;
         font-size: 14px !important;
    }
    #ui-id-1 {
        font-family: "FuturaStd-Book";
       z-index: 9999;
    }*/
    .bx-controls-direction {
        display: none;
    }
    .bx-wrapper:hover .bx-controls-direction {
        display: block;
    }
    .bx-wrapper {
        max-width: inherit !important;
    }
    .content_box{
        width: 187px !important;
    }
    .control-label.cancelation {
        padding: 12px 0 !important;
        text-align: left !important;
    }
</style>
<?php
$cookesdata = get_cookie('cookesdata');
$coociesarray = json_decode($cookesdata);
//echo $coociesarray->star;
//print_r($coociesarray); exit();
?>
<div class="home_container">
    <div class="width-row">
		<?php include('main_uper.php'); ?>
        <!--
        <div class="challenge_count_box">
            <div class="congrat_box">
                <h1>Take the hotel challenge!</h1>
                               <div class="hotels_text">
                                See how our rates can be Considerably lower than the big sites,<br /> 
                                such as <span>Hotel.com<sup>&copy;</sup></span>, <span>Expedia.com<sup>&copy;</sup></span> ,<span>Booking.com<sup>&copy;</sup></span>or <span>Travelocity.com<sup>&copy;</sup></span>.
                                </div>
                <div class="hotels_text">See how you can save UP TO 40% OFF <br /> compared to the major hotel booking sites!</div>
                <div class="div_separator"></div>
                <h2>Congratulations!</h2>
                <div class="count_text">
                    <span style="color: #ffffff; font-family: futuraextended; font-size: 18px; text-shadow: 1px 2px 0 #000000;">Members of this Site have already saved:</span>
                    <ul class="count_rs">
                        <li class="bg_none"><img src="<?php echo base_url(); ?>public/affilliate_theme/img/dollar1.png" style="width: 20px; height: 38px;"></li>
                        <?php
                        //     print_r($maincounter);
                        $arr1 = str_split($maincounter[0]['int_amount']);
                        for ($i = 0; $i < count($arr1); $i++) {
                            $class = "";

                            if (count($arr1) == 4) {
                                if ($i == 0) {
                                    $class = "class='des'";
                                }
                            }
                            if (count($arr1) == 5) {
                                if ($i == 1) {
                                    $class = "class='des'";
                                }
                            }
                            if (count($arr1) == 6) {
                                if ($i == 2) {
                                    $class = "class='des'";
                                }
                            }
                            if (count($arr1) == 7) {
                                if ($i == 0) {
                                    $class = "class='des'";
                                }
                                if ($i == 3) {
                                    $class = "class='des'";
                                }
                            }
                            if (count($arr1) == 8) {
                                if ($i == 1) {
                                    $class = "class='des'";
                                }
                                if ($i == 4) {
                                    $class = "class='des'";
                                }
                            }
                            if (count($arr1) == 9) {
                                if ($i == 2) {
                                    $class = "class='des'";
                                }
                                if ($i == 5) {
                                    $class = "class='des'";
                                }
                            }
                            echo "<li " . $class . "><span>" . $arr1[$i] . "</span></li>";
                        }
                        ?>

                    </ul>
                </div>
            </div>
            <div class="video_img_box">
                <h2>How does it work?</h2>
                <div class="video_iframe">
                    <img src="<?php echo base_url(); ?>public/affilliate_theme/img/video.jpg">
                    <iframe width="325" height="190" src="https://www.youtube.com/embed/7vv9n6H-iKw?modestbranding=1" frameborder="0" allowfullscreen></iframe>
                </div>  
            </div>
            <div class="clear"></div>
        </div>
        -->


        <!--        <div class="home_challenge_box text-center">
                    <h1>Take the hotel challenge!</h1>
                    <div class="hotels_text">
                        See how our rates can be Considerably lower than the big sites,<br /> 
                        such as <span>Hotel.com</span>, <span>Expedia.com</span> or <span>Travelocity.com</span>.
                    </div>
                </div>
                <div class="video_count_boxes">
                    <div class="video_box">
                        <h2>How does it work?</h2>
                        <div class="video_iframe">
                            <img src="<?php echo base_url(); ?>public/affilliate_theme/img/video.jpg">
                        </div>
                    </div>
                    <div class="count_box">
                        <h2>Congratulations!</h2>
                        <div class="count_text">
                            <span>Users of this site have already saved:</span>
                            <ul class="count_rs">
                                <li class="bg_none"><img src="<?php //echo base_url();   ?>public/affilliate_theme/img/dollar.png"></li>
                                <li class="space"><span>2</span></li>
                                <li><span>0</span></li>
                                <li><span>0</span></li>
                                <li class="space"><span>0</span></li>
                                <li><span>2</span></li>
                                <li><span>0</span></li>
                                <li><span>1</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>-->
        <div class="home_form diff_form">

            <form class="form-horizontal form-bordered" action="#" method="post" id="quote_request" style="padding-top: 2px; ">
                <div class="form-body">
                    <h4 class="requestquotenow">REQUEST A QUOTE NOW!</h4>
                    <div class="col-md-4 home_col_45">
                        <div class="form-group">
                            <label class="control-label col-md-12">City/Hotel</label>

                            <div class="col-md-12">
                                <input type="text" name="city" id="select_city" value="<?php echo $coociesarray->select_city ?>" class="form-control" style="height: 33px; text-align: center;" placeholder="Name of city or hotel">
                            </div>
                        </div>
                        <!--                        <div class="form-group">
                                                            <label class="control-label col-md-12">Name of hotel</label>
                                                            <div class="col-md-12">
                                                                <input type="text" name="citname_hotely" id="select_hotel" class="form-control select_hotel" style="height: 33px;text-align: center;" placeholder="Name of hotel">
                                                            </div>
                                                        </div>-->
                    </div>
                    <div class="col-md-6 home_col_65">
                        <div class="col-md-4 home_col_45">
                            <div class="form-group">
                                <label class="control-label col-md-12">Star rating<a data-original-title="If you did NOT specify the exact hotel you wish to stay at, please specify the star rating of the hotel you would like to find a deal for." data-placement="top" class="tooltips"><sup><i class="fa fa-info-circle"></i></sup></a></label>
                                <div class="col-md-12">
                                    <select id="star" class="select2me form-control input-xlarge star_rating"  style="text-align:center;" name="star_rating">
                                            <!--<option value="<?php echo $coociesarray->star; ?>"><?php echo $coociesarray->star; ?></option>-->
                                        <option value="">Select</option>
                                        <option <?php
                        if ($coociesarray->star == '2 Star') {
                            echo 'selected="selected"';
                        }
                        ?> value="2 STAR">2 Star +</option>
                                        <option <?php
                        if ($coociesarray->star == '2.5 Star') {
                            echo 'selected="selected"';
                        }
                        ?> value="2.5 STAR">2.5 Star +</option>
                                        <option <?php
                        if ($coociesarray->star == '3 Star') {
                            echo 'selected="selected"';
                        }
                        ?> value="3 STAR">3 Star +</option>
                                        <option <?php
                        if ($coociesarray->star == '3.5 Star') {
                            echo 'selected="selected"';
                        }
                        ?> value="3.5 STAR">3.5 star +</option>
                                        <option <?php
                        if ($coociesarray->star == '4 Star') {
                            echo 'selected="selected"';
                        }
                        ?>  value="4 STAR">4 Star +</option>
                                        <option <?php
                                            if ($coociesarray->star == '4.5 Star') {
                                                echo 'selected="selected"';
                                            }
                                            ?>  value="4.5 STAR">4.5 Star +</option>
                                        <option <?php
                                        if ($coociesarray->star == '5 Star') {
                                            echo 'selected="selected"';
                                        }
                                        ?>  value="5 STAR">5 star only</option>
                                    </select>
                                </div>
                            </div>  

                        </div>
                        <div class="col-md-4 home_col_45">
                            <div class="form-group">
                                <label class="control-label col-md-12">Check in</label>
                                <div class="col-md-12">
                                    <input type="text" value="<?php echo $coociesarray->chackin; ?>" name="from" placeholder="Check-in" class="form-control icon-pick check_in" id="check_in" style="text-align: left;">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 home_col_45">
                            <div class="form-group">
                                <label class="control-label col-md-12">Check out</label>
                                <div class="col-md-12">
                                    <input type="text" name="top" value="<?php echo $coociesarray->chackout; ?>" placeholder="Check-out" class="form-control icon-pick check_out" id="check_out" style="text-align: left;">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1 home_col_15 text-center">
                        <div class="form-group">
                            <label class="control-label col-md-12" style="text-align: center;">Nights</label>
                            <div class="col-md-12">
                                <input type="hidden" value="<?php echo $coociesarray->night; ?>" name="nights" id="nights" value="0" readonly="true" class="nights form-control text-center">
                                <span  class="night-text text-center nights"><?php echo $coociesarray->night; ?></span>
                            </div>
                        </div>
                    </div>

                    <div class="clear"></div>

                    <div class="red_boxes" style="display: none;">
                        <div class='form_box_title title_redbox' styled="display: none;">
                            Our Hottest savings in the past 7 days...
                        </div>
                        <div class="sethtml slider1">                            
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="row margintop5">
                        <div class="col-md-10" style="padding: 0 16px;">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-12">Name on reservation</label>
                                    <div class="col-md-6">
                                        <input type="text" placeholder="First name" value="<?php echo $coociesarray->fname ? $coociesarray->fname : $userdetail[0]['var_fname']; ?>" style="text-align: center;" name="nor_fname" id="fname" class="form-control col-md-6 reservation_input">
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" placeholder="Last name" value="<?php echo $coociesarray->lname ? $coociesarray->lname : $userdetail[0]['var_lname']; ?>" style="text-align: center;" name="nor_lname" id="lname" class="form-control col-md-6 reservation_input">
                                    </div>
                                </div>
                            </div>
                            <!--                            <div class="col-md-4">
                                                            
                                                        </div>-->
                            <!--                            <div class="col-md-1" style="padding: 28px 0px 0px; width: 3%;">
                                                            <div class="col-md-12 text-center" style="color:#fff; padding: 0;">OR</div>
                                                        </div>-->
                            <div class="col-md-6" style="padding-right: 5px;">
                                <div class="col-md-4 home_col_45">
                                    <div class="form-group">
                                        <label class="control-label col-md-12">Rooms</label>
                                        <div class="col-md-12">
                                            <select class="select2me form-control input-xlarge rooms" name="rooms" style="text-align:center;">
                                    <!--<option value="<?php echo $coociesarray->room; ?>"><?php echo $coociesarray->room; ?></option>-->
                                                <option <?php
                                        if ($coociesarray->room == '1') {
                                            echo 'selected="selected"';
                                        }
                                        ?> value="1">1</option>
                                                <option <?php
                                                    if ($coociesarray->room == '2') {
                                                        echo 'selected="selected"';
                                                    }
                                                    ?> value="2">2</option>
                                                <option <?php
                                                if ($coociesarray->room == '3') {
                                                    echo 'selected="selected"';
                                                }
                                                ?> value="3">3</option>
                                                <option <?php
                                                if ($coociesarray->room == '4') {
                                                    echo 'selected="selected"';
                                                }
                                                ?> value="4">4</option>
                                                <option <?php
                                                    if ($coociesarray->room == '5') {
                                                        echo 'selected="selected"';
                                                    }
                                                    ?> value="5">5</option>
                                                <option <?php
                                                if ($coociesarray->room == '6') {
                                                    echo 'selected="selected"';
                                                }
                                                ?> value="6">6</option>
                                                <option <?php
                                                if ($coociesarray->room == '7') {
                                                    echo 'selected="selected"';
                                                }
                                                ?> value="7">7</option>
                                                <option <?php
                                                if ($coociesarray->room == '8') {
                                                    echo 'selected="selected"';
                                                }
                                                ?> value="8">8</option>
                                                <option <?php
                                                if ($coociesarray->room == '9') {
                                                    echo 'selected="selected"';
                                                }
                                                ?> value="9">9</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 home_col_45">
                                    <div class="form-group">
                                        <label class="control-label col-md-12">Adults</label>
                                        <div class="col-md-12">
                                            <select class="select2me form-control input-xlarge rooms" name="adults" style="text-align:center;">
                                    <!--<option value="<?php echo $coociesarray->adults; ?>"><?php echo $coociesarray->adults; ?></option>-->
                                                <option <?php
                                                if ($coociesarray->adults == '2') {
                                                    echo 'selected="selected"';
                                                }
                                                ?> value="2">2</option>
                                                <option <?php
                                                    if ($coociesarray->adults == '1') {
                                                        echo 'selected="selected"';
                                                    }
                                                ?> value="1">1</option>
                                                <option <?php
                                                    if ($coociesarray->adults == '3') {
                                                        echo 'selected="selected"';
                                                    }
                                                ?> value="3">3</option>
                                                <option <?php
                                                    if ($coociesarray->adults == '4') {
                                                        echo 'selected="selected"';
                                                    }
                                                    ?> value="4">4</option>
                                                <option <?php
                                                if ($coociesarray->adults == '5') {
                                                    echo 'selected="selected"';
                                                }
                                                ?> value="5">5</option>
                                                <option <?php
                                                if ($coociesarray->adults == '6') {
                                                    echo 'selected="selected"';
                                                }
                                                ?>value="6">6</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 home_col_45">
                                    <div class="form-group">
                                        <label class="control-label col-md-12">Children</label>
                                        <div class="col-md-12">
                                            <select class="select2me form-control input-xlarge rooms" name="children" style="text-align:center;">
                                    <!--<option value="<?php echo $coociesarray->children; ?>"><?php echo $coociesarray->children; ?></option>-->
                                                <option <?php
                                        if ($coociesarray->children == '0') {
                                            echo 'selected="selected"';
                                        }
                                        ?> value="0">0</option>
                                                <option <?php
                                        if ($coociesarray->children == '1') {
                                            echo 'selected="selected"';
                                        }
                                        ?> value="1">1</option>
                                                <option <?php
                                        if ($coociesarray->children == '2') {
                                            echo 'selected="selected"';
                                        }
                                        ?> value="2">2</option>
                                                <option <?php
                                        if ($coociesarray->children == '3') {
                                            echo 'selected="selected"';
                                        }
                                        ?> value="3">3</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12" style="padding: 0; width: 100%;">
                                <div class="form-group" style="margin: 0; width: 100%;">
                                    <label class="control-label col-md-12">Comments</label>
                                    <div class="col-md-12">
                                        <textarea cols="30" rows="5" style="height: 85px; resize: none;font-size: 12px; padding: 6px 10px; color: #000;" id="comments" name="comments" class="form-control"><?php echo $coociesarray->comments; ?></textarea>
<?php
if ($coociesarray->comments != "") {
    if (strpos($coociesarray->comments, 'Any requests?') === false) {

        $commentvalue = 1;
    } else {
        $commentvalue = 0;
    }
} else {
    $commentvalue = 0;
}

// $commentvalue = $coociesarray->comments?'1':'0';
?><input type="hidden" class="commentvalue" name="commentvalue" value="<?php echo $commentvalue; ?>">
                                        <input type="hidden" class="commentvalue" value="0">
                                        <p style="color:#fff;font-size: 12px;" class="control-label cancelation col-md-12">* Cancellation policies between <strong><?php echo SITE_NAME;?><sup>sm</sup></strong> quotes and competitor sites may vary.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-1" style="padding: 0">

                            <div class="form-group" style="">
                                <label style="padding: 7px 27px 8px; margin: 0 0 0 -8px;" class="control-label col-md-10"></label>
                                <div style="padding: 0px; font-size: 12px !important; width: 28%;  min-height: 50px" class="col-md-3">
                                    <label style="padding: 0px ! important;" class="checkbox dis_block"><div class="checker"><span></span></div></label>

                                </div>
                                <div class="col-md-10 form_btn_home text-center">
                                    <a href="" data-toggle="">
                                        <button type="submit" class="default_btn"  style="padding: 30px 30px; text-transform: uppercase; text-shadow:1px 1px 2px #000;">Submit</button></a>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </form>    
        </div>
    </div>
</div>
<div id="myModal_autocomplete" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog" style="width:80%;">

        <div class="login_main_cont margin-top-20" style="width:42%;background:#fff;">               
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="fa fa-times"></i></button>               
            <form class="login-form"  method="post">
                <h3 class="form-title">Please login first!</h3>
                <p>All users requesting deals must login first, so that you can be assiseted</p>
                <p>with priority in case you have made booking on this site already!</p>
                <div class="alert alert-danger display-hide" style="display: none;">
                    <button class="close" data-close="alert"></button>
                    <span>
                        Enter any username and password.
                    </span>
                </div>
                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label col-md-12">Username</label>
                    <div class="col-md-12 margin-bottom-10">
                        <input class="form-control" type="text" id="user" name="username"/>
                    </div>
                </div>
                <div class="form-group ">
                    <label class="control-label col-md-12">Password</label>
                    <div class="col-md-12 margin-bottom-10">
                        <input class="form-control" type="password" id="pass" name="password"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="checkbox col-md-6" style="padding: 8px 10 0 35;">
                        <!--<input type="checkbox" name="remember" value="1"/> Remember me-->
                    </label>
                    <div class="col-md-6">
                        <button type="submit" class="default_btn pull-right" style="padding: 5px 25px;">Login</button>
                    </div>
                </div>
                <div class="form-group">
                    <p><span style="color:red">NEW USER?</span> Sign up in 30 seconds and get an <u><a href="<?php echo base_url(); ?>front/home/member_signup">INSTANT<br> 
                                SIGNUP CREDIT</a></u> from <?php echo DOMAIN_NAME;?></p>
                </div>  
                <!--                    <div class="form-group">
                                        >Forgot your password? <a href>CLICK HERE!</a>
                                    </div>-->
                <div class="forget-password margin-top-20">
                    <h4>Forgot your password ?</h4>
                    <p>
                        no worries, click
                        <a href="javascript:;" id="forget-password">
                            here
                        </a>
                        to reset your password.
                    </p>
                </div>
                <div class="form-group">
                    Don't have an account yet? <a href="<?php echo base_url(); ?>front/home/member_signup">SIGN UP NOW!</a>
                </div>
                <div class="clear"></div>
                <div class="forget-password margin-top-20">
                    <h4>Forgot your password ?</h4>
                    <p>
                        no worries, click
                        <a href="javascript:;" id="forget-password">
                            here
                        </a>
                        to reset your password.
                    </p>
                </div>
            </form>
            <!-- END LOGIN FORM -->
            <!-- BEGIN FORGOT PASSWORD FORM -->
            <form class="forget-form" action="" method="post" style="display: none;">
                <h3>Forget Password ?</h3>
                <p>
                    Enter your e-mail address below to request your password.
                </p>
                <div class="form-group">
                    <div class="input-icon">
                        <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email" id="forgetemail"/>
                    </div>
                </div>
                <div class="form-actions">
                    <button type="button" id="back-btn" class="btn red">Back </button>
                    <button type="submit" class="default_btn pull-right" style="padding: 5px 25px;">Submit</button>
                </div>
            </form>
            <!-- END FORGOT PASSWORD FORM -->
        </div> 

        <!--</div>-->
        <!--</div>-->                                            
    </div>
</div>  


<div aria-hidden="false" role="dialog" class="modal fade in" id="req_success" style="display: none;">
    <div class="success_login_main_cont margin-top-20">
        <button aria-hidden="true" data-dismiss="modal" class="close" style="padding-top: 5px;" type="button"><i class="fa fa-times"></i></button>
        <div class="modal-header" style="display: none;">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="fa fa-times"></i></button>
            <h3 class="form-title">Successfully</h3>
        </div>
        <div style="" class="" style="padding-top: 16px;">
            <p style="padding: 10px">Thank you very much for your quote request. We received it and already working on finding you the best deal! We will get back to you momentarily and you will receive an email notification once your quote is ready.</p>
        </div>
        <div style="padding: 3px;" class="modal-footer">
            <div style="text-align:center">
                <button type="button" style="padding: 7px 20px;" data-dismiss="modal" class="btn default_btn clickme">OK</button>
            </div>
        </div>
    </div> 
</div>
