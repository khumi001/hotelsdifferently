<div class="login_main_cont margin-top-20">
    <form class="login-form"  method="post">
		<h3 class="form-title">MEMBER LOGIN</h3>
                <div class="alert alert-danger display-hide" style="display: none;">
			<button class="close" data-close="alert"></button>
			<span>
				 Enter any username and password.
			</span>
		</div>
		<div class="form-group">
			<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
			<label class="control-label col-md-12">Username</label>
			<div class="col-md-12 margin-bottom-10">
				<input class="form-control" type="text" id="user" name="username" tabindex="2"/>
			</div>
		</div>
		<div class="form-group ">
			<label class="control-label col-md-12">Password</label>
			<div class="col-md-12 margin-bottom-10">
				<input class="form-control" type="password" id="pass" name="password" tabindex="2"/>
			</div>
		</div>
                <div class="form-group">
                    <label class="checkbox col-md-6" style="padding: 8px 10 0 35;">
                        <input type="checkbox" name="remember" value="1" tabindex="2"/> Remember me
                    </label>
                    <div class="col-md-6">
                        <button type="submit" class="default_btn pull-right" style="padding: 5px 25px;" tabindex="2" >Login</button>
                    </div>
                </div>
                <div class="clear"></div>
<!--		<div class="login-options">
			<h4>Or login with</h4>
			<ul class="social-icons">
				<li>
					<a class="facebook" data-original-title="facebook" href="#">
					</a>
				</li>
				<li>
					<a class="twitter" data-original-title="Twitter" href="#">
					</a>
				</li>
				<li>
					<a class="googleplus" data-original-title="Goole Plus" href="#">
					</a>
				</li>
				<li>
					<a class="linkedin" data-original-title="Linkedin" href="#">
					</a>
				</li>
			</ul>
		</div>-->
<div class="clear"></div>
<div class="forget-password margin-top-20" style="padding: 0 15px;">
    <h4 style="font-size: 16px;">Forgot your password?</h4>
			<p>No worries, click&thinsp;<a href="javascript:;" id="forget-password" style="text-decoration: underline; color: #000;">HERE</a>&thinsp;to retrieve your password.
			</p>
		</div>
		
		<div class="create-account" style="padding: 0 15px;">
                    <span style="font-size:12px; display: block; text-align: center;">Don’t have an account yet? SIGN UP NOW AS OUR NEW <br /> </span>
                                <a href="<?php echo base_url(); ?>front/home/member_signup">
					 Member
				</a>
                </div>
	</form>
	<!-- END LOGIN FORM -->
        <!-- BEGIN FORGOT PASSWORD FORM -->
        <form class="forget-form" action="" method="post" style="display: none;">
		<h3>Forget Password ?</h3>
		<p>
			Enter your e-mail address below to request your password.
		</p>
		<div class="form-group">
			<div class="input-icon">
				<input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email" id="forgetemail"/>
			</div>
		</div>
		<div class="form-actions">
			<button type="button" id="back-btn" class="btn red">Back </button>
			<button type="submit" class="default_btn pull-right" style="padding: 5px 25px;">Submit</button>
		</div>
	</form>
	<!-- END FORGOT PASSWORD FORM -->
</div> 
