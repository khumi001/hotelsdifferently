<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <?php
    $title = empty($var_meta_title) ? "Wholesale Hotels Group – Great travel deals for Corporate Travel and Individuals" : $var_meta_title;
    $description = empty($var_meta_description) ? "Get WHOLESALE HOTEL inventories and deals worldwide for Corporate Travel, Non-Profit travel, Government travel, Travel Agents and Individual users." : $var_meta_description;
    $keywords = empty($var_meta_keyword) ? "Wholesale Hotels Group, WHotelsGroup.com, wholesale hotel deals, corporate travel, government travel, non-profit travel, travel agency travel, travel agency hotels, wholesale hotels, Online Travel Agency, best OTAs" : $var_meta_keyword;
    ?>

    <title><?php echo $title ?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta content="<?php echo $description ?>" name="description"/>
    <meta content="<?php echo $keywords ?>" name="keywords"/>
    <meta name="google-site-verification" content="ZBUbYCT9lCwyszTdzXzftAqkYQS8o5O6qP8eGRaiQXk" />
    <meta name="p:domain_verify" content="a29fc0e0d9ae02190ff346cc33d9b2b3"/>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <!--<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>-->
    <link href="<?php echo base_url(); ?>public/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url(); ?>public/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url(); ?>public/affilliate_theme/stylesheets/style.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>public/affilliate_theme/stylesheets/custom.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/assets/plugins/data-tables/DT_bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/assets/plugins/bootstrap-toastr/toastr.min.css"/>
    <!--<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/assets/plugins/bootstrap-datepicker/css/datepicker.css"/>-->
    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- Begin From Controller-->
    <?php
    if (!empty($css)) {
        foreach ($css as $value) {
            ?>
            <link rel="stylesheet" href="<?php echo base_url(); ?>public/affilliate_theme/stylesheets/<?php echo $value; ?>" />
            <?php
        }
    }
    ?>

    <?php
    if (!empty($css_plugin)) {
        foreach ($css_plugin as $value_plugin) {
            ?>
            <link rel="stylesheet" href="<?php echo base_url(); ?>public/affilliate_theme/plugins/<?php echo $value_plugin; ?>" />

            <?php
        }
    }
    ?> 
    <!-- End From Controller -->


    <!-- BEGIN THEME STYLES -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/affilliate_theme/plugins/bootstrap-select/bootstrap-select.min.css" />
    <link href="<?php echo base_url(); ?>public/assets/stylesheets/plugins.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url(); ?>public/assets/stylesheets/custom.css" rel="stylesheet" type="text/css"/>
    <!-- END THEME STYLES -->
    <script type="text/javascript">
        var baseurl = "<?php echo base_url(); ?>";
        var car_search_results = "<?php json_encode($car_list);  ?>";
        console.log(car_search_results);
    </script>


    <link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->