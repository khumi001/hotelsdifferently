<!-- BEGIN FOOTER -->
<footer>
    <div class="width-row row padding-left-right-20 margin-bottom-15">
        <div class="col-md-5 foot_wid">
             <h2 class="foot_title">Information</h2>
            <div class="policy_link">
                <a href="<?php echo base_url();?>user/legal_corner/aboutus">About Us</a>
                <a href="<?php echo base_url();?>user/legal_corner/member">Members</a>
                <a href="<?php echo base_url();?>user/legal_corner/affiliateprogram">Affiliate Program</a>
                <a href="<?php echo base_url();?>affiliate/contact_us">Contact Us</a>
            </div>
        </div>
        <div class="col-md-5 foot_wid">
            <h2 class="foot_title">Legal Corner</h2>
             <div class="policy_link">
                <a href="<?php echo base_url();?>user/legal_corner/terms_condition">Terms and Conditions</a>
                <a href="<?php echo base_url();?>user/legal_corner/privacy_policy">Privacy Policy</a>
                <a href="<?php echo base_url();?>user/legal_corner/property_policy">Intellectual Property Policy</a>
                <a href="<?php echo base_url();?>user/legal_corner/dmca_Policy">DMCA Policy</a>
                <a href="<?php echo base_url(); ?>user/legal_corner/bitcoin_policy">Bitcoin Policy</a>
                <a href="<?php echo base_url();?>user/legal_corner/security_statement">Security Statement</a>
            </div>
        </div>
        <div class="col-md-2 foot_wid">
             <h2 class="foot_title">Follow Us On</h2>
              <div class="sc_links">
                 <a href="https://www.facebook.com/hotelsdifferently" target="_blank" class="fb_link">Facebook</a>
                 <a href="https://twitter.com/hotelsdifferent" class="tw_link" target="_blank">Twitter</a>
                 <a href="https://plus.google.com/115490121712099690825/" class="gp_link">Google+</a>
                 <a href="https://www.pinterest.com/hotelsdifferent/" class="pn_link" target="_blank">Pinterest</a>
             </div>
        </div>
<!--        <div class="foot_4">
            
        </div>-->
        <div class="clear"></div>
    </div>
    <div class="width-row copy-right" style="padding: 10px 0 0 8px;">
        <span style="float:left; margin: 3px 0 0;"> &#169; Copyright 2014-2016 Hotels Differently All Rights Reserved.</span>
       <div class="" style="float:right;">
            <span style=""><img src="<?php echo base_url(); ?>public/assets/img/comodo.png"></span>
            <span style=""><img width="40" height="40" src="<?php echo base_url(); ?>public/assets/img/paypal_without_bg.png"></span>
        </div>
       <div style="clear: both"></div>
    </div>
</footer>
<!-- END FOOTER -->
