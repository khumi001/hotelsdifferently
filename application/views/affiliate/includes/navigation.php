<div class="nav">
    <div class="width-row">
        <ul>
            <li <?php if ($this->page_name == 'Home') { echo 'class="active_menu"';} ?>>
                <a href="<?php echo base_url(); ?>affiliate/home">Home</a>
            </li>
            <li <?php if ($this->page_name == 'Account Info') { echo 'class="active_menu"';} ?>>
                <a href="<?php echo base_url(); ?>affiliate/home/account_info">Account Info</a>
            </li>
            <li <?php if ($this->page_name == 'Banner Link') { echo 'class="active_menu"';} ?>>
                <a href="<?php echo base_url(); ?>affiliate/banner_link">Banner/Link</a>
            </li>
            <li <?php if ($this->page_name == 'Payout Preference') { echo 'class="active_menu"';} ?>>
                <a href="<?php echo base_url(); ?>affiliate/payout_prefrence">Payout Preference</a>
            </li>
            <li <?php if ($this->page_name == 'Statistics') { echo 'class="active_menu"';} ?>>
                <a href="<?php echo base_url(); ?>affiliate/statistics">Statistics</a>
            </li>
            <li <?php if ($this->page_name == 'Payout History') { echo 'class="active_menu"';} ?>>
                <a href="<?php echo base_url(); ?>affiliate/payout_history">Payout History</a>
            </li>
            <li <?php if ($this->page_name == 'Affiliate Guide') { echo 'class="active_menu"';} ?>>
                <a href="<?php echo base_url(); ?>affiliate/affiliate_guide">Affiliate Guide</a>
            </li>
            <li <?php if ($this->page_name == 'Contact Us') { echo 'class="active_menu"';} ?>>
                <a href="<?php echo base_url(); ?>affiliate/contact_us">Contact Us</a>
            </li>
        </ul>
    </div>
</div>