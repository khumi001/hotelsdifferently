<!--<script src="<?php echo base_url(); ?>public/assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>-->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<!--[if IE]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->       
            
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url(); ?>public/assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>public/assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<!-- /IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="<?php echo base_url(); ?>public/assets/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>public/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>public/assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>public/assets/plugins/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/plugins/bootstrap-toastr/toastr.min.js"></script>
<script src="<?php echo base_url(); ?>public/assets/plugins/gritter/js/jquery.gritter.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>public/assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>public/assets/plugins/jquery-idle-timeout/jquery.idletimeout.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>public/assets/plugins/jquery-idle-timeout/jquery.idletimer.js" type="text/javascript"></script>


<script src="<?php echo base_url(); ?>public/assets/javascripts/core/app.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>public/assets/javascripts/core/datatable.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/plugins/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/plugins/data-tables/DT_bootstrap.js"></script>
<!--<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/javascripts/common_function.js"></script>-->
<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
<!--<script type='text/javascript' src='http://artofyoga.sg/wp-content/themes/artofyoga/js/additional-methods.min.js'></script>-->
<script type="text/javascript" src="<?php echo base_url(); ?>public/affilliate_theme/plugins/ajaxfileupload.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/affilliate_theme/plugins/jquery.form.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/affilliate_theme/javascripts/affiliate_common.js"></script>
<script src="<?php echo base_url(); ?>public/affilliate_theme/javascripts/custom/ui-idletimeout.js"></script>

 <?php
    if (!empty($js_plugin)) {
        foreach ($js_plugin as $value) {
            ?>
            <script src="<?php echo base_url(); ?>public/affilliate_theme/plugins/<?php echo $value ?>" type="text/javascript"></script>
            <?php
        }
    }
    ?>
            

     


 <?php
    if (!empty($js)) {
        foreach ($js as $value) {
            ?>
            <script src="<?php echo base_url(); ?>public/affilliate_theme/javascripts/<?php echo $value ?>" type="text/javascript"></script>
            <?php
        }
    }
    ?>
            <script id="remove_js" src="<?php echo base_url(); ?>public/assets/javascripts/core/datatable_apply.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<script>
jQuery(document).ready(function() {    
   App.init();
       UIIdleTimeout.init();
 <?php
    if (!empty($init)) {
        foreach ($init as $value) {
            echo $value.';' ;
        }
    }
    ?>   
});
</script>