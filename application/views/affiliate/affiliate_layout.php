<html>
    <?php echo $this->load->view('affiliate/includes/header'); ?>
    <body>
        <?php echo $this->load->view('affiliate/includes/body_header'); ?>
        <?php echo $this->load->view('affiliate/includes/navigation'); ?>
        <div class="clear"></div>
        <div id="result_container">
            <div class="page_container">
                <div class="width-row margin-top-10">
                    <div class="container">
                         <?php // echo $this->load->view('affiliate/includes/breadcrumbs'); ?>
                        <?php echo $this->load->view($page); ?>
                    </div>
                </div>   
            </div>
        </div>   
        <?php echo $this->load->view('affiliate/includes/body_footer'); ?>
        <?php echo $this->load->view('affiliate/includes/footer'); ?>
    </body>
</html>
