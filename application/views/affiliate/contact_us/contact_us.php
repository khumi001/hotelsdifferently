<div class="main_cont">
    <div class="pagetitle margin-bottom-10">
        <h1><?php echo $this->page_name; ?></h1>
    </div> 
    <div class="content_row">

        <form method="post" id="send_mail" class="form-horizontal form-bordered" action="#">

            <div class="form-body">

                <div class="row">
                    <div class="col-md-6">

                        <div class="form-group">
                            <label class="control-label col-md-4">First name:</label>
                            <div  class="col-md-4">
                                <div style="padding: 10px 10px 0;">
                                    <?php echo $acc_info['var_fname']; ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">Last name:</label>
                            <div  class="col-md-4">
                                <div style="padding: 10px 10px 0;">
                                    <?php echo $acc_info['var_lname']; ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">Account number:</label>
                            <div  class="col-md-4">
                                <div style="padding: 10px 10px 0;">
                                    <?php echo $acc_info['var_accountid']; ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">Email address:</label>
                            <div  class="col-md-8">
                                <input class="form-control" name="email_add" value="<?php echo $acc_info['var_email']; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">Topic:</label>
                            <div  class="col-md-8">
                                <select class="form-control input-xlarge select2me" name="topic" placeholder="Select...">
                                    <option value="">Please Select</option>
                                    <option value="Technical / Website Issues">Technical / Website Issues</option>
                                    <option value="Payout / Commision">Payout / Commision</option>
                                    <option value="other">Other</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">Response:</label>
                            <div  class="col-md-8">
                                <select class="form-control input-xlarge select2me" name="response" placeholder="">
                                    <option value="">Please Select</option>
                                    <option value="yes">Yes</option>
                                    <option value="no">No</option>
                                </select>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-6 text-center">

                        <div class="form-group">
                            <label class="control-label col-md-12" style="text-align: center;">Your Message to us:</label>
                            <div  class="col-md-12">
                                <textarea class="form-control " id="count_char" name="message" style="height:192px;"></textarea>
                                <p id="counter" class="text-left" style="display: inline-block; float: left;">1000 Characters left</p>
                                <button type="submit" class="default_btn" style="padding: 25px 50px; display: inline-block; float: right; margin-top: 2px; position:absolute; right:15px; bottom:-25px;">Send</button>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-12" style="padding-top:40px;">
                        <div class="col-md-4"></div>
                        <div  class="col-md-4 text-center">
                             <h4><b> Our Mailing Address: </b></h4>
                                <b><?php echo SITE_NAME;?>, LLC.</b><br/>
                                3651 Lindell Road, D141<br/>
                                Las Vegas, Nevada, 89103<br/>
                                United States of America<br/>

                        </div>
                        <div class="col-md-4"></div>
                    </div>
                </div>                
            </div>    
        </form>
    </div>
</div>    
