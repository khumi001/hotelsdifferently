<div class="main_cont">
    <div class="pagetitle margin-bottom-10">
        <h1><?php echo $this->page_name; ?></h1>
    </div>  
    <div class="text-center margin-bottom-20">
        <iframe width="560" height="315" src="https://www.youtube.com/embed/5Ni7XpDppVk?modestbranding=1" frameborder="0" allowfullscreen></iframe>
    </div>
    <div class="text-left">
        <p>Thank you for your interest in becoming our Affiliate and <b>CONGRATULATIONS</b> on enrolling. Now that you have enrolled, I am sure you would like to know how our Affiliate program works and also to hear why our Affiliate program is <b>ONE OF THE BEST</b> and <b>MOST REWARDING</b> right now.</p>
 
        <p>Any new signups you refer will have your referral number on their future bookings, therefore if you have someone signup once, every time that Member uses that account to make a booking, you will receive commission for as long as they pay for bookings. That way, we not only reward you for referring new clients but we will always reward you:  As long as that previous client you referred to us continues to make bookings with Us from the account they used to sign up from your link, you CONTINUE to receive a commission each and every time they make payments. This can provide you a nice paycheck for the lifetime of that specific account. The more signups you refer, the more money you can make!</p>
        
        <p>The major hotel booking sites take advantage of Affiliates but we prefer to consider them as business partners. Once you refer them a new client, the client will no longer use your link to make bookings, he will just keep on going back to the Site to which you referred them to make a booking &#8211; and then you will no longer receive any compensation. We, at <b>HotelsDifferently<sup>sm</sup>;</b> reward you for as long as the referred person makes bookings with us. </p>

        <p> <b>Our referral program works the following way:</b> </p>
                <ul type="square">
                    <li>5% commission up to $9,999 / month. </li>
                    <li>5.5% commission between $10,000-$49,999 / month!</li>
                    <li>6% commission ABOVE $50,000 / month!</li>
                </ul>
                <p>You have the option whether you wish to be paid via PayPal<sup>&#174;</sup> or Western Union<sup>&#174;</sup>. 
                    On top of that, we have annual incentives that we give out to our very best Affiliates (such as luxurious hotel stays and/or flights paid by <b>HotelsDifferently<sup>sm</sup>;</b>). Be our TOP AFFILIATE, MAKE MONEY and get rewarded with LUXURIOUS GIVEAWAYS!!!
                </p>
                <p>Thank you for watching our video and we very much look forward to making money with you.</p>
                
                <p><b>HotelsDifferently<sup>sm</sup>;</b><br>
                <span style="font-size: 10px;"><b>Take the hotel challenge!</b><span/></p>
    </div>
</div>