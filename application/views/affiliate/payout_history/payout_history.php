<div class="main_cont">
    <div class="pagetitle margin-bottom-10">
        <h1><?php echo $this->page_name; ?></h1>
    </div>
    <div class="form-group col-md-7 hide filter-box">
        <!--<label class="control-label col-md-2" style="padding: 6px 0;width: 67px;">Filter By :</label>-->
        <div class="col-md-9" style="padding: 0px;">
            <div class="input-group input-large date-picker input-daterange " data-date="10/11/2012" data-date-format="mm/dd/yyyy">
                <input type="text" class="form-control" id="from" name="from" value="" placeholder="Date From" style="text-align:left;">
                <span class="input-group-addon">
                    to
                </span>
                <input type="text" class="form-control" id="to" name="to" value="" placeholder="Date To" style="text-align:left;">
            </div>
        </div>    
    </div>
    <div class="content_row">
        <table class="table table-striped table-bordered table-hover" id="payout_history">
            <thead>
                <tr role="row" class="heading" style="text-align: center;">
                    <th width='15%' style="text-align: center;">Created Date</th>
                    <th width='15%' style="text-align: center;">Account ID</th>
                    <th width='20%' style="text-align: center;">Email</th>
                    <th width='15%' style="text-align: center;">Amount</th>
                    <th width='15%' style="text-align: center;">Payout method</th>
                    <th width='28%' style="text-align: center;">Transaction ID</th>
                </tr>
            </thead>
            <tbody></tbody>
            <tfoot>
                <tr>
                    <th></th>
                    <th></th>
                    <th class="text-right">Total:</th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
            </tfoot>
        </table>
    </div> 
    
</div>
