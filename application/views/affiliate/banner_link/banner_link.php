<style type="text/css">
    .banner{
        text-align: center; margin: 0 auto;
    }
    .banner-1{
        width: 300px; padding: 18px 0px; 
    }
    .banner-2{
        width: 468px; padding: 36px 0px; 
    }
    .banner-3{
        width: 100%; padding: 22px 0px; 
    }
    .banners .banner-code textarea{
        height: 160px; width: 100%;
    }
    .banner-size {
        margin: 5px 0 0;
        display: block;
    }
</style>
<div class="main_cont">
    <div class="pagetitle margin-bottom-10">
        <h1><?php echo $this->page_name; ?></h1>
    </div>
    <p>You can retrieve your link/banners to promote our Site and to start earning commissions!</p>

    <div class="row banners">
        <div class="form-group col-md-12 text-center margin-bottom-10">
            <label class="control-label col-md-5 text-right" style="padding: 0;">Your customized referral link<a class="tooltips" data-original-title="Share this link everywhere to raise your chances of earning commission!" daa-position-top="top"><sup><i class="fa fa-info-circle"></i></sup></a> :</label>
            <div class="col-md-6 text-left" style="padding: 0 5px;">
                <strong><a href="javascript:;" style="color: #ff0000;"><?= base_url(); ?>front/home/member_signup/<?= $refer_link['var_accountid']; ?></a></strong>
            </div>
        </div>

        <div class="margin-top-15 col-md-12">
            <div class="col-md-8">
                <div class="banner-1 banner">
                    <a href="<?= base_url(); ?>front/home/member_signup/<?= $refer_link['var_accountid']; ?>">
                    <!--<a href="javascript:;">-->
                        <img src="<?php echo base_url(); ?>public/affilliate_theme/img/banner1.png" width="300" height="100" />
                    </a>
                    <span class="banner-size">(300 * 100)</span>
                </div> 
            </div> 
            <div class="col-md-4">
                <div class="banner-code">
                    <textarea class="form-control">
                        <a href="<?= base_url(); ?>front/home/member_signup/<?= $refer_link['var_accountid']; ?>">
                        <img src="<?php echo base_url(); ?>public/affilliate_theme/img/banner1.png" width="300" height="100" />
                    </a>
                    </textarea>
                </div>
            </div> 
        </div>
        <div class="margin-top-15 col-md-12">
            <div class="col-md-8">
                <div class="banner-2 banner">
                    <a href="<?= base_url(); ?>front/home/member_signup/<?= $refer_link['var_accountid']; ?>">
                        <img src="<?php echo base_url(); ?>public/affilliate_theme/img/banner3.png" width="468" height="60" />
                    </a>
                    <span class="banner-size">(468 * 60)</span>
                </div> 
            </div> 
            <div class="col-md-4">
                <div class="banner-code">
                    <textarea class="form-control">
                        <a href="<?= base_url(); ?>front/home/member_signup/<?= $refer_link['var_accountid']; ?>">
                        <img src="<?php echo base_url(); ?>public/affilliate_theme/img/banner3.png" width="468" height="60" />
                    </a>
                    </textarea>
                </div>
            </div> 
        </div>
        <div class="margin-top-15 col-md-12">
            <div class="col-md-8">
                <div class="banner-3 banner">
                    <a href="<?= base_url(); ?>front/home/member_signup/<?= $refer_link['var_accountid']; ?>">
                        <img src="<?php echo base_url(); ?>public/affilliate_theme/img/banner2.png" width="100%" height="90" />
                    </a>
                    <span class="banner-size">(728 * 90)</span>
                </div> 
            </div> 
            <div class="col-md-4">
                <div class="banner-code">
                    <textarea class="form-control">
                        <a href="<?= base_url(); ?>front/home/member_signup/<?= $refer_link['var_accountid']; ?>">
                        <img src="<?php echo base_url(); ?>public/affilliate_theme/img/banner2.png" width="100%" height="90" />
                    </a>
                    </textarea>
                </div>
            </div> 
        </div>
    </div>


<!--    <div class="row">
        <div class="form margin-top-15">
            <div class="form-group col-md-12 text-center margin-bottom-10">
                <label class="control-label col-md-5 text-right" style="padding: 0;">Your customized referral link<a class="tooltips" data-original-title="Share this link everywhere to raise your chances of earning commission!" daa-position-top="top"><sup><i class="fa fa-info-circle"></i></sup></a> :</label>
                <div class="col-md-6 text-left" style="padding: 0 5px;">
                    <strong><a href="<?= base_url(); ?>front/home/member_signup/<?= $refer_link['var_accountid']; ?>" style="color: #ff0000;"><?= base_url(); ?>front/home/member_signup/<?= $refer_link['var_accountid']; ?></a></strong>
                </div>
            </div>
            <div class="form-group col-md-12">
                <label class="control-label col-md-12"></label>
                <div class="col-md-4 text-center">
                    <img class="img-responsive" src="<?php echo base_url(); ?>public/affilliate_theme/img/banner_80.png" style="margin:0 auto;">
                    (80 * 80)<br />    
                    <br />    
                    <label class="control-label">Referral link: <a class="tooltips" data-placement="top" data-original-title="Share this link everywhere to raise your chances of earning commission!"><sup><i class="fa fa-info-circle"></i></sup></a></label>
                    <textarea class="form-control" style="height: 180px;"><a href="www.HotelsDifferently.com/ref=238742" target="_blank">
                            <img src="<?php echo base_url(); ?>public/affilliate_theme/img/banner_80.jpg" />
                        </a>
                    </textarea>
                </div>
                <div class="col-md-4 text-center">
                    <img class="img-responsive" src="<?php echo base_url(); ?>public/affilliate_theme/img/banner_22.gif"  style="margin:0 auto;" >
                    <object width="290" height="205" data="<?php echo base_url(); ?>public/affilliate_theme/img/animationfile.swf"></object>

                    (310 * 218)<br />   
                    <br />    
                  <label class="control-label">Referral link: <a class="tooltips" data-placement="top" data-original-title="Share this link everywhere to raise your chances of earning commission!"><sup><i class="fa fa-info-circle"></i></sup></a></label>
                    <textarea class="form-control" style="height: 180px;">
<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="310" height="218" id="img/animationfile" align="middle">
				<param name="movie" value="<?php echo base_url(); ?>public/affilliate_theme/img/animationfile.swf" />
				<param name="quality" value="high" />
				<param name="bgcolor" value="#ffffff" />
				<param name="play" value="true" />
				<param name="loop" value="true" />
				<param name="wmode" value="window" />
                                <param name="FlashVars" value="param1=http://multimediaxperts.com" />
				<param name="scale" value="showall" />
				<param name="menu" value="true" />
				<param name="devicefont" value="false" />
				<param name="salign" value="" />
				<param name="allowScriptAccess" value="sameDomain" />
				[if !IE]>
				<object type="application/x-shockwave-flash" data="<?php echo base_url(); ?>public/affilliate_theme/img/animationfile.swf" width="310" height="218">
					<param name="movie" value="<?php echo base_url(); ?>public/affilliate_theme/img/animationfile.swf" />
					<param name="quality" value="high" />
					<param name="bgcolor" value="#ffffff" />
					<param name="play" value="true" />
					<param name="loop" value="true" />
                                        <param name="FlashVars" value="param1=http://multimediaxperts.com" />
					<param name="wmode" value="window" />
					<param name="scale" value="showall" />
					<param name="menu" value="true" />
					<param name="devicefont" value="false" />
					<param name="salign" value="" />
					<param name="allowScriptAccess" value="sameDomain" />
				<![endif]
					<a href="http://www.adobe.com/go/getflash">
						<img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />
					</a>
				[if !IE]>
				</object>
				<![endif]
			</object>
                    </textarea>
                </div>
                <div class="col-md-4 text-center">
                    <img class="img-responsive" src="<?php echo base_url(); ?>public/affilliate_theme/img/banner_11.gif"  style="margin:0 auto;">
                    (600 * 300)<br /> 
                    <br />    
                   <label class="control-label">Referral link: <a class="tooltips" data-placement="top" data-original-title="Share this link everywhere to raise your chances of earning commission!"><sup><i class="fa fa-info-circle"></i></sup></a></label>
                    <textarea class="form-control" style="height: 180px;"><a href="www.HotelsDifferently.com/ref=238742" target="_blank">
                            <img src="<?php echo base_url(); ?>public/affilliate_theme/img/banner_11.gif">
                        </a>
                    </textarea>
                </div>
            </div>
        </div>
    </div>-->
</div>
