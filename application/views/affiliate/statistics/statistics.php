<style>
    .resetbutton {
        background: none repeat scroll 0 0 #fff;
        border: 2px solid #ccc;
        border-radius: 3px;
        height: 33px;
        padding: 0 10px;
    }
</style>

<div class="main_cont">
    <div class="pagetitle margin-bottom-10">
        <h1><?php echo $this->page_name; ?></h1>
    </div>
    <div class="text-left">
        Here you can track your earning progress! For detailed information on how you earn money by being our affiliate, please click <b><a href="<?php echo base_url() . 'affiliate/affiliate_guide'; ?>" style="color:#ff0000;">HERE!</a></b>
    </div>
    <h3 class="sub_title">Pending Transactions</h3>
    <div class="form-group col-md-7 hide filter-box">
        <div class="col-md-9" style="padding-left: 0px">
            <div class="input-group input-large date-picker input-daterange " data-date="10/11/2012" data-date-format="mm/dd/yyyy">
                <input type="text" class="form-control date-filter" id="from" name="from" value="" placeholder="Date From" style="text-align:left; ">
                <span class="input-group-addon">
                    to
                </span>
                <input type="text" class="form-control date-filter" id="to" name="to" value="" placeholder="Date To" style="text-align:left;">
            </div>
        </div>   
        <div class="col-md-3">
            <input type="button" class="resetbutton reset1" value="Reset">
        </div>
    </div>
    <div class="transactions_table">   
        <table class="table table-striped table-bordered table-hover" id="pending_transaction">
            <thead>
                <tr role="row" class="heading">
                    <th width=''>Confirmation number</th>
                    <th width=''>Booking Date</th>
                    <th width=''>Amount</th>
                    <th width=''>Commission</th>
                    <th width=''>Release date<a class="tooltips" data-placement="top" data-original-title="Your commission will be posted around this date.">
                            <sup>
                                <i class="fa fa-info-circle"></i>
                            </sup>
                        </a></th>
                </tr>
            </thead>
            <tbody></tbody>
            <tfoot>
                <tr>
                    <th>Total:</th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
            </tfoot>
        </table>
    </div>
    
    <br/>
    
    <h3 class="sub_title">Chargeback Transactions</h3>
    <div class="form-group col-md-7 hide filter-box1">
        <!--<label class="control-label col-md-2" style="padding: 6px 0;">Filter :</label>-->
        <div class="col-md-9" style="padding-left: 0px">
            <div class="input-group input-large date-picker input-daterange " data-date="10/11/2012" data-date-format="mm/dd/yyyy">
                <input type="text" class="form-control date-filter1" id="from1" name="from1" value="" placeholder="Date From" style="text-align:left;">
                <span class="input-group-addon">
                    to
                </span>
                <input type="text" class="form-control date-filter1" id="to1" name="to1" value="" placeholder="Date To" style="text-align:left;">
            </div>
        </div>   
        <div class="col-md-3">
            <input type="button" class="resetbutton reset2" value="Reset">
        </div>
    </div>
    <div class="transactions_table">   
        <table class="table table-striped table-bordered table-hover" id="charjback_transaction">
            <thead>
                <tr role="row" class="heading">
                    <th width=''>Confirmation number</th>
                    <th width=''>Chargeback Date</th>
                    <th width=''>Amount</th>
                    <th width=''>Note</th>
                </tr>
            </thead>
            <tbody></tbody>
            <tfoot>
                <tr>
                    <th>Total:</th>
                    <th></th>
                    <th></th>
                    <th></th>

                </tr>
            </tfoot>
        </table>
    </div>
        <div class="note_text alert alert-danger margin-top-20">
            NOTE: Transactions will be posted 90 Days AFTER the stay. Posted transactions can be found in the "Payout History" menu.
        </div>
</div>