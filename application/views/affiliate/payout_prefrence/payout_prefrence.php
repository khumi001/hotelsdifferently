<div class="main_cont">
    <div class="pagetitle margin-bottom-10">
        <h1><?php echo $this->page_name; ?></h1>
    </div>
    <div class="form general_form_payout">
        <h3 class="sub_title">Personal Information</h3>
        <form method="post" id="edit_account" class="form-horizontal form-bordered" action="<?php echo base_url(); ?>affiliate/home/account_info">
            <div class="form-body"> 
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4 text-left paout_label">First name:</label>
                            <div  class="col-md-6">
                                <!--<input type="text" class="form-control" name="fname" readonly="true" value="<?php echo $acc_info['var_fname']; ?>">-->
                                 <div style="padding: 10px 0 0;">
                                    <?php echo $acc_info['var_fname']; ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 text-left paout_label">Email:</label>
                            <div  class="col-md-6">
                                <input type="text" name="email" class="form-control" value="<?php echo $acc_info['var_email']; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4 text-left paout_label">Last name:</label>
                            <div  class="col-md-6">
                                <!--<input type="text" class="form-control" name="lname" readonly="true" value="<?php echo $acc_info['var_lname']; ?>">-->
                                <div style="padding: 10px 0 0;">
                                   <?php echo $acc_info['var_lname']; ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <?php 
                                $num =  explode('-',$acc_info['var_phone']);
//                                  print_r($num); exit();
                            ?>
                            <label class="control-label col-md-4">Phone number: <a data-original-title="Optional field" data-placement="top" class="tooltips"><sup><i class="fa fa-info-circle"></i></sup></a></label>
                            <div class="col-md-2">
                                <input type="text" class="form-control" value="<?php echo $num[0]; ?>" name="phone1">
                               
                            </div>
                            <div style="" class="col-md-3">
                                <input type="text" value="<?php echo $num[1]; ?>" class="form-control" name="phone2" style="margin-left: -20px; width:145%;">
                               
                            </div>
                            <div style="" class="col-md-3">
                                <input type="text" value="<?php echo $num[2]; ?>" class="form-control" name="phone3">
                               
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4"></label>
                            <div style="text-align: center;font-size: 12px;" class="col-md-2">
                                <span>Country</span>
                            </div>
                            <div style="text-align: center;font-size: 12px;" class="col-md-3">
                               <span>Number</span>
                            </div>
                            <div style="text-align: center;font-size: 12px;" class="col-md-3">
                               <span>Extension</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 

        </form>
   
    <div class="divider_two_div"></div>
 <form method="post" id="edit_paymentinfo" class="form-horizontal form-bordered payoutprefrance" action="">
    <h3 class="sub_title">Payment Information</h3>
        <div class="col-md-6 right-border">
            <div class="text-center">
                <img class="pay_img" width="200" src="<?php echo base_url(); ?>public/affilliate_theme/img/paypal.png" >
            </div>
            <!--<form method="post" id="paypal_info" class="form form-horizontal form-bordered text-center" action="#">-->
                <div class="form-body"> 
                    <div class="row">
                        <div class="col-md-9" style="margin:0 0 0 56px;">

                            <div class="form-group">
                                <label class="control-label col-md-12 text-left paout_label">Paypal&#8482; email:</label>

                                <div  class="col-md-12">
                                    <input type="text" id="myinput" class="form-control" name="pay_email" value="<?php echo $payment[0]['var_paypalemail']; ?>">
                                </div>
                            </div>

                        </div>
                        <div class="col-md-9 margin-bottom-20" style="margin-left: 56px;">Please take note of the transaction costs charged by PayPal&#8482; and be sure to take into account the delivery time, both of which are subject to PayPal's legal agreements and policies.</div>    
                    </div>
                </div> 

            <!--</form>-->


        </div>
        <div class="col-md-6">
            <div class="text-center">
                <img class="pay_img" width="200" src="<?php echo base_url(); ?>public/affilliate_theme/img/western.png" >
            </div>
            <!--<form method="post" id="western_union_form" class="form form-horizontal form-bordered union_form" action="#">-->
                <div class="form-body"> 
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-12 text-left paout_label">Full name: <a class="tooltips" data-placement="top" data-original-title="Must be exactly the same as the ID you will be using to receive your funds in person"><sup><i class="fa fa-info-circle"></i></sup></a></label>
                                <div  class="col-md-12">
                                    <input type="text" class="form-control" id="full_name" name="full_name" value="<?php echo $payment[0]['var_fullname']; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-12 text-left paout_label">Phone number: <a class="tooltips" data-placement="top" data-original-title="Please include your country code."><sup><i class="fa fa-info-circle"></i></sup></a></label>
                                <div  class="col-md-12">
                                    <input type="text" class="form-control" id="phone_no"  maxlength="12" name="phone_no" value="<?php echo $payment[0]['var_phonenumber']; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-12 text-left paout_label">Country:</label>
                                <div  class="col-md-12">
                                        <select name="country" id="country_list" class="form-control select2me country_list">
                                                <option value="">--Select Country--</option>
                                                <!--<option value="AF" selected="selected"><?php echo $payment[0]['var_country']; ?></option>-->
                                                <option <?php if($payment[0]['var_country'] == 'Afghanistan'){ echo 'selected="selected"';}?> value="Afghanistan">Afghanistan</option>
                                                <option <?php if($payment[0]['var_country'] == 'Albania'){ echo 'selected="selected"';}?> value="Albania">Albania</option>
                                                <option <?php if($payment[0]['var_country'] == 'Algeria'){ echo 'selected="selected"';}?> value="Algeria">Algeria</option>
                                                <option <?php if($payment[0]['var_country'] == 'American Samoa'){ echo 'selected="selected"';}?> value="American Samoa">American Samoa</option>
                                                <option <?php if($payment[0]['var_country'] == 'Andorra'){ echo 'selected="selected"';}?> value="Andorra">Andorra</option>
                                                <option <?php if($payment[0]['var_country'] == 'Angola'){ echo 'selected="selected"';}?> value="Angola">Angola</option>
                                                <option <?php if($payment[0]['var_country'] == 'Anguilla'){ echo 'selected="selected"';}?> value="Anguilla">Anguilla</option>
                                                <option <?php if($payment[0]['var_country'] == 'Antarctica'){ echo 'selected="selected"';}?> value="Antarctica">Antarctica</option>
                                                <option <?php if($payment[0]['var_country'] == 'Argentina'){ echo 'selected="selected"';}?> value="Argentina">Argentina</option>
                                                <option <?php if($payment[0]['var_country'] == 'Armenia'){ echo 'selected="selected"';}?> value="Armenia">Armenia</option>
                                                <option <?php if($payment[0]['var_country'] == 'Aruba'){ echo 'selected="selected"';}?> value="Aruba">Aruba</option>
                                                <option <?php if($payment[0]['var_country'] == 'Australia'){ echo 'selected="selected"';}?> value="Australia">Australia</option>
                                                <option <?php if($payment[0]['var_country'] == 'Austria'){ echo 'selected="selected"';}?> value="Austria">Austria</option>
                                                <option <?php if($payment[0]['var_country'] == 'Azerbaijan'){ echo 'selected="selected"';}?> value="Azerbaijan">Azerbaijan</option>
                                                <option <?php if($payment[0]['var_country'] == 'Bahamas'){ echo 'selected="selected"';}?> value="Bahamas">Bahamas</option>
                                                <option <?php if($payment[0]['var_country'] == 'Bahrain'){ echo 'selected="selected"';}?> value="Bahrain">Bahrain</option>
                                                <option <?php if($payment[0]['var_country'] == 'Bangladesh'){ echo 'selected="selected"';}?> value="Bangladesh">Bangladesh</option>
                                                <option <?php if($payment[0]['var_country'] == 'Barbados'){ echo 'selected="selected"';}?> value="Barbados">Barbados</option>
                                                <option <?php if($payment[0]['var_country'] == 'Belarus'){ echo 'selected="selected"';}?> value="Belarus">Belarus</option>
                                                <option <?php if($payment[0]['var_country'] == 'Belgium'){ echo 'selected="selected"';}?> value="Belgium">Belgium</option>
                                                <option <?php if($payment[0]['var_country'] == 'Belize'){ echo 'selected="selected"';}?> value="Belize">Belize</option>
                                                <option <?php if($payment[0]['var_country'] == 'Benin'){ echo 'selected="selected"';}?> value="Benin">Benin</option>
                                                <option <?php if($payment[0]['var_country'] == 'Bermuda'){ echo 'selected="selected"';}?> value="Bermuda">Bermuda</option>
                                                <option <?php if($payment[0]['var_country'] == 'Bhutan'){ echo 'selected="selected"';}?> value="Bhutan">Bhutan</option>
                                                <option <?php if($payment[0]['var_country'] == 'Bolivia'){ echo 'selected="selected"';}?> value="Bolivia">Bolivia</option>
                                                <option <?php if($payment[0]['var_country'] == 'Bosnia and Herzegowina'){ echo 'selected="selected"';}?> value="Bosnia and Herzegowina">Bosnia and Herzegowina</option>
                                                <option <?php if($payment[0]['var_country'] == 'Botswana'){ echo 'selected="selected"';}?> value="Botswana">Botswana</option>
                                                <option <?php if($payment[0]['var_country'] == 'Bouvet Island'){ echo 'selected="selected"';}?> value="Bouvet Island">Bouvet Island</option>
                                                <option <?php if($payment[0]['var_country'] == 'Brazil'){ echo 'selected="selected"';}?> value="Brazil">Brazil</option>
                                                <option <?php if($payment[0]['var_country'] == 'British Indian Ocean Territory'){ echo 'selected="selected"';}?> value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                                                <option <?php if($payment[0]['var_country'] == 'Brunei Darussalam'){ echo 'selected="selected"';}?> value="Brunei Darussalam">Brunei Darussalam</option>
                                                <option <?php if($payment[0]['var_country'] == 'Bulgaria'){ echo 'selected="selected"';}?> value="Bulgaria">Bulgaria</option>
                                                <option <?php if($payment[0]['var_country'] == 'Burkina Faso'){ echo 'selected="selected"';}?> value="Burkina Faso">Burkina Faso</option>
                                                <option <?php if($payment[0]['var_country'] == 'Burundi'){ echo 'selected="selected"';}?> value="Burundi">Burundi</option>
                                                <option <?php if($payment[0]['var_country'] == 'Cambodia'){ echo 'selected="selected"';}?> value="Cambodia">Cambodia</option>
                                                <option <?php if($payment[0]['var_country'] == 'Cameroon'){ echo 'selected="selected"';}?> value="Cameroon">Cameroon</option>
                                                <option <?php if($payment[0]['var_country'] == 'Canada'){ echo 'selected="selected"';}?> value="Canada">Canada</option>
                                                <option <?php if($payment[0]['var_country'] == 'Cape Verde'){ echo 'selected="selected"';}?> value="Cape Verde">Cape Verde</option>
                                                <option <?php if($payment[0]['var_country'] == 'Cayman Islands'){ echo 'selected="selected"';}?> value="Cayman Islands">Cayman Islands</option>
                                                <option <?php if($payment[0]['var_country'] == 'Central African Republic'){ echo 'selected="selected"';}?> value="Central African Republic">Central African Republic</option>
                                                <option <?php if($payment[0]['var_country'] == 'Chad'){ echo 'selected="selected"';}?> value="Chad">Chad</option>
                                                <option <?php if($payment[0]['var_country'] == 'Chile'){ echo 'selected="selected"';}?> value="Chile">Chile</option>
                                                <option <?php if($payment[0]['var_country'] == 'China'){ echo 'selected="selected"';}?> value="China">China</option>
                                                <option <?php if($payment[0]['var_country'] == 'Christmas Island'){ echo 'selected="selected"';}?> value="Christmas Island">Christmas Island</option>
                                                <option <?php if($payment[0]['var_country'] == 'Cocos (Keeling) Islands'){ echo 'selected="selected"';}?> value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
                                                <option <?php if($payment[0]['var_country'] == 'Colombia'){ echo 'selected="selected"';}?> value="Colombia">Colombia</option>
                                                <option <?php if($payment[0]['var_country'] == 'Comoros'){ echo 'selected="selected"';}?> value="Comoros">Comoros</option>
                                                <option <?php if($payment[0]['var_country'] == 'Congo'){ echo 'selected="selected"';}?> value="Congo">Congo</option>
                                                <option <?php if($payment[0]['var_country'] == 'Congo, the Democratic Republic of the'){ echo 'selected="selected"';}?> value="Congo, the Democratic Republic of the">Congo, the Democratic Republic of the</option>
                                                <option <?php if($payment[0]['var_country'] == 'Cook Islands'){ echo 'selected="selected"';}?> value="Cook Islands">Cook Islands</option>
                                                <option <?php if($payment[0]['var_country'] == 'Costa Rica'){ echo 'selected="selected"';}?> value="Costa Rica">Costa Rica</option>
                                                <option <?php if($payment[0]['var_country'] == 'Cote d Ivoire'){ echo 'selected="selected"';}?> value="Cote d Ivoire">Cote d Ivoire</option>
                                                <option <?php if($payment[0]['var_country'] == 'Croatia (Hrvatska)'){ echo 'selected="selected"';}?> value="Croatia (Hrvatska)">Croatia (Hrvatska)</option>
                                                <option <?php if($payment[0]['var_country'] == 'Cuba'){ echo 'selected="selected"';}?> value="Cuba">Cuba</option>
                                                <option <?php if($payment[0]['var_country'] == 'Cyprus'){ echo 'selected="selected"';}?> value="Cyprus">Cyprus</option>
                                                <option <?php if($payment[0]['var_country'] == 'Czech Republic'){ echo 'selected="selected"';}?> value="Czech Republic">Czech Republic</option>
                                                <option <?php if($payment[0]['var_country'] == 'Denmark'){ echo 'selected="selected"';}?> value="Denmark">Denmark</option>
                                                <option <?php if($payment[0]['var_country'] == 'Djibouti'){ echo 'selected="selected"';}?> value="Djibouti">Djibouti</option>
                                                <option <?php if($payment[0]['var_country'] == 'Dominica'){ echo 'selected="selected"';}?> value="Dominica">Dominica</option>
                                                <option <?php if($payment[0]['var_country'] == 'Dominican Republic'){ echo 'selected="selected"';}?> value="Dominican Republic">Dominican Republic</option>
                                                <option <?php if($payment[0]['var_country'] == 'Ecuador'){ echo 'selected="selected"';}?> value="Ecuador">Ecuador</option>
                                                <option <?php if($payment[0]['var_country'] == 'Egypt'){ echo 'selected="selected"';}?> value="Egypt">Egypt</option>
                                                <option <?php if($payment[0]['var_country'] == 'El Salvador'){ echo 'selected="selected"';}?> value="El Salvador">El Salvador</option>
                                                <option <?php if($payment[0]['var_country'] == 'Equatorial Guinea'){ echo 'selected="selected"';}?> value="Equatorial Guinea">Equatorial Guinea</option>
                                                <option <?php if($payment[0]['var_country'] == 'Eritrea'){ echo 'selected="selected"';}?> value="Eritrea">Eritrea</option>
                                                <option <?php if($payment[0]['var_country'] == 'Estonia'){ echo 'selected="selected"';}?> value="Estonia">Estonia</option>
                                                <option <?php if($payment[0]['var_country'] == 'Ethiopia'){ echo 'selected="selected"';}?> value="Ethiopia">Ethiopia</option>
                                                <option <?php if($payment[0]['var_country'] == 'Falkland Islands (Malvinas)'){ echo 'selected="selected"';}?> value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option>
                                                <option <?php if($payment[0]['var_country'] == 'Faroe Islands'){ echo 'selected="selected"';}?> value="Faroe Islands">Faroe Islands</option>
                                                <option <?php if($payment[0]['var_country'] == 'Fiji'){ echo 'selected="selected"';}?> value="Fiji">Fiji</option>
                                                <option <?php if($payment[0]['var_country'] == 'Finland'){ echo 'selected="selected"';}?> value="Finland">Finland</option>
                                                <option <?php if($payment[0]['var_country'] == 'France'){ echo 'selected="selected"';}?> value="France">France</option>
                                                <option <?php if($payment[0]['var_country'] == 'French Guiana'){ echo 'selected="selected"';}?> value="French Guiana">French Guiana</option>
                                                <option <?php if($payment[0]['var_country'] == 'French Polynesia'){ echo 'selected="selected"';}?> value="French Polynesia">French Polynesia</option>
                                                <option <?php if($payment[0]['var_country'] == 'French Southern Territories'){ echo 'selected="selected"';}?> value="French Southern Territories">French Southern Territories</option>
                                                <option <?php if($payment[0]['var_country'] == 'Gabon'){ echo 'selected="selected"';}?> value="Gabon">Gabon</option>
                                                <option <?php if($payment[0]['var_country'] == 'Gambia'){ echo 'selected="selected"';}?> value="Gambia">Gambia</option>
                                                <option <?php if($payment[0]['var_country'] == 'Georgia'){ echo 'selected="selected"';}?> value="Georgia">Georgia</option>
                                                <option <?php if($payment[0]['var_country'] == 'Germany'){ echo 'selected="selected"';}?> value="Germany">Germany</option>
                                                <option <?php if($payment[0]['var_country'] == 'Ghana'){ echo 'selected="selected"';}?> value="Ghana">Ghana</option>
                                                <option <?php if($payment[0]['var_country'] == 'Gibraltar'){ echo 'selected="selected"';}?> value="Gibraltar">Gibraltar</option>
                                                <option <?php if($payment[0]['var_country'] == 'Greece'){ echo 'selected="selected"';}?> value="Greece">Greece</option>
                                                <option <?php if($payment[0]['var_country'] == 'Greenland'){ echo 'selected="selected"';}?> value="Greenland">Greenland</option>
                                                <option <?php if($payment[0]['var_country'] == 'Grenada'){ echo 'selected="selected"';}?> value="Grenada">Grenada</option>
                                                <option <?php if($payment[0]['var_country'] == 'Guadeloupe'){ echo 'selected="selected"';}?> value="Guadeloupe">Guadeloupe</option>
                                                <option <?php if($payment[0]['var_country'] == 'Guam'){ echo 'selected="selected"';}?> value="Guam">Guam</option>
                                                <option <?php if($payment[0]['var_country'] == 'Guatemala'){ echo 'selected="selected"';}?> value="Guatemala">Guatemala</option>
                                                <option <?php if($payment[0]['var_country'] == 'Guinea'){ echo 'selected="selected"';}?> value="Guinea">Guinea</option>
                                                <option <?php if($payment[0]['var_country'] == 'Guinea-Bissau'){ echo 'selected="selected"';}?> value="Guinea-Bissau">Guinea-Bissau</option>
                                                <option <?php if($payment[0]['var_country'] == 'Guyana'){ echo 'selected="selected"';}?> value="Guyana">Guyana</option>
                                                <option <?php if($payment[0]['var_country'] == 'Haiti'){ echo 'selected="selected"';}?> value="HT">Haiti</option>
                                                <option <?php if($payment[0]['var_country'] == 'Heard and Mc Donald Islands'){ echo 'selected="selected"';}?> value="Heard and Mc Donald Islands">Heard and Mc Donald Islands</option>
                                                <option <?php if($payment[0]['var_country'] == 'Holy See (Vatican City State)'){ echo 'selected="selected"';}?> value="Holy See (Vatican City State)">Holy See (Vatican City State)</option>
                                                <option <?php if($payment[0]['var_country'] == 'Honduras'){ echo 'selected="selected"';}?> value="Honduras">Honduras</option>
                                                <option <?php if($payment[0]['var_country'] == 'Hong Kong'){ echo 'selected="selected"';}?> value="Hong Kong">Hong Kong</option>
                                                <option <?php if($payment[0]['var_country'] == 'Hungary'){ echo 'selected="selected"';}?> value="Hungary">Hungary</option>
                                                <option <?php if($payment[0]['var_country'] == 'Iceland'){ echo 'selected="selected"';}?> value="Iceland">Iceland</option>
                                                <option <?php if($payment[0]['var_country'] == 'India'){ echo 'selected="selected"';}?> value="India">India</option>
                                                <option <?php if($payment[0]['var_country'] == 'Indonesia'){ echo 'selected="selected"';}?> value="Indonesia">Indonesia</option>
                                                <option <?php if($payment[0]['var_country'] == 'Iran (Islamic Republic of)'){ echo 'selected="selected"';}?> value="Iran (Islamic Republic of)">Iran (Islamic Republic of)</option>
                                                <option <?php if($payment[0]['var_country'] == 'Iraq'){ echo 'selected="selected"';}?> value="Iraq">Iraq</option>
                                                <option <?php if($payment[0]['var_country'] == 'Ireland'){ echo 'selected="selected"';}?> value="Ireland">Ireland</option>
                                                <option <?php if($payment[0]['var_country'] == 'Israel'){ echo 'selected="selected"';}?> value="Israel">Israel</option>
                                                <option <?php if($payment[0]['var_country'] == 'Italy'){ echo 'selected="selected"';}?> value="Italy">Italy</option>
                                                <option <?php if($payment[0]['var_country'] == 'Jamaica'){ echo 'selected="selected"';}?> value="Jamaica">Jamaica</option>
                                                <option <?php if($payment[0]['var_country'] == 'Japan'){ echo 'selected="selected"';}?> value="Japan">Japan</option>
                                                <option <?php if($payment[0]['var_country'] == 'Jordan'){ echo 'selected="selected"';}?> value="Jordan">Jordan</option>
                                                <option <?php if($payment[0]['var_country'] == 'Kazakhstan'){ echo 'selected="selected"';}?> value="Kazakhstan">Kazakhstan</option>
                                                <option <?php if($payment[0]['var_country'] == 'Kenya'){ echo 'selected="selected"';}?> value="Kenya">Kenya</option>
                                                <option <?php if($payment[0]['var_country'] == 'Kiribati'){ echo 'selected="selected"';}?> value="Kiribati">Kiribati</option>
                                                <option <?php if($payment[0]['var_country'] == 'Korea, Democratic People s Republic of'){ echo 'selected="selected"';}?> value="Korea, Democratic People s Republic of">Korea, Democratic People s Republic of</option>
                                                <option <?php if($payment[0]['var_country'] == 'Korea, Republic of'){ echo 'selected="selected"';}?> value="Korea, Republic of">Korea, Republic of</option>
                                                <option <?php if($payment[0]['var_country'] == 'Kuwait'){ echo 'selected="selected"';}?> value="Kuwait">Kuwait</option>
                                                <option <?php if($payment[0]['var_country'] == 'Kyrgyzstan'){ echo 'selected="selected"';}?> value="Kyrgyzstan">Kyrgyzstan</option>
                                                <option <?php if($payment[0]['var_country'] == 'Lao Peoples Democratic Republic'){ echo 'selected="selected"';}?> value="Lao Peoples Democratic Republic">Lao Peoples Democratic Republic</option>
                                                <option <?php if($payment[0]['var_country'] == 'Latvia'){ echo 'selected="selected"';}?> value="Latvia">Latvia</option>
                                                <option <?php if($payment[0]['var_country'] == 'Lebanon'){ echo 'selected="selected"';}?> value="Lebanon">Lebanon</option>
                                                <option <?php if($payment[0]['var_country'] == 'Lesotho'){ echo 'selected="selected"';}?> value="Lesotho">Lesotho</option>
                                                <option <?php if($payment[0]['var_country'] == 'Liberia'){ echo 'selected="selected"';}?> value="Liberia">Liberia</option>
                                                <option <?php if($payment[0]['var_country'] == 'Libyan Arab Jamahiriya'){ echo 'selected="selected"';}?> value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
                                                <option <?php if($payment[0]['var_country'] == 'Liechtenstein'){ echo 'selected="selected"';}?> value="Liechtenstein">Liechtenstein</option>
                                                <option <?php if($payment[0]['var_country'] == 'Lithuania'){ echo 'selected="selected"';}?> value="Lithuania">Lithuania</option>
                                                <option <?php if($payment[0]['var_country'] == 'Luxembourg'){ echo 'selected="selected"';}?> value="Luxembourg">Luxembourg</option>
                                                <option <?php if($payment[0]['var_country'] == 'Macau'){ echo 'selected="selected"';}?> value="Macau">Macau</option>
                                                <option <?php if($payment[0]['var_country'] == 'Macedonia, The Former Yugoslav Republic of'){ echo 'selected="selected"';}?> value="Macedonia, The Former Yugoslav Republic of">Macedonia, The Former Yugoslav Republic of</option>
                                                <option <?php if($payment[0]['var_country'] == 'Madagascar'){ echo 'selected="selected"';}?> value="Madagascar">Madagascar</option>
                                                <option <?php if($payment[0]['var_country'] == 'Malawi'){ echo 'selected="selected"';}?> value="Malawi">Malawi</option>
                                                <option <?php if($payment[0]['var_country'] == 'Malaysia'){ echo 'selected="selected"';}?> value="Malaysia">Malaysia</option>
                                                <option <?php if($payment[0]['var_country'] == 'Maldives'){ echo 'selected="selected"';}?> value="Maldives">Maldives</option>
                                                <option <?php if($payment[0]['var_country'] == 'Mali'){ echo 'selected="selected"';}?> value="ML">Mali</option>
                                                <option <?php if($payment[0]['var_country'] == 'Malta'){ echo 'selected="selected"';}?> value="MT">Malta</option>
                                                <option <?php if($payment[0]['var_country'] == 'Marshall Islands'){ echo 'selected="selected"';}?> value="Marshall Islands">Marshall Islands</option>
                                                <option <?php if($payment[0]['var_country'] == 'Martinique'){ echo 'selected="selected"';}?> value="Martinique">Martinique</option>
                                                <option <?php if($payment[0]['var_country'] == 'Mauritania'){ echo 'selected="selected"';}?> value="Mauritania">Mauritania</option>
                                                <option <?php if($payment[0]['var_country'] == 'Mauritius'){ echo 'selected="selected"';}?> value="Mauritius">Mauritius</option>
                                                <option <?php if($payment[0]['var_country'] == 'Mayotte'){ echo 'selected="selected"';}?> value="Mayotte">Mayotte</option>
                                                <option <?php if($payment[0]['var_country'] == 'Mexico'){ echo 'selected="selected"';}?> value="Mexico">Mexico</option>
                                                <option <?php if($payment[0]['var_country'] == 'Micronesia, Federated States of'){ echo 'selected="selected"';}?> value="Micronesia, Federated States of">Micronesia, Federated States of</option>
                                                <option <?php if($payment[0]['var_country'] == 'Moldova, Republic of'){ echo 'selected="selected"';}?> value="Moldova, Republic of">Moldova, Republic of</option>
                                                <option <?php if($payment[0]['var_country'] == 'Monaco'){ echo 'selected="selected"';}?> value="Monaco">Monaco</option>
                                                <option <?php if($payment[0]['var_country'] == 'Mongolia'){ echo 'selected="selected"';}?> value="Mongolia">Mongolia</option>
                                                <option <?php if($payment[0]['var_country'] == 'Montserrat'){ echo 'selected="selected"';}?> value="Montserrat">Montserrat</option>
                                                <option <?php if($payment[0]['var_country'] == 'Morocco'){ echo 'selected="selected"';}?> value="Morocco">Morocco</option>
                                                <option <?php if($payment[0]['var_country'] == 'Mozambique'){ echo 'selected="selected"';}?> value="Mozambique">Mozambique</option>
                                                <option <?php if($payment[0]['var_country'] == 'Myanmar'){ echo 'selected="selected"';}?> value="Myanmar">Myanmar</option>
                                                <option <?php if($payment[0]['var_country'] == 'Namibia'){ echo 'selected="selected"';}?> value="Namibia">Namibia</option>
                                                <option <?php if($payment[0]['var_country'] == 'Nauru'){ echo 'selected="selected"';}?> value="Nauru">Nauru</option>
                                                <option <?php if($payment[0]['var_country'] == 'Nepal'){ echo 'selected="selected"';}?> value="Nepal">Nepal</option>
                                                <option <?php if($payment[0]['var_country'] == 'Netherlands'){ echo 'selected="selected"';}?> value="Netherlands">Netherlands</option>
                                                <option <?php if($payment[0]['var_country'] == 'Netherlands Antilles'){ echo 'selected="selected"';}?> value="Netherlands Antilles">Netherlands Antilles</option>
                                                <option <?php if($payment[0]['var_country'] == 'New Caledonia'){ echo 'selected="selected"';}?> value="New Caledonia">New Caledonia</option>
                                                <option <?php if($payment[0]['var_country'] == 'New Zealand'){ echo 'selected="selected"';}?> value="New Zealand">New Zealand</option>
                                                <option <?php if($payment[0]['var_country'] == 'Nicaragua'){ echo 'selected="selected"';}?> value="Nicaragua">Nicaragua</option>
                                                <option <?php if($payment[0]['var_country'] == 'Niger'){ echo 'selected="selected"';}?> value="Niger">Niger</option>
                                                <option <?php if($payment[0]['var_country'] == 'Nigeria'){ echo 'selected="selected"';}?> value="Nigeria">Nigeria</option>
                                                <option <?php if($payment[0]['var_country'] == 'Niue'){ echo 'selected="selected"';}?> value="NU">Niue</option>
                                                <option <?php if($payment[0]['var_country'] == 'Norfolk Island'){ echo 'selected="selected"';}?> value="Norfolk Island">Norfolk Island</option>
                                                <option <?php if($payment[0]['var_country'] == 'Northern Mariana Islands'){ echo 'selected="selected"';}?> value="Northern Mariana Islands">Northern Mariana Islands</option>
                                                <option <?php if($payment[0]['var_country'] == 'Norway'){ echo 'selected="selected"';}?> value="Norway">Norway</option>
                                                <option <?php if($payment[0]['var_country'] == 'Oman'){ echo 'selected="selected"';}?> value="Oman">Oman</option>
                                                <option <?php if($payment[0]['var_country'] == 'Pakistan'){ echo 'selected="selected"';}?> value="Pakistan">Pakistan</option>
                                                <option <?php if($payment[0]['var_country'] == 'Palau'){ echo 'selected="selected"';}?> value="Palau">Palau</option>
                                                <option <?php if($payment[0]['var_country'] == 'Panama'){ echo 'selected="selected"';}?> value="Panama">Panama</option>
                                                <option <?php if($payment[0]['var_country'] == 'Papua New Guinea'){ echo 'selected="selected"';}?> value="Papua New Guinea">Papua New Guinea</option>
                                                <option <?php if($payment[0]['var_country'] == 'Paraguay'){ echo 'selected="selected"';}?> value="Paraguay">Paraguay</option>
                                                <option <?php if($payment[0]['var_country'] == 'Peru'){ echo 'selected="selected"';}?> value="Peru">Peru</option>
                                                <option <?php if($payment[0]['var_country'] == 'Philippines'){ echo 'selected="selected"';}?> value="Philippines">Philippines</option>
                                                <option <?php if($payment[0]['var_country'] == 'Pitcairn'){ echo 'selected="selected"';}?> value="Pitcairn">Pitcairn</option>
                                                <option <?php if($payment[0]['var_country'] == 'Poland'){ echo 'selected="selected"';}?> value="Poland">Poland</option>
                                                <option <?php if($payment[0]['var_country'] == 'Portugal'){ echo 'selected="selected"';}?> value="Portugal">Portugal</option>
                                                <option <?php if($payment[0]['var_country'] == 'Puerto Rico'){ echo 'selected="selected"';}?> value="Puerto Rico">Puerto Rico</option>
                                                <option <?php if($payment[0]['var_country'] == 'Qatar'){ echo 'selected="selected"';}?> value="Qatar">Qatar</option>
                                                <option <?php if($payment[0]['var_country'] == 'Reunion'){ echo 'selected="selected"';}?> value="Reunion">Reunion</option>
                                                <option <?php if($payment[0]['var_country'] == 'Romania'){ echo 'selected="selected"';}?> value="Romania">Romania</option>
                                                <option <?php if($payment[0]['var_country'] == 'Russian Federation'){ echo 'selected="selected"';}?> value="Russian Federation">Russian Federation</option>
                                                <option <?php if($payment[0]['var_country'] == 'Rwanda'){ echo 'selected="selected"';}?> value="Rwanda">Rwanda</option>
                                                <option <?php if($payment[0]['var_country'] == 'Saint Kitts and Nevis'){ echo 'selected="selected"';}?> value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                                                <option <?php if($payment[0]['var_country'] == 'Saint LUCIA'){ echo 'selected="selected"';}?> value="Saint LUCIA">Saint LUCIA</option>
                                                <option <?php if($payment[0]['var_country'] == 'Saint Vincent and the Grenadines'){ echo 'selected="selected"';}?> value="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option>
                                                <option <?php if($payment[0]['var_country'] == 'Samoa'){ echo 'selected="selected"';}?> value="Samoa">Samoa</option>
                                                <option <?php if($payment[0]['var_country'] == 'San Marino'){ echo 'selected="selected"';}?> value="San Marino">San Marino</option>
                                                <option <?php if($payment[0]['var_country'] == 'Sao Tome and Principe'){ echo 'selected="selected"';}?> value="Sao Tome and Principe">Sao Tome and Principe</option>
                                                <option <?php if($payment[0]['var_country'] == 'Saudi Arabia'){ echo 'selected="selected"';}?> value="Saudi Arabia">Saudi Arabia</option>
                                                <option <?php if($payment[0]['var_country'] == 'Senegal'){ echo 'selected="selected"';}?> value="Senegal">Senegal</option>
                                                <option <?php if($payment[0]['var_country'] == 'Seychelles'){ echo 'selected="selected"';}?> value="Seychelles">Seychelles</option>
                                                <option <?php if($payment[0]['var_country'] == 'Sierra Leone'){ echo 'selected="selected"';}?> value="Sierra Leone">Sierra Leone</option>
                                                <option <?php if($payment[0]['var_country'] == 'Singapore'){ echo 'selected="selected"';}?> value="Singapore">Singapore</option>
                                                <option <?php if($payment[0]['var_country'] == 'Slovakia (Slovak Republic)'){ echo 'selected="selected"';}?> value="Slovakia (Slovak Republic)">Slovakia (Slovak Republic)</option>
                                                <option <?php if($payment[0]['var_country'] == 'Slovenia'){ echo 'selected="selected"';}?> value="Slovenia">Slovenia</option>
                                                <option <?php if($payment[0]['var_country'] == 'Solomon Islands'){ echo 'selected="selected"';}?> value="Solomon Islands">Solomon Islands</option>
                                                <option <?php if($payment[0]['var_country'] == 'Somalia'){ echo 'selected="selected"';}?> value="Somalia">Somalia</option>
                                                <option <?php if($payment[0]['var_country'] == 'South Africa'){ echo 'selected="selected"';}?> value="South Africa">South Africa</option>
                                                <option <?php if($payment[0]['var_country'] == 'South Georgia and the South Sandwich Islands'){ echo 'selected="selected"';}?> value="South Georgia and the South Sandwich Islands">South Georgia and the South Sandwich Islands</option>
                                                <option <?php if($payment[0]['var_country'] == 'Spain'){ echo 'selected="selected"';}?> value="Spain">Spain</option>
                                                <option <?php if($payment[0]['var_country'] == 'Sri Lanka'){ echo 'selected="selected"';}?> value="Sri Lanka">Sri Lanka</option>
                                                <option <?php if($payment[0]['var_country'] == 'St. Helena'){ echo 'selected="selected"';}?> value="St. Helena">St. Helena</option>
                                                <option <?php if($payment[0]['var_country'] == 'PM'){ echo 'selected="selected"';}?> value="PM">St. Pierre and Miquelon</option>
                                                <option <?php if($payment[0]['var_country'] == 'Sudan'){ echo 'selected="selected"';}?> value="Sudan">Sudan</option>
                                                <option <?php if($payment[0]['var_country'] == 'Suriname'){ echo 'selected="selected"';}?> value="Suriname">Suriname</option>
                                                <option <?php if($payment[0]['var_country'] == 'Svalbard and Jan Mayen Islands'){ echo 'selected="selected"';}?> value="Svalbard and Jan Mayen Islands">Svalbard and Jan Mayen Islands</option>
                                                <option <?php if($payment[0]['var_country'] == 'Swaziland'){ echo 'selected="selected"';}?> value="Swaziland">Swaziland</option>
                                                <option <?php if($payment[0]['var_country'] == 'Sweden'){ echo 'selected="selected"';}?> value="Sweden">Sweden</option>
                                                <option <?php if($payment[0]['var_country'] == 'Switzerland'){ echo 'selected="selected"';}?> value="Switzerland">Switzerland</option>
                                                <option <?php if($payment[0]['var_country'] == 'Syrian Arab Republic'){ echo 'selected="selected"';}?> value="Syrian Arab Republic">Syrian Arab Republic</option>
                                                <option <?php if($payment[0]['var_country'] == 'Taiwan, Province of China'){ echo 'selected="selected"';}?> value="Taiwan, Province of China">Taiwan, Province of China</option>
                                                <option <?php if($payment[0]['var_country'] == 'Tajikistan'){ echo 'selected="selected"';}?> value="Tajikistan">Tajikistan</option>
                                                <option <?php if($payment[0]['var_country'] == 'Tanzania, United Republic of'){ echo 'selected="selected"';}?> value="Tanzania, United Republic of">Tanzania, United Republic of</option>
                                                <option <?php if($payment[0]['var_country'] == 'Thailand'){ echo 'selected="selected"';}?> value="Thailand">Thailand</option>
                                                <option <?php if($payment[0]['var_country'] == 'Togo'){ echo 'selected="selected"';}?> value="Togo">Togo</option>
                                                <option <?php if($payment[0]['var_country'] == 'Tokelau'){ echo 'selected="selected"';}?> value="Tokelau">Tokelau</option>
                                                <option <?php if($payment[0]['var_country'] == 'Tonga'){ echo 'selected="selected"';}?> value="Tonga">Tonga</option>
                                                <option <?php if($payment[0]['var_country'] == 'Trinidad and Tobago'){ echo 'selected="selected"';}?> value="Trinidad and Tobago">Trinidad and Tobago</option>
                                                <option <?php if($payment[0]['var_country'] == 'Tunisia'){ echo 'selected="selected"';}?> value="Tunisia">Tunisia</option>
                                                <option <?php if($payment[0]['var_country'] == 'Turkey'){ echo 'selected="selected"';}?> value="Turkey">Turkey</option>
                                                <option <?php if($payment[0]['var_country'] == 'Turkmenistan'){ echo 'selected="selected"';}?> value="Turkmenistan">Turkmenistan</option>
                                                <option <?php if($payment[0]['var_country'] == 'Turks and Caicos Islands'){ echo 'selected="selected"';}?> value="Turks and Caicos Islands">Turks and Caicos Islands</option>
                                                <option <?php if($payment[0]['var_country'] == 'Tuvalu'){ echo 'selected="selected"';}?> value="Tuvalu">Tuvalu</option>
                                                <option <?php if($payment[0]['var_country'] == 'Uganda'){ echo 'selected="selected"';}?> value="Uganda">Uganda</option>
                                                <option <?php if($payment[0]['var_country'] == 'Ukraine'){ echo 'selected="selected"';}?> value="Ukraine">Ukraine</option>
                                                <option <?php if($payment[0]['var_country'] == 'United Arab Emirates'){ echo 'selected="selected"';}?> value="United Arab Emirates">United Arab Emirates</option>
                                                <option <?php if($payment[0]['var_country'] == 'United Kingdom'){ echo 'selected="selected"';}?> value="United Kingdom">United Kingdom</option>
                                                <option <?php if($payment[0]['var_country'] == 'United States'){ echo 'selected="selected"';}?> value="United States">United States</option>
                                                <option <?php if($payment[0]['var_country'] == 'United States Minor Outlying Islands'){ echo 'selected="selected"';}?> value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
                                                <option <?php if($payment[0]['var_country'] == 'Uruguay'){ echo 'selected="selected"';}?> value="Uruguay">Uruguay</option>
                                                <option <?php if($payment[0]['var_country'] == 'Uzbekistan'){ echo 'selected="selected"';}?> value="Uzbekistan">Uzbekistan</option>
                                                <option <?php if($payment[0]['var_country'] == 'Vanuatu'){ echo 'selected="selected"';}?> value="Vanuatu">Vanuatu</option>
                                                <option <?php if($payment[0]['var_country'] == 'Venezuela'){ echo 'selected="selected"';}?> value="Venezuela">Venezuela</option>
                                                <option <?php if($payment[0]['var_country'] == 'Viet Nam'){ echo 'selected="selected"';}?> value="Viet Nam">Viet Nam</option>
                                                <option <?php if($payment[0]['var_country'] == 'Virgin Islands (British)'){ echo 'selected="selected"';}?> value="Virgin Islands (British)">Virgin Islands (British)</option>
                                                <option <?php if($payment[0]['var_country'] == 'Virgin Islands (U.S.)'){ echo 'selected="selected"';}?> value="Virgin Islands (U.S.)">Virgin Islands (U.S.)</option>
                                                <option <?php if($payment[0]['var_country'] == 'Wallis and Futuna Islands'){ echo 'selected="selected"';}?> value="Wallis and Futuna Islands">Wallis and Futuna Islands</option>
                                                <option <?php if($payment[0]['var_country'] == 'Western Sahara'){ echo 'selected="selected"';}?> value="Western Sahara">Western Sahara</option>
                                                <option <?php if($payment[0]['var_country'] == 'Yemen'){ echo 'selected="selected"';}?> value="Yemen">Yemen</option>
                                                <option <?php if($payment[0]['var_country'] == 'Zambia'){ echo 'selected="selected"';}?> value="Zambia">Zambia</option>
                                                <option <?php if($payment[0]['var_country'] == 'Zimbabwe'){ echo 'selected="selected"';}?> value="Zimbabwe">Zimbabwe</option>
                                            </select>
                                </div>
                            </div>
                             <div class="form-group US hidden">
                                        <label class="control-label col-md-12 text-left paout_label">State:</label>
                                        <div class="col-md-12">
                                            <select id="state" class="form-control select2me" name="state">
                                                <option <?php if($payment[0]['var_state'] == ''){ echo 'selected="selected"';}?> value="">-- Select State--</option>
                                                <option <?php if($payment[0]['var_state'] == 'Alaska'){ echo 'selected="selected"';}?> value="Alaska">Alaska</option>
                                                <option <?php if($payment[0]['var_state'] == 'Alabama'){ echo 'selected="selected"';}?> value="Alabama">Alabama</option>
                                                <option <?php if($payment[0]['var_state'] == 'Arizona'){ echo 'selected="selected"';}?> value="Arizona">Arizona</option>
                                                <option <?php if($payment[0]['var_state'] == 'Arkansas'){ echo 'selected="selected"';}?> value="Arkansas">Arkansas</option>
                                                <option <?php if($payment[0]['var_state'] == 'California'){ echo 'selected="selected"';}?> value="California">California</option>
                                                <option <?php if($payment[0]['var_state'] == 'Colorado'){ echo 'selected="selected"';}?> value="Colorado">Colorado</option>
                                                <option <?php if($payment[0]['var_state'] == 'Connecticut'){ echo 'selected="selected"';}?> value="Connecticut">Connecticut</option>
                                                <option <?php if($payment[0]['var_state'] == 'Delaware'){ echo 'selected="selected"';}?> value="Delaware">Delaware</option>
                                                <option <?php if($payment[0]['var_state'] == 'Florida'){ echo 'selected="selected"';}?> value="Florida">Florida</option>
                                                <option <?php if($payment[0]['var_state'] == 'Georgia'){ echo 'selected="selected"';}?> value="Georgia">Georgia</option>
                                                <option <?php if($payment[0]['var_state'] == 'Hawaii'){ echo 'selected="selected"';}?> value="Hawaii">Hawaii</option>
                                                <option <?php if($payment[0]['var_state'] == 'Idaho'){ echo 'selected="selected"';}?> value="Idaho">Idaho</option>
                                                <option <?php if($payment[0]['var_state'] == 'Illinois'){ echo 'selected="selected"';}?> value="Illinois">Illinois</option>
                                                <option <?php if($payment[0]['var_state'] == 'Indiana'){ echo 'selected="selected"';}?> value="Indiana">Indiana</option>
                                                <option <?php if($payment[0]['var_state'] == 'Iowa'){ echo 'selected="selected"';}?> value="Iowa">Iowa</option>
                                                <option <?php if($payment[0]['var_state'] == 'Kansas'){ echo 'selected="selected"';}?> value="Kansas">Kansas</option>
                                                <option <?php if($payment[0]['var_state'] == 'Kentucky'){ echo 'selected="selected"';}?> value="Kentucky">Kentucky</option>
                                                <option <?php if($payment[0]['var_state'] == 'Louisiana'){ echo 'selected="selected"';}?> value="Louisiana">Louisiana</option>
                                                <option <?php if($payment[0]['var_state'] == 'Maine'){ echo 'selected="selected"';}?> value="Maine">Maine</option>
                                                <option <?php if($payment[0]['var_state'] == 'Maryland'){ echo 'selected="selected"';}?> value="Maryland">Maryland</option>
                                                <option <?php if($payment[0]['var_state'] == 'Michigan'){ echo 'selected="selected"';}?> value="Michigan">Michigan</option>
                                                <option <?php if($payment[0]['var_state'] == 'Minnesota'){ echo 'selected="selected"';}?> value="Minnesota">Minnesota</option>
                                                <option <?php if($payment[0]['var_state'] == 'Mississippi'){ echo 'selected="selected"';}?> value="Mississippi">Mississippi</option>
                                                <option <?php if($payment[0]['var_state'] == 'Missouri'){ echo 'selected="selected"';}?> value="Missouri">Missouri</option>
                                                <option <?php if($payment[0]['var_state'] == 'Montana'){ echo 'selected="selected"';}?> value="Montana">Montana</option>
                                                <option <?php if($payment[0]['var_state'] == 'Nebraska'){ echo 'selected="selected"';}?> value="Nebraska">Nebraska</option>
                                                <option <?php if($payment[0]['var_state'] == 'Nevada'){ echo 'selected="selected"';}?> value="Nevada">Nevada</option>
                                                <option <?php if($payment[0]['var_state'] == 'New Hampshire'){ echo 'selected="selected"';}?> value="New Hampshire">New Hampshire</option>
                                                <option <?php if($payment[0]['var_state'] == 'New Jersey'){ echo 'selected="selected"';}?> value="New Jersey">New Jersey</option>
                                                <option <?php if($payment[0]['var_state'] == 'New Mexico'){ echo 'selected="selected"';}?> value="New Mexico">New Mexico</option>
                                                <option <?php if($payment[0]['var_state'] == 'New York'){ echo 'selected="selected"';}?> value="New York">New York</option>
                                                <option <?php if($payment[0]['var_state'] == 'North Carolina'){ echo 'selected="selected"';}?> value="North Carolina">North Carolina</option>
                                                <option <?php if($payment[0]['var_state'] == 'North Carolina'){ echo 'selected="selected"';}?> value="North Dakota">North Dakota</option>
                                                <option <?php if($payment[0]['var_state'] == 'Ohio'){ echo 'selected="selected"';}?> value="Ohio">Ohio</option>
                                                <option <?php if($payment[0]['var_state'] == 'Oklahoma'){ echo 'selected="selected"';}?> value="Oklahoma">Oklahoma</option>
                                                <option <?php if($payment[0]['var_state'] == 'Oregon'){ echo 'selected="selected"';}?> value="Oregon">Oregon</option>
                                                <option <?php if($payment[0]['var_state'] == 'Pennsylvania'){ echo 'selected="selected"';}?> value="Pennsylvania">Pennsylvania</option>
                                                <option <?php if($payment[0]['var_state'] == 'Rhode Island'){ echo 'selected="selected"';}?> value="Rhode Island">Rhode Island</option>
                                                <option <?php if($payment[0]['var_state'] == 'South Carolina'){ echo 'selected="selected"';}?> value="South Carolina">South Carolina</option>
                                                <option <?php if($payment[0]['var_state'] == 'South Dakota'){ echo 'selected="selected"';}?> value="South Dakota">South Dakota</option>
                                                <option <?php if($payment[0]['var_state'] == 'Tennessee'){ echo 'selected="selected"';}?> value="Tennessee">Tennessee</option>
                                                <option <?php if($payment[0]['var_state'] == 'Texas'){ echo 'selected="selected"';}?> value="Texas">Texas</option>
                                                <option <?php if($payment[0]['var_state'] == 'Utah'){ echo 'selected="selected"';}?> value="Utah">Utah</option>
                                                <option <?php if($payment[0]['var_state'] == 'Vermont'){ echo 'selected="selected"';}?> value="Vermont">Vermont</option>
                                                <option <?php if($payment[0]['var_state'] == 'Virginia'){ echo 'selected="selected"';}?> value="Virginia">Virginia</option>
                                                <option <?php if($payment[0]['var_state'] == 'Washington'){ echo 'selected="selected"';}?> value="Washington">Washington</option>
                                                <option <?php if($payment[0]['var_state'] == 'West Virginia'){ echo 'selected="selected"';}?> value="West Virginia">West Virginia</option>
                                                <option <?php if($payment[0]['var_state'] == 'Wisconsin'){ echo 'selected="selected"';}?> value="Wisconsin">Wisconsin</option>
                                                <option <?php if($payment[0]['var_state'] == 'Wyoming'){ echo 'selected="selected"';}?> value="Wyoming">Wyoming</option>
                                            </select>
                                        </div>
                                    </div>
                            <div class="form-group">
                                <label class="control-label col-md-12 text-left paout_label">City:</label>
                                <div  class="col-md-12">
                                    <input type="text" class="form-control" id="city" name="city" value="<?php echo $payment[0]['var_city']; ?>">
                                </div>
                            </div>
                            
                            

                        </div>
                        <div class="col-md-12 text-left margin-bottom-20">
                            All transactions subject to Western Union's Terms & Conditions.
                            Western Union is a registered mark of Western Union Holdings, Inc. 
                        </div>     
                    </div>

                    <div class="form-group text-center">
                    </div>
                </div> 

            <!--</form>-->

        </div>
    <div class="clear"></div>

      <div class="divider_two_div"></div>
      <div class="content_row">
        <!--<form method="post" id="edit_account1" class="form-horizontal form-bordered" action="">-->
            <div class="form-body"> 
                <div class="form-group">
                    <label class="control-label col-md-4">I prefer to get paid:</label>
                    <div  class="col-md-3">
                        <select id="method_payment" class="form-control input-xlarge select2me" name="method_payment" placeholder="Select...">
                            <option <?php if($acc_info['var_payment'] == 'paypal'){ echo 'selected="selected"';}?> value="paypal">PayPal&#8482;</option>
                            <option <?php if($acc_info['var_payment'] == 'westerrn_union'){ echo 'selected="selected"';}?> value="westerrn_union">Western Union&#174;</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4">Please only send me money once I reach:</label>
                    <div  class="col-md-3">
                        <input type="text" class="form-control price" name="send_money" value="<?php echo $payment[0]['var_money']; ?>">
                    </div>
                </div>
                <div class="form-group text-center">
                    <div  class="col-md-12">
                        <button type="submit" class="default_btn" style="padding: 8px 30px; margin: 10px 0 0;">Update</button>
                    </div>
                </div>
            </div>    

      
      </div>  
      
   <div class="content_row">
       <div class="text-left">
           <span>Please note that any sending fees (if applicable) will be deducted from your commission as you must cover any fees associated with getting the money from Us to you.</span>
           <br />
           <br />
           Thank you for your understanding!
       </div>
   </div> 
      </form>
       </div>
</div>    

<div aria-hidden="false" role="dialog" class="modal fade in" id="req_success" style="display: none;">
    <div class="success_login_main_cont margin-top-20">
        <button aria-hidden="true" data-dismiss="modal" class="close" style="padding-top: 5px;" type="button"><i class="fa fa-times"></i></button>
        <div class="modal-header">
            <!--<button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="fa fa-times"></i></button>-->
            <h3 class="form-title">ACCOUNT UPDATE</h3>
        </div>
        <div style="" class="" style="padding-top: 16px;">
            <p style="padding: 10px">Thank you for keeping your account updated, your changes were successfully saved!</p>
        </div>
        <div style="padding: 3px;" class="modal-footer">
            <div style="text-align:center">
                <button type="button" style="padding: 7px 20px;" data-dismiss="modal" class="btn default_btn clickme">OK</button>
            </div>
        </div>
    </div> 
</div>