<div class="main_cont_home">
    <div class="pagetitle">
        <h1><?php echo $this->page_name; ?></h1>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="section"><a href="<?php echo base_url(); ?>affiliate/home/account_info"><i class="fa fa-info-circle"></i>Account Info</a></div>
            <div class="section"><a href="<?php echo base_url(); ?>affiliate/payout_prefrence"><i class="fa fa-user"></i>Payout Preference</a></div>
            <div class="section"><a href="<?php echo base_url(); ?>affiliate/contact_us"><i class="fa fa-phone"></i>Contact Us</a></div>
        </div>
        <div class="col-md-4">
            <div class="section"><a href="<?php echo base_url(); ?>affiliate/banner_link"><i class="fa fa-external-link "></i>Banner / Link</a></div>
            <div class="section"><a href="<?php echo base_url(); ?>affiliate/affiliate_guide"><i class="fa fa-book"></i>Affilliate Guide</a></div> 
        </div>
        <div class="col-md-4">
            <div class="section"><a href="<?php echo base_url(); ?>affiliate/statistics"><i class="fa fa-bar-chart-o"></i>Statistics</a></div>
            <div class="section"><a href="<?php echo base_url(); ?>affiliate/payout_history"><i class="fa fa-usd "></i>Payout History</a></div>
        </div>
    </div>
</div>
