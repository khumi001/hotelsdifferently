<div class="main_cont">
    <div class="pagetitle margin-bottom-10">
        <h1><?php echo $this->page_name; ?></h1>
    </div>
    <div class="text-left">
        Here you can track your earning progress! For detailed information on how you earn money by being our affiliate, please click <b><a href="#" style="color:#ff0000;">HERE!</a></b>
    </div>
    <h3 class="sub_title">Pending Transactions</h3>
    <div class="form-group col-md-7 hide filter-box">
        <label class="control-label col-md-2" style="padding: 6px 0;">Filter By :</label>
            <div class="col-md-9">
                <div class="input-group input-large date-picker input-daterange " data-date="10/11/2012" data-date-format="mm/dd/yyyy">
                    <input type="text" class="form-control" id="from" name="from" value="" placeholder="Date From" style="text-align:left;">
                    <span class="input-group-addon">
                        to
                    </span>
                    <input type="text" class="form-control" id="to" name="to" value="" placeholder="Date To" style="text-align:left;">
                </div>
            </div>    
        </div>
    <div class="transactions_table">   

        <table class="table table-striped table-bordered table-hover" id="pending_transactions">
            <thead>
                <tr role="row" class="heading">
                    <th width=''>Account ID</th>
                    <th width=''>Booking Date</th>
                    <th width=''>Amount</th>
                    <th width=''>Commission</th>
                    <th width=''>Release date</th>
                    <th width=''>Note</th>
                </tr>
            </thead>
            <tbody></tbody>
            <tfoot>
                <tr>
                    <th>Total:</th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
            </tfoot>
        </table>
    </div>
    <div class="note_text alert alert-danger margin-top-20">
        NOTE: Transactions will be posted 90 Days AFTER the stay. Posted transactions can be found in the "Payout History" menu.
    </div>
</div>