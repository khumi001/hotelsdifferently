<div class="main_cont">
    <div class="pagetitle margin-bottom-10">
        <h1><?php echo $this->page_name; ?></h1>
    </div>
    <div class="form general_form_payout">
        <h3 class="sub_title">Personal Information</h3>
        <form method="post" id="edit_account" class="form-horizontal form-bordered" action="<?php echo base_url(); ?>affiliate/home/account_info">
            <div class="form-body"> 
                <div class="row">
                    <div class="col-md-6">

                        <div class="form-group">
                            <label class="control-label col-md-4 text-left paout_label">First name:</label>
                            <div  class="col-md-6">
                                <!--<input type="text" class="form-control" name="fname" readonly="true" value="<?php echo $acc_info['var_fname']; ?>">-->
                                 <div style="padding: 10px 0 0;">
                                    <?php echo $acc_info['var_fname']; ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 text-left paout_label">Email:</label>
                            <div  class="col-md-6">
                                <input type="text" name="email" class="form-control" value="<?php echo $acc_info['var_email']; ?>">
                            </div>
                        </div>

                    </div>
                    <div class="col-md-6">

                        <div class="form-group">
                            <label class="control-label col-md-4 text-left paout_label">Last name:</label>
                            <div  class="col-md-6">
                                <!--<input type="text" class="form-control" name="lname" readonly="true" value="<?php echo $acc_info['var_lname']; ?>">-->
                                <div style="padding: 10px 0 0;">
                                   <?php echo $acc_info['var_fname']; ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 text-left paout_label">Phone number: <a class="tooltips" data-placement="top" data-original-title="Please include your country code."><sup><i class="fa fa-info-circle"></i></sup></a></label>
                            <div  class="col-md-6">
                                <input type="text" name="phone_number" class="form-control" value="<?php echo $acc_info['var_phone']; ?>">
                            </div>
                        </div>
                    </div>
                </div>
            </div> 

        </form>
    </div>
    <div class="divider_two_div"></div>

    <h3 class="sub_title">Payment Information</h3>
        <div class="col-md-6 right-border">
            <div class="text-center">
                <img class="pay_img" width="200" src="<?php echo base_url(); ?>public/affilliate_theme/img/paypal.png" >
            </div>
            <form method="post" id="paypal_info" class="form form-horizontal form-bordered text-center" action="#">
                <div class="form-body"> 
                    <div class="row">
                        <div class="col-md-9" style="margin:0 0 0 56px;">

                            <div class="form-group">
                                <label class="control-label col-md-12 text-left paout_label">Paypal&#8482; email:</label>

                                <div  class="col-md-12">
                                    <input type="text" class="form-control" name="pay_email">
                                </div>
                            </div>

                        </div>
                        <div class="col-md-9 margin-bottom-20" style="margin-left: 56px;">Please take note of the transaction costs charged by PayPal&#8482; and be sure to take into account the delivery time, both of which are subject to PayPal's legal agreements and policies.</div>    
                    </div>
                </div> 

            </form>


        </div>
        <div class="col-md-6">
            <div class="text-center">
                <img class="pay_img" width="200" src="<?php echo base_url(); ?>public/affilliate_theme/img/western.png" >
            </div>
            <form method="post" id="western_union_form" class="form form-horizontal form-bordered union_form" action="#">
                <div class="form-body"> 
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-12 text-left paout_label">Full name: <a class="tooltips" data-placement="top" data-original-title="Must be exactly the same as the ID you will be using to receive your funds in person"><sup><i class="fa fa-info-circle"></i></sup></a></label>
                                <div  class="col-md-12">
                                    <input type="text" class="form-control" name="full_name">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-12 text-left paout_label">Phone number: <a class="tooltips" data-placement="top" data-original-title="Please include your country code."><sup><i class="fa fa-info-circle"></i></sup></a></label>
                                <div  class="col-md-12">
                                    <input type="text" class="form-control" name="phone_no">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-12 text-left paout_label">City:</label>
                                <div  class="col-md-12">
                                    <input type="text" class="form-control" name="city">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-12 text-left paout_label">State:</label>
                                <div  class="col-md-12">
                                    <input type="text" class="form-control" name="state">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-12 text-left paout_label">Country:</label>
                                <div  class="col-md-12">
                                    <input type="text" class="form-control" name="country">
                                </div>
                            </div>

                        </div>
                        <div class="col-md-12 text-left margin-bottom-20">
                            All transactions subject to Western Union's Terms & Conditions.
                            Western Union is a registered mark of Western Union Holdings, Inc. 
                        </div>     
                    </div>

                    <div class="form-group text-center">
                    </div>
                </div> 

            </form>

        </div>
    <div class="clear"></div>

      <div class="divider_two_div"></div>
      <div class="content_row">
        <form method="post" id="edit_account" class="form-horizontal form-bordered" action="#">
            <div class="form-body"> 
                <div class="form-group">
                    <label class="control-label col-md-4">I prefer to get paid:</label>
                    <div  class="col-md-3">
                        <select class="form-control input-xlarge select2me" placeholder="Select...">
                            <option value="AL">PayPal&#8482;</option>
                            <option value="WY">Western Union&#174;</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4">Please only send me money once I reach:</label>
                    <div  class="col-md-3">
                        <input type="text" class="form-control" name="send_money" value="$100.00">
                    </div>
                </div>
                <div class="form-group text-center">
                    <div  class="col-md-12">
                        <button type="submit" class="default_btn" style="padding: 8px 30px; margin: 10px 0 0;">Update</button>
                    </div>
                </div>
            </div>    

        </form>
      </div>    
   <div class="content_row">
       
       <div class="text-left">
           
           <span>Please note that any sending fees (if applicable) will be deducted from your commission as you must cover any fees associated with getting the money from Us to you.</span>
           <br />
           <br />
           
           Thank you for your understanding!
       </div>
       
   </div>  


</div>    