<div class="main_cont">
    <div class="pagetitle margin-bottom-10">
        <h1><?php echo $this->page_name; ?></h1>
    </div> 
    <div class="content_row">

        <form method="post" id="edit_account" class="form-horizontal form-bordered" action="#">

            <div class="form-body">

                <div class="row">
                    <div class="col-md-6">

                        <div class="form-group">
                            <label class="control-label col-md-4">First name:</label>
                            <div  class="col-md-4">
                                <div style="padding: 10px 10px 0;">
                                    John
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">Last name:</label>
                            <div  class="col-md-4">
                                <div style="padding: 10px 10px 0;">
                                    Smith
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">Account number:</label>
                            <div  class="col-md-4">
                                <div style="padding: 10px 10px 0;">
                                    0534164561322 
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">Email address:</label>
                            <div  class="col-md-8">
                                <input class="form-control" name="email_add" value="john_smith@gmail.com">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">Topic:</label>
                            <div  class="col-md-8">
                                <select class="form-control input-xlarge select2me" placeholder="Select...">
                                    <option value="">Please Select</option>
                                    <option value="t/w">Technical / Website Issues</option>
                                    <option value="p/c">Payout / Commision</option>
                                    <option value="other">Other</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">Response:</label>
                            <div  class="col-md-8">
                                <select class="form-control input-xlarge select2me" placeholder="">
                                    <option value="">Please Select</option>
                                    <option value="y">Yes</option>
                                    <option value="n">No</option>
                                </select>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-6 text-center">

                        <div class="form-group">
                            <label class="control-label col-md-12" style="text-align: center;">Your Message to us:</label>
                            <div  class="col-md-12">
                                <textarea class="form-control " id="count_char" style="height:192px;"></textarea>
                                <p id="counter" class="text-right">0 Characters left</p>
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-center">
                        <div  class="col-md-12">
                            <button type="submit" class="default_btn" style="padding: 8px 30px;">Send</button>
                        </div>
                    </div>
                </div>                
            </div>    
        </form>
    </div>
</div>    
