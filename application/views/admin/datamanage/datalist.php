<div class="row">
    <div class="col-md-12">
        <form id="userdropdown" method="post" class="form-horizontal form-row-seperated" action="<?php echo base_url(); ?>admin/dropdownmanage/adduserdetail">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-shopping-cart"></i>Add state city country
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-3">
                            <span class="box_title">Country</span>

                            <div class="list_box radio-list">
                                <ul>
                                    <?php foreach ($countrylist as $row) { ?>
                                        <li class="country_sel" data-id="<?php echo $row['int_glcode']; ?>"><?php echo $row['var_country']; ?></li>
                                    <?php } ?>
                                </ul>
                            </div>
                            <!--                            <div class="radio-list list_box">
                            <?php //foreach ($countrylist as $row) {?>
                                                            <label>
                                                                <div class="radio"><input type="radio" class='country_check' value="<?php // echo $row['var_country'];  ?>" data-id="<?php // echo $row['int_glcode'] ?>" name="country"></div>&nbsp;<?php // echo $row['var_country']  ?></label>
                            <?php //} ?>
                                                        </div>-->

                            <div class="text_btn_box">
                                <div class="text_add">
                                    <input type="text" name="country" class="form-control country" value="" > 
                                </div>
                                <div class="add_delete_links">
                                    <a href="javascript:;" class="btn btn-sm green countryadd" ><i class="fa fa-plus"></i></a>
                                    <a href="javascript:;" class="btn btn-sm purple countryedit" style="display: none;" ><i class="fa fa-pencil"></i></a>
                                    <a href="javascript:;" class="btn btn-sm red countrydelete" ><i class="fa fa-times"></i></a>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <span class="box_title">State</span>
                            <div class="state_list">
                                <div class="list_box radio-list">
                                    <ul>
                                        <?php foreach ($statelist as $row) { ?>
                                            <li class="state_sel" data-id="<?php echo $row['int_glcode']; ?>"><?php echo $row['var_state']; ?></li>
                                        <?php } ?>
                                    </ul>
                                </div>
<!--                                <div class="radio-list list_box">
                                    <?php// foreach ($statelist as $row) { ?>
                                        <label>
                                            <div class="radio">
                                                <input type="radio" class="state_check" value="<?php //echo $row['var_state']; ?>" data-id="<?php// echo $row['int_glcode'] ?>" name="state">
                                            </div>&nbsp;<?php //echo $row['var_state']; ?>
                                        </label>
                                    <?php //} ?>
                                </div>-->
                            </div>
                            <div class="text_btn_box">
                                <div class="text_add">
                                    <input type="text" name="state" class="form-control state" disabled=""> 
                                </div>
                                <div class="add_delete_links">
                                    <a href="javascript:;" class="btn btn-sm green stateadd"><i class="fa fa-plus"></i></a>
                                    <a href="javascript:;" class="btn btn-sm purple stateedit" style="display: none;" ><i class="fa fa-pencil"></i></a>
                                    <a href="javascript:;" class="btn btn-sm red statedelete" ><i class="fa fa-times"></i></a>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <span class="box_title">City</span>
                            <div class="city_list">
                                <div class="list_box radio-list">
                                    <ul>
                                        <?php foreach ($citylist as $row) { ?>
                                            <li class="city_sel" data-id="<?php echo $row['int_glcode']; ?>"><?php echo $row['var_city']; ?></li>
                                        <?php } ?>
                                    </ul>
                                </div>
<!--                                <div class="radio-list list_box">
                                    <?php //foreach ($citylist as $row) { ?>
                                        <label>
                                            <div class="radio"><input type="radio" class="city_check" value="<?php //echo $row['var_city']; ?>" data-id="<?php// echo $row['int_glcode'] ?>" name="city"></div>&nbsp;<?php //echo $row['var_city']; ?></label>
                                    <?php// } ?>
                                </div>-->
                            </div>
                            <div class="text_btn_box">
                                <div class="text_add">
                                    <input type="text" name="city" class="form-control city" disabled=""> 
                                </div>
                                <div class="add_delete_links">
                                    <a href="javascript:;" class="btn btn-sm green cityadd"><i class="fa fa-plus"></i></a>
                                    <a href="javascript:;" class="btn btn-sm purple cityedit" style="display:none;"><i class="fa fa-pencil"></i></a>
                                    <a href="javascript:;" class="btn btn-sm red citydelete" ><i class="fa fa-times"></i></a>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <span class="box_title">Hotel</span>
                            <div class="hotel_list">
                                <div class="list_box radio-list">
                                    <ul>
                                        <?php foreach ($hotellist as $row) { ?>
                                            <li class="hotel_sel" data-id="<?php echo $row['int_glcode']; ?>"><?php echo $row['var_hotelname']; ?></li>
                                        <?php } ?>
                                    </ul>
                                </div>
<!--                                <div class="radio-list list_box">
                                    <?php //foreach ($hotellist as $row) { ?>
                                        <label>
                                            <div class="radio"><input type="radio" class="hotel_check" value="<?php // echo $row['int_glcode']; ?>" name="hotel"></div>&nbsp;<?php //echo $row['var_hotelname']; ?></label>
                                    <?php // } ?>
                                </div>-->
                            </div>
                            <div class="text_btn_box">
                                <div class="text_add">
                                    <input type="text" name="hotel" class="form-control hotel" disabled="true"> 
                                </div>
                                <div class="add_delete_links">
                                    <a href="javascript:;" class="btn btn-sm green hoteladd"><i class="fa fa-plus"></i></a>
                                    <a href="javascript:;" class="btn btn-sm purple hoteledit" style="display:none;"><i class="fa fa-pencil"></i></a>
                                    <a href="#delete_hotel" data-toggle="modal" class="btn btn-sm red hoteldelete" ><i class="fa fa-times"></i></a>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<div id="delete_hotel" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Delete Hotel Data</h4>
            </div>
            <div class="modal-body">
                <h4>Are you Sure to Delete ?...</h4>
            </div>
            <div class="modal-footer" >
                <button type="button" class="btn btn-default" data-dismiss="modal" >Close</button>
                <button type="submit" id="btndelete" class="btn red"><i class="fa fa-check"></i>Delete</button>
            </div>
        </div>
    </div>
</div>
