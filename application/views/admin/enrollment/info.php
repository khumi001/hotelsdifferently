<div class="row">
  <div class="col-md-3 col-sm-2 "><strong> Company Name:</strong> </div>
  <div class="col-md-3 col-sm-2 ">
    <?=$data->company_name?>
  </div>
  <div class="col-md-3 col-sm-2 "><strong>Company Type:</strong> </div>
  <div class="col-md-3 col-sm-2 ">
    <?=$data->company_type_name?>
  </div>
</div>


<div class="row">
  <div class="col-md-3 col-sm-2 "><strong> Company Website:</strong> </div>
  <div class="col-md-3 col-sm-2 ">
    <?=$data->website?>
  </div>
  <div class="col-md-3 col-sm-2 "><strong>Company phone:</strong> </div>
  <div class="col-md-3 col-sm-2 ">
    <?=$data->phone_country_code.'-'.$data->phone_number.' '.$data->phone_ex?>
  </div>
</div>

<div class="row">
  <div class="col-md-3 col-sm-2 "><strong> Country:</strong> </div>
  <div class="col-md-3 col-sm-2 ">
    <?=$data->country_name?>
  </div>
  <div class="col-md-3 col-sm-2 "><strong> State:</strong> </div>
  <div class="col-md-3 col-sm-2 ">
    <?=$data->state_name?>
  </div>
</div>

<div class="row">
  <div class="col-md-3 col-sm-2 "><strong> City:</strong> </div>
  <div class="col-md-3 col-sm-2 ">
    <?=$data->city?>
  </div>
  <div class="col-md-3 col-sm-2 "><strong>Street:</strong> </div>
  <div class="col-md-3 col-sm-2 ">
    <?=$data->street?>
  </div>
</div>


<div class="row">
  <div class="col-md-3 col-sm-2 "><strong> Company owner:</strong> </div>
  <div class="col-md-3 col-sm-2 ">
    <?=$data->company_owner?>
  </div>
  <div class="col-md-3 col-sm-2 "><strong>IATA:</strong> </div>
  <div class="col-md-3 col-sm-2 ">
    <?=$data->iata?>
  </div>
</div>

<div class="row">
  <div class="col-md-3 col-sm-2 "><strong> CLIA:</strong> </div>
  <div class="col-md-3 col-sm-2 ">
    <?=$data->clia?>
  </div>
  <div class="col-md-3 col-sm-2 "><strong>ARC:</strong> </div>
  <div class="col-md-3 col-sm-2 ">
    <?=$data->arc?>
  </div>
</div>

<div class="row">
  <div class="col-md-3 col-sm-2 "><strong> Where did you hear about us?:</strong> </div>
  <div class="col-md-3 col-sm-2 ">
    <?=$data->hear_about?>
  </div>
  <?php 
  	if($data->hear_about == 'Other'){
  ?>
  	<div class="col-md-3 col-sm-2 "></div>
  	<div class="col-md-3 col-sm-2 ">
    <?=$data->other_hear?>
  </div>
  <?php } ?>
</div>
<div class="clearfix"></div>
<div class="row">
<div class="compny-information">
    <h2>Contact Information</h2>
</div>
</div>


<div class="row">
  <div class="col-md-3 col-sm-2 "><strong> First Name:</strong> </div>
  <div class="col-md-3 col-sm-2 ">
    <?=$data->contact_f_name?>
  </div>
  <div class="col-md-3 col-sm-2 "><strong>Last Name:</strong> </div>
  <div class="col-md-3 col-sm-2 ">
    <?=$data->contact_l_name?>
  </div>
</div>

<div class="row">
  <div class="col-md-3 col-sm-2 "><strong> Email:</strong> </div>
  <div class="col-md-3 col-sm-2 ">
    <?=$data->email?>
  </div>
  <div class="col-md-3 col-sm-2 "><strong>Cell phone:</strong> </div>
  <div class="col-md-3 col-sm-2 ">
    <?=$data->contact_country_code.'-'.$data->contact_phone?>
  </div>
</div>

<div class="row">
    <div class="col-md-3 col-sm-2 "><strong> Last 4 digits:</strong> </div>
    <div class="col-md-3 col-sm-2 ">
        <?=$data->cardNumber?>
    </div>
</div>
<?php /*if( !empty($_GET['show_4_digit']) ){*/?><!--
<div class="row">
  <div class="col-md-3 col-sm-2 "><strong> 4 digits Code:</strong> </div>
  <div class="col-md-3 col-sm-2 ">
    <input type="text" class="form-control" maxlength="4" value="<?/*=$data->activation_code*/?>">
  </div>
</div> 
--><?php /*}*/?>