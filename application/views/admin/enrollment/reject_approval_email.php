

<div class="main" style="background:url('http://hotels.softech.website/public/affilliate_theme/img/home-bg.jpg');background-repeat:no-repeat;background-size:100% 100%;width:547px;margin:auto">
   <div class="m_3275774443844637159containt-main" style="background:rgba(17,125,200,0.61);color:#fff;width:100%">
      <div class="m_first">
         <div class="left" style="margin:auto;text-align:center;float:none">
            <a class="logo" href="<?php echo base_url(); ?>" style="display:inline-block;margin:10px 0 0 10px" target="_blank">
            <img src="http://hotels.softech.website/public/affilliate_theme/img/logo.png" style="width:130px" class="CToWUd">
            </a>
         </div>
         <div class="m_clear" style="clear:both"></div>
      </div>
      <div class="top-containt">
         <h1 style="max-width:350px;margin:20px auto"><img style="display:block;width:100%;height:auto" src="http://hotels.softech.website/public/affilliate_theme/img/txt-email.png" alt="" class="CToWUd"></h1>
      </div>
      <div class="top-hotel" style="width:90%;margin:40px auto auto auto;background:#0c5fa7;color:#ffffff">
         <div style="padding-bottom:10px;padding-top:10px;width:90%;margin:auto;font-family:'Open Sans',sans-serif;color:#fff!important;font-size:12px;line-height:16px">
            <p style="color:#fff;margin-bottom:10px">Dear 
               <strong style="color:#fff"><?=$data['company_name'];?></strong>,
            </p>
            <p style="color:#fff;margin-bottom:10px">
               We just wanted to let you know that you either failed to complete the authorization form within <strong>30 days <u>and/or</u></strong> failed to provide the scanned version of your credit card <strong><u>and/or</u></strong> failed to provide your ID card/passport. 
               If you think this was a mistake, please reach out to us via the Contact Us tab on <a href="<?php echo base_url(); ?>" style="color:#fff;text-decoration:underline" target="_blank"><u>www.WHotelsGroup.com</u></a> or should you wish to try again please click on this link below:<br /><br />
               <a href="<?php echo base_url(); ?>travel-professional-signup" style="color:#fff;text-decoration:underline;" target="_blank"><u>www.WHotelsGroup.com/Travel-Professional-signup</u></a>
            </p>
            <p style="color:#fff;margin-bottom:10px">
               Thank you for choosing <strong>WHotelsGroup.com</strong> where we are <i>Where  better deals are made for YOU!</i>
            </p>
            <p style="color:#fff"><i>Sincerely,</i><br>
               <strong style="color:#fff">
               <a href="<?php echo base_url(); ?>" style="color:#fff;text-decoration:none" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=<?php echo base_url(); ?>&amp;source=gmail&amp;ust=1491043630695000&amp;usg=AFQjCNGJeZMVLW2F_9PD-Xf6jcoG7wXK-Q">
               <b><span class="il">Wholesale Hotels Group</span></b>
               <sup>sm</sup>
               </a>
               </strong><br>
               <i style="font-size:10px">Where  
               <span class="il">better </span> 
                deals are made for YOU!
               <sup>sm</sup>
               </i>
            </p>
            <p style="color:#fff;margin-bottom:10px">
               <strong>PS: This inbox is NOT monitored, please do NOT send us any emails here.</strong>
            </p>

            <div class="yj6qo"></div>
            <div class="clear adL" style="clear:both"></div>
         </div>
         <div class="adL"></div>
      </div>
      <div class="adL"></div>
      <div class="adL" style="text-align:center;padding:10px 0"></div>
      <div class="adL"></div>
      <div style="font-size:11px;color:#fff;padding:5px;font-family:'Open Sans',sans-serif;display:block" class="adL">&nbsp;</div>
      <div class="adL"></div>
   </div>
   <div class="adL"></div>
</div>