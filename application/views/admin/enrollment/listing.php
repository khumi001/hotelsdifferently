<?php
$i = 1;
?>
<div class="row" id="log">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-user"></i><?= $Heading ?>
                </div>
            </div>

            <div class="portlet-body">
                <div id="mainlog">
                    <table class="table table-striped table-bordered table-hover" id="sample_2">
                        <thead>
                        <tr>
                            <th>
                                Date/Time
                            </th>
                            <th>
                                Company Name
                            </th>
                            <th>
                                Type
                            </th>
                            <th>
                                Site
                            </th>
                            <th>
                                Phone
                            </th>
                            <th>
                                Contact
                            </th>
                            <th>
                                Email
                            </th>
                            <th>
                                Cell
                            </th>
                            <th>
                                Referred
                            </th>
                            <?php if (!isSkipLogin()) {?>
                                <th>
                                    Action
                                </th>
                            <?php }?>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($users->result() as $usersData) { ?>
                            <tr id="tr_<?= $usersData->int_glcode ?>">
                                <td><?= date(DISPLAY_DATE_FORMAT_FULL, strtotime($usersData->created_date)); ?></td>
                                <td><?= $usersData->company_name ?></td>
                                <td><?= $usersData->company_type_name ?></td>
                                <td><?= $usersData->website ?></td>
                                <td><?= $usersData->phone_number ?></td>
                                <td><?= $usersData->contact_f_name . ' ' . $usersData->contact_l_name ?></td>
                                <td><?= $usersData->email ?></td>
                                <td><?= $usersData->contact_phone ?></td>
                                <td><?= $usersData->hear_about ?></td>
                                <?php if (!isSkipLogin()) {?>
                                    <td><a data-toggle="modal" data-target="#detailModal"
                                           onclick="getUserInfo('<?= site_url('admin/enrollment/info/' . $usersData->int_glcode) ?>','<?= $usersData->int_glcode ?>')"
                                           href="javascript:void(0)">Details</a> <?php if ($action == 'true') { ?>|
                                            <button type="button" data-id="36" class="btn default btn-xs green"
                                                    onClick="updateStatusApproved('<?=$usersData->int_glcode?>')">Approve
                                            </button> | <a href="javascript:void(0)"
                                                           onClick="updateStatusReject('<?= $usersData->int_glcode ?>')">
                                                Decline</a><?php
                                        }
                                        if (isset($can_delete) && $can_delete == 'true') {
                                            ?>
                                            | <a href="javascript:void(0)"
                                                 onClick="deleteUser('<?= $usersData->int_glcode ?>')">Delete</a>
                                        <?php } ?>
                                    </td>
                                <?php }?>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>

            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>

    <div id="detailModal" class="modal fade" role="dialog" aria-hidden="true">
        <div class="modal-dialog" style="width: 800px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">User Detail</h4>
                </div>
                <div class="modal-body" id="model_load_content">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" id="close_model" data-dismiss="modal">Close</button>
                    <button data-toggle="modal" data-target="#deleteExtendModal" type="button" class="btn btn-default" >Edit</button>
                </div>
            </div>
        </div>
    </div>

    <div id="deleteExtendModal" class="modal fade" role="dialog" aria-hidden="true">
        <div class="modal-dialog" style="width: 800px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Please enter password</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group furture-form-style">
                        <label>Password:*</label>
                        <input type="password" class="form-control form-style" id="delete_password_extend" >
                    </div>
                </div>
                <div class="modal-footer" >
                    <button type="button" class="btn btn-default" id="close_model" data-dismiss="modal" >Close</button>
                    <button type="button" class="btn btn-default" onClick="extendCheck()" >Edit</button>
                </div>
            </div>
        </div>
    </div>

    <div id="editModal" class="modal fade" role="dialog" aria-hidden="true">
        <div class="modal-dialog" style="width: 800px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Travel Professional Edit</h4>
                </div>
                <div class="modal-body" id="model_load_content_edit">

                </div>
                <div class="modal-footer" >
                    <button type="button" class="btn btn-default" id="close_model" data-dismiss="modal" >Close</button>
                    <button type="button" class="btn btn-default" onClick="validateForm()" >Update</button>
                </div>
            </div>
        </div>
    </div>

    <div id="delete-model" class="modal fade" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Delete Enrollment</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <input type="text" value="" id="del_code_password" class="form-control"
                               placeholder="Please enter password">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" id="close_model" data-dismiss="modal">Close</button>
                    <button type="submit" id="btndelete" class="btn red" onClick="deleteEnrollment()"><i
                            class="fa fa-check"></i>Delete
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div id="deleteApprovalModal" class="modal fade" role="dialog" aria-hidden="true">
        <div class="modal-dialog" style="width: 800px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Please enter password</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group furture-form-style">
                        <label>Password:*</label>
                        <input type="password" class="form-control form-style" id="approval_delete_password" >
                    </div>
                </div>
                <div class="modal-footer" >
                    <button type="button" class="btn btn-default" id="close_model" data-dismiss="modal" >Close</button>
                    <button type="button" class="btn btn-default" onClick="approvalCall()" >Ok</button>
                </div>
            </div>
        </div>
    </div>

    <div id="deleteDeclineModal" class="modal fade" role="dialog" aria-hidden="true">
        <div class="modal-dialog" style="width: 800px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Please enter password</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group furture-form-style">
                        <label>Password:*</label>
                        <input type="password" class="form-control form-style" id="decline_delete_password" >
                    </div>
                </div>
                <div class="modal-footer" >
                    <button type="button" class="btn btn-default" id="close_model" data-dismiss="modal" >Close</button>
                    <button type="button" class="btn btn-default" onClick="declineCall()" >Ok</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        var pass = '<?php echo PASS_TO_DELETE; ?>';
        function getUserInfo(_url,uid) {
            detailUid = uid;
            $.ajax({
                type: "GET",
                url: _url,
                data: {show_4_digit: 1},
                cache: false,
                success: function (result) {
                    $('#model_load_content').html(result);
                }
            });
        }
        var deleteUserId;
        var detailUid;
        function deleteUser(uid) {
            deleteUserId = uid;
            $('#delete-model').modal('show');
            /*if(window.confirm('Are you sure you want to Delete this user?')){
             var dataString = 'uid='+uid;
             $.ajax({
             type: "POST",
             url: "
            <?=site_url('admin/enrollment/deleteuser')?>",
             data: dataString,
             cache: false,
             success: function(result){
             if($.trim(result) == 'success'){
             window.location.reload();
             }
             },
             error: function (request, status, error) {}
             });
             }*/
        }

        function extendCheck(){
            if($('#delete_password_extend').val() == pass){
                $('#deleteExtendModal').modal('toggle');
                editUserInfo();
            }else{
                alert('Sorry! Password is wrong.');
            }
        }

        function editUserInfo(){
            var _url = "<?=site_url('admin/enrollment/edit')?>/"+detailUid;
            $.ajax({
                type: "POST",
                url: _url,
                data: {},
                cache: false,
                success: function(result){
                    $('#model_load_content_edit').html(result);
                    $('#editModal').modal('toggle');
                }
            });
        }

        function deleteEnrollment() {
            var del_password = $('#del_code_password').val();
            if (del_password != 'bUw#kef5ate$U8a2') {
                alert('Please enter correct password.');
                return false;
            }
            else {
                var dataString = 'uid=' + deleteUserId;
                $.ajax({
                    type: "POST",
                    url: "<?=site_url('admin/enrollment/deleteuser')?>",
                    data: dataString,
                    cache: false,
                    success: function (result) {
                        if ($.trim(result) == 'success') {
                            window.location.reload();
                        }
                    },
                    error: function (request, status, error) {
                    }
                });
            }

        }
    </script>
<?php if ($action == 'true') { ?>
    <!--<div id="approve-model" class="modal fade" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Approve Enrollment</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <input type="text" maxlength="4" value="" id="u_code" class="form-control"
                               placeholder="4 digits (numbers only).">
                        <input type="hidden" id="uid" value="">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" id="close_model" data-dismiss="modal">Close</button>
                    <button type="submit" id="btndelete" class="btn green" onClick="updateStatusApproved()"><i
                            class="fa fa-check"></i>Approve
                    </button>
                </div>
            </div>
        </div>
    </div>-->

    <script>
        var dataString = '';
        function updateStatusReject(uid) {
            var status = 'R';
            //if (window.confirm('Are you sure you want to Reject this user?')) {
            dataString = 'uid=' + uid + '&status=' + status;
            $('#deleteDeclineModal').modal('show');
            ///}
        }

        function declineCall(){
            if($('#decline_delete_password').val() == pass){
                $.ajax({
                    type: "POST",
                    url: "<?=site_url('admin/enrollment/update_approval')?>",
                    data: dataString,
                    cache: false,
                    success: function (result) {
                        if ($.trim(result) == 'success') {
                            //$('#tr_' + uid).remove();
                            window.location.reload();
                        }
                    },
                    error: function (request, status, error) {
                    }
                });
            }else{
                alert('Sorry! Password is wrong.');
            }
        }

        function updateStatusApproved(uid) {
            var status = 'A';
            //var code = $('#u_code').val();
            //var uid = $('#uid').val();
            /*if (code.length != 4) {
             alert('Please enter 4 digits for code.');
             return false;
             }*/
            dataString = 'uid=' + uid + '&status=' + status;
            $('#deleteApprovalModal').modal('show');
        }

        function approvalCall(){
            if($('#approval_delete_password').val() == pass){
                $.ajax({
                    type: "POST",
                    url: "<?=site_url('admin/enrollment/update_approval')?>",
                    data: dataString,
                    cache: false,
                    success: function (result) {
                        if ($.trim(result) == 'success') {
                            $('#close_model').trigger('click');
                            window.location.reload();
                        }
                    },
                    error: function (request, status, error) {
                    }
                });
            }else{
                alert('Sorry! Password is wrong.');
            }
        }
    </script>
<?php } ?>