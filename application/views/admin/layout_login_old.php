<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.1.1
Version: 2.0.2
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title>Dealz Admin Panel</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <meta content="Webfirm" name="description"/>
        <meta content="Webfirm" name="author"/>

        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>public/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>public/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>public/assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/assets/plugins/select2/select2.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/assets/plugins/select2/select2-metronic.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/assets/plugins/bootstrap-toastr/toastr.min.css"/>
        <link rel="stylesheet" href="<?php echo base_url(); ?>public/assets/plugins/bootstrap-datepicker/css/datepicker.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>public/assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>public/assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css" />
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME STYLES -->
        <link href="<?php echo base_url(); ?>public/assets/stylesheets/style-metronic.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>public/assets/stylesheets/style.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>public/assets/stylesheets/style-responsive.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>public/assets/stylesheets/plugins.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>public/assets/stylesheets/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
        <link href="<?php echo base_url(); ?>public/assets/stylesheets/pages/login-soft.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>public/assets/stylesheets/custom.css" rel="stylesheet" type="text/css"/>
        <!-- END THEME STYLES -->
        <link rel="shortcut icon" href="favicon.ico"/>
        <script>
            var baseurl = "<?php echo base_url(); ?>";
        </script>

    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
    <!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
    <!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
    <!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
    <!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
    <!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
    <!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
    <!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
    <!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->

    <body class="login">
        <!-- BEGIN LOGO -->
        <div class="logo">
            <a href="<?php echo base_url(); ?>admin">
                <img src="<?php echo base_url(); ?>public/assets/img/logo.png" alt="Hotels" width="114"/>
            </a>
        </div>
        <!-- END LOGO -->
        <?php echo $this->load->view($page); ?>
        <!-- BEGIN COPYRIGHT -->
        <!--<div class="copyright">
            2014 &copy; Dealz Inc.
        </div>-->
        <!-- END COPYRIGHT -->
        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
        <!-- BEGIN CORE PLUGINS -->
        <!--[if lt IE 9]>
                <script src="<?php echo base_url(); ?>public/assets/plugins/respond.min.js"></script>
                <script src="<?php echo base_url(); ?>public/assets/plugins/excanvas.min.js"></script> 
                <![endif]-->
        <script src="<?php echo base_url(); ?>public/assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>public/assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>public/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>public/assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>public/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>public/assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>public/assets/plugins/jquery.cokie.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>public/assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?php echo base_url(); ?>public/assets/plugins/jquery-validation/dist/jquery.validate.min.js" type="text/javascript"></script>
        <!--<script src="<?php echo base_url(); ?>public/assets/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>-->
        <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/plugins/select2/select2.min.js"></script>
        <script src="<?php echo base_url(); ?>public/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>public/assets/plugins/bootstrap-daterangepicker/moment.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>public/assets/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>public/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/plugins/bootstrap-toastr/toastr.min.js"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?php echo base_url(); ?>public/assets/javascripts/core/app.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>public/assets/javascripts/custom/login-soft.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>public/assets/javascripts/common_function.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>public/assets/javascripts/custom/components-pickers.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->                
        <script>
            jQuery(document).ready(function() {
                App.init();
                Login.init();
                ComponentsPickers.init_datepicker();
            });
        </script>
        <!-- END JAVASCRIPTS -->
    </body>  
    <!-- END BODY -->
</html>