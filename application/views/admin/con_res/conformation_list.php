<style>
    .portlet-body.data-grid-custom .col-sm-12.col-md-3 {
  display: none;
}
 .portlet-body.data-grid-costom .col-sm-12.col-md-3 {
  display: none;
}
.portlet-body.data-grid-costom .form-control.input-small.input-inline {
  width: 275px !important;
}
.control-label{text-align: center}
 .input-small {
        width: 245px !important;
    }
    .table-condensed > thead > tr > th, .table-condensed > tbody > tr > th, .table-condensed > tfoot > tr > th, .table-condensed > thead > tr > td, .table-condensed > tbody > tr > td, .table-condensed > tfoot > tr > td {
        padding: 4px;
    }
    .datepicker {
        width: 195px !important;
    }
    
</style>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-user"></i>Confirmed Reservation
                </div>
            </div>
            <div class="portlet-body data-grid-custom">
                <div class="form-group">
                    <label class="checkbox" style="padding: 0px; margin:6px 0 0;float: left;">
                        <input type="radio" style="margin-top: -2px;margin-left: 10px; padding-left:0;" id="filter" name="filter"  checked="checked"  class="radio-inline filter" value="-1">&nbsp;ALL
                    </label>
                    <label class="checkbox" style="padding: 0px; margin:6px 0 0;float: left;"><input type="radio" style="margin-top: -2px;margin-left: 10px; padding-left:0;" id="filter3" name="filter" class="radio-inline filter" value="1">&nbsp;ACTIVE </label>
                    <label class="checkbox" style="padding: 0px;  margin:6px 0 0;float: left;"><input type="radio" style="margin-top: -2px;margin-left: 10px; padding-left:0;" id="filter1" name="filter" class="radio-inline filter" value="2">&nbsp;EXPIRED </label>
                </div>
                <div class="clearfix"></div>
                <table class="table table-striped table-bordered table-hover "  id="sample_2">
                    <thead>
                        <tr>
                            <th>Date/Time</th>
                            <th>Check-in</th>
                            <th>Check-out</th>
                            <th>Last 4</th>
                            <th>Hotel name</th>
                            <th>Email address</th>
                            <th>Account ID</th>
                            <th>Confirmation #</th>
                            <th>PAID</th>
                            <th style="text-align:center;">Source</th>
                            <th>Phone number</th>
                            <th>Source conf </th>
                            <th>Invoice</th>
                            <th>BKG</th>
                        </tr>
                    </thead>
                    <tbody id="resultBox">
						<?php 
							foreach($confReservations as $res)
							{
								$bookUser = getUserById($res->uid);
								?>
								<tr>
									<td><?php echo date(DISPLAY_DATE_FORMAT_FULL, strtotime(getBookingDateTime($res->book_id))); ?></td>
									<td><?php echo getBookingcheckIn($res->book_id); ?></td>
									<td><?php echo getBookingcheckOut($res->book_id); ?></td>
									<td><?php echo getBookingcardNo($res->book_id); ?></td>
									<td><?php echo getBookinghotelName($res->book_id); ?></td>
									<td><?php echo $bookUser->var_email; ?></td>
									<td><?php echo $bookUser->var_accountid; ?></td>
									<td><?php echo $res->confirm_no; ?></td>
									<td><?php echo "$".getBookingperRoomPaid($res->book_id); ?></td>
									<td>
										<input type="text" <?php if(isSkipLogin()){ ?> readonly="true"  <?php } ?> name="sourceName<?php echo $res->id; ?>" id="sourceName<?php echo $res->id; ?>" class="form-control" style="height: 33px; width: 140px; text-align: center;" value="<?php echo getSourcename($res->sourceID); ?>"  onfocus="this.placeholder = ''">
										<input type='hidden' name='sourceId<?php echo $res->id; ?>' id='sourceId<?php echo $res->id; ?>'>
                                        <script type="text/javascript">
                                            jQuery(document).ready(function () {
                                                jQuery("#sourceName<?php echo $res->id; ?>").autocomplete({
                                                    source: function (request, response)
                                                    {
                                                        jQuery.getJSON(baseurl + "admin/conf_res/get_sources", {term: jQuery("#sourceName<?php echo $res->id; ?>").val()},
                                                            response);
                                                    },
                                                    minLength: 3,
                                                    select: function (event, ui)
                                                    {
                                                        $("#sourceId<?php echo $res->id; ?>").val(ui.item.id);
                                                        change_source(<?php echo $res->id; ?> , ui.item.id , ui.item.shortName);
                                                        $("#sourcenum<?php echo $res->id; ?>").text("("+ui.item.phone+")");
                                                    }
                                                });
                                            });
                                        </script>
                                    </td>
									<td id="sourcenum<?php echo $res->id; ?>">
										<?php echo "(".getSourcePhone($res->sourceID).")"; ?>
									</td>
									<td>
										<input  <?php if(isSkipLogin()){ ?> readonly="true"  <?php } ?> type="text" value="<?php echo $res->reservationId; ?>" style="width:76px;float:left;" id="reserv_id<?php echo $res->id; ?>" />
										<div <?php if(!isSkipLogin()){ ?> onclick="change_res(<?php echo $res->id; ?>)"  <?php } ?>><i style="background: #dbdbe2;padding: 4.3px;margin-top: -0.3px;border: 1px solid;" class="fa fa-pencil" aria-hidden="true"></i></div>
									</td>
									<td>
										<?php 
											echo '<a target="_blank" href="'.base_url().'front/quote_request/pdf/'.$res->book_id.'"><img src="http://iconbug.com/data/5b/507/52ff0e80b07d28b590bbc4b30befde52.png" style="width:30px" /></a>';
										?>
									</td>
									<td><?php echo getHotelRoomsCount($res->book_id); ?></td>
								</tr>
								<?php
							}
						?>
                    </tbody>
                </table>
                <div id="deleteModal" class="modal fade" role="dialog" aria-hidden="true">
                    <div class="modal-dialog" style="width: 800px;">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                <h4 class="modal-title">Please enter password</h4>
                            </div>
                            <div class="modal-body">
                                <div class="form-group furture-form-style">
                                    <label>Password:*</label>
                                    <input type="password" class="form-control form-style" id="delete_password" >
                                </div>
                            </div>
                            <div class="modal-footer" >
                                <button type="button" class="btn btn-default" id="close_model" data-dismiss="modal" >Close</button>
                                <button type="button" class="btn btn-default" onClick="changeResCall()" >Ok</button>
                            </div>
                        </div>
                    </div>
                </div>
				<script type="text/javascript">
                    var PASS_TO_DELETE = '<?php echo PASS_TO_DELETE; ?>';
                    var value =0;
                    function change_res(val)
                    {
                        $('#loading').show();
                        value = val;
                        $("#deleteModal").modal('show');
                        //admin_changed
                    }

                    function changeResCall(){
                        if($('#delete_password').val() == PASS_TO_DELETE){
                        $.ajax({
                            type: 'post',
                            url: "<?php echo base_url();?>" + 'admin/conf_res/changeTransectionNum/'+value,
                            data: {'nconf': $('#reserv_id'+value).val()},
                            success: function(output) {
                                $('#loading').hide();
                                if(output)
                                {
                                    $("#deleteModal").modal('hide');
                                    alert("Updated");
                                }
                                else
                                {
                                    alert("Error in updation!");
                                }
                            }
                        });
                        }else{
                            alert('Sorry! Password is wrong.');
                        }
                    }

                    $(document).on('change', '.filter', function() {
                        $('.filter').removeAttr('checked');
                        $(this).attr('checked', 'checked');
                        var status = $(this).val();
                        $('#loading').show();
                        $.ajax({
                            type: 'post',
                            url: "<?php echo base_url();?>" + 'admin/conf_res/getFilterResult/',
                            data: {status: status},
                            success: function(output) {
                                $('#loading').hide();
                                if(output)
                                {
                                    var dataTable = $("#sample_2").dataTable();
                                    dataTable.fnClearTable();
                                    dataTable.fnDraw();
                                    dataTable.fnDestroy();
                                    $("#resultBox").html(output);
                                    // to reload
                                    //dataTable.ajax.reload();
                                    refreshTable();
                                }

                                else
                                {
                                    alert("Something went wrong. Please try again");
                                }
                            }
                        });
                    });
				</script>
<!--                <table class="table table-striped table-bordered table-hover" id="sample_2">
                    <thead>
                        <tr>
                            <th>
                                Date/Time
                            </th>
                            <th>
                                Username
                            </th>
                            <th>
                                Email address
                            </th>
                            <th>
                                Account ID
                            </th>
                            <th>
                                Confirmation number 
                            </th>
                            <th>
                                Paid amount 
                            </th>
                            <th>
                                View
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($conform_res as $row) { ?>
                            <tr>
                                <td>
                                    <?php echo $row['var_createddate']; ?>
                                </td>
                                <td>
                                    <?php echo $row['var_username']; ?>
                                </td>
                                <td>
                                    <?php echo $row['var_email']; ?>
                                </td>
                                <td>
                                    <?php echo $row['var_accountid']; ?>
                                </td>
                                <td>        
                                    <?php echo $row['var_transactionid']; ?>
                                </td>
                                <td>
                                    <?php echo $row['var_paid_amount']; ?>
                                </td>
                                <td>
                                    <a href="#myModal_autocomplete" data-toggle="modal" data-id="<?php echo $row['int_fkquote']; ?>" id="conform_list" class="sentquote label label-sm label-success">View</a>
                                </td>
                            </tr>
                        <?php } ?>   
                    </tbody>
                </table>-->
            </div>
        </div>
    </div>
</div>

<div id="myModal_autocomplete" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog" style="width:80%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"> Confirmed Reservations </h4>
            </div>
            <form method="post" id="add_quoterequests" class="form-horizontal"> <div class="portlet-body form">
                    <div class="form-body">
                        <div class="quoterequest">  
                            
                        </div>
                    </div>
                </div>
            </form> 
        </div>                                               
    </div>
</div>              