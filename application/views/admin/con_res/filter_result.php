<?php
foreach($confReservations as $res)
{
    $bookUser = getUserById($res->uid);
    ?>
    <tr>
        <td><?php echo date(DISPLAY_DATE_FORMAT_FULL, strtotime(getBookingDateTime($res->book_id))); ?></td>
        <td><?php echo getBookingcheckIn($res->book_id); ?></td>
        <td><?php echo getBookingcheckOut($res->book_id); ?></td>
        <td><?php echo getBookingcardNo($res->book_id); ?></td>
        <td><?php echo getBookinghotelName($res->book_id); ?></td>
        <td><?php echo $bookUser->var_email; ?></td>
        <td><?php echo $bookUser->var_accountid; ?></td>
        <td><?php echo $res->confirm_no; ?></td>
        <td><?php echo "$".getBookingperRoomPaid($res->book_id); ?></td>
        <td>
            <input type="text" <?php if(isSkipLogin()){ ?> readonly="true"  <?php } ?> name="sourceName<?php echo $res->id; ?>" id="sourceName<?php echo $res->id; ?>" class="form-control" style="height: 33px; width: 140px; text-align: center;" value="<?php echo getSourcename($res->sourceID); ?>"  onfocus="this.placeholder = ''">
            <input type='hidden' name='sourceId<?php echo $res->id; ?>' id='sourceId<?php echo $res->id; ?>'>
        </td>
        <td id="sourcenum<?php echo $res->id; ?>">
            <?php echo "(".getSourcePhone($res->sourceID).")"; ?>
        </td>
        <td>
            <input  <?php if(isSkipLogin()){ ?> readonly="true"  <?php } ?> type="text" value="<?php echo $res->reservationId; ?>" style="width:76px;float:left;" id="reserv_id<?php echo $res->id; ?>" />
            <div <?php if(!isSkipLogin()){ ?> onclick="change_res(<?php echo $res->id; ?>)"  <?php } ?>><i style="background: #dbdbe2;padding: 4.3px;margin-top: -0.3px;border: 1px solid;" class="fa fa-pencil" aria-hidden="true"></i></div>
        </td>
        <td>
            <?php
            echo '<a target="_blank" href="'.base_url().'front/quote_request/pdf/'.$res->book_id.'"><img src="http://iconbug.com/data/5b/507/52ff0e80b07d28b590bbc4b30befde52.png" style="width:30px" /></a>';
            ?>
        </td>
        <td><?php echo getHotelRoomsCount($res->book_id); ?></td>
    </tr>
<?php
}
?>