<?php
$i = 1;
//var_dump($log_result);
?>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-user"></i>Admin Activity Log
                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="sample_2">
                    <thead>
                    <tr>
                        <th>
                            Activity Type
                        </th>
                        <th>
                            Activity Detail
                        </th>
                        <th>
                            Activity Time
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($log_result as $single_log) { ?>
                        <tr>
                            <td><?php echo $single_log['type'] ?></td>
                            <td><?php echo $single_log['desc']; ?></td>
                            <td><?php echo date(DISPLAY_DATE_FORMAT, strtotime($single_log['time'])); ?></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>