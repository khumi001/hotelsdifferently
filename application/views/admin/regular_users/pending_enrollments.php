<?php
$i = 1;
?>
<div class="row" id="log">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-user"></i><?= $Heading ?>
                </div>
            </div>

            <div class="portlet-body">
                <div id="mainlog">
                    <table class="table table-striped table-bordered table-hover" id="sample_2">
                        <thead>
                        <tr>
                            <th>
                                Date/Time
                            </th>
                            <th>
                                Full name
                            </th>
                            <th>
                                Email
                            </th>
                            <th>
                                Phone
                            </th>
                            <th>
                                Amount
                            </th>
                            <th>Refunded</th>
                            <?php if (!isSkipLogin()) {?>
                                <th>Actions</th>
                            <?php }?>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($users as $usersData) {
                            ?>
                            <tr <?php if($usersData->is_subscribed == 4 && $usersData->status == 2) echo "style='border: 2px solid red; color: red;'"; ?>  id="tr_<?= $usersData->int_glcode ?>">
                                <td><?= date(DISPLAY_DATE_FORMAT_FULL, strtotime($usersData->startDate)); ?></td>
                                <td><?= $usersData->var_fname . ' ' . $usersData->var_lname ?></td>
                                <td><?= $usersData->var_email ?></td>
                                <td><?= $usersData->var_phone ?></td>
                                <td><?= "$".$usersData->paid ?></td>
                                <td>
                                    <input  <?php if($usersData->is_subscribed == 4 && $usersData->status == 2) echo "disabled"?> <?php if(isSkipLogin()){ ?> readonly="true"  <?php } ?> type="text" value="<?php echo $usersData->refundedAmount;?>" style="width:76px;float:left;" class="priceper" id="reserv_id<?php echo $usersData->id; ?>" />
                                </td>
                                <?php if (!isSkipLogin()) {?>
                                    <td>
                                        <button <?php if($usersData->is_subscribed == 4 && $usersData->status == 2) echo "disabled";?> type="button" data-id="<?php echo "reserv_id".$usersData->id; ?>" class="btn default btn-xs green"
                                                                                                                                       onClick="AddRefundAmount('<?php echo "reserv_id".$usersData->id; ?>','<?php echo $usersData->id; ?>')">OK</button>

                                        <a data-toggle="modal" data-target="#deleteModal" onClick="setDeleteUid('<?=$usersData->int_glcode?>')" href="javascript:void(0)">Delete</a>

                                    </td>
                                <?php }?>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>

            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
    <div id="deleteModal" class="modal fade" role="dialog" aria-hidden="true">
        <div class="modal-dialog" style="width: 800px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Please enter password</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group furture-form-style">
                        <label>Password:*</label>
                        <input type="password" class="form-control form-style" id="delete_password" >
                    </div>
                </div>
                <div class="modal-footer" >
                    <button type="button" class="btn btn-default" id="close_model" data-dismiss="modal" >Close</button>
                    <button type="button" class="btn btn-default" onClick="deleteAccount()" >Ok</button>
                </div>
            </div>
        </div>
    </div>

    <div id="deleteRefundModal" class="modal fade" role="dialog" aria-hidden="true">
        <div class="modal-dialog" style="width: 800px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Please enter password</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group furture-form-style">
                        <label>Password:*</label>
                        <input type="password" class="form-control form-style" id="refund_delete_password" >
                    </div>
                </div>
                <div class="modal-footer" >
                    <button type="button" class="btn btn-default" id="close_model" data-dismiss="modal" >Close</button>
                    <button type="button" class="btn btn-default" onClick="refundCall()" >Ok</button>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">

        var deleteUid;
        var detailUid;
        var amount = 0;
        var mId = 0;
        var value = 0;
        var PASS_TO_DELETE = '<?php echo PASS_TO_DELETE; ?>';

        function setDeleteUid(user_id){
            deleteUid = user_id;
        }
        function deleteAccount(){
            var uid = deleteUid;
            if($('#delete_password').val() == PASS_TO_DELETE){
                var status = 'R';
                $.ajax({
                    type: "POST",
                    url: "<?=site_url('admin/regular_users/delete')?>",
                    data: {userId:uid},
                    cache: false,
                    success: function(result){
                        if($.trim(result) == 'success'){
                            $('#tr_'+uid).remove();
                            $('#deleteModal').modal('toggle');
                        }
                    },
                    error: function (request, status, error) {}
                });
            }else{
                alert('Sorry! Password is wrong.');
            }
        }

        function AddRefundAmount(val, membershipId) {
            $('#loading').show();
            amount = $("#" + val).val();
            mId = membershipId;
            value = val
            $('#deleteRefundModal').modal('show');
            //admin_changed
        }

        function refundCall(){
            if($('#refund_delete_password').val() == PASS_TO_DELETE){
                $.ajax({
                    type: 'post',
                    url: "<?php echo base_url();?>" + 'admin/regular_users/AddRefundAmount/' + value,
                    data: {amount: amount, membershipId: mId},
                    success: function (output) {
                        $('#loading').hide();
                        if (output) {
                            $('#deleteRefundModal').modal('hide');
                            alert("Refunded amount added successfully");
                        }
                        else {
                            alert("Error!");
                        }
                    }
                });
            }else{
                alert('Sorry! Password is wrong.');
            }
        }
        $(document).ready(function() {
            $(".priceper").maskMoney({
                prefix: '',
                allowNegative: true,
                thousands: ',',
                decimal: '.',
                affixesStay: false
            });
        });
    </script>