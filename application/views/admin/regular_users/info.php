<div class="edit-pop">
    <div class="row">
        <div class="col-md-3 col-sm-2 "><strong> User Name:</strong></div>
        <div class="col-md-3 col-sm-2 ">
            <?= $data->var_fname . " " . $data->var_lname ?>
        </div>

        <div class="col-md-3 col-sm-2 "><strong> Email:</strong></div>
        <div class="col-md-3 col-sm-2 ">
            <?= $data->var_email ?>
        </div>
    </div>


    <div class="row">

        <div class="col-md-3 col-sm-2 "><strong>Phone:</strong></div>
        <div class="col-md-3 col-sm-2 ">
            <?= $data->var_phone ?>
        </div>

        <div class="col-md-3 col-sm-2 "><strong> Last 4 Digits:</strong></div>
        <div class="col-md-3 col-sm-2 ">
            <?= $data->cardNumber ?>
        </div>
    </div>

    <div class="row">

        <div class="col-md-3 col-sm-2 "><strong>Date/Time:</strong></div>
        <div class="col-md-3 col-sm-2 ">
            <?=date(DISPLAY_DATE_FORMAT_FULL, strtotime($data->startDate));?>
        </div>

        <div class="col-md-3 col-sm-2 "><strong> Membership Expiration:</strong></div>
        <div class="col-md-3 col-sm-2 ">
            <?= date(DISPLAY_DATE_FORMAT, strtotime($data->endDate));?>
        </div>
    </div>


    <!--<div class="row">
  <div class="col-md-3 col-sm-2 "><strong> Country:</strong> </div>
  <div class="col-md-3 col-sm-2 ">
    <? /*=$data->country_name*/ ?>
  </div>
  <div class="col-md-3 col-sm-2 "><strong> State:</strong> </div>
  <div class="col-md-3 col-sm-2 ">
    <? /*=$data->state_name*/ ?>
  </div>
</div>

<div class="row">
  <div class="col-md-3 col-sm-2 "><strong> City:</strong> </div>
  <div class="col-md-3 col-sm-2 ">
    <? /*=$data->city*/ ?>
  </div>
  <div class="col-md-3 col-sm-2 "><strong>Street:</strong> </div>
  <div class="col-md-3 col-sm-2 ">
    <? /*=$data->street*/ ?>
  </div>
</div>


<div class="row">
  <div class="col-md-3 col-sm-2 "><strong> No of Employees :</strong> </div>
  <div class="col-md-3 col-sm-2 ">
    <? /*=$data->company_peoples*/ ?>
  </div>
</div>
<div class="row">
  <div class="col-md-3 col-sm-2 "><strong> Where did you hear about us?:</strong> </div>
  <div class="col-md-3 col-sm-2 ">
    <? /*=$data->hear_about*/ ?>
  </div>
  <?php /*
  	if($data->hear_about == 'Other'){
  */
    ?>
  	<div class="col-md-3 col-sm-2 "></div>
  	<div class="col-md-3 col-sm-2 ">
    <? /*=$data->other_hear*/ ?>
  </div>
  <?php /*} */ ?>
</div>
<div class="clearfix"></div>
<div class="row">
<div class="compny-information">
    <h2>Contact Information</h2>
</div>
</div>


<div class="row">
  <div class="col-md-3 col-sm-2 "><strong> Full Name:</strong> </div>
  <div class="col-md-3 col-sm-2 ">
    <? /*=$data->contact_full_name*/ ?>
  </div>
  <div class="col-md-3 col-sm-2 "><strong> Email:</strong> </div>
  <div class="col-md-3 col-sm-2 ">
    <? /*=$data->email*/ ?>
  </div>
</div>

<div class="row">
  <div class="col-md-3 col-sm-2 "><strong>Cell phone:</strong> </div>
  <div class="col-md-3 col-sm-2 ">
    <? /*=$data->contact_country_code.'-'.$data->contact_phone*/ ?>
  </div>
  <div class="col-md-3 col-sm-2 "><strong> Employee Email Domain:</strong> </div>
  <div class="col-md-3 col-sm-2 ">
    <? /*=$data->email_domain*/ ?>
  </div>
</div>
<?php /*if(trim($data->entity_sec_code) != ''){*/ ?>
<div class="row">
  <div class="col-md-3 col-sm-2 "><strong>Authorization Code:</strong> </div>
  <div class="col-md-3 col-sm-2 ">
    <? /*=$data->entity_sec_code*/ ?>
  </div>
</div>
<?php /*} */ ?>
</div>-->