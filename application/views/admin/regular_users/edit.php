<form method="post" id="rUpdate" action="<?php echo base_url(); ?>admin/regular_users/update">
    <input type="hidden" name="uid" value="<?= $data->int_glcode ?>">

    <div class="row">
        <div class="col-md-4 col-sm-2 ">
            <div class="form-group">
                <label>First Name:</label>
                <input type="text" class="form-control form-style" name="var_fname" value="<?= $data->var_fname ?>">
            </div>
        </div>
        <div class="col-md-4 col-sm-2 ">
            <div class="form-group">
                <label>Last Name:</label>
                <input type="text" class="form-control form-style" name="var_lname" value="<?= $data->var_lname ?>">
            </div>
        </div>
        <div class="col-md-4 col-sm-2 ">
            <div class="form-group">
                <label>Phone:</label>
                <input type="text" class="form-control form-style" name="var_phone" value="<?= $data->var_phone ?>">
            </div>
        </div>
        <div class="col-md-4 col-sm-2 ">
            <div class="form-group furture-form-style">
                <label>Last 4 Digits:</label>
                <input type="text" maxlength="4" class="form-control form-style" name="cardNumber" value="<?= $data->cardNumber ?>">
            </div>
        </div>
    </div>
</form>
<script>
    function validateForm() {
        $('.has-error').removeClass('has-error');
        var error = false;
        if ($.trim($('input[name="var_fname"]').val()) == '') {
            $('input[name="var_fname"]').parent('.form-group').addClass('has-error');
            error = true;
        }
        if ($.trim($('input[name="var_lname"]').val()) == '') {
            $('input[name="var_lname"]').parent('.form-group').addClass('has-error');
            error = true;
        }
        if ($.trim($('input[name="cardNumber"]').val()) == '' || $.trim($('input[name="cardNumber"]').val()) == 0) {
            $('input[name="cardNumber"]').parent('.form-group').addClass('has-error');
            error = true;
        }
        if (error == false) {
            $('#rUpdate').submit();
        }
    }
</script>