<style>
    .portlet-body.data-grid-custom .col-sm-12.col-md-3 {
        display: none;
    }
    .portlet-body.data-grid-costom .col-sm-12.col-md-3 {
        display: none;
    }
    .portlet-body.data-grid-costom .form-control.input-small.input-inline {
        width: 275px !important;
    }
    .control-label{text-align: center}
    .input-small {
        width: 245px !important;
    }
    .table-condensed > thead > tr > th, .table-condensed > tbody > tr > th, .table-condensed > tfoot > tr > th, .table-condensed > thead > tr > td, .table-condensed > tbody > tr > td, .table-condensed > tfoot > tr > td {
        padding: 4px;
    }
    .datepicker {
        width: 195px !important;
    }

</style>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-user"></i>Pending reservations
                </div>
                <div class="actions">
                </div>
            </div>
            <div class="portlet-body data-grid-custom">
				<?php
					if($this->session->flashdata('success'))
					{
						?>
							<div class="alert alert-success"><?php echo  $this->session->flashdata('success'); ?></div>
						<?php
					}
				?>
				<?php
					if(isset($error))
					{
						?>
							<div class="alert alert-error"><?php echo  $error; ?></div>
						<?php
					}
				?>
                <table class="table table-striped table-bordered table-hover "  id="sample_2">
                    <thead>
                        <tr>
                            <th>Date&Time</th>
                            <th>Booking number</th>
							<th>Check-in</th>
							<th>Check-out</th>
							<th>Rooms</th>
							<th>Hotel Name</th>
                            <th>Email address</th>
                            <th>Account ID</th>
                            <th>Paid amount</th>
                            <th>Invoice</th>
							<?php 
								if(!isSkipLogin())
								{
									?>
									<th>Decision</th>
									<?php
								}
							?>
                        </tr>
                    </thead>
                    <tbody>
						<?php 
							foreach($confReservations as $res)
							{
								$bookUser = getUserById($res->uid);
								?>
								<tr>
									<td><?php echo date(DISPLAY_DATE_FORMAT_FULL, strtotime($res->date_time)); ?></td>
									<td><?php echo $res->codeID; ?></td>
									<td><?php echo date(DISPLAY_DATE_FORMAT, strtotime($res->checkIn));; ?></td>
									<td><?php echo date(DISPLAY_DATE_FORMAT, strtotime($res->checkOut));; ?></td>
									<td><?php echo $res->rooms; ?></td>
									<td><?php echo $res->hotelName; ?></td>
									<td><?php echo $bookUser->var_email; ?></td>
									<td><?php echo $bookUser->var_accountid; ?></td>
									<td><?php echo $res->paid; ?></td>
                                    <td>
                                        <a  target="_blank" href="<?php echo base_url(); ?>front/quote_request/pdf/<?php echo $res->id; ?>"><img src="https://iconbug.com/data/5b/507/52ff0e80b07d28b590bbc4b30befde52.png" width="50" height="50" /></a>
                                    </td>
									<?php 
										if(!isSkipLogin())
										{
											?>
												<td>
												<?php 
													if($res->pending_status == 0)
													{
														?>
														<a style="color:#fff; background:#069709;
														margin:0 0 3px; min-width:60px; text-align:center;" class="btn default btn-xs status" href="<?php echo base_url(); ?>admin/pen_res/index/approve/<?php echo $res->id; ?>">Approve</a>
														<br>
														<a style="color:#fff;margin:0 0 3px; min-width:60px; text-align:center;" class="btn default btn-xs red status" href="<?php echo base_url(); ?>admin/pen_res/index/decline/<?php echo $res->id; ?>">Decline</a>
														<?php
													}
													else
													{
														echo "<font style='color:red;'>Decline</font>";
													}
												?>
												</td>
											<?php
										}
									?>
								</tr>
								<?php
							}
						?>
                    </tbody>
                </table>
				<!-- Modal -->
				<div id="paynow" class="modal fade" role="dialog">
				  <div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Modal Header</h4>
					  </div>
					  <div class="modal-body">
						<form>
							<h4>Payment Method</h4>
							<div class="payment">
								<span class="fa fa-cc-mastercard"></span>
								<span class="fa fa-cc-visa"></span>
								<span class="fa fa-cc-amex"></span>
								<span class="fa fa-cc-jcb"></span>
								<span class="fa fa-cc-discover"></span>
							</div>
							<span>Your information is safe and secure</span>
							<!--
							<p>Your credit card will be charged on the following date: <span class="text-primary">12/21/2015</span></p>
							<div class="form-group">
								<label class="control-label col-sm-3">Card Type:</label>
								<div  class="col-sm-9">
									<select class="select2me form-control input-xlarge" name="data[cardType]" id="" required>
										<option class="" value="">Select</option>
										<option class="" value="51|true">Visa</option>
										<option class="" value="39|true" selected="selected">MasterCard</option>
										<option class="" value="31|true">Discover</option>
										<option class="" value="125|true">JCB</option>
										<option class="" value="11|true">AmericanExpress</option>
									</select>
								</div>
							</div>
							-->
							<div class="form-group">
								<label class="control-label col-sm-3">Card Number:</label>
								<div  class="col-sm-9">
									<input type='text' name='data[cardNumber]' class="form-control input-medium" id='' value='' required>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-sm-3">Expiration:</label>
								<div  class="col-sm-9">
									<div class="row">
										<div class="col-sm-12">
											<select class="select2me form-control input-xlarge" name="data[month]" id="" required>
												<option class="" value="-1" selected="selected">month</option>
												<option class="" value="01">January</option>
												<option class="" value="02">February</option>
												<option class="" value="03">March</option>
												<option class="" value="04">April</option>
												<option class="" value="05">May</option>
												<option class="" value="06">June</option>
												<option class="" value="07">July</option>
												<option class="" value="08">August</option>
												<option class="" value="09">September</option>
												<option class="" value="10">October</option>
												<option class="" value="11">November</option>
												<option class="" value="12">December</option>
											</select>
										</div>
										<div class="col-sm-12">
											<select class="select2me form-control input-xlarge" name="data[year]" id="" required>
												<option class="" value="-1" selected="selected">year</option>
												<option class="" value="2015">2015</option>
												<option class="" value="2016">2016</option>
												<option class="" value="2017">2017</option>
												<option class="" value="2018">2018</option>
												<option class="" value="2019">2019</option>
												<option class="" value="2020">2020</option>
												<option class="" value="2021">2021</option>
												<option class="" value="2022">2022</option>
												<option class="" value="2023">2023</option>
												<option class="" value="2024">2024</option>
												<option class="" value="2025">2025</option>
												<option class="" value="2026">2026</option>
												<option class="" value="2027">2027</option>
												<option class="" value="2028">2028</option>
												<option class="" value="2029">2029</option>
											</select>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-sm-3">Security Code:</label>
								<div  class="col-sm-9">
									<input type='text' name='data[security_code]' class="form-control input-medium" id='' value='' required>
								</div>
							</div>
							<div class="clearfix mt20">
								<button type="submit" class="btn btn-primary pull-right">Continue</button>
							</div>
						</form>
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					  </div>
					</div>

				  </div>
				</div>
<!--                <table class="table table-striped table-bordered table-hover" id="sample_2">
                    <thead>
                        <tr>
                            <th>
                                Date/Time
                            </th>
                            <th>
                                Username
                            </th>
                            <th>
                                Email address
                            </th>
                            <th>
                                Account ID
                            </th>
                            <th>
                                Confirmation number 
                            </th>
                            <th>
                                Paid amount 
                            </th>
                            <th>
                                View
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                <?php foreach ($conform_res as $row) { ?>
                                    <tr>
                                        <td>
                    <?php echo $row['var_createddate']; ?>
                                        </td>
                                        <td>
                    <?php echo $row['var_username']; ?>
                                        </td>
                                        <td>
                    <?php echo $row['var_email']; ?>
                                        </td>
                                        <td>
                    <?php echo $row['var_accountid']; ?>
                                        </td>
                                        <td>
                    <?php echo $row['var_transactionid']; ?>
                                        </td>
                                        <td>
                    <?php echo $row['var_paid_amount']; ?>
                                        </td>
                                        <td>
                                            <a href="#myModal_autocomplete" data-toggle="modal" data-id="<?php echo $row['int_fkquote']; ?>" id="conform_list" class="sentquote label label-sm label-success">View</a>
                                        </td>
                                    </tr>
                <?php } ?>
                    </tbody>
                </table>-->
            </div>
        </div>
    </div>
</div>

<div id="myModal_autocomplete" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog" style="width:80%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"> Pending Reservations </h4>
            </div>
            <form method="post" id="add_quoterequests" class="form-horizontal"> <div class="portlet-body form">
                    <div class="form-body">
                        <div class="quoterequest">  

                        </div>
                    </div>
                </div>
            </form> 
        </div>                                               
    </div>
</div>   
<div id="myModal_paid" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog" style="width:50%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"> Pending reservations </h4>
            </div>
            <form method="post" id="add_paid" class="form-horizontal"> <div class="portlet-body form">
                    <div class="form-body">
                        <div class="">  
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Source:</label>
                                        <div class="col-md-6">
                                            <input type="text" name="source" id="Source" class="form-control">
                                            <input type="hidden" name="editid" id="editid" class="form-control idvalues" value="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Amount:</label>
                                        <div class="col-md-6">
                                            <input type="text" name="amount" id="Amount" class="form-control amount">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Source confirmation:</label>
                                        <div class="col-md-6">
                                            <input type="text" name="source_confirmation" id="Source_confirmation" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer" style="text-align: center">
                                <button type="submit"  class="btn green" >Submit</button>
                                <!--<button type="button" id="approve" class="btn green" style="margin-left:10%;"><i class="fa fa-check"></i> APPROVE</button>-->
                            </div>
                        </div>
                    </div>
                </div>
            </form> 
        </div>                                               
    </div>
</div> 