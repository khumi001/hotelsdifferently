<?php for ($i = 0; $i < count($confirm); $i++) { ?> 
    <!--<h4 style="text-align: center">Confirm Reservation </h4>-->  
<!--    <h4 style="text-align: left">Your reservation confirmation number is <strong><?php echo $confirm[$i]['var_uniq_quoteid']; ?></strong></h4>  
     <h4 style="text-align: left">Once again, here are the detail of the reservation:</h4>  -->
    <div class="row">
            <div class="col-md-4">
                <div class="form-group group_margin">
                    <label class="control-label col-md-4">Check-in:</label>
                    <div class="col-md-7">
                        <label class="control-label" style="padding: 6px 16px 0;"><?php echo date(DISPLAY_DATE_FORMAT, strtotime($confirm[$i]['var_checkin'])); ?></label>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group group_margin">
                    <label class="control-label col-md-4">Check-out:</label>
                    <div class="col-md-5">
                        <label class="control-label" style="padding: 6px 16px 0;"><?php echo date(DISPLAY_DATE_FORMAT, strtotime($confirm[$i]['var_checkout'])); ?></label>
                    </div>

                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group group_margin">
                        <label class="control-label col-md-4">City:</label>
                        <div class="col-md-7">
                            <label class="control-label" style="padding: 6px 16px 0;text-align: left;"><?php echo $confirm[$i]['var_city']; ?></label>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
        <div class="row">
            <div class="col-md-4">
                <div class="form-group group_margin">
                    <label class="control-label col-md-4">Quote ID:</label>
                    <div class="col-md-7">
                        <label class="control-label" style="padding: 6px 16px 0;"><?php echo $confirm[$i]['var_uniq_quoteid']; ?></label>
                    </div>
                </div>
            </div>


            <div class="col-md-4">
                <div class="form-group group_margin">
                    <label class="control-label col-md-4">Name of hotel:</label>
                    <div class="col-md-5">
                        <label class="control-label" style="padding: 6px 16px 0;"><?php echo $confirm[$i]['var_NOH']; ?></label>
                    </div>

                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group group_margin">
                    <label class="control-label col-md-4">Stars:</label>
                    <div class="col-md-7">
                        <label class="control-label" style="padding: 6px 16px 0;"><?php echo $confirm[$i]['var_star']; ?></label>
                    </div>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group group_margin">
                    <label class="control-label col-md-4">Rooms:</label>
                    <div class="col-md-7">
                        <label class="control-label" style="padding: 6px 16px 0;"><?php echo $confirm[$i]['var_room']; ?></label>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group group_margin">
                    <label class="control-label col-md-4">Adults:</label>
                    <div class="col-md-7">
                        <label class="control-label" style="padding: 6px 16px 0;"><?php echo $confirm[$i]['var_adult']; ?></label>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group group_margin">
                    <label class="control-label col-md-4">Children:</label>
                    <div class="col-md-7">
                        <label class="control-label" style="padding: 6px 16px 0;"><?php echo $confirm[$i]['var_child']; ?></label>
                    </div>
                </div>
            </div>
        </div>                      
        <div class="row">

            <div class="col-md-4">
                <div class="form-group group_margin">
                    <label class="control-label col-md-4">TripAdvisor:</label>
                    <div class="col-md-4">
                        <label class="control-label" style="padding: 6px 16px 0;"><?php echo $confirm[$i]['var_tripadvisor']; ?></label>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group group_margin">                                                                       
                    <label class="control-label col-md-4">Cancellation policy:</label>
                    <div class="col-md-8">
                        <label class="control-label" style="padding: 6px 16px 0;text-align: left;"><?php echo $confirm[$i]['var_policy']; ?></label>
                    </div>
                </div>
            </div> 
            <div class="col-md-4">
                <div class="form-group group_margin">
                    <label class="control-label col-md-4">Comments:</label>
                    <div class="col-md-7">
                        <label class="control-label" style="padding: 6px 16px 0;"><?php echo $confirm[$i]['var_comment']; ?></label>
                    </div>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-md-4">          
                <div class="form-group group_margin">
                    <label class="control-label col-md-4">Quoted price:</label>
                    <div class="col-md-4">
                        <label class="control-label" style="padding: 6px 16px 0;"><?php echo '$' . $confirm[$i]['var_prize']; ?></label>
                        <!--<label class="control-label" style="padding: 6px 16px 0;"><?php // echo $confirm[$i]['var_actual_amont'];    ?></label>-->
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group group_margin">
                    <label class="control-label col-md-4">Select coupons:</label>
                    <div class="col-md-5">
                        <label class="control-label" style="padding: 6px 16px 0;"><?php echo $coupanstr; ?></label>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group group_margin">
                    <label class="control-label col-md-4">Paid amount:</label>
                    <div class="col-md-7">
                        <label class="control-label" style="padding: 6px 16px 0;"><?php echo '$' . $finalamount; ?></label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">          
                <div class="form-group group_margin">
                    <label class="control-label col-md-4">Confirmation number:</label>
                    <div class="col-md-4">
                        <label class="control-label" style="padding: 6px 16px 0;"><?php echo $confirm[$i]['var_transactionid']; ?></label>
                        <!--<label class="control-label" style="padding: 6px 16px 0;"><?php // echo $confirm[$i]['var_actual_amont'];    ?></label>-->
                    </div>
                </div>
            </div>
            <div class="col-md-4">          
                <div class="form-group group_margin">
                    <label class="control-label col-md-4">Src :</label>
                    <div class="col-md-4">
                        <label class="control-label" style="padding: 6px 16px 0;"><?php echo $confirm[$i]['var_src'] . '-$' . $confirm[$i]['int_src_price']; ?></label>
                        <!--<label class="control-label" style="padding: 6px 16px 0;"><?php // echo $confirm[$i]['var_actual_amont'];    ?></label>-->
                    </div>
                </div>
            </div>
        </div>
    <?php if($confirm[$i]['var_cancalationtype'] != ""){ 
        if($confirm[$i]['var_cancalationtype'] == 'coupanamount')
        {
            $type = 'Coupan';
        }
        if($confirm[$i]['var_cancalationtype'] == 'norefund')
        {
            $type = 'No refund';
        }
        if($confirm[$i]['var_cancalationtype'] == 'refund')
        {
            $type = 'Refund';
        }
        ?>
        <div class="row">
            <div class="col-md-4">          
                <div class="form-group group_margin">
                    <label class="control-label col-md-4">Decision:</label>
                    <div class="col-md-4">
                        <label class="control-label" style="padding: 6px 16px 0;text-align: left;"><?php echo $type.'-'.$confirm[$i]['var_refundamount']; ?></label>
                        <!--<label class="control-label" style="padding: 6px 16px 0;"><?php // echo $confirm[$i]['var_actual_amont'];    ?></label>-->
                    </div>
                </div>
            </div>
            <div class="col-md-4">          
                <div class="form-group group_margin">
                    <label class="control-label col-md-4">Processed By:</label>
                    <div class="col-md-4">
                        <label class="control-label" style="padding: 6px 16px 0;text-align: left;"><?php echo $userdetail[0]['var_fname']; ?></label>
                        <!--<label class="control-label" style="padding: 6px 16px 0;"><?php // echo $confirm[$i]['var_actual_amont'];    ?></label>-->
                    </div>
                </div>
            </div>
            <div class="col-md-4">          
                <div class="form-group group_margin">
                    <label class="control-label col-md-4">Processed Date/Time:</label>
                    <div class="col-md-8">
                        <label class="control-label" style="padding: 6px 16px 0;text-align: left;"><?php echo date(DISPLAY_DATE_FORMAT_FULL,  strtotime($confirm[$i]['var_updateddate'])); ?></label>
                        <!--<label class="control-label" style="padding: 6px 16px 0;"><?php // echo $confirm[$i]['var_actual_amont'];    ?></label>-->
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    
<?php } ?>  