<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.1.1
Version: 2.0.2
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->    
    <?php echo $this->load->view('admin/includes/header'); ?>    
    <body class="page-header-fixed">
        <?php echo $this->load->view('admin/includes/body_header'); ?>
        <div class="clearfix">
        </div>
        <div class="page-container">
            <?php echo $this->load->view('admin/includes/body_leftpanel'); ?>
            <div class="page-content-wrapper">
                <div id="result_container">
                    <div class="page-content" id="input-container">
                    <?php echo $this->load->view('admin/includes/breadcrumb'); ?>
                    <?php echo $this->load->view($page); ?>
                </div>
                </div>
            </div>
        </div>
        <?php echo $this->load->view('admin/includes/body_footer'); ?>
        <?php echo $this->load->view('admin/includes/footer'); ?>
    </body>
</html>