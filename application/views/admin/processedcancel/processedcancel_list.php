<style>
    .portlet-body.data-grid-custom .col-sm-12.col-md-3 {
        display: none;
    }
    .portlet-body.data-grid-costom .col-sm-12.col-md-3 {
        display: none;
    }
    .portlet-body.data-grid-costom .form-control.input-small.input-inline {
        width: 275px !important;
    }
    .control-label{text-align: center}
    .input-small {
        width: 245px !important;
    }
    .table-condensed > thead > tr > th, .table-condensed > tbody > tr > th, .table-condensed > tfoot > tr > th, .table-condensed > thead > tr > td, .table-condensed > tbody > tr > td, .table-condensed > tfoot > tr > td {
        padding: 4px;
    }
    .datepicker {
        width: 195px !important;
    }

</style>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-user"></i>Processed Cancellation
                </div>
                <div class="actions">
                </div>
            </div>
            <div class="portlet-body data-grid-custom">
                <table class="table table-striped table-bordered table-hover "  id="sample_2">
                    <thead>
                    <tr>
                        <th>Date/Time</th>
                        <th>Check-in</th>
                        <th>Check-out</th>
                        <th>Hotel name</th>
                        <th>Email address</th>
                        <th>Account ID</th>
                        <th>Confirmation number </th>
                        <th>Paid</th>
                        <th style="text-align:center;">Source</th>
                        <th>Phone number</th>
                        <th>Source conf </th>
                        <th>Refunded</th>
                        <th>Refund #</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach($confReservations as $res)
                    {
                        $bookUser = getUserById($res->uid);
                        ?>
                        <tr>
                            <td><?php echo getBookingDateTime($res->book_id); ?></td>
                            <td><?php echo getBookingcheckIn($res->book_id); ?></td>
                            <td><?php echo getBookingcheckOut($res->book_id); ?></td>
                            <td><?php echo getBookinghotelName($res->book_id); ?></td>
                            <td><?php echo $bookUser->var_email; ?></td>
                            <td><?php echo $bookUser->var_accountid; ?></td>
                            <td><?php echo $res->confirm_no; ?></td>
                            <td><?php echo "$".getBookingperRoomPaid($res->book_id); ?></td>

                            <td>
                                <input type="text" <?php if(isSkipLogin()){ ?> readonly="true"  <?php } ?> name="sourceName<?php echo $res->id; ?>" id="sourceName<?php echo $res->id; ?>" class="form-control" style="height: 33px; width: 140px; text-align: center;" value="<?php echo getSourcename($res->sourceID); ?>"  onfocus="this.placeholder = ''">
                                <input type='hidden' name='sourceId<?php echo $res->id; ?>' id='sourceId<?php echo $res->id; ?>'>
                                <script type="text/javascript">
                                    jQuery(document).ready(function () {
                                        jQuery("#sourceName<?php echo $res->id; ?>").autocomplete({
                                            source: function (request, response)
                                            {
                                                jQuery.getJSON(baseurl + "admin/conf_res/get_sources", {term: jQuery("#sourceName<?php echo $res->id; ?>").val()},
                                                    response);
                                            },
                                            minLength: 3,
                                            select: function (event, ui)
                                            {
                                                $("#sourceId<?php echo $res->id; ?>").val(ui.item.id);
                                                change_source(<?php echo $res->id; ?> , ui.item.id);
                                                $("#sourcenum<?php echo $res->id; ?>").text("("+ui.item.phone+")");
                                            }
                                        });
                                    });
                                </script>
                            </td>
                            <td id="sourcenum<?php echo $res->id; ?>">
                                <?php echo "(".getSourcePhone($res->sourceID).")"; ?>
                            </td>
                            <td>
                                <input  <?php if(isSkipLogin()){ ?> readonly="true"  <?php } ?> type="text" value="<?php echo $res->reservationId; ?>" style="width:76px;float:left;" id="reserv_id<?php echo $res->id; ?>" />
                                <div <?php if(!isSkipLogin()){ ?> onclick="change_res(<?php echo $res->id; ?>)"  <?php } ?>><i style="background: #dbdbe2;padding: 4.3px;margin-top: -0.3px;border: 1px solid;" class="fa fa-pencil" aria-hidden="true"></i></div>
                            </td>



                            <td><?php echo "$".$res->return_amount; ?></td>
                            <td><?php echo $res->stripe_no; ?></td>
                        </tr>
                    <?php
                    }
                    ?>
                    </tbody>
                </table>
                <!--                <table class="table table-striped table-bordered table-hover" id="sample_2">
                    <thead>
                        <tr>
                            <th>
                                Date/Time
                            </th>
                            <th>
                                Username
                            </th>
                            <th>
                                Email address
                            </th>
                            <th>
                                Account ID
                            </th>
                            <th>
                                Confirmation number 
                            </th>
                            <th>
                                Paid amount 
                            </th>
                            <th>
                                View
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                <?php foreach ($conform_res as $row) { ?>
                                    <tr>
                                        <td>
                    <?php echo $row['var_createddate']; ?>
                                        </td>
                                        <td>
                    <?php echo $row['var_username']; ?>
                                        </td>
                                        <td>
                    <?php echo $row['var_email']; ?>
                                        </td>
                                        <td>
                    <?php echo $row['var_accountid']; ?>
                                        </td>
                                        <td>        
                    <?php echo $row['var_transactionid']; ?>
                                        </td>
                                        <td>
                    <?php echo $row['var_paid_amount']; ?>
                                        </td>
                                        <td>
                                            <a href="#myModal_autocomplete" data-toggle="modal" data-id="<?php echo $row['int_fkquote']; ?>" id="conform_list" class="sentquote label label-sm label-success">View</a>
                                        </td>
                                    </tr>
                <?php } ?>   
                    </tbody>
                </table>-->
            </div>
        </div>
    </div>
</div>

<div id="myModal_autocomplete" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog" style="width:80%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">   Processed Cancellations  </h4>
            </div>
            <form method="post" id="add_quoterequests" class="form-horizontal"> <div class="portlet-body form">
                    <div class="form-body">
                        <div class="quoterequest">

                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div id="myModal_paid" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog" style="width:50%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"> Cancel reservations </h4>
            </div>
            <form method="post" id="add_paid" class="form-horizontal"> <div class="portlet-body form">
                    <div class="form-body">
                        <div class="">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label col-md-2"></label>
                                        <div class="radio-list  col-md-2">
                                            <label class="radio-inline">
                                                <input type="radio" name="canceltype" id="optionsRadios1" value="coupanamount" > Coupon: </label>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="text" name="amount1" id="coupanamount" class="form-control amount">
                                            <input type="hidden" name="replyquotes" id="replyquotes" class="form-control replyquotes">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">

                                        <label class="control-label col-md-2"></label>
                                        <div class="radio-list  col-md-2">
                                            <label class="radio-inline">
                                                <input type="radio" name="canceltype" id="optionsRadios2" value="refund"> Refund: </label>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="text" name="amount2" id="refund" class="form-control amount">
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label col-md-2"></label>
                                        <div class="radio-list  col-md-2">
                                            <label class="radio-inline">
                                                <input type="radio" name="canceltype" id="optionsRadios3" value="norefund" > No Refund: </label>
                                        </div>
                                        <!--                                        <div class="col-md-6">
                                                                                    <input type="text" name="source" id="Source" class="form-control amount">
                                                                                </div>-->
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer" style="text-align: center">
                                <button type="submit"  class="btn green" >Submit</button>
                                <!--<button type="button" id="approve" class="btn green" style="margin-left:10%;"><i class="fa fa-check"></i> APPROVE</button>-->
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    function change_res(val)
    {
        $('#loading').show();
        $.ajax({
            type: 'post',
            url: "<?php echo base_url();?>" + 'admin/conf_res/changeTransectionNum/'+val,
            data: {'nconf': $('#reserv_id'+val).val()},
            success: function(output) {
                $('#loading').hide();
                if(output)
                {
                    alert("Updated");
                }
                else
                {
                    alert("Error in updation!");
                }
            }
        });
        //admin_changed
    }

    function change_source(id , source_id)
    {
        console.log(id+"-"+source_id);
        $.ajax({
            type: 'post',
            url: baseurl + 'admin/conf_res/changesource_res',
            data: {id: id , source_id : source_id},
            success: function(data) {

            }
        });
    }
</script>