<?php
$i = 1;
?>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-user"></i>Affiliate Payment Due
                </div>

                <div class="actions">
                </div>
            </div>
            <div class="portlet-body">

                <table class="table table-striped table-bordered table-hover" id="sample_2">
                    <thead>
                        <tr>
                            <th>
                                Date
                            </th>
                            <th>
                                Account ID
                            </th>
                            <th>
                                Email address
                            </th>
                            <th>
                                Full Name
                            </th>
                            <th>
                                Paypal
                            </th>
                            <th>
                                Amount Due
                            </th>
                            <th>
                                Paid
                            </th>     
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($affipayment as $row) { ?>
                        <tr>
                            <td>
                               <?php echo $row['dt_created_date'];?>
                            </td>
                            <td>
                                <?php echo $row['var_accountid'];?>
                                <!--1-->
                            </td>
                            <td>
                                <?php echo $row['var_email'];?>
                            </td>
                            <td>
                                <?php echo $row['var_fname'];?>
                            </td>
                            <td>
                               www.paypal.com
                            </td>
                            <td>
                               <?php echo $row['var_commision_amount']; ?>
                                <input type="hidden" class="aff_status" name="afilate_id" value="<?php echo $row['int_glcode'];?>">
                            </td>

                            <td>
                                <a class="btn default btn-xs blue btnid" data-toggle="modal" href="#mymodal_payment" 
                                   payment_id = "<?php  echo $row['paymanetid'];?>" 
                                   comssionid = "<?php  echo $row['commsionid'];?>" 
                                   charjbackid = "<?php  echo $row['chagebackid'];?>" 
                                   amount = "<?php  echo $row['var_commision_amount'];?>"  
                                   user-id = "<?php echo $row['int_glcode'];?>">
                                    <i class="fa fa-edit">PAID</i>
                                </a>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
<div id="myModal_autocomplete1" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Delete Event Data</h4>
            </div>
            <div class="modal-body">
                <h3>Are you Sure to Delete ?...</h3>
            </div>
            <div class="modal-footer" >
                <button type="button" class="btn btn-default" data-dismiss="modal" >Close</button>
                <button type="submit" id="btndelete" class="btn red"><i class="fa fa-check"></i>Delete</button>
            </div>
        </div>
    </div>
</div>

<div id="mymodal_payment" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Payment</h4>
            </div>
            
            <div class="modal-body">
                <form name="payment" method="post" id="payment" class="form-horizontal">
                <div class="row">
                    <!--<div class="form-group">-->
                  <div class="col-md-12">
                   <div class="radio-list">
                        <label class="radio-inline">
                            <input type="radio" name="optionsRadios" id="paypal" value="PayPal"><a href="javascript:;"><img src="<?php echo base_url(); ?>public/assets/img/paypal.jpg" style="width: 100px;" /></a></label>
                        <label class="radio-inline">
                            <input type="radio" name="optionsRadios" id="wunion" value="WesternUnion"><a href="javascript:;"><img src="<?php echo base_url(); ?>public/assets/img/WesternUnion.png" /></a></label>
                       <label class="radio-inline">
                            <input type="radio" name="optionsRadios" id="other" value="Other"> Others </label>
                    </div>
                  </div>
<!--                        <label class="radio-inline"><input type="radio" name="other" id="other" value="option1"></label>-->
                       
                   <!--</div>-->
                </div>
                
                <div class="row">
              
                        <label class="control-label col-md-3">Transaction ID :</label>
             
                    <div class="col-md-6">
                       <input type="text" class="form-control" placeholder="Enter Transaction ID" name="ptransaction" id="ptransaction" />
                    </div>
                    
                </div>
                <div class="modal-footer" >
                <button type="button" class="btn btn-default" data-dismiss="modal" >Close</button>
                <input type="hidden" class="aff_status" name="payid" id="payid" value="">
                <input type="hidden" class="aff_status" name="amount" id="amount" value="">
                <input type="hidden" class="aff_status" name="charjbackid" id="charjbackid" value="">
                <input type="hidden" class="aff_status" name="comssionid" id="comssionid" value="">
                <button type="submit" id="btnsubmit" class="btn blue aff_status" ><i class="fa fa-check"></i>Submit</button>
            </div>
            </form> 
            </div>
           
            
        </div>
    </div>
</div>
