<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-user"></i>Processed IP Address Ban
                </div>
            </div>
            <div class="portlet-body">

                <table class="table table-striped table-bordered table-hover" id="sample_2">
                    <thead>
                        <tr>
                            <th>
                                Date/Time 
                            </th>
                            <th>
                                IP Address
                            </th>
							<th>
                                Duration
                            </th>
                            <th>
                               Reason
                            </th>
                            <th>
                               Banned by
                            </th>
							<?php 
							if(!isSkipLogin())
							{
								?>
                            <th>
                               Status
                            </th>
								<?php
							}
							?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach($ipban as $row)
                            {  
                            ?>
                        <tr>
                            <td>
                                <?php echo date(DISPLAY_DATE_FORMAT_FULL, strtotime($row['var_createddate']));?>
                            </td>
                            <td>
								<?php 
									if(empty($row['var_ipban']))
									{
										$user1 = getUserById($row['fk_user']);
										echo $user1->var_email;
									}
									else
									{
										echo $row['var_ipban'];
									}
	//                          ?>
                            </td>
							<td>
								<?php 
									$date1 = new DateTime($row['date_banstart_time']);
									$date2 = new DateTime($row['date_banend_time']);
									$diff = $date1->diff($date2);
									$hours = $diff->h;
									$hours = $hours + ($diff->days*24);
									echo $hours." hours";
	//                          ?>
                            </td>
                            <td>
                                <?php echo $row['var_reason'];?>
                            </td>
                            <td>
                                <?php       
                              echo $row['var_banby'];
                                ?>
                            </td>
							<?php 
							if(!isSkipLogin())
							{
								?>
                            <td>
                                <a class="btn default btn-xs green status"  href="javascript:;" data-id="<?php echo $row['int_glcode'];?>">LIFT</a> 
                            </td>
								<?php
							}
							?>
                        </tr>
                            <?php }?>
					</tbody>
                </table>
            </div>
        </div><!--
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

