<style>
    .radiosetting div > span {
        margin-left: -10px;
    }
    .radiosetting {
        width: 100px;
    }
</style>
<?php
$i = 1;
?>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-user"></i>Coupons
                </div>
                <div class="actions">
                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="sample_2">
                    <thead>
                    <tr>
                        <th>  <input type="checkbox" class="form-control"> </th>
                        <th>
                            Email address
                        </th>
                        <th>
                            Account ID
                        </th>
                        <th>
                            Coupon ID
                        </th>

                        <th>
                            Amount
                        </th>
                        <th>
                            Minimum Spend
                        </th>
                        <th>
                            Status
                        </th>
                        <?php
                        if(!isSkipLogin())
                        {
                            ?>
                            <th>
                                Action
                            </th>
                        <?php
                        }
                        ?>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($userlist as $row) { ?>
                        <tr>
                            <td>  <input type="checkbox" class="form-control check_checkbox" value="<?php echo $row['userid']; ?>" name="check[]"></td>
                            <td>
                                <?php echo $row['var_email']; ?>
                            </td>
                            <td>
                                <?php echo $row['var_accountid']; ?>
                            </td>
                            <td>
                                <?php echo $row['var_couponcode']; ?>
                            </td>

                            <td>
                                <?php if ($row['var_coupan_type'] == 1) { ?>
                                    <?php echo $row['var_couponvalue']; ?>%
                                <?php } else { ?>
                                    $<?php echo $row['var_couponvalue']; ?>
                                <?php } ?>
                            </td>
                            <td>
                                <?php
                                echo $row['min_amount'];
                                ?>
                            </td>
                            <td>
                                <?php if ($row['var_status'] == 'A') {
                                    echo 'Activate';

                                }else if($row['var_status'] == 'U')
                                {
                                    echo 'Used';
                                }else if($row['var_status'] == 'E')
                                {
                                    echo 'Expired';
                                }else if($row['var_status'] == 'D' || $row['var_status'] == 'AD' ) { echo 'Deactive';

                                } ?>
                            </td>
                            <?php
                            if(!isSkipLogin())
                            {
                                ?>
                                <td>
                                    <?php if ($row['var_status'] == 'A') { ?>
                                        <a class="btn default btn-xs red status"  href="javascript:;" data-id="<?php echo $row['int_glcode']; ?>">
                                            Deactivate
                                        </a>
                                    <?php } if ($row['var_status'] == 'D' || $row['var_status'] == 'AD' ) { ?>
                                        <a class="btn default btn-xs blue status"  href="javascript:;" data-id="<?php echo $row['int_glcode']; ?>">
                                            <i class="fa fa-edit"></i>Activate
                                        </a>
                                    <?php } ?>
                                </td>
                            <?php
                            }
                            ?>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>

            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
<?php
if(!isSkipLogin())
{
    ?>
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-user"></i>Coupons Generate
                    </div>

                    <div class="actions">

                    </div>
                </div>
                <div class="portlet-body">
                    <form method="post" class="horizontal-form" id="coupongen" name="coupongen">
                        <div class="row">
                            <div class="col-md-12" style="margin-top:10px;">
                                <div class="form-group">
                                    <label class="col-md-2" style="width: 13%; padding-right: 0; font-size:12px; font-weight:bold;margin:6px 0 0;" >Generate to All</label>
                                    <div class="col-md-1" style="width: 2%; padding-right: 0;">
                                        <div class="radio-list" style="">
                                            <label class="radio-inline radiosetting">
                                                <input type="radio" id="myinput" name="coupon" value="1" data-status="all" class="coup_off generateall">
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-1" style="padding-right: 0;">
                                        <input type="text" class="form-control coupanoff1" name="couponper">
                                    </div>
                                    <label class="col-md-1" style="padding: 0px 7px; width: 4%;font-size:12px; font-weight:bold;margin:6px 0 0;"> %OFF</label>
                                    <label class="col-md-1" style="margin:6px 0 0;width: 4%; font-size:12px; font-weight:bold;margin:6px 0 0;">OR</label>
                                    <div class="col-md-1" style="width: 4.4%; padding-right: 0;">
                                        <div class="radio-list">
                                            <label class="radio-inline radiosetting">
                                                <input type="radio" name="coupon" value="2" data-status="all" class="coup_off">$</label>
                                        </div>
                                    </div>
                                    <div class="col-md-1" style="padding-right: 0">
                                        <input type="text" class="form-control coupanoff2" name="couponoff" id="couponper">
                                    </div>

                                    <div class="coupon-med" style="margin:30px 0 -5px; float:right; width:54%;">
                                        <label class="col-md-2" style="padding-right: 0; font-size:12px; font-weight:bold;margin:6px 0 0;">OFF with</label>
                                        <label class="col-md-1 expiration" style="padding-left: 0;display: none;"></label>
                                        <div class="col-md-3" >
                                            <div class="date date-picker" data-date-format="dd-mm-yyyy" data-date-start-date="+0d">
                                                <input type="text" class="form-control exdate1 icon-pick">
										<span class="input-group-btn">
										</span>
                                            </div>
                                        </div>
                                        <label class="col-md-3" style=" padding: 0; font-size:12px; font-weight:bold;margin:6px 0 0;">Minimum spend off</label>
                                        <div class="col-md-2" style=" padding: 0;">
                                            <input type="text" id="minSpAmo" class="form-control">
                                        </div>
                                        <div class="col-md-1">
                                            <input  class="btn blue dataaction" type="submit" value="Generate" style="border-radius: 4px !important; text-transform: capitalize; font-size:14px; padding:6px 10px;" >
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">

                                    <label class="col-md-2" style="width: 13%; padding-right: 0; font-size:12px; font-weight:bold;margin:6px 0 0;" >Generate to Individual</label>
                                    <div class="col-md-1" style="width: 2%; padding-right: 0;">
                                        <div class="radio-list" >
                                            <label class="radio-inline radiosetting">
                                                <input type="radio" name="coupon" value="3" data-status="individual" class="coup_off"> </label></div></div>
                                    <div class="col-md-1" style="padding-right: 0;">
                                        <input type="text" class="form-control coupanoff3"  name="couponper">
                                    </div>
                                    <label class="col-md-1" style="padding: 0px 7px; width: 4%; font-size:12px; font-weight:bold;margin:6px 0 0;"> %OFF</label>
                                    <label class="col-md-1" style="width: 4%; font-size:12px; font-weight:bold;">OR</label>
                                    <div class="col-md-1" style="width: 4.4%; padding-right: 0;">
                                        <div class="radio-list">
                                            <label class="radio-inline radiosetting" >
                                                <input type="radio" name="coupon" id="" data-status="individual" value="4" class="coup_off">$</label></div></div>
                                    <div class="col-md-1" style="padding-right: 0">
                                        <input type="text" class="form-control coupanoff4" name="couponoff">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
<?php
}
?>
<div id="myModal_autocomplete1" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="padding: 7px;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Coupon Generate Data</h4>
            </div>
            <div class="modal-body" style="padding: 10px;">
                <p>Generate Coupon or not?</p>
            </div>
            <div class="modal-footer" style="padding: 7px;">

                <button type="submit" id="btnyes" class="btn "><i class="fa fa-check green"></i>YES</button>
                <button type="button" class="btn btn-default red" data-dismiss="modal" >NO</button>
            </div>
        </div>
    </div>
</div>

<div id="deleteModal" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog" style="width: 800px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Please enter password</h4>
            </div>
            <div class="modal-body">
                <div class="form-group furture-form-style">
                    <label>Password:*</label>
                    <input type="password" class="form-control form-style" id="deletePassword" >
                </div>
            </div>
            <div class="modal-footer" >
                <button type="button" class="btn btn-default" id="close_model" data-dismiss="modal" >Close</button>
                <button type="button" class="btn btn-default" id="deleteButton" >Ok</button>
            </div>
        </div>
    </div>
</div>

<div id="deleteStatusModal" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog" style="width: 800px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Please enter password</h4>
            </div>
            <div class="modal-body">
                <div class="form-group furture-form-style">
                    <label>Password:*</label>
                    <input type="password" class="form-control form-style" id="statusDeletePassword" >
                </div>
            </div>
            <div class="modal-footer" >
                <button type="button" class="btn btn-default" id="close_model" data-dismiss="modal" >Close</button>
                <button type="button" class="btn btn-default" id="deleteStatusButton" >Ok</button>
            </div>
        </div>
    </div>
</div>