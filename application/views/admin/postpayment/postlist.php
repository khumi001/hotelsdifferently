<?php
$i = 1;
?>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-user"></i>Affiliate Posted Payment
                </div>

                <div class="actions">
<!--                    <input type="checkbox" class="checkboxes" name="new" id="new"/>New &nbsp;
                    <input type="checkbox" class="checkboxes" name="new" id="new"/>Pending &nbsp;
                    <input type="checkbox" class="checkboxes" name="new" id="new"/>Expired &nbsp;
                    <input type="checkbox" class="checkboxes" name="new" id="new"/>Published  &nbsp;-->
                    
                </div>
            </div>
            <div class="portlet-body">

                <table class="table table-striped table-bordered table-hover" id="sample_2">
                    <thead>
                        <tr>
                            <th>
                                Date
                            </th>
                             <th>
                               Account ID
                            </th>
                            <th>
                                Email address
                            </th>
                            <th>
                                Full Name
                            </th>
                            <th>
                                Payment method
                            </th>
                            <th>
                                Amount Due
                            </th>
                            <th>
                                Transaction ID
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($post_payment as $row) { ?>
                        <tr>
                            <td>
                                <?php echo $row['var_created_date']?>
                            </td>
                            <td>
                                <!--1-->
                                <?php echo $row['var_accountid'];?>
                            </td>
                            <td>
                                <?php echo $row['var_email']?>
                            </td>
                            <td>
                                <?php echo $row['var_username']?>
                            </td>
                            <td>
                                http;//www.paypal.com
                            </td>
                            <td>
                               <?php echo $row['var_amount']; ?>
                            </td>
                                                  
                             <td>
                               <?php echo $row['var_transid'];?>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
<div id="myModal_autocomplete1" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Delete Event Data</h4>
            </div>
            <div class="modal-body">
                <h3>Are you Sure to Delete ?...</h3>
            </div>
            <div class="modal-footer" >
                <button type="button" class="btn btn-default" data-dismiss="modal" >Close</button>
                <button type="submit" id="btndelete" class="btn red"><i class="fa fa-check"></i>Delete</button>
            </div>
        </div>
    </div>
</div>

