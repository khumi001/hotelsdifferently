<style>
    .portlet-body.data-grid-custom .col-sm-12.col-md-3 {
        display: none;
    }
    .portlet-body.data-grid-costom .col-sm-12.col-md-3 {
        display: none;
    }
    .portlet-body.data-grid-costom .form-control.input-small.input-inline {
        width: 275px !important;
    }
    .control-label{text-align: center}
    .input-small {
        width: 245px !important;
    }
    .table-condensed > thead > tr > th, .table-condensed > tbody > tr > th, .table-condensed > tfoot > tr > th, .table-condensed > thead > tr > td, .table-condensed > tbody > tr > td, .table-condensed > tfoot > tr > td {
        padding: 4px;
    }
    .datepicker {
        width: 195px !important;
    }

</style>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-user"></i>Cancel reservations
                </div>
                <div class="actions">
                </div>
            </div>
            <div class="portlet-body data-grid-custom">
                <div class="row" style="margin-bottom: 15px ;display: none;">
                    <div class="col-md-3"></div>
                    <div class="col-md-8 search_box_custome" id="search_box_custome" >
                        <div class="form-group">
                            <label class="control-label col-md-1" style="padding-right: 0px;">Check In</label>
                            <div class="col-md-3">
                                <input type="text" id="example_range_from_1" class="form-control form-control-inline date-picker example">
                            </div>
                            <label class="control-label col-md-2" style="padding-right: 0px;">Check Out</label>
                            <div class="col-md-3">
                                <input type="text" id="example_range_to_1" class="form-control form-control-inline date-picker example">
                            </div>
                            <div class="col-md-2">
                                <button class="btn btn-default resetbutton" type="button">Reset</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3"></div>
                </div>
                <table class="table table-striped table-bordered table-hover " id="tracking_table">
                    <thead>
                        <tr>
                            <th>
                                Date/Time
                            </th>
                            <th>
                                Username
                            </th>
                            <th>
                                Email address
                            </th>
                            <th>
                                Account ID
                            </th>
                            <th>
                                Confirmation number 
                            </th>
                            <th>
                                Paid amount 
                            </th>
                            <th>
                                View
                            </th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
<!--                <table class="table table-striped table-bordered table-hover" id="sample_2">
                    <thead>
                        <tr>
                            <th>
                                Date/Time
                            </th>
                            <th>
                                Username
                            </th>
                            <th>
                                Email address
                            </th>
                            <th>
                                Account ID
                            </th>
                            <th>
                                Confirmation number 
                            </th>
                            <th>
                                Paid amount 
                            </th>
                            <th>
                                View
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                <?php foreach ($conform_res as $row) { ?>
                                    <tr>
                                        <td>
                    <?php echo $row['var_createddate']; ?>
                                        </td>
                                        <td>
                    <?php echo $row['var_username']; ?>
                                        </td>
                                        <td>
                    <?php echo $row['var_email']; ?>
                                        </td>
                                        <td>
                    <?php echo $row['var_accountid']; ?>
                                        </td>
                                        <td>        
                    <?php echo $row['var_transactionid']; ?>
                                        </td>
                                        <td>
                    <?php echo $row['var_paid_amount']; ?>
                                        </td>
                                        <td>
                                            <a href="#myModal_autocomplete" data-toggle="modal" data-id="<?php echo $row['int_fkquote']; ?>" id="conform_list" class="sentquote label label-sm label-success">View</a>
                                        </td>
                                    </tr>
                <?php } ?>   
                    </tbody>
                </table>-->
            </div>
        </div>
    </div>
</div>

<div id="myModal_autocomplete" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog" style="width:80%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"> Cancel Reservations </h4>
            </div>
            <form method="post" id="add_quoterequests" class="form-horizontal"> <div class="portlet-body form">
                    <div class="form-body">
                        <div class="quoterequest">  

                        </div>
                    </div>
                </div>
            </form> 
        </div>                                               
    </div>
</div>   
<div id="myModal_paid" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog" style="width:50%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"> Cancel reservations </h4>
            </div>
            <form method="post" id="add_paid" class="form-horizontal"> <div class="portlet-body form">
                    <div class="form-body">
                        <div class="">  
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label col-md-2"></label>
                                        <div class="radio-list  col-md-2">
                                                <label class="radio-inline">
                                                <input type="radio" name="canceltype" id="optionsRadios1" value="coupanamount" > Coupon: </label>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="text" name="amount1" id="coupanamount" class="form-control amount">
                                            <input type="hidden" name="replyquotes" id="replyquotes" class="form-control replyquotes">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        
                                        <label class="control-label col-md-2"></label>
                                        <div class="radio-list  col-md-2">
                                                <label class="radio-inline">
                                                <input type="radio" name="canceltype" id="optionsRadios2" value="refund"> Refund: </label>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="text" name="amount2" id="refund" class="form-control amount">
                                        </div>
                                    
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label col-md-2"></label>
                                        <div class="radio-list  col-md-2">
                                                <label class="radio-inline">
                                                <input type="radio" name="canceltype" id="optionsRadios3" value="norefund" > No Refund: </label>
                                        </div>
<!--                                        <div class="col-md-6">
                                            <input type="text" name="source" id="Source" class="form-control amount">
                                        </div>-->
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer" style="text-align: center">
                                <button type="submit"  class="btn green" >Submit</button>
                                <!--<button type="button" id="approve" class="btn green" style="margin-left:10%;"><i class="fa fa-check"></i> APPROVE</button>-->
                            </div>
                        </div>
                    </div>
                </div>
            </form> 
        </div>                                               
    </div>
</div> 