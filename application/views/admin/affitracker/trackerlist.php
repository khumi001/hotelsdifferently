<style>
   .portlet-body.data-grid-costom .col-sm-12.col-md-3 {
  display: none;
}
.portlet-body.data-grid-costom .form-control.input-small.input-inline {
  width: 275px !important;
}
.control-label{text-align: center}
</style>
<?php
$i = 1;
?>
<style>
    .input-small {
        width: 245px !important;
    }
    .table-condensed > thead > tr > th, .table-condensed > tbody > tr > th, .table-condensed > tfoot > tr > th, .table-condensed > thead > tr > td, .table-condensed > tbody > tr > td, .table-condensed > tfoot > tr > td {
        padding: 4px;
    }
    .datepicker {
        width: 195px !important;
    }
</style>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-user"></i>Affiliate Tracker
                </div>

                <div class="actions">
<!--                    <input type="checkbox" class="checkboxes" name="new" id="new"/>New &nbsp;
                    <input type="checkbox" class="checkboxes" name="new" id="new"/>Pending &nbsp;
                    <input type="checkbox" class="checkboxes" name="new" id="new"/>Expired &nbsp;
                    <input type="checkbox" class="checkboxes" name="new" id="new"/>Published  &nbsp;-->

                </div>
            </div>

            <div class="portlet-body data-grid-costom">
                <div class="row" style="margin-bottom: 15px ;display: none;">
                    <div class="col-md-3"></div>
                    <div class="col-md-8 search_box_custome" id="search_box_custome" >
                        <div class="form-group">
                            <label class="control-label col-md-1">From</label>
                            <div class="col-md-3">
                                <input type="text" id="example_range_from_1" class="form-control form-control-inline date-picker example">
                            </div>
                            <label class="control-label col-md-1">To</label>
                            <div class="col-md-3">
                                <input type="text" id="example_range_to_1" class="form-control form-control-inline date-picker example">
                            </div>
                              <div class="col-md-2">
                                 <button type="button" class="btn btn-default resetbutton" >Reset</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3"></div>
                </div>
                <table class="table table-striped table-bordered table-hover " id="tracking_table">
                    <thead>
                        <tr>
                            <th>
                                Account ID
                            </th>
                            <th>
                                Email address
                            </th>
                            <th>
                                Best Source
                            </th>
                            <th>
                                Signups
                            </th>
                            <th>
                                Total sales
                            </th>
                            <th>
                                Pending commission
                            </th>

                            <th>
                                Paid commission
                            </th>

                        </tr>
                    </thead>
                    <tbody>
<!--                        <tr>
                            <td>
                                1
                            </td>
                            <td>
                                test@test.com
                            </td>
                            <td>
                                www.test.com
                            </td>
                            <td>
                                10
                            </td>
                            <td>
                                $120
                            </td>
                            <td>
                                $30
                            </td>
                            <td>
                                $90
                            </td>
                            
                        </tr>
                           <tr>
                            <td>
                                2
                            </td>
                            <td>
                                test12@test.com
                            </td>
                            <td>
                                www.test12.com
                            </td>
                            <td>
                                12
                            </td>
                            <td>
                                $150
                            </td>
                            <td>
                                $30
                            </td>
                            <td>
                                $120
                            </td>
                            
                        </tr>
                          <tr>
                            <td>
                                3
                            </td>
                            <td>
                                test11@test.com
                            </td>
                            <td>
                                www.test11.com
                            </td>
                            <td>
                                15
                            </td>
                            <td>
                                $120
                            </td>
                            <td>
                                $30
                            </td>
                            <td>
                                $90
                            </td>
                            
                        </tr>-->

                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
<div id="myModal_autocomplete1" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Delete Event Data</h4>
            </div>
            <div class="modal-body">
                <h3>Are you Sure to Delete ?...</h3>
            </div>
            <div class="modal-footer" >
                <button type="button" class="btn btn-default" data-dismiss="modal" >Close</button>
                <button type="submit" id="btndelete" class="btn red"><i class="fa fa-check"></i>Delete</button>
            </div>
        </div>
    </div>
</div>
