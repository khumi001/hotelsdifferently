<?php
    $i=1;
?>
<div class="row" id="log">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-user"></i>Login Log
                </div>
            </div>
           
            <div class="portlet-body">
                <form class="form-horizontal" role="form">
                    <div class="form-body">
                        <div class="form-group">
                        <label class="col-md-3 control-label"></label>
                        <div class="col-md-9">
                            <div class="radio-list">
                                <label class="radio-inline">
                                    <input type="radio" name="optionsRadios" id="logmember" value="option1" > IP Addresses  Log-Members 
								</label>
                                <label class="radio-inline">
                                    <input type="radio" name="optionsRadios" id="logadmin" value="option3"> IP Addresses Log-Admins 
								</label>
                            </div>
                        </div>
                    </div>
                   </div>
                </form>
                <div id="mainlog">
                    <table class="table table-striped table-bordered table-hover" id="sample_2">
                    <thead>
                        <tr>
                            <th>
                                Date/Time
                            </th>
                            <th>
                                Email address
                            </th>
                            <th>
                                Ip addresses 
                            </th>
                            <th>
                                Status
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                </div>
                <div id="memberlogdiv" style="display: none">
                    <table class="table table-striped table-bordered table-hover" id="sample_3">
                    <thead>
                        <tr>
                            <th>
                                Date/Time
                            </th>
                            <th>
                                Email address
                            </th>
                            <th>
                                Ip address
                            </th>
                            <th>
                                Status
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                </div>
                 <div id="affilatelogdiv" style="display: none">
                    <table class="table table-striped table-bordered table-hover" id="sample_4">
                    <thead>
                        <tr>
                            <th>
                                Date/Time
                            </th>
                            <th>
                                Email address
                            </th>
                            <th>
                                Ip address
                            </th>
                            <th>
                                Status
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                 </div>
                <div id="adminlogdiv" style="display: none">
                    <table class="table table-striped table-bordered table-hover" id="sample_5">
                    <thead>
                        <tr>
                            <th>
                                Date/Time
                            </th>
                            <th>
                                Email address
                            </th>
                            <th>
                                Ip address
                            </th>
                            <th>
                                Status
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                </div>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>


