<?php
$i = 1;
?>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-user"></i>Chargeback
                </div>

                <div class="actions">
<!--                    <input type="checkbox" class="checkboxes" name="new" id="new"/>New &nbsp;
                    <input type="checkbox" class="checkboxes" name="new" id="new"/>Pending &nbsp;
                    <input type="checkbox" class="checkboxes" name="new" id="new"/>Expired &nbsp;
                    <input type="checkbox" class="checkboxes" name="new" id="new"/>Published  &nbsp;-->

                </div>
            </div>
            <div class="portlet-body">

                <table class="table table-striped table-bordered table-hover" id="sample_2">
                    <thead>
                        <tr>
                            <th>
                                Account ID
                            </th>
                            
                            <th>
                                Email address
                            </th>
                            <th>
                                Full Name
                            </th>
                            <th>
                                Paypal
                            </th>
                            <th>
                                Country
                            </th>
                            <th>
                                Chargeback
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no = 1;
                        foreach ($charge as $row) {
                            ?>
                            <tr>

                                <td>
                                    <?php echo $row['var_accountid']; ?>
                                </td>
                                
                                <td>
                                    <?php echo $row['var_email']; ?>
                                </td>
                                <td>
                                    <?php echo $row['var_fname']; ?>  <?php echo $row['var_lname']; ?>

                                </td>
                                <td>
                                    <?php echo $row['var_payment']; ?>  
                                </td>
                                <td>
                                    <?php echo $row['var_country']; ?>  
                                </td>                         
                                <td>
                                    <a class="btn default btn-xs red chargeview" id="<?php echo $row['int_glcode']; ?>" data-toggle="modal" href="#myModal_autocomplete">
                                        <i class="fa fa-edit">View</i>
                                    </a>

                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
<form method="post" id="add_quoterequests" class="form-horizontal">
    <div id="myModal_autocomplete" class="modal fade" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Issue Chargeback</h4>
                </div>
                <div class="portlet-body form">                                                    
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">                                                                       
                                    <label class="control-label col-md-2">Amount:</label>
                                    <div class="col-md-8">
                                        <!--<input type="hidden" name="id" id="<?php echo $row['int_glcode']; ?>" value="<?php echo $row['int_glcode']; ?>">-->
                                        <input type="text" class="form-control amount" placeholder="amount" name="amount">
                                    </div>
                                </div>
                            </div>                                                                
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Comment:</label>
                                    <div class="col-md-8">
                                        <textarea rows="3" class="form-control comment" name="comment"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                                                  
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" id="submit_btn" class="btn green"><i class="fa fa-check"></i> Submit</button>
                    <!--                    <a class="btn green cahargeback" data-toggle="modal" href="#chargeback_modal">
                                                        <i class="fa fa-check">Submit</i>
                                                    </a>-->
                </div>
            </div>
        </div>
    </div>
</form>
<form method="post" id="add_quoterequests" class="form-horizontal">
    <div id="chargeback_modal" class="modal fade" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Issue Chargeback</h4>
                </div>
                <div class="portlet-body form">                                                    
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <!--                                <div class="form-group">                                                                       
                                                                    <label class="control-label col-md-2">Amount:</label>
                                                                    <div class="col-md-8">
                                                                        <input type="hidden" name="id" id="<?php echo $row['int_glcode']; ?>" value="<?php echo $row['int_glcode']; ?>">
                                                                        <input type="text" class="form-control amount" placeholder="amount" name="amount">
                                                                    </div>
                                                                </div>-->
                                <p class="chrgeback_html">Are you sure that you want to issue a chargeback to Affiliate ID XXXXXX account in the amount of $XXX.XX?</p>
                            </div>                                                                
                        </div>
                    </div>                                                  
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">NO</button>
                    <button type="button" id="btn" class="btn blue"><i class="fa fa-check"></i>Yes</button>
                    <!--                    <a class="btn green " data-toggle="modal" href="#chargeback_modal">
                                                        <i class="fa fa-check">Submit</i>
                                                    </a>-->
                </div>
            </div>
        </div>
    </div>
</form>