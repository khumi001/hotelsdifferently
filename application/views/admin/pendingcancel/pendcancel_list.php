<style>
    .portlet-body.data-grid-custom .col-sm-12.col-md-3 {
        display: none;
    }
    .portlet-body.data-grid-costom .col-sm-12.col-md-3 {
        display: none;
    }
    .portlet-body.data-grid-costom .form-control.input-small.input-inline {
        width: 275px !important;
    }
    .control-label{text-align: center}
    .input-small {
        width: 245px !important;
    }
    .table-condensed > thead > tr > th, .table-condensed > tbody > tr > th, .table-condensed > tfoot > tr > th, .table-condensed > thead > tr > td, .table-condensed > tbody > tr > td, .table-condensed > tfoot > tr > td {
        padding: 4px;
    }
    .datepicker {
        width: 195px !important;
    }

</style>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="">
            <?php
            if(isset($error))
            {
                ?>
                <div class="alert alert-danger"><?php echo $error; ?></div>
            <?php
            }
            ?>
            <?php
            if(isset($success))
            {
                ?>
                <div class="alert alert-success"><?php echo $success; ?></div>
            <?php
            }
            ?>
        </div>
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-user"></i>Pending Cancellations
                </div>
                <div class="actions">
                </div>
            </div>
            <div class="portlet-body data-grid-custom">
                <table class="table table-striped table-bordered table-hover "  id="sample_2">
                    <thead>
                    <tr>
                        <th>Date/Time</th>
                        <th>Check-in</th>
                        <th>Check-out</th>
                        <th>Hotel</th>
                        <th>Email address</th>
                        <th>Account ID</th>
                        <th>Confirmation number </th>
                        <th>Paid</th>
                        <th style="text-align:center;">Source</th>
                        <th>Phone number</th>
                        <th>Source conf </th>
                        <th>Refunded</th>
                        <th>Refund #</th>
                        <?php
                        if(!isSkipLogin())
                        {
                            ?>
                            <th>Processed</th>
                        <?php
                        }
                        ?>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach($confReservations as $res)
                    {
                        $bookUser = getUserById($res->uid);
                        ?>
                        <tr>
                            <td><?php echo getBookingDateTime($res->book_id); ?></td>
                            <td><?php echo getBookingcheckIn($res->book_id); ?></td>
                            <td><?php echo getBookingcheckOut($res->book_id); ?></td>
                            <td><?php echo getBookinghotelName($res->book_id); ?></td>
                            <td><?php echo $bookUser->var_email; ?></td>
                            <td><?php echo $bookUser->var_accountid; ?></td>
                            <td><?php echo $res->confirm_no; ?></td>
                            <td><?php echo "$".getBookingperRoomPaid($res->book_id); ?></td>

                            <td>
                                <input type="text" <?php if(isSkipLogin()){ ?> readonly="true"  <?php } ?> name="sourceName<?php echo $res->id; ?>" id="sourceName<?php echo $res->id; ?>" class="form-control" style="height: 33px; width: 140px; text-align: center;" value="<?php echo getSourcename($res->sourceID); ?>"  onfocus="this.placeholder = ''">
                                <input type='hidden' name='sourceId<?php echo $res->id; ?>' id='sourceId<?php echo $res->id; ?>'>
                                <script type="text/javascript">
                                    jQuery(document).ready(function () {
                                        jQuery("#sourceName<?php echo $res->id; ?>").autocomplete({
                                            source: function (request, response)
                                            {
                                                jQuery.getJSON(baseurl + "admin/conf_res/get_sources", {term: jQuery("#sourceName<?php echo $res->id; ?>").val()},
                                                    response);
                                            },
                                            minLength: 3,
                                            select: function (event, ui)
                                            {
                                                $("#sourceId<?php echo $res->id; ?>").val(ui.item.id);
                                                change_source(<?php echo $res->id; ?> , ui.item.id);
                                                $("#sourcenum<?php echo $res->id; ?>").text("("+ui.item.phone+")");
                                            }
                                        });
                                    });
                                </script>
                            </td>
                            <td id="sourcenum<?php echo $res->id; ?>">
                                <?php echo "(".getSourcePhone($res->sourceID).")"; ?>
                            </td>
                            <td>
                                <input  <?php if(isSkipLogin()){ ?> readonly="true"  <?php } ?> type="text" value="<?php echo $res->reservationId; ?>" style="width:76px;float:left;" id="reserv_id<?php echo $res->id; ?>" />
                                <div <?php if(!isSkipLogin()){ ?> onclick="change_res(<?php echo $res->id; ?>)"  <?php } ?>><i style="background: #dbdbe2;padding: 4.3px;margin-top: -0.3px;border: 1px solid;" class="fa fa-pencil" aria-hidden="true"></i></div>
                            </td>

                            <?php echo form_open('',array('id' => 'res_'.$res->id)); ?>
                            <td><input <?php if(isSkipLogin()){ ?> readonly="true"  <?php } ?> type="text" name="ret_amount" class="form-control amount" value="<?php echo $res->return_amount; ?>" /></td>
                            <td><input <?php if(isSkipLogin()){ ?> readonly="true"  <?php } ?> type="text" name="confirm_number" class="form-control" value="<?php echo $res->stripe_no; ?>" /></td>
                            <?php
                            if(!isSkipLogin())
                            {
                                ?>
                                <td><input class="btn btn-info procBut" id="<?php echo $res->id; ?>" type="submit" class="form-control" value="Processed" />
                                    <input type="hidden" value="<?php echo $res->id; ?>" name="rid" />
                                </td>
                            <?php
                            }
                            ?>
                            <?php echo form_close(); ?>
                        </tr>
                    <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<div id="processModal" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog" style="width: 800px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Please enter password</h4>
            </div>
            <div class="modal-body">
                <div class="form-group furture-form-style">
                    <label>Password:*</label>
                    <input type="password" class="form-control form-style" id="process_delete_password" >
                </div>
            </div>
            <div class="modal-footer" >
                <button type="button" class="btn btn-default" id="close_model" data-dismiss="modal" >Close</button>
                <button type="button" class="btn btn-default" onClick="processCall()" >Ok</button>
            </div>
        </div>
    </div>
</div>


<div id="deleteModal" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog" style="width: 800px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Please enter password</h4>
            </div>
            <div class="modal-body">
                <div class="form-group furture-form-style">
                    <label>Password:*</label>
                    <input type="password" class="form-control form-style" id="delete_password" >
                </div>
            </div>
            <div class="modal-footer" >
                <button type="button" class="btn btn-default" id="close_model" data-dismiss="modal" >Close</button>
                <button type="button" class="btn btn-default" onClick="changeResCall()" >Ok</button>
            </div>
        </div>
    </div>
</div>

<script>

    var PASS_TO_DELETE = '<?php echo PASS_TO_DELETE; ?>';
    var value =0;
    var reservationId = 0;

    function processCall(){
        if($('#process_delete_password').val() == PASS_TO_DELETE){
            $("#processModal").modal('hide');
            $("#res_"+reservationId).submit();
        }else{
            alert('Sorry! Password is wrong.');
        }
    }
    function change_res(val)
    {
        $('#loading').show();
        value = val;
        $("#deleteModal").modal('show');
        //admin_changed
    }

    function changeResCall(){
        if($('#delete_password').val() == PASS_TO_DELETE){
            $.ajax({
                type: 'post',
                url: "<?php echo base_url();?>" + 'admin/conf_res/changeTransectionNum/'+value,
                data: {'nconf': $('#reserv_id'+value).val()},
                success: function(output) {
                    $('#loading').hide();
                    if(output)
                    {
                        $("#deleteModal").modal('hide');
                        alert("Updated");
                    }
                    else
                    {
                        alert("Error in updation!");
                    }
                }
            });
        }else{
            alert('Sorry! Password is wrong.');
        }
    }

    function change_source(id , source_id)
    {
        console.log(id+"-"+source_id);
        $.ajax({
            type: 'post',
            url: baseurl + 'admin/conf_res/changesource_res',
            data: {id: id , source_id : source_id},
            success: function(data) {

            }
        });
    }

    $(document).ready(function(){
        $(".procBut").click(function(){
            reservationId = $(this).attr('id');
            $("#processModal").modal('show');
            return false;
        });
        /*$('#sample_2').dataTable({
         "order": [[ 1, 'desc' ]]
         });*/
    });
</script>