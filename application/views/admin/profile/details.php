<?php  //print_r($result); exit;?>
<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-reorder"></i> Edit profile
        </div>
    </div>
    <div class="portlet-body form">
        <!--BEGIN FORM-->
        <form class="form-horizontal" name="user_edit" id="profile_edit" method="post" enctype="multipart/form-data">
        <?php //foreach($result as $row){  ?>
            <div class="form-body">
                <input type="hidden" value="" id="user_id" name="user_id"/>
                <div class="form-group">
                    <label class="col-md-2 control-label">Status :</label>
                    <div class="col-md-2">
                        <select name="status" id="status" class="form-control select2me">
                            <option value="Active">Active</option>
                            <option value="Inactive">Inactive</option>
                        </select>  
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">Name :</label>
                    <div class="col-md-2">
                        <select name="firstname" id="firstname" class="form-control select2me">
                            <option value="Mr.">Mr.</option>
                            <option value="Mrs.">Mrs.</option>
                            <option value="Miss.">Miss.</option>
                        </select>  
<!--                                    <input type="text" class="form-control " placeholder="Enter First Name"  name="firstname" id="firstname">      -->
                    </div>                              
                    <div class="col-md-3">
                        <input type="text" class="form-control" id="firstname" placeholder="Enter First Name"  name="firstname" value="<?php echo $result[0]['var_fname'] ?>">
                    </div>
                    <div class="col-md-3">
                        <input type="text" class="form-control" id="lastname" placeholder="Enter Last Name"  name="lastname" value="<?php echo $result[0]['var_lname'] ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">Postal Address :</label>
                    <div class="col-md-3">
                        <input type="text" name="address" id="address" class="form-control required" placeholder="Enter Your address" value="<?php echo $result[0]['txt_address'] ?>"> 
                    </div>
                    <div class="col-md-3">
                        <select name="city" id="city" class="form-control select2me">
                            <option value="Ahemdabad">East Victoria Park</option>
                            <option value="Rajkot">West Victoria Park</option>
                            <option value="Bhavnagar">North Victoria Park</option>
                            <option value="Baroda">South Victoria Park</option>
                        </select>   
                    </div>
                    <div class="col-md-2">
                        <select name="country" id="country" class="form-control select2me">
                            <option value="WA">WA</option>
                            <option value="QLD">QLD</option>
                            <option value="NT">NT</option>
                            <option value="VIC">VIC</option>
                        </select>   
                    </div>
                </div> 
                <div class="form-group">
                    <label class="col-md-2 control-label">Date Of Birth :</label>
                    <div class="col-md-3">
                        <input type="text" class="form-control required date-picker" placeholder="Enter Your Birthdate" format="mm-dd-yyyy" name="dob" id="dob"  value="<?php echo $result[0]['var_dob']?>">   
                    </div>
                </div>                        
                <div class="form-group">
                    <label class="col-md-2 control-label">Email :</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control " placeholder="Enter Your Email"  name="email" id="email" value="<?php echo $result[0]['var_email']?>">                                  
                    </div>
                </div>    
                <div class="form-group">
                    <label class="control-label col-md-2">Subscription :</label>
                    <div class="col-md-9">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div>                                      
                                <div class="radio-list col-md-12">
                                      <?php
                                    for ($i = 0; $i < count($userdetail['newslatter']); $i++) {
                                        ?>
                                        <label class="radio-inline">
                                            <input type="radio" name="check[]" id="check" value="<?php echo $userdetail['newslatter'][$i]; ?>" <?php if ($userdetail['newslatter'][$i] == $result[0]['var_subscription']) { ?> checked <?php } ?> ><?php echo $userdetail['newslatter'][$i]; ?></label>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>          
            </div>
          <?php //}  ?>
            <div class="form-actions fluid">
                <div class="col-md-offset-3 col-md-9">
                    <button type="submit" class="btn blue">Submit</button>
                    <button type="button" class="btn default">Cancel</button>
                </div>
            </div>
        </form>
        <!--END FORM-->
    </div>

</div>


