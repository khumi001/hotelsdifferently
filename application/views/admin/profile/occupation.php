<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-reorder"></i> Occupation                            
        </div>
    </div>
    <div class="portlet-body form">
        <!--BEGIN FORM-->
        <form  action="<?php echo base_url(); ?>admin/feature/add_feature" class="form-horizontal" name="feature_add" id="feature_add" method="post" enctype="multipart/form-data">
            <div class="form-body">                                       
                <div class="form-group">
                    <label class="col-md-2 control-label">Enter your subject :</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" name="subject" id="subject">        
                    </div>
                </div> 
                <div class="form-group">
<!--                    <label class="control-label col-md-3"></label>-->
                    <div class="col-md-8">
                        <textarea class="ckeditor form-control" name="editor1" rows="6">Fell free to ask,tell,advise,question,suggest...anything!:)</textarea>
                    </div>
                </div>
            </div>   
            <div class="form-actions fluid">
                <div class="col-md-offset-3 col-md-9">
                    <button type="submit" class="btn blue">Submit</button>
                    <button type="button" class="btn default">Cancel</button>
                </div>
            </div>
        </form>
        <!--END FORM-->
    </div>

</div>


