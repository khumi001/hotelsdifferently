<?php //print_r($result);exit;?>
<div class="portlet box blue">
<div class="portlet-title">
        <div class="caption">
                <i class="fa fa-reorder"></i> Add Picture
        </div>
</div>
<div class="portlet-body form">
         <!--BEGIN FORM-->
        <form   class="form-horizontal" name="profileimg" id="profileimg" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>admin/profile/profile_img">
            
            <div class="form-body">
                <div class="form-group">
                    <label class="col-md-2 control-label"></label>
                    <div class="col-md-2">
                           <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 167px; height: 120px;">
                                    <img alt="" src="<?php echo base_url().$result[0]['profile_pic'];?>">
                                </div>
                                <div>                                   
                                        <span class="btn default btn-file">                                          
                                                <span class="fileinput-new">
                                                         Select image
                                                </span>
                                                <span class="fileinput-exists">
                                                         Change
                                                </span>
                                                <input type="file" name="prof_img" value="<?php echo $result[0]['profile_pic'];?>">
                                        </span>
                                        <a href="#" class="btn default fileinput-exists" data-dismiss="fileinput">
                                                 Remove
                                        </a>                                  
                                </div>
                           </div>
                    </div>
                </div>
            </div>
            <div class="form-actions fluid">
                    <div class="col-md-offset-2 col-md-9">
                            <button type="submit" class="btn blue">Submit</button>
                            <button type="button" class="btn default">Cancel</button>
                    </div>
            </div>
        </form>
         <!--END FORM-->
    </div>
    
</div>


