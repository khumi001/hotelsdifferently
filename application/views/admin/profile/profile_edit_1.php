<style>
    .has-switch label,.has-switch span {
        height: 33px;
    }
    .has-switch label {
        margin-top: -4px;}
    </style>
<?php
$profile_content = $this->profile_model->get_profile('admin');
if($profile_content[0]['profile_pic'] == '')
{
    $profile_content[0]['profile_pic'] = 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image';
}
else
{
    $profile_content[0]['profile_pic'] = base_url().'/'.$profile_content[0]['profile_pic'];
}
//echo $profile_content[0]['profile_pic'];
?>
<div class="tab-pane" id="tab_1_3">
    <div class="row profile-account">
        <div class="col-md-3">
            <ul class="ver-inline-menu tabbable margin-bottom-10">
                <li class="active">
                    <a data-toggle="tab" href="#tab_1-1" >
                        <i class="fa fa-cog"></i> Personal info
                    </a>
                    <span class="after">
                    </span>
                </li>
                <li>
                    <a data-toggle="tab" href="#tab_3-3" >
                        <i class="fa fa-lock"></i> Change Password
                    </a>
                </li>
                <li>
                    <a data-toggle="tab" href="#tab_2-2">
                        <i class="fa fa-picture-o"></i> Change Image
                    </a>
                </li>

            </ul>
        </div>
        <div class="col-md-9">
            <div class="tab-content">
                <div id="tab_1-1" class="tab-pane active">
                    <form role="form" id="personal_info" action="#" method="post">
                        <?php
                       
 foreach ($result as $rows){ ?>
                        <input type="hidden" value="<?php echo $rows['id']; ?>" name="eid"/>
                        <label class="control-label">User Name</label>
                        <div class="form-group">
                            <input type="text" placeholder="Username" value="<?php echo $rows['var_username']; ?>" name="username" id="username" class="form-control"/>
                        </div>
                         <label class="control-label">Email Address</label>
                        <div class="form-group">
                            <input type="email" placeholder="Email Address" value="<?php echo $rows['var_email']; ?>" name="email" id="email" class="form-control"/>
                        </div>
                        <label class="control-label">First Name</label>
                        <div class="form-group">
                            <input type="text" placeholder="First Name" value="<?php echo $rows['var_fname']; ?>" name="fname" id="fname" class="form-control"/>
                        </div>
                         
                        <label class="control-label">Last Name</label>
                        <div class="form-group">
                            <input type="text" placeholder="Last Name" value="<?php echo $rows['var_lname']; ?>" name="lname" id="lname" class="form-control"/>
                        </div>
                        <div class="margiv-top-10">
                            <input type="submit" class="btn green" value="Save"/>
                            <a href="#" class="btn default">
                                Cancel
                            </a>
                        </div>
                         <?php } ?>
                    </form>
                       
                </div>
                          <div id="tab_2-2" class="tab-pane">
          <!--                                    <p>
                                 Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
                        </p>-->
                    <form action="<?php echo base_url(); ?>admin/profile/profile_img" enctype="multipart/form-data" method="post" role="form">
                        <div class="form-group">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                    <img src="<?php echo $profile_content[0]['profile_pic']; ?>" alt=""/>   
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                                </div>
                                <div>
                                    <span class="btn default btn-file">
                                        <span class="fileinput-new">
                                            Select image
                                        </span>
                                        <span class="fileinput-exists">
                                            Change
                                        </span>
                                        <input type="file" name="prof_img">
                                    </span>
                                    <a href="#" class="btn default fileinput-exists" data-dismiss="fileinput">
                                        Remove
                                    </a>
                                </div>
                            </div>
                          
                        </div>
                        <div class="margin-top-10">
                            <button type="submit" class="btn green">
                                Submit
                            </button>
                            <button type="button" class="btn default">
                                Cancel
                            </button>
                        </div>
                    </form>
                </div>
                <div id="tab_3-3" class="tab-pane ">
                    <form method="post" id="change_password" action="#">
                        <label class="control-label">Current Password</label>
                        <div class="form-group">
                            <input type="password" name="old_psw" id="oldpassword" class="form-control"/>
                        </div>
                        <label class="control-label">New Password</label>
                        <div class="form-group">

                            <input type="password" name="new_psw" id="newpassword" class="form-control"/>
                        </div>
                        <label class="control-label">Re-type New Password</label>
                        <div class="form-group">
                            <input type="password" name="conf_psw" class="form-control"/>
                        </div>
                        <div class="margin-top-10">
                            <input type="submit" class="btn green" value="Change Password"/>
                        </div>

                    </form>
                </div>

            </div>
        </div>
        <!--end col-md-9-->
    </div>
</div>

