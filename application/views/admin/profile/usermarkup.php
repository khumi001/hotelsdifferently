<style type="text/css">
    .col-md-5.no-padding {
        padding: 0;
    }

    .col-md-7.no-padding {
        padding: 0;
    }

    .control-label.lbe {
        font-weight: bold;
        left: 15px;
        position: relative;
        top: 5px;
    }

    .submit-btn-furture {
        background: rgba(0, 0, 0, 0) linear-gradient(to bottom, #71d1ff 0%, #22b3f4 4%, #198fef 100%) repeat scroll 0 0;
        border: medium none;
        box-shadow: 0 1px 3px rgba(0, 0, 0, 0.4);
        color: #fff;
        font-family: "FuturaStd-Book";
        font-size: 14px;
        margin: 10px 0 0;
        padding: 7px 15px;
        text-shadow: 1px 1px 2px #000;
        text-transform: uppercase;
    }
</style>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    USER MARKUP
                </div>
                <div class="actions">
                </div>
            </div>
            <div class="portlet-body">
                <div id="mainlog">
                    <form action="<?php echo base_url('admin/markup'); ?>" id="markupUpForm" method="post">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">TRAVEL PROFESSIONAL + ENTITY: </label>
                                    <input class="priceper form-control" id="TF_EN" name="TF_EN" placeholder="10.50" value="<?php echo $records['TF_EN']?>" type="text">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">INDIVIDUAL MEMBER: </label>
                                    <input class="priceper form-control" id="U" name="U" placeholder="12.50" value="<?php echo $records['U']?>" type="text">
                                </div>
                            </div>
                        </div>
                        <?php
                        if (!isSkipLogin()) {
                            ?>
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <button type="submit" id="markupUp" class="submit-btn-furture">update</button>
                                </div>
                            </div>
                        <?php }?>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="deleteModal" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog" style="width: 800px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Please enter password</h4>
            </div>
            <div class="modal-body">
                <div class="form-group furture-form-style">
                    <label>Password:*</label>
                    <input type="password" class="form-control form-style" id="delete_password" >
                </div>
            </div>
            <div class="modal-footer" >
                <button type="button" class="btn btn-default" id="close_model" data-dismiss="modal" >Close</button>
                <button type="button" class="btn btn-default" onClick="markCall()" >Ok</button>
            </div>
        </div>
    </div>
</div>

<script>
    var PASS_TO_DELETE = '<?php echo PASS_TO_DELETE; ?>';
    function markCall(){
        if($('#delete_password').val() == PASS_TO_DELETE){
            $("#markupUpForm").submit();
        }else{
            alert('Sorry! Password is wrong.');
        }
    }
    $(document).ready(function() {
        $("#markupUp").click(function(){
            $("#deleteModal").modal('show');
            return false;
        });
        $(".priceper").maskMoney({
            prefix: '',
            allowNegative: true,
            thousands: ',',
            decimal: '.',
            affixesStay: false
        });
    });
</script>