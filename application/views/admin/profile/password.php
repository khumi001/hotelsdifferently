<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-reorder"></i> Change Password
        </div>
    </div>
    <div class="portlet-body form">
        <!--BEGIN FORM-->
        <form class="form-horizontal" name="change_password" id="change_password" method="post">
            <div class="form-body">
               <div class="form-group">
                    <label class="col-md-3 control-label con-margin ">Current Password :</label>
                    <div class="col-md-4">
                        <input type="password" class="form-control" name="old_psw" id="old_psw">         
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label con-margin">New Password :</label>
                    <div class="col-md-4">
                        <input type="password" class="form-control" name="new_psw" id="new_psw">      
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label con-margin">Confirm New Password :</label>
                    <div class="col-md-4">
                        <input type="password" class="form-control " name="conf_psw" id="conf_psw">      
                    </div>
                </div>
                <div class="form-actions fluid">
                    <div class="col-md-offset-1 col-md-9">
                        <button type="submit" class="btn blue">Submit</button>
                        <button type="button" class="btn default">Cancel</button>
                    </div>
                </div>       
            </div>
        </form>
        <!--END FORM-->
    </div>
</div>
<div class="modal fade" id="delete_conformation" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Conformation</h4>
            </div>
            <div class="modal-body">
                Are You Sure You want to delete this file?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Close</button>
                <button type="button" class="btn red" id="delete_data_model">Delete</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

