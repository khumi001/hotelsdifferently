<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-user"></i>Authorization
                </div>
            </div>
            <div class="portlet-body">

                <table class="table table-striped table-bordered table-hover" id="sample_2">
                    <thead>
                    <tr>
                        <th>
                            Account ID
                        </th>
                        <th>
                            Name
                        </th>
                        <th>
                            Email address
                        </th>
                        <th>
                            User Type
                        </th>

                        <?php
                        if (!isSkipLogin()) {
                            ?>
                            <th>
                                Authorize
                            </th>
                        <?php } ?>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($userlist as $row) {
                        if ($row['int_glcode'] != 0) {
                            ?>
                            <tr>
                                <td><?php echo $row['var_accountid']; ?></td>
                                <td><?php echo $row['var_fname']; ?></td>
                                <td><?php echo $row['var_email']; ?></td>
                                <td><?php if ($row['chr_user_type'] == 'A') {
                                        echo 'Admin';
                                    } else if ($row['chr_user_type'] == 'AF') {
                                        echo 'Affiliate';
                                    } else if ($row['chr_user_type'] == 'U') {
                                        echo 'Member';
                                    } ?></td>

                                <?php
                                if (!isSkipLogin()) {
                                    ?>
                                    <td>
                                        <a class="btn default btn-xs blue btn_authorization"
                                           data-id="<?php echo $row['int_glcode']; ?>" href="javascript:;">
                                            <i class="fa fa-edit">Authorize</i>
                                        </a>
                                        <a class="btn default btn-xs red btn_delete_user"
                                           data-email="<?php echo $row['var_email']; ?>"
                                           data-id="<?php echo $row['int_glcode']; ?>" data-toggle="modal"
                                           href="#deleteConfModal">
                                            <i class="fa fa-trash-o"> Delete</i>
                                        </a>
                                    </td>
                                <?php } ?>
                            </tr>
                        <?php }
                    } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

<div id="deleteModal" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog" style="width: 800px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Please enter password</h4>
            </div>
            <div class="modal-body">
                <div class="form-group furture-form-style">
                    <label>Password:*</label>
                    <input type="password" class="form-control form-style" id="deletePassword">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="close_model" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-default" id="deleteButton">Ok</button>
            </div>
        </div>
    </div>
</div>

<div id="deleteConfModal" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog" style="width: 800px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Please enter password</h4>
            </div>
            <div class="modal-body">
                <div class="form-group furture-form-style">
                    <label>Password:*</label>
                    <input type="password" class="form-control form-style" id="deleteConfPassword">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="close_model" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-default" id="deleteConfModalButton">Ok</button>
            </div>
        </div>
    </div>
</div>

<div id="mymodal_autho" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Authorization</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <!--                    <label class="col-md-3 control-label">Checkboxes</label>-->
                    <form method="post" id="authorization" name="authorization">
                        <div class="checkbox-list">
                            <div id="lll"></div>
                            <!--                            <label>
                                                            <input type="checkbox" name="authocheck[]" value="Sent Quotes"> Sent Quotes </label>
                                                        <label>
                                                            <input type="checkbox" name="authocheck[]" value="Savings Database"> Savings Database </label>
                                                        <label>
                                                            <input type="checkbox" name="authocheck[]" value="Coupons"> Coupons </label>
                                                        <label>
                                                            <input type="checkbox" name="authocheck[]" value="Affiliate Request"> Affiliate Request </label>
                                                        <label>
                                                            <input type="checkbox" name="authocheck[]" value="Affiliate Database"> Affiliate Database </label>
                                                        <label>
                                                            <input type="checkbox" name="authocheck[]" value="Affiliate Tracker"> Affiliate Tracker </label>
                                                        <label>
                                                            <input type="checkbox" name="authocheck[]" value="Affiliate Payment"> Affiliate Payment </label>
                                                        <label>
                                                            <input type="checkbox" name="authocheck[]" value="Posted Payments"> Posted Payments </label>
                                                         <label>
                                                            <input type="checkbox" name="authocheck[]" value="Chargebacks"> Chargebacks </label>
                                                        <label>
                                                          <input type="checkbox" name="authocheck[]" value="Processed Chargebacks"> Processed Chargebacks </label>
                                                        <label>
                                                          <input type="checkbox" name="authocheck[]" value="Authorization"> Authorization </label>
                                                        <label>
                                                          <input type="checkbox" name="authocheck[]" value="IP Address BAN"> IP Address BAN </label>
                                                        <label>
                                                          <input type="checkbox" name="authocheck[]" value="Processed IP BAN"> Processed IP BAN </label>
                                                        
                                                        <label>
                                                          <input type="checkbox" name="authocheck[]" value="Database Management">  Database Management </label>
                                                         <label>
                                                          <input type="checkbox" name="authocheck[]" value="Login Log"> Login Log  </label>
                                                         <label>
                                                          <input type="checkbox" name="authocheck[]" value="Statistics"> Statistics </label>
                                                         <label>
                                                          <input type="checkbox" name="authocheck[]" value="Profile"> Profile </label>                           -->

                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="btn_submit" class="auth btn blue"><i class="fa fa-check"></i>Submit</button>
            </div>
        </div>
    </div>
</div>


<!--delete model-->
<div id="mymodal_delete" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Delete User</h4>
            </div>
            <div class="modal-body">
                <span class="user_email"><h5>Are you sure you want to delete </h5></span>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" id="btndelete" class="btn red"><i class="fa fa-check"></i>Delete</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $("#deleteConfModalButton").click(function () {
            if ($('#deleteConfPassword').val() == "bUw#kef5ate$U8a2") {
                $('#deleteConfModal').modal('hide');
                $('#mymodal_delete').modal('show');
            } else {
                alert('Sorry! Password is wrong.');
            }

        });
    });
</script>
