<!--<script>
    var city_array = new Array;
<?php foreach ($city as $row) { ?>
        city_array.push('<?php echo trim($row['var_city']) ? trim($row['var_city']) . ',' : " "; ?><?php echo trim($row['var_state']) ? trim($row['var_state']) . ',' : " "; ?><?php echo $row['var_country']; ?>');
<?php } ?>    
</script>     -->
<style type="text/css">
    .ui-autocomplete {z-index: 99999 !important;}
    .price_tooltips span{visibility: hidden;display: none;}
    .tooltips span {position: absolute;color: #FFFFFF;background: #000000;height: 30px;line-height: 30px;text-align: center;visibility: hidden;border-radius: 6px;}
    .tooltips span::after {border-left: 8px solid transparent;border-right: 8px solid transparent;border-top: 8px solid #000000;content: "";height: 0;left: 50%;margin-left: -8px;position: absolute;top: 86%;width: 0;}
    .price_tooltips.tooltips span {bottom: 44px !important;display: block;left: 0;margin-left: 141px !important;opacity: 0.8;visibility: visible;z-index: 999;width:155px;}
</style>

<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-user"></i>Quote Requests 
                </div>                          
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="quoterequests">
                    <thead>
                        <tr>
                            <th>
                                Username
                            </th>
                            <th>
                                Points
                            </th>
                            <th>
                                Date/Time
                            </th>
                            <th>
                                City
                            </th>
                            <th>
                                Check-in
                            </th>
                            <th>
                                Check-out
                            </th>
                            <th>
                                Rooms
                            </th>
                            <th>
                                NOH
                            </th>
                            <th>
                                Stars
                            </th>
                            <th>
                                REPLY
                            </th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php
                       
                        foreach ($quote as $row) {
                            ?>
                            <tr class="odd gradeX">
                                <td>
                                    <?php echo $row['var_username']; ?>
                                    <!--bob123-->
                                </td>
                                <td>
                                    <?php echo $row['var_point']; ?>
                                    <!--1,329-->
                                </td>                                   
                                <td>
                                    <?php echo $row['dt_reuested_date']; ?>
                                    <!--                                             01/11/14
                                                                                 7:45AM-->
                                </td>
                                <td>
                                    <?php echo $row['var_city']; ?>
                                    <!--                                            Las Vegas-->
                                </td>
                                <td>

                                    <?php
                                    echo date("m-d-Y", strtotime($row['var_checkin']));
                                    //                                            echo $row['var_checkin'];
                                    ?>
                                    <!--                                            01/11/14-->
                                </td>
                                <td>
                                    <?php
                                    echo date("m-d-Y", strtotime($row['var_checkout']));
                                    // echo $row['var_checkout'];
                                    ?>
                                    <!--                                            01/13/14-->
                                </td>
                                <td>
                                    <?php echo $row['var_room']; ?>
                                    <!--                                           1-->
                                </td>
                                <td>
                                    <?php echo $row['var_hotelname']; ?>
                                    <!--                                            Ritz Carlton-->
                                </td>
                                <td>
                                    <?php echo $row['var_rating']; ?>
                                </td>
                                
                                <td>
                                    <a href="#myModal_autocomplete" data-toggle="modal" class="label label-sm label-success quote_val" data-id="<?php echo $row['int_glcode']; ?>" data-resubmit-id="<?php echo $row['fk_resubmit_id']; ?>">Quote</a>
                                    <a href="#delete_modal" data-toggle="modal" class="label label-sm label-danger delete_id"  data-id="<?php echo $row['int_glcode']; ?>">Delete!</a>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>

                </table>
            </div>

            <div id="myModal_autocomplete" class="modal fade quote_modal_form" role="dialog" aria-hidden="true">
                <div class="modal-dialog" style="width:70%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close popupclose" data-dismiss="modal" aria-hidden="true"></button>

                            <h4 class="modal-title">Add Quote</h4>
                        </div>

                        <div class="portlet-body form add_quote">
                            <div class="form-body">
                                <form method="post" role="form" id="add_quote" class="form-horizontal" action=""> 
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group group_margin">
                                                <label class="control-label col-md-4">Username:</label>
                                                <div class="col-md-7">
                                                    <input type="text" class="form-control username"  readonly="true">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group group_margin">
                                                <label class="control-label col-md-4">Check-in:</label>
                                                <div class="col-md-7">
                                                    <input type="text" class="form-control indate"  readonly="true">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group group_margin">
                                                <label class="control-label col-md-4">Rooms:</label>
                                                <div class="col-md-7">
                                                    <input type="text" class="form-control rooms"  readonly="true">
                                                    <input type="hidden" class="parentaccount">
                                                    <input type="hidden" class="coupan">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group group_margin">
                                                <label class="control-label col-md-4">Account ID:</label>
                                                <div class="col-md-7">
                                                    <input type="text" class="form-control accountid"  readonly="true">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group group_margin">
                                                <label class="control-label col-md-4">Check-out:</label>
                                                <div class="col-md-7">
                                                    <input type="text" class="form-control outcheck"  readonly="true">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group group_margin">
                                                <label class="control-label col-md-4">Children:</label>
                                                <div class="col-md-7">
                                                    <input type="text" class="form-control children"  readonly="true">
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group group_margin">
                                                <label class="control-label col-md-4">Points:</label>
                                                <div class="col-md-7">
                                                    <input type="text" class="form-control point"  readonly="true">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group group_margin">
                                                <label class="control-label col-md-4">City:</label>
                                                <div class="col-md-7">
                                                    <input type="text" class="form-control city"  readonly="true">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group group_margin">
                                                <label class="control-label col-md-4">Adults:</label>
                                                <div class="col-md-7">
                                                    <input type="text" class="form-control adults"  readonly="true">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group group_margin">
                                                <label class="control-label col-md-4">Date of request:</label>
                                                <div class="col-md-7">
                                                    <input type="text" class="form-control requesteddate"  readonly="true">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group group_margin">
                                                <label class="control-label col-md-4">Name of hotel:</label>
                                                <div class="col-md-7">
                                                    <input type="text" class="form-control hotelname"  readonly="true">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group group_margin">
                                                <label class="control-label col-md-4">Comments:</label>
                                                <div class="col-md-7">
                                                    <textarea rows="1" class="form-control comment" readonly="true" id="comment"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group group_margin">
                                                <label class="control-label col-md-4">Name:</label>
                                                <div class="col-md-7">
                                                    <input type="text" class="form-control res_name"  readonly="true">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">

                                    </div>
                                </form>
                                <form method="post" role="form" id="rplyquote" name="rplyquote" class="form-horizontal" action="">
                                    <div class="quote1">
                                        <h4 style="text-align: center; margin: 0 0 4px;"><strong>QUOTE#1</strong></h4>
                                        <input type="hidden" id="fk_quote" name="fkquote">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group  group_margin">
                                                    <label class="control-label col-md-4">Name of hotel:</label>
                                                    <div class="col-md-7 valid_quot">
                                                        <input tabindex="1" type="text" name="hotel[]" id="select_hotel" data-position ="1" class="form-control select_hotel validate">
                                                        <!--<input type="text" class="form-control validate" name="hotel[]">-->
                                                        <input type="hidden" name="indate" id="indate" class="indate" value="">
                                                        <input type="hidden" name="outcheck" id="outcheck" class="outcheck" value="">
                                                        <input type="hidden" name="resubmitid" id="resubmitid" class="resubmitid" value="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group group_margin">
                                                    <label class="control-label col-md-4">Site #1:</label>
                                                    <div class="col-md-5 valid_quot">
                                                        <select tabindex="5" class="form-control select2me  input-xmedium site validate select_hotel_name" id="select_name_hotel" name="w_site[]">
                                                            <option value="">--Select--</option>
                                                            <option value="Hotels.com">Hotels.com</option>
                                                            <option value="Expedia.com">Expedia.com</option>
                                                            <option value="Travelocity.com">Travelocity.com</option>
                                                            <option value="Booking.com">Booking.com</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-3 valid_quot site1">
                                                        <input type="text" tabindex="6" class="form-control validate commanPrice ksite" data-position="1" style="width: 180%; margin-left: -23px" name="k_site[]" >
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4 price_tooltips">
                                                <div class="form-group  group_margin">
                                                    <label class="control-label col-md-4">Price quote:</label>
                                                    <!--<img src="<?php echo base_url() ?>public/assets/img/jTxr4ArTE.png" class="hidden img_chatbubble"/>-->
                                                    <div class="col-md-7 valid_quot">
                                                        <input tabindex="11" type="text" class="form-control price validate" id="price_mask"  name="pricequote[]">
                                                        <span class="help-block price_massage hidden" id="price_massage" style="display: none" >You have all ready Registered.</span>
                                                        <input type="hidden" name="price" id="price" >
                                                        <!--<span style="">Minimum $115.00</span>-->
                                                    </div>

                                                </div>
                                            </div>

                                        </div>  
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group  group_margin">
                                                    <label class="control-label col-md-4">Stars:</label>
                                                    <div class="col-md-7 valid_quot">
                                                        <select tabindex="2" class="form-control select2me select2-offscreen input-xmedium validate rating_star_hotel" name="rating[]">
                                                            <option value="">--Select Star--</option>
                                                            <option value="2 STAR">2 Stars</option>
                                                            <option value="2.5 STAR">2.5 Stars</option>
                                                            <option value="3 STAR">3 Stars</option>
                                                            <option value="3.5 STAR">3.5 Stars</option>
                                                            <option value="4 STAR">4 Stars</option>
                                                            <option value="4.5 STAR">4.5 Stars</option>
                                                            <option value="5 STAR">5 Stars</option>
                                                        </select>                                                                            
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group  group_margin">
                                                    <label class="control-label col-md-4">Site #2:</label>
                                                    <div class="col-md-5">
                                                        <input type="text" tabindex="7" class="form-control validate f_site" name="f_site[]">
                                                    </div>
                                                    <div class="col-md-3">
                                                        <input type="text" tabindex="8" class="form-control validate ssite commanPrice" data-position="1" style="width: 180%; margin-left: -23px" name="s_site[]">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group group_margin">
                                                    <label class="control-label col-md-4">TripAdvisor:</label>
                                                    <div class="col-md-7">
                                                        <input tabindex="12" type="text" class="form-control validate hotel_adviser"  name="tripadvisor[]">
                                                        <input type="hidden" class="flag2" value="1">
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group group_margin">                                                                       
                                                    <label class="control-label col-md-4">Room Type:</label>
                                                    <div class="col-md-7 valid_quot col-md-77">
                                                        <input tabindex="3" type="text" name="room_type[]" id="room_type" class="form-control validate room_type">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group group_margin">
                                                    <label class="control-label col-md-4">SRC</label>
                                                    <div class="col-md-5 valid_quot">
                                                        <input type="text" tabindex="9" class="form-control validate src" name="src[]">
                                                    </div>
                                                    <div class="col-md-3 valid_quot">
                                                        <input type="text" tabindex="10" class="form-control validate src_price ssrc commanPrice" style="width: 180%; margin-left: -23px" data-position="1" name="s_src[]">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group group_margin">
                                                    <label class="control-label col-md-4">Comments:</label>
                                                    <div class="col-md-7">
                                                        <textarea rows="1" tabindex="13" class="form-control validate hotel_comments" name="comment[]"></textarea>
                                                        <input type="hidden" class="tabindex" value="12">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                        <div class="col-md-4">
                                                <div class="form-group group_margin">                                                                       
                                                    <label class="control-label col-md-4">Cancellation policy:</label>
                                                    <div class="col-md-7 valid_quot col-md-77">
                                                        <textarea tabindex="4" type="text" name="policy[]" id="select_hotel" class="form-control validate hotel_cancelation"><?php echo "non-refundable" ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group group_margin">                                                                       
                                                    <label class="control-label col-md-4">Address:</label>
                                                    <div class="col-md-7 valid_quot col-md-77">
                                                        <input tabindex="13" type="text" name="address[]" id="address" class="form-control validate address"/>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                    </div>

                                    <div class="quote"></div>
                                    <div class="row" style="text-align: center">
                                        <div class="col-md-12">
                                            <div class="col-md-6 text-right">
                                                <button type="button" tabindex="14" id="addquote" class="btn btn-default"><i class="fa fa-plus"></i> ADD QUOTE </button>
                                            </div>
                                            <div class="col-md-3 afflatehascoupanclass hidden">
                                            <div class="form-group group_margin">
                                                <label class="control-label col-md-12" style="padding: 0 15px; text-align: left;">
                                                    <h3 style="margin: 0px; line-height: 34px">A - C</h3>
                                                </label>
<!--                                                <div class="col-md-7">
                                                    <input type="text" class="form-control res_name"  readonly="true">
                                                </div>-->
                                            </div>
                                        </div>
                                        <div class="col-md-3 afflateclass hidden">
                                            <div class="form-group group_margin">
                                                <label class="control-label col-md-12" style="padding: 0 15px; text-align: left;">
                                                    <h3 style="margin: 0px; line-height: 34px">A</h3>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-3 coupanclass hidden">
                                            <div class="form-group group_margin">
                                                <label class="control-label col-md-12" style="padding: 0 15px; text-align: left;">
                                                    <h3 style="margin: 0px; line-height: 34px">C</h3>
                                                </label>
                                            </div>
                                        </div>
<!--                                            <button type="button" id="minusquote" class="btn btn-default hidden"><i class="fa fa-minus"></i> REMOVE QUOTE </button>-->
                                        </div>
                                    </div>
                                    <div class="modal-footer" style="text-align: center">                                                 
                                        <input type="submit" id="btn" tabindex="14" class="btn green squote sendquote" value="SEND QUOTE">
                                    </div>
                                </form> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>    

<div class="modal fade" id="delete_modal" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Delete Quote</h4>
            </div>
            <div class="modal-body">
                Are you sure you want to delete this Quote?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn red pop_delete_id" id ="delete_quets"data-id="">Delete</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>