<style>
    .price span{visibility: hidden; display: none;}
    .tooltips span {position: absolute;color: #FFFFFF;background: #000000;height: 30px;line-height: 30px;text-align: center;visibility: hidden;border-radius: 6px;}
    .ui-autocomplete {z-index: 99999 !important;}
    .tooltips span::after {border-left: 8px solid transparent;border-right: 8px solid transparent;border-top: 8px solid #000000;content: "";height: 0;left: 50%;margin-left: -8px;position: absolute;top: 86%;width: 0;}
    .price.tooltips span {bottom: 38px;display: block;left: 0;margin-left: 16px;opacity: 0.8;visibility: visible;z-index: 999;width:155px;}
    .copyquote {position: absolute;right: 190px;top: 0;}
    .minusquote {position: absolute;right: 26px;top: 0;}
    .quote_div{position:relative;margin: 10px 0 12px;}
</style>
<div class="quote_div" style="">
    <h4 style="text-align: center; margin: 0 0 25px;"><strong>QUOTE #<?php echo $countdata; ?></strong></h4>
    <button type="button"  id="copyquote" data-id="<?php echo $countdata; ?>" class="btn btn-default copyquote">COPY</button>
    <button type="button" id="minusquote" data-id="<?php echo $countdata; ?>" class="btn btn-default minusquote"><i class="fa fa-minus"></i> REMOVE QUOTE </button>
    <div class="quote2">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group group_margin">
                    <label class="control-label col-md-4">Name of hotel:</label>
                    <div class="col-md-7 valid_quot">
                        <input  tabindex="<?php echo $tabindex + 1; ?>" type="text" class="form-control validate select_hotel requered_true" data-position ="2" name="hotel[]">
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group group_margin">
                    <label class="control-label col-md-4">Site #1:</label>
                    <div class="col-md-5 valid_quot site-1<?php echo $countdata;?>">
                        <select tabindex="<?php echo $tabindex + 4; ?>" class="form-control select2me  requered_true input-xmedium site validate select_hotel_name" name="w_site[]">
                            <option value="">--Select--</option>
                            <option value="Hotels.com">Hotels.com</option>
                            <option value="Expedia.com">Expedia.com</option>
                            <option value="Travelocity.com">Travelocity.com</option>
                            <option value="Booking.com">Booking.com</option>
                        </select>
                    </div>
                    <div class="col-md-3 valid_quot site1">
                        <input type="text" tabindex="<?php echo $tabindex + 5; ?>" class="form-control requered_true validate ksite commanPrice" style="width: 180%; margin-left: -23px" name="k_site[]" >
                    </div>
                </div>
            </div>
            <div class="col-md-4 price_tooltips">
                <div class="form-group group_margin">
                    <label class="control-label col-md-4">Price quote:</label>
                    <div class="col-md-7 valid_quot">
                        <input type="text" tabindex="<?php echo $tabindex + 10; ?>" class="form-control price requered_true validate"  name="pricequote[]">
                        <span class="help-block price_massage hidden" id="price_massage" style="display: none" >You have all ready Registered.</span>
                        <input type="hidden" name="price" id="price" >
                    </div>
                </div>
            </div>
        </div>  
        <div class="row">
            <div class="col-md-4">
                <div class="form-group group_margin">
                    <label class="control-label col-md-4">Stars:</label>
                    <div class="col-md-7 valid_quot site-2<?php echo $countdata;?>">
                        <select tabindex="<?php echo $tabindex + 2; ?>" class="form-control select2me select2-offscreen requered_true input-xmedium validate rating_star_hotel" name="rating[]">
                            <option value="">--Select Star--</option>
                            <option value="2 STAR">2 Stars</option>
                            <option value="2.5 STAR">2.5 Stars</option>
                            <option value="3 STAR">3 Stars</option>
                            <option value="3.5 STAR">3.5 Stars</option>
                            <option value="4 STAR">4 Stars</option>
                            <option value="4.5 STAR">4.5 Stars</option>
                            <option value="5 STAR">5 Stars</option>
                        </select>                                                                            
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group group_margin">
                    <label class="control-label col-md-4">Site #2:</label>
                    <div class="col-md-5">
                        <input type="text" tabindex="<?php echo $tabindex + 6; ?>" class="form-control validate f_site" name="f_site[]">
                    </div>
                    <div class="col-md-3">
                        <input type="text" tabindex="<?php echo $tabindex + 7; ?>" class="form-control validate ssite commanPrice" style="width: 180%; margin-left: -23px" name="s_site[]">
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group group_margin">
                    <label class="control-label col-md-4">TripAdvisor:</label>
                    <div class="col-md-7">
                        <input type="text" tabindex="<?php echo $tabindex + 11; ?>" class="form-control validate hotel_adviser"  name="tripadvisor[]">
                    </div>
                </div>
            </div>
        </div>   
        <div class="row">
            <div class="col-md-4">
                <div class="form-group group_margin">                                                                       
                    <label class="control-label col-md-4">Room Type:</label>
                    <div class="col-md-7 valid_quot col-md-77">
                        <input name="room_type[]" tabindex="<?php echo $tabindex + 3; ?>" class="form-control requered_true input-xmedium validate room_type"  rows="1">
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group group_margin">
                    <label class="control-label col-md-4">SRC</label>
                    <div class="col-md-5 valid_quot">
                        <input type="text" tabindex="<?php echo $tabindex + 8; ?>" class="form-control requered_true validate src" name="src[]">
                    </div>
                    <div class="col-md-3 valid_quot">
                        <input type="text" tabindex="<?php echo $tabindex + 9; ?>" class="form-control requered_true validate src_price ssrc commanPrice" style="width: 180%; margin-left: -23px" data-position="2" name="s_src[]">
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group group_margin">
                    <label class="control-label col-md-4">Comments:</label>
                    <div class="col-md-7">
                        <textarea rows="1" tabindex="<?php echo $tabindex + 12; ?>" class="form-control validate hotel_comments" name="comment[]"></textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group group_margin">                                                                       
                    <label class="control-label col-md-4">Cancellation policy:</label>
                    <div class="col-md-7 valid_quot col-md-77">
                        <textarea name="policy[]" tabindex="<?php echo $tabindex + 3; ?>" class="form-control requered_true input-xmedium validate hotel_cancelation"  rows="1"><?php echo "non-refundable" ?></textarea>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group group_margin">                                                                       
                    <label class="control-label col-md-4">Address:</label>
                    <div class="col-md-7 valid_quot col-md-77">
                        <input tabindex="<?php echo $tabindex + 13; ?>"  type="text" name="address[]" id="address" class="form-control validate address"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>