<?php $i = 1;?>
<div class="tab-pane" id="tab_1">
    <div class="portlet box blue">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-reorder"></i>Add Quotes
            </div>
            <div class="tools">
                <a href="javascript:;" class="collapse">
                </a>
                <a href="#portlet-config" data-toggle="modal" class="config">
                </a>
                <a href="javascript:;" class="reload">
                </a>
                <a href="javascript:;" class="remove">
                </a>
            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form action="#" class="horizontal-form" id="addquotes" name="addquotes">
                <div class="form-body">
<!--                    <h3 class="form-section">Person Info</h3>-->
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">City</label>
                                <input type="text" id="city" name="city" class="form-control" placeholder="city">
                                <input type="hidden" id="getcount" name="getcount" class="form-control" value="1" data-id="1">
                                <span class="help-block">
                                    This is inline help
                                </span>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Points:</label>
                                <input type="text" id="point" name="point" class="form-control" placeholder="point">
                                <span class="help-block">
<!--                                    This is inline help-->
                                </span>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Check in:</label>
                                <input type="text" id="checkin" name="checkin" class="form-control" placeholder="12/12/2014">
                                <span class="help-block">
<!--                                    This is inline help-->
                                </span>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Account ID:</label>
                                <input type="text" id="accountid" name="accountid" class="form-control" placeholder="Account ID">
                                <span class="help-block">
<!--                                    This is inline help-->
                                </span>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Check Out:</label>
                                <input type="text" id="checkout" name="checkout" class="form-control" placeholder="Check Out">
                                <span class="help-block">
<!--                                    This is inline help-->
                                </span>
                            </div>
<!--                            <div class="form-group">
                                <label class="control-label">Category</label>
                                <select class="select2_category form-control" data-placeholder="Choose a Category" tabindex="1">
                                    <option value="Category 1">Category 1</option>
                                    <option value="Category 2">Category 2</option>
                                    <option value="Category 3">Category 5</option>
                                    <option value="Category 4">Category 4</option>
                                </select>
                            </div>-->
                        </div>
                        <!--/span-->
                        <div class="col-md-6">
                           <div class="form-group">
                                <label class="control-label">Username:</label>
                                <input type="text" id="username" name="username" class="form-control" placeholder="username">
                                <span class="help-block">
<!--                                    This is inline help-->
                                </span>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <!--<h3 class="form-section">Address</h3>-->
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Rooms:</label>
                                <input type="text" id="room" name="room" class="form-control" placeholder="room">
                                <span class="help-block">
<!--                                    This is inline help-->
                                </span>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Date of request:</label>
                                <input type="text" id="requestdate" name="requestdate" class="form-control" placeholder="requestdate">
                                <span class="help-block">
<!--                                    This is inline help-->
                                </span>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Adults:</label>
                                <input type="text" id="adult" name="adult" class="form-control" placeholder="adult">
                                <span class="help-block">
<!--                                    This is inline help-->
                                </span>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Children:</label>
                                <input type="text" id="children" name="children" class="form-control" placeholder="Children">
                                <span class="help-block">
<!--                                    This is inline help-->
                                </span>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                     <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Name of reservation:</label>
                                <input type="text" id="reservname" name="reservname" class="form-control" placeholder="Name of reservation">
                                <span class="help-block">
<!--                                    This is inline help-->
                                </span>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                     <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Name of hotel:</label>
                                <input type="text" id="hotelname" name="hotelname" class="form-control" placeholder="Name of hotel">
                                <span class="help-block">
<!--                                    This is inline help-->
                                </span>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                     <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Stars:</label>
                                <input type="text" id="stars" name="stars" class="form-control" placeholder="stars">
                                <span class="help-block">
<!--                                    This is inline help-->
                                </span>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">CommentS:</label>
                                <textarea rows="1" class="form-control" name="comment" id="comment">Comment</textarea>
                                <span class="help-block">
<!--                                    This is inline help-->
                                </span>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                   
                <div class="form-actions right">
                    <button type="button" class="btn default">Cancel</button>
                    <button type="submit" class="btn blue"><i class="fa fa-check"></i> Save</button>
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
</div>
    
 