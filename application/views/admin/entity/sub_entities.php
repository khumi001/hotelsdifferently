<?php
$i = 1;
?>
<div class="row" id="log">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-user"></i><?= $Heading ?>
                </div>
            </div>

            <div class="portlet-body">
                <div id="mainlog">
                    <table class="table table-striped table-bordered table-hover" id="sample_2">
                        <thead>
                        <tr>
                            <th>
                                Account ID
                            </th>
                            <th>
                                Date/Time
                            </th>
                            <th>
                                Full name
                            </th>
                            <th>
                                Email
                            </th>
                            <th>Membership Expiration</th>
                            <?php if (!isSkipLogin()) {?>
                                <th>Actions</th>
                            <?php }?>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($users->result() as $usersData) { ?>
                            <tr id="tr_<?= $usersData->int_glcode ?>">
                                <td><?= $usersData->var_accountid ?></td>
                                <td><?= date(DISPLAY_DATE_FORMAT_FULL, strtotime($usersData->entity_activate_time)) ?></td>
                                <td><?= $usersData->var_fname . ' ' . $usersData->var_lname ?></td>
                                <td><?= $usersData->var_email ?></td>
                                <td><?php if($usersData->isLifeTime) echo "LIFETIME";else echo date(DISPLAY_DATE_FORMAT,strtotime($usersData->endDate));?></td>
                                <?php if (!isSkipLogin()) {?>
                                    <td>
                                        <a data-toggle="modal" data-target="#deleteModal" onClick="setDeleteUid('<?=$usersData->int_glcode?>')" href="javascript:void(0)">Delete</a>
                                    </td>
                                <?php }?>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>

            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
    <div id="deleteModal" class="modal fade" role="dialog" aria-hidden="true">
        <div class="modal-dialog" style="width: 800px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Please enter password</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group furture-form-style">
                        <label>Password:*</label>
                        <input type="password" class="form-control form-style" id="delete_password" >
                    </div>
                </div>
                <div class="modal-footer" >
                    <button type="button" class="btn btn-default" id="close_model" data-dismiss="modal" >Close</button>
                    <button type="button" class="btn btn-default" onClick="deleteAccount()" >Ok</button>
                </div>
            </div>
        </div>
    </div>
    <script>
        var deleteUid;
        var detailUid;
        function setDeleteUid(user_id){
            deleteUid = user_id;
        }
        function deleteAccount(){
            var uid = deleteUid;
            var PASS_TO_DELETE = '<?php echo PASS_TO_DELETE; ?>';
            if($('#delete_password').val() == PASS_TO_DELETE){
                var status = 'R';
                $.ajax({
                    type: "POST",
                    url: "<?=site_url('admin/entities/sub_delete')?>",
                    data: {userId:uid},
                    cache: false,
                    success: function(result){
                        if($.trim(result) == 'success'){
                            $('#tr_'+uid).remove();
                            $('#deleteModal').modal('toggle');
                        }
                    },
                    error: function (request, status, error) {}
                });
            }else{
                alert('Sorry! Password is wrong.');
            }
        }
    </script>