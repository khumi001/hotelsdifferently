<?php
$i=1;
?>
<div class="row" id="log">
<div class="col-md-12 col-sm-12">
    <!-- BEGIN EXAMPLE TABLE PORTLET-->
    <div class="portlet box blue">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-user"></i><?=$Heading?>
            </div>
        </div>

        <div class="portlet-body">
            <div id="mainlog">
                <table class="table table-striped table-bordered table-hover" id="sample_2">
                    <thead>
                    <tr>
                        <th>
                            Date/Time
                        </th>
                        <th>
                            Account #
                        </th>
                        <th>
                            Company Name
                        </th>
                        <th>
                            Site
                        </th>
                        <th>
                            Phone
                        </th>
                        <th>
                            Email
                        </th>
                        <th>
                            Cell
                        </th>
                        <th>
                            Membership Expiration
                        </th>
                        <?php if (!isSkipLogin()) {?>
                            <th>
                                Action
                            </th>
                        <?php }?>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($users->result() as $usersData){?>
                        <tr id="tr_<?=$usersData->int_glcode?>">
                            <td><?=date(DISPLAY_DATE_FORMAT_FULL,strtotime($usersData->dt_created_date))?></td>
                            <td><?=$usersData->var_accountid?></td>
                            <td><?=$usersData->company_name?></td>
                            <td><?=$usersData->website?></td>
                            <td><?=$usersData->phone_number?></td>
                            <!--<td><?/*=$usersData->contact_f_name.' '.$usersData->contact_l_name*/?></td>-->
                            <td><?=$usersData->email?></td>
                            <td><?=$usersData->contact_phone?></td>
                            <td><?php echo date(DISPLAY_DATE_FORMAT, strtotime($usersData->entity_activate_time." + 90 days"));?></td>
                            <?php if (!isSkipLogin()) {?>
                                <td>
                                    <a data-toggle="modal" data-target="#detailModal" onclick="getUserInfo('<?=site_url('admin/entities/info/'.$usersData->int_glcode)?>')" href="javascript:void(0)">Details</a>
                                    <?php if($action == 'true'){?>|
                                        <button type="button" class="btn default btn-xs green" onClick="updateStatusApproved('<?=$usersData->int_glcode?>')">Approve</button> |
                                        <a href="javascript:void(0)" onClick="updateStatusReject('<?=$usersData->int_glcode?>')">Decline</a>
                                    <?php } ?>
                                </td>
                            <?php }?>
                        </tr>
                    <?php }?>
                    </tbody>
                </table>
            </div>

        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

<div id="detailModal" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog" style="width: 800px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">User Detail</h4>
            </div>
            <div class="modal-body" id="model_load_content">

            </div>
            <div class="modal-footer" >
                <button type="button" class="btn btn-default" id="close_model" data-dismiss="modal" >Close</button>
            </div>
        </div>
    </div>
</div>

<div id="deleteApprovalModal" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog" style="width: 800px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Please enter password</h4>
            </div>
            <div class="modal-body">
                <div class="form-group furture-form-style">
                    <label>Password:*</label>
                    <input type="password" class="form-control form-style" id="approval_delete_password" >
                </div>
            </div>
            <div class="modal-footer" >
                <button type="button" class="btn btn-default" id="close_model" data-dismiss="modal" >Close</button>
                <button type="button" class="btn btn-default" onClick="approvalCall()" >Ok</button>
            </div>
        </div>
    </div>
</div>

<div id="deleteDeclineModal" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog" style="width: 800px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Please enter password</h4>
            </div>
            <div class="modal-body">
                <div class="form-group furture-form-style">
                    <label>Password:*</label>
                    <input type="password" class="form-control form-style" id="decline_delete_password" >
                </div>
            </div>
            <div class="modal-footer" >
                <button type="button" class="btn btn-default" id="close_model" data-dismiss="modal" >Close</button>
                <button type="button" class="btn btn-default" onClick="declineCall()" >Ok</button>
            </div>
        </div>
    </div>
</div>

<?php if($action == 'true'){?>


    <script>
        var dataString = '';
        var PASS_TO_DELETE = '<?php echo PASS_TO_DELETE; ?>';
        function updateStatusReject(uid){
            var status = 'R';
            //if(window.confirm('Are you sure you want to Reject this user?')){
            dataString = 'uid='+uid+'&status='+status;
            $('#deleteDeclineModal').modal('show');
            //}
        }

        function declineCall(){
            if($('#decline_delete_password').val() == PASS_TO_DELETE){
                $.ajax({
                    type: "POST",
                    url: "<?=site_url('admin/entities/update')?>",
                    data: dataString,
                    cache: false,
                    success: function(result){
                        if($.trim(result) == 'success'){
                            window.location.reload();
                        }
                    },
                    error: function (request, status, error) {}
                });
            }else{
                alert('Sorry! Password is wrong.');
            }
        }

        function updateStatusApproved(uid){
            var status = 'A';
            dataString = 'uid='+uid+'&status='+status;
            $('#deleteApprovalModal').modal('show');
        }

        function approvalCall(){
            if($('#approval_delete_password').val() == PASS_TO_DELETE){
                $.ajax({
                    type: "POST",
                    url: "<?=site_url('admin/entities/update')?>",
                    data: dataString,
                    cache: false,
                    success: function(result){
                        if($.trim(result) == 'success'){
                            window.location.reload();
                        }
                    },
                    error: function (request, status, error) {}
                });
            }else{
                alert('Sorry! Password is wrong.');
            }
        }


    </script>
<?php }?>
<script>
    function getUserInfo(_url){
        $.ajax({
            type: "GET",
            url: _url,
            data: {},
            cache: false,
            success: function(result){
                $('#model_load_content').html(result);
            }
        });
    }
</script>