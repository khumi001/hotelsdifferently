<div class="main" style="background:url(https://ci4.googleusercontent.com/proxy/leDLIkEWLmtNXzlvN7qPqHHCokXVukgwEQzjcPA3D_W0F0SxOrNFWOdyS2l8z8oDow_YFeSkSe3lId3icvNfruMpDGdZPsIdTOrcDMhbUzo4Um6zoZp3BA=s0-d-e1-ft#<?php echo base_url(); ?>public/emailtemplate/img/body_bg2.jpg);background-repeat:no-repeat;background-size:100% 100%;width:547px;margin:auto">
    <div class="m_3275774443844637159containt-main" style="background:rgba(17,125,200,0.61);color:#fff;width:100%">
        <div class="m_first">
            <div class="left" style="margin:auto;text-align:center;float:none">
                <a class="logo" href="<?php echo base_url(); ?>" style="display:inline-block;margin:10px 0 0 10px" target="_blank">
                    <img src="https://ci5.googleusercontent.com/proxy/PMWMpDCFn9O9akTGiTgjaPQesfCoAcgtCC_hCU6J8FNjVlsgEmASP_PvAXzJvctmLkXTHeKu1aIoymRLnp1O4JjJS38rTtISASwcijtGNN9B1kyQ1TaYEm6t6nc=s0-d-e1-ft#<?php echo base_url(); ?>public/affilliate_theme/img/logo.png" style="width:130px" class="CToWUd">
                </a>
            </div>
            <div class="m_clear" style="clear:both"></div>
        </div>
        <div class="top-containt">
            <h1 style="max-width:350px;margin:20px auto"><img style="display:block;width:100%;height:auto" src="https://ci5.googleusercontent.com/proxy/-bSdtcUnHZxyIlhAcvIsAEsQhAQX1XZ-IvR4qwuLdhJgE0Y3n6jpDuQoyKYEg0izRJpeALZ6v1UueLoZnoYkzb6Wdy6y5bgGrMyo3be2YEwXkoMt7GkmmCoAFZP3Xlq72KHDqNMDCn4=s0-d-e1-ft#<?php echo base_url(); ?>public/affilliate_theme/img/text-slogan-new.png" alt="" class="CToWUd"></h1>
        </div>
        <div class="top-hotel" style="width:90%;margin:40px auto auto auto;background:#0c5fa7;color:#ffffff">
            <div style="padding-bottom:10px;padding-top:10px;width:90%;margin:auto;font-family:'Open Sans',sans-serif;color:#fff!important;font-size:12px;line-height:16px">
                <p style="color:#fff;margin-bottom:10px">Dear
                    <strong style="color:#fff"><?=$data['company_name'];?></strong>,
                </p>
                <p style="color:#fff;margin-bottom:10px">

                    We just wanted to let you know that your enrollment is now fully finalized and you have been added to our database.
                    Please login to <a href="<?php echo base_url(); ?>" style="color:#fff;text-decoration:underline" target="_blank"><u><?php echo DOMAIN_NAME;?></u></a> with the credentials you provided upon signup.
                </p>
                <p>
                    You can decide whether you wish to enroll your employees yourself OR you also have the option to provide your login to your employees who will be able to do it themselves.
                </p>
                <p>
                    <strong style="color:red;">IMPORTANT: </strong>Your 7-digit authentication code to perform any kind of Admin tasks (such as: changing password for your corporate account or managing employee database) is <b><?=$data['user_info']['entity_sec_code'];?></b> . We advise you to <span style="text-decoration:underline">NEVER</span> give out this 7 digit authorization code and store it at a safe place.
                </p>
                <p style="color: #fff;">
                    <strong>You can now start sharing your signup link (only with your employees), which is: <br>
                        <a style="text-decoration:none;color: #fff;" href=" <?php echo base_url("front/entity/entity-sub-signup/".$data['user_info']['var_accountid']); ?>">  <?php echo base_url("front/entity/entity-sub-signup/".$data['user_info']['var_accountid']); ?> </a>
                    </strong>
                </p>
                <p>
                    Any employees that share the same email domain that you used for your entity master account signup will now be able to enroll.
                </p>
                <p>
                    <strong>Example:</strong> If you used <a style="text-decoration:none;color: #fff;" href="master@mycompanyname.com"> master@mycompanyname.com </a> to sign up for a master account, then any employees that has <a style="text-decoration:none;color: #fff;" href="@mycompanyname.com"> @mycompanyname.com </a> domain can sign up for FREE.
                </p>
                <p>
                    We are very much looking forward to a mutually beneficial relationship with you and wish you lots of savings on your bookings!
                </p>
                <p style="color:#fff;margin-bottom:10px">
                    Thank you for choosing <strong>HotelsDifferently.com</strong> where we are <i>Making hotels happen for YOU!</i>
                </p>

                <p style="color:#fff"><i>Sincerely,</i><br>
                    <strong style="color:#fff">
                        <a href="<?php echo base_url(); ?>" style="color:#fff;text-decoration:none" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=<?php echo base_url(); ?>&amp;source=gmail&amp;ust=1491043630695000&amp;usg=AFQjCNGJeZMVLW2F_9PD-Xf6jcoG7wXK-Q">
                            <b><span class="il">HotelsDifferently</span></b>
                            <sup>sm</sup>
                        </a>
                    </strong><br>
                    <i style="font-size:10px">Making
                        <span class="il">hotels</span>
                        happen for YOU!
                        <sup>sm</sup>
                    </i>
                </p>

                <p style="color:#fff;margin-bottom:10px">
                    <strong>PS: This inbox is NOT monitored, please do NOT send us any emails here.</strong>
                </p>

                <div class="yj6qo"></div>
                <div class="clear adL" style="clear:both"></div>
            </div>
            <div class="adL"></div>
        </div>
        <div class="adL"></div>
        <div class="adL" style="text-align:center;padding:10px 0"></div>
        <div class="adL"></div>
        <div style="font-size:11px;color:#fff;padding:5px;font-family:'Open Sans',sans-serif;display:block" class="adL">&nbsp;</div>
        <div class="adL"></div>
    </div>
    <div class="adL"></div>
</div>