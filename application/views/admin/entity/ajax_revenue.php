<?php foreach ($users as $usersData) { ?>
    <tr id="tr_<?= $usersData['id'] ?>">
        <td><?= $usersData['company'] ?></td>
        <td><?= $usersData['email'] ?></td>
        <td><?= $usersData['name'] ?></td>
        <td><?= $usersData['phone'] ?></td>
        <td><?= $usersData['totalSpend'] == '' ? '$0' : '$'.round($usersData['totalSpend'],2); ?></td>
        <td><?= $usersData['refund'] == '' ? '$0' : '$'.round($usersData['refund']); ?></td>
        <td><?= '$'.$usersData['revenue'] ?></td>
    </tr>
<?php } ?>