<?php
$i = 1;
?>
<div class="row" id="log">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-user"></i><?= $Heading ?>
                </div>
            </div>

            <input style="width:12%;float:left;" type="text" id="from" class="date-picker form-control icon-pick date-filter" />
            <input style="width:12%;float:left; margin-left:10px;" type="text" id="to" class="date-picker form-control icon-pick date-filter" />

            <div class="portlet-body">
                <div id="mainlog">
                    <table class="table table-striped table-bordered table-hover" id="sample_2">
                        <thead>
                        <tr>
                            <th>
                                Name of Company
                            </th>
                            <th>
                                Email
                            </th>
                            <th>
                                Name of Contact
                            </th>
                            <th>
                                Cell Phone
                            </th>
                            <th>Amount Spent</th>
                            <th>Refund</th>
                            <th>Revenue</th>
                        </tr>
                        </thead>
                        <tbody id="dataCon">
                        <?php foreach ($users as $usersData) { ?>
                            <tr id="tr_<?= $usersData['id'] ?>">
                                <td><?= $usersData['company'] ?></td>
                                <td><?= $usersData['email'] ?></td>
                                <td><?= $usersData['name'] ?></td>
                                <td><?= $usersData['phone'] ?></td>
                                <td><?= $usersData['totalSpend'] == '' ? '$0' : '$'.round($usersData['totalSpend'],2); ?></td>
                                <td><?= $usersData['refund'] == '' ? '$0' : '$'.round($usersData['refund']); ?></td>
                                <td><?= '$'.$usersData['revenue'] ?></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>

            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $(document).on('change', '#from', function() {
                var from = $(this).val();
                var to = $("#to").val();
                $.ajax({
                    type: 'post',
                    url: baseurl + 'admin/entities/getAjaxRevenue',
                    data: {from:from,to:to},
                    success: function (output) {
                        $('#dataCon').html(output);
                    }
                });
            });

            $(document).on('change', '#to', function() {
                var to = $(this).val();
                var from = $("#from").val();
                $.ajax({
                    type: 'post',
                    url: baseurl + 'admin/entities/getAjaxRevenue',
                    data: {from:from,to:to},
                    success: function (output) {
                        $('#dataCon').html(output);
                    }
                });
            });
        });
    </script>
