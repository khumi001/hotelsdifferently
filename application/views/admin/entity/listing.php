<?php
$i=1;
?>
<div class="row" id="log">
<div class="col-md-12 col-sm-12">
    <!-- BEGIN EXAMPLE TABLE PORTLET-->
    <div class="portlet box blue">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-user"></i><?=$Heading?>
            </div>
        </div>

        <div class="portlet-body">
            <div id="mainlog">
                <table class="table table-striped table-bordered table-hover" id="sample_2">
                    <thead>
                    <tr>
                        <th>
                            Date/Time
                        </th>
                        <th>
                            Account #
                        </th>
                        <th>
                            Company Name
                        </th>
                        <th>
                            Site
                        </th>
                        <th>
                            Phone
                        </th>
                        <th>
                            Email
                        </th>
                        <th>
                            Cell
                        </th>
                        <th>
                            Membership Expiration
                        </th>
                        <?php if (!isSkipLogin()) {?>
                            <th>
                                Action
                            </th>
                        <?php }?>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($users->result() as $usersData){?>
                        <tr id="tr_<?=$usersData->int_glcode?>">
                            <td><?=date(DISPLAY_DATE_FORMAT_FULL,strtotime($usersData->entity_activate_time))?></td>
                            <td><?=$usersData->var_accountid?></td>
                            <td><?=$usersData->company_name?></td>
                            <td><?=$usersData->website?></td>
                            <td><?=$usersData->phone_number?></td>
                            <!--<td><?/*=$usersData->contact_f_name.' '.$usersData->contact_l_name*/?></td>-->
                            <td><?=$usersData->email?></td>
                            <td><?=$usersData->contact_phone?></td>
                            <!--<td><?/*=$usersData->hear_about*/?></td>-->
                            <td><?php if($usersData->isLifeTime) echo "LIFETIME";else echo date(DISPLAY_DATE_FORMAT,strtotime($usersData->endDate))?></td>
                            <?php if (!isSkipLogin()) {?>
                                <td>
                                    <a data-toggle="modal" data-target="#detailModal" onclick="getUserInfo('<?=site_url('admin/entities/info/'.$usersData->int_glcode)?>','<?=$usersData->int_glcode?>')" href="javascript:void(0)">Details</a>
                                    |
                                    <?php /*?><a href="javascript:void(0)" onClick="updateStatusReject('<?=$usersData->int_glcode?>')">Delete</a><?php */?>
                                    <a data-toggle="modal" data-target="#deleteModal" onClick="setDeleteUid('<?=$usersData->int_glcode?>')" href="javascript:void(0)">Delete</a>
                                    <?php if(!$usersData->isLifeTime){ ?>
                                        |
                                        <a data-toggle="modal" data-target="#extendMembership" onClick="setDeleteUid('<?=$usersData->int_glcode?>')" href="javascript:void(0)">Renew</a>
                                    <?php }?>
                                </td>
                            <?php }?>
                        </tr>
                    <?php }?>
                    </tbody>
                </table>
            </div>

        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

<div id="detailModal" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog" style="width: 800px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">User Detail</h4>
            </div>
            <div class="modal-body" id="model_load_content">

            </div>
            <div class="modal-footer" >
                <button type="button" class="btn btn-default" id="close_model" data-dismiss="modal" >Close</button>
                <?php if($edit_detail == 'true'){ ?>
                    <button data-toggle="modal" data-target="#deleteExtendModal" type="button" class="btn btn-default" >Edit</button>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<div id="extendMembership" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog" style="width: 800px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Please enter password</h4>
            </div>
            <div class="modal-body">
                <div class="form-group furture-form-style">
                    <label>Password:*</label>
                    <input type="password" class="form-control form-style" id="deletePassword" >
                </div>
            </div>
            <div class="modal-footer" >
                <button type="button" class="btn btn-default" id="close_model" data-dismiss="modal" >Close</button>
                <button type="button" class="btn btn-default" onClick="extendMembershipCall()" >Ok</button>
            </div>
        </div>
    </div>
</div>

<div id="deleteModal" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog" style="width: 800px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Please enter password</h4>
            </div>
            <div class="modal-body">
                <div class="form-group furture-form-style">
                    <label>Password:*</label>
                    <input type="password" class="form-control form-style" id="delete_password" >
                </div>
            </div>
            <div class="modal-footer" >
                <button type="button" class="btn btn-default" id="close_model" data-dismiss="modal" >Close</button>
                <button type="button" class="btn btn-default" onClick="updateStatusReject()" >Ok</button>
            </div>
        </div>
    </div>
</div>

<div id="deleteExtendModal" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog" style="width: 800px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Please enter password</h4>
            </div>
            <div class="modal-body">
                <div class="form-group furture-form-style">
                    <label>Password:*</label>
                    <input type="password" class="form-control form-style" id="delete_password_extend" >
                </div>
            </div>
            <div class="modal-footer" >
                <button type="button" class="btn btn-default" id="close_model" data-dismiss="modal" >Close</button>
                <?php if($edit_detail == 'true'){ ?>
                    <button type="button" class="btn btn-default" onClick="extendCheck()" >Edit</button>
                <?php } ?>
            </div>
        </div>
    </div>
</div>

<div id="editModal" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog" style="width: 800px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">User Edit</h4>
            </div>
            <div class="modal-body" id="model_load_content_edit">

            </div>
            <div class="modal-footer" >
                <button type="button" class="btn btn-default" id="close_model" data-dismiss="modal" >Close</button>
                <button type="button" class="btn btn-default" onClick="validateForm()" >Update</button>
            </div>
        </div>
    </div>
</div>
<?php if($action == 'true'){?>
    <script>
        var deleteUid;
        var detailUid;
        var pass = '<?php echo PASS_TO_DELETE; ?>'
        function setDeleteUid(user_id){
            deleteUid = user_id;
        }

        function extendMembershipCall(){
            var uid = deleteUid;
            if($('#deletePassword').val() == pass ){
                var status = 'R';
                var dataString = 'extend=true&userId='+uid;
                $.ajax({
                    type: "POST",
                    url: "<?=site_url('admin/entities/extendMembershipCall')?>",
                    data: dataString,
                    cache: false,
                    success: function(result){
                        if($.trim(result) == 'success'){
                            $('#extendMembership').modal('toggle');
                            location.reload();
                        }else{
                            alert($.trim(result));
                        }
                    },
                    error: function (request, status, error) {}
                });
            }else{
                alert('Sorry! Password is wrong.');
            }
        }

        function updateStatusReject(){
            var uid = deleteUid;
            if($('#delete_password').val() == pass){
                var status = 'R';
                var dataString = 'send_email=false&uid='+uid+'&status='+status;
                $.ajax({
                    type: "POST",
                    url: "<?=site_url('admin/entities/update')?>",
                    data: dataString,
                    cache: false,
                    success: function(result){
                        if($.trim(result) == 'success'){
                            $('#tr_'+uid).remove();
                            $('#deleteModal').modal('toggle');
                        }
                    },
                    error: function (request, status, error) {}
                });
            }else{
                alert('Sorry! Password is wrong.');
            }
        }

        function extendCheck(){
            if($('#delete_password_extend').val() == pass){
                $('#deleteExtendModal').modal('toggle');
                editUserInfo();
            }else{
                alert('Sorry! Password is wrong.');
            }
        }
    </script>
<?php }?>
<script>
    function getUserInfo(_url,uid){
        detailUid = uid;
        $.ajax({
            type: "GET",
            url: _url,
            data: {},
            cache: false,
            success: function(result){
                $('#model_load_content').html(result);
            }
        });
    }
    function editUserInfo(){
        var _url = "<?=site_url('admin/entities/edit')?>/"+detailUid;
        $.ajax({
            type: "GET",
            url: _url,
            data: {},
            cache: false,
            success: function(result){
                $('#model_load_content_edit').html(result);
                $('#editModal').modal('toggle');
            }
        });
    }

</script>