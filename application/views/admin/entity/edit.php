 <form method="post" id="entity_signup" action="<?php echo base_url(); ?>admin/entities/update_entity">
<input type="hidden" name="uid" value="<?=$data->user_id?>">
<div class="row">
  <div class="col-md-4 col-sm-2 ">
    <div class="form-group">
      <label>Company Name:</label>
      <input type="text" class="form-control form-style" name="company_name" value="<?=$data->company_name?>">
    </div>
  </div>
  <div class="col-md-4 col-sm-2 ">
    <div class="form-group furture-form-style">
      <label>Company Website:</label>
      <input type="text" class="form-control form-style" name="website" value="<?=$data->website?>">
    </div>
  </div>
  <div class="col-md-4 col-sm-2 ">
    <div class="form-group furture-form-style">
      <label>Company phone:</label>
      <div class="clear-fix"></div>
      <div class="col-md-2 no-padding">
        <input type="text" class="form-control form-style" name="phone_country_code" value="<?=$data->phone_country_code?>">
      </div>
      <div class="col-md-8 no-padding">
        <input type="text" class="form-control form-style" name="phone_number" value="<?=$data->phone_number?>">
      </div>
      <div class="col-md-2 no-padding">
        <input type="text" class="form-control form-style" name="phone_ex" value="<?=$data->phone_ex?>">
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-4 col-sm-2 ">
    <div class="form-group">
      <label>Country:</label>
      <select class="form-control input-medium select2me brder-radius-none" id="country" name="country" onChange="getStates(this.value)">
        <option value="">Select Country...</option>
        <?php foreach($countries as $country){?>
        <option value="<?=$country['id']?>">
        <?=$country['name']?>
        </option>
        <?php }?>
      </select>
      <?php /*?><input type="text" class="form-control form-style" name="country_name" value="<?=$data->country_name?>"><?php */?>
    </div>
  </div>
  <div class="col-md-4 col-sm-2 ">
    <div class="form-group">
      <label>State:</label>
      <select class="form-control input-medium select2me brder-radius- " name="state" id="state">
      </select>
      <?php /*?><input type="text" class="form-control form-style" name="state_name" value="<?=$data->state_name?>"><?php */?>
    </div>
  </div>
  <div class="col-md-4 col-sm-2 ">
    <label>City:</label>
    <input type="text" class="form-control form-style" name="city" value="<?=$data->city?>">
  </div>
</div>
<div class="row">
  <div class="col-md-4 col-sm-2 ">
    <div class="form-group furture-form-style">
      <label>Street:</label>
      <input type="text" class="form-control form-style" name="street" value="<?=$data->street?>" >
    </div>
  </div>
  <div class="col-md-4 col-sm-4 ">
    <div class="form-group furture-form-style">
      <label>Amount of employees</label>
      <select class="form-control input-medium select2me brder-radius-none" id="how_many_people" name="how_many_people">
        <?php foreach($how_many_people_arr as $how_many_people){
            if($how_many_people['value'] == -1)
                continue;

			if($data->company_peoples == $how_many_people['value']){
				$selectedpeo = 'selected="selected"';
			}else{
				$selectedpeo = '';
			}
			?>
        <option <?=$selectedpeo?> value="<?=$how_many_people['value']?>">
        <?=$how_many_people['name']?>
        </option>
        <?php }?>
      </select>
      <?php //$data->company_peoples?>
    </div>
  </div>
  <div class="col-md-4 col-sm-4">
    <div class="form-group furture-form-style">
      <label>Where did you hear about us?</label>
      <select class="form-control input-medium select2me brder-radius-none" name="hear_about" id="hear_about">
        <?php foreach($hear_abouts as $hear_about){ 
					if($data->other_hear == $hear_about){
						$selected = 'selected="selected"';
					}else{
						$selected = '';
					}
                    ?>
        <option <?=$selected?> value="<?=$hear_about?>">
        <?=$hear_about?>
        </option>
        <?php }?>
      </select>
    </div>
  </div>
</div>
<div class="clearfix"></div>
<div class="row">
  <div class="compny-information">
    <h2>Contact Information</h2>
  </div>
</div>
<div class="row">
  <div class="col-md-4 col-sm-4 ">
    <div class="form-group furture-form-style">
      <label>Full Name:</label>
      <input type="text" class="form-control form-style" name="contact_full_name" value="<?=$data->contact_full_name?>" >
    </div>
  </div>
  <div class="col-md-4 col-sm-4 ">
    <div class="form-group furture-form-style">
      <label>Email:</label>
      <input type="text" class="form-control form-style" onKeyUp="extract_domain(this.value)" name="email" value="<?=$data->email?>" >
    </div>
  </div>
  <div class="col-md-4 col-sm-4 cell-phone">
    <label class="phone-lable">Cell phone </label>
    <div class="clear-fix"></div>
    <div class="col-md-3 no-padding">
      <div class="form-group furture-form-style">
        <input type="text" placeholder="Country" class="form-control form-style" name="contact_country" value="<?=$data->contact_country_code?>" >
      </div>
    </div>
    <div class="col-md-9 no-padding">
      <div class="form-group furture-form-style">
        <input type="text" placeholder="Number" class="form-control form-style" name="contact_phone" value="<?=$data->contact_phone?>" >
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-8 col-sm-8 ">
    <div class="form-group furture-form-style">
      <label>Employee email domain</label>
      <div class="doamin-sec">
        <div class="col-md-4" style="padding: 0;"><span>XXXXXXXXXX@</span><span id="email_domain_span">
          <?=$data->email_domain?>
          </span></div>
      </div>
    </div>
  </div>
  <div class="col-md-4 col-sm-4 ">
    <div class="form-group furture-form-style">
      <label>Authorization Code:</label>
      <input type="text" placeholder="Authorization Code" class="form-control form-style" name="entity_sec_code" value="<?=$data->entity_sec_code?>">
    </div>
  </div>
</div>
</form>
<script>
$(document).ready(function(e) {
    $('#country').val(<?=$data->country?>);
	getStates(<?=$data->country?>);
});
function extract_domain(email){
	var domain = '';
	var name = '';
	
	if($.trim(email) != ''){
		name   = email.substring(0, email.lastIndexOf("@"));
		domain = email.substring(email.lastIndexOf("@") +1);
		if(domain != email){
			domain = email.substring(email.lastIndexOf("@") +1);	
		}else{
			domain = '';
		}
	}
	$('#email_domain_span').text(domain);
}
function getStates(countryId){
	$('#state').html('');
	$.getJSON( "<?php echo base_url(); ?>front/entity/states/"+countryId, function( data ) {
		$.each( data, function( key, val ) {
			var selected = '';
			if(val.id == '<?=$data->state?>'){
				selected = 'selected="selected"';
			}
			var str = '<option '+selected+' value="'+val.id+'">'+decodeEntities(val.name)+'</option>';
			$('#state').append(str);
		});
	});
}
function decodeEntities(encodedString) {
	var textArea = document.createElement('textarea');
	textArea.innerHTML = encodedString;
	return textArea.value;
}
function validateForm(){
	$('.has-error').removeClass('has-error');
	var error = false;
	if($.trim($('input[name="company_name"]').val()) == ''){
		$('input[name="company_name"]').parent('.form-group').addClass('has-error');
		error = true;
	}
	if($.trim($('input[name="website"]').val()) == ''){
		$('input[name="website"]').parent('.form-group').addClass('has-error');
		error = true;
	}
	if($.trim($('input[name="phone_country_code"]').val()) == ''){
		$('input[name="phone_country_code"]').parent('.form-group').addClass('has-error');
		error = true;
	}
	
	if($.trim($('input[name="phone_number"]').val()) == ''){
		$('input[name="phone_number"]').parent('.form-group').addClass('has-error');
		error = true;
	}
	
	if($.trim($('input[name="city"]').val()) == ''){
		$('input[name="city"]').parent('.form-group').addClass('has-error');
		error = true;
	}
	if($.trim($('input[name="street"]').val()) == ''){
		$('input[name="street"]').parent('.form-group').addClass('has-error');
		error = true;
	}
	
	if($.trim($('input[name="contact_full_name"]').val()) == ''){
		$('input[name="contact_full_name"]').parent('.form-group').addClass('has-error');
		error = true;
	}
	
	if($.trim($('input[name="email"]').val()) == ''){
		$('input[name="email"]').parent('.form-group').addClass('has-error');
		error = true;
	}else{
		if(!validateEmail($('input[name="email"]').val())){
			$('input[name="email"]').parent('.form-group').addClass('has-error');
			error = true;
		}
	}
	
	if($.trim($('input[name="contact_country"]').val()) == ''){
		$('input[name="contact_country"]').parent('.form-group').addClass('has-error');
		error = true;
	}
	
	if($.trim($('input[name="contact_phone"]').val()) == ''){
		$('input[name="contact_phone"]').parent('.form-group').addClass('has-error');
		error = true;
	}
	if(error == false){
		$('#entity_signup').submit();
	}
}
function validateEmail(emailAddress){
	var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	var valid = regex.test(emailAddress);
	if (!valid) {
		return false;
	} else {
		return true;
	}
}
</script>