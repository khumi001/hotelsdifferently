<?php
$i = 1;
?>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-user"></i>Processed Chargebacks
                </div>

                <div class="actions">
<!--                    <input type="checkbox" class="checkboxes" name="new" id="new"/>New &nbsp;
                    <input type="checkbox" class="checkboxes" name="new" id="new"/>Pending &nbsp;
                    <input type="checkbox" class="checkboxes" name="new" id="new"/>Expired &nbsp;
                    <input type="checkbox" class="checkboxes" name="new" id="new"/>Published  &nbsp;-->
                    
                </div>
            </div>
            <div class="portlet-body">

                <table class="table table-striped table-bordered table-hover" id="sample_2">
                    <thead>
                        <tr>
                             <th>
                               Account ID
                            </th>
                            <th>
                                Email address
                            </th>
                            <th>
                                Full Name
                            </th>
                            <th>
                                Paypal
                            </th>
                            <th>
                                Country
                            </th>
                            <th>
                                Comment
                            </th>
                            <th>
                                Issued by
                            </th>
<!--                           <th>
                                Chargeback
                            </th>-->
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($charge as $row){ ?>
                        <tr>
                        
                            <td>
                                <?php echo $row['var_accountid']; ?>
                            </td>
                            <td>
                                 <?php echo $row['var_email']; ?>
                            </td>
                            <td>
                                 <?php echo $row['var_fname']; ?> <?php echo $row['var_lname']; ?>
                            </td>
                            <td>
                                 <?php echo $row['var_amount']; ?>
                            </td>
                            <td>
                                 <?php echo $row['var_country']; ?> 
                            </td>  
                            <td>
                                 <?php echo $row['var_comment']; ?>
                            </td> 
                            <td>
                                <?php if(chr_user_type == 'A'){
                                    echo "Admin";
                                } else {echo "Affiliate";}?> 
                            </td> 
<!--                             <td>
                                <a class="btn default btn-xs blue" data-toggle="modal" href="#myModal_autocomplete">
                                    <i class="fa fa-edit">Chargeback</i>
                                </a>

                            </td>-->
                        </tr>
                        <?php } ?>     
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
<div id="myModal_autocomplete1" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Delete Event Data</h4>
            </div>
            <div class="modal-body">
                <h3>Are you Sure to Delete ?...</h3>
            </div>
            <div class="modal-footer" >
                <button type="button" class="btn btn-default" data-dismiss="modal" >Close</button>
                <button type="submit" id="btndelete" class="btn red"><i class="fa fa-check"></i>Delete</button>
            </div>
        </div>
    </div>
</div>
  <form method="post" id="add_quoterequests" class="form-horizontal">
                        <div id="myModal_autocomplete" class="modal fade" role="dialog" aria-hidden="true">
                            <div class="modal-dialog">
                                    <div class="modal-content">
                                            <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                    <h4 class="modal-title">Issue Chargeback</h4>
                                            </div>
                                                <div class="portlet-body form">
                                                        <div class="form-body">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-2">Amount:</label>
                                                                        <div class="col-md-8">
                                                                            <input type="text" class="form-control" placeholder="amount">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                
                                                            </div>
                                                            <div class="row">
                                                                 <div class="col-md-12">
                                                                    <div class="form-group">
                                                                           <label class="control-label col-md-2">Comment:</label>
                                                                           <div class="col-md-8">
                                                                                   <textarea rows="3" class="form-control"></textarea>
                                                                           </div>
                                                                   </div>
                                                                 </div>
                                                            </div>
                                                            <!--/row-->
                                                        </div>
                                                    <!-- END FORM-->
                                                </div>
                                            <!--</div>-->
                                        <!--</div>-->
                                            <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    <button type="submit" id="btn" class="btn green"><i class="fa fa-check"></i> Submit</button>
                                            </div>
                                    </div>
                            </div>
                        </div>
                   </form>


