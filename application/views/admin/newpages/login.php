<style type="text/css">
    .form-horizontal .radio > span {
  margin-left: -3px;
  position: relative;
  margin-top: -1.5px;
}
</style>

<?php
   $i=1;
   ?>
<div class="row" id="log">
   <div class="col-md-12 col-sm-12">
      <!-- BEGIN EXAMPLE TABLE PORTLET-->
      <div class="portlet box blue">
         <div class="portlet-title">
            <div class="caption">
               <i class="fa fa-user"></i>ADMIN LOGIN
            </div>
         </div>
         <div class="portlet-body">
            <!-- <form class="form-horizontal" role="form">
               <div class="form-body">
                  <div class="form-group">
                     <label class="col-md-3 control-label"></label>
                     <div class="col-md-9">
                        <div class="radio-list">
                           <label class="radio-inline">
                              <div class="radio" id="uniform-logmember"><span><input name="optionsRadios" id="logmember" value="option1" type="radio"></span></div>
                              IP Addresses  Log-Members 
                           </label>
                           <label class="radio-inline">
                              <div class="radio" id="uniform-logadmin"><span><input name="optionsRadios" id="logadmin" value="option3" type="radio"></span></div>
                              IP Addresses Log-Admins 
                           </label>
                        </div>
                     </div>
                  </div>
               </div>
            </form> -->
            <div id="mainlog">
               <div id="sample_2_wrapper" class="dataTables_wrapper dataTables_extended_wrapper" role="grid">
                  <div class="row">
                     <div class="col-md-8 col-sm-12">
                        <div class="dataTables_paginate paging_bootstrap_extended">
                           <div class="pagination-panel"> Page <a href="#" class="btn btn-sm default prev disabled" title="Prev"><i class="fa fa-angle-left"></i></a><input class="pagination-panel-input form-control input-mini input-inline input-sm" maxlenght="5" style="text-align:center; margin: 0 5px;" type="text"><a href="#" class="btn btn-sm default next" title="Next"><i class="fa fa-angle-right"></i></a> of <span class="pagination-panel-total">43</span></div>
                        </div>
                        <div id="sample_2_length" class="dataTables_length">
                           <label>
                              <span class="seperator">|</span>View 
                              <select size="1" name="sample_2_length" aria-controls="sample_2" class="form-control input-xsmall input-sm">
                                 <option value="20">20</option>
                                 <option value="50">50</option>
                                 <option value="100">100</option>
                                 <option value="150">150</option>
                                 <option value="-1">All</option>
                              </select>
                              records
                           </label>
                        </div>
                        <div class="dataTables_info" id="sample_2_info"><span class="seperator">|</span>Found total 1,271 records</div>
                     </div>
                     <div class="col-md-4 col-sm-12">
                        <div class="dataTables_filter" id="sample_2_filter"><label>Search: <input aria-controls="sample_2" class="form-control input-small input-inline" type="text"></label></div>
                        <div class="table-group-actions pull-right"></div>
                     </div>
                     <div id="sample_2_processing" class="dataTables_processing" style="visibility: hidden;"><img src="http://hlive.matrix-intech.com/public/assets/img/loading-spinner-grey.gif"><span>&nbsp;&nbsp;Loading...</span></div>
                  </div>
                  <div class="table-scrollable">
                     <table class="table table-striped table-bordered table-hover dataTable" id="sample_2" aria-describedby="sample_2_info">
                        <thead>
                           <tr role="row">
                              <th class="sorting_desc" role="columnheader" tabindex="0" aria-controls="sample_2" rowspan="1" colspan="1" aria-label="
                                 Date/Time
                                 : activate to sort column ascending">
                                 Date/Time
                              </th>
                              <th class="sorting" role="columnheader" tabindex="0" aria-controls="sample_2" rowspan="1" colspan="1" aria-label="
                                 Email address
                                 : activate to sort column ascending">
                                 Username
                              </th>
                              <th class="sorting" role="columnheader" tabindex="0" aria-controls="sample_2" rowspan="1" colspan="1" aria-label="
                                 Ip addresses 
                                 : activate to sort column ascending">
                                 Ip addresses 
                              </th>
                              <th class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" aria-label="
                                 Status
                                 ">
                                 Status
                              </th>
                           </tr>
                        </thead>
                        <tbody role="alert" aria-live="polite" aria-relevant="all">
                           <tr class="odd">
                              <td class=" sorting_1">2017-06-23 15:12:29</td>
                              <td class="">Rehan Kausar</td>
                              <td class="">182.186.120.138</td>
                              <td class=""><span style="color:green;">OK</span></td>
                           </tr>
                           <tr class="even">
                              <td class=" sorting_1">2017-06-23 14:18:19</td>
                              <td class="">Khurram Aslam</td>
                              <td class="">70.173.71.163</td>
                              <td class=""><span style="color:green;">OK</span></td>
                           </tr>
                           <tr class="odd">
                              <td class=" sorting_1">2017-06-23 14:03:52</td>
                              <td class="">Szilard</td>
                              <td class="">182.186.120.138</td>
                              <td class=""><span style="color:green;">OK</span></td>
                           </tr>
                           <tr class="even">
                              <td class=" sorting_1">2017-06-23 13:56:04</td>
                              <td class="">Rehan</td>
                              <td class="">182.186.120.138</td>
                              <td class=""><span style="color:green;">OK</span></td>
                           </tr>
                           <tr class="odd">
                              <td class=" sorting_1">2017-06-23 12:48:08</td>
                              <td class="">Admin</td>
                              <td class="">70.173.71.163</td>
                              <td class=""><span style="color:green;">OK</span></td>
                           </tr>
                           <tr class="even">
                              <td class=" sorting_1">2017-06-23 12:47:46</td>
                              <td class="">Ali Raza</td>
                              <td class="">70.173.71.163</td>
                              <td class=""><span style="color:green;">OK</span></td>
                           </tr>
                           <tr class="odd">
                              <td class=" sorting_1">2017-06-22 16:34:19</td>
                              <td class="">Khurram</td>
                              <td class="">70.173.71.163</td>
                              <td class="">Failed</td>
                           </tr>
                           <tr class="even">
                              <td class=" sorting_1">2017-06-22 14:56:43</td>
                              <td class="">Ali</td>
                              <td class="">70.173.71.163</td>
                              <td class="">Failed</td>
                           </tr>
                           <tr class="odd">
                              <td class=" sorting_1">2017-06-22 12:51:57</td>
                              <td class="">Timsmith</td>
                              <td class="">43.245.8.233</td>
                              <td class="">Failed</td>
                           </tr>
                           <tr class="even">
                              <td class=" sorting_1">2017-06-22 11:08:51</td>
                              <td class="">Rehankausar</td>
                              <td class="">70.173.71.163</td>
                              <td class="">Failed</td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
                  <div class="row">
                     <div class="col-md-8 col-sm-12">
                        <div class="dataTables_paginate paging_bootstrap_extended">
                           <div class="pagination-panel"> Page <a href="#" class="btn btn-sm default prev disabled" title="Prev"><i class="fa fa-angle-left"></i></a><input class="pagination-panel-input form-control input-mini input-inline input-sm" maxlenght="5" style="text-align:center; margin: 0 5px;" type="text"><a href="#" class="btn btn-sm default next" title="Next"><i class="fa fa-angle-right"></i></a> of <span class="pagination-panel-total">43</span></div>
                        </div>
                        <div class="dataTables_length">
                           <label>
                              <span class="seperator">|</span>View 
                              <select size="1" name="sample_2_length" aria-controls="sample_2" class="form-control input-xsmall input-sm">
                                 <option value="20">20</option>
                                 <option value="50">50</option>
                                 <option value="100">100</option>
                                 <option value="150">150</option>
                                 <option value="-1">All</option>
                              </select>
                              records
                           </label>
                        </div>
                        <div class="dataTables_info"><span class="seperator">|</span>Found total 1,271 records</div>
                     </div>
                     <div class="col-md-4 col-sm-12"></div>
                     <div class="dataTables_processing" style="visibility: hidden;"><img src="http://hlive.matrix-intech.com/public/assets/img/loading-spinner-grey.gif"><span>&nbsp;&nbsp;Loading...</span></div>
                  </div>
               </div>
            </div>
            <div id="memberlogdiv" style="display: none">
               <div id="sample_3_wrapper" class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper dataTables_extended_wrapper" role="grid">
                  <div class="row">
                     <div class="col-md-8 col-sm-12">
                        <div class="dataTables_paginate paging_bootstrap_extended">
                           <div class="pagination-panel"> Page <a href="#" class="btn btn-sm default prev disabled" title="Prev"><i class="fa fa-angle-left"></i></a><input class="pagination-panel-input form-control input-mini input-inline input-sm" maxlenght="5" style="text-align:center; margin: 0 5px;" type="text"><a href="#" class="btn btn-sm default next" title="Next"><i class="fa fa-angle-right"></i></a> of <span class="pagination-panel-total">41</span></div>
                        </div>
                        <div id="sample_3_length" class="dataTables_length">
                           <label>
                              <span class="seperator">|</span>View 
                              <select size="1" name="sample_3_length" aria-controls="sample_3" class="form-control input-xsmall input-sm">
                                 <option value="20">20</option>
                                 <option value="50">50</option>
                                 <option value="100">100</option>
                                 <option value="150">150</option>
                                 <option value="-1">All</option>
                              </select>
                              records
                           </label>
                        </div>
                        <div class="dataTables_info" id="sample_3_info"><span class="seperator">|</span>Found total 401 records</div>
                     </div>
                     <div class="col-md-4 col-sm-12">
                        <div class="dataTables_filter" id="sample_3_filter"><label>Search: <input aria-controls="sample_3" class="form-control input-small input-inline" type="text"></label></div>
                        <div class="table-group-actions pull-right"></div>
                     </div>
                     <div id="sample_3_processing" class="dataTables_processing" style="visibility: hidden;"><img src="http://hlive.matrix-intech.com/public/assets/img/loading-spinner-grey.gif"><span>&nbsp;&nbsp;Loading...</span></div>
                  </div>
                  <div class="table-scrollable">
                     <table class="table table-striped table-bordered table-hover dataTable" id="sample_3" aria-describedby="sample_3_info">
                        <thead>
                           <tr role="row">
                              <th class="sorting_desc" role="columnheader" tabindex="0" aria-controls="sample_3" rowspan="1" colspan="1" aria-label="
                                 Date/Time
                                 : activate to sort column ascending">
                                 Date/Time
                              </th>
                              <th class="sorting" role="columnheader" tabindex="0" aria-controls="sample_3" rowspan="1" colspan="1" aria-label="
                                 Email address
                                 : activate to sort column ascending">
                                 Email address
                              </th>
                              <th class="sorting" role="columnheader" tabindex="0" aria-controls="sample_3" rowspan="1" colspan="1" aria-label="
                                 Ip address
                                 : activate to sort column ascending">
                                 Ip address
                              </th>
                              <th class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" aria-label="
                                 Status
                                 ">
                                 Status
                              </th>
                           </tr>
                        </thead>
                        <tbody role="alert" aria-live="polite" aria-relevant="all">
                           <tr class="odd">
                              <td class=" sorting_1">2017-06-23 13:56:04</td>
                              <td class="">khuramatrix@gmail.com</td>
                              <td class="">182.186.120.138</td>
                              <td class=""><span style="color:green;">OK</span></td>
                           </tr>
                           <tr class="even">
                              <td class=" sorting_1">2017-06-22 23:32:34</td>
                              <td class="">sfoboy85@gmail.com</td>
                              <td class="">70.173.71.163</td>
                              <td class=""><span style="color:green;">OK</span></td>
                           </tr>
                           <tr class="odd">
                              <td class=" sorting_1">2017-06-22 21:55:42</td>
                              <td class="">sfoboy85@gmail.com</td>
                              <td class="">70.173.71.163</td>
                              <td class=""><span style="color:green;">OK</span></td>
                           </tr>
                           <tr class="even">
                              <td class=" sorting_1">2017-06-22 17:16:17</td>
                              <td class="">khuramatrix@gmail.com</td>
                              <td class="">43.245.9.132</td>
                              <td class=""><span style="color:green;">OK</span></td>
                           </tr>
                           <tr class="odd">
                              <td class=" sorting_1">2017-06-21 12:31:23</td>
                              <td class="">khuramatrix@gmail.com</td>
                              <td class="">182.186.20.173</td>
                              <td class=""><span style="color:green;">OK</span></td>
                           </tr>
                           <tr class="even">
                              <td class=" sorting_1">2017-06-21 12:12:58</td>
                              <td class="">khuramatrix@gmail.com</td>
                              <td class="">182.186.20.173</td>
                              <td class=""><span style="color:green;">OK</span></td>
                           </tr>
                           <tr class="odd">
                              <td class=" sorting_1">2017-06-21 10:44:00</td>
                              <td class="">khuramatrix@gmail.com</td>
                              <td class="">182.186.20.173</td>
                              <td class=""><span style="color:green;">OK</span></td>
                           </tr>
                           <tr class="even">
                              <td class=" sorting_1">2017-06-21 01:44:45</td>
                              <td class="">sfoboy85@gmail.com</td>
                              <td class="">70.173.71.163</td>
                              <td class=""><span style="color:green;">OK</span></td>
                           </tr>
                           <tr class="odd">
                              <td class=" sorting_1">2017-06-20 10:37:10</td>
                              <td class="">khuramatrix@gmail.com</td>
                              <td class="">103.255.4.46</td>
                              <td class=""><span style="color:green;">OK</span></td>
                           </tr>
                           <tr class="even">
                              <td class=" sorting_1">2017-06-19 15:27:17</td>
                              <td class="">sfoboy85@gmail.com</td>
                              <td class="">12.51.221.223</td>
                              <td class=""><span style="color:green;">OK</span></td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
                  <div class="row">
                     <div class="col-md-8 col-sm-12">
                        <div class="dataTables_paginate paging_bootstrap_extended">
                           <div class="pagination-panel"> Page <a href="#" class="btn btn-sm default prev disabled" title="Prev"><i class="fa fa-angle-left"></i></a><input class="pagination-panel-input form-control input-mini input-inline input-sm" maxlenght="5" style="text-align:center; margin: 0 5px;" type="text"><a href="#" class="btn btn-sm default next" title="Next"><i class="fa fa-angle-right"></i></a> of <span class="pagination-panel-total">41</span></div>
                        </div>
                        <div class="dataTables_length">
                           <label>
                              <span class="seperator">|</span>View 
                              <select size="1" name="sample_3_length" aria-controls="sample_3" class="form-control input-xsmall input-sm">
                                 <option value="20">20</option>
                                 <option value="50">50</option>
                                 <option value="100">100</option>
                                 <option value="150">150</option>
                                 <option value="-1">All</option>
                              </select>
                              records
                           </label>
                        </div>
                        <div class="dataTables_info"><span class="seperator">|</span>Found total 401 records</div>
                     </div>
                     <div class="col-md-4 col-sm-12"></div>
                     <div class="dataTables_processing" style="visibility: hidden;"><img src="http://hlive.matrix-intech.com/public/assets/img/loading-spinner-grey.gif"><span>&nbsp;&nbsp;Loading...</span></div>
                  </div>
               </div>
            </div>
            <div id="affilatelogdiv" style="display: none">
               <table class="table table-striped table-bordered table-hover" id="sample_4">
                  <thead>
                     <tr>
                        <th>
                           Date/Time
                        </th>
                        <th>
                           Email address
                        </th>
                        <th>
                           Ip address
                        </th>
                        <th>
                           Status
                        </th>
                     </tr>
                  </thead>
                  <tbody>
                  </tbody>
               </table>
            </div>
            <div id="adminlogdiv" style="display: none">
               <div id="sample_5_wrapper" class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper" role="grid">
                  <div class="row">
                     <div class="col-md-8 col-sm-12">
                        <div class="dataTables_paginate paging_bootstrap_extended">
                           <div class="pagination-panel"> Page <a href="#" class="btn btn-sm default prev disabled" title="Prev"><i class="fa fa-angle-left"></i></a><input class="pagination-panel-input form-control input-mini input-inline input-sm" maxlenght="5" style="text-align:center; margin: 0 5px;" type="text"><a href="#" class="btn btn-sm default next" title="Next"><i class="fa fa-angle-right"></i></a> of <span class="pagination-panel-total">23</span></div>
                        </div>
                        <div id="sample_5_length" class="dataTables_length">
                           <label>
                              <span class="seperator">|</span>View 
                              <select size="1" name="sample_5_length" aria-controls="sample_5" class="form-control input-xsmall input-sm">
                                 <option value="20">20</option>
                                 <option value="50">50</option>
                                 <option value="100">100</option>
                                 <option value="150">150</option>
                                 <option value="-1">All</option>
                              </select>
                              records
                           </label>
                        </div>
                        <div class="dataTables_info" id="sample_5_info"><span class="seperator">|</span>Found total 228 records</div>
                     </div>
                     <div class="col-md-4 col-sm-12">
                        <div class="dataTables_filter" id="sample_5_filter"><label>Search: <input aria-controls="sample_5" class="form-control input-small input-inline" type="text"></label></div>
                        <div class="table-group-actions pull-right"></div>
                     </div>
                     <div id="sample_5_processing" class="dataTables_processing" style="visibility: hidden;"><img src="http://hlive.matrix-intech.com/public/assets/img/loading-spinner-grey.gif"><span>&nbsp;&nbsp;Loading...</span></div>
                  </div>
                  <div class="table-scrollable">
                     <table class="table table-striped table-bordered table-hover dataTable" id="sample_5" aria-describedby="sample_5_info">
                        <thead>
                           <tr role="row">
                              <th class="sorting_desc" role="columnheader" tabindex="0" aria-controls="sample_5" rowspan="1" colspan="1" aria-label="
                                 Date/Time
                                 : activate to sort column ascending">
                                 Date/Time
                              </th>
                              <th class="sorting" role="columnheader" tabindex="0" aria-controls="sample_5" rowspan="1" colspan="1" aria-label="
                                 Email address
                                 : activate to sort column ascending">
                                 Email address
                              </th>
                              <th class="sorting" role="columnheader" tabindex="0" aria-controls="sample_5" rowspan="1" colspan="1" aria-label="
                                 Ip address
                                 : activate to sort column ascending">
                                 Ip address
                              </th>
                              <th class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" aria-label="
                                 Status
                                 ">
                                 Status
                              </th>
                           </tr>
                        </thead>
                        <tbody role="alert" aria-live="polite" aria-relevant="all">
                           <tr class="odd">
                              <td class=" sorting_1">2017-06-23 15:12:29</td>
                              <td class="">Admin@hotelsdifferently.com</td>
                              <td class="">182.186.120.138</td>
                              <td class=""><span style="color:green;">OK</span></td>
                           </tr>
                           <tr class="even">
                              <td class=" sorting_1">2017-06-23 14:18:19</td>
                              <td class="">admin@hotelsdifferently.com</td>
                              <td class="">70.173.71.163</td>
                              <td class=""><span style="color:green;">OK</span></td>
                           </tr>
                           <tr class="odd">
                              <td class=" sorting_1">2017-06-23 14:03:52</td>
                              <td class="">Admin@hotelsdifferently.com</td>
                              <td class="">182.186.120.138</td>
                              <td class=""><span style="color:green;">OK</span></td>
                           </tr>
                           <tr class="even">
                              <td class=" sorting_1">2017-06-23 12:48:08</td>
                              <td class="">admin@hotelsdifferently.com</td>
                              <td class="">70.173.71.163</td>
                              <td class=""><span style="color:green;">OK</span></td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
                  <div class="row">
                     <div class="col-md-8 col-sm-12">
                        <div class="dataTables_paginate paging_bootstrap_extended">
                           <div class="pagination-panel"> Page <a href="#" class="btn btn-sm default prev disabled" title="Prev"><i class="fa fa-angle-left"></i></a><input class="pagination-panel-input form-control input-mini input-inline input-sm" maxlenght="5" style="text-align:center; margin: 0 5px;" type="text"><a href="#" class="btn btn-sm default next" title="Next"><i class="fa fa-angle-right"></i></a> of <span class="pagination-panel-total">23</span></div>
                        </div>
                        <div class="dataTables_length">
                           <label>
                              <span class="seperator">|</span>View 
                              <select size="1" name="sample_5_length" aria-controls="sample_5" class="form-control input-xsmall input-sm">
                                 <option value="20">20</option>
                                 <option value="50">50</option>
                                 <option value="100">100</option>
                                 <option value="150">150</option>
                                 <option value="-1">All</option>
                              </select>
                              records
                           </label>
                        </div>
                        <div class="dataTables_info"><span class="seperator">|</span>Found total 228 records</div>
                     </div>
                     <div class="col-md-4 col-sm-12"></div>
                     <div class="dataTables_processing" style="visibility: hidden;"><img src="http://hlive.matrix-intech.com/public/assets/img/loading-spinner-grey.gif"><span>&nbsp;&nbsp;Loading...</span></div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- END EXAMPLE TABLE PORTLET-->
   </div>
</div>

