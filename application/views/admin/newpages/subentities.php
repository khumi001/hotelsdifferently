<style type="text/css">
	.col-md-5.no-padding{
		padding: 0;
	}
	.col-md-7.no-padding{
		padding: 0;
	}
	.control-label.lbe {
	  font-weight: bold;
	  left: 15px;
	  position: relative;
	  top: 5px;
	}
</style>
<div class="row">
   <div class="col-md-12 col-sm-12">
      <div class="portlet box blue">
         <div class="portlet-title">
            <div class="caption">
               Sub Entities
            </div>
            <div class="actions">
            </div>
         </div>
          <div class="portlet-body">
                <div id="mainlog">
                    <table class="table table-striped table-bordered table-hover" id="sample_2">
                        <thead>
                        <tr>
                            <th>
                                Account ID
                            </th>
                            <th>
                                Date/Time
                            </th>
                            <th>
                                Full name
                            </th>
                            <th>
                                Email
                            </th>
                            <th>Expiration</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                          <tr>
                              <td class=" ">68498974</td>
                              <td class=" ">2017-08-26 09:00:36</td>
                              <td class=" ">Peter Huko</td>
                              <td class=" ">bkkhun@yahoo.com</td>
                              <td class=" ">2018-08-26 09:00:36</td>
                              <td class=" ">
                                  <a data-toggle="modal" data-target="#deleteModal" onclick="setDeleteUid('537')" href="javascript:void(0)">Delete</a>
                              </td>
                          </tr>
                        </tbody>
                    </table>
                </div>
            </div>
      </div>
   </div>
</div>