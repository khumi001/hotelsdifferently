<style type="text/css">
	.col-md-5.no-padding{
		padding: 0;
	}
	.col-md-7.no-padding{
		padding: 0;
	}
	.control-label.lbe {
	  font-weight: bold;
	  left: 15px;
	  position: relative;
	  top: 5px;
	}
  .submit-btn-furture {
  background: rgba(0, 0, 0, 0) linear-gradient(to bottom, #71d1ff 0%, #22b3f4 4%, #198fef 100%) repeat scroll 0 0;
  border: medium none;
  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.4);
  color: #fff;
  font-family: "FuturaStd-Book";
  font-size: 14px;
  margin: 10px 0 0;
  padding: 7px 15px;
  text-shadow: 1px 1px 2px #000;
  text-transform: uppercase;
}
</style>
<div class="row">
   <div class="col-md-12 col-sm-12">
      <div class="portlet box blue">
         <div class="portlet-title">
            <div class="caption">
               USER MARKUP
            </div>
            <div class="actions">
            </div>
         </div>
          <div class="portlet-body">
                <div id="mainlog">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="control-label">TRAVEL PROFESSIONAL + ENTITY: </label>
                        <input class="form-control" name="" placeholder="" type="text">
                      </div> 
                    </div> 
                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="control-label">INDIVIDUAL MEMBER: </label>
                        <input class="form-control"  name="" placeholder="" type="text">
                      </div> 
                    </div> 
                  </div> 
                  <div class="row">
                    <div class="col-md-12 text-center">
                      <button type="button" class="submit-btn-furture">update</button>
                    </div>
                  </div> 
                </div>
            </div>
      </div>
   </div>
</div>