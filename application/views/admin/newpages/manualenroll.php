<style type="text/css">
  .form-enrolment .form-group > label {
    display: block;
    font-size: 15px;
  }
  .form-enrolment .form-style {
    border: 1px solid #ddd;
    display: block;
    height: 37px;
    padding: 10px;
    width: 100%;
  }
  .form-enrolment .expir {
  color: ;
  display: block;
  font-size: 14px;
  letter-spacing: 1px;
  position: relative;
  text-transform: uppercase;
  top: 10px;
}
.form-enrolment .btn-enr {
  display: block;
  margin:0;
  text-align: center;
}
.form-enrolment .btn-enr .endrl {
  background: rgba(0, 0, 0, 0) linear-gradient(to bottom, #71d1ff 0px, #22b3f4 4%, #198fef 100%) repeat scroll 0 0;
  border: medium none;
  border-radius: 6px;
  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.4);
  color: #fff;
  font-family: FuturaStd-Book;
  font-size: 16px;
  margin: 0;
  text-shadow: 1px 1px 0 rgba(0, 0, 0, 0.5);
  width: 100px;
}
.form-enrolment .btn-enr .endrl:hover {
    background: rgba(0, 0, 0, 0) linear-gradient(to bottom, #198fef 0px, #22b3f4 96%, #71d1ff 100%) repeat scroll 0 0;
}
.form-enrolment .modal-header {
    border-bottom: 0px;
}
.nopadding{
  padding: 0;

}
.col-md-2.nopadding.full-name {
  width: 23.4%;
}
.col-md-2.nopadding {
  width: 12.2%;
}
.col-md-2.email {
  width: 22.5%;
}
.col-md-2.type {
  width: 10.5%;
}
.col-md-2.nopadding.expirtion {
  width: 16.5%;
}
.form-enrolment .modal-dialog {
  margin: 30px auto;
  width: 300px;
}
.btn.btn-yes {
  background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
  border: 1px solid #198fef;
  padding: 4px 0;
  width: 65px;
}
.btn.btn-no {
  background: rgba(0, 0, 0, 0) linear-gradient(to bottom, #71d1ff 0px, #22b3f4 4%, #198fef 100%) repeat scroll 0 0;
  border: 1px solid rgba(0, 0, 0, 0) linear-gradient(to bottom, #71d1ff 0px, #22b3f4 4%, #198fef 100%) repeat scroll 0 0;
  padding: 4px 0;
  width: 65px;
   border: 1px solid #198fef;
  color: #fff;
}
.form-enrolment .modal-body {
  padding: 40px;
  position: relative;
}
</style>
<div class="row">
   <div class="col-md-12 col-sm-12">
      <div class="portlet box blue">
         <div class="portlet-title">
            <div class="caption">
               Manual Enrollment 
            </div>
            <div class="actions">
            </div>
         </div>
         <div class="portlet-body data-grid-custom">
            <div id="sample_2_wrapper" class="dataTables_wrapper" role="grid">
               <div class="row">
                  <div class="col-md-6 col-sm-12">
                     <div id="sample_2_length" class="dataTables_length">
                        <label>
                           <div class="select2-container form-control input-xsmall" id="s2id_autogen5">
                              <a href="javascript:void(0)" onclick="return false;" class="select2-choice" tabindex="-1">   <span class="select2-chosen">25</span><abbr class="select2-search-choice-close"></abbr>   <span class="select2-arrow"><b></b></span></a><input class="select2-focusser select2-offscreen" id="s2id_autogen6" type="text">
                              <div class="select2-drop select2-display-none select2-with-searchbox">
                                 <div class="select2-search">       <input autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" class="select2-input" type="text">   </div>
                                 <ul class="select2-results">   </ul>
                              </div>
                           </div>
                           <select size="1" name="sample_2_length" aria-controls="sample_2" class="form-control input-xsmall select2-offscreen" tabindex="-1">
                              <option value="25" selected="selected">25</option>
                              <option value="50">50</option>
                              <option value="100">100</option>
                              <option value="-1">All</option>
                           </select>
                           records
                        </label>
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-12">
                     <div class="dataTables_filter" id="sample_2_filter"><label>Search: <input aria-controls="sample_2" class="form-control input-large input-inline input-small" type="text"></label></div>
                  </div>
               </div>
               <div class="table-scrollable">
                 <table class="table table-striped table-bordered table-hover dataTable" id="sample_2" aria-describedby="sample_2_info">
                    <thead>
                        <tr role="row">
                        	<th class="sorting" role="columnheader" tabindex="0" aria-controls="sample_2" rowspan="1" colspan="1" style="width: 133px;" aria-label="
                                Date/Time
                            : activate to sort column ascending">
                                Full name
                            </th><th class="sorting" role="columnheader" tabindex="0" aria-controls="sample_2" rowspan="1" colspan="1" style="width: 129px;" aria-label="
                                Company Name
                            : activate to sort column ascending">
                                Email
                            </th>
                            <th class="sorting_desc" role="columnheader" tabindex="0" aria-controls="sample_2" rowspan="1" colspan="1" style="width: 42px;" aria-sort="descending" aria-label="
                                Type
                            : activate to sort column ascending">
                                Phone
                            </th>
                            <th class="sorting" role="columnheader" tabindex="0" aria-controls="sample_2" rowspan="1" colspan="1" style="width: 50px;" aria-label="
                                Site
                            : activate to sort column ascending">
                                Type
                            </th>
                            <th class="sorting" role="columnheader" tabindex="0" aria-controls="sample_2" rowspan="1" colspan="1" style="width: 55px;" aria-label="
                                Phone
                            : activate to sort column ascending">
                                Expiration
                            </th>
                            <th class="sorting" role="columnheader" tabindex="0" aria-controls="sample_2" rowspan="1" colspan="1" style="width: 84px;" aria-label="
                                Contact
                            : activate to sort column ascending">
                                Enroll
                            </th>
                          </tr>
                    </thead>
                    
                <!-- <tbody role="alert" aria-live="polite" aria-relevant="all"><tr id="tr_450" class="odd">
                            	<td class=" ">2017-04-26 11:45:18</td>
                                <td class=" ">Rehan kausar</td>
                                <td class=" "> rehankausar@gmail.com</td>
                                <td class=" ">Pakistan</td>
                                <td class=" ">+92355 545 7897</td>
                                <td class=" ">$5454788</td>
                                <td class=" "><a data-toggle="modal" data-target="#detailModal" onclick="getUserInfo('http://hlive.matrix-intech.com/admin/enrollment/info/450')" href="javascript:void(0)">Details</a> | <button type="button" data-id="36" class="btn default btn-xs green" data-toggle="modal" href="#approve-model" onclick="$('#uid').val('450')">Approve</button> | <a href="javascript:void(0)" onclick="updateStatusReject('450')">Decline</a>

                              </td>
                            </tr></tbody> --></table>
               </div>
               <div class="form-enrolment">
                <div class="col-md-2 nopadding full-name">
                  <div class="form-group">
                     <input type="text" name="" class="form-style" />
                  </div>
                </div>  
                <div class="col-md-2 nopadding email">
                  <div class="form-group">
                     <input type="text" name="" class="form-style" />
                  </div>
                </div>  
                <div class="col-md-2 nopadding">
                  <div class="form-group">
                     <input type="text" name="" class="form-style" />
                  </div>
                </div> 
                <div class="col-md-2 nopadding type">
                  <div class="form-group">
                     <select class="form-style">
                        <option>select</option>
                       <option>Travel professional</option>
                       <option>Entity</option>
                       <option>Individual</option>
                     </select>
                  </div>
                </div>   
                <div class="col-md-2 nopadding expirtion">
                  <div class="form-group">
                     <input name="check_in" class="form-control icon-pick check_in text-align-left form-style" id="check_in" type="text">
                  </div>
                </div>  
                <div class="col-md-1">
                  <div class="btn-enr">
                    <button class="btn btn-md endrl" data-toggle="modal" data-target="#Enroll">Enroll</button>
                    <!-- Modal -->
                    <div id="Enroll" class="modal fade" role="dialog">
                      <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-body">
                            <p>Are you sure you want to add this ?</p>
                            <div class="btn-yes-n">
                              <button class="btn btn-yes">Yes</button>
                              <button class="btn btn-no">No</button>
                            </div>
                          </div>
                         <!--  <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          </div> -->
                        </div>

                      </div>
                    </div>
                  </div>
                </div>  
               </div>
               <div class="row">
                  <div class="col-md-5 col-sm-12"></div>
                  <div class="col-md-7 col-sm-12">
                     <div class="dataTables_paginate paging_bootstrap">
                        <ul class="pagination" style="visibility: visible;">
                           <li class="prev disabled"><a href="#" title="Prev"><i class="fa fa-angle-left"></i></a></li>
                           <li class="next disabled"><a href="#" title="Next"><i class="fa fa-angle-right"></i></a></li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Modal -->
            <div id="paynow" class="modal fade" role="dialog">
               <div class="modal-dialog">
                  <!-- Modal content-->
                  <div class="modal-content">
                     <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Modal Header</h4>
                     </div>
                     <div class="modal-body">
                        <form>
                           <h4>Payment Method</h4>
                           <div class="payment">
                              <span class="fa fa-cc-mastercard"></span>
                              <span class="fa fa-cc-visa"></span>
                              <span class="fa fa-cc-amex"></span>
                              <span class="fa fa-cc-jcb"></span>
                              <span class="fa fa-cc-discover"></span>
                           </div>
                           <span>Your information is safe and secure</span>
                           <!--
                              <p>Your credit card will be charged on the following date: <span class="text-primary">12/21/2015</span></p>
                              <div class="form-group">
                              	<label class="control-label col-sm-3">Card Type:</label>
                              	<div  class="col-sm-9">
                              		<select class="select2me form-control input-xlarge" name="data[cardType]" id="" required>
                              			<option class="" value="">Select</option>
                              			<option class="" value="51|true">Visa</option>
                              			<option class="" value="39|true" selected="selected">MasterCard</option>
                              			<option class="" value="31|true">Discover</option>
                              			<option class="" value="125|true">JCB</option>
                              			<option class="" value="11|true">AmericanExpress</option>
                              		</select>
                              	</div>
                              </div>
                              -->
                           <div class="form-group">
                              <label class="control-label col-sm-3">Card Number:</label>
                              <div class="col-sm-9">
                                 <input name="data[cardNumber]" class="form-control input-medium" id="" value="" required="" type="text">
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="control-label col-sm-3">Expiration:</label>
                              <div class="col-sm-9">
                                 <div class="row">
                                    <div class="col-sm-12">
                                       <div class="select2-container select2me form-control input-xlarge" id="s2id_autogen1">
                                          <a href="javascript:void(0)" onclick="return false;" class="select2-choice" tabindex="-1">   <span class="select2-chosen">month</span><abbr class="select2-search-choice-close"></abbr>   <span class="select2-arrow"><b></b></span></a><input class="select2-focusser select2-offscreen" id="s2id_autogen2" type="text">
                                          <div class="select2-drop select2-display-none select2-with-searchbox">
                                             <div class="select2-search">       <input autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" class="select2-input" type="text">   </div>
                                             <ul class="select2-results">   </ul>
                                          </div>
                                       </div>
                                       <select class="select2me form-control input-xlarge select2-offscreen" name="data[month]" id="" required="" tabindex="-1">
                                          <option class="" value="-1" selected="selected">month</option>
                                          <option class="" value="01">January</option>
                                          <option class="" value="02">February</option>
                                          <option class="" value="03">March</option>
                                          <option class="" value="04">April</option>
                                          <option class="" value="05">May</option>
                                          <option class="" value="06">June</option>
                                          <option class="" value="07">July</option>
                                          <option class="" value="08">August</option>
                                          <option class="" value="09">September</option>
                                          <option class="" value="10">October</option>
                                          <option class="" value="11">November</option>
                                          <option class="" value="12">December</option>
                                       </select>
                                    </div>
                                    <div class="col-sm-12">
                                       <div class="select2-container select2me form-control input-xlarge" id="s2id_autogen3">
                                          <a href="javascript:void(0)" onclick="return false;" class="select2-choice" tabindex="-1">   <span class="select2-chosen">year</span><abbr class="select2-search-choice-close"></abbr>   <span class="select2-arrow"><b></b></span></a><input class="select2-focusser select2-offscreen" id="s2id_autogen4" type="text">
                                          <div class="select2-drop select2-display-none select2-with-searchbox">
                                             <div class="select2-search">       <input autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" class="select2-input" type="text">   </div>
                                             <ul class="select2-results">   </ul>
                                          </div>
                                       </div>
                                       <select class="select2me form-control input-xlarge select2-offscreen" name="data[year]" id="" required="" tabindex="-1">
                                          <option class="" value="-1" selected="selected">year</option>
                                          <option class="" value="2015">2015</option>
                                          <option class="" value="2016">2016</option>
                                          <option class="" value="2017">2017</option>
                                          <option class="" value="2018">2018</option>
                                          <option class="" value="2019">2019</option>
                                          <option class="" value="2020">2020</option>
                                          <option class="" value="2021">2021</option>
                                          <option class="" value="2022">2022</option>
                                          <option class="" value="2023">2023</option>
                                          <option class="" value="2024">2024</option>
                                          <option class="" value="2025">2025</option>
                                          <option class="" value="2026">2026</option>
                                          <option class="" value="2027">2027</option>
                                          <option class="" value="2028">2028</option>
                                          <option class="" value="2029">2029</option>
                                       </select>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="control-label col-sm-3">Security Code:</label>
                              <div class="col-sm-9">
                                 <input name="data[security_code]" class="form-control input-medium" id="" value="" required="" type="text">
                              </div>
                           </div>
                           <div class="clearfix mt20">
                              <button type="submit" class="btn btn-primary pull-right">Continue</button>
                           </div>
                        </form>
                     </div>
                     <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                     </div>
                  </div>
               </div>
            </div>
            <!--                <table class="table table-striped table-bordered table-hover" id="sample_2">
               <thead>
                   <tr>
                       <th>
                           Date/Time
                       </th>
                       <th>
                           Username
                       </th>
                       <th>
                           Email address
                       </th>
                       <th>
                           Account ID
                       </th>
                       <th>
                           Confirmation number 
                       </th>
                       <th>
                           Paid amount 
                       </th>
                       <th>
                           View
                       </th>
                   </tr>
               </thead>
               <tbody>
               
               </tbody>
               </table>-->
         </div>
      </div>
   </div>
</div>