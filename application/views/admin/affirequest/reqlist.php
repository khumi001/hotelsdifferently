<div class="row">
     <div class="col-md-12 col-sm-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box blue">
                    <div class="portlet-title">
                            <div class="caption">
                                    <i class="fa fa-user"></i>Affiliate Request
                            </div>                          
                    </div>
                    <div class="portlet-body">
                            <table class="table table-striped table-bordered table-hover" id="sample_2">
                            <thead>
                            <tr>
<!--                                    <th style="width1:8px;">
                                            <input type="checkbox" class="group-checkable" data-set="#sample_2 .checkboxes"/>
                                    </th>-->
                                    <th>
                                             First name
                                    </th>
                                    <th>
                                             Last name
                                    </th>
                                    <th>
                                             Company name
                                    </th>
                                    <th>
                                             Email address  
                                    </th>
                                    <th>
                                             Country
                                    </th>
                                    <th>
                                             Website URL
                                    </th>                                    
                                    <th>
                                             VIEW
                                    </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                                foreach ($affiliate_details as $row){
                            ?>
                            <tr class="odd gradeX">
<!--                                    <td>
                                            <input type="checkbox" class="checkboxes" value="1"/>
                                    </td>-->
                                    <td>
                                             <?php echo $row['var_fname']; ?>
                                    </td>
                                    <td>
                                            <?php echo $row['var_lname']; ?>
                                    </td>                                   
                                      <td>
                                             <?php echo $row['var_company']; ?>
                                    </td>
                                    <td>
                                           <?php echo $row['var_email']; ?>
                                    </td>
                                    <td>
                                           <?php echo $row['var_country']; ?>
                                    </td>
                                    <td>
                                            <?php echo $row['var_websiteurl']; ?>
                                    </td>
                                    <td>
                                            <a href="#myModal_autocomplete" id="btn_affiliatereq" data-id="<?php echo $row['int_glcode']; ?>" data-toggle="modal" class="label label-sm label-success btn_affiliatereq">VIEW</a>
                                    </td>                                    
                            </tr>
                             <?php
                                }
                             ?>
                            </tbody>
                            </table>
                    </div>
                   <form method="post" id="add_quoterequests" class="form-horizontal">
                        <div id="myModal_autocomplete" class="modal fade" role="dialog" aria-hidden="true">
                            <div class="modal-dialog" style="width:70%">
                                <div class="modal-content">
                                            <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                    <h4 class="modal-title">NEW AFFILIATE REQUEST</h4>
                                            </div>
                                                    <div class="portlet-body form">
                                                        <div class="form-body">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-5">First name:</label>
                                                                        <div class="col-md-6">
                                                                            <input type="text" name="firstname" id="firstname" class="form-control" Readonly>
                                                                            <input type="hidden" name="req_id" id="req_id" class="form-control" Readonly>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-5">Last name:</label>
                                                                        <div class="col-md-6">
                                                                            <input type="text" class="form-control" name="lastname" id="lastname"  Readonly>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/row-->
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-5">Email address:</label>
                                                                        <div class="col-md-6">
                                                                            <input type="text" class="form-control" name="email" id="email"  Readonly>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-5">Title:</label>
                                                                        <div class="col-md-6">
                                                                            <input type="text" name="title" id="title" class="form-control"  Readonly>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/row-->
                                                            <!--/row-->
                                                            <!--<br>-->
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-5">Company name:</label>
                                                                        <div class="col-md-6">
                                                                            <input type="text" name="companyname" id="companyname" class="form-control" Readonly>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-5">City:</label>
                                                                        <div class="col-md-6">
                                                                            <input type="text" name="title" id="city" class="form-control"  Readonly>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/row-->
                                                            <!--/row-->
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-5">Country:</label>
                                                                        <div class="col-md-6">
                                                                            <input type="text" name="country" id="country" class="form-control" Readonly>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-5">State:</label>
                                                                        <div class="col-md-6">
                                                                            <input type="text" name="state" id="state" class="form-control"  Readonly>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                               

                                                            </div>
                                                            <!--/row-->
                                                            <!--/row-->
                                                            <div class="row">
                                                                 <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-5">Address1:</label>
                                                                        <div class="col-md-6">
                                                                            <input type="text" name="address1" id="address1" class="form-control"  Readonly>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                              
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-5">Address2:</label>
                                                                        <div class="col-md-6">
                                                                            <input type="text" name="address2" id="address2" class="form-control"  Readonly>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/row-->
                                                            <!--/row-->                                                            
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-5">Phone number</label>
                                                                        <div class="col-md-6">
                                                                            <input type="text"  name="phone" id="phone" class="form-control" Readonly>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                  <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-5">ZIP:</label>
                                                                        <div class="col-md-6">
                                                                            <input type="text" name="zip" id="zip" class="form-control" Readonly>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/row-->
                                                            <!--/row-->                                                                                                                       
                                                             <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-5">Website/Newsletter name:</label>
                                                                        <div class="col-md-6">
                                                                            <input type="text" name="newsletter" id="newsletter" class="form-control"  Readonly>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-5">Website URL:</label>
                                                                        <div class="col-md-6">
                                                                            <input type="text" name="wesiteurl" id="wesiteurl" class="form-control"  Readonly>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                             <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-5">Site description</label>
                                                                        <div class="col-md-6">
                                                                            <input type="text" name="sitedesc" id="sitedesc" class="form-control"  Readonly>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-5">Monthly unique visitors:</label>
                                                                        <div class="col-md-6">
                                                                            <input type="text" name="visitors" id="visitors" class="form-control"  Readonly>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                             <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-5">Preferred payment method</label>
                                                                        <div class="col-md-6">
                                                                            <input type="text" name="paymentmethod" id="paymentmethod" class="form-control"  Readonly>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                           
                                                            <!--/row-->                                                            
                                                        </div>
                                                    <!-- END FORM-->
                                                </div>
                                            <!--</div>-->
                                        <!--</div>-->
                                        <div class="modal-footer" style="text-align: center">
                                            <button type="button" id="reject" name="reject" class="btn btn-danger" >REJECT</button>
                                                    <button type="button" id="approve" class="btn green" style="margin-left:10%;"><i class="fa fa-check"></i> APPROVE</button>
                                            </div>
                                    </div>
                            </div>
                        </div>
                   </form>
     
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>    
</div>