<!-- BEGIN CHART PORTLETS

 END CHART PORTLETS
 BEGIN PIE CHART PORTLET
<?php // print_r($sum);exit; ?>
<style>
    .affiliate_form .col-md-4 {
        width: 30%;
    }
    .affiliate_form .control-label {
        text-align: left;
        font-size: 13px;
    }
#chartdiv {
        width		: 100%;
        height		: 500px;
        font-size	: 11px;
}	

</style>
<style>
    .resetbutton {
  background: none repeat scroll 0 0 #fff;
  border: 2px solid #ccc;
  border-radius: 3px;
  height: 33px;
  padding: 0 10px;
}
#tracking_table_filter label{
    width: 400px !important;
}
#tracking_table_filter label input{
    width: 347px !important;
}
#search_box_custome{
    margin-right: 25%;
}
</style>
<?php
$i = 1;
?>
<div class="row">
   
    <div class="col-md-12 col-sm-9">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-user"></i>Affiliate Statics Data
                </div>
                <div class="actions">
                </div>
            </div>
            <div class="portlet-body">
                <div class="row" style="margin-bottom: 15px ;display: none;">
                    <div class="col-md-3"></div>
                    <div class="col-md-5 search_box_custome" id="search_box_custome" >
                        <div class="form-group">
                            <label class="control-label col-md-1" style="padding-right: 0px;"></label>
                            <div class="col-md-4">
                                <input type="text" id="example_range_from_1" class="form-control form-control-inline date-picker example">
                            </div>
                            <label class="control-label col-md-1" style="padding-right: 0px;"></label>
                            <div class="col-md-4">
                                <input type="text" id="example_range_to_1" class="form-control form-control-inline date-picker example">
                            </div>
                            <div class="col-md-2">
                                <input type="button" class="resetbutton" value="Reset">
                                </div>
                        </div>
                    </div>
                    <div class="col-md-3"></div>
                </div>
                <table class="table table-striped table-bordered table-hover " id="tracking_table">
                    <thead>
                        <tr>
                            <th>
                                 Account ID
                            </th>
                            <th>
                                 Email 
                            </th>
                            <th>
                                Signups
                            </th>
                            <th>
                                 Total sales
                            </th>
                            <th>
                               Paid commission
                            </th>
                            <th>
                                Paid amount 
                            </th>
                            <th>
                                View
                            </th>
                        </tr>
                    </thead>
                    <tbody>


                    </tbody>
                </table>
                <table class="table table-striped table-bordered table-hover" id="sample_2">
                    <thead>
                        <tr>
                            <th>
                                Date/Time
                            </th>
                            <th>
                                Username
                            </th>
                            <th>
                                Email address
                            </th>
                            <th>
                                Account ID
                            </th>
                            <th>
                                Confirmation number 
                            </th>
                            <th>
                                Paid amount 
                            </th>
                            <th>
                                View
                            </th>
                        </tr>
                    </thead>
                    <tbody>
<?php foreach ($conform_res as $row) { ?>
                                    <tr>
                                        <td>
    <?php echo $row['var_createddate']; ?>
                                        </td>
                                        <td>
    <?php echo $row['var_username']; ?>
                                        </td>
                                        <td>
    <?php echo $row['var_email']; ?>
                                        </td>
                                        <td>
    <?php echo $row['var_accountid']; ?>
                                        </td>
                                        <td>        
    <?php echo $row['var_transactionid']; ?>
                                        </td>
                                        <td>
    <?php echo $row['var_paid_amount']; ?>
                                        </td>
                                        <td>
                                            <a href="#myModal_autocomplete" data-toggle="modal" data-id="<?php echo $row['int_fkquote']; ?>" id="conform_list" class="sentquote label label-sm label-success">View</a>
                                        </td>
                                    </tr>
<?php } ?>   
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>





<div class="row">
    
</div>-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-user"></i>Statistics
                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="sample_2">
                    <thead>
                        <tr>
                            <th>
                                Referred by
                            </th>
                            <th>
                               Count
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($about as $row) {
                            
                            if($row['var_abuot_us'] != ""){
                            ?>
                            <tr class="odd gradeX">
                                <td>
                                    <?php echo $row['var_abuot_us']; ?>
                                </td>

                                <td>
                                    <?php echo $row['count']; ?>
                                </td>
                            </tr>
                            <?php }
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>