<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
<div class="page-sidebar navbar-collapse collapse">
<!-- add "navbar-no-scroll" class to disable the scrolling of the sidebar menu -->
<!-- BEGIN SIDEBAR MENU -->
<?php
$blobmenu = $this->session->userdata['valid_adminuser']['menu_details'];
$blobmenuarray = unserialize(urldecode($blobmenu));
//echo '<pre>';print_r($blobmenuarray);exit;
?>
<ul class="page-sidebar-menu" data-auto-scroll="true" data-slide-speed="200">
<li class="sidebar-toggler-wrapper">
    <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
    <div class="sidebar-toggler hidden-phone">
    </div>
    <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
</li>
<li class="nav-item">
<a href="javascript:;" class="nav-link nav-toggle">
    <i class="icon-diamond"></i>
    <span class="title">MEMBERSHIPS </span>
    <span class="arrow"></span>
</a>
<ul class="sub-menu" style="display: none;">
<?php
if ($blobmenuarray['Pending Enrollment'] == 'yes' || $blobmenuarray['Membership Database'] == 'yes')
{
    ?>
    <li class="nav-item">
        <a href="javascript:;" class="nav-link nav-toggle">
            <span class="title">Individual Members</span>
            <span class="arrow"></span>
        </a>
        <ul class="sub-menu" style="display: none;">
            <!--<li class="nav-item ">
                        <a href="<?php /*echo base_url(); */?>admin/pages/newpages" class="nav-link ">
                           <i class="fa fa-thumbs-up"></i>
                           <span class="title">
                              Manual Enrollment
                           </span>
                           <span class="selected">
                           </span>
                        </a>
                     </li>-->

            <?php
            if ($blobmenuarray['Pending Enrollment'] == 'yes' || $blobmenuarray['Pending Enrollment'] == '')
            {
                ?>
                <li class="nav-item">
                    <?php if (isset($this->enrollment_model)) $rusers = $this->enrollment_model->getSubscribedUsers();else $rusers = array(); ?>
                    <a href="<?php echo base_url(); ?>admin/regular_users" class="nav-link ">
                        <i class="fa fa-thumbs-up"></i>
                           <span class="title">
                              <!--Confirmed Reservations-->
                              Pending Enrollment (<?php echo count($rusers);?>)
                           </span>
                           <span class="selected">
                           </span>
                    </a>
                </li>
            <?php }?>

            <?php
            if ($blobmenuarray['Membership Database'] == 'yes' || $blobmenuarray['Membership Database'] == '')
            {
                ?>
                <li class="nav-item ">
                    <a href="<?php echo base_url(); ?>admin/regular_users/memberships" class="nav-link ">
                        <i class="fa fa-thumbs-up"></i>
                           <span class="title">
                              <!--Confirmed Reservations-->
                              Membership Database
                           </span>
                           <span class="selected">
                           </span>
                    </a>
                </li>
            <?php }?>
        </ul>
    </li>
<?php }?>


<?php
if ($blobmenuarray['Entity Revenue'] == 'yes' || $blobmenuarray['Sub Entities'] == 'yes' || $blobmenuarray['Entities Database'] == 'yes' || $blobmenuarray['Entity Pending Enrollment'] == 'yes')
{
    ?>
    <li class="nav-item">
        <a href="javascript:;" class="nav-link nav-toggle">
            <i class="icon-diamond"></i>
            <span class="title">Entities</span>
            <span class="arrow"></span>
        </a>
        <ul class="sub-menu" style="display: none;">
            <?php
            if ($blobmenuarray['Sub Entities'] == 'yes' || $blobmenuarray['Sub Entities'] == '')
            {
                ?>
                <li class="nav-item" <?php echo $conf_res; ?>>
                    <a href="<?php echo base_url(); ?>admin/entities/sub_entities">
                        <i class="fa fa-briefcase"></i>
                      <span class="title">
                        Sub-entities 
                      </span>
                        <span class="selected"></span>
                    </a>
                </li>
            <?php }?>

            <?php
            if ($blobmenuarray['Entity Pending Enrollment'] == 'yes' || $blobmenuarray['Entity Pending Enrollment'] == '')
            {
                ?>
                <li class="nav-item" <?php echo $conf_res; ?>>
                    <?php if (isset($this->entity_model)) {$rusers = $this->entity_model->getEnrollments('AP');$rusers=$rusers->result();}else $rusers = array(); ?>
                    <a href="<?php echo base_url(); ?>admin/entities/pending">
                        <i class="fa fa-briefcase"></i>
                      <span class="title">
                      Pending Enrollment (<?php echo count($rusers);?>)
                      </span>
                        <span class="selected"></span>
                    </a>
                </li>
            <?php }?>

            <?php
            if ($blobmenuarray['Entities Database'] == 'yes' || $blobmenuarray['Entities Database'] == '')
            {
                ?>
                <li class="nav-item" <?=isset($us_listing)?$us_listing:''?>>
                    <a href="<?php echo base_url(); ?>admin/entities">
                        <i class="fa fa-briefcase"></i>
                      <span class="title">
                      Entities Database
                      </span>
                        <span class="selected"></span>
                    </a>
                </li>
            <?php }?>
            <?php
            if ($blobmenuarray['Entity Revenue'] == 'yes' || $blobmenuarray['Entity Revenue'] == '')
            {
                ?>
                <li class="nav-item">
                    <a href="<?php echo base_url(); ?>admin/entities/revenue" class="nav-link nav-toggle">
                        <i class="fa fa-briefcase"></i>
                        <span class="title">Entity Revenue</span>
                    </a>
                </li>
            <?php }?>

        </ul>
    </li>
<?php }?>

<?php
if ($blobmenuarray['TP Revenue'] == 'yes' ||$blobmenuarray['TP Database'] == 'yes' || $blobmenuarray['TP Pending Enrollments'] == 'yes' || $blobmenuarray['Approval Enrollments'] == 'yes')
{
    ?>
    <li class="nav-item">
        <a href="javascript:;" class="nav-link nav-toggle">
            <i class="icon-diamond"></i>
            <span class="title">Travel Professionals</span>
            <span class="arrow"></span>
        </a>
        <ul class="sub-menu" style="display: none;">
            <?php
            if ($blobmenuarray['TP Pending Enrollments'] == 'yes' || $blobmenuarray['TP Pending Enrollments'] == '')
            {
                ?>
                <li class="nav-item" <?php echo $conf_res; ?>>
                    <?php if (isset($this->enrollment_model)) {$rusers = $this->enrollment_model->getEnrollments('AP');$rusers=$rusers->result();}else $rusers = array(); ?>
                    <a href="<?php echo base_url(); ?>admin/enrollment/pending">
                        <i class="fa fa-briefcase"></i>
                          <span class="title">
                          Pending Enrollment (<?php echo count($rusers);?>)
                          </span>
                        <span class="selected"></span>
                    </a>
                </li>
            <?php }?>

            <?php
            if ($blobmenuarray['Approval Enrollments'] == 'yes' || $blobmenuarray['Approval Enrollments'] == '')
            {
                ?>
                <li class="nav-item" <?=isset($us_listing)?$us_listing:''?>>
                    <?php if (isset($this->enrollment_model)) {$rusers = $this->enrollment_model->getEnrollments('P');$rusers=$rusers->result();}else $rusers = array(); ?>
                    <a href="<?php echo base_url(); ?>admin/enrollment">
                        <i class="fa fa-briefcase"></i>
                          <span class="title">
                          Approval Enrollment (<?php echo count($rusers);?>)
                          </span>
                        <span class="selected"></span>
                    </a>
                </li>
            <?php }?>

            <?php
            if ($blobmenuarray['TP Database'] == 'yes' || $blobmenuarray['TP Database'] == '')
            {
                ?>
                <li class="nav-item" <?=isset($us_listing)?$us_listing:''?>>
                    <a href="<?php echo base_url(); ?>admin/enrollment/travel_professionals">
                        <i class="fa fa-briefcase"></i>
                          <span class="title">
                          TP Database
                          </span>
                        <span class="selected"></span>
                    </a>
                </li>
            <?php }?>

            <?php
            if ($blobmenuarray['TP Revenue'] == 'yes' || $blobmenuarray['TP Revenue'] == '')
            {
                ?>
                <li class="nav-item" <?=isset($us_listing)?$us_listing:''?>>
                    <a href="<?php echo base_url(); ?>admin/enrollment/revenue">
                        <i class="fa fa-briefcase"></i>
                          <span class="title">
                          TP Revenue
                          </span>
                        <span class="selected"></span>
                    </a>
                </li>
            <?php }?>

        </ul>
    </li>
<?php }?>
</ul>
</li>

<?php
if ($blobmenuarray['Pending Reservations'] == 'yes' || $blobmenuarray['Processed Cancellations'] == 'yes' || $blobmenuarray['Confirmed Reservations'] == 'yes' || $blobmenuarray['Pending Cancellations'] == 'yes')
{
    ?>
    <li class="nav-item">
        <a href="javascript:;" class="nav-link nav-toggle">
            <i class="icon-diamond"></i>
            <span class="title">HOTELS</span>
            <span class="arrow"></span>
        </a>
        <ul class="sub-menu" style="display: none;">
            <?php
            if ($blobmenuarray['Pending Reservations'] == 'yes' || $blobmenuarray['Pending Reservations'] == '')
            {
                ?>
                <li class="nav-item" <?php echo $conf_res; ?>>
                    <?php $rusers = $this->db->order_by("id", "desc")->where('book_status' , 'Request')->where('pending_status' , 0)/*->or_where('pending_status' , 1)*/->get('book_hotal')->num_rows(); ?>
                    <a href="<?php echo base_url(); ?>admin/pen_res">
                        <i class="fa fa-thumbs-up"></i>
                  <span class="title">
                  Pending Reservations (<?php echo ($rusers);?>)
                  </span>
                  <span class="selected">
                  </span>
                    </a>
                </li>
            <?php }?>

            <?php
            if ($blobmenuarray['Confirmed Reservations'] == 'yes' || $blobmenuarray['Confirmed Reservations'] == '')
            {
                ?>
                <li class="nav-item" <?=isset($us_listing)?$us_listing:''?>>
                    <a href="<?php echo base_url(); ?>admin/conf_res">
                        <i class="fa fa-thumbs-up"></i>
                     <span class="title">
                        <!--Confirmed Reservations-->
                        Confirmed Reservations
                     </span>
                     <span class="selected">
                     </span>
                    </a>
                </li>
            <?php }?>

            <?php
            if ($blobmenuarray['Pending Cancellations'] == 'yes' || $blobmenuarray['Pending Cancellations'] == '')
            {
                ?>
                <li class="nav-item" <?=isset($us_listing)?$us_listing:''?>>
                    <?php $rusers = $this->db->order_by("id", "asc")->where('cancel_status' , 1)->/*where('admin_changed' , 1)->*/get('booking_detail')->num_rows();?>
                    <a href="<?php echo base_url(); ?>admin/pending_cancellation">
                        <i class="fa fa-thumbs-up"></i>
                  <span class="title">
                  Pending Cancellations (<?php echo ($rusers);?>)
                  </span>
                  <span class="selected">
                  </span>
                    </a>
                </li>
            <?php }?>

            <?php
            if ($blobmenuarray['Processed Cancellations'] == 'yes' || $blobmenuarray['Processed Cancellations'] == '')
            {
                ?>
                <li class="nav-item" <?=isset($us_listing)?$us_listing:''?>>
                    <a href="<?php echo base_url(); ?>admin/processedcancel">
                        <i class="fa fa-thumbs-up"></i>
                  <span class="title">
                  Processed Cancellations
                  </span>
                  <span class="selected">
                  </span>
                    </a>
                </li>
            <?php }?>
        </ul>
    </li>
<?php }?>

<?php
if ($blobmenuarray['Activities'] == 'yes' || $blobmenuarray['Activities'] == '')
{
    ?>
    <li class="nav-item">
        <a href="<?php echo base_url(); ?>admin/admin_log" class="nav-link nav-toggle">
            <i class="icon-diamond"></i>
            <span class="title">ACTIVITIES</span>
        </a>
    </li>
<?php }?>

<?php
if ($blobmenuarray['Coupons'] == 'yes' || $blobmenuarray['Coupons'] == '')
{
    ?>
    <li class="nav-item">
        <a href="<?php echo base_url(); ?>admin/coupons" class="nav-link nav-toggle">
            <i class="icon-diamond"></i>
            <span class="title">COUPONS</span>
        </a>
    </li>
<?php }?>

<?php
if ($blobmenuarray['IP Address BAN'] == 'yes' || $blobmenuarray['Authorization'] == 'yes' || $blobmenuarray['Processed IP BAN'] == 'yes' || $blobmenuarray['Profile'] == 'yes' || $blobmenuarray['Master Account'] == 'yes' || $blobmenuarray['User Markup'] == 'yes')
{
    ?>
    <li class="nav-item">
        <a href="javascript:;" class="nav-link nav-toggle">
            <i class="icon-diamond"></i>
            <span class="title">ADMIN TASKS</span>
            <span class="arrow"></span>
        </a>
        <ul class="sub-menu" style="display: none;">
            <?php
            if ($blobmenuarray['IP Address BAN'] == 'yes' || $blobmenuarray['IP Address BAN'] == '')
            {
                ?>
                <li class="nav-item" <?php echo $conf_res; ?>>
                    <a href="<?php echo base_url(); ?>admin/ipaddressban">
                        <i class="fa fa-briefcase"></i>
                  <span class="title">
                  IP Address Ban
                  </span>
                        <span class="selected"></span>
                    </a>
                </li>
            <?php }?>

            <?php
            if ($blobmenuarray['Processed IP BAN'] == 'yes' || $blobmenuarray['Processed IP BAN'] == '')
            {
                ?>
                <li class="nav-item" <?=isset($us_listing)?$us_listing:''?>>
                    <a href="<?php echo base_url(); ?>admin/processipban">
                        <i class="fa fa-briefcase"></i>
                  <span class="title">
                  Processed IP Ban
                  </span>
                        <span class="selected"></span>
                    </a>
                </li>
            <?php }?>
            <!--<li class="nav-item" <?/*=isset($us_listing)?$us_listing:''*/?>>
                  <a href="<?php /*echo base_url(); */?>admin/booking_api/ShowHotelSearchForm">
                  <i class="fa fa-briefcase"></i>
                  <span class="title">
                  Hotel Price Update
                  </span>
                  <span class="selected"></span>
                  </a>
               </li>-->
            <?php
            if ($blobmenuarray['Authorization'] == 'yes' || $blobmenuarray['Authorization'] == '')
            {
                ?>
                <li class="nav-item" <?=isset($us_listing)?$us_listing:''?>>
                    <a href="<?php echo base_url(); ?>admin/authorization">
                        <i class="fa fa-briefcase"></i>
                  <span class="title">
                  Authorization
                  </span>
                        <span class="selected"></span>
                    </a>
                </li>
            <?php }?>

            <?php
            if ($blobmenuarray['Profile'] == 'yes' || $blobmenuarray['Profile'] == '')
            {
                ?>
                <li class="nav-item" <?=isset($us_listing)?$us_listing:''?>>
                    <a href="<?php echo base_url(); ?>admin/profile">
                        <i class="fa fa-briefcase"></i>
                  <span class="title">
                  Profile
                  </span>
                        <span class="selected"></span>
                    </a>
                </li>
            <?php }?>

            <?php
            if ($blobmenuarray['Master Account'] == 'yes' || $blobmenuarray['Master Account'] == '')
            {
                ?>
                <li class="nav-item" <?=isset($us_listing)?$us_listing:''?>>
                    <a href="<?php echo base_url(); ?>admin/masterlogin">
                        <i class="fa fa-briefcase"></i>
                  <span class="title">
                  Master Account
                  </span>
                        <span class="selected"></span>
                    </a>
                </li>
            <?php }?>

            <?php
            if ($blobmenuarray['User Markup'] == 'yes' || $blobmenuarray['User Markup'] == '')
            {
                ?>
                <li class="nav-item">
                    <a href="<?php echo base_url(); ?>admin/markup">
                        <i class="fa fa-briefcase"></i>
                  <span class="title">
                  User Markup
                  </span>
                        <span class="selected"></span>
                    </a>
                </li>
            <?php }?>
        </ul>
    </li>
<?php }?>

<?php
if ($blobmenuarray['Statistics'] == 'yes' || $blobmenuarray['Statistics'] == '')
{
    ?>
    <li class="nav-item">
        <a href="<?php echo base_url(); ?>admin/statistics" class="nav-link nav-toggle">
            <i class="icon-diamond"></i>
            <span class="title">STATISTICS</span>
        </a>
    </li>
<?php }?>
<?php
if ($blobmenuarray['Login Log'] == 'yes' || $blobmenuarray['Admin Activity Log'] == 'yes')
{
    ?>
    <li class="nav-item">
        <a href="javascript:;" class="nav-link nav-toggle">
            <i class="icon-diamond"></i>
            <span class="title">LOGS</span>
            <span class="arrow"></span>
        </a>
        <ul class="sub-menu" style="display: none;">
            <?php
            if ($blobmenuarray['Login Log'] == 'yes' || $blobmenuarray['Login Log'] == '')
            {
                ?>
                <li class="nav-item" <?=isset($us_listing)?$us_listing:''?>>
                    <a href="<?php echo base_url(); ?>admin/admin_log">
                        <i class="fa fa-briefcase"></i>
                  <span class="title">
                  Admin Activity log
                  </span>
                        <span class="selected"></span>
                    </a>
                </li>
            <?php }?>

            <?php
            if ($blobmenuarray['Admin Activity Log'] == 'yes' || $blobmenuarray['Admin Activity Log'] == '')
            {
                ?>
                <li class="nav-item" <?=isset($us_listing)?$us_listing:''?>>
                    <a href="<?php echo base_url(); ?>admin/log">
                        <i class="fa fa-briefcase"></i>
                  <span class="title">
                  Login Log
                  </span>
                        <span class="selected"></span>
                    </a>
                </li>
            <?php }?>
        </ul>
    </li>
<?php }?>
</ul>
<!-- END SIDEBAR MENU -->
</div>
</div>
<!-- END SIDEBAR -->

