<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
    <?php
    $title = empty($var_meta_title) ? "credit_system" : $var_meta_title;
    $description = empty($var_meta_description) ? "credit_system" : $var_meta_description;
    $keywords = empty($var_meta_keyword) ? "credit_system" : $var_meta_keyword;
    ?>

<title><?php echo $title ?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="<?php echo  $description ?>" name="description"/>
<meta content="<?php echo  $keywords ?>" name="keywords"/>

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<!--<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>-->
<link href="<?php echo base_url(); ?>public/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>public/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>public/assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->

<!-- Begin From Controller-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/assets/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/assets/plugins/select2/select2-metronic.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/assets/plugins/bootstrap-toastr/toastr.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/assets/plugins/data-tables/DT_bootstrap.css"/>

<link rel="stylesheet" href="<?php echo base_url(); ?>public/affilliate_theme/plugins/jquery-ui/jquery-ui.css" />

<!-- Fancy Box CSS --> 
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/assets/plugins/fancybox/source/jquery.fancybox.css" />
<!-- Fancy Box CSS --> 
<script src="<?php echo base_url(); ?>public/assets/javascripts/core/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>public/assets/javascripts/core/jquery-ui.min.js"></script>
<div id="header-css">
<?php
    if (!empty($css)) {
        foreach ($css as $value) {
            ?>
            <link rel="stylesheet" href="<?php echo base_url(); ?>public/assets/stylesheets/<?php echo $value; ?>" />
            <?php
        }
    }
    ?>
<?php
    if (!empty($css_plugin)) {
        foreach ($css_plugin as $value_plugin) {
            ?>
            <link rel="stylesheet" href="<?php echo base_url(); ?>public/assets/plugins/<?php echo $value_plugin; ?>" />
            <?php
        }
    }
    ?>     
</div>
<!-- End From Controller -->

<!-- BEGIN THEME STYLES -->
<link href="<?php echo base_url(); ?>public/assets/plugins/gritter/css/jquery.gritter.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>public/assets/stylesheets/style-metronic.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>public/assets/stylesheets/style.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>public/assets/stylesheets/style-responsive.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>public/assets/stylesheets/plugins.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>public/assets/stylesheets/pages/tasks.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>public/assets/stylesheets/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
<link href="<?php echo base_url(); ?>public/assets/stylesheets/print.css" rel="stylesheet" type="text/css" media="print"/>
<link href="<?php echo base_url(); ?>public/assets/stylesheets/custom.css" rel="stylesheet" type="text/css"/>
<!-- END THEME STYLES -->
      <script type="text/javascript">
        var baseurl="<?php echo base_url();?>";
      </script>


<link rel="shortcut icon" href="http://hotels.softech.website/public/affilliate_theme/img/logo.png"/>
</head>
<!-- END HEAD -->