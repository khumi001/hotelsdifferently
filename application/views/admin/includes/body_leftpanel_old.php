<!-- BEGIN SIDEBAR -->
	<div class="page-sidebar-wrapper">
		<div class="page-sidebar navbar-collapse collapse">
			<!-- add "navbar-no-scroll" class to disable the scrolling of the sidebar menu -->
			<!-- BEGIN SIDEBAR MENU -->
			<ul class="page-sidebar-menu" data-auto-scroll="true" data-slide-speed="200">
				<li class="sidebar-toggler-wrapper">
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
					<div class="sidebar-toggler hidden-phone">
					</div>
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
				</li>
<!--				<li class="sidebar-search-wrapper">
					 BEGIN RESPONSIVE QUICK SEARCH FORM 
					<form class="sidebar-search" action="extra_search.html" method="POST">
						<div class="form-container">
							<div class="input-box">
								<a href="javascript:;" class="remove">
								</a>
								<input type="text" placeholder="Search..."/>
								<input type="button" class="submit" value=" "/>
							</div>
						</div>
					</form>
					 END RESPONSIVE QUICK SEARCH FORM 
				</li>-->
				<li <?php echo $dashborad; ?>>
					<a href="<?php echo base_url(); ?>admin/dashboard">
						<i class="fa fa-home"></i>
						<span class="title">
							Dashboard
						</span>
						<span class="selected">
						</span>
					</a>
				</li>
                                <li <?php echo $about; ?>>
                                        <a href="<?php echo base_url(); ?>admin/about">
						<i class="fa fa-inbox"></i>
						<span class="title">
							About
						</span>
						<span class="selected">
						</span>
					</a>
				</li>
                                <li <?php echo $portfolio; ?>>
                                        <a href="<?php echo base_url(); ?>admin/portfolio">
						<i class="fa fa-inbox"></i>
						<span class="title">
							Portfolio
						</span>
						<span class="selected">
						</span>
					</a>
				</li>
                                 <li <?php echo $contact; ?>>
                                        <a href="<?php echo base_url(); ?>admin/contact">
						<i class="fa fa-inbox"></i>
						<span class="title">
							Contact
						</span>
						<span class="selected">
						</span>
					</a>
				</li>
<!--                                <li <?php echo $home; ?>>
                                        <a href="<?php echo base_url(); ?>admin/home">
						<i class="fa fa-inbox"></i>
						<span class="title">
							Home
						</span>
						<span class="selected">
						</span>
					</a>
				</li>
                                <li <?php echo $about; ?>>
                                        <a href="<?php echo base_url(); ?>admin/about">
						<i class="fa fa-inbox"></i>
						<span class="title">
							About
						</span>
						<span class="selected">
						</span>
					</a>
				</li>
                                
                                
                                
                                <li <?php echo $service; ?>>
					<a href="javascript:;">
						<i class="fa fa-table"></i>
						<span class="title">
							Services
						</span>
						<span class="selected">
						</span>
						<span class="arrow open">
						</span>
					</a>
					<ul class="sub-menu">
						<li <?php echo $services; ?>>
							<a href="<?php echo base_url(); ?>admin/service">
								 Service
							</a>
						</li>
                                                <li <?php echo $web_design_programing; ?>>
							<a href="<?php echo base_url(); ?>admin/web_design_programing">
								 Web Designing & Programming
							</a>
						</li>
                                                <li <?php echo $ecommerce_development; ?>>
							<a href="<?php echo base_url(); ?>admin/e_commerce_development">
								E-Commerce Development
							</a>
						</li>
                                                <li <?php echo $social_media_integration; ?>>
							<a href="<?php echo base_url(); ?>admin/social_media_integration">
								
                                                             Social Media Integration
							</a>
						</li>
                                                <li <?php echo $cms_development_left; ?>>
							<a href="<?php echo base_url(); ?>admin/cms_development">
								
                                                             CMS Development
							</a>
						</li>
                                                 <li <?php echo $api_development; ?>>
							<a href="<?php echo base_url(); ?>admin/api_development">
								
                                                             API Development
							</a>
						</li>
                                                <li <?php echo $mobile_development; ?>>
							<a href="<?php echo base_url(); ?>admin/mobile_development">
								
                                                             Mobile Development
							</a>
						</li>
                                                
					</ul>
				</li>
				
                                <li <?php echo $inquiry; ?>>
                                        <a href="<?php echo base_url(); ?>admin/inquiry">
						<i class="fa fa-inbox"></i>
						<span class="title">
							Inquiry
						</span>
						<span class="selected">
						</span>
					</a>
				</li>
                                <li <?php echo $request_quote; ?>>
                                        <a href="<?php echo base_url(); ?>admin/request_quote">
						<i class="fa fa-inbox"></i>
						<span class="title">
							Request Quote
						</span>
						<span class="selected">
						</span>
					</a>
				</li>
                                <li <?php echo $career; ?>>
                                        <a href="<?php echo base_url(); ?>admin/career">
						<i class="fa fa-inbox"></i>
						<span class="title">
							Career
						</span>
						<span class="selected">
						</span>
					</a>
				</li>
                                 <li <?php echo $job_request; ?>>
                                        <a href="<?php echo base_url(); ?>admin/job_request">
						<i class="fa fa-inbox"></i>
						<span class="title">
							Job Request
						</span>
						<span class="selected">
						</span>
					</a>
				</li>
                                <li <?php echo $setting; ?>>
					<a href="javascript:;">
						<i class="fa fa-table"></i>
						<span class="title">
							Setting
						</span>
						<span class="selected">
						</span>
						<span class="arrow open">
						</span>
					</a>
					<ul class="sub-menu">
						<li <?php echo $general_setting; ?>>
							<a href="<?php echo base_url(); ?>admin/general_setting">
								 General Setting
							</a>
						</li>
                                                <li <?php echo $account_setting; ?>>
							<a href="<?php echo base_url(); ?>admin/account_setting">
								 Account Setting
							</a>
						</li>
                                              
					</ul>
				</li>
                               -->
                                
			</ul>
			<!-- END SIDEBAR MENU -->
		</div>
	</div>
	<!-- END SIDEBAR -->