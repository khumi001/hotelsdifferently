<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <!-- add "navbar-no-scroll" class to disable the scrolling of the sidebar menu -->
        <!-- BEGIN SIDEBAR MENU -->

        <?php
        $blobmenu = $this->session->userdata['valid_adminuser']['menu_details'];
        $blobmenuarray = unserialize(urldecode($blobmenu));

        ?>



        <ul class="page-sidebar-menu" data-auto-scroll="true" data-slide-speed="200">
            <li class="sidebar-toggler-wrapper">
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                <div class="sidebar-toggler hidden-phone">
                </div>
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
            </li>
            <li class="nav-item bold-txt" <?=isset($us_listing)?$us_listing:''?>>
                <a href="javascript:void();">
                    <span class="title ">
                         MEMBERSHIPS
                    </span>
                </a>
            </li>
            <li class="nav-item">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-diamond"></i>
                    <span class="title">Individual Members</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu" style="display: none;">

                    <li class="nav-item" <?php echo $conf_res; ?>>
                        <a href="<?php echo base_url(); ?>admin/pages/newpages">
                            <i class="fa fa-thumbs-up"></i>
                            <span class="title">
                                <!--Confirmed Reservations-->
                                Manual Enrollment
                            </span>
                            <span class="selected">
                            </span>
                        </a>
                    </li>
                    <li class="nav-item" <?=isset($us_listing)?$us_listing:''?>>
                        <a href="<?php echo base_url(); ?>admin/regular_users">
                            <i class="fa fa-thumbs-up"></i>
                            <span class="title">
                                <!--Confirmed Reservations-->
                                Pending Enrollment
                            </span>
                            <span class="selected">
                            </span>
                        </a>
                    </li>
                    <li class="nav-item" <?php echo $conf_res; ?>>
                        <a href="<?php echo base_url(); ?>admin/pages/revenue">
                            <i class="fa fa-thumbs-up"></i>
                            <span class="title">
                                <!--Confirmed Reservations-->
                                Revenue
                            </span>
                            <span class="selected">
                            </span>
                        </a>
                    </li>
                    <li class="nav-item" <?=isset($us_membership)?$us_membership:''?> >
                        <a href="<?php echo base_url(); ?>admin/regular_users/memberships">
                            <i class="fa fa-thumbs-up"></i>
                            <span class="title">
                                <!--Confirmed Reservations-->
                                Membership Database
                            </span>
                            <span class="selected">
                            </span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-diamond"></i>
                    <span class="title">Entities</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu" style="display: none;">

                    <li class="nav-item" <?php echo $conf_res; ?>>
                        <a href="<?php echo base_url(); ?>admin/entities/pending">
                            <i class="fa fa-briefcase"></i>
                            <span class="title">
                                Pending Enrollment (0)
                            </span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <li class="nav-item" <?=isset($us_listing)?$us_listing:''?>>
                        <a href="<?php echo base_url(); ?>admin/entities">
                            <i class="fa fa-briefcase"></i>
                            <span class="title">
                                Entities Database
                            </span>
                            <span class="selected"></span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-diamond"></i>
                    <span class="title">Travel Professionals</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu" style="display: none;">

                    <li class="nav-item" <?php echo $conf_res; ?>>
                        <a href="<?php echo base_url(); ?>admin/enrollment/pending">
                            <i class="fa fa-briefcase"></i>
                            <span class="title">
                                Pending Enrollment (0)
                            </span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <li class="nav-item" <?=isset($us_listing)?$us_listing:''?>>
                        <a href="<?php echo base_url(); ?>admin/enrollment">
                            <i class="fa fa-briefcase"></i>
                            <span class="title">
                                Approval Enrollment (0)
                            </span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <li class="nav-item" <?=isset($us_listing)?$us_listing:''?>>
                        <a href="<?php echo base_url(); ?>admin/enrollment/travel_professionals">
                            <i class="fa fa-briefcase"></i>
                            <span class="title">
                                TP Database
                            </span>
                            <span class="selected"></span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-diamond"></i>
                    <span class="title">HOTELS</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu" style="display: none;">

                    <li class="nav-item" <?php echo $conf_res; ?>>
                        <a href="<?php echo base_url(); ?>admin/pen_res">
                            <i class="fa fa-thumbs-up"></i>
                            <span class="title">
                                Pending Reservations (0)
                            </span>
                            <span class="selected">
                            </span>
                        </a>
                    </li>
                    <li class="nav-item" <?=isset($us_listing)?$us_listing:''?>>
                        <a href="<?php echo base_url(); ?>admin/conf_res">
                            <i class="fa fa-thumbs-up"></i>
                            <span class="title">
                                <!--Confirmed Reservations-->
                                Confirmed Reservations
                            </span>
                            <span class="selected">
                            </span>
                        </a>
                    </li>
                    <li class="nav-item" <?=isset($us_listing)?$us_listing:''?>>
                        <a href="<?php echo base_url(); ?>admin/pending_cancellation">
                            <i class="fa fa-thumbs-up"></i>
                            <span class="title">
                                Pending Cancellations (0)
                            </span>
                            <span class="selected">
                            </span>
                        </a>
                    </li>
                     <li class="nav-item" <?=isset($us_listing)?$us_listing:''?>>
                        <a href="<?php echo base_url(); ?>admin/processedcancel">
                            <i class="fa fa-thumbs-up"></i>
                            <span class="title">
                                Processed Cancellations
                            </span>
                            <span class="selected">
                            </span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item">
                <a href="<?php echo base_url(); ?>admin/admin_log" class="nav-link nav-toggle">
                    <i class="icon-diamond"></i>
                    <span class="title">ACTIVITIES</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="<?php echo base_url(); ?>admin/coupons" class="nav-link nav-toggle">
                    <i class="icon-diamond"></i>
                    <span class="title">COUPONS</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-diamond"></i>
                    <span class="title">ADMIN TASKS</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu" style="display: none;">

                    <li class="nav-item" <?php echo $conf_res; ?>>
                        <a href="<?php echo base_url(); ?>admin/ipaddressban">
                            <i class="fa fa-briefcase"></i>
                            <span class="title">
                               IP Address Ban
                            </span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <li class="nav-item" <?=isset($us_listing)?$us_listing:''?>>
                        <a href="<?php echo base_url(); ?>admin/processipban">
                            <i class="fa fa-briefcase"></i>
                            <span class="title">
                                Processed IP Ban
                            </span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <li class="nav-item" <?=isset($us_listing)?$us_listing:''?>>
                        <a href="<?php echo base_url(); ?>admin/booking_api/ShowHotelSearchForm">
                            <i class="fa fa-briefcase"></i>
                            <span class="title">
                                Hotel Price Update
                            </span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <li class="nav-item" <?=isset($us_listing)?$us_listing:''?>>
                        <a href="<?php echo base_url(); ?>admin/authorization">
                            <i class="fa fa-briefcase"></i>
                            <span class="title">
                                Authorization
                            </span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <li class="nav-item" <?=isset($us_listing)?$us_listing:''?>>
                        <a href="<?php echo base_url(); ?>admin/profile">
                            <i class="fa fa-briefcase"></i>
                            <span class="title">
                                Profile
                            </span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <li class="nav-item" <?=isset($us_listing)?$us_listing:''?>>
                        <a href="<?php echo base_url(); ?>admin/masterlogin">
                            <i class="fa fa-briefcase"></i>
                            <span class="title">
                                Master Account
                            </span>
                            <span class="selected"></span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item">
                <a href="<?php echo base_url(); ?>admin/statistics" class="nav-link nav-toggle">
                    <i class="icon-diamond"></i>
                    <span class="title">STATISTICS</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="<?php echo base_url(); ?>admin/pages/revenue" class="nav-link nav-toggle">
                    <i class="icon-diamond"></i>
                    <span class="title">REVENUE</span>
                </a>
            </li>
             <li class="nav-item">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-diamond"></i>
                    <span class="title">LOGS</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu" style="display: none;">

                    <li class="nav-item" <?php echo $conf_res; ?>>
                        <a href="<?php echo base_url(); ?>admin/pages/login">
                            <i class="fa fa-briefcase"></i>
                            <span class="title">
                               Admin login
                            </span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <li class="nav-item" <?=isset($us_listing)?$us_listing:''?>>
                        <a href="<?php echo base_url(); ?>admin/admin_log">
                            <i class="fa fa-briefcase"></i>
                            <span class="title">
                                Admin Activity log
                            </span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <li class="nav-item" <?=isset($us_listing)?$us_listing:''?>>
                        <a href="<?php echo base_url(); ?>admin/log">
                            <i class="fa fa-briefcase"></i>
                            <span class="title">
                                Login Log
                            </span>
                            <span class="selected"></span>
                        </a>
                    </li>
                </ul>
            </li>
    </ul>
    <!-- END SIDEBAR MENU -->
</div>
</div>
<!-- END SIDEBAR -->