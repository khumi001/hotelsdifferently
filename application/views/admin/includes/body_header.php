<?php
$profile_content = $this->profile_model->get_profile();
//print_r($profile_content);exit;
if($profile_content[0]['profile_pic'] == '')
{
    $profile_content[0]['profile_pic'] = 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image';
}
else
{
    $profile_content[0]['profile_pic'] = base_url().$profile_content[0]['profile_pic'];
}
?>

<!-- BEGIN HEADER -->
<div class="header navbar navbar-fixed-top">
	<!-- BEGIN TOP NAVIGATION BAR -->
	<div class="header-inner">
		<!-- BEGIN LOGO -->
		<a class="navbar-brand" href="<?php echo base_url(); ?>admin/pen_res">
                        <img src="http://hotels.softech.website/public/affilliate_theme/img/logo.png" alt="Dealz" class="img-responsive"/>
		</a>
		<!-- END LOGO -->
		<!-- BEGIN RESPONSIVE MENU TOGGLER -->
		<a href="javascript:;" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
			<img src="<?php echo base_url(); ?>public/assets/img/menu-toggler.png" alt=""/>
		</a>
		<!-- END RESPONSIVE MENU TOGGLER -->
		<!-- BEGIN TOP NAVIGATION MENU -->
		<ul class="nav navbar-nav pull-right">
			<!-- BEGIN USER LOGIN DROPDOWN -->
			<li class="dropdown user">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <img alt="" src="<?php echo $profile_content[0]['profile_pic']?>" style="width: 40px;height: 40px;" />
					<span class="username">
						 <?php echo $profile_content[0]['var_fname']." ".$profile_content[0]['var_lname'] ?>
					</span>
					<i class="fa fa-angle-down"></i>
				</a>
				<ul class="dropdown-menu">
					<li>
                                            <a href="<?php echo base_url(); ?>admin/profile">
							<i class="fa fa-user"></i> My Profile
						</a>
					</li>
					<li>
						<a href="javascript:;" id="trigger_fullscreen">
							<i class="fa fa-arrows"></i> Full Screen
						</a>
					</li>
<!--					<li>
						<a href="extra_lock.html">
							<i class="fa fa-lock"></i> Lock Screen
						</a>
					</li>-->
					<li>
                                            <a href="<?php echo base_url(); ?>admin/account/logout" refresh="1">
							<i class="fa fa-key"></i> Log Out
						</a>
					</li>
				</ul>
			</li>
			<!-- END USER LOGIN DROPDOWN -->
		</ul>
		<!-- END TOP NAVIGATION MENU -->
	</div>
	<!-- END TOP NAVIGATION BAR -->
</div>
<!-- END HEADER -->