<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="assets/plugins/respond.min.js"></script>
<script src="assets/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="<?php echo base_url(); ?>public/assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>public/assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>

<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="<?php echo base_url(); ?>public/assets/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>public/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>public/assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>public/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>public/assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>public/assets/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>public/assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<!--<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>-->
<!--<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/plugins/jquery-validation/dist/additional-methods.min.js"></script>-->
<script type='text/javascript' src='<?php echo base_url(); ?>public/assets/javascripts/core/jquery.validate.min.js'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>public/assets/javascripts/core/additional-methods.min.js'></script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/plugins/select2/select2.min.js"></script>
<script src="<?php echo base_url(); ?>public/assets/plugins/jquery-ui/jquery-ui.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/plugins/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/plugins/data-tables/DT_bootstrap.js"></script>

<!-- Fancy Box JS --> 
<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/plugins/fancybox/source/jquery.fancybox.pack.js"></script>
<!-- Fancy Box JS --> 

<div id="footer-jsplugin">
    <?php
    if (!empty($amchart)) {
        foreach ($amchart as $value) {
            ?>
            <script src="<?php echo $value ?>" type="text/javascript"></script>
            <?php
        }
    }
    ?>
 <?php
    if (!empty($js_plugin)) {
        foreach ($js_plugin as $value) {
            ?>
            <script src="<?php echo base_url(); ?>public/assets/plugins/<?php echo $value ?>" type="text/javascript"></script>
            <?php
        }
    }
    ?>
</div>        
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/plugins/bootstrap-toastr/toastr.min.js"></script>
<script src="<?php echo base_url(); ?>public/assets/plugins/gritter/js/jquery.gritter.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>public/assets/javascripts/core/app.js" type="text/javascript"></script>
<script id="remove_js" src="<?php echo base_url(); ?>public/assets/javascripts/core/datatable_apply.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>public/assets/javascripts/common_function.js" type="text/javascript"></script>

 <div id="footer-js">
 <?php
    if (!empty($js)) {
        foreach ($js as $value) {
            ?>
            <script src="<?php echo base_url(); ?>public/assets/javascripts/<?php echo $value ?>" type="text/javascript"></script>
            <?php
        }
    }
    ?>
            
 </div>
<!-- END PAGE LEVEL SCRIPTS -->
<div id="footer-init">
<script>
jQuery(document).ready(function() {    
   <?php
    if($this->input->is_ajax_request()){
    ?>
    App.initAjax();
    <?php    
    }else{
    ?>
    App.init();
    <?php    
    }
    ?>
 <?php
    if (!empty($init)) {
        foreach ($init as $value) {
            echo $value.';' ;
        }
    }
    ?>   
            
});

</script>
</div>