<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <!-- add "navbar-no-scroll" class to disable the scrolling of the sidebar menu -->
        <!-- BEGIN SIDEBAR MENU -->

        <?php
        $blobmenu = $this->session->userdata['valid_adminuser']['menu_details'];
        $blobmenuarray = unserialize(urldecode($blobmenu));
		
        ?>



        <ul class="page-sidebar-menu" data-auto-scroll="true" data-slide-speed="200">
            <li class="sidebar-toggler-wrapper">
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                <div class="sidebar-toggler hidden-phone">
                </div>
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
            </li>
<?php
//if ($blobmenuarray['Quote Requests'] == 'yes' || $blobmenuarray['Quote Requests'] == '') {
    ?>
                <!--<li <?php //echo $quoterequests; ?>>
                    <a href="<?php //echo base_url(); ?>admin/quoterequests/">
                        <i class="fa fa-home"></i>
                        <span class="title">
                            Quote Requests
                        </span>
                        <span class="selected">
                        </span>
                    </a>
                </li>-->
    <?php
//}
?>

            <?php
            //if ($blobmenuarray['Sent Quotes'] == 'yes' || $blobmenuarray['Sent Quotes'] == '') {
                ?>
                <!--<li <?php //echo $sentquotes; ?>>
                    <a href="<?php //echo base_url(); ?>admin/sentquotes">
                        <i class="fa fa-comments"></i>
                        <span class="title">
                            Sent Quotes
                        </span>
                        <span class="selected">
                        </span>
                    </a>
                </li>-->
    <?php
//}
?>
<!-- <?php
    if ($blobmenuarray['Confirmed Reservations'] == 'yes' || $blobmenuarray['Confirmed Reservations'] == '') 
    {
    ?>
        <li <?php echo $conf_res; ?>>
            <a href="<?php echo base_url(); ?>admin/pages/login">
                <i class="fa fa-thumbs-up"></i>
                <span class="title">
                    Admin Login
                </span>
                
            </a>
        </li>
    <?php
    }
?> -->
        <li class="nav-item bold-txt" <?=isset($us_listing)?$us_listing:''?>>
            <a href="javascript:void();">
                <span class="title ">
                   MEMBERSHIPS
                </span>
            </a>
        </li>
        <li class="nav-item">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="icon-diamond"></i>
                <span class="title">Individual Members</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu" style="display: none;">
                <?php
                    if ($blobmenuarray['Confirmed Reservations'] == 'yes' || $blobmenuarray['Confirmed Reservations'] == '') 
                    {
                    ?>
                        <li class="nav-item" <?php echo $conf_res; ?>>
                            <a href="<?php echo base_url(); ?>admin/pages/newpages">
                                <i class="fa fa-thumbs-up"></i>
                                <span class="title">
                                    <!--Confirmed Reservations-->
                                    Manual Enrollment
                                </span>
                                <span class="selected">
                                </span>
                            </a>
                        </li>
                    <?php
                    }
                ?>
                <li class="nav-item" <?=isset($us_listing)?$us_listing:''?>>
                    <a href="<?php echo base_url(); ?>admin/regular_users">
                        <i class="fa fa-thumbs-up"></i>
                        <span class="title">
                            <!--Confirmed Reservations-->
                            Pending Enrollment
                        </span>
                        <span class="selected">
                        </span>
                    </a>
                </li>
                <?php
                if ($blobmenuarray['Confirmed Reservations'] == 'yes' || $blobmenuarray['Confirmed Reservations'] == '') 
                {
                    ?>
                    <li class="nav-item" <?php echo $conf_res; ?>>
                        <a href="<?php echo base_url(); ?>admin/pages/revenue">
                            <i class="fa fa-thumbs-up"></i>
                            <span class="title">
                                <!--Confirmed Reservations-->
                                Revenue
                            </span>
                            <span class="selected">
                            </span>
                        </a>
                    </li>
                    <?php
                }
                ?>
                <li class="nav-item" <?=isset($us_membership)?$us_membership:''?> >
                    <a href="<?php echo base_url(); ?>admin/regular_users/memberships">
                        <i class="fa fa-thumbs-up"></i>
                        <span class="title">
                            <!--Confirmed Reservations-->
                            Membership Database
                        </span>
                        <span class="selected">
                        </span>
                    </a>
                </li>
            </ul>
        </li>
<?php
	if ($blobmenuarray['Pending reservations'] == 'yes' || $blobmenuarray['Pending reservations'] == '') 
	{
	?>
		<li <?php echo $pen_res; ?>>
			<a href="<?php echo base_url(); ?>admin/pen_res">
				<i class="fa fa-thumbs-up"></i>
				<span class="title">
					Hotels – Pending
				</span>
				<span class="selected">
				</span>
			</a>
		</li>
	<?php
	}
?>

<?php
	if ($blobmenuarray['Confirmed Reservations'] == 'yes' || $blobmenuarray['Confirmed Reservations'] == '') 
	{
	?>
		<li <?php echo $conf_res; ?>>
			<a href="<?php echo base_url(); ?>admin/conf_res">
				<i class="fa fa-thumbs-up"></i>
				<span class="title">
					<!--Confirmed Reservations-->
                    Hotels – Confirmed reservations
				</span>
				<span class="selected">
				</span>
			</a>
		</li>
	<?php
	}
?>
<?php
if ($blobmenuarray['Pending Cancellations'] == 'yes' || $blobmenuarray['Pending Cancellations'] == '')
{
    //$query = $this->db->query('SELECT * FROM booking_detail WHERE cancel_status = "1" AND admin_changed = "1" ');
    $query = $this->db->query('SELECT * FROM booking_detail WHERE cancel_status = "1" ');
    $hotels_pending_cancellation_count = $query->num_rows();
    ?>
    <li <?php echo $pen_cancel; ?>>
        <a href="<?php echo base_url(); ?>admin/pending_cancellation">
            <i class="fa fa-thumbs-up"></i>
				<span class="title">
					Hotels - Pending Cancellations (<?php echo $hotels_pending_cancellation_count;?>)
				</span>
				<span class="selected">
				</span>
        </a>
    </li>
<?php
}
?>
<?php
//if ($blobmenuarray['Pending reservations'] == 'yes' || $blobmenuarray['Pending reservations'] == '') {
    ?>
                <!--<li <?php //echo $pen_res; ?>>
                    <a href="<?php //echo base_url(); ?>admin/pen_res">
                        <i class="fa fa-circle-o"></i>
                        <span class="title">
                            Pending reservations
                        </span>
                        <span class="selected">
                        </span>
                    </a>
                </li>-->
    <?php
//}
?>
<?php
//if ($blobmenuarray['Cancel reservations'] == 'yes' || $blobmenuarray['Cancel reservations'] == '') {
    ?>
                <!--<li <?php //echo $can_res; ?>>
                    <a href="<?php //echo base_url(); ?>admin/cancel_res">
                        <i class="fa fa-android"></i>
                        <span class="title">
                            Cancel reservations
                        </span>
                        <span class="selected">
                        </span>
                    </a>
                </li>-->
    <?php
//}
?>
<?php
if ($blobmenuarray['Processed Cancellations'] == 'yes' || $blobmenuarray['Processed Cancellations'] == '') {
    ?>
                <li <?php echo $process_cancel; ?>>
                    <a href="<?php echo base_url(); ?>admin/processedcancel">
                        <i class="fa fa-comments"></i>
                        <span class="title">
                            Processed Cancellations
                        </span>
                        <span class="selected">
                        </span>
                    </a>
                </li>
    <?php
}
?>          
<?php
if ($blobmenuarray['Promo Codes'] == 'yes' || $blobmenuarray['Promo Codes'] == '') {
    ?>
                <!--<li <?php /*echo $manual; */?>>
                    <a href="<?php /*echo base_url(); */?>admin/manualentry">
                        <i class="fa  fa-list-alt"></i>
                        <span class="title">
                            Promo Codes
                        </span>
                        <span class="selected">
                        </span>
                    </a>
                </li>-->
    <?php
}
?>
<?php 
	if ($blobmenuarray['Hotel Price Updated'] == 'yes' || $blobmenuarray['Hotel Price Updated'] == '') 
	{
    ?>
    <li <?php echo $hotelPriceUpdate; ?> >
        <a href="<?php echo base_url(); ?>admin/booking_api/ShowHotelSearchForm">
            <i class="fa  fa-list-alt"></i>
            <span class="title">
                Hotel Price Update
            </span>
            <span class="selected">
            </span>
        </a>
    </li>
	<?php
	}
?>
<?php
if ($blobmenuarray['Coupons'] == 'yes' || $blobmenuarray['Coupons'] == '') {
    ?>
                <li <?php echo $coupons; ?>>
                    <a href="<?php echo base_url(); ?>admin/coupons">
                        <i class="fa fa-book"></i>
                        <span class="title">
                            Coupons
                        </span>
                        <span class="selected">
                        </span>
                    </a>
                </li>
    <?php
}
?>
            <?php
            if ($blobmenuarray['Authorization'] == 'yes' || $blobmenuarray['Authorization'] == '') {
                ?>
                <li <?php echo $authorization; ?>>
                    <a href="<?php echo base_url(); ?>admin/authorization">
                        <i class="fa fa-thumbs-up"></i>
                        <span class="title">
                            Authorization
                        </span>
                        <span class="selected">
                        </span>
                    </a>
                </li>
                <?php
            }
            ?>
            <?php
            if ($blobmenuarray['IP Address BAN'] == 'yes' || $blobmenuarray['IP Address BAN'] == '') {
                ?>
                <li <?php echo $ipaddressban; ?>>
                    <a href="<?php echo base_url(); ?>admin/ipaddressban">
                        <i class="fa fa-ticket"></i>
                        <span class="title">
                            IP Address BAN
                        </span>
                        <span class="selected">
                        </span>
                    </a>
                </li>
                <?php
            }
            ?>

            <?php
            if ($blobmenuarray['Processed IP BAN'] == 'yes' || $blobmenuarray['Processed IP BAN'] == '') {
                ?>
                <li <?php echo $pipaddressban; ?>>
                    <a href="<?php echo base_url(); ?>admin/processipban">
                        <i class="fa fa-unlock-alt"></i>
                        <span class="title">
                            Processed IP BAN
                        </span>
                        <span class="selected">
                        </span>
                    </a>
                </li>
                <?php
            }
            ?>

            <?php
           // if ($blobmenuarray['Database Management'] == 'yes' || $blobmenuarray['Database Management'] == '') {
                ?>
               <!-- <li <?php //echo $datamanage; ?>>
                    <a href="<?php //echo base_url(); ?>admin/datamanage">
                        <i class="fa fa-list-alt"></i>
                        <span class="title">
                            Database Management
                        </span>
                        <span class="selected">
                        </span>
                    </a>
                </li>-->
    <?php
//}
?>

            <?php
            if ($blobmenuarray['Login Log'] == 'yes' || $blobmenuarray['Login Log'] == '') {
                ?>
                <li <?php echo $log; ?>>
                    <a href="<?php echo base_url(); ?>admin/log">
                        <i class="fa fa-table"></i>
                        <span class="title">
                            Login Log 
                        </span>
                        <span class="selected">
                        </span>
                    </a>
                </li>
    <?php
}
?>

            <?php
            if ($blobmenuarray['Statistics'] == 'yes' || $blobmenuarray['Statistics'] == '') {
                ?>
                <li <?php echo $statistics; ?>>
                    <a href="<?php echo base_url(); ?>admin/statistics">
                        <i class="fa fa-bar-chart-o"></i>
                        <span class="title">
                            Statistics
                        </span>
                        <span class="selected">
                        </span>
                    </a>
                </li>
    <?php
}
?>

            <?php
            if ($blobmenuarray['Profile'] == 'yes' || $blobmenuarray['Profile'] == '') {
                ?>
                <li <?php echo $profile; ?>>
                    <a href="<?php echo base_url(); ?>admin/profile">
                        <i class="fa fa-user"></i>
                        <span class="title">
                            Profile 
                        </span>
                        <span class="selected">
                        </span>
                    </a>
                </li>
    <?php
}
?>
<?php
if ($blobmenuarray['Master account'] == 'yes' || $blobmenuarray['Master account'] == '') {
    ?>
                <li <?php echo $LoginMaster; ?>>
                    <a href="<?php echo base_url(); ?>admin/masterlogin">
                        <i class="fa fa-bar-chart-o"></i>
                        <span class="title">
                            Master account 
                        </span>
                        <span class="selected">
                        </span>
                    </a>
                </li>
    <?php
}
?>
<!--
<?php /*
if ($blobmenuarray['Affiliate Request'] == 'yes' || $blobmenuarray['Affiliate Request'] == '') {
    ?>
                <li <?php echo $affiliaterequest; ?>>
                    <a href="<?php echo base_url(); ?>admin/affiliaterequest">
                        <i class="fa  fa-file"></i>
                        <span class="title">
                            Affiliate Request
                        </span>
                        <span class="selected">
                        </span>
                    </a>
                </li>
    <?php
} */
?>

<?php /*
if ($blobmenuarray['Affiliate Database'] == 'yes' || $blobmenuarray['Affiliate Database'] == '') {
    ?>
                <li <?php echo $affdatabase; ?>>
                    <a href="<?php echo base_url(); ?>admin/affidatabase">
                        <i class="fa fa-file-text"></i>
                        <span class="title">
                            Affiliate Database
                        </span>
                        <span class="selected">
                        </span>
                    </a>
                </li>
                <?php
            }*/
            ?>

<?php /*
if ($blobmenuarray['Affiliate Tracker'] == 'yes' || $blobmenuarray['Affiliate Tracker'] == '') {
    ?>
                <li <?php echo $afftracker; ?>>
                    <a href="<?php echo base_url(); ?>admin/affitracker">
                        <i class="fa fa-location-arrow"></i>
                        <span class="title">
                            Affiliate Tracker
                        </span>
                        <span class="selected">
                        </span>
                    </a>
                </li>
                <?php
            }*/
            ?>

<?php /*
if ($blobmenuarray['Affiliate Payment'] == 'yes' || $blobmenuarray['Affiliate Payment'] == '') {
    ?>
                <li <?php echo $affpayment; ?>>
                    <a href="<?php echo base_url(); ?>admin/affipayment">
                        <i class="fa fa-credit-card"></i>
                        <span class="title">
                            Affiliate Payment
                        </span>
                        <span class="selected">
                        </span>
                    </a>
                </li>
                <?php
            } */
            ?>
-->
<?php /*
if ($blobmenuarray['Posted Payments'] == 'yes' || $blobmenuarray['Posted Payments'] == '') {
    ?>
                <li <?php echo $postpayment; ?>>
                    <a href="<?php echo base_url(); ?>admin/postpayment">
                        <i class="fa fa-keyboard-o"></i>
                        <span class="title">
                            Posted Payments
                        </span>
                        <span class="selected">
                        </span>
                    </a>
                </li>
                <?php
            }
            ?>

<?php
 if ($blobmenuarray['Chargebacks'] == 'yes' || $blobmenuarray['Chargebacks'] == '') {
    ?>
                <li <?php echo $chargebacks; ?>>
                    <a href="<?php echo base_url(); ?>admin/chargeback">
                        <i class="fa fa-suitcase"></i>
                        <span class="title">
                            Chargebacks
                        </span>
                        <span class="selected">
                        </span>
                    </a>
                </li>
                <?php
            }
            ?>

            <?php
            if ($blobmenuarray['Processed Chargebacks'] == 'yes' || $blobmenuarray['Processed Chargebacks'] == '') {
                ?>
                <li <?php echo $procharge; ?>>
                    <a href="<?php echo base_url(); ?>admin/processedcharge">
                        <i class="fa fa-briefcase"></i>
                        <span class="title">
                            Processed Chargebacks
                        </span>
                        <span class="selected">
                        </span>
                    </a>
                </li>
                <?php
            }*/
            ?>
            
            <?php
            if ($blobmenuarray['Admin Activity Log'] == 'yes' || $blobmenuarray['Admin Activity Log'] == '') {?>
                <li <?php echo $admin_act_log; ?>>
                    <a href="<?php echo base_url(); ?>admin/admin_log">
                        <i class="fa fa-briefcase"></i>
                        <span class="title">
                            Admin Activity Log
                        </span>
                        <span class="selected">
                        </span>
                    </a>
                </li>
                <?php
            }
            ?>
            <li <?=isset($tp_enrollment_pending)?$tp_enrollment_pending:''?>>
                <a href="<?php echo base_url(); ?>admin/enrollment/pending">
                    <i class="fa fa-briefcase"></i>
                    <span class="title">
                        TP Enrollment - Pending
                    </span>
                    <span class="selected"></span>
                </a>
            </li>
            <li <?=isset($tp_enrollment_approval)?$tp_enrollment_approval:''?>>
                <a href="<?php echo base_url(); ?>admin/enrollment">
                    <i class="fa fa-briefcase"></i>
                    <span class="title">
                    	TP Enrollment - Approval
                    </span>
                    <span class="selected"></span>
                </a>
            </li>
            <li <?=isset($Travel_Professionals)?$Travel_Professionals:''?>>
                <a href="<?php echo base_url(); ?>admin/enrollment/travel_professionals">
                    <i class="fa fa-briefcase"></i>
                    <span class="title">
                    	Travel Professionals
                    </span>
                    <span class="selected"></span>
                </a>
            </li>
            
            <li <?=isset($tp_entity_pending)?$tp_entity_pending:''?>>
                <a href="<?php echo base_url(); ?>admin/entities/pending">
                    <i class="fa fa-briefcase"></i>
                    <span class="title">
                        Entities – Pending
                    </span>
                    <span class="selected"></span>
                </a>
            </li>
            <li <?=isset($tp_entity)?$tp_entity:''?>>
                <a href="<?php echo base_url(); ?>admin/entities">
                    <i class="fa fa-briefcase"></i>
                    <span class="title">
                        Entities
                    </span>
                    <span class="selected"></span>
                </a>
            </li>
            
        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
</div>
<!-- END SIDEBAR -->