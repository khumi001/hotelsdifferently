<?php
    $i=1;
    //print_r($query);
?>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-user"></i>Promo Codes
                </div>
            </div>
            <div class="portlet-body">
                <form id="quetsId" method="post" action="javascript:;">
                <table class="table table-striped table-bordered table-hover" id="sample_2">
                    <thead>
                        <tr>
                            <th>
                                Promocode
                            </th>
                            <th>
                                Amount
                            </th>
							<th>
                                Minimum spend
                            </th>
                            <th>
                                Action
                            </th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <div class="row">
                                    <div class="col-md-3">
                                        <input type="text" name="promocode" id="promocode" class="form-control input-medium" >
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="row">
                                    <div class="col-md-3">
                                        <input type="text" name="amount" id="amount" class="form-control input-medium" >
                                    </div>
                                    
                                </div>
                            </td>
							<td>
                                <div class="row">
                                    <div class="col-md-3">
                                        <input type="text" name="minspend" id="minspend" class="form-control input-medium" >
                                    </div>
                                    
                                </div>
                            </td>
                            <td>
                                <div class="col-md-2">
                                        <button type="submit" class="btn green" id="quets_ok">Ok</button>
                                    </div>
                            </td>
                        </tr>
                        <?php 
                        for($i=0;$i< count($promocode);$i++){
                        ?>
                        <tr>
                        <td><?php echo $promocode[$i]['var_promocode']; ?></td>
                        <td><?php echo  "$" . (number_format($promocode[$i]['int_amount'], 2)); ?></td>
                        <td><?php echo  "$" . (number_format($promocode[$i]['minspend'], 2)); ?></td>
                        <td><button type="button" data-id="<?php echo $promocode[$i]['int_glcode'];?>" class="btn default btn-xs red deletepromocode" data-toggle="modal" href="#delete-model">DELETE</button></td>
                        </tr>
                         <?php
                        }
                        ?>
                    </tbody>
                </table>
                </form>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

<div id="delete-model" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
            <div class="modal-content">
                    <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title">Delete Promo Codes</h4>
                    </div>
                    <div class="modal-body">
                        <h3>Are you Sure to Delete ?...</h3>
                    </div>
                    <div class="modal-footer" >
                            <button type="button" class="btn btn-default" data-dismiss="modal" >Close</button>
                            <button type="submit" id="btndelete" class="btn red"><i class="fa fa-check"></i>Delete</button>
                    </div>
            </div>
    </div>
</div>