<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
            <title>Dealz</title>
            <!--<link href="<?php echo base_url() ?>public/emailtemplate/css/style.css" rel="stylesheet" type="text/css" />-->
            <!--<link href="<?php echo base_url() ?>public/emailtemplate/css/font-awesome.css" rel="stylesheet" type="text/css" />-->
            <style>
                @font-face {
  font-family: 'FuturaStd-Book';
  src: url('http://demo.webeet.net/dealz/public/emailtemplate/fonts/FuturaStd-Book.eot?#iefix') format('embedded-opentype'),  url('http://demo.webeet.net/dealz/public/emailtemplate/fonts/FuturaStd-Book.otf')  format('opentype'),
	     url('http://demo.webeet.net/dealz/public/emailtemplate/fonts/FuturaStd-Book.woff') format('woff'), url('http://demo.webeet.net/dealz/public/emailtemplate/fonts/FuturaStd-Book.ttf')  format('truetype'), url('http://demo.webeet.net/dealz/public/emailtemplate/fonts/FuturaStd-Book.svg#FuturaStd-Book') format('svg');
  font-weight: normal;
  font-style: normal;
}
                body{margin:0; padding:0; font-family: 'FuturaStd-Book';}

                .clear{clear:both;}
                .leadz-main{background:url(http://demo.webeet.net/dealz/public/emailtemplate/img/Atlantis-Dubai-Hotel.jpeg) no-repeat; background-size:cover; width:600px;  margin:auto; background-attachment:fixed;}
                .leadz-main .containt-main{background:rgba(17, 125, 200, 0.61);}
                .leadz-main .containt-main:hover{background:rgba(40, 40, 40, 0.5); transition:ease-in-out 500ms}
                .leadz-main .containt-main:hover .logo{box-shadow: 0 0 43px #ffffff inset; border-radius: 18px; transition:all 500ms}

                .leadz-main .containt-main .top-containt{}

                .leadz-main .containt-main .first .left{float:left;}
                .leadz-main .containt-main .first .left .logo{display:block; margin:10px 0 0 10px;}
                .leadz-main .containt-main .first .left .logo img {width: 130px;}
                .leadz-main .containt-main .first .right{float:right;}
                .leadz-main .containt-main .first .right ul{margin: 30px 20px 0 0; padding:0;}
                .leadz-main .containt-main .first .right ul li{display:inline-block;}
                .leadz-main .containt-main .first .right ul li a{background:rgba(255, 255, 255, 0.62); border-radius: 3px; color: #818181; font-size: 14px;    padding: 2px 20px; text-decoration: none;}

                .leadz-main .containt-main .sec{padding:70px 0 0 0;}
                .leadz-main .containt-main .sec h1{font-size:50px; text-transform:uppercase; color:#fff; text-align:center; margin:0}
                .leadz-main .containt-main .sec h2{font-size:35px; text-transform:uppercase; color:#fff; text-align:center; margin:0}
                .leadz-main .containt-main .sec .line{text-align:center; margin-top:20px;}
                .leadz-main .containt-main .sec h3{font-size:18px; text-transform:uppercase; color:#fff; text-align:center; margin:20px 0 0}
                .leadz-main .containt-main .sec .price{font-size:30px; text-transform:uppercase; color:#fff; text-align:center; margin:10px 0 0}
                .leadz-main .containt-main .sec .price span{font-size:30px; margin-right:5px;}

                .leadz-main .containt-main .three{margin-top:30px; width:90%; margin:30px auto 0 auto}
                .leadz-main .containt-main .three .hotel-info{margin-top:20px; padding: 5px; background:#fff}
                .leadz-main .containt-main .three .hotel-info .h-left{float:left; width:49%;}
                .leadz-main .containt-main .three .hotel-info .h-left img{width:100%; height:155px;}
                .leadz-main .containt-main .three .hotel-info .h-right{float:right; width:49%;}
                .leadz-main .containt-main .three .hotel-info .h-right img{width:100%; height:155px;}
                .leadz-main .containt-main .three .hotel-info .hotel-des{padding:10px}
                .leadz-main .containt-main .three .hotel-info .hotel-des h1{color:#9a9a9a; font-size:20px; margin:5px 0 0 0px}
                .leadz-main .containt-main .three .hotel-info .hotel-des .place{font-size:20px; color:#9a9a9a;}
                .leadz-main .containt-main .three .hotel-info .hotel-des .place b{margin-right:5px;}
                .leadz-main .containt-main .three .hotel-info .hotel-des p{font-size:14px; color:#9a9a9a;}
                .leadz-main .containt-main .three .hotel-info .hotel-des .price-del{font-size:20px; color:#9a9a9a;}

                .containt-main .book{text-align:center; padding:50px 0;}
                .containt-main .book a{display:inline-block; text-decoration:none; color:#fff; margin:auto; padding:5px 10px;background:#993; text-transform:uppercase; border:solid 2px #fff; border-radius:3px; font-weight:600;}
                .containt-main .book a:hover{background:#099; transition:500ms}

                .containt-main .top-containt h1{color:#fff; text-align:center;}
                .containt-main .top-containt .hotel_text{color:#fff;text-align:center}

                .containt-main .top-hotel{width:90%; margin:40px auto auto auto; background:rgba(255, 255, 255, 0.80);}
                .containt-main .top-hotel .hed{text-align:center; font-size:25px; padding-top:15px; color:#afafaf; text-transform:uppercase;}
                .containt-main .top-hotel .img-main{float:left; width:30%; margin:10px 9px;}
                .containt-main .top-hotel .img-main h2{color: #7d7d7d; font-size: 14px;  margin: 2px 0; text-align: center; text-transform: uppercase;}
                .containt-main .top-hotel .img-main .image-hotel img{width:100%;}

            </style>
            <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'></link>
    </head>

    <body style="margin: 0;padding: 0;font-family: 'FuturaStd-Book'; color: #fff !important;">
        <div class="leadz-main" style="background: url(<?php echo base_url();?>public/emailtemplate/img/body_bg1.jpg) no-repeat; background-size: cover;margin: auto;background-attachment: fixed;">
            <div class="containt-main" style="background: rgba(17, 125, 200, 0.61);color: #fff;">
                <div class="first">
                    <div class="left" style="margin: auto; text-align: center;float: none;">
                        <a class="logo" href="<?php echo base_url();?>" style="display: inline-block;margin: 10px 0 0 10px;">
                            <img src="<?php echo base_url();?>/public/emailtemplate/img/logo.png" style="width: 130px;">
                        </a>
                    </div>
<!--                    <div class="right" style="float: right;">
                        <ul style="margin: 30px 20px 0 0;padding: 0;">
                            <li style="display: inline-block;"><a href="#" style="background: rgba(255, 255, 255, 0.62);border-radius: 3px;color: #818181;font-size: 14px;padding: 2px 20px;text-decoration: none;">Home</a></li>
                            <li style="display: inline-block;"><a href="#" style="background: rgba(255, 255, 255, 0.62);border-radius: 3px;color: #818181;font-size: 14px;padding: 2px 20px;text-decoration: none;">About</a></li>
                            <li style="display: inline-block;"><a href="#" style="background: rgba(255, 255, 255, 0.62);border-radius: 3px;color: #818181;font-size: 14px;padding: 2px 20px;text-decoration: none;">Hotel</a></li>
                        </ul>
                    </div>-->
                    <div class="clear" style="clear: both;"></div>
                </div>

                <div class="top-containt">
                    <h1 style="color: #fff;text-align: center;">Take the hotel challenge!</h1>
                    <div class="hotel_text" style="color: #fff;text-align: center;">See how our rates can be considerably lower than the big sites,<br> such as <span>Hotels.com<sup>&copy;</sup>,</span> <span>Expedia<sup>&copy;</sup>.com, </span> <span>Booking.com<sup>&copy;</sup></span> or <span>Travelocity<sup>&copy;</sup>.com</span></div>
                </div>
                <div class="top-hotel" style="width: 90%;margin: 20px;background: #0C5FA7; color: #ffffff;">
                    <div style="padding-bottom: 10px; padding-top: 10px;width: 90%; margin: auto; font-family: 'Open Sans', sans-serif;color: #fff !important;">
                    &nbsp;<div class='col-md-12' style="color:red;text-align: center;">
                        <b>CONGRATULATIONS! Your reservation is booked!</b>
                    </div>&nbsp;
                    <div class='col-md-12' style="padding: 0px 20px">
                        <div class="row">
                            <span style="color: #fff;">Thank you for booking with <b>HotelsDifferently!<sup>sm</sup></b> We are delighted that you decided to choose us for your next getaway.</span>
                        </div>&nbsp;
                        <div class="row">
                            <span style="color: #fff;">Your charge will appear on your statement as: <b>HOTELSDIFFERENTLY</b></span>
                        </div>&nbsp;
                        <div class="row">
                            <span style="color: #fff;">Your reservation confirmation number is:  <b><?php echo $confirm[0]['var_transactionid']; ?></b></span>
                        </div>&nbsp;
                        <div class="row">
                            <span style="color: #fff;">Once again, here are the details of your reservation: </span>
                        </div>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                    </div>
                    <div style="width: 90%; margin: auto;color: #fff !important;">
                        <div style="float: left; width: 47%;color: #fff !important;">
                            <div style="margin-bottom: 15px;">
                                <div style="float: left; width: 30%;">Check-in:</div>
                                <!--<div style="float: left; width: 1%;">:</div>-->
                                <div style="float: right; width: 65%;"><?php echo $confirm[0]['var_checkin']; ?></div>
                                <div class="clear" style="clear: both;"></div>
                            </div>

                            <div style="margin-bottom: 15px;">
                                <div style="float: left; width: 30%;">City:</div>
                                <!--<div style="float: left; width: 1%;">:</div>-->
                                <div style="float: right; width: 65%;"><?php echo $confirm[0]['var_city']; ?></div>
                                <div class="clear" style="clear: both;"></div>
                            </div>

                            <div style="margin-bottom: 15px;">
                                <div style="float: left; width: 30%;">Name of hotel:</div>
                                <!--<div style="float: left; width: 1%;">:</div>-->
                                <div style="float: right; width: 65%;"><?php echo $confirm[0]['var_NOH']; ?></div>
                                <div class="clear" style="clear: both;"></div>
                            </div>

                        </div>

                        <!--Right Side Listing-->    
                        <div style="float: right; width: 47%; color: #fff !important;">
                            <div style="margin-bottom: 15px;">
                                <div style="float: left; width: 30%;"><b>Check-out:</b></div>
                                <!--<div style="float: left; width: 1%;">:</div>-->
                                <div style="float: right; width: 65%;"><?php echo $confirm[0]['var_checkout']; ?></div>
                                <div class="clear" style="clear: both;"></div>
                            </div>

                            <div style="margin-bottom: 15px;">
                                <div style="float: left; width: 30%;"><b>Quote ID:</b></div>
                                <!--<div style="float: left; width: 1%;">:</div>-->
                                <div style="float: right; width: 65%;"><?php echo $confirm[0]['var_uniq_quoteid']; ?></div>
                                <div class="clear" style="clear: both;"></div>
                            </div>

                            <div style="margin-bottom: 15px;">
                                <div style="float: left; width: 30%;"><b>Stars:</b></div>
                                <!--<div style="float: left; width: 1%;">:</div>-->
                                <div style="float: right; width: 65%;"><?php echo $confirm[0]['var_star']; ?></div>
                                <div class="clear" style="clear: both;"></div>
                            </div>

                        </div>

                        <div class="clear" style="clear: both; color: #fff !important;">
                            <div style="float: left; width: 33%;">
                                <div style="margin-bottom: 15px;">
                                    <div style="float: left; width: 30%;"><b>Rooms:</b></div>
                                    <!--<div style="float: left; width: 1%;">:</div>-->
                                    <div style="float: right; width: 65%;"><?php echo $confirm[0]['var_room']; ?></div>
                                    <div class="clear" style="clear: both;"></div>
                                </div>
                            </div>
                            <div style="float: left; width: 33%;">
                                <div style="margin-bottom: 15px;">
                                    <div style="float: left; width: 32%;"><b>Adults:</b></div>
                                    <!--<div style="float: left; width: 1%;">:</div>-->
                                    <div style="float: right; width: 50%;"><?php echo $confirm[0]['var_adult']; ?></div>
                                    <div class="clear" style="clear: both;"></div>
                                </div>
                            </div>
                            <div style="float: right; width: 33%;">
                                <div style="margin-bottom: 15px;">
                                    <div style="float: left; width: 36%;"><b>Children:</b></div>
                                    <!--<div style="float: left; width: 1%;">:</div>-->
                                    <div style="float: right; width: 55%;"><?php echo $confirm[0]['var_child']; ?></div>
                                    <div class="clear" style="clear: both;"></div>
                                </div>
                            </div>
                        </div>
                        
                        <div style="float: left; width: 100%;">
                            <div style="margin-bottom: 15px;">
                                <div style="float: left; width: 24%;"><b>Cancellation policy:</b></div>
                                <!--<div style="float: left; width: 1%;">:</div>-->
                                <div style="float: right; width: 72%;"><?php echo $confirm[0]['var_policy']; ?></div>
                                <div class="clear" style="clear: both;"></div>
                            </div>

                            <div style="margin-bottom: 15px;">
                                <div style="float: left; width: 24%;"><b>Comments:</b></div>
                                <!--<div style="float: left; width: 1%;">:</div>-->
                                <div style="float: right; width: 72%;"><?php echo $confirm[0]['var_comment']; ?></div>
                                <div class="clear" style="clear: both;"></div>
                            </div>
                        </div>
                        
                        <div style="width: 280px; margin: auto">
                            <div style="margin-bottom: 15px;">
                                <div style="float: left; width: 40%;"><b>Quoted price:</b></div>
                                <!--<div style="float: left; width: 1%;">:</div>-->
                                <div style="float: right; width: 55%;"><?php echo '$' . $confirm[0]['var_prize']; ?></div>
                                <div class="clear" style="clear: both;"></div>
                            </div>

                            <div style="margin-bottom: 15px;">
                                <div style="float: left; width: 40%;"><b>Coupon redeemed:</b></div>
                                <!--<div style="float: left; width: 1%;">:</div>-->
                                <div style="float: right; width: 55%;"><?php echo $coupanstr; ?></div>
                                <div class="clear" style="clear: both;"></div>
                            </div>
                            
                            <div style="margin-bottom: 15px;">
                                <div style="float: left; width: 40%;color:red;"><b>Paid amount:</b></div>
                                <!--<div style="float: left; width: 1%;">:</div>-->
                                <div style="float: right; width: 55%;color:red;"><?php echo '$' . $finalamount; ?></div>
                                <div class="clear" style="clear: both;"></div>
                            </div>
                        </div>
                    </div>
                    <p style="color:#fff;">Sincerely,<br/><strong style="color:#fff;">HotelsDifferently.com</strong></p>
                    <div style="width: 90%; margin: auto;"></div>
                </div>
                    <div class="book" style="text-align: center;padding: 30px 0;"></div>
                     <div style="font-size: 11px;color: #fff;padding: 5px;background-color: #0C5FA7;">Cancellation policies between <b>HotelsDifferently<sup>sm</sup></b> quotes and competitor sites may vary.</div>
                    </div>
                <!--<div class="book" style="text-align: center;padding: 30px 0;"><a href="#" style="display: inline-block;text-decoration: none;color: #fff;margin: auto;padding: 5px 10px;background: #993;text-transform: uppercase;border: solid 2px #fff;border-radius: 3px;font-weight: 600;">book now</a></div>-->
               
            </div>
        </div>
    </body>
</html>
