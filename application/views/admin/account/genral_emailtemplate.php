<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
            <title>Dealz</title>
            <!--<link href="<?php echo base_url() ?>public/emailtemplate/css/style.css" rel="stylesheet" type="text/css" />-->
            <!--<link href="<?php echo base_url() ?>public/emailtemplate/css/font-awesome.css" rel="stylesheet" type="text/css" />-->
            <style>
                @font-face {
                    font-family: 'FuturaStd-Book';
                    src: url('https://hotelsdifferently.com/public/emailtemplate/fonts/FuturaStd-Book.eot?#iefix') format('embedded-opentype'),  url('https://hotelsdifferently.com/public/emailtemplate/fonts/FuturaStd-Book.otf')  format('opentype'),
                        url('https://hotelsdifferently.com/public/emailtemplate/fonts/FuturaStd-Book.woff') format('woff'), url('https://hotelsdifferently.com/public/emailtemplate/fonts/FuturaStd-Book.ttf')  format('truetype'), url('https://hotelsdifferently.com/public/emailtemplate/fonts/FuturaStd-Book.svg#FuturaStd-Book') format('svg');
                    font-weight: normal;
                    font-style: normal;
                }
                body{margin:0; padding:0; font-family: 'FuturaStd-Book';}
				@font-face {
					font-family: darkwoman;
					src: url('https://hotelsdifferently.com/beta/public/affilliate_theme/stylesheets/FuturaStd-Book.otf');
				}
                .clear{clear:both;}
                /*.leadz-main{background:url(http://demo.webeet.net/dealz/public/emailtemplate/img/body_bg1.jpg) no-repeat; background-size: 547px 100% !important; width:600px;  margin:auto; background-attachment:fixed;}*/
                .leadz-main .containt-main{background:rgba(17, 125, 200, 0.61);}
                .leadz-main .containt-main:hover{background:rgba(40, 40, 40, 0.5); transition:ease-in-out 500ms}
                .leadz-main .containt-main:hover .logo{box-shadow: 0 0 43px #ffffff inset; border-radius: 18px; transition:all 500ms}

                .leadz-main .containt-main .top-containt{}

                .leadz-main .containt-main .first .left{float:left;}
                .leadz-main .containt-main .first .left .logo{display:block; margin:10px 0 0 10px;}
                .leadz-main .containt-main .first .left .logo img {width: 130px;}
                .leadz-main .containt-main .first .right{float:right;}
                .leadz-main .containt-main .first .right ul{margin: 30px 20px 0 0; padding:0;}
                .leadz-main .containt-main .first .right ul li{display:inline-block;}
                .leadz-main .containt-main .first .right ul li a{background:rgba(255, 255, 255, 0.62); border-radius: 3px; color: #818181; font-size: 14px;    padding: 2px 20px; text-decoration: none;}

                .leadz-main .containt-main .sec{padding:70px 0 0 0;}
                .leadz-main .containt-main .sec h1{font-size:50px; text-transform:uppercase; color:#fff; text-align:center; margin:0}
                .leadz-main .containt-main .sec h2{font-size:35px; text-transform:uppercase; color:#fff; text-align:center; margin:0}
                .leadz-main .containt-main .sec .line{text-align:center; margin-top:20px;}
                .leadz-main .containt-main .sec h3{font-size:18px; text-transform:uppercase; color:#fff; text-align:center; margin:20px 0 0}
                .leadz-main .containt-main .sec .price{font-size:30px; text-transform:uppercase; color:#fff; text-align:center; margin:10px 0 0}
                .leadz-main .containt-main .sec .price span{font-size:30px; margin-right:5px;}

                .leadz-main .containt-main .three{margin-top:30px; width:90%; margin:30px auto 0 auto}
                .leadz-main .containt-main .three .hotel-info{margin-top:20px; padding: 5px; background:#fff}
                .leadz-main .containt-main .three .hotel-info .h-left{float:left; width:49%;}
                .leadz-main .containt-main .three .hotel-info .h-left img{width:100%; height:155px;}
                .leadz-main .containt-main .three .hotel-info .h-right{float:right; width:49%;}
                .leadz-main .containt-main .three .hotel-info .h-right img{width:100%; height:155px;}
                .leadz-main .containt-main .three .hotel-info .hotel-des{padding:10px}
                .leadz-main .containt-main .three .hotel-info .hotel-des h1{color:#9a9a9a; font-size:20px; margin:5px 0 0 0px}
                .leadz-main .containt-main .three .hotel-info .hotel-des .place{font-size:20px; color:#9a9a9a;}
                .leadz-main .containt-main .three .hotel-info .hotel-des .place b{margin-right:5px;}
                .leadz-main .containt-main .three .hotel-info .hotel-des p{font-size:14px; color:#9a9a9a;}
                .leadz-main .containt-main .three .hotel-info .hotel-des .price-del{font-size:20px; color:#9a9a9a;}

                .containt-main .book{text-align:center; padding:50px 0;}
                .containt-main .book a{display:inline-block; text-decoration:none; color:#fff; margin:auto; padding:5px 10px;background:#993; text-transform:uppercase; border:solid 2px #fff; border-radius:3px; font-weight:600;}
                .containt-main .book a:hover{background:#099; transition:500ms}

                .containt-main .top-containt h1{color:#fff; text-align:center;}
                .containt-main .top-containt .hotel_text{color:#fff;text-align:center}

                .containt-main .top-hotel{width:90%; margin:40px auto auto auto; background:rgba(255, 255, 255, 0.80);}
                .containt-main .top-hotel .hed{text-align:center; font-size:25px; padding-top:15px; color:#afafaf; text-transform:uppercase;}
                .containt-main .top-hotel .img-main{float:left; width:30%; margin:10px 9px;}
                .containt-main .top-hotel .img-main h2{color: #7d7d7d; font-size: 14px;  margin: 2px 0; text-align: center; text-transform: uppercase;}
                .containt-main .top-hotel .img-main .image-hotel img{width:100%;}
				.im{color:#fff;}

            </style>
            <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'/>
    </head>

    <body style="margin: 0;padding: 0;font-family: 'FuturaStd-Book';color: #fff !important;">
        <div class="leadz-main" style="background: url(http://hotels.softech.website/public/affilliate_theme/img/home-bg.jpg); background-repeat:no-repeat;  background-size: 100% 100%; width: 547px; margin: auto;background-attachment: fixed;">
            <div class="containt-main" style="background: rgba(17, 125, 200, 0.61);color: #fff;width: 100%;">
                <div class="first">
                    <div class="left" style="margin: auto; text-align: center;float: none;">
                        <a class="logo" href="https://www.WHotelsGroup.com" style="display: inline-block;margin: 10px 0 0 10px;">
                            <img src="<?php echo base_url(); ?>public/affilliate_theme/img/logo.png" style="width: 130px;">
                        </a>
                    </div>
                    <!--                    <div class="right" style="float: right;">
                                            <ul style="margin: 30px 20px 0 0;padding: 0;">
                                                <li style="display: inline-block;"><a href="#" style="background: rgba(255, 255, 255, 0.62);border-radius: 3px;color: #818181;font-size: 14px;padding: 2px 20px;text-decoration: none;">Home</a></li>
                                                <li style="display: inline-block;"><a href="#" style="background: rgba(255, 255, 255, 0.62);border-radius: 3px;color: #818181;font-size: 14px;padding: 2px 20px;text-decoration: none;">About</a></li>
                                                <li style="display: inline-block;"><a href="#" style="background: rgba(255, 255, 255, 0.62);border-radius: 3px;color: #818181;font-size: 14px;padding: 2px 20px;text-decoration: none;">Hotel</a></li>
                                            </ul>
                                        </div>-->
                    <div class="clear" style="clear: both;"></div>
                </div>

                <div class="top-containt">
                    <h1 style="max-width:350px; margin:20px auto;"><img style="display:block;width:100%;height:auto;" src="http://hotels.softech.website/public/affilliate_theme/img/txt-email.png" alt="" /></h1>
                    <!--<div class="hotel_text" style="color: #fff;text-align: center;">See how you can save UP TO 40% OFF compared <br/>to the major hotel booking sites!</div>-->
                </div>
                <div class="top-hotel" style="width: 90%;margin: 40px auto auto auto;background: #0C5FA7; color: #ffffff;">
                    <div style="padding-bottom: 10px; padding-top: 10px;width: 90%; margin: auto; font-family: 'Open Sans', sans-serif;color: #fff !important; font-size:12px; line-height:16px;">
                        <?php echo $mail_body; ?>
                        <div class="clear" style="clear: both;"></div>
                    </div>
                </div>
                
                <?php
                if($bookbutton == "Yes" )
                {
                ?>
                <div class="book" style="text-align: center;padding: 30px 0;"><a href="<?php echo base_url() . 'user/reservation/quotes'; ?>" style="display: inline-block;text-decoration: none;color: #fff;margin: auto;padding: 5px 10px;background: #993;text-transform: uppercase;border: solid 2px #fff;border-radius: 3px;font-weight: 600;">book now</a></div>
               <?php
                }else{
                ?>
                <div class="book" style="text-align: center;padding: 10px 0;"></div>
                <?php }?>
                <?php if($lastline != "yes"){ ?>
                <!--<div style="font-size: 11px;color: #fff;padding: 5px; font-family: 'Open Sans', sans-serif;background-color: #0C5FA7;">Cancellation policies between <b>HotelsDifferently<sup>sm</sup></b> quotes and competitor sites may vary.</div>-->
                <div style="font-size: 11px;color: #fff;padding: 5px; font-family: 'Open Sans', sans-serif;display: block;">&nbsp;</div>
                <?php }else{ ?>
                <div style="font-size: 11px;color: #fff;padding: 5px; font-family: 'Open Sans', sans-serif;display: block;">&nbsp;</div>
                <?php } ?>
            </div>
        </div>
    </body>
</html>
