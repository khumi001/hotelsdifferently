<?php
$code_sent = $this->session->userdata('code_sent');
$session_username = $this->session->userdata('session_username');
$session_password = $this->session->userdata('session_password');
?>

<?php
if ($code_sent == 'yes') {
    ?>    <!-- BEGIN VERIFICATION FORM -->
    <form class="verify-form" action="account/verify_sms" method="post">
        <h3 class="form-title">Login For Dealz </h3>

        <div id="main_form">
            <div class="pom-agile">
                <input placeholder="SMS Code" id="simple_code" name="sms_code" class="user" type="text">
                <span class="icon1"><i class="fa fa-lock" aria-hidden="true"></i></span>
            </div>
            <div class="sub-w3l">
                <div class="btn-sms-code">
                    <button type="submit" class="btn btn-sm btn-email pull-left">Login</button>
                    <button type="button" id="skipCode" title="Provide the code and enter skip" class="btn btn-sm btn-email pull-right"> Skip</button>
                </div>
            </div>
        </div>
        <!--<div id="skip_form" style="display:none;">
            <div class="alert alert-danger display-hide">
                <button class="close" data-close="alert"></button>
                <span>Enter SMS Code.</span>
            </div>
            <div class="form-group">
                <div class="input-icon">
                    <i class="fa fa-lock"></i>
                    <input id="simple_code" class="form-control placeholder-no-fix" type="text" autocomplete="off"
                           placeholder="Code" name="simple_code"/>
                </div>
            </div>
            <div class="form-actions">
                <button type="button" id="forgot_btn" class="btn blue pull-right">Forgot <i
                        class="m-icon-swapright m-icon-white"></i></button>
            </div>
        </div>-->
        <script src="https://code.jquery.com/jquery-3.1.1.min.js"
                integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
        <script type="text/javascript">
            /*$('#skipBtn').click(function () {
             $('#main_form').hide();
             $('#skip_form').show();
             });*/
            $('#skipCode').click(function () {
                if ($('#simple_code').val() != "") {
                    $.post("<?php echo base_url(); ?>/admin/account/skip_btn_form",
                        {
                            skip_code: $('#simple_code').val()
                        },
                        function (data, status) {
                            if (data == 1) {
                                $('#simple_code').css('border-color', 'green');
                                $('.verify-form').append('<input type="hidden" name="skip" value="1" />');
                                $('.verify-form').submit();
                            }
                            else {
                                $('#simple_code').css('border-color', 'red');
                                alert("Your provided skip code is invalid. Contact to Admin for more detail");
                            }
                        });
                }
                else {
                    $('#simple_code').css('border-color', 'red');
                    alert("Your provided skip code is invalid. Contact to Admin for more detail");
                }
            });
        </script>
        <input type="hidden" name="username" value="<?php echo $session_username ?>">
        <input type="hidden" name="password" value="<?php echo $session_password ?>">
    </form>

    <!-- END VERIFICATION FORM -->    <?php $this->session->unset_userdata('code_sent');

} else {
    ?>    <!-- BEGIN LOGIN FORM -->
    <form class="login-form" method="post">
        <div class="pom-agile input-icon">
            <input placeholder="Username" id="user" name="username" class="user" type="email" required="">
            <span class="icon1"><i class="fa fa-user" aria-hidden="true"></i></span>
        </div>
        <div class="pom-agile input-icon">
            <input placeholder="Password" id="pass" name="password" class="pass" type="password" required="">
            <span class="icon2"><i class="fa fa-unlock" aria-hidden="true"></i></span>
        </div>
        <div class="sub-w3l">
            <!--<h6><a href="#">Forgot Password?</a></h6>-->

            <div class="right-w3l">
                <input type="submit" value="Login">
            </div>
        </div>
    </form>

<?php } ?>