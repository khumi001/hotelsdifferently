<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Dealz</title>
        <!--<link href="<?php echo base_url()?>public/emailtemplate/css/style.css" rel="stylesheet" type="text/css" />-->
        <!--<link href="<?php echo base_url()?>public/emailtemplate/css/font-awesome.css" rel="stylesheet" type="text/css" />-->
        <style>
            @font-face {
  font-family: 'FuturaStd-Book';
  src: url('<?php echo base_url()?>public/emailtemplate/fonts/FuturaStd-Book.eot?#iefix') format('embedded-opentype'),  url('<?php echo base_url()?>public/emailtemplate/fonts/FuturaStd-Book.otf')  format('opentype'),
	     url('<?php echo base_url()?>public/emailtemplate/fonts/FuturaStd-Book.woff') format('woff'), url('<?php echo base_url()?>public/emailtemplate/fonts/FuturaStd-Book.ttf')  format('truetype'), url('<?php echo base_url()?>public/emailtemplate/fonts/FuturaStd-Book.svg#FuturaStd-Book') format('svg');
  font-weight: normal;
  font-style: normal;
}
body{margin:0; padding:0; font-family: 'FuturaStd-Book';}

.clear{clear:both;}
.leadz-main{background:url(<?php echo base_url()?>public/emailtemplate/img/Atlantis-Dubai-Hotel.jpeg) no-repeat; background-size:cover; width:600px;  margin:auto; background-attachment:fixed;}
.leadz-main .containt-main{background:rgba(17, 125, 200, 0.61);}
.leadz-main .containt-main:hover{background:rgba(40, 40, 40, 0.5); transition:ease-in-out 500ms}
.leadz-main .containt-main:hover .logo{box-shadow: 0 0 43px #ffffff inset; border-radius: 18px; transition:all 500ms}

.leadz-main .containt-main .top-containt{}

.leadz-main .containt-main .first .left{float:left;}
.leadz-main .containt-main .first .left .logo{display:block; margin:10px 0 0 10px;}
.leadz-main .containt-main .first .left .logo img {width: 130px;}
.leadz-main .containt-main .first .right{float:right;}
.leadz-main .containt-main .first .right ul{margin: 30px 20px 0 0; padding:0;}
.leadz-main .containt-main .first .right ul li{display:inline-block;}
.leadz-main .containt-main .first .right ul li a{background:rgba(255, 255, 255, 0.62); border-radius: 3px; color: #818181; font-size: 14px;    padding: 2px 20px; text-decoration: none;}

.leadz-main .containt-main .sec{padding:70px 0 0 0;}
.leadz-main .containt-main .sec h1{font-size:50px; text-transform:uppercase; color:#fff; text-align:center; margin:0}
.leadz-main .containt-main .sec h2{font-size:35px; text-transform:uppercase; color:#fff; text-align:center; margin:0}
.leadz-main .containt-main .sec .line{text-align:center; margin-top:20px;}
.leadz-main .containt-main .sec h3{font-size:18px; text-transform:uppercase; color:#fff; text-align:center; margin:20px 0 0}
.leadz-main .containt-main .sec .price{font-size:30px; text-transform:uppercase; color:#fff; text-align:center; margin:10px 0 0}
.leadz-main .containt-main .sec .price span{font-size:30px; margin-right:5px;}

.leadz-main .containt-main .three{margin-top:30px; width:90%; margin:30px auto 0 auto}
.leadz-main .containt-main .three .hotel-info{margin-top:20px; padding: 5px; background:#fff}
.leadz-main .containt-main .three .hotel-info .h-left{float:left; width:49%;}
.leadz-main .containt-main .three .hotel-info .h-left img{width:100%; height:155px;}
.leadz-main .containt-main .three .hotel-info .h-right{float:right; width:49%;}
.leadz-main .containt-main .three .hotel-info .h-right img{width:100%; height:155px;}
.leadz-main .containt-main .three .hotel-info .hotel-des{padding:10px}
.leadz-main .containt-main .three .hotel-info .hotel-des h1{color:#9a9a9a; font-size:20px; margin:5px 0 0 0px}
.leadz-main .containt-main .three .hotel-info .hotel-des .place{font-size:20px; color:#9a9a9a;}
.leadz-main .containt-main .three .hotel-info .hotel-des .place b{margin-right:5px;}
.leadz-main .containt-main .three .hotel-info .hotel-des p{font-size:14px; color:#9a9a9a;}
.leadz-main .containt-main .three .hotel-info .hotel-des .price-del{font-size:20px; color:#9a9a9a;}

.containt-main .book{text-align:center; padding:50px 0;}
.containt-main .book a{display:inline-block; text-decoration:none; color:#fff; margin:auto; padding:5px 10px;background:#993; text-transform:uppercase; border:solid 2px #fff; border-radius:3px; font-weight:600;}
.containt-main .book a:hover{background:#099; transition:500ms}

.containt-main .top-containt h1{color:#fff; text-align:center;}
.containt-main .top-containt .hotel_text{color:#fff;text-align:center}

.containt-main .top-hotel{width:90%; margin:40px auto auto auto; background:rgba(255, 255, 255, 0.80);}
.containt-main .top-hotel .hed{text-align:center; font-size:25px; padding-top:15px; color:#afafaf; text-transform:uppercase;}
.containt-main .top-hotel .img-main{float:left; width:30%; margin:10px 9px;}
.containt-main .top-hotel .img-main h2{color: #7d7d7d; font-size: 14px;  margin: 2px 0; text-align: center; text-transform: uppercase;}
.containt-main .top-hotel .img-main .image-hotel img{width:100%;}

        </style>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'></link>
    </head>

    <body>
        <div class="leadz-main">
            <div class="containt-main">
                <div class="first">
                    <div class="left"><a class="logo" href="#"><img src="<?php echo base_url()?>public/emailtemplate/img/logo.png" /></a></div>
                    <div class="right">
                        <ul>
                            <li><a href="#">Home</a></li>
                            <li><a href="#">About</a></li>
                            <li><a href="#">Hotel</a></li>
                        </ul>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="top-containt">
                    <h1>Take the hotel challenge!</h1>
                    <div class="hotel_text">See how our rates can be considerably lower than the big sites,<br> such as <span>Hotels.com<sup>&copy;</sup>,</span> <span>Expedia<sup>&copy;</sup>.com, </span> <span>Booking.com<sup>&copy;</sup></span> or <span>Travelocity<sup>&copy;</sup>.com</span></div>
                </div>
                <div class="top-hotel">
                    <h1 class="hed">Top Hottest savings by city</h1>
                    <div class="img-main">
                        <h2>Los Angeles</h2>
                        <div class="image-hotel">
                            <img src="http://demo.webeet.net/dealz/public/affilliate_theme/img/gall_img2.jpg">
                        </div>
                    </div>
                    <div class="img-main">
                        <h2>Las Vegas</h2>
                        <div class="image-hotel">
                            <img src="http://demo.webeet.net/dealz/public/affilliate_theme/img/gall_img3.jpg">
                        </div>
                    </div>
                    <div class="img-main">
                        <h2>Orlando</h2>
                        <div class="image-hotel">
                            <img src="http://demo.webeet.net/dealz/public/affilliate_theme/img/gall_img4.jpg">
                        </div>
                    </div>
                    <div class="img-main">
                        <h2>Miami</h2>
                        <div class="image-hotel">
                            <img src="http://demo.webeet.net/dealz/public/affilliate_theme/img/gall_img5.jpg">
                        </div>
                    </div>
                    <div class="img-main">
                        <h2>Paris</h2>
                        <div class="image-hotel">
                            <img src="http://demo.webeet.net/dealz/public/affilliate_theme/img/gall_img6.jpg">
                        </div>
                    </div>
                    <div class="img-main">
                        <h2>Hong kong</h2>
                        <div class="image-hotel">
                            <img src="http://demo.webeet.net/dealz/public/affilliate_theme/img/gall_img7.jpg">
                        </div>
                    </div>
                    <div class="img-main">
                        <h2>istanbul</h2>
                        <div class="image-hotel">
                            <img src="http://demo.webeet.net/dealz/public/affilliate_theme/img/gall_img8.jpg">
                        </div>
                    </div>
                    <div class="img-main">
                        <h2>seoul</h2>
                        <div class="image-hotel">
                            <img src="http://demo.webeet.net/dealz/public/affilliate_theme/img/gall_img1.jpg">
                        </div>
                    </div>
                    <div class="img-main">
                        <h2>Toronto</h2>
                        <div class="image-hotel">
                            <img src="http://demo.webeet.net/dealz/public/affilliate_theme/img/gall_img9.jpg">
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="sec">
                    <h1>Newyork</h1>
                    <h2>Best place for holiday</h2>
                    <div class="line"><img src="<?php echo base_url()?>public/emailtemplate/img/line.png" /></div>
                    <h3>form</h3>
                    <div class="price"><span>$</span>999</div>
                </div>
                <div class="three">
                    <div class="map"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3021.9073786664967!2d-73.97459495460745!3d40.76406183316124!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c258f07d5da561%3A0x61f6aa300ba8339d!2sThe+Plaza+Hotel!5e0!3m2!1sen!2s!4v1424421182984" width="100%" height="200" frameborder="0" style="border:0"></iframe></div>
                    <div class="hotel-info">
                        <div class="h-left"><img src="<?php echo base_url()?>public/emailtemplate/img/gall_img1.jpg" /></div>
                        <div class="h-right"><img src="<?php echo base_url()?>public/emailtemplate/img/gall_img8.jpg" /></div>
                        <div class="clear"></div>
                        <div class="hotel-des">
                            <h1>The London bridge</h1>
                            <div class="place"><b>from:</b><span>London</span></div>
                            <div class="price-del"><span>$</span>999</div>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                        </div>
                    </div>
                </div>
                <div class="book"><a href="#">book now</a></div>
            </div>
        </div>
    </body>
</html>
