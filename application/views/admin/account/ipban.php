<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
    <meta charset="utf-8"/>
    <title>IP ADDRESS BAN</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta content="" name="description"/>
    <meta content="" name="author"/>

    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url();?>public/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url();?>public/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url();?>public/assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN THEME STYLES -->
    <link href="<?php echo base_url();?>public/assets/stylesheets/style-metronic.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url();?>public/assets/stylesheets/style.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url();?>public/assets/stylesheets/style-responsive.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url();?>public/assets/stylesheets/plugins.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url();?>public/assets/stylesheets/pages/coming-soon.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url();?>public/assets/stylesheets/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
    <link href="<?php echo base_url();?>public/assets/stylesheets/custom.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url();?>public/countdown/countdown/jquery.countdown.css" rel="stylesheet" type="text/css"/>
    <!-- END THEME STYLES -->
    <link rel="shortcut icon" href="favicon.ico"/>
    <?php
    date_default_timezone_set(EST_AMERICA_TIME_ZONE);
    ?>
    <script>
        var start_date_pt = '<?php echo date('F d, Y H:i:s'); ?>';
        var userid = '<?php echo $userid; ?>';
    </script>
    <script type="text/javascript">
        var baseurl="<?php echo base_url();?>";
        var year = '<?php echo $year;?>';
        var month = '<?php echo $month - 1;?>';
        var date = '<?php echo $date;?>';
        var houres = '<?php echo $houres + 12;?>';
        var min = '<?php echo $min - 60;?>';
        var sec = '<?php echo $sec + 5;?>';
        var difinsec = <?php echo $diffrenceinsec; ?> * 1000;
    </script>
</head>
<body style="background:#fff !important;">
<div class="container">
    <div class="row">
        <div class="col-md-12 coming-soon-header">
            <a class="brand" href="<?php echo base_url();?>">
                <img src="<?php echo base_url();?>public/affilliate_theme/img/logo.png" alt="logo"/>
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 coming-soon-content">
            <h1>Your IP address is BANNED!</h1>
            <p>
                <?php echo $message;?>
            </p>
            <p>
                Thank you for your understanding!
            </p>
            <p>
                www.WHotelsGroup.com
            </p>
            <br>
        </div>
        <div class="col-md-6 coming-soon-countdown">
            <div id="countdown"  style="margin-top:40px"></div>
        </div>
    </div>
</div>
<script src="<?php echo base_url();?>public/assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>public/countdown/countdown/jquery.countdown.js"></script>
<script type="text/javascript">
    $(function(){
        var note = $('#note'),
            ts = new Date(2016 , 7 , 4),
            newYear = true;
        if((new Date()) > ts)
        {
            // The new year is here! Count towards something else.
            // Notice the *1000 at the end - time must be in milliseconds
            ts = (new Date()).getTime() + difinsec;
            newYear = false;
        }

        $('#countdown').countdown({
            timestamp	: ts,
            callback	: function(days, hours, minutes, seconds){

                var message = "";

                if(days <= 0 && hours <= 0 && minutes <= 0 && seconds <= 0)
                {
                    location.reload();
                }

                message += days + " day" + ( days==1 ? '':'s' ) + ", ";
                message += hours + " hour" + ( hours==1 ? '':'s' ) + ", ";
                message += minutes + " minute" + ( minutes==1 ? '':'s' ) + " and ";
                message += seconds + " second" + ( seconds==1 ? '':'s' ) + " <br />";

                if(newYear)
                {
                    message += "left until the new year!";
                }
                else
                {
                    message += "left to 10 days from now!";
                }
                note.html(message);
            }
        });
    });

</script>
</body>
</html>

