
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-user"></i>Sent Quotes
                </div>
                <div class="actions">
                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="sendquote1">
                    <thead>
                        <tr>
                            <th>
                                Date/Time
                            </th>
                            <th>
                                Username
                            </th>
                            <th>
                                Email address
                            </th>
                            <th>
                                Account ID
                            </th>
                            <th>
                                Quote ID
                            </th>

                            <th>
                                Sent by
                            </th>
                            <th>
                                View
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div id="myModal_autocomplete" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog" style="width:80%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">View Quote</h4>
            </div>
            <form method="post" id="add_quoterequests" class="form-horizontal"> <div class="portlet-body form">
                    <div class="form-body">
                        <div class="quoterequest">  
                        </div>
                    </div>
                </div>
            </form> 
        </div>                                               
    </div>
</div>              