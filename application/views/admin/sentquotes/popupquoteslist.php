<div class="row">
    <div class="col-md-4">
        <div class="form-group group_margin">
            <label class="control-label col-md-4">Username:</label>
            <div class="col-md-7">
                <input type="text" class="form-control username" value="<?php echo $quote[0]['var_username']; ?>" readonly="true">
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group group_margin">
            <label class="control-label col-md-4">Check-in:</label>
            <div class="col-md-7">
                <input type="text" class="form-control indate" value="<?php echo $quote[0]['var_checkin']; ?>" readonly="true">
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group group_margin">
            <label class="control-label col-md-4">Rooms:</label>
            <div class="col-md-7">
                <input type="text" class="form-control rooms" value="<?php echo $quote[0]['var_room']; ?>" readonly="true">
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="form-group group_margin">
            <label class="control-label col-md-4">Account ID:</label>
            <div class="col-md-7">
                <input type="text" class="form-control accountid" value="<?php echo $quote[0]['var_accountid']; ?>" readonly="true">
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group group_margin">
            <label class="control-label col-md-4">Check-out:</label>
            <div class="col-md-7">
                <input type="text" class="form-control outcheck" value="<?php echo $quote[0]['var_checkout']; ?>" readonly="true">
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group group_margin">
            <label class="control-label col-md-4">Children:</label>
            <div class="col-md-7">
                <input type="text" class="form-control children" value="<?php echo $quote[0]['var_child']; ?>" readonly="true">
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="form-group group_margin">
            <label class="control-label col-md-4">Points:</label>
            <div class="col-md-7">
                <input type="text" class="form-control point" value="<?php echo $quote[0]['var_points']; ?>" readonly="true">
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group group_margin">
            <label class="control-label col-md-4">City:</label>
            <div class="col-md-7">
                <input type="text" class="form-control city" value="<?php echo $quote[0]['var_city']; ?>" readonly="true">
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group group_margin">
            <label class="control-label col-md-4">Adults:</label>
            <div class="col-md-7">
                <input type="text" class="form-control adults" value="<?php echo $quote[0]['var_adult']; ?>" readonly="true">
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="form-group group_margin">
            <label class="control-label col-md-4">Date of request:</label>
            <div class="col-md-7">
                <input type="text" class="form-control requesteddate" value="<?php echo $quote[0]['dt_reuested_date']; ?>" readonly="true">
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group group_margin">
            <label class="control-label col-md-4">Name of hotel:</label>
            <div class="col-md-7">
                <input type="text" class="form-control hotelname" value="<?php echo $quote[0]['var_hotelname']; ?>" readonly="true">
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group group_margin">
            <label class="control-label col-md-4">Comments:</label>
            <div class="col-md-7">
                <textarea rows="1" class="form-control comment" readonly="true" id="comment"><?php echo $quote[0]['var_comments']; ?></textarea>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="form-group group_margin">
            <label class="control-label col-md-4">Name:</label>
            <div class="col-md-7">
                <input type="text" class="form-control res_name" value="<?php echo $quote[0]['var_nameofreservation']; ?>"  readonly="true">
            </div>
        </div>
    </div>
</div>

<?php for ($i = 0; $i < count($viewquotereply); $i++) { ?> 
    <h4 style="text-align: center"><strong>QUOTE - <?php echo $viewquotereply[$i]['var_uniq_quoteid']; ?></strong></h4>  
    <div class="row">
        <div class="col-md-4">
            <div class="form-group group_margin">
                <label class="control-label col-md-4">Name of hotel:</label>
                <div class="col-md-7">
                    <label class="control-label" style="padding: 6px 16px 0;"><?php echo $viewquotereply[$i]['var_NOH']; ?></label>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group group_margin">
                <label class="control-label col-md-4">Site #1:</label>
                <div class="col-md-5">
                    <label class="control-label" style="padding: 6px 16px 0;"><?php echo $viewquotereply[$i]['var_site1']; ?></label>
                </div>
                <div class="col-md-3">
                    <label class="control-label" style="padding: 6px 16px 0;"><?php echo $viewquotereply[$i]['int_site1_price']; ?></label>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group group_margin">
                <label class="control-label col-md-4">Price quote:</label>
                <div class="col-md-7">
                    <label class="control-label" style="padding: 6px 16px 0;"><?php echo $viewquotereply[$i]['var_prize']; ?></label>
                </div>
            </div>
        </div>


        <!--        <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-4">City:</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" value="<?php echo $viewquotereply[$i]['var_city']; ?>">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-4">Price quote:</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" value="<?php echo $viewquotereply[$i]['var_prize']; ?>">
                        </div>
                    </div>
                </div>-->
    </div>  
    <div class="row">
        <div class="col-md-4">
            <div class="form-group group_margin">
                <label class="control-label col-md-4">Stars:</label>
                <div class="col-md-7">
                    <label class="control-label" style="padding: 6px 16px 0;"><?php echo $viewquotereply[$i]['var_star']; ?></label>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group group_margin">
                <label class="control-label col-md-4">Site #2:</label>
                <div class="col-md-5">
                    <label class="control-label" style="padding: 6px 16px 0;"><?php echo $viewquotereply[$i]['var_site2']; ?></label>
                </div>
                <div class="col-md-3">
                    <label class="control-label" style="padding: 6px 16px 0;"><?php echo $viewquotereply[$i]['int_site2_price']; ?></label>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group group_margin">
                <label class="control-label col-md-4">TripAdvisor:</label>
                <div class="col-md-7">
                    <label class="control-label" style="padding: 6px 16px 0;"><?php echo $viewquotereply[$i]['var_tripadvisor']; ?></label>
                </div>
                <!--                <div class="col-md-3">
                                    <input type="text" placeholder="SRC" class="form-control validate" name="src[]">
                                </div>-->
            </div>
        </div>
        <!--        <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-4">Check-in:</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" value="<?php echo $viewquotereply[$i]['var_indate']; ?>">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-4">Site #1:</label>
                        <div class="col-md-3">
                            <select class="form-control select2me">
                                <option><?php echo $viewquotereply[$i]['var_site1']; ?></option>                    
                            </select>
                        </div>
                        <div class="col-md-3">
                            <input type="text" class="form-control" value="<?php echo $viewquotereply[$i]['int_site1_price']; ?>">
                        </div>
                    </div>
                </div>-->
    </div>                              
    <div class="row">
        <div class="col-md-4">
            <div class="form-group group_margin">                                                                       
                <label class="control-label col-md-4">Cancellation policy:</label>
                <div class="col-md-7">
                    <label class="control-label" style="padding: 6px 16px 0;"><?php echo $viewquotereply[$i]['var_policy']; ?></label>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group group_margin">
                <label class="control-label col-md-4">SRC</label>
                <div class="col-md-5">
                    <label class="control-label" style="padding: 6px 16px 0;"><?php echo $viewquotereply[$i]['var_src']; ?></label>
                </div>
                <div class="col-md-3">
                    <!--<input type="text" class="form-control validate" name="s_rc[]">-->
                      <label class="control-label" style="padding: 6px 16px 0;"><?php echo $viewquotereply[$i]['int_src_price']; ?></label>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group group_margin">
                <label class="control-label col-md-4">Comments:</label>
                <div class="col-md-7">
                    <label class="control-label" style="padding: 6px 16px 0;"><?php echo $viewquotereply[$i]['var_comment']; ?></label>
                </div>
            </div>
        </div>
        <!--        <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-4">Check-out:</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" value="<?php echo $viewquotereply[$i]['var_outdate']; ?>">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-4">Site #2:</label>
                        <div class="col-md-3">
                            <input type="text" class="form-control" value="<?php echo $viewquotereply[$i]['var_site2']; ?>">
                        </div>
                        <div class="col-md-3">
                            <input type="text" class="form-control" value="<?php echo $viewquotereply[$i]['int_site2_price']; ?>">
                        </div>
                    </div>
                </div>-->
    </div>
    <!--    <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-4">Rooms:</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" value="<?php echo $viewquotereply[$i]['int_room']; ?>">
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-4">TripAdvisor:</label>
                    <div class="col-md-3">
                        <input type="text" class="form-control" value="<?php echo $viewquotereply[$i]['var_tripadvisor']; ?>">
                    </div>
                    <label class="control-label col-md-1">SRC:</label>
                    <div class="col-md-2">
                        <input type="text" class="form-control" value="<?php echo $viewquotereply[$i]['var_src']; ?>">
                    </div>
                </div>
            </div>
        </div>                               
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-4">Adults:</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" value="<?php echo $viewquotereply[$i]['int_adult']; ?>">
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-4">Children:</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" value="<?php echo $viewquotereply[$i]['int_child']; ?>">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-4">Name of hotel:</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" value="<?php echo $viewquotereply[$i]['var_NOH']; ?>">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-4">Stars:</label>
                    <div class="col-md-6">
                        <select class="form-control select2me">
                            <option selected="selected"><?php echo $viewquotereply[$i]['var_star']; ?></option>                    
                        </select>                                                                            
                    </div>
                </div>
            </div>
        </div>                            
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">                                                                       
                    <label class="control-label col-md-4">Cancellation policy:</label>
                    <div class="col-md-8">
                        <select class="form-control select2me">
                            <option selected="selected"><?php echo $viewquotereply[$i]['var_policy']; ?></option>
                        </select>
                        </select>
                    </div>
                </div>
            </div>
        </div>                               
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label col-md-2">Comments:</label>
                    <div class="col-md-9">
                        <textarea rows="1" class="form-control"><?php echo $viewquotereply[$i]['var_comment']; ?></textarea>
                    </div>
                </div>
            </div>
        </div>     -->
<?php } ?>  