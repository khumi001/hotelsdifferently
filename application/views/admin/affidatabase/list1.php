<?php
$i = 1;
?>
<style>
    .affiliate_form .col-md-4 {
  width: 30%;
}
.affiliate_form .control-label {
  text-align: left;
   font-size: 13px;
}
 
</style>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-user"></i>Affiliate Database
                </div>
                <div class="actions">
<!--               <a class="btn red" href="#add_affiliate_detail" data-toggle="modal">
                    <i class="fa fa-pencil"></i>
                    Add
                    </a>-->
                </div>
            </div>
            <div class="portlet-body">

                <table class="table table-striped table-bordered table-hover" id="sample_2">
                    <thead>
                        <tr>
                            <th>
                                Account ID
                            </th>
                            <th>
                                Email address
                            </th>
                            <th>
                                Full name
                            </th>
                            <th>
                                PayPal
                            </th>
                            <th>
                                Country
                            </th>

                            <th>
                                View
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($affiliate_details as $row){
                        ?>
                        <tr>
                            <td>
                                <?php echo $row['var_accountid'];?>
                            </td>
                            <td>
                               <?php echo $row['var_email'] ?>
                            </td>
                            <td>
                               <?php echo $row['var_fname']." ".$row['var_lname']; ?>
                            </td>
                            <td>
                                <?php echo $row['var_paypalemail']; ?>
                            </td>
                            <td>
                                <?php echo $row['var_country'];  ?>
                            </td>
                            <td>
                                <a href="#myModal_autocomplete" id="btn_affiliatereq" data-id="<?php echo $row['int_glcode']; ?>" data-toggle="modal" class="btn default btn-xs blue btn_affiliatereq">View</a>
                            </td>

                        </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
            <form method="post" id="affiliate_database" class="form-horizontal">
                <div id="myModal_autocomplete" class="modal fade" role="dialog" aria-hidden="true">
                    <div class="modal-dialog" style="width:70%">
                        <div class="modal-content">
                            <div class="modal-header" style="padding: 6px 15px;">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="margin-top:8px !important;"></button>
                                <h4 class="modal-title">Affiliate Information</h4>
                            </div>
                            <div class="portlet-body form affiliate_form">
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label col-md-6 bold">First name:</label>
                                                <div class="col-md-6 no-padding">
                                                     <label class="control-label col-md-12 no-padding" id="firstname1"></label>
                                                    <input type="text" class="form-control hidden" placeholder="First Name" name="firstname" id="firstname" >
                                                    <input type="hidden" name="req_id" id="req_id" class="form-control" Readonly>
                                                </div>      
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label col-md-6 bold">Last Name:</label>
                                                <div class="col-md-6">
                                                    <label class="control-label col-md-12 no-padding" id="lastname1"></label>
                                                    <input type="text" class="form-control hidden" placeholder="Last Name" name="lastname" id="lastname" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label col-md-6 bold">Email Address:</label>
                                                <div class="col-md-6">
                                                     <label class="control-label col-md-12 no-padding" id="email1"></label>
                                                    <input type="text" class="form-control hidden" placeholder="Email Address" name="email" id="email" >
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                   
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label col-md-6 bold">Company Name:</label>
                                                <div class="col-md-6 no-padding">
                                                    <label class="control-label col-md-12 no-padding" id="companyname1"></label>
                                                    <input type="text" class="form-control hidden" placeholder="Company Name" name="companyname" id="companyname" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label col-md-6 bold">Title:</label>
                                                <div class="col-md-6">
                                                    <label class="control-label col-md-12 no-padding" id="title1"></label>
                                                    <input type="text" class="form-control hidden" placeholder="Title" name="title" id="title" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label col-md-6 bold">Country:</label>
                                                <div class="col-md-6">
                                                    <label class="control-label col-md-12 no-padding" id="country1"></label>
                                                    <input type="text" class="form-control hidden" placeholder="Country" name="country" id="country" >
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/row-->
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label col-md-6 bold">State:</label>
                                                <div class="col-md-6 no-padding">
                                                     <label class="control-label col-md-12 no-padding" id="state1"></label>
                                                    <input type="text" class="form-control hidden" placeholder="State" name="state" id="state" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label col-md-6 bold">ZIP:</label>
                                                <div class="col-md-6">
                                                    <label class="control-label col-md-12 no-padding" id="zip1"></label>
                                                    <input type="text" class="form-control hidden" placeholder="ZIP" name="zip" id="zip" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label col-md-6 bold">Address1:</label>
                                                <div class="col-md-6">
                                                    <label class="control-label col-md-12 no-padding" id="address1"></label>
                                                    <input type="text" class="form-control hidden" placeholder="Address" name="address" id="address" >
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                      
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label col-md-6 bold">Address2:</label>
                                                <div class="col-md-6 no-padding">
                                                    <label class="control-label col-md-12 no-padding" id="address21"></label>
                                                    <input type="text" class="form-control hidden" placeholder="Address" name="address2" id="address2" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label col-md-6 bold">Website/Newsletter name:</label>
                                                <div class="col-md-6">
                                                    <label class="control-label col-md-12no-padding" id="newsletter1"></label>
                                                    <input type="text" class="form-control hidden" placeholder="Website name" name="newsletter" id="newsletter" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label col-md-6 bold">Website URl:</label>
                                                <div class="col-md-6">
                                                    <label class="control-label col-md-12 no-padding" id="wesiteurl1"></label>
                                                    <input type="text" class="form-control hidden" placeholder="Website URl" name="wesiteurl" id="wesiteurl" >
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label col-md-6 bold">Site description:</label>
                                                <div class="col-md-6 no-padding">
                                                     <label class="control-label col-md-12 no-padding" id="sitedesc1"></label>
                                                    <input type="text" class="form-control hidden" placeholder="Site description" name="sitedesc" id="sitedesc" >
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label col-md-6 bold">Monthly unique visitors:</label>
                                                <div class="col-md-6">
                                                    <label class="control-label col-md-12 no-padding" id="visitors1"></label>
                                                    <input type="text" class="form-control hidden" placeholder="Monthly unique visitors" name="visitors" id="visitors">
                                                </div>
                                            </div>
                                        </div>
                                          <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label col-md-6 bold">Phone Number:</label>
                                                <div class="col-md-6">
                                                    <label class="control-label col-md-12 " id="phone1"></label>
                                                    <input type="text" class="form-control hidden" placeholder="Phone Number" name="phone" id="phone" >
                                                </div>
                                            </div>
                                        </div>
                                       
                                    </div>
                                    <div class="westranunion" style="display: none;">
                                        <div class="row">
                                           <div class="col-md-4">
                                               <div class="form-group">
                                                   <label class="control-label col-md-6 bold">Preffered payment method:</label>
                                                   <div class="col-md-6 no-padding">
                                                       <label class="control-label col-md-12 no-padding paymentmethod1" ></label>
                                                       <input type="text" class="form-control hidden paymentmethod" placeholder="Preffered payment method" name="paymentmethod"  >
                                                   </div>
                                               </div>
                                           </div>
                                            
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label col-md-6 bold">Full name:</label>
                                                <div class="col-md-6">
                                                    <label class="control-label col-md-12 no-padding" id="ppfullname1"></label>
                                                    <input type="text" class="form-control hidden" placeholder="Full name" name="ppfullname" id="ppfullname">
                                                </div>
                                            </div>
                                        </div>
                                          <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label col-md-6 bold">Phone Number:</label>
                                                <div class="col-md-6">
                                                    <label class="control-label col-md-12 " id="ppphone1"></label>
                                                    <input type="text" class="form-control hidden" placeholder="Phone Number" name="ppphone" id="ppphone" >
                                                </div>
                                            </div>
                                        </div>
                                       
                                      </div>
                                        <div class="row">
                                           <div class="col-md-4">
                                               <div class="form-group">
                                                   <label class="control-label col-md-6 bold">City:</label>
                                                   <div class="col-md-6 no-padding">
                                                       <label class="control-label col-md-12 no-padding" id="ppcity1"></label>
                                                       <input type="text" class="form-control hidden" placeholder="City" name="ppcity" id="ppcity" >
                                                   </div>
                                               </div>
                                           </div>
                                            
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label col-md-6 bold">State:</label>
                                                <div class="col-md-6">
                                                    <label class="control-label col-md-12 no-padding" id="ppstate1"></label>
                                                    <input type="text" class="form-control hidden" placeholder="State" name="ppstate" id="ppstate">
                                                </div>
                                            </div>
                                        </div>
                                          <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label col-md-6 bold">Country:</label>
                                                <div class="col-md-6">
                                                    <label class="control-label col-md-12 " id="ppcountry1"></label>
                                                    <input type="text" class="form-control hidden" placeholder="Country" name="ppcountry" id="ppcountry" >
                                                </div>
                                            </div>
                                        </div>
                                       
                                      </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                               <div class="form-group">
                                                   <label class="control-label col-md-6 bold">Amount:</label>
                                                   <div class="col-md-6 no-padding">
                                                       <label class="control-label col-md-12 no-padding ppamount1" ></label>
                                                       <input type="text" class="form-control hidden ppamount" placeholder="Amount" name="ppamount" >
                                                   </div>
                                               </div>
                                           </div>
                                        </div>
                                    </div>
                                    <div class="paypal" style="display: none;">
                                        <div class="row">
                                           <div class="col-md-4">
                                               <div class="form-group">
                                                   <label class="control-label col-md-6 bold">Preffered payment method:</label>
                                                   <div class="col-md-6 no-padding">
                                                       <label class="control-label col-md-12 no-padding paymentmethod1" ></label>
                                                       <input type="text" class="form-control hidden paymentmethod" placeholder="Preffered payment method" name="paymentmethod"  >
                                                   </div>
                                               </div>
                                           </div>
                                            <div class="col-md-4">
                                               <div class="form-group">
                                                   <label class="control-label col-md-6 bold">Paypal Email:</label>
                                                   <div class="col-md-6 ">
                                                       <label class="control-label col-md-12 no-padding" id="paypalemail1"></label>
                                                       <input type="text" class="form-control hidden" placeholder="Paypal Email" name="paypalemail" id="paypalemail" >
                                                   </div>
                                               </div>
                                           </div>
                                            <div class="col-md-4">
                                               <div class="form-group">
                                                   <label class="control-label col-md-6 bold">Amount:</label>
                                                   <div class="col-md-6 ">
                                                       <label class="control-label col-md-12 no-padding ppamount1" ></label>
                                                       <input type="text" class="form-control hidden ppamount" placeholder="City" name="ppamount" >
                                                   </div>
                                               </div>
                                           </div>
                                      </div>
                                    </div>
                                </div>
                                <!-- END FORM-->
                            </div>
                            <!--</div>-->
                            <!--</div>-->
                            <div class="modal-footer" style="text-align: center; margin-top: 0px; padding: 10px 20px;">
                                <button type="button" class="btn btn-default blue editbtn" id="btn">EDIT</button>
                                <!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
                                <button type="button" id="btn_affdb" class="btn green hidden"><i class="fa fa-check"></i>Update</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
<!--Add Affie-->

