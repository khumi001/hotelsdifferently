<html lang="en" class="no-js">
    <!--<![endif]-->    
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>

        <title>Quote Requests</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <meta content="Quote Requests" name="description"/>
        <meta content="Quote Requests" name="keywords"/>

        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>public/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>public/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>public/assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
        <!-- END GLOBAL MANDATORY STYLES -->

        <!-- Begin From Controller-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/assets/plugins/select2/select2.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/assets/plugins/select2/select2-metronic.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/assets/plugins/bootstrap-toastr/toastr.min.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/assets/plugins/data-tables/DT_bootstrap.css"/>

        <!-- Fancy Box CSS --> 
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/assets/plugins/fancybox/source/jquery.fancybox.css" />
        <!-- Fancy Box CSS --> 
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script>
    <div id="header-css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>public/assets/plugins/bootstrap-select/bootstrap-select.min.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>public/assets/plugins/select2/select2.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>public/assets/plugins/select2/select2-metronic.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>public/assets/plugins/data-tables/DT_bootstrap.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>public/assets/plugins/jquery-multi-select/css/multi-select.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>public/assets/plugins/jquery-ui/jquery-ui.css" />

    </div>
    <!-- End From Controller -->

    <!-- BEGIN THEME STYLES -->
    <link href="<?php echo base_url(); ?>public/assets/plugins/gritter/css/jquery.gritter.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url(); ?>public/assets/stylesheets/style-metronic.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url(); ?>public/assets/stylesheets/style.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url(); ?>public/assets/stylesheets/style-responsive.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url(); ?>public/assets/stylesheets/plugins.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url(); ?>public/assets/stylesheets/pages/tasks.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url(); ?>public/assets/stylesheets/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
    <link href="<?php echo base_url(); ?>public/assets/stylesheets/print.css" rel="stylesheet" type="text/css" media="print"/>
    <link href="<?php echo base_url(); ?>public/assets/stylesheets/custom.css" rel="stylesheet" type="text/css"/>
    <!-- END THEME STYLES -->
    <script type="text/javascript">
        var baseurl = "<?php echo base_url(); ?>";
    </script>
    <link rel="shortcut icon" href="favicon.ico"/>
    <style>
        .form-group{
            margin: 0px;
        }
		.portlet-body.form.affiliate_form {
  padding: 20px 0 !important; background:#fff;
}
.modal-header {
  background: none repeat scroll 0 0 #2461A8;
  border-bottom: 1px solid #000;
  color: #cccccc; padding:0 10px 5px 10px !important;
  font-size: 36px;
}
.modal-header  h4 {
  font-size: 22px; color:#fff;
  
}
    </style>
</head>
<!-- END HEAD -->    
<body class="page-header-fixed"> 
    <div class="modal-dialog" style="width:100%">
        <?php $i = 1; foreach ($affilateinfo as $row) { ?>
            <div class="modal-content">
                <div class="modal-header" style="padding: 6px 15px;">
                    <!--<button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="margin-top:8px !important;"></button>-->
                    <h4 class="modal-title"><?php echo $row['fname']; ?> &nbsp;&nbsp;&nbsp; <?php echo $row['lname']; ?></h4>
                </div>
                <div class="portlet-body form affiliate_form">
                    <div class="form-body"><?php // print_r($affilateinfo);exit;   ?>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group" style="margin: 0px;">
                                    <label class="control-label col-md-5 bold"  style="font-size: 10px;">Name: </label><label style="font-size: 10px;width: 300px;"><?php echo $row['fname']; ?> &nbsp;&nbsp;&nbsp; <?php echo $row['lname'] ?></label>
                                    <label class="control-label col-md-5 bold"  style="font-size: 10px;">Website/Newsletter name: </label><label style="font-size: 10px;width: 200px;"><?php echo $row['newsletter']; ?></label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-5 bold"  style="font-size: 10px;">Email Address: </label><label style="font-size: 10px;width: 260px;"><?php echo $row['email']; ?></label>
                                    <label class="control-label col-md-5 bold"  style="font-size: 10px;">Website URl: </label><label style="font-size: 10px;"><?php echo $row['websiteurl']; ?></label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-5 bold"  style="font-size: 10px;">Company Name: </label><label style="font-size: 10px;width: 250px;"><?php echo $row['company']; ?></label>
                                    <label class="control-label col-md-5 bold"  style="font-size: 10px;">Site description: </label><label style="font-size: 10px;"><?php echo $row['sitedescription']; ?></label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-5 bold"  style="font-size: 10px;">Title: </label><label style="font-size: 10px;width: 305px;"><?php echo $row['title']; ?></label>
                                    <label class="control-label col-md-5 bold"  style="font-size: 10px;">Monthly unique visitors: </label><label style="font-size: 10px;"><?php echo $row['visitors']; ?></label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-5 bold"  style="font-size: 10px;">Address1: </label>
                                    <label style="font-size: 10px;width: 280px;"><?php echo $row['address1']; ?></label>
                                    <label class="control-label col-md-5 bold"  style="font-size: 10px;">Payment Information: </label>
                                </div>
                                <div class="form-group">
                                    <label style="font-size: 10px;padding-left: 81px !important;"><?php echo $row['address2']; ?></label>
                                    <br/>
                                    <label style="font-size: 10px;padding-left: 81px !important;"><?php echo $row['city']; ?></label>
                                    <br/>
                                    <label style="font-size: 10px;padding-left: 81px !important;"><?php echo $row['country']; ?></label>
                                    
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-5 bold"  style="font-size: 10px;">Phone Number: </label>
                                    <label style="font-size: 10px;width: 250px;"><?php echo $row['phone']; ?></label>                                                
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-5 bold"  style="font-size: 10px;">Preffered payment method: </label>
                                    <label style="font-size: 10px;width: 305px;"><?php echo $row['payment']; ?></label>
                                     <?php if ($row['payment'] == 'westerrn_union') { ?>
                                    <div class="western" style="margin-top: -90px;">     
                                        <div class="form-group">
                                            <label class="control-label col-md-5 bold"  style="font-size: 10px;padding-left:400px !important;">Full name: </label>
                                            <label style="font-size: 10px;"><?php echo $row['fullname']; ?></label>
                                        </div><div class="form-group">
                                            <label class="control-label col-md-5 bold"  style="font-size: 10px;padding-left:400px !important;">Phone Number: </label>
                                            <label style="font-size: 10px;"><?php echo $row['phonenumber']; ?></label>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-5 bold"  style="font-size: 10px;padding-left:400px !important;">City:</label>
                                            <label style="font-size: 10px;"><?php echo $row['phonenumber']; ?></label>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-5 bold"  style="font-size: 10px;padding-left:400px !important;">State: </label>
                                            <label style="font-size: 10px;"><?php echo $row['state']; ?></label>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-5 bold"  style="font-size: 10px;padding-left:400px !important;">Country:</label>
                                            <label style="font-size: 10px;"><?php echo $row['countrypayment']; ?></label>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-5 bold"  style="font-size: 10px;padding-left:400px !important;">Amount: </label>
                                            <label style="font-size: 10px;"><?php echo $row['money']; ?></label>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php if ($row['payment'] == 'paypal' && $row['paypalemail']!="") { ?>
                                    <div class="paypalemail" style="margin-top: -90px;">  
                                        <div class="form-group">
                               
                                            <label class="control-label col-md-5 bold"  style="font-size: 10px;padding-left:400px !important;">Paypal Email: </label>
                                            <label style="font-size: 10px;"><?php echo $row['paypalemail'] ?></label>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-5 bold"  style="font-size: 10px;padding-left:400px !important;">Amount: </label>
                                            <label style="font-size: 10px;"><?php echo $row['money']; ?></label>
                                        </div>
                                    </div>
                                <?php } ?>
                                </div>
                                <?php //echo  $row['fullname'].'--';?>
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        
        <?php if(($i % 3) == 0)
              {
                echo '<div style="page-break-before:always">&nbsp;</div>';
              }
            
            $i++; } ?>
        <!-- END FORM-->
    </div>


</body>
</html>