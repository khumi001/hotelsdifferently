<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-reorder"></i>IP Address Ban
        </div>

    </div>
    <div class="portlet-body form">
        <form action="#" class="form-horizontal" id="addipban">
            <div class="form-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="radio-list">
                                <label class="col-md-6 control-label radio-inline">
                                    <input type="radio" class="radiobtn" name="optionsRadios" id="optionsRadios25" value="option1" checked>Ban By Ip Address</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" id="ban_by_ip" name="ban_by_ip" placeholder="Enter ip address">
                                </div>
                            </div>
                        </div>
                    </div>
                     <div class="col-md-3">
                        <div class="form-group">
                            <div class="col-md-9" >
                               <select class="form-control select2me  input-xmedium site validate bantime" name="ban_time">
                                    <option value="">--Select--</option>
                                    <option value="1">1 Day</option>
                                    <option value="7">1 Week</option>
                                    <option value="30">1 Month</option>
                                    <option value="1825">Forever</option>
                                </select> 
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <div class="col-md-9" >
                                <textarea class="form-control reason" rows="2" placeholder="Enter Reason" style="position:absolute; height: 80px; margin-left:-65px;" name="reason"></textarea>
                            </div>
                            <div class="col-md-12">
                                <label class="col-md-10 control-label"></label>
                                <?php
                                if(!isSkipLogin())
                                {
                                ?>
                                <button type="submit" class="btn submit red" style="margin-top:20px;position:absolute; margin-left: -30px">Ban</button>
                                <?php }?>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group" >
                            <div class="radio-list">
                                <label class="col-md-6 control-label radio-inline">
                                    <input type="radio" class="radiobtn" name="optionsRadios" id="optionsRadios25" value="option2" >Ban by Account ID or Email</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control"  id="ban_by_acc" name="ban_by_acc" placeholder="Enter Account Id/Username/Email address">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>				
            </div>
        </form>
    </div>
</div>