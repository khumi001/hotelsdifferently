<form class="form-horizontal" action="<?php echo base_url(); ?>admin/Booking_api/ShowCruiseSearchResult" role="form">
    <div class="form-group">
        <label class="control-label col-sm-2" for="email">Location:</label>
        <div class="col-sm-10">
            <select name="cruiseDestination" id="cruiseDestination" class="form-control">
                <?php
                    foreach($cruise_destination_list as $a_cruse_destination)
                    {
                        echo "<option value='{$a_cruse_destination['tourico_destination_id']}'>{$a_cruse_destination['destination_name']}</option>";
                    }
                ?>
                
            </select>
        </div>
    </div>
    <div class="form-group"> 
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-default">Submit</button>
        </div>
    </div>
</form>