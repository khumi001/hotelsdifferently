<form method='post'>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Thumbnail</th>
                    <th>Cruise name</th>
                    <th>Shipname</th>
                    <th>Price Multiplier</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
        <?php
            foreach( $cruise_list as $aCruise )
            {
                $multiplierInPercentage = "Not Set";
                if($aCruise['multiplier_in_percentage']!=null)
                    $multiplierInPercentage = $aCruise['multiplier_in_percentage'] . "%";
                
                echo "<tr>
                        <td><img src='{$aCruise['ship_image']}'></td>
                        <td>{$aCruise['cruise_itinerary_name']}</td>
                        <td>{$aCruise['ship_name']}</td>
                        <td id='percentile-{$aCruise['cruise_itinerary_id']}'>{$multiplierInPercentage}</td>
                        <td>
                            <a href='javascript:void(0);' data-cruise-id='{$aCruise['cruise_itinerary_id']}' data-cruise-name='{$aCruise['cruise_itinerary_name']}' data-ship-image-url='{$aCruise['ship_image']}' data-ship-name='{$aCruise['ship_name']}' class='updateCruisePrice'>Update</a>
                        </td>
                    </tr>";
            }
        ?>
            </tbody>
        </table>
        <input type='hidden'  name='location_name' id='location_name' value='<?php echo $location_name; ?>'>
    </form>
    
    
    <div class="modal fade" id="cruisePriceSetModal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id='hotelPriceSetModalTitle'>Modal title</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" role="form" id="setPercentilePriceForm">
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="profitPercentage">Percentile(%):</label>
                            <div class="col-sm-10">
                                <input type="number" class="form-control" id="profitPercentage" name="profitPercentage" placeholder="Enter Percentage">
                            </div>
                        </div>
                        
                        <input type='hidden' name="cruiseName" id="cruiseName" value="">
                        <input type='hidden' name="shipName" id="shipName" value="">
                        <input type='hidden' name="cruiseId" id="cruiseId" value="">
                        <input type='hidden' name="shipImageUrl" id="shipImageUrl" value="">
                        <input type='hidden' name="touricoDestinationName" id="touricoDestinationName" value="<?php echo $location_name; ?>">
                        
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="savePercentile">Save changes</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->