    <template id='BCTemplate'>
        <form method='post' id='setAllHotelPriceForm'style="text-align: right;margin-right: 0px;">
            <input type='hidden' name='hotelIdList' id='hotelIdList' value='<?php echo json_encode($hotel_id_list);  ?>'>
            <input type='text' name='allHotelMultiplier' id='allHotelMultiplier'>
            <input type='button' id='setAllHotelPrice' value='Update All Price'>
        </form>
    </template>
    <form method='post'>
        <table class="table table-bordered" id='hotelSearchResultList'>
            <thead>
                <tr>
                    <th>Hotel name</th>
                    <th>Hotel Multiplier</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
        <?php
            foreach( $hotel_list as $aHotel )
            {
                $multiplierInPercentage = "Not Set";
                $multiplierForModal = "";
                if($aHotel['multiplier_in_percentage']!=null)
                {
                    $multiplierInPercentage = $aHotel['multiplier_in_percentage'] . "%";
                    $multiplierForModal = $aHotel['multiplier_in_percentage']*100;
                }
                
                echo "<tr>
                        <td>{$aHotel['hotelName']}</td>
                        <td id='percentile-{$aHotel['hotelId']}'>{$multiplierInPercentage}</td>
                        <td>
                            <a href='javascript:void(0);' data-hotel-id='{$aHotel['hotelId']}' data-hotel-name='{$aHotel['hotelName']}' data-hotel-current-multiplier='{$multiplierForModal}' data-hotel-image-url='{$aHotel['hotelThumbUrl']}' class='updateHotelPrice'>Update</a>
                        </td>
                    </tr>";
            }
        ?>
            </tbody>
        </table>
        <input type='hidden'  name='location_name' id='location_name' value='<?php echo $location_name; ?>'>
    </form>
    
    <div class="modal fade" id="hotelPriceSetModal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id='hotelPriceSetModalTitle'>Modal title</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" role="form" id="setPercentilePriceForm">
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="profitPercentage">Percentile(%):</label>
                            <div class="col-sm-10">
                                <input type="number" class="form-control" id="profitPercentage" name="profitPercentage" placeholder="Enter Percentage">
                            </div>
                        </div>
                        
                        <input type='hidden' name="hotelName" id="hotelName" value="">
                        <input type='hidden' name="hotelId" id="hotelId" value="">
                        <input type='hidden' name="hotelImageUrl" id="hotelImageUrl" value="">
                        <input type='hidden' name="touricoDestinationName" id="touricoDestinationName" value="<?php echo $location_name; ?>">
                        
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="savePercentile">Save changes</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->