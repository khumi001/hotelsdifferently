<?php
?>
<form method='post'>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>Thumbnail</th>
                <th>Hotel name</th>
                <th>Hotel Location</th>
                <th>Percentile</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
    <?php
        foreach( $hotel_list as $aHotel )
        {
            echo "<tr>
                      <td><img src='{$aHotel['hotel_image_url']}'></td>
                      <td>{$aHotel['hotel_name']}</td>  
                      <td>{$aHotel['tourico_destination_name']}</td>
                      <td id='percentile-{$aHotel['tourico_hotel_id']}'>{$aHotel['multiplier_in_percentage']}</td> 
                      <td>
                            <a href='javascript:void(0);' data-hotel-location-name='{$aHotel['tourico_destination_name']}' data-hotel-id='{$aHotel['tourico_hotel_id']}' data-hotel-name='{$aHotel['hotel_name']}' data-hotel-image-url='{$aHotel['hotel_image_url']}' class='updateHotelPrice'>Update</a>
                        </td>
                </tr>";
        }
    ?>
        </tbody>
    </table>
    </form>
    
    
    <div class="modal fade" id="hotelPriceSetModal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id='hotelPriceSetModalTitle'>Modal title</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" role="form" id="setPercentilePriceForm">
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="profitPercentage">Percentile(%):</label>
                            <div class="col-sm-10">
                                <input type="number" class="form-control" id="profitPercentage" name="profitPercentage" placeholder="Enter Percentage">
                            </div>
                        </div>
                        
                        <input type='hidden' name="hotelName" id="hotelName" value="">
                        <input type='hidden' name="hotelId" id="hotelId" value="">
                        <input type='hidden' name="hotelImageUrl" id="hotelImageUrl" value="">
                        <input type='hidden' name="touricoDestinationName" id="touricoDestinationName" value="">
                        
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="savePercentile">Save changes</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->