<?php
?>
<form method='post'>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>Location Name</th>
                <th>Percentile</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
    <?php
        foreach( $car_destination_list as $aCarDestinationList )
        {
            echo "<tr>
                    <td>{$aCarDestinationList['destination_name']}</td>
                    <td id='percentile-{$aCarDestinationList['tourico_destination_id']}'>{$aCarDestinationList['multiplier_in_percentage']}</td>
                    <td>
                        <a href='javascript:void(0);' class='updateCarLocationPercentile' data-destination-id='{$aCarDestinationList['tourico_destination_id']}'>Update</a>
                    </td>
                </tr>";
        }
    ?>
        </tbody>
    </table>
    </form>
    
    
    <div class="modal fade" id="carPriceSetModal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id='hotelPriceSetModalTitle'>Modal title</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" role="form" id="setPercentilePriceForm">
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="profitPercentage">Percentile(%):</label>
                            <div class="col-sm-10">
                                <input type="number" class="form-control" id="profitPercentage" name="profitPercentage" placeholder="Enter Percentage">
                            </div>
                        </div>
                        
                        <input type='hidden' name="carLocationId" id="carLocationId" value="">
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="savePercentile">Save changes</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->