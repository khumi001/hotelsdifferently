<form class="form-horizontal" action="<?php echo base_url(); ?>admin/Booking_api/AddCarLocationPrice" role="form">
    <div class="form-group">
        <label class="control-label col-sm-2" for="email">Location:</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="locationName" name="locationName" placeholder="Enter Location">
            <input type="hidden" name="locationId" id="locationId" value="">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="email">Percentile:</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="carLocationPercentile" name="carLocationPercentile" placeholder="Enter Car Location Percentile">
        </div>
    </div>
    <div class="form-group"> 
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-default">Submit</button>
        </div>
    </div>
</form>