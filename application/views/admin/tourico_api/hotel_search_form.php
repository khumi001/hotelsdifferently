<form class="form-horizontal" action="<?php echo base_url(); ?>admin/booking_api/ShowHotelSearchResult" role="form">
    <div class="form-group">
        <label class="control-label col-sm-2" for="email">Location:</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="locationName" name="locationName" placeholder="Enter Location">
            <input type="hidden" name="locationId" id="locationId" value="">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="email">Hotel name:</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="hotelName" name="hotelName" placeholder="Enter hotel name">
        </div>
    </div>
    <div class="form-group"> 
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-default">Submit</button>
        </div>
    </div>
</form>
<?php 
	$attr = array(
		"class" => "form-horizontal"
	);
echo form_open('admin/booking_api/ShowHotelSearchForm' , $attr); ?>
	<div class="text-center">
		<label>UPDATE PRICING ON ALL HOTELS</label>
	</div>
	<div class="form-group">
        <label class="control-label col-sm-2" for="priceper">Price %:</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" value="<?php echo $pprice;  ?>" id="priceper" name="priceper" placeholder="Percentage">
        </div>
    </div>
    <div class="form-group"> 
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-default">Submit</button>
        </div>
    </div>
<?php echo form_close(); ?>
