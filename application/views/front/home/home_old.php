<style>
        .header-logo-sec img {
    display: block;
    margin: 0 auto;
    width: 100%;
    height: auto;
}
.tlsw_suggestion{
    border-bottom:1px solid rgba(238, 238, 238, 0.35);
}
.tlsw_suggestion:last-child{
    border-bottom:0px;
}
#tlsw_content{
    border-bottom:1px solid rgba(238, 238, 238, 0.35);
}
#tlsw_footer{
    background-color: rgba(16, 44, 63, 0.9);
}
#tlsw_header{
    height: 90px;
}
#tlsw_header input{
    width: calc(86% - 69px);
    height: 40px;

}
#tlsw_header #tlsw_searchBtn{
    width: 17%;
    height: 40px
}
.events-srch{
    background: rgba(0, 0, 0, 0) linear-gradient(to bottom, #71d1ff 0%, #22b3f4 4%, #198fef 100%) repeat scroll 0 0;
    border: medium none;
    box-shadow: 0 1px 3px rgba(0, 0, 0, 0.4);
    color: #fff;
    font-family: "FuturaStd-Book";
    font-size: 16px;
    text-shadow: 1px 1px 2px #000;
    text-transform: uppercase;
}
</style>
<?php
date_default_timezone_set('America/Los_Angeles');
?>
<script>var start_date_pt = '<?php echo date('F d, Y H:i:s'); ?>';var city_array = new Array;</script>

<div class="home_container">
        <div class="header-logo-sec">
        <img src="<?php echo base_url(); ?>public/assets/img/header-logo.png" class="img-responsive" alt="no-follow"/>
        
        </div>
        
	<div class="width-row display_off padding-10-0-0-8">
		<div itemscope itemtype="http://schema.org/Person">
		   <span itemprop="name">Wholesale Hotels Group</span>
		   <span itemprop="company">Wholesale Hotels Group</span>
		   <span itemprop="tel">888-287-2307</span>
		</div>
	</div>
    <div class="width-row">
        <?php include('main_uper.php'); ?>
        <div role="tabpanel" id="searchTabs">
            <ul class="nav nav-tabs nav-justified" role="tablist">
				<li role="presentation">
                    <a href="#flights" data-target="#flights" aria-controls="flights" role="tab" data-toggle="tab">FLIGHTS</a>
                </li>
                <li role="presentation" class="active">
                    <a href="#hotelSearch" data-target="#hotelSearch" aria-controls="hotelSearch" role="tab" data-toggle="tab">HOTELS</a>
                </li>
                <!-- <li role="presentation">
                    <a href="#cars" data-toggle="tooltip" data-placement="top" title="Coming soon!" aria-controls="cars" role="tab">CARS</a>
                </li>
                <li role="presentation">
                    <a href="#cruises" data-toggle="tooltip" data-placement="top" title="Coming soon!" aria-controls="cruises" role="tab">CRUISES</a>
                </li> -->
                <li role="presentation">
                    <a href="#events" data-target="#events" data-toggle="tab" data-placement="top" aria-controls="events" role="tab">EVENTS</a>
                </li>
                <li role="presentation">
					<a href="#activities" data-toggle="tooltip" data-placement="top" title="Coming soon!" aria-controls="activities" role="tab">ACTIVITIES</a>
				</li>
            </ul>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js" ></script>
            <script type="text/javascript">$(document).ready(function () {$('[data-toggle="tooltip"]').tooltip();});</script>
            <div class="tab-content">
				<div role="tabpanel" class="tab-pane" id="flights">
                    <form class="form-horizontal form-bordered" action="<?php echo base_url(); ?>view_search_flights" method="get" id="flight_request">
                        <div class="form-body clearfix">
                            <div class="col-md-10">
                                <div class="row">
                                    <div class="clearfix">
                                        <div class="col-md-12 text-center">
											<div class="radio">
												<label><input class="onewayresturnradio" type="radio" name="optradio" value="1" >One-way</label>
											</div>
											<div class="radio">
												<label><input class="onewayresturnradio" type="radio" name="optradio" value="2" checked>Round-trip</label>
											</div>
										</div>
										<div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-12">From</label>
                                                <div class="col-md-12">
													<input type="text" name="Flight_from" id="Flight_from" class="form-control search_destination_field" onfocus="this.placeholder = ''">
                                                    <input type='hidden' name='Flight_fromID' id='Flight_fromID'>
												</div>
                                            </div>
                                        </div>
										<div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-12">To</label>
                                                <div class="col-md-12">
                                                    <input type="text" name="Flight_to" id="Flight_to" class="form-control search_destination_field" onfocus="this.placeholder = ''">
                                                    <input type='hidden' name='Flight_toID' id='Flight_toID'>
                                                </div>
                                            </div>
                                        </div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-12">Departure</label>
												<div class="col-md-12">
													<input type="text" name="departure_date"  class="form-control icon-pick departure_date text-align-left" id="departure_date">
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-12">Return</label>
												<div class="col-md-12">
													<input type="text" name="return_date"  class="form-control icon-pick return_date text-align-left" id="return_date">
												</div>
											</div>
										</div>
                                    </div>
								</div>                                
                            </div>   
                            <div class="col-md-2">
                                <div class="row">
                                    <div class="clearfix">
                                        <button class="btn btnc1" id="quote_request_button1">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                
                <div role="tabpanel" class="tab-pane active" id="hotelSearch">
                    <form autocomplete="off" class="form-horizontal form-bordered" action="<?php echo base_url(); ?>view_search_result" method="get" id="quote_request">
                        <div class="form-body clearfix">
                            <div class="col-md-10">
                                <div class="row">
                                    <div class="clearfix">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-12">Where are you going?</label>
                                                <div class="col-md-12">
                                                    <input type="text" name="locationName" id="locationName" class="form-control search_destination_field"  onfocus="this.placeholder = ''">
                                                    <input type='hidden' name='locationId' id='locationId'>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-12">Check-in</label>
                                                        <div class="col-md-12">
                                                            <input type="text" name="check_in"  class="form-control icon-pick check_in text-align-left" id="check_in" >
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-12">Check-out</label>
                                                        <div class="col-md-12">
                                                            <input type="text" name="check_out"  class="form-control icon-pick check_out text-align-left" id="check_out">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label col-sm-12">Rooms</label>
                                                <div class="col-sm-12">
                                                    <select class="select2me form-control input-xlarge rooms text-align-center" name='roomNo' id='roomNo' >
                                                        <?php
                                                        for ($index = 1; $index <= 9; $index++) {
                                                            echo "<option value='{$index}'>{$index}</option>";
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div  id="hotel_rooms_id">
                                            <div class="clearfix">
                                                <div class="col-sm-2">
                                                    <p class="margin-36-0-0-0 text-align-right">Room 1:</p></div>
                                                <div class="col-sm-3"><div class="form-group"><label class="control-label col-sm-12">Adults</label><div class="col-sm-12"><select class="text-align-center select2me form-control input-xlarge numberOfAdults" name="numberOfAdults[]" id="numberOfAdults"><option value="1">1</option><option value="2" selected="">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option></select></div></div></div><div class="col-sm-3"><div class="form-group"><label class="control-label col-md-12">Children</label><div class="col-md-12"><select class="text-align-center select2me form-control input-xlarge numberOfChild" name="numberOfChild[]" id="numberOfChild" ><option data-for_room="1" value="0">0</option><option data-for_room="1" value="1">1</option><option data-for_room="1" value="2">2</option><option data-for_room="1" value="3">3</option></select></div></div></div></div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="clearfix" id="room1"></div>
                                    <div class="clearfix" id="room2"></div>
                                    <div class="clearfix" id="room3"></div>
                                    <div class="clearfix" id="room4"></div>
                                    <div class="clearfix" id="room5"></div>
                                    <div class="clearfix" id="room6"></div>
                                    <div class="clearfix" id="room7"></div>
                                    <div class="clearfix" id="room8"></div>
                                    <div class="clearfix" id="room9"></div>
                                    <div class="clearfix"></div>
                                    <div class="clearfix"></div>
                                    <div class="clearfix">
                                        <h4 class="formHeading">OPTIONAL FIELDS:</h4>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-12">Hotel Name</label>
                                                <div class="col-md-12">
                                                    <input type="text" name="hotelName" id="hotelName" class="form-control search_destination_field" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-12">Star rating</label>
                                                <div class="col-md-12">
                                                    <select class="select2me form-control input-xlarge text-align-center" name='hotelRating' id='hotelRating'>
                                                        <option value="0">Show All</option>
                                                        <option value="2">2 stars or better</option>
                                                        <option value="3">3 stars or better</option>
                                                        <option value="4">4 stars or better</option>
                                                        <option value="5">5 stars only</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>   
                            <div class="col-md-2">
                                <div class="row">
                                    <div class="clearfix">
                                        <button class="btn btnc1" id="quote_request_button">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>     
                </div>
                
                <div role="tabpanel" class="tab-pane" id="cars">
                    <div class="tag_img">
                        <?php /*?><img src="<?php echo base_url(); ?>public/affilliate_theme/img/commings.png" alt="Coming Soon" /><?php */?>
                    </div>
                </div>
                
                <div role="tabpanel" class="tab-pane" id="cruises">
                    <div class="tag_img">
                        <?php /*?><img src="<?php echo base_url(); ?>public/affilliate_theme/img/commings.png"  alt="Coming Soon" /><?php */?>
                    </div>
                </div>
                
                <div role="tabpanel" class="tab-pane" id="events" style="padding: 0;margin:-10px 0 0 0;">
                    <div class="coupon-holder" style="">
				
				 <script type="text/javascript" src="https://tickettransaction2.com/Brokers/01504-011/WidgetJS/scriptsretail-stringutils.js"></script> <script type="text/javascript">var widgetCSS=document.createElement("link"); widgetCSS.setAttribute("rel", "stylesheet"); widgetCSS.setAttribute("type", "text/css"); widgetCSS.setAttribute("href", "https://tickettransaction2.com/Brokers/01504-011/WidgetCSS/Style.css"); document.getElementsByTagName("head")[0].appendChild(widgetCSS); </script><p></p>
<div id="TL_Search_Widget" style="width: 100%; border: 1px solid rgba(238, 238, 238, 0.35);">
<div id="tlsw_header" style="background-color:rgba(16, 44, 63, 0.9);"> <span>Find Tickets</span><input id="tlsw_searchTxt" value="Search by Artist or Event" onblur="if(this.value=='') this.value=this.defaultValue;" onfocus="if(this.value==this.defaultValue) this.value='';" type="text"><p></p>
<button id="tlsw_searchBtn" class="events-srch"  onclick="tn_SubmitSearch()">Search <script type="text/javascript">function tn_KeyDownHandler(e){if (e==null) e=window.event; if (e.keyCode==13) tn_SubmitSearch();}document.getElementById('tlsw_searchTxt').onkeypress=tn_KeyDownHandler; </script> </div>
</button>
<ul id="tlsw_content" class="suggestionsArea"></ul>
<div id="tlsw_scripts"><script type="text/javascript">function tn_fill_top_events() {var tlsw_content = new Array();for (var tn_counter = 0; tn_counter < tn_top_performers.length; tn_counter++)tlsw_content.push('<li class="tlsw_suggestion" onmouseover="this.style.backgroundPosition=\'bottom right\';" onmouseout="this.style.backgroundPosition=\'top right\';" style="background-color:rgba(16, 44, 63, 0.9);"><a target= "_blank"href="//www.jdoqocy.com/click-8137399-10858765?sid=&amp;url=http:\/\/www.ticketliquidator.com/tix/' + Fixer(tn_top_performers[tn_counter]) + '-tickets.aspx"><span class="tlsw_linktext">' + tn_top_performers[tn_counter] + '</span></a></li>');document.getElementById('tlsw_content').innerHTML = tlsw_content.join('');}</script><script type="text/javascript" src="//tickettransaction.com/?bid=1&amp;tid=top_sellers&amp;javaarray=true&amp;listsize=5"></script><script type="text/javascript">function tn_SubmitSearch() {var SearchPhrase = document.getElementById('tlsw_searchTxt').value.trim().replace(/\s/g, '-');window.open('//www.jdoqocy.com/click-8137399-10858765?sid=&amp;url=' + 'http://www.ticketliquidator.com/tix/' + SearchPhrase + '-tickets.aspx');}</script></div>
<div id="tlsw_footer"> <a id="tlsw_footer_link" href="//www.jdoqocy.com/click-8137399-10858765?sid=&amp;url=http://www.ticketliquidator.com/default.aspx" target="_blank"> <img src="//s3.amazonaws.com/ticketliquidator/affiliates/widgetMaker/SearchWidget/logo-white.png"> </a> </div>
</div>



			</div>
                </div>
                
                <div role="tabpanel" class="tab-pane" id="activities">
                    <form class="form-horizontal form-bordered" action="<?php echo base_url(); ?>view_activity_search_result" method="post" id="activitiesForm" >
                        <div class="form-body clearfix">
                            <div class="clearfix">
                                <div class="col-md-10">
                                    <div class="row">
                                        <div class="clearfix">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label col-md-12">Where are you going?</label>
                                                    <div class="col-md-12">
                                                        <input type="text" name="activityLocationName" id="activityLocationName" class="form-control" />
                                                        <input type='hidden' name='activityLocationId' id='activityLocationId'>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-12">From</label>
                                                            <div class="col-md-12">
                                                                <input type="text" name="activity_date_from" class="form-control icon-pick check_in" id="activity_date_from">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-12">To</label>
                                                            <div class="col-md-12">
                                                                <input type="text" name="activity_date_to" class="form-control icon-pick check_out" id="activity_date_to">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix">
                                            <h4 class="formHeading">OPTIONAL FIELDS:</h4>
                                            <div class="col-md-12">
                                                <label class="control-label col-md-12">Name Of Activity</label>
                                                <input type="text" name="activity_name" class="form-control" id="activity_name">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="row">
                                        <div class="clearfix">
                                            <button class="btn btnc1">Search</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
	<div class="width-row">
		<div class="main_cont">
			<h4 class="text-align-center" >Welcome to <strong>Wholesale Hotels Group!</strong></h4>
			<!-- <p>We are a comprehensive travel site that strives to bring our customers the best deals on flights, hotels, AND more, at steeply discounted rates.</p> -->
			<p>The creation of our company was structured after extensive research of the travel industry. We have formed a solid foundation through obtaining top accreditation such as IATAN<sup>TM</sup>, ASTA<sup>TM</sup> licensing, contracting and a vast network infrastructure and affiliations within the industry. Unlike third-party booking sites, our website provides <strong>deeply discounted  </strong> inventory rates to customers! The average individual cannot purchase travel through a wholesaler and is usually limited to booking sites we all know too well. However, our website bridges this chasm by providing a medium for customers to purchase inexpensive rates for hotels, at prices not available to the public. Moreover, we display a more extensive network of domestic and international flights, which enables us to many instances provide lower rates than our competition. Most online websites are only able to offer flights with limited number of carriers, but  <strong>Wholesale Hotels Group</strong> works with a third-party affiliate who has access to essentially all airlines worldwide. This enables our company to deliver you airline rates at much better prices.</p>
			<p>With the help of  <strong>Wholesale Hotels Group,</strong> you can keep more money in your pocket while affording the getaway of your dreams! To begin your exploration for discounted travel, you simply sign-up for membership and start searching for your desired hotels and/or activities. For those wanting only flights, event tickets or travel insurance; you just simply access the respective sections of our site, with no membership required. <strong>Wholesale Hotels Group</strong> also provides discounted rates on all the most popular tours and activities for your travel destination. Again, we provide these at deeply discounted rates due to our connection with the biggest suppliers in the industry.</p>
			<p>We also provide our customers with connections for the best deals among our competitor and affiliate sites.  <strong>Wholesale Hotels Group</strong> provides members and the public with one of the cheapest flight, hotel, and tour sales through our Coupons Page, blog, and social media; Facebook, Twitter, Google Plus, Pinterest, or Instagram. We offer such services because we recognize that today’s customer is time-restrained. We want to provide you with a seamless experience, that’s quick and covers all of your travel needs with the best price available.</p>
			<p>At <strong>Wholesale Hotels Group</strong>, as our name suggests, we hope you see travel differently. In an effort to not only provide the lowest prices; we also hope to provide the best service.</p>
			<p>Our customer service is available via email 24/7 for questions or concerns. Additionally, reservation assistance is also available 24/7, 365-days a year, via a toll-free phone number.</p>
			<p>We appreciate your patronage and realize that there are many options when choosing travel, but we hope that once you experience  <strong>Wholesale Hotels Group</strong>, it will be the only site you’ll need.</p>
		</div>
	</div>
</div>
<div id="myModal_autocomplete" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog width-80-percent">
        <div class="login_main_cont margin-top-20">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><em class="fa fa-times"></em></button>
            <form class="login-form-request-quotation"  method="post">

                <h3 class="form-title">LOGIN</h3>
                <div class="alert alert-danger display-hide hidden display_off" id='msg'>
                    <button class="close" data-close="alert"></button>
                    <span>
                        Enter any username and password.
                    </span>
                </div>
                <div class="alert alert-danger display-hide display_off" id='login'>
                    <button class="close" data-close="alert"></button>
                    <span>
                        Please enter your email address and password.
                    </span>
                </div>
                <div class="form-group">
                    <label class="control-label">Email address</label>
                    <div class="margin-bottom-10">
                        <input class="form-control" tabindex="2" type="text" id="user" name="username" value="<?php echo get_cookie('username'); ?>" />
                    </div>
                </div>
                <div class="form-group ">
                    <label class="control-label">Password</label>
                    <div class="margin-bottom-10">
                        <input class="form-control" tabindex="2" type="password" id="pass" name="password" value="<?php echo get_cookie('password'); ?>" />
                    </div>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn-login" value="Login" tabindex="4">
                    <label class="checkbox padding-8-0-0-0" >
                        <input type="checkbox" tabindex="3" name="remember" value="1" id="remember" <?php
                        if (get_cookie('username') != '') {
                            echo 'checked="checked"';
                        }
                        ?> /> Remember me
                    </label>
                </div>
                <div class="clear"></div>
                <div class="form-group margin-top-10">
                    <div class="">
                        <label class="control-label pull-left forgot_pass"><a href="javascript:;" tabindex="5" id="forget-password">Forgot your password?</a></label>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="form-group margin-top-10 margin-bottom-10 create-account">
                    <strong class="signUpNow">
                        <span class="txt-red">New here? </span>No worries,
                        <a href="<?php echo base_url(); ?>front/home/member-signup">SIGN UP NOW</a>
                        which takes 30 seconds and access the best deals online right away!
                    </strong>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
                <div class="create-account hide padding-0-15">
                    <a href="<?php echo base_url(); ?>front/home/member-signup">
                        Member
                    </a>
                </div>
            </form>
            <form class="forget-form display_off" action="#" method="post" >
                <h3>Forgot password?</h3>
                <p class="text-align-center" >
                    Enter your e-mail address below to request your password.
                </p>
                <div class="form-group">
                    <div class="input-icon">
                        <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email" id="forgetemail"/>
                    </div>
                </div>
                <div class="form-actions">
                    <button type="button" id="back-btn" class="btn red_btn padding-5-35">Back </button>
                    <button type="submit" class="default_btn forgot_pass_btn">Submit</button>
                </div>
            </form>
        </div> 
    </div>
</div>

<script src="<?php echo base_url(); ?>public/assets/plugins/jquery-1.10.2.min.js" type="text/javascript" ></script>
<script src="<?php echo base_url(); ?>public/affilliate_theme/plugins/checkincheckout/bootstrap-datepicker.js" type="text/javascript"></script>
<script>var hotel_search_results;</script>
<script>var firstColumnChangeRoom = "sm-2";var secondColumnChangeRoom = "sm-3";var thirdColumnChangeRoom = "sm-12";var clearFixDivStartChangeRoom = "<div class='clearfix'>";var clearFixDivEndChangeRoom = "</div>";</script>
<script src="<?php echo base_url(); ?>public/hotel_search/changing_rooms_home.js" type="text/javascript"></script>
<script>var divRoomLabel = 1;var firstColumn = "sm-3";var secondColumn = "sm-3";</script>
<script src="<?php echo base_url(); ?>public/hotel_search/show_children_ages_drop_down_home.js" type="text/javascript"></script>
<div aria-hidden="false" role="dialog" class="modal fade in display_off" id="success_registration">
    <div class="success_login_main_cont margin-top-20">         
        <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><em class="fa fa-times"></em></button>
            <h3 class="form-title">Success!</h3>
        </div>
        <div class="">
            <p class="padding-10"> We just emailed your password to your email address on file.</p>
        </div>
        <div class="modal-footer padding-3">
            <div class="text-align-center">
                <button type="button" data-dismiss="modal" class="btn default_btn clickhome padding-7-20">OK</button>
            </div>
        </div>           
    </div>                                           
</div>
<div aria-hidden="false" role="dialog" class="modal fade in display_off" id="error_registration" >
    <div class="success_login_main_cont margin-top-20">

        <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><em class="fa fa-times"></em></button>
            <h3 class="form-title">Sorry!</h3>
        </div>
        <div class="">
            <p class="padding-10"> It seems that your email address is not in our database. Please check if you typed your email address properly.</p>
        </div>
        <div class="modal-footer padding-3">
            <div class="text-align-center">
                <button type="button" data-dismiss="modal" class="btn default_btn clickhome padding-7-20">OK</button>
            </div>
        </div>

    </div>                                           
</div>
