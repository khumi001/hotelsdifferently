<style>
    .tooltip-inner {
        max-width: 600px;
        /* If max-width does not work, try using width instead */
        width: 600px; 
    }
    .pricacy-width{
        width: 100%; overflow: auto; height: 200px;
    }
    a.tooltips {
  position: relative;
  display: inline;
}
.username1 span{
    visibility: hidden;
    display: none;
}
.email1 span{
    visibility: hidden;
    display: none;
}

.tooltips span {
  position: absolute;
  
  color: #FFFFFF;
  background: #000000;
  height: 30px;
  line-height: 30px;
  text-align: center;
  visibility: hidden;
  border-radius: 6px;
}
.tooltips span::after {
  border-left: 8px solid transparent;
  border-right: 8px solid transparent;
  border-top: 8px solid #000000;
  content: "";
  height: 0;
  left: 50%;
  margin-left: -8px;
  position: absolute;
  top: 86%;
  width: 0;
}
.username1.tooltips span {
  bottom: 38px;
  display: block;
  left: 0;
  margin-left: 16px;
  opacity: 0.8;
  visibility: visible;
  z-index: 999;
  width:155px;
}
.email1.tooltips span {
  bottom: 38px;
  display: block;
  left: 0;
  margin-left: 16px;
  opacity: 0.8;
  visibility: visible;
  z-index: 999;
  width:255px;
}
</style>

<div class="width-row-700 margin-top-10">
    <div class="main_cont">
        <div class="pagetitle margin-bottom-20">
            <h1><?php echo $this->page_name; ?></h1>
        </div>
        <div class="row">
            <div class="col-md-12" id="form_wizard_1">
                <p><?php echo $message;?></p>
                <p><?php echo $regards;?></p>
            </div>
        </div>
    </div>
</div>


