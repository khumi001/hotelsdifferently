<style>
    .tooltip-inner {
        max-width: 600px;
        /* If max-width does not work, try using width instead */
        width: 600px; 
    }
</style>

<div class="width-row-700 margin-top-10">
    <div class="main_cont">
        <div class="pagetitle margin-bottom-20">
            <h1><?php echo $this->page_name; ?></h1>
        </div>
        <div class="row">
            <div class="col-md-12" id="form_wizard_1">
                <form action="#" class="form-horizontal signup_form" id="submit_form">
                    <div class="form-wizard">
                        <div class="form-body">
                            <ul class="nav nav-pills nav-justified steps" style="background: none !important;">
                                <li>
                                    <a href="#tab1" data-toggle="tab" class="step" style="background: none !important;">
                                        <span class="number">
                                            1
                                        </span>
                                        <span class="desc">
                                            <i class="fa fa-check"></i> General Details
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#tab2" data-toggle="tab" class="step" style="background: none !important;">
                                        <span class="number">
                                            2
                                        </span>
                                        <span class="desc">
                                            <i class="fa fa-check"></i> Terms and Conditions
                                        </span>
                                    </a>
                                </li>
                            </ul>
                            <div id="bar" class="progress progress-striped" role="progressbar">
                                <div class="progress-bar progress-bar-success">
                                </div>
                            </div>
                            <div class="tab-content">
                                <div class="alert alert-danger display-none">
                                    <button class="close" data-dismiss="alert"></button>
                                    You have some form errors. Please check below.
                                </div>
                                <div class="alert alert-success display-none">
                                    <button class="close" data-dismiss="alert"></button>
                                    Your Registration is successful!
                                </div>
                                <div class="tab-pane active" id="tab1">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Username<a class="tooltips" data-placement="top" data-original-title="Your username must be between 4 to 10 characters, only letters and numbers. Please note that your username can be displayed on our homepage in case your booking qualifies for our hottest deals!"><SUP><i class="fa fa-info-circle"></i></sup></a>:</label></label>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control" name="username"/>
                                            <input type="hidden" name="accountid" id="accountid" value="<?php echo $accountId; ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">First name:</label>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control" name="firstname"/>                                           
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Last name:</span>
                                        </label>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control" name="lastname"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Email address:</label>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control" name="email" id="email"/>
                                            <span class="help-block email_massage" id="email_massage" style="display: none" >You have all ready Registered.</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Password<a class="tooltips" data-placement="top" data-original-title="Your password is CaSe SeNsItIvE, must be at least 8 characters and must contain at least one letter and one number."><sup><i class="fa fa-info-circle"></i></sup></a>:</label>
                                        <div class="col-md-9">
                                            <input type="password" class="form-control" name="password" id="submit_form_password"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Confirm password:</label>
                                        <div class="col-md-9">
                                            <input type="password" class="form-control" name="rpassword"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Phone number<a class="tooltips" data-placement="top" data-original-title="Optional field."><sup><i class="fa fa-info-circle"></i></sup></a>:</label>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control" name="phone1"/>
                                            <span class="small_label">Country</span>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" name="phone2"/>
                                            <span class="small_label">Number</span>
                                        </div>
                                        <div class="col-md-3">
                                            <input type="text" class="form-control" name="phone3"/>
                                            <span class="small_label">Extension</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <!--<label class="control-label col-md-3">DOB:</label>-->
                                        <div class="col-md-9">
                                            <input type="hidden" class="form-control date-picker" name="dob"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Country:</label>
                                        <div class="col-md-9">
                                            <select name="country" id="country_list" class="form-control select2me">
                                                <option value=""></option>
                                                <option value="AF">Afghanistan</option>
                                                <option value="AL">Albania</option>
                                                <option value="DZ">Algeria</option>
                                                <option value="AS">American Samoa</option>
                                                <option value="AD">Andorra</option>
                                                <option value="AO">Angola</option>
                                                <option value="AI">Anguilla</option>
                                                <option value="AQ">Antarctica</option>
                                                <option value="AR">Argentina</option>
                                                <option value="AM">Armenia</option>
                                                <option value="AW">Aruba</option>
                                                <option value="AU">Australia</option>
                                                <option value="AT">Austria</option>
                                                <option value="AZ">Azerbaijan</option>
                                                <option value="BS">Bahamas</option>
                                                <option value="BH">Bahrain</option>
                                                <option value="BD">Bangladesh</option>
                                                <option value="BB">Barbados</option>
                                                <option value="BY">Belarus</option>
                                                <option value="BE">Belgium</option>
                                                <option value="BZ">Belize</option>
                                                <option value="BJ">Benin</option>
                                                <option value="BM">Bermuda</option>
                                                <option value="BT">Bhutan</option>
                                                <option value="BO">Bolivia</option>
                                                <option value="BA">Bosnia and Herzegowina</option>
                                                <option value="BW">Botswana</option>
                                                <option value="BV">Bouvet Island</option>
                                                <option value="BR">Brazil</option>
                                                <option value="IO">British Indian Ocean Territory</option>
                                                <option value="BN">Brunei Darussalam</option>
                                                <option value="BG">Bulgaria</option>
                                                <option value="BF">Burkina Faso</option>
                                                <option value="BI">Burundi</option>
                                                <option value="KH">Cambodia</option>
                                                <option value="CM">Cameroon</option>
                                                <option value="CA">Canada</option>
                                                <option value="CV">Cape Verde</option>
                                                <option value="KY">Cayman Islands</option>
                                                <option value="CF">Central African Republic</option>
                                                <option value="TD">Chad</option>
                                                <option value="CL">Chile</option>
                                                <option value="CN">China</option>
                                                <option value="CX">Christmas Island</option>
                                                <option value="CC">Cocos (Keeling) Islands</option>
                                                <option value="CO">Colombia</option>
                                                <option value="KM">Comoros</option>
                                                <option value="CG">Congo</option>
                                                <option value="CD">Congo, the Democratic Republic of the</option>
                                                <option value="CK">Cook Islands</option>
                                                <option value="CR">Costa Rica</option>
                                                <option value="CI">Cote d'Ivoire</option>
                                                <option value="HR">Croatia (Hrvatska)</option>
                                                <option value="CU">Cuba</option>
                                                <option value="CY">Cyprus</option>
                                                <option value="CZ">Czech Republic</option>
                                                <option value="DK">Denmark</option>
                                                <option value="DJ">Djibouti</option>
                                                <option value="DM">Dominica</option>
                                                <option value="DO">Dominican Republic</option>
                                                <option value="EC">Ecuador</option>
                                                <option value="EG">Egypt</option>
                                                <option value="SV">El Salvador</option>
                                                <option value="GQ">Equatorial Guinea</option>
                                                <option value="ER">Eritrea</option>
                                                <option value="EE">Estonia</option>
                                                <option value="ET">Ethiopia</option>
                                                <option value="FK">Falkland Islands (Malvinas)</option>
                                                <option value="FO">Faroe Islands</option>
                                                <option value="FJ">Fiji</option>
                                                <option value="FI">Finland</option>
                                                <option value="FR">France</option>
                                                <option value="GF">French Guiana</option>
                                                <option value="PF">French Polynesia</option>
                                                <option value="TF">French Southern Territories</option>
                                                <option value="GA">Gabon</option>
                                                <option value="GM">Gambia</option>
                                                <option value="GE">Georgia</option>
                                                <option value="DE">Germany</option>
                                                <option value="GH">Ghana</option>
                                                <option value="GI">Gibraltar</option>
                                                <option value="GR">Greece</option>
                                                <option value="GL">Greenland</option>
                                                <option value="GD">Grenada</option>
                                                <option value="GP">Guadeloupe</option>
                                                <option value="GU">Guam</option>
                                                <option value="GT">Guatemala</option>
                                                <option value="GN">Guinea</option>
                                                <option value="GW">Guinea-Bissau</option>
                                                <option value="GY">Guyana</option>
                                                <option value="HT">Haiti</option>
                                                <option value="HM">Heard and Mc Donald Islands</option>
                                                <option value="VA">Holy See (Vatican City State)</option>
                                                <option value="HN">Honduras</option>
                                                <option value="HK">Hong Kong</option>
                                                <option value="HU">Hungary</option>
                                                <option value="IS">Iceland</option>
                                                <option value="IN">India</option>
                                                <option value="ID">Indonesia</option>
                                                <option value="IR">Iran (Islamic Republic of)</option>
                                                <option value="IQ">Iraq</option>
                                                <option value="IE">Ireland</option>
                                                <option value="IL">Israel</option>
                                                <option value="IT">Italy</option>
                                                <option value="JM">Jamaica</option>
                                                <option value="JP">Japan</option>
                                                <option value="JO">Jordan</option>
                                                <option value="KZ">Kazakhstan</option>
                                                <option value="KE">Kenya</option>
                                                <option value="KI">Kiribati</option>
                                                <option value="KP">Korea, Democratic People's Republic of</option>
                                                <option value="KR">Korea, Republic of</option>
                                                <option value="KW">Kuwait</option>
                                                <option value="KG">Kyrgyzstan</option>
                                                <option value="LA">Lao People's Democratic Republic</option>
                                                <option value="LV">Latvia</option>
                                                <option value="LB">Lebanon</option>
                                                <option value="LS">Lesotho</option>
                                                <option value="LR">Liberia</option>
                                                <option value="LY">Libyan Arab Jamahiriya</option>
                                                <option value="LI">Liechtenstein</option>
                                                <option value="LT">Lithuania</option>
                                                <option value="LU">Luxembourg</option>
                                                <option value="MO">Macau</option>
                                                <option value="MK">Macedonia, The Former Yugoslav Republic of</option>
                                                <option value="MG">Madagascar</option>
                                                <option value="MW">Malawi</option>
                                                <option value="MY">Malaysia</option>
                                                <option value="MV">Maldives</option>
                                                <option value="ML">Mali</option>
                                                <option value="MT">Malta</option>
                                                <option value="MH">Marshall Islands</option>
                                                <option value="MQ">Martinique</option>
                                                <option value="MR">Mauritania</option>
                                                <option value="MU">Mauritius</option>
                                                <option value="YT">Mayotte</option>
                                                <option value="MX">Mexico</option>
                                                <option value="FM">Micronesia, Federated States of</option>
                                                <option value="MD">Moldova, Republic of</option>
                                                <option value="MC">Monaco</option>
                                                <option value="MN">Mongolia</option>
                                                <option value="MS">Montserrat</option>
                                                <option value="MA">Morocco</option>
                                                <option value="MZ">Mozambique</option>
                                                <option value="MM">Myanmar</option>
                                                <option value="NA">Namibia</option>
                                                <option value="NR">Nauru</option>
                                                <option value="NP">Nepal</option>
                                                <option value="NL">Netherlands</option>
                                                <option value="AN">Netherlands Antilles</option>
                                                <option value="NC">New Caledonia</option>
                                                <option value="NZ">New Zealand</option>
                                                <option value="NI">Nicaragua</option>
                                                <option value="NE">Niger</option>
                                                <option value="NG">Nigeria</option>
                                                <option value="NU">Niue</option>
                                                <option value="NF">Norfolk Island</option>
                                                <option value="MP">Northern Mariana Islands</option>
                                                <option value="NO">Norway</option>
                                                <option value="OM">Oman</option>
                                                <option value="PK">Pakistan</option>
                                                <option value="PW">Palau</option>
                                                <option value="PA">Panama</option>
                                                <option value="PG">Papua New Guinea</option>
                                                <option value="PY">Paraguay</option>
                                                <option value="PE">Peru</option>
                                                <option value="PH">Philippines</option>
                                                <option value="PN">Pitcairn</option>
                                                <option value="PL">Poland</option>
                                                <option value="PT">Portugal</option>
                                                <option value="PR">Puerto Rico</option>
                                                <option value="QA">Qatar</option>
                                                <option value="RE">Reunion</option>
                                                <option value="RO">Romania</option>
                                                <option value="RU">Russian Federation</option>
                                                <option value="RW">Rwanda</option>
                                                <option value="KN">Saint Kitts and Nevis</option>
                                                <option value="LC">Saint LUCIA</option>
                                                <option value="VC">Saint Vincent and the Grenadines</option>
                                                <option value="WS">Samoa</option>
                                                <option value="SM">San Marino</option>
                                                <option value="ST">Sao Tome and Principe</option>
                                                <option value="SA">Saudi Arabia</option>
                                                <option value="SN">Senegal</option>
                                                <option value="SC">Seychelles</option>
                                                <option value="SL">Sierra Leone</option>
                                                <option value="SG">Singapore</option>
                                                <option value="SK">Slovakia (Slovak Republic)</option>
                                                <option value="SI">Slovenia</option>
                                                <option value="SB">Solomon Islands</option>
                                                <option value="SO">Somalia</option>
                                                <option value="ZA">South Africa</option>
                                                <option value="GS">South Georgia and the South Sandwich Islands</option>
                                                <option value="ES">Spain</option>
                                                <option value="LK">Sri Lanka</option>
                                                <option value="SH">St. Helena</option>
                                                <option value="PM">St. Pierre and Miquelon</option>
                                                <option value="SD">Sudan</option>
                                                <option value="SR">Suriname</option>
                                                <option value="SJ">Svalbard and Jan Mayen Islands</option>
                                                <option value="SZ">Swaziland</option>
                                                <option value="SE">Sweden</option>
                                                <option value="CH">Switzerland</option>
                                                <option value="SY">Syrian Arab Republic</option>
                                                <option value="TW">Taiwan, Province of China</option>
                                                <option value="TJ">Tajikistan</option>
                                                <option value="TZ">Tanzania, United Republic of</option>
                                                <option value="TH">Thailand</option>
                                                <option value="TG">Togo</option>
                                                <option value="TK">Tokelau</option>
                                                <option value="TO">Tonga</option>
                                                <option value="TT">Trinidad and Tobago</option>
                                                <option value="TN">Tunisia</option>
                                                <option value="TR">Turkey</option>
                                                <option value="TM">Turkmenistan</option>
                                                <option value="TC">Turks and Caicos Islands</option>
                                                <option value="TV">Tuvalu</option>
                                                <option value="UG">Uganda</option>
                                                <option value="UA">Ukraine</option>
                                                <option value="AE">United Arab Emirates</option>
                                                <option value="GB">United Kingdom</option>
                                                <option value="US">United States</option>
                                                <option value="UM">United States Minor Outlying Islands</option>
                                                <option value="UY">Uruguay</option>
                                                <option value="UZ">Uzbekistan</option>
                                                <option value="VU">Vanuatu</option>
                                                <option value="VE">Venezuela</option>
                                                <option value="VN">Viet Nam</option>
                                                <option value="VG">Virgin Islands (British)</option>
                                                <option value="VI">Virgin Islands (U.S.)</option>
                                                <option value="WF">Wallis and Futuna Islands</option>
                                                <option value="EH">Western Sahara</option>
                                                <option value="YE">Yemen</option>
                                                <option value="ZM">Zambia</option>
                                                <option value="ZW">Zimbabwe</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Opt-in for newsletter<a class="tooltips" data-placement="top" data-original-title="Opt-in for our newsletter to get notification about website updates and the greatest deals & savings that our Members book."><sup><i class="fa fa-info-circle"></i></sup></a>:</label></label>
                                        <div class="col-md-9">
                                            <select name="newsletter" class="form-control select2me">
                                                <option value=""></option>
                                                <option value="Y" selected='selected'>Yes</option>
                                                <option value="N">No</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">How did you hear about us?:</label>
                                        <div class="col-md-9">
                                            <input type="text" name='about_us' class='form-control'>
                                        </div>
                                    </div>
                                    <?php
                                    foreach ($info as $row) {
                                     $id=$row['var_accountid'];
                                    }
                                    $type='AF';
                                    if($row['chr_user_type']==$type && $row['var_accountid']==$id ){
                                    ?>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">SIGNUP COUPON:</label>
                                        <div class="col-md-9">
                                            <span style="color: #ff0000; display: block;font-weight: bold;padding: 8px 10px;">CONGRATULATIONS!!! $20 DISCOUNT ON YOUR FIRST BOOKING! </span>
                                        </div>
                                    </div>
                                    <?php } 
                                     $type='U';
                                    if($row['chr_user_type']==$type && $row['var_accountid']==$id ){
                                    ?>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">SIGNUP COUPON:</label>
                                        <div class="col-md-9">
                                            <span style="color: #ff0000; display: block;font-weight: bold;padding: 8px 10px;">CONGRATULATIONS!!! $25 DISCOUNT ON YOUR FIRST BOOKING! </span>
                                        </div>
                                    </div>
                                    <?php }            
                                    ?>
                                   <?php
                                   if($row['chr_user_type']=='' && $row['var_accountid']=='' ){
                                    ?>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">SIGNUP COUPON:</label>
                                        <div class="col-md-9">
                                            <span style="color: #ff0000; display: block;font-weight: bold;padding: 8px 10px;">CONGRATULATIONS!!! $20 DISCOUNT ON YOUR FIRST BOOKING! </span>
                                        </div>
                                    </div>
                                    <?php }            
                                    ?>
                                  
                                </div>
                                <div class="tab-pane" id="tab2">

                                    <div class="panel-group accordion" id="accordion3">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a class="accordion-toggle accordion-toggle-styled" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_1">
                                                        Privacy Policy
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapse_3_1" class="panel-collapse in">
                                                <div class="panel-body">
                                                    <p>
                                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.   
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_2">
                                                        Terms and Conditions
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapse_3_2" class="panel-collapse collapse">
                                                <div class="panel-body" style="height:200px; overflow-y:auto;">
                                                    <p>
                                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.   
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_3">
                                                        Intellectual Property Policy
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapse_3_3" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p>
                                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.   
                                                    </p>
                                                </div>
                                            </div>
                                        </div>   
                                        <div class="form-group margin-top-10">
                                            <div class='col-md-12'>
                                                <div class="checkbox-list">
                                                    <label class="checkbox-inline control-label" style="font-weight: bold;">
                                                        <div class="checker" id="uniform-inlineCheckbox21"><span class=""><input type="checkbox" value="option1" name="remember" id="inlineCheckbox21"></span></div>I hereby accept the Terms and Conditions, Privacy Policy and the Intellectual Property Policy. </label>
                                                </div>
                                            </div>

                                            <!--                                            <label class="col-md-12" style="display: inline-block;">
                                                                                            <input type="checkbox" name="policy" style="display: inline-block;" value="1" data-title="I hereby accept the Terms and Conditions, Privacy Policy and the Intellectual Property Policy.  ."/>I hereby accept the Terms and Conditions, Privacy Policy and the Intellectual Property Policy.</label>-->
                                            <div id="register_tnc_error">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions fluid margin-top-40">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="text-center col-md-12">
                                        <a href="javascript:;" class="btn grey_btn button-previous listing_btn" style="padding: 8px 20px; margin: 0 8px;">
                                            <i class="m-icon-swapleft"></i> Back
                                        </a>
                                        <a href="javascript:;" class="btn red_btn listing_btn" id="reset_form" style="padding: 8px 20px; margin: 0 8px;">
                                            Reset <i class="m-icon-swapright m-icon-white"></i>
                                        </a>
                                        <a href="javascript:;" class="btn green_btn listing_btn button-next" style="padding: 8px 20px; margin: 0 8px;">
                                            Next <i class="m-icon-swapright m-icon-white"></i>
                                        </a>
                                        <!--                                    <a href="javascript:;" class="btn green button-submit">
                                                                                Register <i class="m-icon-swapright m-icon-white"></i>
                                                                            </a>-->
                                        <input type="submit" value="Register" class="btn red_btn button-submit listing_btn" style="padding: 8px 20px; margin: 0 8px;">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
