

<style scoped>
   .tooltip-inner {max-width: 600px;width: 600px;}
   .pricacy-width{width: 100%; overflow: auto; height: 200px;}
   a.tooltips {position: relative;display: inline;}
   .username1 span{visibility: hidden;display: none;}
   .email1 span{
   visibility: hidden;
   display: none;
   }
   .tooltips span {
   position: absolute;
   color: #FFFFFF;
   background: #000000;
   height: 30px;
   line-height: 30px;
   text-align: center;
   visibility: hidden;
   border-radius: 6px;
   }.tooltips span::after {
   border-left: 8px solid transparent;
   border-right: 8px solid transparent;
   border-top: 8px solid #000000;
   content: "";
   height: 0;
   left: 50%;
   margin-left: -8px;
   position: absolute;
   top: 86%;
   width: 0;
   }.username1.tooltips span {
   bottom: 38px;
   display: block;
   left: 0;
   margin-left: 16px;
   opacity: 0.8;
   visibility: visible;
   z-index: 999;
   width:155px;
   }.email1.tooltips span {
   bottom: 38px;
   display: block;
   left: 0;
   margin-left: 16px;
   opacity: 0.8;
   visibility: visible;
   z-index: 999;
   width:255px;
   }
   #capatcha {
   margin: 0 28%;
   display: block
   }
</style>
<div class="width-row-700 margin-top-10">
   <div class="width-row" style="display:none;padding: 10px 0 0 8px;">
      <div itemscope itemtype="http://schema.org/Person">
         <span itemprop="name">Hotels Differently</span>
         <span itemprop="company">HotelsDifferently LLC</span>
         <span itemprop="tel">888-287-2307</span>
      </div>
   </div>
   <div class="main_cont">
      <div class="pagetitle margin-bottom-20">
         <h1><?php echo $this->page_name; ?></h1>
      </div>
      <div class="row">
         <div class="col-md-12" id="form_wizard_1">
            <form action="#" class="form-horizontal signup_form" id="submit_form">
               <div class="form-wizard">
                  <div class="form-body">
                     <div class="tab-content">
                        <div class="alert alert-danger display-none hidden">
                           <button class="close" data-dismiss="alert"></button>
                           Ooops, you made some mistakes. Please check fields highlighted in red!
                        </div>
                        <div class="alert alert-success display-none hidden">
                           <button class="close" data-dismiss="alert"></button>
                           Your Registration is successful!
                        </div>
                        <div >
                           <div class="panel-group accordion" id="accordion3">
                              <div class="panel panel-default">
                                 <div class="panel-heading">
                                    <h4 class="panel-title">
                                       <a class="accordion-toggle accordion-toggle-styled" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_1">
                                       Privacy Policy
                                       </a>
                                    </h4>
                                 </div>
                                 <div id="collapse_3_1" class="panel-collapse in">
                                    <div class="panel-body ">
                                       <div class="width-row pricacy-width margin-top-20">
                                          <div class="main_cont">
                                             <div class="pagetitle margin-bottom-10">
                                                <!--<h1><?php echo $this->page_name; ?></h1>-->
                                                <p><strong>Effective as of February 1, 2015</strong></p>
                                                <p>Unless otherwise set forth herein with specificity, the definitions set forth in our TERMS AND CONDITIONS POLICY apply to this Privacy Policy (the &ldquo;PRIVACY POLICY&rdquo;). Protecting your private information is our priority. This PRIVACY POLICY applies to <strong>www.HotelsDifferently.com<sup>sm</sup></strong> and <strong>HotelsDifferently, LLC</strong> and its parent LLC and governs data collection and usage as set forth herein. The Company Site is a service for those person or persons seeking lower cost Hotel stays. By using the Company Site, you consent to the data practices described in this PRIVACY POLICY.</p>
                                                <p>&nbsp;</p>
                                                <p><strong>Section I. PERSONALLY IDENTIFIABLE INFORMATION.</strong></p>
                                                <p><strong>1. For Members or Potential Members:</strong> The following are considered PERSONALLY IDENTIFIABLE INFORMATION:</p>
                                                <p>A. Your First and Last Name</p>
                                                <p>B. Your Email Address</p>
                                                <p>C. Your Phone Number (if you supply one)</p>
                                                <p>D. Your Account Number</p>
                                                <p><strong>2. For Affiliates or Potential Affiliates:</strong> The following are considered PERSONALLY IDENTIFIABLE INFORMATION:</p>
                                                <p>A. Your company name</p>
                                                <p>B. The First and Last Name of any person supplied by you as a point of contact for us.</p>
                                                <p>C. Your Email Address</p>
                                                <p>D. Your Phone Number(s)</p>
                                                <p>E. Your street address but NOT your city, state, zip code or country</p>
                                                <p>F. Your account number</p>
                                                <p>G. Your site description</p>
                                                <p>H. Any information provided in order for us to process payments to you.</p>
                                                <p><br />
                                                   <strong>SECTION II. INFORMATION WHICH IS NOT PERSONALLY IDENTIFIABLE INFORMATION</strong>
                                                </p>
                                                <p>1. <strong>For Members or Potential Members</strong>: The following are NOT considered PERSONALLY IDENTIFIABLE INFORMATION:</p>
                                                <p>A. Any Voluntary Data Collection as set forth in this PRIVACY POLICY and in our TERMS AND CONDITIONS POLICY.</p>
                                                <p>B. Your Username and password; however your password will be known only to you and Company unless you disclose your password to another which is PROHIBITED under our TERMS AND CONDITIONS POLICY.</p>
                                                <p>C. Any ELECTRONIC COMMUNICATIONS as set forth in <strong>Section II(4) of our TERMS AND CONDITIONS POLICY</strong>, <strong>or any information shared with a Service Provider as set forth in Section II(7)(B) of our TERMS AND CONDITIONS POLICY, except</strong> that if <strong>any of the items denominated in Section I(1)(A-D) of this PRIVACY POLICY</strong> are included in Such ELECTRONIC COMMUNICATION or with a Service Provider, <strong>then the items in Section I(1)(A-D) of this PRIVACY POLICY are still considered Personally Identifiable Information.</strong></p>
                                                <p>D. Any information you supply to LINKS TO THIRD PARTY SITES/THIRD PARTY SERVICES as set forth in <strong>Section II(7)(A) of our TERMS AND CONDITIONS POLICY.</strong></p>
                                                <p>E. Any information you supply in your USE OF COMMUNICATION SERVICES as set forth in <strong>Section</strong> <strong>II(4) of our TERMS AND CONDITIONS POLICY</strong>.</p>
                                                <p>F. Any information you supply in INTERACTION BETWEEN USERS as set forth in <strong>Section II(10)</strong> <strong>of our TERMS AND CONDITIONS POLICY.</strong></p>
                                                <p>G. Any information that we are need to share with a Hotel in order to make a booking for you.</p>
                                                <p>H. Any information as denominated in Section <strong>III(1)(A-B) and Section II(11) of our TERMS AND CONDITIONS POLICY</strong></p>
                                                <p>I. Any information supplied in requesting, approving or denying a Membership.</p>
                                                <p>J. &nbsp;Any information supplied to THIRD PARTY ACCOUNTS as set forth in <strong>Section IV(1) of our TERMS AND CONDITIONS POLICY.</strong></p>
                                                <p><strong>2. For Affiliates or Potential Affiliates: </strong>The following are <strong>NOT</strong> considered <strong>PERSONALLY IDENTIFIABLE INFORMATION:</strong></p>
                                                <p>A. Your city, state, zip code or country</p>
                                                <p>B. Any Voluntary Data Collection as set forth in this PRIVACY POLICY and in our TERMS AND CONDITIONS POLICY.</p>
                                                <p>C. Your password will be known only to you and Company unless you disclose your password to another which is PROHIBITED under our TERMS AND CONDITIONS POLICY.</p>
                                                <p>D. Any ELECTRONIC COMMUNICATIONS as set forth in <strong>Section II(4) of our TERMS AND CONDITIONS POLICY,</strong> <strong>or any information shared with a Service Provider as set forth in Section II(7)(B) of our TERMS AND CONDITIONS POLICY, except</strong> that <strong>if any of the items</strong> denominated in <strong>Section I(2)(A-H</strong>) of this PRIVACY POLICY are included in Such ELECTRONIC COMMUNICATION or with a Service Provider, <strong>then the items in Section I(2)(A-H) of this PRIVACY POLICY are still considered Personally Identifiable Information.</strong></p>
                                                <p>E. Any information you supply to LINKS TO THIRD PARTY SITES/THIRD PARTY SERVICES as set forth in <strong>Section II(7) of our TERMS AND CONDITIONS POLICY.</strong></p>
                                                <p>F. Any information you supply in INTERACTION BETWEEN USERS as set forth in <strong>Section</strong> <strong>II(10) of our TERMS AND CONDITIONS POLICY.</strong></p>
                                                <p>G. Any information supplied to THIRD PARTY ACCOUNTS as set forth in <strong>Section IV(1) of our TERMS AND CONDITIONS POLICY.</strong></p>
                                                <p>H. Any information supplied in requesting, approving or denying Affiliate status.</p>
                                                <p><strong>&nbsp;SECTION III. INFORMATION GENERALLY</strong></p>
                                                <p><strong>1. Use of Information, Generally</strong></p>
                                                <p>A. Personally Identifiable Information will never be sold, leased, given, transferred, rented or otherwise provided to another User or to a third party except as otherwise as set forth in Section II, Section III and Section IV(2) of this PRIVACY POLICY.</p>
                                                <p>B. Company may also collect anonymous demographic information, which is not unique to you, Such as your city, state and country (for Affiliates), and Voluntary Data. All of our payments to us are processed using PayPal&reg; or Bitcoin using coinbase&reg; as our exchanger. Our payments to Affiliates are made by us using either Western Union&reg; or PayPal&reg; as chosen by the Affiliate.</p>
                                                <p>We do not engage in any direct credit card/debit card or e-check transactions. PayPal&reg; may ask for your billing address information, including zip code, among other information, to process payment electronically by Members to us. Bitcoin may ask for certain information to protect your transaction. Western Union&reg; may ask for the same or similar information from Affiliates for payments by us to Affiliates. We do NOT have access to your PayPal&reg;  Western Union&reg; or Bitcoin account information.</p>
                                                <p>C. &nbsp;Information about your computer hardware and software may be automatically collected by Company. This information can include: Your IP address, browser type, domain names, access times and referring website addresses. This information is used for the operation of the service, to maintain quality of the service, and to provide general statistics regarding use of the Company website.</p>
                                                <p>D. Please keep in mind that if you directly disclose Personally Identifiable Information or personally sensitive data with anyone OTHER than the Company, this information may be collected and used by others.</p>
                                                <p>E. The Company encourages you to review the privacy statements of websites you choose to link to from Company, if any, so that you can understand how those websites collect, use and share your information. Company is not responsible for the privacy statements or other content on websites outside of the Company&rsquo; Site.</p>
                                                <p>&nbsp;<strong>2. Use of Your Personally Identifiable Information by Company</strong></p>
                                                <p>A. Company collects and uses your Personally Identifiable Information to operate its website(s) and deliver the services you have requested.</p>
                                                <p>B. Company may also use your Personally Identifiable Information to inform you of other products or services available from Company and its affiliates. Company may also contact you via surveys to conduct research about your opinion of current services or of potential new services that may be offered. You will not consider this to be SPAM.</p>
                                                <p>C. Company may share data with trusted partners, Such as Service Providers, to help perform statistical analysis, send you email, track referrals by Affiliates or to provide customer support. All Such third parties are prohibited from using your Personally Identifiable Information except to provide these services to Company, and they are required to maintain the confidentiality of your information.</p>
                                                <p>D. Company may keep track of the websites and pages our Users visit within Company, in order to determine what Company services are the most popular. This data is used to deliver customized content and advertising within Company to customers whose behavior indicates that they are interested in a particular subject area.</p>
                                                <p>E. Company will disclose your Personally Identifiable Information, without notice, only if required to do so by law or in the good faith belief that Such action is necessary to: (a) conform to the edicts of the law or comply with legal process served on Company or the site; (b) protect and defend the rights or property of Company; or (c) act under exigent circumstances to protect the personal safety of Users of Company, or the public.</p>
                                                <p><strong>3. Use of Cookies</strong></p>
                                                <p>The Company website may use &quot;cookies&quot; and similar automated tools to help you personalize your online experience. A cookie is a text file that is placed on your hard disk by a web page server. Cookies are uniquely assigned to you, and can only be read by a web server in the domain that issued the cookie to you.</p>
                                                <p>One of the primary purposes of cookies is to provide a convenience feature to save you time. The purpose of a cookie is to tell the web server that you have returned to a specific page. For example, if you personalize Company pages, or register with Company site or services, a cookie helps Company to recall your specific information on subsequent visits. This simplifies the process of recording your Personally Identifiable Information, Such as Service preferences, etc. When you return to the same Company website, the information you previously provided can be retrieved, so you can easily use the Company features that you customized.</p>
                                                <p>You have the ability to accept or decline cookies. Most Web browsers automatically accept cookies, but you can usually modify your browser setting to decline cookies if you prefer. If you choose to decline cookies, you may not be able to fully experience the interactive features of the Company services or websites you visit.</p>
                                                <p>&nbsp;<strong>4. Security of Your Personally Identifiable Information</strong></p>
                                                <p>&nbsp;Company secures your Personally Identifiable Information from unauthorized access, use or disclosure.</p>
                                                <p>&nbsp;<strong>5. Minors Under Minimum Age.</strong></p>
                                                <p>Company does not knowingly collect personally identifiable information from minors under the Minimum Age.</p>
                                                <p><strong>6. Disconnecting Your Company Account from Third Party Websites</strong></p>
                                                <p>You may be able to connect your Company Account to third party accounts, Such as social media. BY CONNECTING YOUR COMPANY ACCOUNT TO A THIRD PARTY ENTITY, YOU ACKNOWLEDGE AND AGREE THAT YOU ARE CONSENTING TO THE CONTINUOUS RELEASE OF INFORMATION ABOUT YOU TO OTHERS (IN ACCORDANCE WITH YOUR PRIVACY SETTINGS ON THOSE THIRD PARTY SITES). IF YOU DO NOT WANT INFORMATION ABOUT YOU, INCLUDING PERSONALLY IDENTIFYING INFORMATION, TO BE SHARED IN THIS MANNER, DO NOT USE THE THIS FEATURE IF WE MAKE IT AVAILABLE. You may disconnect your Account from a third party entity at any time. If we make this function available, Members may learn how to disconnect their Accounts from third-party websites by visiting that third-party website.</p>
                                                <p><strong>7. Opt-Out &amp; Unsubscribe</strong></p>
                                                <p>We respect your privacy and give you an opportunity to opt-out of receiving newsletters and announcements of certain information. Users may opt in or out of our newsletter as outlined in our Frequently Asked Questions. With regard to our newsletter, you must affirmatively choose to opt in; we do NOT automatically send you our Newsletter. Users may opt-out of receiving any or all other communications not directly related to the provision of Company Services by using the Contact Us page of our Site.</p>
                                                <p><strong>8. Changes to this Statement</strong></p>
                                                <p>Company will occasionally update this PRIVACY POLICY, in its sole discretion and without prior notice, to reflect Company and User feedback. Company encourages you to periodically review this PRIVACY POLICY to be informed of how Company is protecting your information. The date of the most current PRIVACY POLICY will always be at the top of this PRIVACY POLICY.</p>
                                                <p><strong>9.</strong>&nbsp;<strong>Contact Information</strong></p>
                                                <p>Company welcomes your questions or comments regarding this PRIVACY POLICY. If you believe that Company has not adhered to this Statement, please contact Company using the Contact Us page on our Site.</p>
                                                <p><strong>I0. FAILURE TO ABIDE OR ACCEPT. If you DO NOT AGREE TO ABIDE BY, OR FULLY ACCEPT, and ELECTRONICALLY SIGN this PRIVACY POLICY AND ALL OTHER POLICIES, you MUST IMMEDIATELY EXIT THIS SITE. Links to this PRIVACY POLICY AND ALL OF OUR POLICIES may be found on the Home Page of this Site.</strong></p>
                                                <p><strong>11. ELECTRONIC SIGNATURE ACT.</strong> By entering this Site, you are, pursuant to the Electronic Signature Act as defined in Section IV (16) of our TERMS AND CONDITIONS, indicating: </p>
                                                <p>A. Your unconditional acceptance and agreement to abide by the this PRIVACY POLICY AND ALL OUR POLICIES and,</p>
                                                <p>B. That you are of MINIMUM AGE, and</p>
                                                <p>C. You are submitting an unsworn declaration, certificate and verification, in writing of your agreement with, and that you will abide by, this PRIVACY POLICY AND ALL OUR POLICIES, that is subscribed by you, as true under penalty of perjury, and dated on, and in the form required, to be in compliance with the Electronic Signature Act.</p>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="panel panel-default">
                                 <div class="panel-heading">
                                    <h4 class="panel-title">
                                       <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_2">
                                       Terms and Conditions
                                       </a>
                                    </h4>
                                 </div>
                                 <div id="collapse_3_2" class="panel-collapse collapse">
                                    <div class="panel-body ">
                                       <div class="width-row pricacy-width margin-top-20">
                                          <div class="main_cont">
                                             <div class="pagetitle margin-bottom-10">
                                                <!--<h1><?php echo $this->page_name; ?></h1>-->
                                                <!--<p align="center"><strong>TERMS AND CONDITIONS</strong></p>-->
                                                <p>&nbsp;</p>
                                                <p><strong>Effective May 1, 2015</strong></p>
                                                <p>&nbsp;</p>
                                                <p><strong>Agreement between User (as defined below) and www.HotelsDifferently.com.</strong></p>
                                                <p>Welcome to <strong>www.HotelsDifferently.com</strong>. The <strong>www.HotelsDifferently.com </strong>website (the &quot;Site&quot; as defined below) is comprised of various web pages operated by <strong>HotelsDifferently, LLC</strong>  (the &quot;Company&quot; as defined below). The Site is offered to you (as defined below) conditioned on your (as defined below) acceptance without modification of the terms, conditions, and notices contained herein (the &quot;TERMS AND CONDITIONS POLICY&quot; or the &ldquo;TERMS AND CONDITIONS&rdquo; or the &ldquo;POLICY&rdquo;). Your use of the Site constitutes your legally binding agreement to all Such TERMS AND CONDITIONS. Please read these TERMS AND CONDITIONS carefully. For the good and adequate consideration to enter, view, browse, or use our Site, the adequacy and sufficiency of which you hereby acknowledge, you agree as follows:</p>
                                                <p>&nbsp;</p>
                                                <p><strong>SECTION I. DEFINITIONS.</strong></p>
                                                <p><strong>1. DEFINITIONS</strong>: As used in this POLICY, the following terms have the meanings as set forth in each definition. The word or words for which Such definition is provided shall apply regardless of whether Such word or words are capitalized, or in all upper case, or in all lower case letters, unless the context requires otherwise. In addition, the definitions in this TERMS AND CONDITIONS apply to our Site (as defined below). If a term is defined in an OTHER POLICY (as defined below), Such definition in that OTHER POLICY shall apply only to that OTHER POLICY; if a term is not defined in an OTHER POLICY, but defined in this TERMS AND CONDITIONS, then the definition in this TERMS AND CONDITIONS shall apply to each Such OTHER POLICY. For the purposes of this TERMS AND CONDITIONS, and each OTHER POLICY, unless otherwise noted, all references to <strong>HotelsDifferently, LLC</strong> include <strong>www.HotelsDifferently.com</strong>&nbsp;and Company (as defined below). The Site is a service for those person or persons seeking lower cost Hotel (as defined below) stays. By using the Site, you consent to all provisions described in this TERMS AND CONDITIONS.</p>
                                                <p>A. &ldquo;Account&rdquo; means the account established by the Company for either an Affiliate or a Member, as the context may require.</p>
                                                <p>B. &ldquo;Account Number&rdquo; means the unique number assigned to each Member and Affiliate by Company.</p>
                                                <p>C. &ldquo;Actual Name&rdquo; is the legal first and last name of a Member or Affiliate, or the legal name of any Commercial Entity, (i) supplied by a Member during Member registration, sign up or requesting to make a Reservation on the Site (ii) and/or the legal first and last name of a person, other than a Member, &nbsp;which is supplied by a Member when seeking to book a Reservation, or change a Confirmed Reservation, under a different name than that of a Member, or (iii) supplied by an Affiliate or Potential Affiliate as part of the sign up process for an Affiliate.&nbsp; Actual Name shall also mean the legal first and last name of a Member, Affiliate or a Commercial Entity, as applicable, when used as part of our &ldquo;Contact Us&rdquo; function. A Member shall not use their Actual Name as their Username.</p>
                                                <p>The &ldquo;Actual Name&rdquo; is for use by the individual or Commercial Entity supplying it and the Company ONLY; it will never be displayed to any other User and it is considered PERSONALLY IDENTIFIABLE INFORMATION and will never be shared with a third party except (1) with a Service Provider to assist in providing services to Company, (2) if a Member used his Actual Name as a Username, which is prohibited, or (3) as required by law.</p>
                                                <p>D. &ldquo;Advertisement&rdquo;, or &ldquo;Ad&rdquo;, means an advertisement placed on our Site, with our permission, by a Commercial Advertiser.</p>
                                                <p>E. &ldquo;Affiliate&rdquo; means a person or Commercial Entity, other than a Commercial Advertiser, who has signed up for an Affiliate Account on the Site, whether or not they have posted or otherwise promoted Company&rsquo;s Services, and also includes Such individuals or a Commercial Entity who have promoted Company&rsquo;s Services. Once a person or entity meets this definition, then, and at all times thereafter, they are considered an Affiliate.</p>
                                                <p>F. &ldquo;Affiliate Link&rdquo; shall mean a website page or other web access provided to an Affiliate to activate access to the Site. &nbsp;</p>
                                                <p>G. &ldquo;Affiliate Payment&rdquo; shall be as defined in Section III(2)(H)</p>
                                                <p>H. &ldquo;Alternative Minimum Age&rdquo; shall mean you are the legal minimum age or older, Such that you have the legal capacity to enter into a binding legal contract pursuant to the law of your country of citizenship and your political subdivision thereof, if any, where Such age is greater than eighteen (18) years of age.</p>
                                                <p>I. &ldquo;Banner&rdquo; shall mean a Company approved banner type advertisement provided to an Affiliate in order for the Affiliate to place it on one or more of Affiliate&rsquo;s websites or in other locations as chosen by the Affiliate.</p>
                                                <p>J. &ldquo;Cancellation&rdquo; shall mean a Member, who within the timeframe as set forth in our Cancellation Policy, and in complete compliance with our Cancellation Policy, cancels their Reservation(s).</p>
                                                <p>K. &ldquo;Cancellation Policy&rdquo; shall mean the policy as set forth in Section III(1)(H) of this TERMS AND CONDITIONS.</p>
                                                <p>L. &ldquo;Commercial Advertiser&rdquo; means a person or entity, other than an Affiliate, whose Company has allowed to post an Advertisement for goods or services, other than those provided by Company, on the Site, and which may consist of a link to a website other than a Site of Company, Such as a banner advertisement or similar referral to Such other website not owned or operated by Company.</p>
                                                <p>M. &ldquo;Commercial Entity&rdquo; shall mean any type of business formation permitted under the laws of the country and any legal subdivision thereof in which an Affiliate&rsquo;s or Potential Affiliate&rsquo;s business is organized, including but not limited to, a corporation, any type of partnership, sole proprietorship, &ldquo;doing business as&rdquo;, any type of limited liability company or corporation, or trust.</p>
                                                <p>N. &ldquo;Company&rdquo; shall mean www.HotelsDifferently.com, and <strong>HotelsDifferently, LLC</strong> the owner and/or publisher of the Site.</p>
                                                <p>O. &ldquo;Company Name&rdquo; shall mean the name entered by a Potential Affiliate during signing up to become an Affiliate.</p>
                                                <p>P. &ldquo;Confirmed Reservation&rdquo; shall mean that a Member has accepted a Reservation and the Member has paid us in FULL for the amount specified in the Reservation, less any applicable Coupons. No Confirmed Reservation shall be issued if less than the full amount of the Reservation is paid to us by the expiration date and time of the Reservation.</p>
                                                <p>Q. &ldquo;Electronic Signature Act&rdquo; shall have the meaning as set forth in Section IV(16).</p>
                                                <p>R. &ldquo;Email Address&rdquo; shall mean an email address where an Affiliate, Member, Potential Affiliate or Potential Member may be reached by Company staff.</p>
                                                <p>S. &ldquo;Hotel&rdquo; shall mean a hotel, motel, or similar facility which offers lodging to guests for a payment for a single night or longer.</p>
                                                <p>T. &ldquo;Home Page&rdquo; shall mean the initial page seen by Users upon accessing or logging into the Site.</p>
                                                <p>U. &ldquo;Member&rdquo; means a person who enters the Site, other than as an Affiliate or, Commercial Advertiser,&nbsp; or Potential Member and who has applied for, or signed up with the Site, for a Member Account and been granted a Membership&nbsp; in order to browse the Site and/or potentially or actually use Company&rsquo;s Services.</p>
                                                <p>V. Membership shall mean having become a Member.</p>
                                                <p>W. &ldquo;Minimum Age&rdquo; shall mean either eighteen (18) years of age or older where Such age is sufficient to enter into a legally binding contract, or where eighteen (18) years of age is insufficient to enter into a legally binding contract, the Alternative Minimum Age or older.</p>
                                                <p>X. &ldquo;OTHER POLICIES&rdquo; shall mean this Site&rsquo;s TERMS AND CONDITIONS, INTELLECTUAL PROPERTY POLICY, PRIVACY POLICY, DMCA policy, FAQ (Frequently Asked Questions) and all other terms and conditions of this Site that govern access to, the browsing or viewing thereof, and/or the use of any feature, of this Site.</p>
                                                <p>Y. &ldquo;our&rdquo;, &ldquo;us&rdquo;, and &ldquo;we&rdquo; mean the Company.</p>
                                                <p>Z. &ldquo;PERSONALLY IDENTIFIABLE INFORMATION&rdquo; shall be as defined in our PRIVACY POLICY.</p>
                                                <p>A1. &ldquo;Potential Affiliate&rdquo; means any person or Commercial Entity who visits this Site in contemplation of becoming an Affiliate of Company and/or who has applied for to become an Affiliate but whose Affiliate status has not been approved.</p>
                                                <p>A2. &ldquo;Potential Member&rdquo; means any person who visits this Site in contemplation of becoming a Member of this Site and/or who has applied for Membership but whose Membership has not been approved.</p>
                                                <p>A3. &ldquo;Reservation&rdquo; shall mean the United States Dollar or Dollar and Cents amount that Company provides in response to a Member&rsquo;s request(s) for the price of a Hotel stay, based upon the information furnished by the Member to the Company.</p>
                                                <p>A4. &ldquo;Rate&rdquo; shall mean the dollar amount charged by Company to a Member for a Hotel room or rooms for one or more nights.</p>
                                                <p>A5. &ldquo;Services&rdquo; shall mean the provision, by Company, of quotes and/or bookings for Hotel stays.</p>
                                                <p>A6. &ldquo;Service Provider&rdquo; shall mean a person or entity contracted with Company in order to assist Company in the provision of its Services. &nbsp;A Service Provider may not use PERSONALLY IDENTIFIABLE INFORMATION for ANY purpose other than the business purposes of Company in order to assist Company in the provision of its Services and Such Service Provider is likewise prohibited from any disclosure of any PERSONALLY IDENTIFIABLE INFORMATION to any third party.</p>
                                                <p>A7. &ldquo;Site&rdquo; shall mean this website (<strong>www.HotelsDifferently.com<sup>sm</sup>)</strong> or any other website or website page wholly owned or published by Company, but not any website of an Affiliate, Commercial Advertiser or any other third party:</p>
                                                <p style="margin-left:.5in;">(i) to which a User may be redirected as a result of the User selecting a particular function or use from within <strong>www.HotelsDifferently.com<sup>sm</sup>,</strong> or</p>
                                                <p style="margin-left:.5in;">(ii) which the User enters directly by typing in the website address of Such other website or page into their internet browser, or</p>
                                                <p style="margin-left:.5in;">(iii) which the User enters by copying and pasting Such website address of Such other website or page in their internet browser, or by means of the User selecting a bookmark the User has created, or</p>
                                                <p>(iv) which the User enters by any other means.</p>
                                                <p>A8. &ldquo;THIRD PARTY ACCOUNTS&rdquo; shall mean accounts which are not operated by Company but which a User may elect to utilize or link their Company account, including, but not limited to, social media platforms and websites.</p>
                                                <p>A9. &ldquo;User&rdquo; shall mean any person who accesses this Site for ANY reason, including, but not limited to, an Affiliate, a Member, a Commercial Advertiser, a Potential Affiliate, a Potential Member, or any other person entering this Site for any purpose, except as otherwise set forth in this Site&rsquo;s PRIVACY POLICY.</p>
                                                <p>A10. &ldquo;Username&rdquo; is a name chosen by a Member or Potential Member during the process of requesting to become a Member. This MUST be a name OTHER than the Actual Name of the person supplying this information. The Username is necessary to display as part of the Member&rsquo;s login, for promotional purposes by Company on the Site, and may also be displayed in other features of the Site, and may be visible to other User&rsquo;s from multiple locations in the Site. An example of a Username might be &ldquo;TRAVELALOT&rdquo; or &ldquo;HOTELHUNTER&rdquo;. In no event is a Username considered to be a Member&rsquo;s PERSONALLY IDENTIFIABLE INFORMATION.</p>
                                                <p>A11. &ldquo;Voluntary Data Collection&rdquo; shall be as defined in our PRIVACY POLICY.</p>
                                                <p>A12. &ldquo;you&rdquo; and &ldquo;your&rdquo; shall mean any User.<br />
                                                   <br />
                                                   &nbsp;
                                                </p>
                                                <p><strong>SECTION II. GENERAL</strong></p>
                                                <p><strong>1. TERMS AND CONDITIONS-GENERALLY</strong>.&nbsp; In addition to the specific contents of this POLICY and OTHER POLICIES, all provisions as set forth in each and every page, portion, section, html page or subdomain of this Site are also terms and conditions of use under this POLICY.</p>
                                                <p><br />
                                                   <strong>2. ELECTRONIC SIGNATURE. </strong>By entering our Site, you are deemed to have accepted, and signed this TERMS AND CONDITIONS POLICY AND OUR OTHER POLICIES pursuant to the Electronic Signature Act; see also Section IV(16) of this POLICY.
                                                </p>
                                                <p><br />
                                                   <strong>3. OTHER POLICIES. </strong>Your use of the Site is subject to Company&#39;s OTHER POLICIES as well as this POLICY. Please review ALL of these OTHER POLICIES, including, but not limited to, our PRIVACY POLICY, DMCA policy, and INTELLECTUAL PROPERTY POLICY, which also govern the Site and informs Users of our data collection practices and other practices and policies. You may find links to any of these OTHER POLICIES and this POLICY on the Home Page of this Site.
                                                </p>
                                                <p><br />
                                                   <strong>4. ELECTRONIC COMMUNICATIONS.</strong> Visiting the Site or sending emails to Company or receiving an email from Company in response to an email sent by you constitutes electronic communications. You consent to receive electronic communications and you agree that all agreements, notices, disclosures and other communications that we provide to you electronically, via email and on the Site, satisfy any legal requirement that Such communications be in writing.
                                                </p>
                                                <p>A. For all Users, if you OPTED IN for our newsletter, you will not consider our newsletter as SPAM. You may opt out of the newsletter at any time by using the unsubscribe hyperlink that you will receive in any newsletter.</p>
                                                <p>B. For all Members or Potential Members, we may send you emails in order to provide, or to potentially provide, Services to you and related functions. These may include, but are not limited to, activating your Member Account, letting you know that you have used a wrong Account activation link, or that you have previously activated your account , sending you a Reservation, to advise if your request for Membership has been declined, suspended or terminated, to verify any change in Account information Such as a change in your email address, password, a forgotten password or other Account information, in response to a request or information you seek by using the Contact Us function of the Site, sending you a Confirmed Reservation, a link for our REFER A FRIEND offering, and other matters related to your Account or changes thereto, and/or our Services, and you will not consider Such emails as SPAM. If you OPTED IN for our newsletter, you will not consider our newsletter as SPAM. You may opt out of the newsletter at any time by using the unsubscribe hyperlink that you will receive in any newsletter.</p>
                                                <p>C. For Affiliates or Potential Affiliates, we may send you emails in order to provide, or to potentially provide, with your benefits and activities as an Affiliate, and related functions. These may include, but are not limited to, activating your Affiliate Account, letting you know that you have used a wrong Account activation link, or that you have previously activated your account , to advise if your request for Affiliate status has been declined, suspended or terminated, to verify any change in Account information Such as a change in your email address, password, a forgotten password, payment information or other Account information, in response to a request for information you seek by using the Contact Us function of the Site, sending you a link to activate your Affiliate Status, and other matters related to your Account and/or our Services, and you will not consider Such emails as SPAM. You may opt out of the newsletter at any time by using the unsubscribe hyperlink that you will receive in any newsletter. &nbsp;</p>
                                                <p>D. For Affiliates and Members, at present we do not intend to send you any emails other than those other than those set forth in Sections II(4)(A) and II(4)(B), above. Should we decide, at a later date, to offer to send you emails for other purposes, Such as an opportunity to receive special offers, etc., you will be given an opportunity to Opt In or Out of Such emails, and you will not consider any offering to Opt In or Opt Out to be &ldquo;SPAM&rdquo;.</p>
                                                <p><br />
                                                   <strong>5. YOUR ACCOUNT AND YOUR IP ADDRESS</strong>. If you use this Site, you are responsible for maintaining the confidentiality of your Account and password and for restricting access to your computer, and you agree to accept responsibility for all activities that occur under your Account or password. You may not assign, disclose, or otherwise transfer your Account to any other person or entity, nor to allow Such other person or entity to access your Account. You acknowledge that Company is not responsible for third party access to your Account that results from theft or misappropriation of your Account.
                                                </p>
                                                <p>Your IP address (a) may be suspended for six (6) hours if you unsuccessfully attempt five times to log into the Site, or (b) in the sole discretion of Company, suspended or permanently banned for activity in contravention of this POLICY, or any OTHER POLICY, (c) for actual or perceived fraud, or (d) any other reason Company deems Such suspension or ban is in Company&rsquo;s best interest.<br />
                                                   &nbsp;
                                                </p>
                                                <p><strong>6. PERSONS UNDER THE MINIMUM AGE/DENIAL OF ACCESS TO SITES/BYPASSING PROTECTIONS</strong>.</p>
                                                <p>A. Company does not permit the access, viewing or utilization of this Site, or knowingly collect, either online or offline, personal information, from persons under the Minimum Age. You are solely responsible if a person under the Minimum Age accesses your Account, and you indemnify and hold harmless the Company if a person under the Minimum Age accesses your Account.</p>
                                                <p>B. You represent and warrant to Company that you are at least of Minimum Age, understand and agree to abide by the laws and regulations of the location(s) from which you access this Site</p>
                                                <p>C. You shall not, by means of &ldquo;favorites&rdquo; or &ldquo;bookmarks&rdquo; or like functions on any internet browser, or by any manual or automated process, gain access to any portion of our Site that allows you or any other person or entity to bypass agreeing to our TERMS AND CONDITIONS or OTHER POLICIES. If you enter the Site in violation of the foregoing, you or any other person or entity have accepted the TERMS AND CONDITIONS, and all OTHER POLICIES, and your entrance to the Site is your legally binding acceptance and signature of agreement of same, dated as of the date you entered the Site.</p>
                                                <p>D. Our Site, and the software used therein, are governed by the laws of United States of America, and are subject to its import and export laws. This Site and software may not be exported to any country where Such exportation is illegal, or to any country in which its importation is illegal.</p>
                                                <p>E. You expressly indemnify and hold harmless the Company for any claims of any nature, losses or damages as a result of your failure to abide by the provisions of this Section 6.</p>
                                                <p><br />
                                                   <strong>7.</strong><strong> LINKS TO THIRD PARTY SITES/THIRD PARTY SERVICES</strong>
                                                </p>
                                                <p>A. This Site may contain links to other websites (&quot;Linked Sites&quot;) provided by Commercial Advertisers, social media providers, or others. The Linked Sites are not under the control of Company and Company is not responsible for the contents of any Linked Site, including without limitation, any link contained within in a Linked Site, or any changes or updates to a Linked Site. Company is providing these Linked Sites to you only as a convenience, and the inclusion of any Linked Site does not imply endorsement by Company of the Lined Site or any association with its operators, except that</p>
                                                <p style="margin-left:.5in;">&nbsp;(i) with regard to a link within this Site to a Commercial Advertiser to view and/or interact with a Commercial Advertiser&rsquo;s Advertisement (the &ldquo;Commercial Advertiser Link&rdquo;) on this Site does mean that Company may have accepted payment from a Commercial Advertiser for Such Advertisement. The Company does not endorse any particular Commercial Advertiser and does not warrant the Advertisement of any Commercial Advertiser, including any links included within a Commercial Advertiser Link. Company does not refer you to any particular Commercial Advertiser, and does not provide any contact information for a Commercial Advertiser other than the contact information provided by a Commercial Advertiser in their Advertisement.</p>
                                                <p>B. Certain services made available via the Site, including, but not limited to Site hosting and Service Providers, are delivered by third party sites and organizations. By using any product, service or functionality originating from the Site domain, you hereby acknowledge and consent that Company may share Such information, and data with any third party with whom Company has a contractual relationship to provide the requested product, service or functionality on behalf of Site Users. Provided however, Site hosting services and other Service Providers are prohibited from using any PERSONALLY IDENTIFIABLE INFORMATION for any purpose other than to provide their services to our Site.</p>
                                                <p>C. We enter into promotional relationships on a commercial basis with Affiliates to promote or market our Site by a variety of methods. We do not endorse, and we are not responsible or liable for any content of Such promotions or the types of promotions entered into on our behalf by a third party.</p>
                                                <p><br />
                                                   <strong>8. LIMITATIONS. </strong>You are granted ONLY, a non-exclusive, non-transferable, revocable, at our sole discretion and without prior notice, license to (a) access and use the Site strictly in accordance with this TERMS AND CONDITIONS POLICY and this Site&rsquo;s OTHER POLICIES and (b) to your Username and Password. As a condition of your use of the Site, you warrant to Company that you will not use the Site for any purpose that is unlawful or prohibited by these TERMS AND CONDITIONS or OTHER POLICIES. You may not use the Site in any manner which could damage, disable, overburden, or impair the Site or interfere with any other party&#39;s use and enjoyment of the Site. You may not obtain or attempt to obtain any materials or information through any means not intentionally made available or provided for through the Site.
                                                </p>
                                                <p>We do not grant you any licenses, express or implied, to the intellectual property or proprietary information of Company or our licensors except as expressly authorized by these TERMS AND CONDITIONS or OTHER POLICIES.</p>
                                                <p>We plan, in the future, to offer access to the Site, other than by internet web browsers, through applications for smart phone, smart watches, tablets, etc. When we do, we will amend this TERMS AND CONDITIONS and OTHER POLICIES as necessary to provide for the use of Such applications.</p>
                                                <p><br />
                                                   <strong>9.&nbsp; MATERIALS PROVIDED TO SITE OR POSTED ON ANY COMPANY WEB PAGE</strong>
                                                </p>
                                                <p>A. Company does not claim ownership of the materials you provide to Site (including feedback and suggestions) or post, upload, input or submit to any Company Site or our associated services which is Voluntary Data Collection. However, by posting, uploading, inputting, providing or submitting Voluntary Data Collection, you are granting Company, our affiliated companies and necessary sub licensee&rsquo;s permission to use Voluntary Data Collection in connection with the operation of their internet businesses including, without limitation, the rights to: copy, distribute, transmit, publicly display, publicly perform, reproduce, edit, translate and reformat Voluntary Data Collection. Voluntary Data Collection DOES NOT INCLUDE PERSONALLY IDENTIFIABLE INFORMATION.</p>
                                                <p>B. No compensation will be paid with respect to the use of Voluntary Data Collection as provided herein. Company is under no obligation to post or use any Voluntary Data Collection. You may provide and we may remove any Voluntary Data Collection at any time in Company&#39;s sole discretion.</p>
                                                <p>C. By posting, uploading, inputting, providing or submitting Voluntary Data Collection you warrant and represent that you own or otherwise control all of the rights to Voluntary Data Collection as described in this section including, without limitation, all the rights necessary for you to provide, post, upload, input or submit Such information.</p>
                                                <p>D. In no event do we warrant, under any theory of equity or of law, the contents of this Site, whether submitted by an Affiliate, Member, Potential Affiliate, Potential Member or Commercial Advertiser, person or entity promoting our Site by any means. Your choice to interact with, purchase from, or follow a link to another website, or to an Affiliate or Commercial Advertiser is solely your choice and you assume all risks for any Such action. Other than for your PERSONALLY IDENTIFIABLE INFORMATION, we make no representation as to the safety, security or otherwise, of the contents of this Site. Other than your PERSONALLY IDENTIFIABLE INFORMATION, you have no right of, or expectation of a right of, privacy of any information you submit to this Site except as otherwise explicitly provided for in this TERMS AND CONDITIONS and OTHER POLICIES.</p>
                                                <p>E. Our Site may include surveys/questionnaire on its Site for Users, which shall be considered a part of Voluntary Data Collection and shall not be considered &ldquo;SPAM&rdquo;.</p>
                                                <p><br />
                                                   <strong>10. INTERACTION BETWEEN USERS</strong>.
                                                </p>
                                                <p>A. Company does not have access to, and therefore does not monitor, any communications or interactions of whatever nature between Users on this Site, including by and among Affiliates and Members. Affiliates and Members ARE SOLELY responsible for any interactions by and among them, on any other website or by any other means.</p>
                                                <p>B. You absolutely hold harmless and indemnify Company from any actions or inactions, or damages or losses of any nature as a result of choosing to interact with another User, whether online on another website or by any other means. Company EXPRESSLY disclaims ANY LIABILITY OF ANY NATURE WHATSOEVER AS A RESULT OF ANY INTERACTION BETWEEN USERS, ON ANY OTHER WEBSITE OR BETWEEN USERS BY ANY OTHER MEANS.</p>
                                                <p><br />
                                                   <strong>11. USER SUBMISSION</strong>: Each User, including, but not limited to, Affiliates and Members:
                                                </p>
                                                <p>A. agrees that:</p>
                                                <p style="margin-left:.5in;">(i) Company is not responsible and does not represent or warrant the accuracy or veracity of any User Submission. As used in this Section 11, &ldquo;User Submission&rdquo; means, with regard to a:</p>
                                                <p style="margin-left:.5in;">1. Member: &nbsp;Any content in any Voluntary Data Collection&nbsp;</p>
                                                <p style="margin-left:.5in;">2. Affiliate: Any content in any Voluntary Data Collection&nbsp;&nbsp;&nbsp;</p>
                                                <p style="margin-left:1.0in;">3. Commercial Advertisement: Any content in a Commercial Advertisement, including any content in a website owned or operated by a Commercial Advertiser.</p>
                                                <p style="margin-left:1.0in;">4. Any information on any website of any other third party, including social media sites.</p>
                                                <p>B. agree that no User has any confidential information or proprietary rights with regard to any Voluntary Data Collection, nor shall any User misappropriate any confidential information or proprietary rights of any other person in Voluntary Data Collection or in an Advertisement.</p>
                                                <p>C. each User is aware that you may be exposed to another User&rsquo;s submission which may be objectionable, inaccurate, or not useful, and to which Such other User may or may not have intellectual or other property rights or proprietary interest in Such User submission, and the Company bears no responsibility or liability for Such User submissions.</p>
                                                <p>D. Company does review or monitor User submissions made directly to Company. In addition, the Company reserves the right to delete User submissions, including Commercial Advertisements, so long as it does not conflict with Applicable Law, it is under no obligation to do so.</p>
                                                <p>E. each User is responsible for their User submission(s) and is solely responsible for any loss, damage or other consequences of Such User submission.</p>
                                                <p>F. a User Submission of one User may not be shared, used, reproduced, copied or otherwise used by another User excepted as permitted in this policy.</p>
                                                <p>G. No User may attempt to, or actually upload any picture, graphic or other Image to any portion of the Site, by any means. As used herein, &ldquo;Image&rdquo; shall mean any photographic, graphic, artwork or similar visualization, whether or not Such Image includes or does not include any written or verbal component.</p>
                                                <p>H. All payments by a Member to the Company are processed through a third party credit/debit card processing service, using SSL or similar encryption technology. We are not responsible for the safety, security or accuracy of any information you provide to, or receive from, this third party provider; however, our commercial terms with Such third party shall require that Such third party be responsible to you, the Affiliate, for the safety, security, and accuracy of your information and to reverse any charge appearing as being from us that in fact was not submitted by you for payment to us.</p>
                                                <p>I. All our prices for Reservations, for payments to Affiliates and payment by Members, and any refund, if any, are in United States Dollars (USD). Payments to obtain a Confirmed Reservation will be charged to the Member in USD via Stripe® if the Member paid using Stripe® or in USD via Stripe&reg; if the Member paid using Bitcoin. Payments to Affiliates will never be made using Bitcoin. Any foreign transaction fees, currency conversion fees, dynamic currency conversion (DCC) fees, or like fees incurred because the Affiliate&rsquo;s or Member&rsquo;s local currency is not USD will be the responsibility of the Affiliate or Member. Further, any and all fees charged by Bitcoin or Stripe® relative to a Member’s use of Bitcoin and Stripe&reg; are the sole responsibility of the Member. Please see Section II(K) below relative to Bitcoin and Stripe&reg;.</p>
                                                <p>J. No Member or Affiliate shall share his Account number or password with another person or entity, including, but not limited to, another User. Sharing of either an Account number and/or password with any other person will result in a suspension and/or termination of a Member or Affiliate in Company&rsquo;s sole discretion.</p>
                                                <p>
                                                   K. This Bitcoin Payment Policy (“Bitcoin Payment Policy”) applies to your use of Bitcoin as your selected payment method. With regard to ANY Bitcoin payment, the following terms and conditions apply, in order of precedence: All Bitcoin Terms and Conditions and policies (“BTC”), all Stripe® Terms and Conditions and polices (“CTC”), and our Terms and Conditions. If you do not agree with any of this Section K or our refund policy with regard to Bitcoin and Stripe®, you MUST NOT use Bitcoin as your payment method. 
                                                </p>
                                                <p style="margin-left:1.0in;">1. Our acceptance of Bitcoin is through Stripe® as our exchange server. We are not responsible for, or guarantee, Stripe®’s services or the availability of Such servics.  To complete your payment, you will be re-directed to Stripe®’s website, where you will see the total cost of your Reservation in Bitcoin, based on the Stripe®’s exchange rate. The Bitcoin price for your Reservation will remain valid for ten (10) minutes. If you do not initiate your payment during this time, the Bitcoin exchange rate will be updated and the Bitcoin price for your Reservation may change.</p>
                                                <p style="margin-left:1.0in;">2. A very small transaction fee may be added by the Bitcoin network to the total cost in Bitcoin of your Reservation if you are sending Bitcoin from a non-Stripe® wallet. This fee covers the cost of verifying Bitcoin transactions. We have no control over this fee and we do not receive any portion of the fee. </p>
                                                <p style="margin-left:1.0in;">3. Bitcoin transactions are final. Once you initiate a Bitcoin transaction, you cannot cancel it; this is the policy of the Bitcoin network over which we have no control. Refunds, if any, are governed by our refund policy. </p>
                                                <p style="margin-left:1.0in;">4. If you attempt a payment via BitCoin wallet, in an amount of Bitcoins different from the exact cost of the Reservation indicated by Stripe®, your Confirmed Reservation will not complete. If, using a non-Stripe® wallet, you initiate a payment of Bitcoins different from the exact cost (plus transaction fees) of the Reservation indicated by Stripe®, your Confirmed Reservation will not complete; however your payment maybe processed by the Bitcoin network. If that incorrect payment is processed, your payment will be refunded by Stripe®. After an incorrect payment you will be required to re-submit payment for the correct amount in order to receive a Confirmed Reservation and the Bitcoin exchange rate may change (which we do not control). </p>
                                                <p style="margin-left:1.0in;">5. Transactions complete once confirmed. Once a Bitcoin transaction is submitted to the Bitcoin network, it will be unconfirmed for a period of time pending full verification of the transaction by the Bitcoin network. A transaction is not complete until it is fully verified and your Confirmed Reservation will not occur until the transaction is completely verified.</p>
                                                <p><br />
                                                   <strong>12. MEMBERSHIP AND AFFILIATE STATUS.</strong> Membership and Affiliate status are at the sole discretion of Company, and may be denied, or suspended or revoked at any time, with or without prior notice. Further, no individual or Commercial Entity may have more than one Membership, and Users should be aware that Company has and shall use technology to determine if any individual has attempted or succeeded in obtaining more than one Membership. In Such event, Such individual or Commercial Entity shall have the Membership suspended or revoked, and any or all Coupons forfeited at Company&rsquo;s sole discretion.
                                                </p>
                                                <p><br />
                                                   <strong>SECTION III. ADDITIONAL TERMS AND CONDITIONS FOR MEMBERS AND AFFILIATES</strong>
                                                </p>
                                                <p><strong>1. FOR MEMBERS</strong>. For Members, the following additional terms and conditions apply:</p>
                                                <p>A. The following information is not proprietary to you, and may, or may not, be considered PERSONALLY IDENTIFIABLE INFORMATION (see our PRIVACY POLICY). Provided however, any other provisions of this or any of our OTHER POLICIES to the contrary, if you use your Actual Name as your Username, YOUR ACTUAL NAME IS NOT CONSIDERED PERSONALLY IDENTIFIABLE INFORMATION! <strong style="text-decoration:underline;"><em>PLEASE, DO NOT USER YOUR ACTUAL NAME AS YOUR Username!</em></strong> YOUR Username MAY BE VISIBLE TO OTHER USERS! The following information will only be seen by you, and if necessary, by Company and/or Service Provider(s): any email directed specifically to you by Company or email sent by you to Company, information you provide to seek a Reservation, any use by you of the Site&rsquo;s Contact Us function, Opting In or Opting Out of our newsletter, My Account, My Info. My Coupons, Refer A Friend, Change Password, Change Email Address, Reservations, Requests Submitted, Reservations, Confirmed Reservations, FAQ and the Legal Corner and all submenus thereof. 
                                                   Please note the following: We don’t obtain/retain credit/debit card or checking account information on our Site; all payments are processed through the payment processor.
                                                </p>
                                                <p>B. Other than Voluntary Data Collection, Company does not claim ownership of the materials you provide to Site with regard to a request to become a Member.</p>
                                                <p>C. No compensation will be paid with respect to the use of Voluntary Data Collection as provided herein. Company is under no obligation to post or use any Voluntary Data Collection. You may provide and we may remove any Voluntary Data Collection at any time in Company&#39;s sole discretion.</p>
                                                <p>D. The use of fictitious names, whether by an individual or by Commercial Entities is FORBIDDEN.</p>
                                                <p>E. Reservations are valid ONLY for forty-eight (48) hours commencing at the time we email the Reservation to you. We are not responsible for any delays or failure of you to receive a Reservation in a timely fashion including, but not limited to, any issues with a third party server, internet connection, or your email system.</p>
                                                <p>F. A reservation becomes a Confirmed Reservation ONLY if you accept both the Reservation and make Payment in full to us within the timeframe set forth in Section III(1)(E) above. If you send an amount less than Payment in full and do not send the remaining balance for the Reservation within the time frame set forth in Section III(1)E above, you will not be entitled to a Confirmed Reservation.</p>
                                                <p>G. Once a Cancellation has been received by us, the previous Reservation made by us to make the Reservation which is being cancelled, is no longer valid. A Member must submit a new request for a Reservation once a legitimate Cancellation is made by the Member and then the Member must accept (make a Reservation and make payment in full) or decline the new Reservation (by not accepting the Reservation and not paying in full for the Reservation); all within the time frames set forth in the Cancellation Policy.</p>
                                                <p>H. CANCELLATION AND REFUND POLICY:</p>
                                                <p>(i).&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Definitions and Special Bitcoin policy:</p>
                                                <p style="margin-left:1.0in;">a. Cancellation Period shall mean that period of time stated in your Confirmed Reservation, Such period beginning at the time your Confirmed Reservation is emailed by us to you, and ending as of the check in date and time of the Hotel at the location of the Hotel designated in your Confirmed Reservation. By way of example, and not limitation, if your Hotel stay begins on August 11, your Confirmed Reservation states you have a seven (7) day cancellation period, and the Hotel&rsquo;s check in time is 11:00AM (all Hotel check in times and dates are the local times at dates where the where the Hotel is located), then you would have seven days, (168 hours) prior to 11:00AM on August 11 (measured by the Hotel&rsquo;s local date and time) in order to cancel. The Cancellation Period is NOT measured by the Member&rsquo;s local date and time.</p>
                                                <p style="margin-left:1.0in;">b. Cancellation and Refund Policy shall mean this Section III(1)(H) of this Terms and Conditions.</p>
                                                <p style="margin-left:1.0in;">c. Chargeback shall mean any attempt of any nature by Member to seek a credit or refund from a third party including, but not limited to, a credit card issuer, processor, merchant bank or like entity where we have provided Services in accordance with the Terms and Conditions.</p>
                                                <p style="margin-left:1.0in;">d. Reservation-The quote or quotes for a specific Hotel or Hotels provided by us to you based upon the information you supplied to us in requesting the quote. When we send you a Reservation, the Reservation will specify the specific Cancellation Policy applicable to that Reservation.</p>
                                                <p style="margin-left:1.0in;">e. Payment-shall mean the payment, in full, irrespective of manner or form used by you to pay us, in full, for the Reservation.&nbsp; No Confirmed Reservation shall be issued unless payment in full has been received by us.</p>
                                                <p style="margin-left:1.0in;">f. Refund shall mean a payment by us, if any, to you, for a cancellation of a Confirmed Reservation where Such cancellation by you is in complete compliance with our Terms and Conditions. The Refund Amount shall be the full amount paid to us for the Confirmed Reservation  subject to III(H)(i)g immediately below if you used Bitcoin as your payment method. If a Member paid only a partial payment(s) but never completely paid in full for the Reservation within the required timeframe for a Confirmed Reservation, the amount of the refund, if any, shall be the total amount of partial payments made subject to III(H)(i)g immediately below if you used Bitcoin as your payment method.</p>
                                                <p style="margin-left:1.0in;"> g. Whether your Confirmed Reservation qualifies for a refund is governed by this Terms and Conditions policy, regardless of whether you pay with Bitcoin or any other payment method. In order to receive a refund (if we grant one) on a Confirmed Reservation paid for with Bitcoin, you will have to follow certain procedures in order to claim your refund: </p>
                                                <p style="margin-left:1.5in;">1. A valid email address and a Stripe® account are required for refunds. Your refund will be issued through Stripe®, and you must have (or create) a Stripe® account in order to receive the refund. Stripe will send a refund notification email to the email address that you provided to Stripe®. THE EMAIL ADDRESS OF YOUR COINBASE ACCOUNT MUST BE IDENTICAL TO THE ONE YOU HAVE ON FILE WITH US AS A MEMBER AND WHICH YOU USED TO OBTAIN YOUR QUOTE! </p>
                                                <p style="margin-left:1.5in;">2. If you do not have a Stripe® account associated with the email address which you used to obtain your Reservation, then visit www.stripe.com ; establish an account and/or make sure any existing Stripe® account has the same email address as your email address that you used to obtain your Reservation.  </p>
                                                <p style="margin-left:1.5in;">3. If you fail to access your Stripe® account, making sure you have the identical mail address for your Stripe® account as the one you used to obtain your Reservation, then within thirty (30) days, the funds will be returned to us. If a refund notification email is sent to an email address associated with an existing Stripe® account, the refund will automatically be credited by Stripe® to the associated Stripe® account. You agree that, if you are unable to (1) access the email address associated with the email you used to obtain your Reservation, or (2) to use the Stripe® service or to access a Stripe® account, you will not be able to receive a refund for your Confirmed Reservation even if we have authorized Such a refund. </p>
                                                <p style="margin-left:1.5in;">4. Refunds, if any, are issued for the USD value as set forth in this Policy. Your refund will be issued by Stripe® in Bitcoin for that USD value, less any applicable fees, including any cancellation fees. Your refund will be converted from USD to Bitcoin based on an exchange rate set by Stripe® at the time we initiate the refund. You acknowledge that if the value of Bitcoin against USD has risen since the time of your purchase, you will receive less Bitcoin you might otherwise have received an alternative payment method. </p>
                                                <p style="margin-left:.5in;">(ii). Other than as set forth is Section III(1)(H)(iii), below, no Refund shall be made after the termination of the Cancellation Period under any circumstances; any exception thereto shall be in our complete and sole discretion. Notwithstanding the foregoing in this Section III(1)(H)(ii), Company MAY, in its complete and sole discretion, make a Refund, in the form of a Coupon in any amount not to exceed One Hundred Dollars USD ($100.00) if the Cancellation Period was listed as &ldquo;NON-REFUNDABLE&rdquo;; the exercise of Such sole discretion by Company is a courtesy that the Company may, or may not, elect to extend to a Member.</p>
                                                <p style="margin-left:.5in;">(iii). If between the time we provide a Member with a Reservation, and the Member makes a Payment, and we are unable to book the Hotel for the Member at the rate in the Reservation because the Hotel has increased its rate to us, the Member shall have a choice of a full Refund of the Payment or the option to make an additional payment to cover the cost of Such increase, but not both.</p>
                                                <p style="margin-left:.5in;">(iv). No Refund shall be made if the Member stays for a shorter period of time than that set forth in the Confirmed Reservation.</p>
                                                <p style="margin-left:.5in;">(v). No Refund, whether in whole or in part, shall be made if the price charged to us by the Hotel decreases between the time we provide a Member with a Reservation, and the Member makes a Payment.</p>
                                                <p>(vi). No Refund shall be made if the Member cancels directly with the Hotel.</p>
                                                <p style="margin-left:.5in;">(vii). Under no circumstances shall a Member attempt to receive, or receive, any credit or payment, whether in whole or in part, of any funds paid by us to Hotel. Member understands and agrees that we will take any and all steps necessary, including legal action, and including without limitation, recovery of attorney&rsquo;s fees, legal costs, other fees and any other damages to which we are entitled, to recover Such credit or payment.</p>
                                                <p style="margin-left:.5in;">(viii). Member understands and agrees that we will dispute any Chargeback to the fullest possible legal extent, including but not limited to, the commencement of legal action including without limitation, recovery of attorney&rsquo;s fees, legal costs, other fees and any other damages to which we are entitled, to recover Such credit, or reversal of Payment to the Member and in addition, we may choose, in our sole discretion, to report to the credit/debit card issuer any fraudulent Chargeback.</p>
                                                <p style="margin-left:.5in;">(ix). Coupons. Coupons are an amount, in USD, that will be automatically deducted from your Reservation if you elect to redeem one or more of your earned Coupons. No more than one (1) coupon may be redeemed for any single Reservation.</p>
                                                <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (x). General Terms and Conditions for earning Coupons.</p>
                                                <p style="margin-left:1.0in;">a. The ways in which to earn Coupons, the number of Coupons that may be used on any single Reservation, expiration date of Coupons, or the maximum number or dollar amount of coupons that may be accumulated, may be changed, or discontinued at any time by Company, without prior notice except as otherwise set forth in this Section III(1)(H)(x).</p>
                                                <p style="margin-left:1.0in;">b. Company does place an expiration date on all type(s) of Coupon(s). Any changes in expiration date(s) of any Coupon(s), including those previously issued, will be reflected in a change to this Policy.</p>
                                                <p style="margin-left:1.0in;">c. If there is a change by Company in the number of Coupons that may be used on any single Reservation, how Coupons may be earned, or the maximum number or dollar amount of coupons that may be accumulated, any Such changes will be reflected in a change to this Policy.</p>
                                                <p style="margin-left:1.0in;">d. At the present date and time, there is no maximum number or dollar amount of Coupons that may be accumulated by a Member; all of which may change or be discontinued as set forth herein.</p>
                                                <p style="margin-left:1.0in;">e. Affiliate Bonus.&nbsp; A Potential Member shall receive a Coupon in the amount set forth on the Site as of the date the Potential Member uses an Affiliate Banner or an Affiliate Link to enter the Site and Such Potential Member becomes a Member (the &ldquo;New Member&quot;). The Coupon will be credited to the New Member instantly upon becoming a Member, but Such Coupon may be used ONLY for a Confirmed Reservation in the amount of One Hundred Fifty USD ($150.00) or more.</p>
                                                <!--<p style="margin-left:1.0in;">f. Refer a Friend Bonus. When a Member (the &ldquo;Referring Member&rdquo;) uses the Referring Member&rsquo;s personal link (as part of the Refer a Friend process) to have a third party become a Member (the &ldquo;New Member&rdquo;), The New Member will instantly receive a Refer A Friend Coupon in the amount set forth on the Site, but Such Coupon may be used ONLY for a Confirmed Reservation in the amount of One Hundred Fifty USD ($150.00) or more. The Coupon will be credited to the New Member instantly. In addition, once the New Member makes their first Confirmed Reservation which is not cancelled, refunded or charged back (the &ldquo;First New Member Trip&rdquo;), the Referring Member will receive five percent (5%) of the amount of the New Member&rsquo;s First New Member Trip. The amount of the First New Member&rsquo;s Trip is the full amount paid by the New Member to Company [less any Coupon(s)] in order for the New Member to receive a Confirmed Reservation. This five percent (5%) is valid ONLY for the First New Member Trip made by the New Member and not any subsequent trips made by that New Member. The five percent will be in the form of a Coupon to the Referring Member, ninety (90) days AFTER the check-out date of the First New Member Trip. However, if you refer yet a different friend who then also becomes a Member, you will receive the same benefits. By way of example and not limitation: You are a Member whose name is Kate. You give your friend Sam your personal Refer a Friend Link. Sam uses that link to become a Member. Sam instantly receives a Refer A Friend Coupon. Sam then accepts a Quote in the gross amount of $1,020.00, and applies a Coupon valued at $20.00. Sam the pays the Company $1000.00 and receives a Confirmed Reservation, with a checkout date of 5/1/15. Sam does not cancel, get a refund of, or charge back his Confirmed Reservation. You Kate, then, on 7/29/15, receive a Coupon in the amount of $50.00 (5% of $1,000.00). If Sam makes any additional Confirmed Reservations, you will NOT receive five percent (5%) of any of his subsequent trips. &nbsp;&nbsp;&nbsp;</p>-->
                                                <p style="margin-left:1.0in;">f. Direct Sign Up Bonus. A Member shall receive a Coupon in the amount set forth on the Site as of the date the Potential Member accesses the Site directly and Such Potential Member becomes a Member. The Coupon will be credited to the Member instantly.</p>
                                                <p style="margin-left:1.0in;">g. A New Member may receive only an Affiliate Bonus, or a Refer a Friend Bonus or a Direct Sign Up Bonus; a Member shall be entitled to only ONE bonus depending on the method the Potential Member used initially to sign up as a Member. However, a User who is already a Member and is a Referring Member as defined above, may receive a Refer A Friend Bonus commission for each new individual that the Referring Member uses his/her link to sign up a New Member as set forth in Section III(1)(H)(x)(f)above.</p>
                                                <p><br />
                                                   <strong>2. FOR AFFILIATES</strong>. For Affiliates, the following additional terms and conditions apply:
                                                </p>
                                                <p>A. The following information is not proprietary to you, and may, or may not, be considered PERSONALLY IDENTIFIABLE INFORMATION (see our PRIVACY POLICY</p>
                                                <p>B. Other than Voluntary Data Collection, Company does not claim ownership of the materials you provide to Site with regard to a request to become an Affiliate.</p>
                                                <p>C. No compensation will be paid with respect to the use of Voluntary Data Collection, as provided herein. Company is under no obligation to post or use any Voluntary Data Collection. You may provide and we may remove any Voluntary Data Collection at any time in Company&#39;s sole discretion.</p>
                                                <p>D. While the following information is not proprietary to you, it will only be seen by you, and if necessary, by Company and/or its Service Provider(s): any email directed specifically to you by Company or email sent by you to Company, information you provide to become an Affiliate, except as set forth in or PRIVACY POLICY, any use by you of the Site&rsquo;s Change Password, Change Email Address, ACCOUNT INFO, PAYOUT PREFERENCE, STATISTICS, PAYOUT HISTORY AFFILIATE GUIDE, CONTACT US, OPTING IN OR OUT of our newsletter,&nbsp; FAQ and the Legal Corner.</p>
                                                <p>E. The following information is not proprietary to you, is not PERSONALLY IDENTIFIABLE INFORMATION, and may be seen by a Member(s), Users, other Affiliates and the general public: your Banner advertisement and Affiliate Link provided by Company. You may not make any changes to the language or content of the Banner advertisement or Affiliate Link supplied by Company without prior authorization from Company. You will indemnify and hold harmless Company from any claims or any nature or damages or other legal actions for any changes to you make to the Banner we supply to, and/or related in any way to, any legal claims or legal action as a result of any language you add, delete or modify with regard to the Banner or Affiliate Link or adjacent to them as to the Company&rsquo;s Services.</p>
                                                <p>F. CANCELLATION AND REFUND POLICY: Please refer to Section III(1)(H) of this POLICY so that you are aware of our policy regarding cancellations and refunds to Members. The amount of any refund or chargeback by a Member who made a reservation on our Site through your Banner will be deducted from your Affiliate Payment.</p>
                                                <p>G. You may have as few as one or an unlimited number of websites upon which you may post your Banner or Affiliate Link provided that you are authorized to make Such a posting; the Company shall not be responsible for any fees of any nature that you may incur for the posting of your Banner(s), including, but not limited to, payment to a third party to post your Banner and/or Affiliate Link on their website(s), hosting costs, etc. Provided however, you SHALL NOT post your Banner and/or Affiliate Link on any website that has links to any other HOTEL booking sites or similar sites that a reasonable and prudent business person would determine to be in competition with Company.</p>
                                                <p>H. Affiliate Payment:</p>
                                                <p style="margin-left:.5in;">(i). Payment Preference. You may choose to have your Affiliate Payment made either via PayPal&reg; or Western Union&reg; and you may change it at any time by going to the Affiliate Home Page, Payout Preferences, &ldquo;I preferred to be paid&rdquo;, changing your preference and clicking on &ldquo;Update&rdquo; section. Any fees for sending your Affiliate Payment will be deducted from the amount of your Affiliate Payment, and you will be responsible for fees, if any, that you must pay as the recipient of the Affiliate Payment.</p>
                                                <p style="margin-left:.5in;">(ii) Also, under Payment Preference, you may choose the dollar amount you have accumulated at which point you wish to be paid. This section of the Payment Preference says &ldquo;Please only send me money once I reach: $X.XX&rdquo; You may enter any dollar amount from $10.00USD to $2,999.99&rdquo;. Currently, the default value is $100.00USD. You may change the dollar amount at any time by going to the Affiliate Home Page, Payout Preferences, &ldquo;Please only send me money when I reach $X.XX&rdquo;, changing your preference and clicking on the &ldquo;Update&rdquo; section.<br />
                                                   (iii). All Affiliate Payments are processed every two (2) weeks.
                                                </p>
                                                <p style="margin-left:.5in;">(iv). How the amount of Affiliate Payments is determined: Company has a commission plan for Affiliates as follows:</p>
                                                <ol style="list-style-type:lower-alpha;">
                                                   <li>Five percent (5%) commission up to $9,999.99 per month, in Confirmed Reservations as a result of a Member having been directed to our Site by the Affiliate, and which Confirmed Reservation(s) have not been canceled, refunded, or charged back.</li>
                                                   <li>Five and half percent (5.5%) commission between $10,000-$49,999.99 per month, in Confirmed Reservations as a result of a Member having been directed to our Site by the Affiliate, and which Confirmed Reservation(s) have not been canceled, refunded, or charged back.</li>
                                                   <li>Six percent (6%) commission on $50,000.00 or more, per month, in Confirmed Reservations as a result of a Member having been directed to our Site by the Affiliate, and which Confirmed Reservation(s) have not been canceled, refunded, or charged back.</li>
                                                   <li>Commencing on the stated check out date and time of the Hotel of the Confirmed Reservation for a Member, the commission for Such Confirmed Reservation is posted to the commission account of the Affiliate (the &ldquo;Posting Date&rdquo;). However, to allow for Company to process Cancellations, Refunds and Chargebacks, the Affiliate will not be eligible for payment of Such commission until ninety (90) days after the Posting Date, and then will be paid on the first two (2) week payment cycle after the Posting Date.</li>
                                                   <li>Unlike other websites, this Site will, upon the first time a Member makes a Confirmed Reservation using the Affiliate&rsquo;s Banner or Affiliate Link, for that first Confirmed Reservation and for ALL Confirmed Reservations of that Member thereafter, the Affiliate will continue to earn commission, subject to Section III(2)(H)(iv)(a-d) above.</li>
                                                   <li>The Pending Transactions section of the Affiliate portion of the Site will give you an APPROXIMATE date of when any earned commission.</li>
                                                </ol>
                                                <p>(v). Other.</p>
                                                <p style="margin-left:1.0in;">a. Please remember that each new User who, through your Affiliate Link(s), becomes a Member (remember there is no charge for anyone to become a Member) will receive a one-time Twenty Dollar ($20.00) off their first Confirmed Reservation, subject to the provisions of Section III(1)(H)(x)</p>
                                                <p style="margin-left:1.0in;">b. Top Affiliate Award. Commencing on the date the first User successfully becomes an Affiliate, and every 365 days thereafter (the &ldquo;Award Period&rdquo;), we may have annual incentives that we give out to our very best affiliate(s) (the &ldquo;Top Affiliate&rdquo;). Such incentives may be as luxurious hotel stays and/or flights paid by <strong>HotelsDifferently<sup>sm</sup></strong>. The designation as a Top Affiliate award recipient is based upon the highest commission amount(s) earned by our Affiliates in the Award Period. We may in our sole discretion award incentives to more than one Top Affiliate in descending order of commissions earned during an Award Period. Be our TOP AFFILIATE, MAKE MONEY and get rewarded with LUXURIOUS GIVEAWAYS!!! The number of Top Affiliate Awards per Award Period, the dollar value and the type of award are at the SOLE discretion of the Company, and the Top Affiliate Award program may be changed, modified, edited or deleted at any time, by the Company, without prior notice. A Top Affiliate Award recipient may elect to transfer their award to another individual by supplying the necessary information to Company via the Contact Us function of the Site. Provided however, once a Top Affiliate Award recipient makes Such a transfer, Such transfer is irrevocable and may NOT BE changed. In addition, the Top Affiliate who makes Such a transfer shall be responsible for any and all taxes for Such a transfer, including, but not limited to, any gift tax (if applicable).</p>
                                                <p style="margin-left:1.0in;">c. Company. The amount of commission, the amount of dollar ranges for various commission levels, Posting Date, payment cycles, and Affiliate vouchers for travel on our Site for a Member&rsquo;s first Confirmed Reservation may be changed at any time by Company.<br />
                                                   <br />
                                                   &nbsp;
                                                </p>
                                                <p><strong>SECTION IV. APPLICABLE TO ALL USERS AND ALL OTHER POLICIES: EACH AND EVERY TERM, CONDITION AND PROVISION SET FORTH BELOW IS APPLICABLE TO ALL USERS, AND FURTHER EACH AND EVERY TERM, CONDITION AND PROVISION SET FORTH BELOW APPLIES TO, IS INTEGRATED INTO, AND INCORPORATED BY REFERENCE INTO EACH OF THE OTHER POLICIES:</strong></p>
                                                <p><br />
                                                   <strong>1. THIRD PARTY ACCOUNTS</strong>
                                                </p>
                                                <p>You may be able to connect your Company Account to THIRD PARTY ACCOUNTS, including, but not limited to, common and popular social media sites. By connecting your Affiliate Account to your third party account, you acknowledge and agree that you are consenting to the continuous release of information about you to others (in accordance with your privacy settings on those third party sites). If you do not want information about you to be shared in this manner, do not use this feature.</p>
                                                <p><br />
                                                   <strong>2. INTERNATIONAL USERS</strong>
                                                </p>
                                                <p>The Service is controlled, operated and administered by Company from pursuant to the laws of the United States of America. All transactions are deemed to have occurred, for all purposes, within the United States of America regardless of the citizenship, domicile or location of any User, Member or Affiliate. If you access the Service from a location outside of the United States of America, you are responsible for compliance with all local laws of Such locations. You agree that you will not use the Company&rsquo;s content accessed through the Site in any country or in any manner prohibited by any applicable laws, restrictions or regulations.</p>
                                                <p><br />
                                                   <strong>3. INDEMNIFICATION</strong>
                                                </p>
                                                <p>You agree to indemnify, defend and hold harmless Company, its officers, directors, employees, agents and third parties, for any losses, costs, liabilities and expenses (including reasonable attorneys&#39; fees) relating to or arising out of your use of or inability to use the Site or services, your violation of any TERMS AND CONDITIONS AND ALL OTHER POLICIES of this Agreement or your violation of any rights of a third party, or your violation of any applicable laws, rules or regulations or violation of any OTHER POLICIES. Company reserves the right, at its own cost, to assume the exclusive defense and control of any matter subject to indemnification by you, in which event you will fully cooperate with Company in asserting any available defenses.</p>
                                                <p><br />
                                                   <strong>4. LIABILITY DISCLAIMER</strong>
                                                </p>
                                                <p>THE INFORMATION, SOFTWARE, PRODUCTS, AND SERVICES INCLUDED IN OR AVAILABLE THROUGH THE SITE MAY INCLUDE INACCURACIES OR TYPOGRAPHICAL ERRORS. CHANGES ARE PERIODICALLY ADDED TO THE INFORMATION HEREIN. <strong>HotelsDifferently, LLC</strong>, AND/OR ITS SUPPLIERS MAY MAKE IMPROVEMENTS AND/OR CHANGES IN THE SITE AT ANY TIME.</p>
                                                <p><strong>HotelsDifferently, LLC</strong>, AND/OR ITS SUPPLIERS MAKE NO REPRESENTATIONS ABOUT THE SUITABILITY, RELIABILITY, AVAILABILITY, TIMELINESS, SAFETY, PRIVACY AND ACCURACY OF THE INFORMATION, SOFTWARE, PRODUCTS, SERVICES AND RELATED GRAPHICS CONTAINED ON THE SITE FOR ANY PURPOSE. TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, ALL Such INFORMATION, SOFTWARE, PRODUCTS, SERVICES AND RELATED GRAPHICS ARE PROVIDED &quot;AS IS&quot; WITHOUT WARRANTY OR CONDITION OF ANY KIND. <strong>HotelsDifferently, LLC</strong>, AND/OR ITS SUPPLIERS HEREBY DISCLAIM ALL WARRANTIES AND CONDITIONS WITH REGARD TO THIS INFORMATION, SOFTWARE, PRODUCTS, SERVICES AND RELATED GRAPHICS, INCLUDING ALL IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT.</p>
                                                <p>TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, IN NO EVENT SHALL <strong>HotelsDifferently, LLC</strong>, AND/OR ITS SUPPLIERS BE LIABLE FOR, AND YOU WAIVE AND RELEASE <strong>HotelsDifferently, LLC</strong> &nbsp;FROM ANY AND ALL DIRECT, INDIRECT, PUNITIVE, INCIDENTAL, SPECIAL, CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF USE, DATA OR PROFITS, ARISING OUT OF OR IN ANY WAY CONNECTED WITH THE USE OR PERFORMANCE OF THE SITE, YOUR VIOLATION OF THE TERMS AND CONDITIONS OR ANY OTHER POLICY OF THE SITE, INCLUDING ANY COMPANY REMEDIES CONTAINED THEREIN, WITH THE DELAY OR INABILITY TO USE THE SITE OR RELATED SERVICES, THE PROVISION OF OR FAILURE TO PROVIDE SERVICES, OR FOR ANY INFORMATION, SOFTWARE, PRODUCTS, SERVICES AND RELATED GRAPHICS OBTAINED THROUGH THE SITE, OR OTHERWISE ARISING OUT OF THE USE OF THE SITE, WHETHER BASED ON CONTRACT, TORT, NEGLIGENCE, STRICT LIABILITY OR OTHERWISE, EVEN IF <strong>HotelsDifferently, LLC</strong>, OR ANY OF ITS SUPPLIERS HAS BEEN ADVISED OF THE POSSIBILITY OF DAMAGES. BECAUSE SOME STATES/JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF LIABILITY FOR CONSEQUENTIAL OR INCIDENTAL DAMAGES, THE ABOVE LIMITATION MAY NOT APPLY TO YOU. IF YOU ARE DISSATISFIED WITH ANY PORTION OF THE SITE, OR WITH ANY OF THESE TERMS AND CONDITIONS AND ALL OTHER POLICIES OF USE, YOUR SOLE AND EXCLUSIVE REMEDY IS TO DISCONTINUE USING THE SITE.</p>
                                                <p>COMPANY IS NOT, AND WILL NOT BE, LIABLE OR RESPONSBILE FOR ANY THIRD PARTY CONTENT ON THIS SITE.</p>
                                                <p><br />
                                                   <strong>5. TERMINATION/ACCESS RESTRICTION/APPLICABLE LAWS/JURISDICTION/VENUE/CONTRACT FORMATION/OTHER</strong>
                                                </p>
                                                <p>A. Company reserves the right, in its sole discretion, to terminate your access to the Site and the related services or any portion thereof at any time, without notice. To the maximum extent permitted by law, this agreement is governed by the laws of the State of Nevada, United States of America, without reference to its choice of laws or conflict of laws provision. User hereby consents to the exclusive jurisdiction and venue of courts in Nevada in all disputes arising out of or relating to the use of the Site. In any action to enforce the provisions of this TERMS AND CONDITIONS OR ANY OTHER POLICIES, venue and jurisdiction shall lie in the Las Vegas Township Justice Court, or in the Nevada Eighth Judicial District Court, or in the United States District Court, District of Nevada-Las Vegas, and each User hereby irrevocably consents to the jurisdiction of Such Courts and waives any argument or assertion of forum non conveniens.</p>
                                                <p>Use of the Site is unauthorized in any jurisdiction that does not give effect to all provisions of these TERMS AND CONDITIONS AND ALL OTHER POLICIES including, without limitation, this provision. </p>
                                                <p>You agree that no joint venture, partnership, employment, or agency relationship exists between you and Company as a result of this Agreement or use of the Site. Company's performance of this Agreement is subject to existing laws and legal process, and nothing contained in this Agreement is in derogation of Company's right to comply with governmental, court and law enforcement requests or requirements relating to your use of the Site or information provided to or gathered by Company with respect to Such use. If any part of this Agreement is determined to be invalid or unenforceable pursuant to applicable law including, but not limited to, the warranty disclaimers and liability limitations set forth above, then the invalid or unenforceable provision will be deemed superseded by a valid, enforceable provision that most closely matches the intent of the original provision and the remainder of the Agreement shall continue in effect. </p>
                                                <p>Unless otherwise specified herein, this Agreement constitutes the entire Agreement between the User and Company with respect to the Site and it supersedes all prior or contemporaneous communications and proposals, whether electronic, oral or written, between the User and Company with respect to the Site. A printed version of this Agreement and of any notice given in electronic form shall be admissible in judicial or administrative proceedings based upon or relating to this Agreement to the same extent and subject to the same conditions as other business documents and records originally generated and maintained in printed form. It is the express policy of the Company and a User that this TERMS AND CONDITONS AND OTHER POLICIES and all related documents are to be written in English, and this English language version, not any translation into any other language, shall be controlling. Neither this TERMS AND CONDITION NOR ANY OTHER POLICIES nor any term hereof may be amended, waived, discharged or terminated other than by Company.</p>
                                                <p>B. Contract Formation:</p>
                                                <p style="margin-left:45.0pt;">(i). For all Users, you explicitly agree that when you enter this Site, you agree to be legally bound to all TERMS AND CONDITIONS and all OTHER POLICIES, and you entry into the Site is your Electronic Signature and you agree that you have entered into a legal binding contract by entering the site, the consideration for Such contract being your ability to view this website in exchange for us being able to share with you certain information about our Services, and for both parties, other good and valuable consideration, the value of which is acknowledged by the parties, .</p>
                                                <p style="margin-left:45.0pt;">(ii). When a User becomes a Member and/or an Affiliate, and you click the checkbox entitled &ldquo;I hereby accept the Terms and Conditions, Privacy Policy , Intellectuall Property Policy&rdquo;, DMCA Policy and all OTHER POLICIES,  you have provided your Electronic Signature and have entered into a legal binding contract agreeing to all TERMS AND CONDITIONS and all OTHER POLICIES, the consideration for Such contract being:</p>
                                                <ol style="list-style-type:lower-alpha;">
                                                   <li>A Member having the ability to view the Member portion of this Site, earn and redeem Coupons, request and receive Reservations and the ability to make a Confirmed Reservation, as well as all other right and privileges as a Member, and our consideration being the ability to provide you Reservations, award Coupons, provide Confirmed Reservations and all of our other Services and features available to Members, and for both parties, other good and valuable consideration, the value of which is acknowledged by the parties.</li>
                                                   <li>An Affiliate having the ability to view the Affiliate portion of this Site, obtain an Affiliate Link, earn commissions, receive a Banner and Affiliate Link for said Affiliate to place on one or more websites and potentially earn additional rewards for being a Top Affiliate, as well as all other right and privileges as an Affiliate, and our consideration being the ability to provide you an Affiliate Link, a Banner, potentially provide you a reward for being a Top Affiliate and for us to receive income from Users who have become Members based on your referral of the Member to us and for both parties, other good and valuable consideration, the value of which is acknowledged by the parties.</li>
                                                </ol>
                                                <ol style="list-style-type:lower-roman;">
                                                   <li value="3">You acknowledge that all Electronic Communications from us to you regarding Reservations, Confirmed Reservations, Cancellations, Refunds, Commissions, Top Affiliate Incentives and all applicable Site pages are in compliance with all laws and judicial determinations of the United States of America relative to contract law.</li>
                                                </ol>
                                                <p><br />
                                                   <strong>6. CHANGES TO TERMS AND CONDITIONS AND ALL OTHER POLICIES.</strong> Company reserves the right, in its sole discretion, without prior notice, to change the TERMS AND CONDITIONS AND ALL OTHER POLICIES, under which Site is offered. The most current version of this TERMS AND CONDITIONS AND ALL OTHER POLICIES will supersede all previous versions of each respective POLICY. Company encourages you to periodically review the TERMS AND CONDITIONS AND ALL OTHER POLICIES to stay informed of our updates. You may find links to the most current TERMS AND CONDITIONS AND ALL OTHER POLICIES on the Home Page of this Site.
                                                </p>
                                                <p><br />
                                                   <strong>7. DELAYS OR OMISSIONS</strong>. No delay or omission to exercise any right, power or remedy accruing to any Company upon any breach or default of any User under this TERMS AND CONDITIONS OR ANY OTHER POLICIES shall impair any Such right, power or remedy of Company, nor shall it be construed to be a waiver of any Such breach or default, or an acquiescence therein, or of, or in, any similar breach or default thereafter occurring; nor shall any waiver of any single breach or default be deemed a waiver of any other breach or default theretofore or thereafter occurring. Any waiver, permit, consent or approval of any kind or character on the part of Company of any breach or default under the TERMS AND CONDITIONS OR ANY OTHER POLICIES, or any waiver on the part of Company of any provisions or conditions of the TERMS AND CONDITIONS OR ANY OTHER POLICIES, must be in writing and shall be effective only to the extent specifically set forth in Such writing or as provided in the TERMS AND CONDITIONS OR OTHER POLICIES.
                                                </p>
                                                <p><br />
                                                   <strong>8. ENFORCEMENT</strong>. User agrees that irreparable damage for which money damages would not be an adequate remedy would occur in the event that any of the provision of this TERMS AND CONDITIONS OR ANY OTHER POLICIES were not performed in accordance with its specific terms or was otherwise breached. It is accordingly agreed that, in addition to any other remedies the Company may have at law or equity, the Company shall be entitled to seek an injunction or injunctions, without the necessity to post bond, to prevent Such breaches of this TERMS AND CONDITIONS OR ANY OTHER POLICIES and to enforce specifically the terms hereof.
                                                </p>
                                                <p><br />
                                                   <strong>9. CONSTRUCTION</strong>. The normal rule of construction that any ambiguity or uncertainty in a writing shall be interpreted against the Company in drafting this TERMS AND CONDITIONS OR ANY OTHER POLICIES of, this Site shall not apply to any action on these TERMS AND CONDITIONS OR ANY OTHER POLICIES of this Site.
                                                </p>
                                                <p><br />
                                                   <strong>10. FORCE MAJEURE.</strong> The Company is not liable for failure to perform the Company&rsquo;s obligations, if any, if Such failure is as a result of Acts of God (including fire, flood, earthquake, storm, hurricane or other natural disaster), war, invasion, act of foreign enemies, hostilities (regardless of whether war is declared), civil war, rebellion, revolution, insurrection, military or usurped power or confiscation, terrorist activities, nationalization, government sanction, blockage, embargo, labor dispute, strike, lockout or interruption or failure of electricity, internet, telephone or other utility service (&lsquo;Force Majeure&rsquo;).&nbsp;
                                                </p>
                                                <p><br />
                                                   <strong>11. CONFLICT</strong>. If there is any conflict or ambiguity between this TERMS AND CONDITIONS and any OTHER POLICIES, the terms, conditions and provisions of this TERMS AND CONDITIONS policy shall prevail, provided that Such interpretation is consistent with Company&rsquo;s intent.
                                                </p>
                                                <p><br />
                                                   <strong>12.</strong> <strong>PAROL EVIDENCE</strong>. No parol evidence may be introduced to contravene or dispute this TERMS AND CONDITIONS OR ANY OTHER POLICIES except Such parol evidence may be introduced by Company as to its intent as to the interpretation of this TERMS AND CONDITIONS and any OTHER POLICY.
                                                </p>
                                                <p><br />
                                                   <strong>13. CONTACT US</strong>
                                                </p>
                                                <p>Company welcomes your questions or comments regarding the TERMS AND CONDITIONS AND ALL OTHER POLICIES at the address listed below. 
                                                   <br/><strong>HotelsDifferently, LLC</strong> <br/>
                                                   3651 Lindell Road, D141<br/>
                                                   Las Vegas, Nevada, 89103<br/>
                                                   United States of America<br/>
                                                </p>
                                                <p><br />
                                                   <strong>14. FAILURE TO ABIDE OR ACCEPT. If you DO NOT AGREE TO ABIDE BY, OR FULLY ACCEPT, and ELECTRONICALLY SIGN the TERMS AND CONDITIONS AND ALL OTHER POLICIES, you MUST IMMEDIATELY EXIT THIS SITE. Links to these TERMS AND CONDITIONS AND ALL OTHER POLICIES may be found on the Home Page of this Site.</strong>
                                                </p>
                                                <p><br />
                                                   <strong>15. </strong><strong>ORDER OF PREFERENCE IN INTERPRETATION</strong>.
                                                </p>
                                                <p>A.&nbsp; If there is any conflict between any definitions or provisions of this TERMS AND CONDITIONS and any OTHER POLICY, this TERMS AND CONDITIONS POLICY shall prevail.</p>
                                                <p>B.&nbsp; If there is any conflict between any language in contained within our Site, including but not limited to any FAQ (Frequently Asked Questions), and any definitions or provisions of this TERMS AND CONDITIONS and any OTHER POLICY, then the definitions or provisions of this TERMS AND CONDITIONS and any OTHER POLICY shall prevail over any Such language within the Site, in the order of precedence as set forth herein and otherwise in this POLICY.</p>
                                                <p><br />
                                                   <strong>16. ELECTRONIC SIGNATURE ACT</strong>. By entering this Site, you are, pursuant to the Electronic Signature Act, 28 U.S.C. § 1746 indicating:
                                                </p>
                                                <p>A. Your unconditional acceptance and agreement to abide by the this TERMS AND CONDITIONS AND ALL OTHER POLICIES and,</p>
                                                <p>B. That you are of MINIMUM AGE, and</p>
                                                <p>C. You are submitting an unsworn declaration, certificate and verification, in writing of your agreement with, and that you will abide by, this TERMS AND CONDITIONS AND ALL OTHER POLICIES, that is subscribed by you, as true under penalty of perjury, and dated, in substantially the following form: </p>
                                                <p style="margin-left:45.0pt;">(i) If executed without the United States: “I declare and verify,  under penalty of perjury under the laws of the United States of America that the foregoing is true and correct”. Executed on the date on which entered this Site, and you accept your entrance to this Site as your binding, legal signature, for the purposes of this declaration and verification, or</p>
                                                <p style="margin-left:45.0pt;">(ii) If executed within the United States, its territories, possessions, or commonwealths: “I declare and verify, under penalty of perjury that the foregoing is true and correct”. Executed on the date on which you entered this Site, and you accept your entrance to this Site as your binding, legal signature, for the purposes of this declaration and verification.</p>
                                                <p>&nbsp;</p>
                                                <p><strong>17. CHANGES TO THIS POLICY. </strong>Company reserves the right, at any time, and for any reason, in its sole discretion to change this POLICY. The Effective Date of the most recent version of this POLICY will always be displayed at the very beginning of this POLICY, and the POLICY shall be effective as of said date. Users are encouraged to frequently check this POLICY and all OTHER POLICIES of this Site.</p>
                                                <p><strong>18. AIRLINE ITINERARIES, RESERVATION(S), BOOKINGS, PRICING, PURCHASES, CANCELLATIONS AND OTHER ACTIVITIES RELATING TO AIRLINES. </strong>
                                                <p>A. Effective as of December 28, 2016 or at some point thereafter, we are happy to be able to provide you access to what we believe, based on our research, is a Linked Site that may offer you extremely competitive pricing for domestic airfare in the United States AND in addition, we think you may find almost unbelievably low pricing for international travel when compared against other airfare websites. </p>
                                                <p>B. We are providing you the opportunity for access to this airfare website (a “Commercial Advertiser”) as a courtesy to you via  a “Linked Site”. Such Linked Site is hereafter referred to as  the “Airfare Site”</p>
                                                <p>C. The “Airfare Site” is NOT under the control of Company and Company is not responsible for the contents of the Airfare Site, including without limitation, any link contained within the Airfare Site, or any changes or updates to the “Airfare Site”. Company is providing this “Airfare Site” to you only as a convenience for you, and the inclusion of the Airfare Site does not imply endorsement by Company of the Airfare Site  or any association with its operators, except as set forth in Section 18(D) below. </p>
                                                <p> D. The link in our Site to the “Airfare Site” may mean that Company accepted payment or will receive commissions from the “Airfare Site”. The Company does not endorse the “Airfare Site” and does not warrant any information in the “Airfare Site” including any links included within the “Airfare Site”. Company does not provide any contact information for the “Airfare Site” other than the contact information provided by the “Airfare Site”.</p>
                                                <p>E. If you elect to use the link we have provided to the Airfare Site, you should read carefully the terms and conditions, and other polices and procedures of the Airfare Sites, including but not limited to reservations, bookings, cancellations, refunds, disputes and payment (the “Rules”). Once you have chosen to link from our Site to the Airfare Site, you are subject to the aforementioned “Rules” of the “Airfare Site”. </p>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="panel panel-default">
                                 <div class="panel-heading">
                                    <h4 class="panel-title">
                                       <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion4" href="#collapse_4_4">
                                       BitCoin policy
                                       </a>
                                    </h4>
                                 </div>
                                 <div id="collapse_4_4" class="panel-collapse collapse">
                                    <div class="panel-body pricacy-width">
                                       <!--<h1><?php // echo $this->page_name;       ?></h1>-->
                                       <div class="main_cont">
                                          <div class="pagetitle margin-bottom-10"">
                                              <p> BitCoin policy content needed</p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="form-group margin-top-10">
                                 <!--                                     
                                
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="form-actions fluid margin-top-40">
                     <div class="row">
                        <div class="col-md-12">
                           <div class="text-center col-md-12" style="margin: 30px 0 0;">
                              <!-- <a href="javascript:;" class="btn red_btn button-previous listing_btn" style="padding: 8px 20px; margin: 0 8px;">
                                 <em class="m-icon-swapleft"></em> Back
                                 </a> -->
                              <!--                                        <a href="javascript:;" class="btn red_btn listing_btn" id="reset_form" style="padding: 8px 20px; margin: 0 8px;">
                                 Back <em class="m-icon-swapright m-icon-white"></em>
                                 </a>-->
                              <!-- <a href="javascript:;" class="btn green_btn listing_btn button-next" style="padding: 8px 20px; margin: 0 8px;">
                                 Next <em class="m-icon-swapright m-icon-white"></em>
                                 
                                 </a> -->
                              <input type="submit" value="Next" class="btn red_btn button-submit listing_btn" style="padding: 8px 20px; margin: 0 20px;float:right;">
                              <!--next button-->
                             
                           </div>
                        </div>
                     </div>
                  </div>
                  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
                  <script type="text/javascript">
                     $('#restbtn').click(function(){
                        $('#submit_form')[0].reset();
                     });
                  </script>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="next" role="dialog">
   <div class="modal-dialog modal-sm">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Modal Header</h4>
         </div>
         <div class="modal-body">
            <p>This is a small modal.</p>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<div aria-hidden="false" role="dialog" class="modal fade in" id="success_registration" style="display: none;">
   <div class="success_login_main_cont margin-top-20">
      <div class="modal-header">
         <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><em class="fa fa-times"></em></button>
         <!--<h4 class="modal-title">Registration Successfully</h4>-->
         <h3 class="form-title">Registration Successful!</h3>
      </div>
      <div style="" class="">
         <p style="padding: 10px">Thank you! We received your registration and a confirmation email is on its way to you! If it does not arrive to your mailbox within 5 minutes please check your junk/spam folder.</p>
      </div>
      <div style="padding: 3px;" class="modal-footer">
         <div style="text-align:center">
            <button type="button" style="padding: 7px 20px;" data-dismiss="modal" class="btn default_btn clickhome">OK</button>
         </div>
      </div>
   </div>
</div>
<div aria-hidden="false" role="dialog" class="modal fade in" id="invalid-promo" style="display: none;">
   <div class="success_login_main_cont margin-top-20">
      <div class="modal-header">
         <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><em class="fa fa-times"></em></button>
         <!--<h4 class="modal-title">Registration Successfully</h4>-->
         <h3 class="form-title">Promo code!</h3>
      </div>
      <div style="" class="">
         <p style="padding: 10px">Your promo code is invalid!</p>
      </div>
      <div style="padding: 3px;" class="modal-footer">
         <div style="text-align:center">
            <button type="button" style="padding: 7px 20px;" data-dismiss="modal" class="btn default_btn">OK</button>
         </div>
      </div>
   </div>
</div>
<script>
   function changeHear(elem){
    if( $(elem).val() == 'Other' ){
        $('#hear_text').removeAttr('disabled');
        $('#hear_text').show();
    }else{
        $('#hear_text').attr('disabled', 'disabled');
        $('#hear_text').hide();
    }
   }
</script>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Modal Header</h4>
         </div>
         <div class="modal-body">
            <p>Some text in the modal.</p>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>

