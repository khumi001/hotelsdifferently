<style>
	   h2,h1,h3,h4,h5 {
		margin: 0;
		padding: 0;
	}
	.bg-furture{
		background: rgba(255, 255, 255, 0.74);;
		height: auto;
		width:100%;
		padding: 15px 50px;
	}
	.heading-text h1 {
		color: #fff;
		font-weight: 600;
		text-transform: capitalize;
		 margin: 30px 0 0;
	}
	.heading-text {
		display: block;
		margin: 0 0 60px;
		overflow: hidden;
		text-align: center;
	}
	.bg-furture h2 {
		border-bottom: 1px solid #999;
		color: #3d7db6;
		display: block;
		font-size: 20px;
		padding: 0 0 5px;
		margin: 0 0 10px;
		text-transform: uppercase;
	}
	.furture-form-style label {
		font-weight: normal;
		margin: 15px 0 5px;
		text-transform: capitalize;
	}
	.form-style {
		display: block;
		margin:0 0 10px;
		border-radius: 0px;
		font-size: 12px;
		text-align:center;
	}
	.phone-lable {
		display: block;
		width: 100%;
	}
	.no-padding{
		display: block;
		padding: 0;
		margin:0;
	}
	.col-md-4.no-padding {
		padding: 0 5px 0 0;
	}
	.col-md-3.no-padding {
		padding: 0 5px 0 0;
	}
	.phone-lable {
		display: block;
		font-weight: normal;
		margin: 15px 0 5px;
		width: 100%;
	}
	.compny-information {
		margin: 70px 0 0;
		padding: 0 15px;
	}
	.submit-btn-furture {
		background: rgba(0, 0, 0, 0) linear-gradient(to bottom, #71d1ff 0%, #22b3f4 4%, #198fef 100%) repeat scroll 0 0;
		border: medium none;
		border-radius: 6px;
		box-shadow: 0 1px 3px rgba(0, 0, 0, 0.4);
		color: #fff;
		font-family: "FuturaStd-Book";
		font-size: 16px;
		padding: 10px 25px;
		text-shadow: 1px 1px 2px #000;
		text-transform: uppercase;
		margin: 0 0 30px;
	}
	.bg-furture .fa-info-circle {
		cursor: pointer;
		vertical-align: super;
		font-size: 11px;
		color: #0d9de3;
	}
	.bg-furture .fa-info-circle:hover{
		color: #0b73a5;
	}
	.brder-radius-none{
		border-radius: 0px;
	}
	.doamin-sec span {
	  position: relative;
	  top: 7px;
	}
   ::-webkit-input-placeholder { /* Chrome/Opera/Safari */
color: pink;
}
select {
border-radius: 0 !important;
font-size: 12px !important;
}
</style>

<div class="container ">
	<div class="heading-text">
		<h1>Entity Sign up Page</h1>
	</div>
	<div class="bg-furture">
		<h2>Company Detials:</h2>
		<form>
			<div class="row">
				<div class="col-md-4 col-sm-4 ">
					<div class="form-group furture-form-style">
						<label>Company Name:</label>
						<input type="text" class="form-control form-style" name="" >
					</div>
				</div>
				<div class="col-md-4 col-sm-4">
					<div class="form-group furture-form-style">
						<label>Company Website:</label>
						<input type="text" class="form-control form-style" name="" >
					</div>
				</div>
				<div class="col-md-4 col-sm-4">
					<label class="phone-lable">Company phone :
					 <i class="inf-phone fa fa-info-circle"></i></label>
					<div class="col-md-3 no-padding"> 
						<div class="form-group furture-form-style">

							<input type="text" class="form-control form-style" placeholder="Country" name="" >
						</div>
					</div> 
					<div class="col-md-4 no-padding"> 
						<div class="form-group furture-form-style">

							<input type="text"  placeholder="Number" class="form-control form-style" name="" >
						</div>
					</div> 
					<div class="col-md-5 no-padding"> 
						<div class="form-group furture-form-style">

							<input placeholder="Extension" type="text" class="form-control form-style" name="" >
						</div>
					</div>    
				</div>
				<div class="col-md-4 col-sm-4">
					<div class="form-group furture-form-style">
						<label>Country:</label>
						<select class="form-control input-medium select2me" name="visitors" data-placeholder="Select...">
							<option value=""> Loram text</option>
							<option value=""> Loram text</option>
							<option value=""> Loram text</option>
						</select>	
					</div> 
				</div>
				<div class="col-md-4 col-sm-4">
					<div class="form-group furture-form-style">
						<label>State:</label>
						<select class="form-control input-medium select2me" name="visitors" data-placeholder="Select...">
							<option value=""> Select State</option>
							<option value=""> Loram text</option>
							<option value=""> Loram text</option>
						</select>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 ">
					<div class="form-group furture-form-style">
						<label>City</label>
						<input type="text" class="form-control form-style" name="" >
					</div>
				</div>
				<div class="col-md-4 col-sm-4 ">
					<div class="form-group furture-form-style">
						<label>Street:</label>
						<input type="text" class="form-control form-style" name="" >
					</div>
				</div>
				<div class="col-md-4 col-sm-4">
					<div class="form-group furture-form-style">
						<label>How many employees does the company have ?</label>
						<select class="form-control input-medium select2me" name="visitors" data-placeholder="Select...">
							<option value=""> Select State</option>
							<option value=""> Loram text</option>
							<option value=""> Loram text</option>
						</select>
					</div>
				</div>
				<div class="col-md-4 col-sm-4">
					<div class="form-group furture-form-style">
						<label>Where did you hear about us ?</label>
						<select class="form-control input-medium select2me" name="visitors" data-placeholder="Select...">
							<option value=""> Select State</option>
							<option value=""> Loram text</option>
							<option value=""> Loram text</option>
						</select>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="compny-information">
					<h2>Contact Information Or travel Department Contact</h2>
				</div>
				<div class="col-md-4 col-sm-4 ">
					<div class="form-group furture-form-style">
						<label>Full Name:</label>
						<input type="text" class="form-control form-style" name="" >
					</div>
				</div>
				<div class="col-md-4 col-sm-4 ">
					<div class="form-group furture-form-style">
						<label>Email:</label>
						<input type="text" class="form-control form-style" name="" >
					</div>
				</div>
				<div class="col-md-4 col-sm-4 ">
					<div class="form-group furture-form-style">
						<label>Password: <i class="inf-pass fa fa-info-circle"></i></label>
						<input type="text" class="form-control form-style" name="" >
					</div>
				</div>
				<div class="col-md-4 col-sm-4 ">
					<div class="form-group furture-form-style">
						<label>Password confirmation:</label>
						<input type="text" class="form-control form-style" name="" >
					</div>
				</div>
				<div class="col-md-4 col-sm-4 cell-phone">
					<label class="phone-lable">Cell phone <i class="inf-cell-phone fa fa-info-circle"></i></label>
					<div class="col-md-3 no-padding"> 
						<div class="form-group furture-form-style">

							<input type="text" placeholder="Country" class="form-control form-style" name="" >
						</div>
					</div> 
					<div class="col-md-9 no-padding"> 
						<div class="form-group furture-form-style">

							<input type="text" placeholder="Number" class="form-control form-style" name="" >
						</div>
					</div>    
				</div>
				<div class="col-md-4 col-sm-4 ">
					<div class="form-group furture-form-style">
						<label>Employee email domain <i class="inf-email fa fa-info-circle"></i></label>
						<div class="doamin-sec">
							<div class="col-md-4" style="padding: 0;"> <span>XXXXXXXXXX@</span></div>
							
							<div class="col-md-8"  style="padding: 0;"> <input type="text" class="form-control form-style" name="" ></div>
							
						</div>
				</div>
				<div class="col-md-12 col-sm-12">
					<button type="submit" class="submit-btn-furture pull-right">NEXT</button>
				</div>
			</div>
            </div>
		</form>	
	</div>	
</div>

<script>
	$('.inf-phone').tooltip({title:'Country - Phone - Extension',placement:'top'});
	$('.inf-cell-phone').tooltip({title:'Country - Phone',placement:'top'});
	$('.inf-iata').tooltip({title:'Enter IATA Number',placement:'top'});
	$('.inf-clia').tooltip({title:'Enter CLIA Number',placement:'top'});
	$('.inf-arc').tooltip({title:'Enter ARC Number',placement:'top'});
	$('.inf-email').tooltip({title:'This email address will be used for you to login to your account, signup and reservation information will be sent here',placement:'top'});
	$('.inf-pass').tooltip({title:'Your password is CaSe SeNsItIvE, must be at least 8 characters and must contain at least one letter and one number.',placement:'top'});
</script>

