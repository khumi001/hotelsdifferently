<!DOCTYPE html>
<html lang="en">
    <head lang="en">
        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,400i,700,700i|Roboto+Slab|Roboto:400,700,700i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Kaushan+Script" rel="stylesheet"> 
        <style>
            #bg
            {
                background-image: url(<?php echo base_url(); ?>public/affilliate_theme/img/loading-bg.png);
                background-repeat: no-repeat;
                background-attachment: fixed;
                background-position: center auto;
                
            }
            .loading-page{
                background: rgba(255, 255, 255, 0.8) none repeat scroll 0 0;
                max-width: 968px;
                padding: 8px 35px;
                border-radius: 4px;
                margin: 0 auto;
            }
            .loading-page-follow{
                margin-bottom: 44px;
                margin-top: 25px;
                padding-top: 17px;

            }
            .unq-hover {
                transition: all 0.85s ease 0s;
            }
            .unq-hover:hover {
                box-shadow: 0px 1px 20px 0px rgba(0, 0, 0, 0.28);
                transition: all 0.25s ease 0s;
                border-radius: 4px;
            }
            .loading-page .heading > span{
                color: #bc0005;
                font-family: 'Roboto Condensed', sans-serif;
                text-shadow: 2px 2px #fff200;
                font-style: italic;
                font-weight: bold;
                font-size: 24px;
            }
            .loading-page .heading{
                font-family: 'Roboto Condensed', sans-serif;
                color: #00559f;
                font-weight:bold;
                font-size:24px;
            }
            .heading > .content{
                margin-top: 15px;
            }
            .loading-page .sale{
                margin-bottom: 38px;
            }
            .loading-page .DEALS{
                margin-bottom: 44px;
            }
            .loading-page .EXTRA{
                margin-bottom: 28px;
            }
            .loading-page .SPORTING{
                margin-bottom: 10px;
            }
            .heading .loading-list ul{
                font-family: 'Roboto Condensed', sans-serif;
                font-weight:normal;
                color: #252525;
                font-size:16px;
                list-style: none;
                padding:0;
            }
            .heading .loading-list span{
                font-family: 'Roboto Condensed', sans-serif;
                font-weight:bold;                
            }

            .heading .loading-list img{
                display: inline-block;
                vertical-align: middle;
                position: relative;
                margin-right: 5px;
            }
            .heading .loading-list >.no-extra >li > img{
                height: 20px;
                width: 24px;
                margin-right: 5px;
            }
            .heading .loading-list >.no-extra >li{
                margin-bottom: 10px;
                margin-left: 5px;
            }
            .heading .loading-list >.no-extra >li:nth-of-type(1){
                margin-top: 10px;
            }

            .heading .loading-list .rvl-never li span{
                color: #bc0005;
                font-style: italic;
            }
            .heading .loading-list .rvl-never li span:nth-of-type(2){
                font-style: normal;
            }
            .heading .loading-list .no-extra > span:nth-of-type(1){
                font-style: bold;
                background-color:#fff200;
                margin-left: 5px;
            }
            .heading .sold-out > span:nth-of-type(1){
                font-style: normal;
                font-weight: normal;
                color:#252525;
                font-size: 16px;
                display: block;
            }
            /*.coming-soon > span:nth-of-type(1){
                font-style: normal;
                font-weight: normal;
                color:#252525;
                font-size: 16px;
                text-align: center;
                background-color:#fff200;
            }*/
            .coming-soon > span:nth-of-type(3){
                font-family: 'Roboto', sans-serif;
                font-style: normal;
                font-weight: normal;
            }
            .coming-soon > h1 {  
            background: rgba(249,228,38,1);
            background: -moz-linear-gradient(45deg, rgba(249,228,38,1) 0%, rgba(255,255,0,1) 100%);
            background: -webkit-gradient(left bottom, right top, color-stop(0%, rgba(249,228,38,1)), color-stop(100%, rgba(255,255,0,1)));
            background: -webkit-linear-gradient(45deg, rgba(249,228,38,1) 0%, rgba(255,255,0,1) 100%);
            background: -o-linear-gradient(45deg, rgba(249,228,38,1) 0%, rgba(255,255,0,1) 100%);
            background: -ms-linear-gradient(45deg, rgba(249,228,38,1) 0%, rgba(255,255,0,1) 100%);
            background: linear-gradient(45deg, rgba(249,228,38,1) 0%, rgba(255,255,0,1) 100%);
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9e426', endColorstr='#ffff00', GradientType=1 );
              border: 1px solid #ddd;
              color: #00559f;
              font-family: "Kaushan Script";
              font-size: 18px;
              letter-spacing: 1px;
              margin: 12px 0 17px;
              padding: 5px 5px 15px;
              -webkit-box-shadow: 0 8px 6px -6px black;
       -moz-box-shadow: 0 8px 6px -6px black;
            box-shadow: 0 8px 6px -6px black;
              word-spacing: 1px;
              border-radius: 0 15px 0 15px;
            }
            .coming-soon h1>span{
                color:#bc0005;
                display: ;
            }
            .btn{
                padding: 20px 80px;
                background: rgba(3,58,159,1);
                background: -moz-radial-gradient(center, ellipse cover, rgba(3,58,159,1) 0%, rgba(51,153,255,1) 100%);
                background: -webkit-gradient(radial, center center, 0px, center center, 100%, color-stop(0%, rgba(3,58,159,1)), color-stop(100%, rgba(51,153,255,1)));
                background: -webkit-radial-gradient(center, ellipse cover, rgba(3,58,159,1) 0%, rgba(51,153,255,1) 100%);
                background: -o-radial-gradient(center, ellipse cover, rgba(3,58,159,1) 0%, rgba(51,153,255,1) 100%);
                background: -ms-radial-gradient(center, ellipse cover, rgba(3,58,159,1) 0%, rgba(51,153,255,1) 100%);
                background: radial-gradient(ellipse at center, rgba(3,58,159,1) 0%, rgba(51,153,255,1) 100%);
                filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#033a9f', endColorstr='#3399ff', GradientType=1 );
                color: #fff;
                font-size:16px;
            }
            .btn-block:before{
                content: url(<?php echo base_url(); ?>public/affilliate_theme/img/arrow-left.png);
            }
            .btn-block:after{
                content: url(<?php echo base_url(); ?>public/affilliate_theme/img/arrow-right.png);
            }
            .btn:hover{
                box-shadow: 1px 2px 20px rgba(0, 0, 0, 0.48);
                color: #fff;
                text-shadow: 0px 4px rgba(0, 0, 0, 0.48);
            }

            .btn-block {
                display: block;
                margin-bottom: 52px;
                text-align: center;
            }
            .font-red{
                color: #bc0005;
            }
            .whl-rates.inline-block > li {
              display: inline-flex;
            }
             .coming-soon > h1 {  
            background: rgba(249,228,38,1);
            background: -moz-linear-gradient(45deg, rgba(249,228,38,1) 0%, rgba(255,255,0,1) 100%);
            background: -webkit-gradient(left bottom, right top, color-stop(0%, rgba(249,228,38,1)), color-stop(100%, rgba(255,255,0,1)));
            background: -webkit-linear-gradient(45deg, rgba(249,228,38,1) 0%, rgba(255,255,0,1) 100%);
            background: -o-linear-gradient(45deg, rgba(249,228,38,1) 0%, rgba(255,255,0,1) 100%);
            background: -ms-linear-gradient(45deg, rgba(249,228,38,1) 0%, rgba(255,255,0,1) 100%);
            background: linear-gradient(45deg, rgba(249,228,38,1) 0%, rgba(255,255,0,1) 100%);
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9e426', endColorstr='#ffff00', GradientType=1 );
              border: 1px solid #ddd;
              color: #00559f;
              font-family: "Kaushan Script";
              font-size: 18px;
              letter-spacing: 1px;
              margin: 12px 0 17px;
              padding: 13px 5px 15px;
              -moz-box-shadow:inset 0 0 10px #000000;
               -webkit-box-shadow:inset 0 0 10px #000000;
               box-shadow:inset 0 0 10px #000000;
              word-spacing: 1px;
              border-radius: 0 15px 0 15px;
              font-weight: bold;
            }
            .rvl-never img {
              display: block;
              height: 20px;
              width: 24px;
            }
            .img-icon > img {
              display: block;
              height: 20px;
              width: 24px;
            }
            .whl-rates img {
             display: block;
             height: 20px;
              width: 24px;
            }
        </style>
    </head>
    <body id="bg">    	
        <div class="container loading-page">

            
            <div class="col-md-12 col-sm-12">
                <div class="heading sale">
                    <span>DEEPLY DISCOUNTED </span> HOTEL RATES
                    <div class="content">
                        <div class="loading-list">
                            <ul class="whl-rates inline-block">
                                <li><div class="img-icon"><img src="<?php echo base_url(); ?>public/affilliate_theme/img/icon-loading.png" alt="list-logo"/></div><div class="icon-conent"><span>DEEPLY DISCOUNTED HOTEL RATES </span>  are now possible for consumers like you! See how much you can save with us and enjoy the hotels of your dreams! Take a look and see why hotels don't allow us to display their rates publicly and enjoy your savings!</div></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="heading DEALS">
                    REVOLUTIONARY FLIGHT DEALS LIKE <span> NEVER SEEN BEFORE !</span>
                    <div class="content">
                        <div class="loading-list">
                            <ul class="rvl-never">
                                <li><img src="<?php echo base_url(); ?>public/affilliate_theme/img/icon-loading.png" alt="list-logo"/>Most Sites offer a network of 400+ airlines. We are Conntected to an aggregate supplier with <span>OVER 900</span><span>!!!</span> <span>More airlines = cheaper prices! </span> Simple, Right? Membership is not required for this feature on our site.</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-sm-12">
                <div class="heading EXTRA">
                    COMPLIMENTARY COVERAGE* AT <span> NO EXTRA CHARGE !</span>
                    <div class="content">
                        <div class="loading-list">
                            <ul class="no-extra">
                                <span>DELAYS? FLIGHT CANCELLATIONS? These things can happen to us all, but you can get:</span>
                                <li><img src="<?php echo base_url(); ?>public/affilliate_theme/img/icon-loading.png" alt="list-logo"/>Re-accommodation on an alternative flight <span> AND/OR</span></li>
                                <li><img src="<?php echo base_url(); ?>public/affilliate_theme/img/icon-loading.png" alt="list-logo"/>Partial refund of your plane ticket <span> AND/OR</span></li>
                                <li><img src="<?php echo base_url(); ?>public/affilliate_theme/img/icon-loading.png" alt="list-logo"/>Overnight accommodation <span> AND/OR</span></li>
                                <li><img src="<?php echo base_url(); ?>public/affilliate_theme/img/icon-loading.png" alt="list-logo"/>Alternative transportation <span> AND/OR</span></li>
                                <li><img src="<?php echo base_url(); ?>public/affilliate_theme/img/icon-loading.png" alt="list-logo"/>Meal and Beverage compensation</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-sm-12">
                <div class="heading SPORTING">
                    <span>SOLD-OUT </span> CONCERTS OR SPORTING EVENTS?
                    <div class="content">
                        <div class="loading-list">
                            <ul class="whl-rates">
                                <li><img src="<?php echo base_url(); ?>public/affilliate_theme/img/icon-loading.png" alt="list-logo"/><span></span>Access select events with no official availability!</li>
                                <li><img src="<?php echo base_url(); ?>public/affilliate_theme/img/icon-loading.png" alt="list-logo"/><span></span>Membership is not required for this feature on our site.</li>
                            </ul>  
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-sm-12">
                <div class="heading sale">
                    <span>DISCOUNTED </span> ACTIVITIES?
                    <div class="content">
                        <div class="loading-list">
                            <ul class="whl-rates">
                                <li><img src="<?php echo base_url(); ?>public/affilliate_theme/img/icon-loading.png" alt="list-logo"/><span></span>Get your sightseeing and selected shows for a discount!</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
             <div class="col-md-12 col-sm-12 text-center">

                <div class="coming-soon">
                   <?php /*?> <span>COMING SOON: WHOLESALE tours / activities, car rentals, and cruises!</span><?php */?>
                    <h1>GET YOUR ANNUAL MEMBERSHIP NOW FOR <span class="font-red">$49.99!</span> <?php /*?><span>(LIMITED TIME OFFER)</span><?php */?></h1>
                    <div class="clearfix"></div>
                    <span class="btn-block"><a href="<?php echo base_url(); ?>front/home/member-signup<?php echo (!empty($hear_about_us)  ? '?hear_about_us='.$hear_about_us : '');?>" class="btn btn-default unq-hover">SIGN UP NOW!</a></span>
                    <span>* Only on select flights. This coverage is subject to the terms and conditions and policies of our flight aggregate supplier.</span>
                </div>

            </div>
        </div>
    </body>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.js"></script>
</html>