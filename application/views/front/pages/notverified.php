<style type="text/css">
  .image-holder-pay img {
  display: block;
  margin: 25px auto 20px;
  max-width: 71%;
}
.image-holder-pay > a {
    display: block;
}
.main_cont.payment-sec > h1 {
  font-size: 20px;
  font-weight: bold;
  margin: 0 0 10px;
  text-align: center;
  text-transform: capitalize;
}
.member-content {
  font-size: 15px;
  margin: 0 0 5px;
  text-align: center;
  text-transform: capitalize;
}
.content-holder-payment {
  display: block;
  margin: 0 auto;
  max-width: 90%;
}
.submit-btn-furture {
  background: rgba(0, 0, 0, 0) linear-gradient(to bottom, #71d1ff 0%, #22b3f4 4%, #198fef 100%) repeat scroll 0 0;
  border: medium none;
  border-radius: 6px;
  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.4);
  color: #fff;
  font-family: "FuturaStd-Book";
  font-size: 16px;
  margin: 0 60px 30px 40px;
  padding: 10px 25px;
  text-shadow: 1px 1px 2px #000;
  text-transform: uppercase;
}
.submit-btn-furture {
  background: rgba(0, 0, 0, 0) linear-gradient(to bottom, #71d1ff 0%, #22b3f4 4%, #198fef 100%) repeat scroll 0 0;
  border: medium none;
  border-radius: 6px;
  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.4);
  color: #fff;
  font-family: "FuturaStd-Book";
  font-size: 16px;
  margin: 0 60px 30px 40px;
  padding: 10px 25px;
  text-shadow: 1px 1px 2px #000;
  text-transform: uppercase;
  bottom: 3px;
  height: 38px;
  position: relative;
  height: 35px;
    line-height: 13px;
    right: 51px;
    top: 0;
}
.form-group.furture-form-style {
  margin: 15px 0 0;
  text-align: center;
}
.amound-contet{
  display: block;
  margin: 0 0 20px;
}
.form-style {
  border: 1px solid #ddd;
  border-radius: 3px;
  height: 35px;
  padding: 8px;
  width: 93%;
}
.verify-page .col-md-3 > label {
  position: relative;
  top: 8px;
}
strong.margin-ton {
  display: block;
  margin: -12px 0 0;
}
.content-holder-payment > p:first-child {
  margin-top: 20px;
}
.red-text{
  color: red;
}
</style>
<div id="result_container">
   <div class="page_container">                                            
      <div class="width-row" style="background-color: rgba(255, 255, 255, 0.74) none repeat scroll 0 0;">
         <div class="main_cont payment-sec verify-page">
            <h1 class="red-text">YOUR ACCOUNT WAS NOT VERIFIED!</h1>  
            <div class="clearfix"></div>  
            <div class="content-holder-payment">
              <p>Unfortunately, you failed to verify your method of payment therefore in order to fight debit and credit card fraud, we issued a full refund and closed your account permanently. You will receive a full refund within the next 5-7 business days. If you don’t see the refunded amount on your statement or bank account, please call your card issuer or financial institution you used. </p>
              <p>We are sorry that you weren’t able to join our website but hope to get to see you on another occasion.</p> 
              <p>Sincerely,</p>
              <div class="clearfix"></div>
              <strong class="margin-ton">Wholesale Hotels Group</strong>
            </div>             
          </div>
      </div>
   </div>
</div>   