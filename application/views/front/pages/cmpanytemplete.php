<div id="result_container">
   <div class="page_container">                                            
      <div class="width-row" style="background-color: rgba(255, 255, 255, 0.74) none repeat scroll 0 0;">
         <div class="main_cont">
              <strong class="text-center" style="text-align:center;display:block;margin:0 0 15px;">Welcome to HotelsDifferently</strong>
              <div class="" style="margin: 10px 0;">
                <b>CONGRATULATIONS!</b> You have successfully submitted your request to enroll on <b>HotelsDifferently.com.</b>
              </div>
              
              <p>
                  <span style="text-decoration:; font-weight:bold;display: block;">What’s next? </span>We just sent you an email with a link for you to activate your account. Once we send out the link you will have <strong>72 hours </strong>to click on it. If you fail to complete the signup process you need to enroll on our site again.
            </p>
             <p style="margin: 0 0 60px;">Thank you for choosing <b>HotelsDifferently.com</b> where we are <i>Making hotels happen for YOU!</i></p>
              <p></p>                
          </div>
      </div>
   </div>
</div>   