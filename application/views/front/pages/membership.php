<style type="text/css">
@import url('https://fonts.googleapis.com/css?family=Artifika');
  .image-holder-pay img {
    display: block;
    margin: 25px auto 10px;
    max-width: 90%;
}
.image-holder-pay > a {
    display: block;
}
.main_cont.payment-sec > h1 {
  font-size: 20px;
  font-weight: bold;
  margin: 0 0 10px;
  text-align: center;
  text-transform: capitalize;
}
.member-content {
  font-size: 15px;
  margin: 0 0 5px;
  text-align: center;
  text-transform: capitalize;
}
.content-holder-payment {
  display: block;
  margin: 20px auto 0;
  max-width: 90%;
}
.submit-btn-furture {
  background: rgba(0, 0, 0, 0) linear-gradient(to bottom, #71d1ff 0%, #22b3f4 4%, #198fef 100%) repeat scroll 0 0;
  border: medium none;
  border-radius: 6px;
  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.4);
  color: #fff;
  font-family: "FuturaStd-Book";
  font-size: 16px;
  margin: 0 60px 30px 40px;
  padding: 10px 25px;
  text-shadow: 1px 1px 2px #000;
  text-transform: uppercase;
}
.submit-btn-furture {
  background: rgba(0, 0, 0, 0) linear-gradient(to bottom, #71d1ff 0%, #22b3f4 4%, #198fef 100%) repeat scroll 0 0;
  border: medium none;
  border-radius: 6px;
  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.4);
  color: #fff;
  font-family: "FuturaStd-Book";
  font-size: 16px;
  margin: 0 60px 30px 40px;
  padding: 10px 25px;
  text-shadow: 1px 1px 2px #000;
  text-transform: uppercase;
}
.form-group.furture-form-style {
  margin: 15px 0 0;
  text-align: center;
}
.membership-price {
  display: block;
  margin: 91px auto 45px;
  max-width: 650px;
}
.membership-price .price-1{

background: rgba(232,154,58,1);
background: -moz-radial-gradient(center, ellipse cover, rgba(232,154,58,1) 0%, rgba(191,98,6,1) 100%);
background: -webkit-gradient(radial, center center, 0px, center center, 100%, color-stop(0%, rgba(232,154,58,1)), color-stop(100%, rgba(191,98,6,1)));
background: -webkit-radial-gradient(center, ellipse cover, rgba(232,154,58,1) 0%, rgba(191,98,6,1) 100%);
background: -o-radial-gradient(center, ellipse cover, rgba(232,154,58,1) 0%, rgba(191,98,6,1) 100%);
background: -ms-radial-gradient(center, ellipse cover, rgba(232,154,58,1) 0%, rgba(191,98,6,1) 100%);
background: radial-gradient(ellipse at center, rgba(232,154,58,1) 0%, rgba(191,98,6,1) 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#e89a3a', endColorstr='#bf6206', GradientType=1 );
border: 1px solid #666;
border-radius: 15px 15px 0 0;
display: block;
height: 180px;
width: 100%;
margin: 0 0 10px;
}
.box {
  position: relative;
}
.ribbon-1 {
  position: absolute;
  right: -5px; top: -5px;
  z-index: 1;
  overflow: hidden;
  width: 75px; height: 75px;
  text-align: right;
}
.ribbon-1 span {
  font-size: 10px;
  font-weight: bold;
  color: #000;
  text-transform: uppercase;
  text-align: center;
  line-height: 20px;
  transform: rotate(45deg);
  -webkit-transform: rotate(45deg);
  width: 100px;
  display: block;
  background: rgba(0, 0, 0, 0) -moz-linear-gradient(center top , #f3cd50, #f3cd96) repeat scroll 0 0;
  background: linear-gradient(#f3cd50 0%, #f3cd96 100%);
  box-shadow: 0 3px 10px -5px rgba(0, 0, 0, 1);
  position: absolute;
  top: 19px; right: -21px;
  font-family: "Artifika",Helvetica,sans-serif;
}
.ribbon-1 span::before {
  content: "";
  position: absolute; left: 0px; top: 100%;
  z-index: -1;
  border-left: 3px solid #f3cd96;
  border-right: 3px solid transparent;
  border-bottom: 3px solid transparent;
  border-top: 3px solid #f3cd96;
}
.ribbon-1 span::after {
  content: "";
  position: absolute; right: 0px; top: 100%;
  z-index: -1;
  border-left: 3px solid transparent;
  border-right: 3px solid #f3cd96;
  border-bottom: 3px solid transparent;
  border-top: 3px solid #f3cd96;
}
.ribbon {
  background: rgba(0, 0, 0, 0) -moz-linear-gradient(center top , #f3cd50, #f3cd96) repeat scroll 0 0;
  box-shadow: 0 1px 1px rgba(0, 0, 0, 0.3);
  font-family: "Artifika",Helvetica,sans-serif;
  left: 1px;
  position: absolute;
  text-align: center;
  top: 10px;
  width: 99%;
  -webkit-box-shadow: rgba(000,000,000,0.3) 0 1px 1px;
   -moz-box-shadow: rgba(000,000,000,0.3) 0 1px 1px;
}
.ribbon h1 {
  color: #801111;
  font-size: 15px;
  margin: 0;
  padding: 2px 5px;
  text-shadow: 0 1px 0 #d65c5c;
  font-weight: 600;
}
/*.ribbon:before, .ribbon:after {
   content: '';
   position: absolute;
   display: block;
   bottom: -1em;
   border: 1.5em solid #c23a3a;
   z-index: -1;
   }*/
.ribbon:before {
   left: -2em;
   border-right-width: 1.5em;
   border-left-color: transparent;
   -webkit-box-shadow: rgba(000,000,000,0.4) 1px 1px 1px;
   -moz-box-shadow: rgba(000,000,000,0.4) 1px 1px 1px;
   box-shadow: rgba(000,000,000,0.4) 1px 1px 1px;
   }
.ribbon:after {
   right: -2em;
   border-left-width: 1.5em;
   border-right-color: transparent;
   -webkit-box-shadow: rgba(000,000,000,0.4) -1px 1px 1px;
   -moz-box-shadow: rgba(000,000,000,0.4) -1px 1px 1px;
   box-shadow: rgba(000,000,000,0.4) -1px 1px 1px;
   }
.ribbon .ribbon-content::before, .ribbon .ribbon-content::after {
  border-color: #f3cd96 transparent transparent;
  border-style: solid;
  bottom: -1em;
  content: "";
  display: block;
  position: absolute;
}
.ribbon .ribbon-content:before {
   left: 0;
   border-width: 1em 0 0 1em;
   }
.ribbon .ribbon-content:after {
   right: 0;
   border-width: 1em 1em 0 0;
   }
.ribbon-stitches-top {
   margin-top:2px;
   border-top: 1px dashed rgba(0, 0, 0, 0.2);
   -moz-box-shadow: 0px 0px 2px rgba(0, 0, 0, 0.5);
   -webkit-box-shadow: 0px 0px 2px rgba(0, 0, 0, 0.5);
   box-shadow: 0px 0px 2px rgba(255, 255, 255, 0.5);
   }
.ribbon-stitches-bottom {
   margin-bottom:2px;
   border-top: 1px dashed rgba(0, 0, 0, 0.2);
   -moz-box-shadow: 0px 0px 2px rgba(0, 0, 0, 0.3);
   -webkit-box-shadow: 0px 0px 2px rgba(0, 0, 0, 0.3);
   box-shadow: 0px 0px 2px rgba(255, 255, 255, 0.3);
   }
   .ribbon-content > img {
    float: left;
    left: 20px;
    position: relative;
    top: 2px;
    width: 20px;
  }
  .pr-main {
  color: #fff;
  display: block;
  font-family: "Artifika",Helvetica,sans-serif;
  margin: 55px 0 0 -10px;
  text-align: center;
  text-shadow: 0 2px 2px #000;
}
.pr-main .pr-1 {
  bottom: 4px;
  font-size: 17px;
  left: -4px;
  position: relative;
}
.pr-main .pr-2 {
  font-size: 37px;
}
.pr-main sup {
  font-size: 15px;
  position: relative;
  top: -18px;
}
.month-pr {
  font-weight: normal;
  margin: 0 -22px;
  font-size: 12px;
}
.price-1 > p {
  color: #fff;
  display: block;
  font-size: 10px;
  font-weight: 600;
  margin: 5px 0;
  text-align: center;
  text-shadow: 2px 2px rgba(0, 0, 0, 0.35);
}
.price-1.price-2{

background: rgba(54,223,232,1);
background: -moz-linear-gradient(45deg, rgba(54,223,232,1) 0%, rgba(67,141,152,1) 100%);
background: -webkit-gradient(left bottom, right top, color-stop(0%, rgba(54,223,232,1)), color-stop(100%, rgba(67,141,152,1)));
background: -webkit-linear-gradient(45deg, rgba(54,223,232,1) 0%, rgba(67,141,152,1) 100%);
background: -o-linear-gradient(45deg, rgba(54,223,232,1) 0%, rgba(67,141,152,1) 100%);
background: -ms-linear-gradient(45deg, rgba(54,223,232,1) 0%, rgba(67,141,152,1) 100%);
background: linear-gradient(45deg, rgba(54,223,232,1) 0%, rgba(67,141,152,1) 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#36dfe8', endColorstr='#438d98', GradientType=1 );
height: 210px;
margin: -30px 0 10px;
}
.price-1.price-2 .ribbon {
  background: rgba(0, 0, 0, 0) -moz-linear-gradient(center top , #f3cd50, #f3cd96) repeat scroll 0 0;
  box-shadow: 0 1px 1px rgba(0, 0, 0, 0.3);
  font-family: "Artifika",Helvetica,sans-serif;
  left: 1px;
  position: absolute;
  text-align: center;
  top: -21px;
  width: 99%;
}
.price-1.price-2 .ribbon h1 {
  color: #801111;
  font-size: 15px;
  margin: 0 25px 0 0 !important;
  padding: 2px 5px;
  text-shadow: 0 1px 0 #d65c5c;
}
.price-1.price-2.price-3{

background: rgba(87,128,12,1);
background: -moz-radial-gradient(center, ellipse cover, rgba(87,128,12,1) 0%, rgba(162,207,71,1) 100%);
background: -webkit-gradient(radial, center center, 0px, center center, 100%, color-stop(0%, rgba(87,128,12,1)), color-stop(100%, rgba(162,207,71,1)));
background: -webkit-radial-gradient(center, ellipse cover, rgba(87,128,12,1) 0%, rgba(162,207,71,1) 100%);
background: -o-radial-gradient(center, ellipse cover, rgba(87,128,12,1) 0%, rgba(162,207,71,1) 100%);
background: -ms-radial-gradient(center, ellipse cover, rgba(87,128,12,1) 0%, rgba(162,207,71,1) 100%);
background: radial-gradient(ellipse at center, rgba(87,128,12,1) 0%, rgba(162,207,71,1) 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#57800c', endColorstr='#a2cf47', GradientType=1 );
height: 240px;
margin: -60px 0 10px;

}
.price-1.price-2.price-3 small {
  color: #fff;
  display: block;
  text-align: center;
  text-shadow: 1px 2px 2px #000;
}
.price-1.price-2.price-3 .ribbon {
  background: rgba(0, 0, 0, 0) -moz-linear-gradient(center top , #f3cd50, #f3cd96) repeat scroll 0 0;
  box-shadow: 0 1px 1px rgba(0, 0, 0, 0.3);
  font-family: "Artifika",Helvetica,sans-serif;
  left: 1px;
  position: absolute;
  text-align: center;
  top: -50px;
  width: 99%;
}
.price-1.price-2.radio p {
    color: #fff;
    display: block;
    font-size: 10px;
    font-weight: 600;
    margin: 5px 0;
    padding: 0 !important;
    text-align: center;
    text-shadow: 2px 2px rgba(0, 0, 0, 0.35);
}
.price-1.radio p{
  color: #fff;
    display: block;
    font-size: 10px;
    font-weight: 600;
    margin: 5px 0;
    padding: 0 !important;
    text-align: center;
    text-shadow: 2px 2px rgba(0, 0, 0, 0.35);
}
.checkbox, .radio {
  display: inline-block;
  margin: 0;
  min-height: 0;
  padding-left: 0;
}
.slect-txt {
  font-size: 20px;
  text-align: center;
  color: #801111;
  text-shadow: 0 1px 0 #d65c5c;
  font-family: "Artifika",Helvetica,sans-serif;
  font-weight: 600;
  padding: 10px 0 0;
}
.image-holder-pay.last-img img {
    max-width: 85%;
}
.radio-inline {
    text-align: center;
}
.image-holder-pay.margin-img img {
    margin: 70px 0 10px;
}
.radio-inline strong {
    display: block;
    font-size: 12px;
    line-height: 1;
}
</style>
<div id="result_container">
   <div class="page_container">                                            
      <div class="width-row" style="background-color: rgba(255, 255, 255, 0.74) none repeat scroll 0 0;">
         <div class="main_cont payment-sec">
            <h1>Membership payment page</h1>
            <h3 class="slect-txt">Select your desired method of payment </h3>  
            <div class="col-md-3 col-sm-3">
              <div class="image-holder-pay margin-img">
                <label class="radio-inline">
                  <img src="<?php echo base_url(); ?>public/assets/img/apple-pay.png" alt="payment">
                  <input type="radio" name="optradio">
                </label>
              </div>
            </div> 
            <div class="col-md-3 col-sm-3">
              <div class="image-holder-pay margin-img">
                <label class="radio-inline">
                 <img src="<?php echo base_url(); ?>public/assets/img/and-pay.png" alt="payment">
                  <input type="radio" name="optradio">
                </label> 
              </div>
            </div>
            <div class="col-md-3 col-sm-3">
              <div class="image-holder-pay ">
                <label class="radio-inline">
                  <img src="<?php echo base_url(); ?>public/assets/img/credit-crd.png" alt="payment">
                  <strong>NEW credit card
                  </strong>
                  <input type="radio" name="optradio">
                </label>  
              </div>
            </div>
            <div class="col-md-3 col-sm-3">
              <div class="image-holder-pay last-img">
                <label class="radio-inline">
                  <img src="<?php echo base_url(); ?>public/assets/img/card-2.png" alt="payment" />
                  <strong>Credit card on file<br />
                  ending with 1234
                  </strong>
                  <input type="radio" name="optradio">
                </label>
              </div>
            </div> 
            <div class="clearfix"></div>  
            <div class="content-holder-payment">
              <div class="clearfix"></div>
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
            </div> 
            <div class="form-group furture-form-style">
                <button type="button" class="submit-btn-furture " style="background: red;">Back <i class="fa fa-refresh fa-lg fa-spin loadin_ajax" style="color: #ffffff;display:none;"></i></button>
                <button type="button" class="submit-btn-furture">Next</button>
            </div>             
          </div>
      </div>
   </div>
</div>   