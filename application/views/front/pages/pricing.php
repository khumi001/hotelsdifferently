<style>
            h2,h1,h3,h4,h5 {
                margin: 0;
                padding: 0;
            }
            .bg-furture{
                background: rgba(255, 255, 255, 0.74);;
                height: auto;
                width:100%;
                padding: 15px 50px;
                margin: 0 0 30px;
            }
            .heading-text h1 {
                color: #fff;
                font-weight: 600;
                text-transform: capitalize;
                 margin: 30px 0 0;
            }
            .heading-text {
                display: block;
                margin: 0 0 60px;
                overflow: hidden;
                text-align: center;
            }
            .bg-furture h2 {
                border-bottom: 1px solid #999;
                color: #3d7db6;
                display: block;
                font-size: 20px;
                padding: 0 0 5px;
                margin: 0 0 10px;
                text-transform: uppercase;
            }
            table {
              margin: 50px auto;
              width: 90%;  
              padding: 10px;
              display: block;
             background-color: #fff;
            }
            .table-header th {
                border: 1px solid #efefef !important;
                font-size: 12px;
                padding: 13px 4px;
                text-align: center;
                width: 8%;
            }
            .table-header th:first-child {
                width: 40%;
            }
            .table-body td {
                border: 1px solid #efefef !important;
                font-size: 12px;
                font-weight: 600;
                padding: 7px 5px;
                text-transform: capitalize;
            }
            .table-body td img {
              display: block;
              margin: 0 auto;
              max-width: 25px;
            }
            .table-body .color-red {
                color: red;
                text-transform: uppercase;
            }
            .table-body .discont-td{
                color:red;
                text-align: center;
            }
        </style>
<div class="container ">
            <div class="heading-text">
                <h1>Incentives</h1>
            </div>
            <div class="bg-furture">
                <h2>Incentives:</h2>
                <div class="prcing-sec">
                    <p><strong>HotelsDifferently.com</strong> offers some really exciting incentives for our TOP revenue generators out there. Besides saving money for your travel agency and its employees whether it is for business or leisure, we want to make sure that your loyalty doesn’t go unnoticed or taken for granted. Consider this our way of saying: Thank you for sticking with us!</p>
                    <table class="table-responsive table-bordered">
                        <tr class="table-header">
                            <th>&nbsp;</th>
                            <th>&lt;$500k</th>
                            <th>$500k - $1M</th>
                            <th>$1M - $2,5M</th>
                            <th>$2,5M - $5M</th>
                            <th>&gt;$5M</th>
                        </tr> 
                        <tr class="table-body">
                            <td class="color-red">Free membership</td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                        </tr>
                        <tr class="table-body">
                            <td>Instant online invoicing</td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                        </tr> 
                        <tr class="table-body">
                            <td>3D secure payment processing</td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                        </tr>
                        <tr class="table-body">
                            <td>24/7/365 Toll-free phone support</td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                        </tr>
                        <tr class="table-body">
                            <td>24/7/365 Email customer support</td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                        </tr> 
                        <tr class="table-body">
                            <td>Encrypted site with SSL encrypted technology</td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                        </tr> 
                        <tr class="table-body">
                            <td>1% discount on our pricing by using bitCoin payments</td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                        </tr> 
                        <tr class="table-body">
                            <td>Eligibility for our annual FREE luxury trip giveaway</td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                        </tr> 
                        <tr class="table-body">
                            <td>1% discount on our alternative payment platform for USA accounts</td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/cancel.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/cancel.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                        </tr> 
                        <tr class="table-body">
                            <td>Monthly Skype consultation with the CEO of HotelsDifferently.com</td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/cancel.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/cancel.png" alt="logo"/></td>
                            <td class="text-center">30 Minutes</td>
                            <td class="text-center">30 Minutes</td>
                            <td class="text-center">60 Minutes</td>
                        </tr> 
                        <tr class="table-body">
                            <td>Dedicated customer service representative</td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/cancel.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/cancel.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/cancel.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                        </tr>
                        <tr class="table-body">
                            <td>Additional tier discount for the lifetime of your account</td>
                            <td class="discont-td">0.05%</td>
                            <td class="discont-td">1%</td>
                            <td class="discont-td">1.5%</td>
                            <td class="discont-td">2%</td>
                            <td class="discont-td">Contact us for special pricing</td>
                        </tr> 
                    </table>
                    <p>Every calendar year we assess which travel professional produced the highest revenue for our company (we calculate revenue in a very simple way: Dollar amount your agency or agents spent combined MINUS any refunds in one calendar year) and if your company ranks #1 on the 31st of December at 11:59pm each year EST, we will provide the travel service (contact) person a <b>FREE TRIP</b> which includes a roundtrip economy airfare and a 5 night hotel stay at a destination which you can choose from a list we provide upon winning. Alternatively, you will also have an option to take a one-time discounted coupon that you can use for booking a hotel on our site instead of the trip. Want to know which position your company takes place? Please see your LIVE ranking below:</p>
                </div>
            </div>  
        </div>