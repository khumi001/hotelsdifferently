
   <style type="text/css">
      .card-img-holder {
      margin: 0 0 -12px;
      }
      .card-img-holder > img {
      display: block;
      margin: 0 auto 10px;
      max-width: 150px;
      }
      .radio.radio-danger {
      display: inline-flex;
      margin: 0 auto 0 42px;
      text-align: center;
      width: 100%;
      }
      .radio.radio-danger &gt; input {
      margin: 0 5px;
      }
      .radio.radio-danger &gt; label {
      font-size: 12px;
      font-weight: 600;
      }
      .payment.no-pding .col-md-offset-2.col-md-5{
      padding-right: 0;
      }
      .checkbox.check_out, .radio {
      display: inline-block;
      margin: 0;
      min-height: 0;
      padding-left: 0;
      }
      .checkbox.check_out input {
      margin: 3px 10px 0 0;
      }
      .text-danger.note-para {
      color: red;
      display: block;
      font-size: 13px;
      margin: 15px 0 0;
      }
      .form-group {
      display: block;
      overflow: hidden;
      }
      .btn-primary.btn-red {
      background-color: red;
      border-color: red;
      color: #fff;
      margin: 0 0 0 175px;
      width: 75px;
      }
      .bill-mrgn-sec {
      margin: 20px 0 15px;
      }
      .billing-name{
      display: block;
      margin: 8px 0;
      }
      #uniform-radio3 input {
      margin: 2px 5px 0;
      }
      #uniform-radio2 input {
      margin: 2px 5px 0;
      }
      .form-control.input-medium.secuity-question {
      margin: 0;
      width: 150px;
      }
      .control-label.col-sm-3.mrgn-lebel-top {
      position: relative;
      top: 7px;
      }
     /* .width-row {
      margin: 0 auto;
      width: 700px;
      }*/
   .card-img-holder {
   margin: 0 0 -12px;
   }
   .card-img-holder > img {
   display: block;
   margin: 0 auto;
   max-width: 150px;
   }
   .radio.radio-danger {
   display: inline-flex;
   margin: 0 auto 0 42px;
   text-align: center;
   width: 100%;
   }
   .radio.radio-danger > input {
   margin: 0 5px;
   }
   .radio.radio-danger > label {
   font-size: 12px;
   font-weight: 600;
   }
   .payment.no-pding .col-md-offset-2.col-md-5{
   padding-right: 0;
   }
   .checkbox.check_out, .radio {
   display: inline-block;
   margin: 0;
   min-height: 0;
   padding-left: 0;
   }
   .checkbox.check_out input {
   margin: 3px 10px 0 0;
   }
   .text-danger.note-para {
   color: red;
   display: block;
   font-size: 13px;
   margin: 15px 0 0;
   }
   .form-group {
   display: block;
   overflow: hidden;
   }
   .btn-primary.btn-red {
   background-color: red;
   border-color: red;
   color: #fff;
   }
   </style>
   <div class="width-row" style="width:700px;margin:0 auto;display: block; ">
      <div class="tab-content wizTab " id="traveller_information_body">
         <h4>Payment Method</h4>
         <div class="payment no-pding">
            <div class="col-md-offset-2 col-md-5">
               <div class="card-img-holder">
                  <img src="http://hlive.matrix-intech.com/public/assets/img/credit-crd.jpg" alt="logo">
               </div>
               <div class="radio radio-danger">
                  <input name="radio2" id="radio3" value="option1" type="radio">
                  <label for="radio3">
                  Credit card on file ending with 1234
                  </label>
               </div>
            </div>
            <div class="col-md-5">
               <div class="card-img-holder">
                  <img src="http://hlive.matrix-intech.com/public/assets/img/Bitcoin-Work.png" alt="logo">
               </div>
               <div class="radio radio-danger">
                  <input name="radio2" id="radio2" value="option1" type="radio">
                  <label for="radio2">
                  BitCoin payment
                  </label>
               </div>
            </div>
            <div class="clearfix"></div>
         </div>
         <span class="payment-errors"></span>
         <div class="form-group hidden_div" style="display: block;">
             <label class="control-label col-sm-3">Card Number:</label>
             <div class="col-sm-7">
                 <input class="form-control input-medium" id="card-number" placeholder="Enter first 11 digits for AMEX or first 12 digits for VISA/MC" required="" type="text">
             </div>
             <div class="col-sm-2">
                <strong class="billing-name" style="font-size: 20px; position: relative; bottom: 5px; right: 20px;">1234</strong>
             </div>
         </div>
         <div class="form-group hidden_div" style="display: block;">
            <label class="control-label col-sm-3">Expiration:</label>
            <div class="col-sm-9">
                  <div class="row">
                     <div class="col-sm-6">
                       <select class="invalid_number invalid_expiry_month cardInfo select2me form-control input-xlarge" data-stripe="exp_month" id="card-expiry-month" name="data[month]" required="">
                         <option class="" value="-1" selected="selected">month</option>

                         <option class="" value="01">01</option>
                         <option class="" value="02">02</option>
                         <option class="" value="03">03</option>
                         <option class="" value="04">04</option>
                         <option class="" value="05">05</option>
                         <option class="" value="06">06</option>
                         <option class="" value="07">07</option>
                         <option class="" value="08">08</option>
                         <option class="" value="09">09</option>
                         <option class="" value="10">10</option>
                         <option class="" value="11">11</option>
                         <option class="" value="12">12</option>
                     </select>
                  </div>
                  <div class="col-sm-6">
                       <select class="invalid_number invalid_expiry_year cardInfo select2me form-control input-xlarge" data-stripe="exp_year" id="card-expiry-year" name="data[year]" required="">
                         <option class="" value="-1" selected="selected">year</option>
                         <option class="" value="2017">2017</option>
                         <option class="" value="2018">2018</option>
                         <option class="" value="2019">2019</option>
                         <option class="" value="2020">2020</option>
                         <option class="" value="2021">2021</option>
                         <option class="" value="2022">2022</option>
                         <option class="" value="2023">2023</option>
                         <option class="" value="2024">2024</option>
                         <option class="" value="2025">2025</option>
                         <option class="" value="2026">2026</option>
                         <option class="" value="2027">2027</option>
                         <option class="" value="2028">2028</option>
                         <option class="" value="2029">2029</option>
                         <option class="" value="2030">2030</option>
                         <option class="" value="2031">2031</option>
                         <option class="" value="2032">2032</option>
                      </select>
                  </div>
               </div>
             </div>
          </div>
          <div class="form-group hidden_div" style="display: block;">
             <label class="control-label col-sm-3">Security Code:</label>
             <div class="col-sm-9">
                 <input data-stripe="cvc" class="form-control input-medium" id="card-cvc" required="" type="text">
             </div>
         </div>
         <h4 class="bill-mrgn-sec">Billing Information</h4>
         <form method="post" action="http://hlive.matrix-intech.com/request_quote?stp=th" id="payment-form" class="form-horizontal form-bordered">
            <input id="th_fname" onkeyup="update_name();" name="data[firstname]" class="form-control input-medium" value="Naeem" required="" type="hidden">
            <input id="th_lname" onkeyup="update_name();" name="data[lastname]" class="form-control input-medium" value="Shehzad" required="" type="hidden">
            <input id="stripe_name" data-stripe="name" value="Naeem Shehzad" type="hidden">
            <input id="last_four" name="data[lastfour]" value="" type="hidden">
            <div class="form-group">
               <label class="control-label col-sm-3"> Billing Name:</label>
               <div class="col-sm-9">
                  <strong class="billing-name">Rehan Kausar</strong>
               </div>
            </div>
            <div class="form-group">
               <label class="control-label col-sm-3"> Phone:</label>
               <div class="col-sm-9">
                  <input name="data[phone]" class="form-control input-medium" id="phone" value="54654" required="" type="text">
               </div>
            </div>
            <div class="form-group">
               <label class="control-label col-sm-3">Country:</label>
               <div class="col-sm-9">
                  <select class="form-control">
                     <option></option>
                     <option value="3">AFGHANISTAN</option>
                     <option value="5">ALBANIA</option>
                     <option value="52">ALGERIA</option>
                     <option value="1">ANDORRA</option>
                     <option value="7">ANGOLA</option>
                     <option value="8">ANTARCTICA</option>
                     <option value="4">ANTIGUA AND BARBUDA</option>
                     <option value="9">ARGENTINA</option>
                     <option value="6">ARMENIA</option>
                     <option value="12">ARUBA</option>
                     <option value="11">AUSTRALIA</option>
                     <option value="10">AUSTRIA</option>
                     <option value="13">AZERBAIJAN</option>
                     <option value="27">BAHAMAS</option>
                     <option value="20">BAHRAIN</option>
                     <option value="16">BANGLADESH</option>
                     <option value="15">BARBADOS</option>
                     <option value="30">BELARUS</option>
                     <option value="17">BELGIUM</option>
                     <option value="31">BELIZE</option>
                     <option value="22">BENIN</option>
                     <option value="23">BERMUDA</option>
                     <option value="28">BHUTAN</option>
                     <option value="25">BOLIVIA</option>
                     <option value="14">BOSNIA AND HERZEGOVINA</option>
                     <option value="29">BOTSWANA</option>
                     <option value="26">BRAZIL</option>
                     <option value="87">BRITISH INDIAN OCEAN TERRITORY</option>
                     <option value="24">BRUNEI DARUSSALAM</option>
                     <option value="19">BULGARIA</option>
                     <option value="18">BURKINA FASO</option>
                     <option value="21">BURUNDI</option>
                     <option value="98">CAMBODIA</option>
                     <option value="39">CAMEROON</option>
                     <option value="32">CANADA</option>
                     <option value="44">CAPE VERDE</option>
                     <option value="34">CENTRAL AFRICAN REPUBLIC</option>
                     <option value="178">CHAD</option>
                     <option value="38">CHILE</option>
                     <option value="40">CHINA</option>
                     <option value="41">COLOMBIA</option>
                     <option value="100">COMOROS</option>
                     <option value="35">CONGO</option>
                     <option value="33">CONGO, DEMOCRATIC REPUBLIC OF THE</option>
                     <option value="42">COSTA RICA</option>
                     <option value="37">COTE D'IVOIRE</option>
                     <option value="80">CROATIA</option>
                     <option value="43">CUBA</option>
                     <option value="45">CYPRUS</option>
                     <option value="46">CZECH REPUBLIC</option>
                     <option value="49">DENMARK</option>
                     <option value="48">DJIBOUTI</option>
                     <option value="50">DOMINICA</option>
                     <option value="51">DOMINICAN REPUBLIC</option>
                     <option value="53">ECUADOR</option>
                     <option value="55">EGYPT</option>
                     <option value="175">EL SALVADOR</option>
                     <option value="74">EQUATORIAL GUINEA</option>
                     <option value="57">ERITREA</option>
                     <option value="54">ESTONIA</option>
                     <option value="59">ETHIOPIA</option>
                     <option value="63">FAROE ISLANDS</option>
                     <option value="61">FIJI</option>
                     <option value="60">FINLAND</option>
                     <option value="64">FRANCE</option>
                     <option value="69">FRENCH GUIANA</option>
                     <option value="179">FRENCH SOUTHERN TERRITORIES</option>
                     <option value="65">GABON</option>
                     <option value="71">GAMBIA</option>
                     <option value="68">GEORGIA</option>
                     <option value="47">GERMANY</option>
                     <option value="70">GHANA</option>
                     <option value="75">GREECE</option>
                     <option value="67">GRENADA</option>
                     <option value="73">GUADELOUPE</option>
                     <option value="76">GUATEMALA</option>
                     <option value="72">GUINEA</option>
                     <option value="77">GUINEA-BISSAU</option>
                     <option value="78">GUYANA</option>
                     <option value="81">HAITI</option>
                     <option value="79">HONDURAS</option>
                     <option value="82">HUNGARY</option>
                     <option value="90">ICELAND</option>
                     <option value="86">INDIA</option>
                     <option value="83">INDONESIA</option>
                     <option value="89">IRAN, ISLAMIC REPUBLIC OF</option>
                     <option value="88">IRAQ</option>
                     <option value="84">IRELAND</option>
                     <option value="85">ISRAEL</option>
                     <option value="91">ITALY</option>
                     <option value="93">JAMAICA</option>
                     <option value="95">JAPAN</option>
                     <option value="92">JERSEY</option>
                     <option value="94">JORDAN</option>
                     <option value="105">KAZAKHSTAN</option>
                     <option value="96">KENYA</option>
                     <option value="99">KIRIBATI</option>
                     <option value="102">KOREA, DEMOCRATIC PEOPLE'S REPUBLIC OF</option>
                     <option value="103">KOREA, REPUBLIC OF</option>
                     <option value="104">KUWAIT</option>
                     <option value="97">KYRGYZSTAN</option>
                     <option value="106">LAO PEOPLE'S DEMOCRATIC REPUBLIC</option>
                     <option value="115">LATVIA</option>
                     <option value="107">LEBANON</option>
                     <option value="112">LESOTHO</option>
                     <option value="111">LIBERIA</option>
                     <option value="116">LIBYAN ARAB JAMAHIRIYA</option>
                     <option value="109">LIECHTENSTEIN</option>
                     <option value="113">LITHUANIA</option>
                     <option value="114">LUXEMBOURG</option>
                     <option value="126">MACAU</option>
                     <option value="122">MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF</option>
                     <option value="121">MADAGASCAR</option>
                     <option value="132">MALAWI</option>
                     <option value="134">MALAYSIA</option>
                     <option value="131">MALDIVES</option>
                     <option value="123">MALI</option>
                     <option value="127">MARTINIQUE</option>
                     <option value="128">MAURITANIA</option>
                     <option value="130">MAURITIUS</option>
                     <option value="133">MEXICO</option>
                     <option value="62">MICRONESIA, FEDERATED STATES OF</option>
                     <option value="119">MOLDOVA</option>
                     <option value="118">MONACO</option>
                     <option value="125">MONGOLIA</option>
                     <option value="120">MONTENEGRO</option>
                     <option value="129">MONTSERRAT</option>
                     <option value="117">MOROCCO</option>
                     <option value="135">MOZAMBIQUE</option>
                     <option value="124">MYANMAR</option>
                     <option value="136">NAMIBIA</option>
                     <option value="142">NEPAL</option>
                     <option value="140">NETHERLANDS</option>
                     <option value="143">NEW ZEALAND</option>
                     <option value="139">NICARAGUA</option>
                     <option value="137">NIGER</option>
                     <option value="138">NIGERIA</option>
                     <option value="141">NORWAY</option>
                     <option value="144">OMAN</option>
                     <option value="149">PAKISTAN</option>
                     <option value="153">PALAU</option>
                     <option value="151">PALESTINIAN TERRITORY</option>
                     <option value="145">PANAMA</option>
                     <option value="147">PAPUA NEW GUINEA</option>
                     <option value="154">PARAGUAY</option>
                     <option value="146">PERU</option>
                     <option value="148">PHILIPPINES</option>
                     <option value="150">POLAND</option>
                     <option value="152">PORTUGAL</option>
                     <option value="155">QATAR</option>
                     <option value="156">REUNION</option>
                     <option value="157">ROMANIA</option>
                     <option value="159">RUSSIAN FEDERATION</option>
                     <option value="160">RWANDA</option>
                     <option value="166">SAINT HELENA</option>
                     <option value="101">SAINT KITTS AND NEVIS</option>
                     <option value="108">SAINT LUCIA</option>
                     <option value="195">SAINT VINCENT AND THE GRENADINES</option>
                     <option value="170">SAN MARINO</option>
                     <option value="174">SAO TOME AND PRINCIPE</option>
                     <option value="161">SAUDI ARABIA</option>
                     <option value="171">SENEGAL</option>
                     <option value="158">SERBIA</option>
                     <option value="163">SEYCHELLES</option>
                     <option value="169">SIERRA LEONE</option>
                     <option value="168">SLOVAKIA</option>
                     <option value="167">SLOVENIA</option>
                     <option value="162">SOLOMON ISLANDS</option>
                     <option value="172">SOMALIA</option>
                     <option value="200">SOUTH AFRICA</option>
                     <option value="58">SPAIN</option>
                     <option value="110">SRI LANKA</option>
                     <option value="164">SUDAN</option>
                     <option value="173">SURINAME</option>
                     <option value="177">SWAZILAND</option>
                     <option value="165">SWEDEN</option>
                     <option value="36">SWITZERLAND</option>
                     <option value="176">SYRIAN ARAB REPUBLIC</option>
                     <option value="188">TAIWAN, PROVINCE OF CHINA</option>
                     <option value="182">TAJIKISTAN</option>
                     <option value="189">TANZANIA, UNITED REPUBLIC OF</option>
                     <option value="181">THAILAND</option>
                     <option value="183">TIMOR-LESTE (EAST TIMOR)</option>
                     <option value="180">TOGO</option>
                     <option value="187">TRINIDAD AND TOBAGO</option>
                     <option value="185">TUNISIA</option>
                     <option value="186">TURKEY</option>
                     <option value="184">TURKMENISTAN</option>
                     <option value="191">UGANDA</option>
                     <option value="190">UKRAINE</option>
                     <option value="2">UNITED ARAB EMIRATES</option>
                     <option value="66">UNITED KINGDOM</option>
                     <option value="192" selected="">UNITED STATES</option>
                     <option value="193">URUGUAY</option>
                     <option value="194">UZBEKISTAN</option>
                     <option value="198">VANUATU</option>
                     <option value="196">VENEZUELA</option>
                     <option value="197">VIETNAM</option>
                     <option value="56">WESTERN SAHARA</option>
                     <option value="199">YEMEN</option>
                     <option value="201">ZAMBIA</option>
                     <option value="202">ZIMBABWE</option>
                  </select>
               </div>
            </div>
            <div class="form-group">
               <label class="control-label col-sm-3">State:</label>
               <div class="col-sm-9">
                  <select class="form-control">
                     <option value="Alabama">Alabama</option>
                     <option value="Alaska">Alaska</option>
                     <option value="Arizona">Arizona</option>
                     <option value="Arkansas">Arkansas</option>
                     <option value="California">California</option>
                     <option value="Colorado">Colorado</option>
                     <option value="Connecticut">Connecticut</option>
                     <option value="Delaware">Delaware</option>
                     <option value="Florida">Florida</option>
                     <option value="Georgia">Georgia</option>
                     <option value="Hawaii">Hawaii</option>
                     <option value="Idaho">Idaho</option>
                     <option value="Illinois">Illinois</option>
                     <option value="Indiana">Indiana</option>
                     <option value="Iowa">Iowa</option>
                     <option value="Kansas">Kansas</option>
                     <option value="Kentucky">Kentucky</option>
                     <option value="Louisiana">Louisiana</option>
                     <option value="Maine">Maine</option>
                     <option value="Maryland">Maryland</option>
                     <option value="Massachusetts">Massachusetts</option>
                     <option value="Michigan">Michigan</option>
                     <option value="Minnesota">Minnesota</option>
                     <option value="Mississippi">Mississippi</option>
                     <option value="Missouri">Missouri</option>
                     <option value="Montana">Montana</option>
                     <option value="Nebraska">Nebraska</option>
                     <option value="Nevada">Nevada</option>
                     <option value="New Hampshire">New Hampshire</option>
                     <option value="New Jersey">New Jersey</option>
                     <option value="New Mexico">New Mexico</option>
                     <option value="New York">New York</option>
                     <option value="North Carolina">North Carolina</option>
                     <option value="North Dakota">North Dakota</option>
                     <option value="Ohio">Ohio</option>
                     <option value="Oklahoma">Oklahoma</option>
                     <option value="Oregon">Oregon</option>
                     <option value="Pennsylvania">Pennsylvania</option>
                     <option value="Rhode Island">Rhode Island</option>
                     <option value="South Carolina">South Carolina</option>
                     <option value="South Dakota">South Dakota</option>
                     <option value="Tennessee">Tennessee</option>
                     <option value="Texas">Texas</option>
                     <option value="Utah">Utah</option>
                     <option value="Vermont">Vermont</option>
                     <option value="Virginia">Virginia</option>
                     <option value="Washington">Washington</option>
                     <option value="West Virginia">West Virginia</option>
                     <option value="Wisconsin">Wisconsin</option>
                     <option value="Wyoming">Wyoming</option>
                  </select>
               </div>
            </div>
            <div class="form-group">
               <label class="control-label col-sm-3">Zip:</label>
               <div class="col-sm-9">
                  <input data-stripe="address_zip" id="address-zip" value="" name="data[zip]" class="form-control input-medium" required="" type="text">
               </div>
            </div>
            <div class="form-group">
               <label class="control-label col-sm-3">City:</label>
               <div class="col-sm-9">
                  <input data-stripe="address_city" id="address_city" value="" name="data[city]" class="form-control input-medium" required="" type="text">
               </div>
            </div>
            <div class="form-group">
               <label class="control-label col-sm-3">Street:</label>
               <div class="col-sm-9">
                  <input data-stripe="address_line1" id="address_line1" value="" name="data[street]" class="form-control input-medium" required="" type="text">
               </div>
            </div>
            <div class="clearfix mt20">
               <button type="button" onclick="javascript:location.href='http://hlive.matrix-intech.com/request_quote?stp=f'" class="btn btn-primary pull-left btn-red">Back</button>
               <button type="submit" class="btn btn-primary pull-right">Continue</button>
            </div>
         </form>
      </div>
   </div>
