<style type="text/css">
    .color-red{
        color:red;
        font-weight:bold;
    }
    .bold-font{
        font-weight: bold;
    }
    .verif-txt{
        margin: 17px 0;
    }
    .member-msg{
      margin: 17px 0;
    }
</style>
<div class="width-row" style="background-color: rgba(255, 255, 255, 0.74) none repeat scroll 0 0;">
	<div class="main_cont">
        <div class="" style="margin: 10px 0;">
          <p>Dear Rehan kausar,</p>
        </div>
        <p>
            Thank you very much for your payment for your  membership! 
            <span style="display: block;">Your payment of Amount that was paid (e.g: <span class="bold-font">$89.99</span>) for your membership was successfully charged and your charges will appear as <span class="color-red">"HOTELSDIFF"</span> or <span class="color-red">"HOTELSDFRNTL8882872307"</span> and a receipt has been sent to you as well.</span>
        </p>
        <p>
            <span style="text-decoration:; font-weight:bold;display: block;">What happens next? </span> In order to verify that you are the authorized cardholder, we have issued a refund back to your credit card. Please check your online statement once it posts and log back in to verify the amount that was refunded back to your card. Once you verify the refund amount, your account will be active and ready to use. Please note that you only have 3 attempts and upon the third unsuccessful attempt we will revoke your membership and refund the full remaining amount for your membership due to security risks. Upon successful verification – since we cannot revert a refund that we have already issued – you will be charged for the refunded amount. 
            <ins>Example:</ins> You paid $89.99 for your  membership and you are charged that amount, then we will issue a refund of – let’s say - $1.99. Upon successful verification, we will charge your account for the same refund amount ($1.99) so your total charge will not change.

		</p>

        <p class="verif-txt">Upon <span class="bold-font">unsuccessful verification,</span> we will refund the remainder amount of – in this case – $88.00. </p>
        <p  class="member-msg"><b>Would you like to get</b><span class="color-red"> 1 EXTRA MONTH MEMBERSHIP FOR FREE? </span> <!-- Go to MY ACCOUNT – REFER A FRIEND and refer your friends. 
        <span class="bold-font">Every 3 friends </span>you refer to sign up, pay and successfully enroll on our site, you get <span class="bold-font">1 YEAR MEMBERSHIP EXTENSION FREE.</span> Furthermore, you will be sending your friends a discounted link for a membership. Be good to them and be rewarded for it ! --> Go to MY ACCOUNT – REFER A FRIEND and refer your friends. Every friend you refer to sign up, pay <ins>and</ins> successfully enroll on our site, you get 1 MONTH MEMBERSHIP EXTENSION FREE. Furthermore, you will be sending your friends a discounted link for a membership. Be good to them and be rewarded for it! </p>
        <!-- <p  class="member-msg"><span class="bold-font">Life without limits!</span> Interested in our 
        <span class="bold-font">Lifetime Plan? </span> Reach out to our customer service to inquire about pricing!</p> -->

        <p  class="member-msg">We are very much looking forward to a mutually beneficial relationship with you and wish you lots of savings on your bookings!</p>

	    <p style="margin: 0 0 60px;">Thank you for choosing <b>HotelsDifferently.com</b> where we are <i>Making hotels happen for YOU!</i></p>
        </p>                
    </div>
</div>
