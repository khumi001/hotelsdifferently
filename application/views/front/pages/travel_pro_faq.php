<div class="page_container">
   <div class="width-row margin-top-20">
      <div class="width-row" style="display:none;padding: 10px 0 0 8px;">
         <div itemscope="" itemtype="http://schema.org/Person">
            <span itemprop="name">Hotels Differently</span>
            <span itemprop="company">HotelsDifferently LLC</span>
            <span itemprop="tel">888-287-2307</span>
         </div>
      </div>
      <div class="main_cont">
         <div class="pagetitle margin-bottom-10">
            <h1>TRAVEL PROFESSIONAL FAQ</h1>
         </div>
         <!--    <div class="faqs_title">Account related questions:</div>-->
         <div class="row text-center margin-bottom-10 margin-top-10">
            <div class="form-group col-md-12 text-center margin-bottom-10">
               <label class="control-label col-md-5" style="padding: 6px 0;width: 37%;"> Questions? Type in a keyword (e.g: password):</label>
               <div class="col-md-7" style="padding: 0 5px;">
                  <input name="que_search" id="que_search" class="form-control" type="text">
               </div>
            </div>
         </div>
         <div id="faqsearch">
            <div class="questions">
               <span>Q 01</span>
               I forgot my password, how do I retrieve it?
            </div>
            <div class="answers">
               No worries! To retrieve your password, please go to the homepage, click on the "Login" button, click on the "Forgot your password?" text, type in your email address then click on the "SUBMIT" button and your password should arrive instantly to your inbox. <br><br>
               Does it seem like it has been taking too long and you still have not received your password? Please check your SPAM/JUNK folder or try again. 
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 02</span>
               How do I change my email address?
            </div>
            <div class="answers">
               To change your email address, you first need to login to your account. Once you are logged in, go to "My Account" then click on the "Change Email Address" tab. Once you submit changes, you will receive a notification email to your old email address and an activation link to the new email address. Your new email address will only become active once you confirm it by clicking on the link in the email.
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 03</span>
               How do I change my name in my account?
            </div>
            <div class="answers">
               In order to prevent abuse and fraud, we require legal documentation for name changes. First, upload it somewhere (such as Dropbox or other free file-sharing services) and send us the link so we can review it, then send us a message (while logged-in) via the Contact Us form and write a short request so we will know what you would like to accomplish. As soon as we were able to verify the authenticity of your uploaded document, we will change your name on your account.
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 04</span>
               What payment options are available if I make a booking?
            </div>
            <div class="answers">
               We currently accept AndroidPay, ApplePay, Visa, MasterCard, Discover, American Express, BitCoin&trade; and PayPal&trade; and use a mix of payment processors.
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 05</span>
               When will I be charged for the reservation I make?
            </div>
            <div class="answers">
               Your charge will commence instantly once you submit your payment. In rare occasions your payment is put on hold until the hotel confirms your reservation request and these are labeled "ON REQUEST".
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 06</span>
               How does the reservation confirmation work?
            </div>
            <div class="answers">
               Once you pay for your reservation, your reservation is immediately confirmed. When you see “ON REQUEST” then your payment is put on hold until the hotel confirms your reservation request usually within 48 hours. You can retrieve your reservation details from the website, when you go to Reservations - Pending reservations. 
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 07</span>
               Can I put the reservation onto 2 (or more) separate credit cards or debit cards?
            </div>
            <div class="answers">
               Unfortunately, no. The general rule is one transaction, one payment.
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 08</span>
               What are my check-in and check-out times at my hotel?
            </div>
            <div class="answers">
               Every hotel's check-in and check-out times vary greatly. Please make sure to book your hotel only if you have checked and agreed to the hotel's check-in and check-out times. For more information, please feel free to visit your hotel's website or call them.
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 09</span>
               What are the pet policies of my hotel?   
            </div>
            <div class="answers">
               Every hotel's pet policy varies greatly. Please make sure to book your hotel only if you are aware of its pet policy. For more information, please feel free to visit your hotel's website or call them.
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 10</span>
               How do I get a receipt for my booking?
            </div>
            <div class="answers">
               Once you successfully pay for your reservation, you can immediately download your official receipt/invoice that is generated by our system. Furthermore, the receipt will also be downloadable in your account under Reservations &ndash; Hotel Reservations.
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 11</span>
               Can I cancel my reservation? 
            </div>
            <div class="answers">
               The cancellation policy depends on your booking. Please check the booking details in the Reservations &ndash; Hotel Reservations menu. Your cancellation policy will appear in the "Cancellation Policy" column. 
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 12</span>
               What if I find a lower rate on another website once I made a booking with you? Can you match it?
            </div>
            <div class="answers">
               Unfortunately, we do not offer such service at this time BUT if your reservation is fully refundable, then you can cancel it, get the refund and book the hotel elsewhere. To double-check your reservation's cancellation policy, please go to Reservations &ndash; Hotel Reservations menu. Your cancellation policy will appear in the "Cancellation Policy" column. 
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 13</span>
               What is needed for check-in at the hotel?
            </div>
            <div class="answers">
               Most hotels only require a valid driver's license, government issued ID or a passport. Certain hotels have age restrictions, so please check that to ensure that you will not have a problem during check-in. Should you have any concerns regarding to the hotel's minimum age check-in policy, please do not hesitate to contact them. Also, most hotels also require a deposit which also varies from one hotel to another. Some do not need a deposit; others only accept credit cards while certain hotels accept cash as a deposit as well. It is advisable to check with the hotel beforehand.
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 14</span>
               Is there a loyalty program?
            </div>
            <div class="answers">
               Currently, we do NOT offer a loyalty program; we strictly focus on " what we consider " the most important part of a hotel booking experience: <b class="bullet_color" style="color: red;">THE PRICE!</b> <br>
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 15</span>
               HOW CAN YOUR RATES BE LOWER (sometimes substantially) THAN ANY OF THE MAJOR LISTING SITES?
            </div>
            <div class="answers">
               We have an extremely extensive network and database of travel agencies WORLDWIDE; certain rates can only be accessed by local agencies, direct discounted rates are provided from select hotels in certain situations or consolidators as well as our contracted wholesale suppliers.
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 16</span>
               Will I be able to earn points with my hotel program that I am a Member of?
            </div>
            <div class="answers">
               Every hotel's loyalty program differs greatly and some of them change without any prior notice. It is advisable to ask your hotel's loyalty program to see if you can earn points on your stay if you book your reservation outside of the hotel's own website.
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 17</span>
               I know that there is no refund on non-refundable reservations but what happens if the stay has to be canceled due to extreme circumstances, such as a flight cancellation or an accident?
            </div>
            <div class="answers">
               <p>Unfortunately, these things do happen to some of us. Our recommendation is to use a credit card for the booking that covers these extreme situations, so you are protected in this unlikely event. 
                  <br>
                  Another option for you is to purchase travel insurance from a third party insurance company. Please note that we do not deal with or sell any types of insurances, we are strictly Affiliates and any insurance concerns should be addressed with the insurance company directly.
                  <br>
               </p>
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 18</span>
               Where can I see my confirmed reservations?
            </div>
            <div class="answers">
               To retrieve your confirmed reservations, please go to Reservations &ndash; Hotel Reservations. 
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 19</span>
               Can I make a booking over the phone?
            </div>
            <div class="answers">
               Unfortunately, making a booking over the phone is not an option at this time. We prefer everything to be in writing to avoid any unnecessary disputes and miscommunication. 
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 20</span>
               Can I have more guests check-in than the amount of guests I specified when I made the booking?
            </div>
            <div class="answers">
               The rate you paid for is determined by the number of people on your reservation. If more people check-in than it was originally included in the booking, then the hotel might charge you additional fees. To inquire regarding this potential surcharge, please contact the hotel directly.
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 21</span>
               I accidentally canceled my reservation. What can I do now?
            </div>
            <div class="answers">
               Once a reservation is cancelled, we cannot reinstate it. If your cancellation request has already gone through and the reservation was refundable, then your refund is processed automatically. If your reservation was non-refundable, then you are not entitled to a refund.
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 22</span>
               Can I book directly with the hotel and receive the same rate? 
            </div>
            <div class="answers">
               We are able to provide you a low rate because we have access to a discounted rates due to our buying power and number of bookings, that are NOT available to the general public. In addition, in some circumstances, we may have packaged amenities (such as a complimentary breakfast) that would not be available to you otherwise. So trying to book directly with the hotel in an attempt to get the same low price directly from them; even if it was available could cause you to lose certain amenities. As the old saying goes: "You have to compare apples to apples and oranges to oranges".
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 23</span>
               What is the Cancellation Policy? 
            </div>
            <div class="answers">
               The Cancellation Period is fully defined in our Terms and Conditions Policy, but essentially it is the length of time, if any, that is stated in the invoice/payment page and subsequently in the Reservations - Confirmed Reservations menu. The Cancellation Policy will either tell you (a) that your reservation is non-refundable, or (b) the number of days you have, prior to check-in date and time (using the hotel's date and time) in order to cancel your reservation and receive a refund.
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 24</span>
               When am I entitled to a refund?
            </div>
            <div class="answers">
               <p>You are entitled to a refund as long as your Cancellation Policy was not listed as non-refundable <b><u>AND</u></b> you cancelled prior to the expiration of the Cancellation Policy for a refundable reservation <b><u>AND</u></b> you complied with all of our other Terms and Conditions.</p>
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 25</span>
               What happens if I stay fewer nights at the hotel and want a refund? 
            </div>
            <div class="answers">
               Please understand that the rate we obtained for you for your stay was based, among other things, upon the length of your stay. It is quite possible that if you had initially requested a shorter stay, the rate per night we would have been able to offer you could have been considerably higher.
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 26</span>
               Can I cancel directly with the hotel that your Site booked for me?
            </div>
            <div class="answers">
               Nothing prohibits you from doing so, but please be aware that you will not receive ANY refund from the hotel. If your Cancellation Policy with us has expired or was non-refundable, we will be unable to provide you a refund. You must notify us in accordance with our Cancellation Policy to be eligible for a refund; cancelling with the hotel without notifying us as required by our Cancellation Policy will preclude you receiving a refund from us. In addition, the hotel will be unable to provide you with a refund (please see the question directly below). 
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 27</span>
               Can I receive a refund directly from the hotel? 
            </div>
            <div class="answers">
               No. We paid the hotel on your behalf, and we are the only entity legally entitled to a refund from the hotel for any reason. The hotel will NOT provide a refund directly to you.
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 28</span>
               What if I am not satisfied with my stay?
            </div>
            <div class="answers">
               If you were dissatisfied by the hotel staff, your room(s), or the hotel otherwise didn't meet your personal standards; it is your responsibility to bring those concerns to the attention of hotel management and your recourse is solely limited to whatever the hotel may or may not offer you, to make amends. Of course, we would welcome you using our Contact Us function after your stay to let us know of any difficulties you encountered, and what, if anything, the hotel did to accommodate you. This will help us in making hotel recommendations to you and other Members in the future.
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 29</span>
               If I am not satisfied with my stay, or your refund policy, may I dispute the charge on my credit card/debit card or any other form of payment? 
            </div>
            <div class="answers">
               While we cannot prohibit a Member from filing a dispute, each Member has, as a condition of booking on our Site has agreed to our Terms and Conditions and all Other Policies, including those related to cancellation and refunds. Where we have provided the Services as promised and in accordance with our Terms and Conditions, we will take all necessary steps to refute any attempt on the part of a Member to dispute their payment to us for any reason, regardless of the form of payment.
               <br>
               <br>
               In addition, we will endeavor to recover our costs, including but not limited to, legal fees, and any lost income or other damages attributable to an attempted chargeback or similar attempt to avoid or recover monies paid to us. 
               <br>
               <br>
               This policy is necessary to keep prices low for all Members so they are not penalized by anyone who is trying to take advantage of our Services without paying for our Services. We know you understand that we must take the appropriate steps if a Member tries to avoid their contractually binding agreement to pay for our Services.
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 30</span>
               Do I have any recourse at all if I am unhappy with my stay? 
            </div>
            <div class="answers">
               Obviously, <b style="color:#ff0000">we want ALL of Our Members to have a thoroughly pleasant stay EACH and EVERY time they book a room with us.</b> We pride ourselves on our Customer Service, we want you to continue to use our Site, and for you to encourage others to do so as well.
               <br>
               <br>
               <b style="color:#ff0000">So, PLEASE contact us if you are dissatisfied or unhappy for any reason; we will work with you to try and find a mutually agreeable resolution.</b> Unfortunately, we can offer no guarantee that our attempts to totally resolve your concerns may prove completely satisfactory to you, but you can be assured that we will work very hard to make you happy. Any resolutions that we may offer are in our sole discretion.
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 31</span>
               What does your newsletter contain?
            </div>
            <div class="answers">
               Our newsletter contains interesting news, such as: <br>
               <ul class="bullet_color" type="square">
                  <li><span class="ul_text">Our biggest savings of the week</span></li>
                  <li><span class="ul_text">Website updates</span></li>
                  <li><span class="ul_text">Coupons that can be used for future bookings!</span></li>
               </ul>
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 32</span>
               Anything that is not included in the price? What can I be charged for later?
            </div>
            <div class="answers">
               You MIGHT be charged by the hotel for the following in addition to your quoted rate (if applicable): <br>
               <ul class="bullet_color">
                  <li><span class="ul_text">Hospitality or Tourism tax (if applicable)</span></li>
                  <li><span class="ul_text">Resort fees (if applicable) and any applicable taxes</span></li>
                  <li><span class="ul_text">Lodging fees (if applicable)</span></li>
                  <li><span class="ul_text">Any fees in connection with hotel services (such as parking, dry cleaning, internet, breakfast, etc.) unless it is specified as "included" in your reservation. The hotel may charge applicable taxes on some fees.</span></li>
               </ul>
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 33</span>
               How do I cancel my reservation?
            </div>
            <div class="answers">
               <p>To cancel your reservation, please go to Reservations &ndash; Hotel Reservations and click on the "Cancel" button. Please note that once a reservation is cancelled, We can NOT reinstate it. If your reservation was refundable, then your refund will be processed automatically. If your reservation was non-refundable, then you are not entitled to a refund.<br>
               </p>
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 34</span>
               What are the tourism taxes, hospitality taxes or lodigng fees?
            </div>
            <div class="answers">
               Certain cities charge a hospitality taxes, tourism taxes or lodging fees. This is a per person, per night charge that is collected by the hotel and it is not included in your rate.
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 35</span>
               How do I redeem a coupon?
            </div>
            <div class="answers">
               You are welcome to apply any of your coupons on the payment page once you selected your room. On the left side, your eligible coupons will appear and you will be able to apply 1 coupon per reservation.
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 36</span>
               Can I use multiple coupons for the same reservation?
            </div>
            <div class="answers">
               You can currently use ONE COUPON per reservation. If you would like to use multiple coupons, all you need to do is to request the nights separately and apply them individually.
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 37</span>
               Do you sell gift cards?
            </div>
            <div class="answers">
               No, at this time we do NOT offer such service, sorry.
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 38</span>
               How do I subscribe for the newsletter?
            </div>
            <div class="answers">
               To subscribe for our newsletter, please go to the homepage and on the top left section you will see the newsletter subscription. Enter your email address and then confirm it once you receive the confirmation email. As soon as you confirm it, your email address will be added and you will get our newsletter from then on.
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 39</span>
               How do I unsubscribe from the newsletter?
            </div>
            <div class="answers">
               If you would like to stop receiving newsletters from us, you can easily unsubscribe by clicking on the "unsubscribe" hyperlink that is included at the bottom of our newsletters.
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 40</span>
               What is my hotel's policy about children?
            </div>
            <div class="answers">
               Most of the hotels worldwide allow children as long as they are accompanied by an adult. To ensure that the hotel of your choice allows children, simply select the number of children in the drop-down when asking for a quote and you will only receive quotes from hotels that allow children at their property. Many hotels will allow children to stay for free or at a reduced rate when they share a room with parents or guardians and use existing bedding. The age at which this applies varies from hotel to hotel. If your child will be the third person in a room designated for two, extra-person charges may apply but you will see that reflected in your quote.
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 41</span>
               Is it possible to change a name on a reservation and do I have to pay to change it?
            </div>
            <div class="answers">
               Every reservation is different. Some reservations allow name changes, others allow name additions only and there are some where any kind of changes (including name) is not allowed. Please login first and then use the Contact Us menu to send us a question regarding to your specific booking. Be sure to provide as much information as possible in order for us to make the necessary adjustments.
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 42</span>
               How will your charge appear on my credit card statement?
            </div>
            <div class="answers">
               When you make a payment through our credit card processor, your payment will appear as "<b>HOTELSDIFFERENTLY</b>" or "<b>HOTELSDFRNTL8882872307</b>" depending on which payment platform you used. 
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 43</span>
               Can you put my refund onto a credit card other than the one I used to pay for the hotel?
            </div>
            <div class="answers">
               No. For your and our protection, we can only refund your payment onto the original form of payment. Has your credit card number since changed? Check out our next Q&amp;A. <br>
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 44</span>
               My credit card information was compromised elsewhere and the number on my credit card has changed. I want to cancel my reservation and get the refund but how will you process the refund if my credit card number is blocked? 
            </div>
            <div class="answers">
               Most credit card issuers although they change your credit card number to prevent additional fraud, your account number with the credit card issuer does NOT change. When sending a refund to your already blocked credit card, most issuers will automatically adjust that amount to your new credit card number since it belongs to the same account number; therefore getting a refund to your new credit card number should be automated and no problem. You are always welcome to contact the credit card issuer first to ensure that you get your refund promptly as credit card issuers systems vary.
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 45</span>
               How secure is the payment on this website?
            </div>
            <div class="answers">
               Our payment Site is <b>EXTREMELY SECURE!</b> Our address bar has the green bar, which means we implemented SSL + Extended Validation which is currently one of the most secure system out there. For more information about the SSL+EV, please click <a style="font-weight:bold;" href="https://www.hotelsdifferently.com/user/legal-corner/security-statement" target="_blank">HERE</a> to access our <b>Security Statement.</b> <br><br>
               For our payment processor we use PayPal<sup>TM</sup> and Stripe<sup>TM</sup> that are the most trusted payment processors in the world right now and when you submit your highly sensitive payment information, you will be directed to their payment site to ensuring that your payment information is in safe hands.<br> 
               <b style="color:#ff0000">We DO NOT maintain any credit or debit card information in our systems; it is maintained ONLY with the payment processor!</b>
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 46</span>
               Do you offer travel insurance?
            </div>
            <div class="answers">
               <p>We (<b>HotelsDifferently</b><sup>sm</sup>) do NOT offer travel insurance, however you can purchase travel insurance from a third party insurance company. Please note that we do not directly sell any types of insurance; we are solely an Affiliate to a third party insurance company.</p>
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 47</span>
               What is the minimum legal age for hotel check-in?
            </div>
            <div class="answers">
               Certain hotels have age restrictions, so please check directly with the hotel to ensure that you will not have a problem during check-in. Should you have any concerns regarding to the hotel's minimum age check-in policy, please do not hesitate to contact them.
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 48</span>
               How many rooms can I purchase at once?
            </div>
            <div class="answers">
               The highest amount of rooms that you can purchase is currently set at 9. 
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 49</span>
               What if I have to check-out from the hotel earlier?
            </div>
            <div class="answers">
               Every hotel has its own policy when it comes to early check-outs. Please inquire about it with the hotel of your choice.
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 50</span>
               What does “No Show” mean?
            </div>
            <div class="answers">
               A. Under the cancellation policy on our Site, “No Show” means that if for any reason you are not going to be able to arrive at the Hotel by its stated check in time, it is very important that you call the Hotel directly and advise them that your arrival will be delayed. Each Hotel has its own policy in this regard as to how long they will continue to hold your Confirmed Reservation and what the no show penalty is. In addition, we always recommend that EACH MEMBER always check with a Hotel, even if they do not specify their “No Show” penalty and/or any other Service Provider, e.g., a rental car company as to what their policies are in the event you are unable to arrive by the time the Service Provider has listed as your expected arrival time.
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 51</span>
               Why is that sometimes I cannot see the refund amount I am getting when I try to click on CANCEL RESERVATION?
            </div>
            <div class="answers">
               A. We work with numerous wholesale providers (Service Providers) in the industry in order to get you the lowest possible price on all of our Services. Not every wholesale provider gives us a live refund amount that we can automatically display to you. Rest assured, your refund will be processed promptly in the correct amount and should you encounter any issues, you are welcome to contact us anytime. Please use the Contact Us function and choose the drop down menu labeled “Billing” if you have any questions or concerns.
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 52</span>
               Can I redeem a Coupon or Promo Code for cash or a cash equivalent (a credit to MY debit/credit card, or to my Bitcoin wallet or in some other cash equivalent form like a check payable to me)?  
            </div>
            <div class="answers">
               A. Unfortunately, no. For Coupons and/or Promo Codes, there is a required purchase of our Service(s). No redemption is available for cash, or a cash equivalent, e.g., Bitcoin or a credit to a credit/debit card. Cash value 1/99¢. Coupons/Promo Codes are void if altered, duplicated, copied, reproduced, transferred, sold, purchased, traded, auctioned or exchanged or where prohibited by, restricted by or taxed by law.
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 53</span>
               What is needed for check-in at the hotel?
            </div>
            <div class="answers">
               Most hotels only require a valid driver's license, government issued ID or a passport. Certain hotels have age restrictions, so please check that to ensure that you will not have a problem during check-in. Should you have any concerns regarding to the hotel's minimum age check-in policy, please do not hesitate to contact them. Also, most hotels also require a deposit which also varies from one hotel to another. Some do not need a deposit; others only accept credit cards while certain hotels accept cash as a deposit as well. It is advisable to check with the hotel beforehand.Be sure to also bring your voucher/receipt that you can download anytime from your account as some hotels / tour operators may ask for it.
            </div>
            <div class="ques_border"></div>
             <div class="questions">
               <span>Q 54</span>
               How can I change my credit card information?
            </div>
            <div class="answers">
               In order to change your credit card on file, please contact us by using the “Contact Us” menu and our representative will instruct you on how to proceed. 
            </div>
            <div class="ques_border"></div>
             <div class="questions">
               <span>Q 55</span>
               Can I have more than one credit cards on file?
            </div>
            <div class="answers">
              No. Currently, we can only offer you to store and use one credit card and we need to have all related paperwork stored on our secured and encrypted third party site in order for you to use it.
            </div>
            <div class="ques_border"></div>
             <div class="questions">
               <span>Q 56</span>
              If my agency has more than one travel agents, do they have to sign up separately?
            </div>
            <div class="answers">
              It depends. <br />
              <ins>Scenario A:</ins> If your agency is using more than one debit or credit cards for payments on our site then yes; each agent that has a different debit or credit card would need to enroll under a separate account. In this case, once all of your travel professionals are enrolled, please reach out to us and specify all of those accounts and then our system will merge every individual account that belongs to your agency in order to potentially qualify for any tier discounts (if applicable). <br />
              <ins>Scenario B:</ins> If your agency is using the same one corporate credit or debit card then you can just share your account login credentials amongst all agents in your company and a separate enrollment will not be necessary.

            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 57</span>
              How do you calculate the total revenue for each calendar year?
            </div>
            <div class="answers">
              At the end of every calendar year, we assess total revenue in a simple way:<strong>Amount spent </strong>(this includes the combined amount spent within your account which includes all the agents spending in your agency if applicable) <strong>MINUS</strong> the <strong>total amount of refunds</strong> we issued to those accounts which <strong>equals</strong> to the <strong>TOTAL REVENUE. </strong>

            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 58</span>
              How much does it cost to have an account with HotelsDifferently.com?
            </div>
            <div class="answers">
              The account for qualified entities are <strong>FREE </strong> of charge. HotelsDifferently.com is a paid membership based site which sells memberships to individual users, therefore the website makes money mostly on selling memberships while providing one of the most competitive rates in the industry. If we detect that you abuse this privilege, have an account that it is not in good standing (filing an unreasonable chargeback) or INACTIVE for a 12 month period, then we have a right to revoke your account(s).

            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 59</span>
             When will my account qualify for a tier discount?
            </div>
            <div class="answers">
              If our combined revenue with you has reached any amount above $500,000 in a calendar year, then you need to request your tier discount manually by sending us a message. We will review your request and in case your account is eligible, it will be awarded with a tier discount for the<strong>LIFETIME </strong> of your account. However please note that if we detect any abuse, fraud, unreasonable chargebacks or being inactive for 12 months or longer, then it may jeopardize your tier discount and your privileges can be revoked permanently. 

            </div>
              <div class="ques_border"></div>
            <div class="questions">
               <span>Q 60</span>
             My name has changed and so did on my credit / debit card. How can I change it?
            </div>
            <div class="answers">
              Should you wish to change your name on your account, you need to request it through the contact us menu. In case of name change, you may need to supply supporting documentation (e.g: legal name change document) to us. 
            </div>
            <div class="ques_border"></div>
         </div>
      </div>
   </div>
</div>

