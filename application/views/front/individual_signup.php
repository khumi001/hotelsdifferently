<!DOCTYPE html>
<html lang="en">
    <head lang="en">
        <?php echo $this->load->view('front/includes/header'); ?>
        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,400i,700,700i|Roboto+Slab|Roboto:400,700,700i" rel="stylesheet">
        <style>
            #bg
            {
                background-image: url(<?php echo base_url(); ?>public/affilliate_theme/img/loading-bg.png);
                background-repeat: no-repeat;
                background-attachment: fixed;
                background-position: center auto;
                
            }
            .loading-page{
                background: rgba(255, 255, 255, 0.8) none repeat scroll 0 0;
                max-width: 968px;
                padding: 8px 35px;
                border-radius: 4px;
                margin: 0 auto;
            }
            .loading-page-follow{
                margin-bottom: 44px;
                margin-top: 25px;
                padding-top: 17px;

            }
            .unq-hover {
                transition: all 0.85s ease 0s;
            }
            .unq-hover:hover {
                box-shadow: 0px 1px 20px 0px rgba(0, 0, 0, 0.28);
                transition: all 0.25s ease 0s;
                border-radius: 4px;
            }
            .loading-page .heading > span{
                color: #bc0005;
                font-family: 'Roboto Condensed', sans-serif;
                text-shadow: 2px 2px #fff200;
                font-style: italic;
                font-weight: bold;
                font-size: 24px;
            }
            .loading-page .heading{
                font-family: 'Roboto Condensed', sans-serif;
                color: #00559f;
                font-weight:bold;
                font-size:24px;
            }
            .heading > .content{
                margin-top: 15px;
            }
            .loading-page .sale{
                margin-bottom: 38px;
            }
            .loading-page .DEALS{
                margin-bottom: 44px;
            }
            .loading-page .EXTRA{
                margin-bottom: 28px;
            }
            .loading-page .SPORTING{
                margin-bottom: 10px;
            }
            .heading .loading-list ul{
                font-family: 'Roboto Condensed', sans-serif;
                font-weight:normal;
                color: #252525;
                font-size:16px;
                list-style: none;
                padding:0;
            }
            .heading .loading-list span{
                font-family: 'Roboto Condensed', sans-serif;
                font-weight:bold;                
            }

            .heading .loading-list img{
                display: inline-block;
                vertical-align: middle;
                position: relative;
                margin-right: 5px;
            }
            .heading .loading-list >.no-extra >li > img{
                height: 20px;
                width: 24px;
                margin-right: 5px;
            }
            .heading .loading-list >.no-extra >li{
                margin-bottom: 10px;
                margin-left: 5px;
            }
            .heading .loading-list >.no-extra >li:nth-of-type(1){
                margin-top: 10px;
            }

            .heading .loading-list .rvl-never li span{
                color: #bc0005;
                font-style: italic;
            }
            .heading .loading-list .rvl-never li span:nth-of-type(2){
                font-style: normal;
            }
            .heading .loading-list .no-extra > span:nth-of-type(1){
                font-style: bold;
                background-color:#fff200;
                margin-left: 5px;
            }
            .heading .sold-out > span:nth-of-type(1){
                font-style: normal;
                font-weight: normal;
                color:#252525;
                font-size: 16px;
                display: block;
            }
            .coming-soon > span:nth-of-type(1){
                font-style: normal;
                font-weight: normal;
                color:#252525;
                font-size: 16px;
                text-align: center;
                background-color:#fff200;
            }
            .coming-soon > span:nth-of-type(3){
                font-family: 'Roboto', sans-serif;
                font-style: normal;
                font-weight: normal;
            }
            .coming-soon h1{
                font-family: 'Roboto Slab', serif;
                font-weight: bold;
                text-align: center;
            }
            .coming-soon h1>span{
                color:#bc0005;
                display: block;
            }
            .btn{
                padding: 20px 80px;
                background: rgba(3,58,159,1);
                background: -moz-radial-gradient(center, ellipse cover, rgba(3,58,159,1) 0%, rgba(51,153,255,1) 100%);
                background: -webkit-gradient(radial, center center, 0px, center center, 100%, color-stop(0%, rgba(3,58,159,1)), color-stop(100%, rgba(51,153,255,1)));
                background: -webkit-radial-gradient(center, ellipse cover, rgba(3,58,159,1) 0%, rgba(51,153,255,1) 100%);
                background: -o-radial-gradient(center, ellipse cover, rgba(3,58,159,1) 0%, rgba(51,153,255,1) 100%);
                background: -ms-radial-gradient(center, ellipse cover, rgba(3,58,159,1) 0%, rgba(51,153,255,1) 100%);
                background: radial-gradient(ellipse at center, rgba(3,58,159,1) 0%, rgba(51,153,255,1) 100%);
                filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#033a9f', endColorstr='#3399ff', GradientType=1 );
                color: #fff;
                font-size:16px;
            }
            .btn-block:before{
                content: url(<?php echo base_url(); ?>public/affilliate_theme/img/arrow-left.png);
            }
            .btn-block:after{
                content: url(<?php echo base_url(); ?>public/affilliate_theme/img/arrow-right.png);
            }
            .btn:hover{
                box-shadow: 1px 2px 20px rgba(0, 0, 0, 0.48);
                color: #fff;
                text-shadow: 0px 4px rgba(0, 0, 0, 0.48);
            }

            .btn-block {
                display: block;
                margin-bottom: 52px;
                text-align: center;
            }

        </style>
    </head>
    <body id="bg">
    	<?php echo $this->load->view('front/includes/body_header'); ?>
    	<?php echo $this->load->view('front/includes/navigation'); ?>
    	
        <div class="container loading-page">

            
            <div class="col-md-12 col-sm-12">
                <div class="heading sale">
                    <span>WHOLESALE </span> HOTEL RATES
                    <div class="content">
                        <div class="loading-list">
                            <ul class="whl-rates">
                                <li><img src="<?php echo base_url(); ?>public/affilliate_theme/img/icon-loading.png" alt="list-logo"/><span>WHOLESALE HOTEL RATES </span> are now possible for consumers like you! See how much you can save with us and enjoy the hotels of your dreams!</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="heading DEALS">
                    REVOLUTIONARY FLIGHT DEALS LIKE <span> NEVER SEEN BEFORE !</span>
                    <div class="content">
                        <div class="loading-list">
                            <ul class="rvl-never">
                                <li><img src="<?php echo base_url(); ?>public/affilliate_theme/img/icon-loading.png" alt="list-logo"/>Most Sites offer a network of 400+ airlines. We are Conntected to an aggregate supplier with <span>OVER 900</span><span>!!!</span> <span>More airlines = cheaper prices! </span> Simple, Right?</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-sm-12">
                <div class="heading EXTRA">
                    COMPLIMENTARY COVERAGE* AT <span> NO EXTRA CHARGE !</span>
                    <div class="content">
                        <div class="loading-list">
                            <ul class="no-extra">
                                <span>DELAYS? FLIGHT CANCELLATIONS? These things can happen to us all, but you can get:</span>
                                <li><img src="<?php echo base_url(); ?>public/affilliate_theme/img/icon-loading.png" alt="list-logo"/>Re-accommodation on an alternative flight <span> AND/OR</span></li>
                                <li><img src="<?php echo base_url(); ?>public/affilliate_theme/img/icon-loading.png" alt="list-logo"/>Partial refund of your plane ticket <span> AND/OR</span></li>
                                <li><img src="<?php echo base_url(); ?>public/affilliate_theme/img/icon-loading.png" alt="list-logo"/>Overnight accommodation <span> AND/OR</span></li>
                                <li><img src="<?php echo base_url(); ?>public/affilliate_theme/img/icon-loading.png" alt="list-logo"/>Alternative transportation <span> AND/OR</span></li>
                                <li><img src="<?php echo base_url(); ?>public/affilliate_theme/img/icon-loading.png" alt="list-logo"/>Meal and Beverage compensation</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-sm-12">
                <div class="heading SPORTING">
                    <span>SOLD-OUT </span> CONCERTS OR SPORTING EVENTS?
                    <div class="content">
                        <div class="sold-out">
                            <span>Access select events with no official availability!</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-sm-12">
		        <div class="heading SPORTING">
                    <span>DISCOUNTED</span> ACTIVITIES?
                    <div class="content">
                        <div class="loading-list">
                            <ul class="whl-rates">
                                <li><img src="http://hotelsdifferently.loc/public/affilliate_theme/img/icon-loading.png" alt="list-logo">Get your sightseeing and selected shows for a discount!</li>
                                <li><img src="http://hotelsdifferently.loc/public/affilliate_theme/img/icon-loading.png" alt="list-logo">DELETE <strong>“COMING SOON: WHOLESALE tours / activities, car rentals, and cruises!”</strong></li>
                            </ul>
                        </div>
                    </div>
                    <div class="clear-fix"></div>
                </div>
                
                
                <div class="heading SPORTING">
                    <span>VERSION </span> #1:
                    <div class="content">
                        <div class="loading-list">
                            <ul class="whl-rates">
                                <li><img src="http://hotelsdifferently.loc/public/affilliate_theme/img/icon-loading.png" alt="list-logo">Get your sightseeing and selected shows for a discount!</li>
                                <li><img src="http://hotelsdifferently.loc/public/affilliate_theme/img/icon-loading.png" alt="list-logo">DELETE <strong>“COMING SOON: WHOLESALE tours / activities, car rentals, and cruises!”</strong></li>
                            </ul>
                        </div>
                    </div>
                    <div class="clear-fix"></div>
                </div>
                
                
                
                
                
                <?php /*?><div class="coming-soon">
                    <span>COMING SOON: WHOLESALE tours / activities, car rentals, and cruises!</span>
                    <h1>GET YOUR MEMBERSHIP NOW FOR FREE ! <span>(LIMITED TIME OFFER)</span></h1>
                    <span class="btn-block"><a href="<?php echo base_url(); ?>front/home/member-signup<?php echo (!empty($hear_about_us)  ? '?hear_about_us='.$hear_about_us : '');?>" class="btn btn-default unq-hover">SIGN UP NOW!</a></span>
                    <span>* Only on select flights. This coverage is subject to the terms and conditions and policies of our flight aggregate supplier.</span>
                </div><?php */?>
		    </div>
        </div>
        <?php echo $this->load->view('front/includes/body_footer'); ?>
        <?php echo $this->load->view('front/includes/footer'); ?>
    </body>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.js"></script>
</html>