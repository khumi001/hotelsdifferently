<div class="invoice-holder">
	<strong class="invoice-logo">
		<img src="http://tradehub.pk/projects/hotelsdifferently/public/affilliate_theme/img/logo.png">
	</strong>
	<div class="overflow">
		<strong class="invoice-by left-align">Invoice issued by:</strong>
		<strong class="company-name right-align"><?php echo SITE_NAME;?></strong>
	</div>
	<div class="overflow">
		<strong class="tax-id left-align">TAX ID: 45-4230568</strong>
		<address class="invoice-address right-align">
			3651 Lindell Road D141<br/>
			Las Vegas, Nevada, 89103<br/>
			United States of America
		</address>
	</div>
	
	<div class="overflow">
		<span class="left-align">[SCRIPT – Hotel name]</span>
		<span class="right-align">[SCRIPT – PAYMENT DATE]</span>
	</div>
	<div class="overflow">
		<span class="left-align">[SCRIPT – Hotel address]</span>
		<span class="right-align">[SCRIPT – RESERVATION NUMBER]</span>
	</div>
	<div class="overflow">
		<span class="left-align">[SCRIPT – Hotel phone number]</span>
		<span class="right-align">[SCRIPT – CONFIRMATION NUMBER]</span>
	</div>
	
	<h2>Reservation Details:</h2>
	<table class="detail-table">
		<tr>
			<th>Check-in:</th>
			<th>Check-out:</th>
			<th>RoomType:</th>
			<th>Adults:</th>
			<th>Children:</th>
		</tr>
		<tr>
			<td>[SCRIPT]</td>
			<td>[SCRIPT]</td>
			<td>[SCRIPT]</td>
			<td>[SCRIPT]</td>
			<td>[SCRIPT]</td>
		</tr>
	</table>
	<div class="overflow">
		<strong>Comments:</strong>[SCRIPT – Admin comments] + text below:
		<p>If your check-in is in less than 72 hours, then your reservation is sent to the hotel immediately but every hotel has a different ways and timeframes of processing incoming reservations and hotels usually prioritize incoming bookings by arrival date. Therefore if your check-in date is for a future date, your reservation might not show up in their system for 5 days from your booking. </p>
	</div>
	<h3>PaymentInformation: </h3>
	<div class="overflow">
		<strong class="left-align">Room Rate: </strong>[SCRIPT – Total minus coupon minus taxes]
	</div>
	<div class="overflow">
		<strong class="left-align">Taxes: </strong>[SCRIPT – Total minus coupon minus room rate]
	</div>
	<div class="overflow">
		<strong class="left-align">Coupon: </strong>[SCRIPT – Coupon amount (if coupon was used, if not leave empty]
	</div>
	<div class="overflow txt-red m-20">
		<strong class="left-align">TOTAL PAID: </strong>[SCRIPT – Total amount charged]
	</div>
	<div class="overflow m-20">
		<strong class="left-align">Name on reservation: </strong>[SCRIPT]
	</div>
	<div class="overflow m-20">
		<strong class="left-align">Special request by you (not guaranteed): </strong>[SCRIPT]
	</div>
	<div class="overflow m-20">
		<strong class="left-align">Hotel’s message: </strong>[SCRIPT]
	</div>
	<div class="overflow m-20">
		<strong class="left-align">Cancellation Policy: </strong>[SCRIPT]
	</div>
	<h3>Problems? NO PROBLEM!</h3>
	<p>Should you encounter any problems with the hotel, please send us a message immediately via the "Contact Us" page and select "Problems with booking" so that we can rectify any problems you may have experienced or call us at 888-885-5259. Those emails enjoy high priority and you can expect a very fast reply.</p>
	<div class="overflow m-20">
		[SCRIPT - INSERT MAP HERE!]
	</div>
	<div class="overflow m-20">
		[SCRIPT - INSERT DRIVING DIRECTIONS HERE!]
	</div>
	<a href="#" class="button-send">Send</a>
</div>