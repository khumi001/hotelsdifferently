<script src="<?php echo base_url(); ?>public/assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
<div class="width-row">
    <div class="clearfix">
        <div class="col-sm-4">

            <div class="wizWized">
                <div class="sWhead">
                    <h4>Reservation Details </h4>
                </div>
                <div class="clearfix">
                    <div class="media" style="padding:0px 15px;">
                        <a class="pull-left" href="#">
                            <img style="width:100px; margin-bottom: 0px;" class="media-object img-responsive thumbnail" src="<?php echo $hotel_list[0]['hotelThumbUrl'] ?>" alt="image">
                        </a>
                        <div class="media-body">
                            <div class="panel-title">
                                <b><?php echo $hotel_list[0]['hotelName'] ?></b>
                            </div>
                            <input type="text" name="rating" id="inputRating" class="srRating hidden" value="<?php echo $hotel_list[0]['star_level'] ?>">
                            <address class="hotel_location"></address>
                        </div>
                    </div>
                    <div class="bookingContent">
                        <ul class="list-unstyled">
                            <li role="separator" class="divider"></li>
                            <li><b>Rooms: </b><span class="number_of_rooms">1</span></li>
                            <li><b>Room Type: </b><span class="room_type_category"></span></li>
                            <li><b>Adult(s)/Child(ren): </b><span class="num_adult_children"></span></li>
                            <li role="separator" class="divider"></li>
                            <li><b>Check In: </b><span><?php echo $check_in ?></span></li>
                            <li><b>Check Out: </b><span><?php echo $check_out ?></span></li>
                            <li role="separator" class="divider"></li>
                            <li class="text"><b>Room Cost:</b><span id="total_room_cost"><?php echo $total_room_cost ?></span></li>
                            <li class="text"><b>Taxes/Fees:</b><span id="total_tax_fees"><?php echo $total_tax_fees ?></span></li>
							<?php
								if(isset($usedCoupon))
								{
									if(count($usedCoupon) > 0)
									{
										?>
										<li id="lastcouponli"><b>Coupon:</b><span>-<?php echo $usedCoupon->var_couponvalue; ?></span></li>
										<?php
									}
								}
							?>
                            <li id="couponli" style="display:none;"><b>Coupon:</b><span id="coupon_val"></span></li>
                            <li role="separator" class="divider"></li>
                            <li class="text-danger"><b>Total:</b><span id="total_price_html"><?php echo $total_price_html ?></span></li>
							<li role="separator" class="divider"></li>
							<li id="coupounID">
								<?php 
								if($page_step == 1)
								{
									if($input_data_book_step1['avalibilty'] == 'Available')
									{
										if(count($coupons) > 0)
										{
											echo '<br><button id="applyCpoBtn" onclick="applyCoupon();">Apply</button><button style="display:none;" id="cancelCpoBtn" onclick="cancelCoupon();">Remove</button><div id="showallcouponDiv"><br><b>Coupons:</b><br>';
											
											foreach($coupons as $coupon)
											{
												if($coupon['int_glcode'] == $fcoupon)
												{
													if($page_step != 1)
													{
													?>
														<script type="text/javascript">
															$('form').append('<input type="hidden" value="<?php echo $coupon['int_glcode']; ?>" name="coupons" id="form_coupon" />');
															$('#couponli').show();
															$('#coupon_val').text("-$"+"<?php echo $coupon['var_couponvalue']; ?>");
														</script>
													<?php
													}
													?>
													<div class="radio">
													  <label class="coupon"><input type="radio" name="coupon" ht-ctype="<?php echo $coupon['var_coupan_type']; ?>" ht-price="<?php echo $coupon['var_couponvalue']; ?>" min-amount="<?php echo $coupon['min_amount']; ?>" value="<?php echo $coupon['int_glcode']; ?>" checked="true" >
														<?php 
															if($coupon['var_coupan_type'] == 1)
															{
																echo $coupon['var_couponvalue']."%";
															}
															else
															{
																echo "$".$coupon['var_couponvalue'];
															}
														?>
													  </label>
													</div>
													<br>
													<?php
												}
												else
												{
													?>
													<div class="radio">
													  <label class="coupon"><input type="radio" name="coupon"  ht-ctype="<?php echo $coupon['var_coupan_type']; ?>" ht-price="<?php echo $coupon['var_couponvalue']; ?>" min-amount="<?php echo $coupon['min_amount']; ?>" value="<?php echo $coupon['int_glcode']; ?>"  >
														<?php 
															if($coupon['var_coupan_type'] == 1)
															{
																echo $coupon['var_couponvalue']."%";
															}
															else
															{
																echo "$".$coupon['var_couponvalue'];
															}
														?>
													  </label>
													</div>
													<br>
													<?php
												}
											}
											
											echo '<br><span ><b>Missing a coupon?</b> You can only see the coupons that meet your minimum spending requirement for this reservation.<br>';
											
											echo "</div>";
										}
									}
									else
									{
										?>
											Coupons cannot be used for <b>On Request</b> reservations. Thank you for your understanding!”
										<?php
									}
									
								}
								else
								{
									foreach($coupons as $coupon)
									{
										if($coupon['int_glcode'] == $fcoupon)
										{
											if($page_step != 1)
											{
											?>
											<script type="text/javascript">
												$('form').append('<input type="hidden" value="<?php echo $coupon['int_glcode']; ?>" name="coupons" id="form_coupon" />');
												$('#couponli').show();
												<?php
													if($coupon['var_coupan_type'] == 1)
													{
														?>
															$('#coupon_val').text("-$"+"<?php echo $input_data_book_step2['total_parcentage_amount']; ?>");
														<?php
													}
													else
													{
														?>
															$('#coupon_val').text("-$"+"<?php echo $coupon['var_couponvalue']; ?>");
														<?php
													}
												?>
											</script>
											<?php
											}
										}
									}
								}
								?>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="row">
                <div role="tabpanel" class="wizard" data-initialize="wizard" id="">
					<?php
						if(!isset($success))
						{
							?>
                    <!-- Nav tabs -->
                    <div class="steps-container">
                        <ul class="nav nav-tabs nav-justified nav-pills wizTH  steps" role="tablist">
                            
                            <li role="presentation" <?php if($page_step == 1)echo 'class="active"' ?> data-step="1" data-name="ti">

                                <a href="<?php echo base_url() ?>request_quote?stp=f" aria-controls="ti">Traveler Information</a>
                            </li>
                            <li role="presentation" <?php if($page_step == 2)echo 'class="active"'?> data-step="2" data-name="bi">
                                <a href="#bi/" aria-controls="bi" role="tab" data-toggle="tab">Billing Information</a>
                            </li>
                            <li role="presentation" <?php if($page_step == 3)echo 'class="active"' ?> data-step="3" data-name="rc">
                                <a href="#rc/" aria-controls="rc" role="tab" data-toggle="tab">Review & Confirm</a>
                            </li>
                        </ul>
                    </div>

                    <!-- Tab panes -->
					<?php
						}
					?>
                    <div class="tab-content wizTab " id="traveller_information_body">  <!-- step-content -->
                        <?php if($page_step == 1){ ?>
                        <div role="tabpanel" class="tab-pane active clearfix" id="ti" > 
                            <h4>Traveler Information</h4>
							<?php 
								if(isset($_SESSION['error'])) 
								{
									?>
									<div class="alert alert-danger">
									<?php 
										echo $_SESSION['error'];
										unset($_SESSION['error']);
									?>
									</div>
									<?php
								}
							?>
                            <p>Please provide one main contact for each room.</p> 
                            <div class="tabpanel_item"> 
                                <form method="post" action="<?php echo base_url() ?>request_quote?stp=s" id="quote_request" class="form-horizontal form-bordered">
                                    <input type="hidden" value="" name="data[total_room_cost]" class="total_room_cost">
                                    <input type="hidden" value="" name="data[total_room_acost]" class="total_room_acost">
                                    <input type="hidden" value="" name="data[total_tax_fees]" class="total_tax_fees">
                                    <input type="hidden" value="" name="data[total_price_html]" class="total_price_html">
                                    <input type="hidden" value="" name="data[total_parcentage_amount]" class="total_parcentage_amount">
                                    <input type="hidden" value="" name="data[room_pref_string]" class="room_pref_string">
                                    <span id="room_booking_data">
                                      
                                    </span>
                                    <div class="clearfix mt20 actions">
                                        <button type="submit" class="btn btn-primary pull-right">Continue</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <?php }elseif($page_step == 2){?>
                        <div role="tabpanel" class="tab-pane active clearfix" id="bi" data-step="2">
                            <h4>Billing Information</h4>
							<div class="row">
								<?php 
									if(isset($error)) 
									{
										?>
										<div class="alert alert-danger">
										  <?php echo $error; ?>
										</div>
										<?php
									}
								?>
							</div>
                            <form method="post" action="<?php echo base_url() ?>request_quote?stp=th" id="quote_request" class="form-horizontal form-bordered">
                                <div class="form-group">
                                    <label class="control-label col-sm-3"> First Name:</label>
                                    <div  class="col-sm-9">
                                        <input type='text' name='data[firstname]' class="form-control input-medium" id='' value='<?php echo (isset($input_data_book_step2['roomInfo'][1]['firstname'])) ? $input_data_book_step2['roomInfo'][1]['firstname'] : ''; ?>' required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3"> Last Name:</label>
                                    <div  class="col-sm-9">
                                        <input type='text' name='data[lastname]' class="form-control input-medium" id='' value='<?php echo (isset($input_data_book_step2['roomInfo'][1]['lastname'])) ? $input_data_book_step2['roomInfo'][1]['lastname'] : ''; ?>' required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3"> Phone:</label>
                                    <div  class="col-sm-9">
                                        <input type='text' name='data[phone]' class="form-control input-medium" id='' value='<?php echo (isset($input_data_book_step2['roomInfo'][1]['phone'])) ? $input_data_book_step2['roomInfo'][1]['phone'] : ''; ?>' required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3">Country:</label>
                                    <div  class="col-sm-9">
                                        <select class="select2me form-control input-xlarge" id="country" name="data[country]" id="" required>
											<?php 
												foreach($countries as $contr)
												{
													if($contr->id == 192)
													{
														?>
															<option value="<?php echo $contr->id; ?>" selected><?php echo $contr->name; ?></option>
														<?php

													}
													else
													{
														?>
															<option value="<?php echo $contr->id; ?>"><?php echo $contr->name; ?></option>
														<?php
													}
													
												}
											?>
                                            
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3">State:</label>
                                    <div  class="col-sm-9">

                                        <select id="state" class="select2me form-control input-xlarge" name="data[state]" id="" required>
                                           
										   <?php 
												echo $states;
											?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3">Zip:</label>
                                    <div  class="col-sm-9">
                                        <input type='text' name='data[zip]' class="form-control input-medium" id='' value='' required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3">City:</label>
                                    <div  class="col-sm-9">
                                        <input type='text' name='data[city]' class="form-control input-medium" id='' value='' required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3">Street:</label>
                                    <div  class="col-sm-9">
                                        <input type='text' name='data[street]' class="form-control input-medium" id='' value='' required>
                                    </div>
                                </div>
                                <h4>Payment Method</h4>
                                <div class="payment">
                                    <span class="fa fa-cc-mastercard"></span>
                                    <span class="fa fa-cc-visa"></span>
                                    <span class="fa fa-cc-amex"></span>
                                    <span class="fa fa-cc-jcb"></span>
                                    <span class="fa fa-cc-discover"></span>
                                </div>
                                <span>Your information is safe and secure</span>
                                <div class="form-group">
                                    <label class="control-label col-sm-3">Card Number:</label>
                                    <div  class="col-sm-9">
                                        <input type='text' name='data[cardNumber]' class="form-control input-medium" id='' value='' required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3">Expiration:</label>
                                    <div  class="col-sm-9">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <select class="select2me form-control input-xlarge" name="data[month]" id="" required>
                                                    <option class="" value="-1" selected="selected">month</option>
                                                    <option class="" value="01">January</option>
                                                    <option class="" value="02">February</option>
                                                    <option class="" value="03">March</option>
                                                    <option class="" value="04">April</option>
                                                    <option class="" value="05">May</option>
                                                    <option class="" value="06">June</option>
                                                    <option class="" value="07">July</option>
                                                    <option class="" value="08">August</option>
                                                    <option class="" value="09">September</option>
                                                    <option class="" value="10">October</option>
                                                    <option class="" value="11">November</option>
                                                    <option class="" value="12">December</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-6">
                                                <select class="select2me form-control input-xlarge" name="data[year]" id="" required>
                                                    <option class="" value="-1" selected="selected">year</option>
                                                    <option class="" value="2016">2016</option>
                                                    <option class="" value="2017">2017</option>
                                                    <option class="" value="2018">2018</option>
                                                    <option class="" value="2019">2019</option>
                                                    <option class="" value="2020">2020</option>
                                                    <option class="" value="2021">2021</option>
                                                    <option class="" value="2022">2022</option>
                                                    <option class="" value="2023">2023</option>
                                                    <option class="" value="2024">2024</option>
                                                    <option class="" value="2025">2025</option>
                                                    <option class="" value="2026">2026</option>
                                                    <option class="" value="2027">2027</option>
                                                    <option class="" value="2028">2028</option>
                                                    <option class="" value="2029">2029</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3">Security Code:</label>
                                    <div  class="col-sm-9">
                                        <input type='text' name='data[security_code]' class="form-control input-medium" id='' value='' required>
                                    </div>
                                </div>
                                <div class="clearfix mt20">
                                    <button type="submit" class="btn btn-primary pull-right">Continue</button>
                                </div>
                            </form>
                        </div>
                        <?php }
                        elseif($page_step == 3){ ?>
                        <div role="tabpanel" class="tab-pane active clearfix" id="rc" data-step="3">
                            <h4>Review & Confirmation</h4>
							<div class="row">
								<?php 
									if(isset($error)) 
									{
										?>
										<div class="alert alert-danger">
										  <?php echo $error; ?>
										</div>
										<?php
									}
								?>
							</div>
                            <div class="media" style="padding:0px 15px;">
                                <a class="pull-left" href="#">
                                    <img style="width:100px; margin-bottom: 0px;" class="media-object img-responsive thumbnail" src="<?php echo $hotel_list[0]['hotelThumbUrl'] ?>" alt="image">
                                </a>
                                <div class="media-body">
                                    <div class="panel-title">
                                        <?php echo $hotel_list[0]['hotelName'] ?>
                                    </div>
                                    <p class="hotel_location">5625 West Flamingo Road Las Vegas Nevada 89103</p>
<!--                                    <p>+17022513435</p>-->
                                </div>
                            </div>
                            
                            <div class="bookingContent">
                                <ul class="list-unstyled">
                                    <li role="separator" class="divider"></li>
                                    <li>Rooms: <span class="number_of_rooms">1</span></li>
                                    <li>Room Type: <span class="room_type_category">Luxury-1 King Suite</span></li>
                                    <li role="separator" class="divider"></li>
                                    <li>Check In: <span><?php echo $check_in ?></span></li>
                                    <li>Check Out: <span><?php echo $check_out ?></span></li>
                                    <li>Contact Name:<span><?php echo ucfirst($step2_data[1]['firstname']).' '.$step2_data[1]['lastname'] ?></span></li>
                                    <li>Adult(s)/Child(ren): <span class="num_adult_children">2/0</span></li>
<!--                                    <li>Bed Type: <span>None</span></li>-->
                                    <li role="separator" class="divider"></li>
                                    <!-- <li>Total:<span>   $98.99</span></li> -->
                                    <li><strong>Supplements</strong></li>
                                    <li>Supplements paid by customer directly to the hotel</li>
                                    <?php if(!empty($step1_data['supp']))
                                            foreach($step1_data['supp'] as $supp){
                                              echo '<li>'.$supp['suppName'].':  <span>$'.$supp['publishPrice'].'/Night</span></li>';
                                            }
                                          else
                                             echo '<li>Resort Fee:  <span>$30.00/Night</span></li>';
                                    ?>
                                    
                                    <li><small>This amount does not include local taxes and should be paid directly to the hotel upon check out</small></li>
                                    <li role="separator" class="divider"></li>
                                    <li><strong>Total</strong><span><strong><?php echo $step2_data['total_price_html'] ?></strong></span></li>
                                </ul>
                                <div class="divider"></div>
                                <h4>Cancellation Policy</h4>
                                <?php if(!empty($cancellation_policies_list))
                                      foreach($cancellation_policies_list as $cancellation_policy){
                                        echo '<p>'.$cancellation_policy['message'].'</p>';
                                      }  
                                      else
                                          echo ' <p>Please note you are within cancellation penalty of 1 night/s fee
                                    No-show is subjected to 1 night/s fee</p>';
                                 ?>
                                <div class="divider"></div>
                                <h4>Summary of Billing Information</h4>
                                <ul class="list-unstyled">
                                    <li class="text-danger">Price:<span><strong><?php echo $step2_data['total_price_html'] ?></strong></span></li>
<!--                                    <li>Your credit card will be charged on the following date:<span><strong>Dec 23, 2015</strong></span></li>-->
                                    <li role="separator" class="divider"></li>
                                    <li>Name:<span> <?php echo ucfirst($card_data['firstname']).' '.$card_data['lastname'] ?></span></li>
<!--                                    <li>Credit Card:<span> Mastercard</span></li>-->
                                    <li>Credit Card Number:<span>  <?php echo "**** **** **** ".substr($card_data['cardNumber'], -4); ?></span></li>
                                    <li>Expiration:<span>  <?php echo $card_data['month'].', '.$card_data['year']?></span></li>
                                    <li>Address:<span> <?php echo $card_data['street'].', '.$card_data['city'] .' '.$card_data['country']?></span></li>
                                    <li>Phone:<span>   <?php echo $card_data['phone'] ?></span></li>
                                </ul>
                                <div class="divider"></div>
                                <iframe width="100%" style="border:1px solid #E5E5E5;" src="terms.html"></iframe>
                                <iframe width="100%" style="border:1px solid #E5E5E5;" src="terms1.html"></iframe>
                                <form method="post" action="<?php echo base_url() ?>request_quote?stp=cnfrm" id="quote_request" class="form-horizontal form-bordered">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" value="1" name="terms" required>
                                            I have read and agree with the Terms and Conditions as well as the FTC disclosure. 
                                        </label>
                                    </div>
                                    <div class="clearfix mt20">
                                        <button type="submit" class="btn btn-primary pull-right">Finish</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <?php }elseif($page_step == 4){ ?>
                        <div role="tabpanel" class="tab-pane active clearfix" id="rc" data-step="3">
							<?php
								if(isset($success))
								{
									?>
										<div class="alert alert-success">
										  Good job! Your reservation has been successfully made and we forwarded it to the hotel.
										</div>
									<?php 
										if($input_data_book_step1['avalibilty'] == 'Available')
										{
											?>
                                            <p>
                                                Please note that once you paid for your reservation, it is immediately confirmed and your room is guaranteed (with the exception of ON REQUEST bookings). However, every hotel processes and inputs incoming reservations differently, therefore it may happen that you contact the hotel and they may not see your booking until closer to your arrival. <b>Rest assured that your room is secured</b> the moment your reservation processes and a <b>HotelsDifferently.com confirmation number</b> is issued. Should you have any questions or concerns please do not hesitate to let us know via our Contact Us menu or the toll-free number that is listed on your reservation confirmation.
                                            </p>
												<br>
												<p>Should you encounter any problems with this reservation, please send us a message immediately via the <b>"Contact Us"</b> page and select <b>"Problems with booking"</b> so that we can rectify any problems you may have experienced. Those emails enjoy high priority and you can expect a very fast reply. Alternatively, you are also welcome to call us at <b>888-885-5259</b> and our customer service representatives will be happy to assist you <b>24 hours a day, 7 days a week, 365 days a year.</b></p>
												
												
											<?php
										}
										else
										{
											?>
												<h2 style="font-size:22px;">Your reservation can not be confirmed at this time.</h2>
												<p>Due to this amazing deal you are trying to secure, the allotment of rooms at this hotel have been sold out; however; our team of dedicated professionals negotiating with the hotel on your behalf to secure additional rooms out our advertised rate. Upon booking this reservation, you will receive a reservation number for status identification purposes only.</p>
												<p>Within the next 48 business hours the hotel will reply with a definite confirmation or decline of this reservation.</p>
								<p style="color: red;">If your reservation date is within 3 days, we highly encourage you to perform a follow-up message immediately via our CONTACT US menu and manually request an expedited review of the On Request booking because we would need to contact our source immediately in order to get a speedy reply of confirmation or decline of your booking.</p>				
												<p>Once the request is confirmed by the hotel, you will receive an email with notification of the confirmation. Should the request be declined by the hotel, you will receive an email with notification of the decline and a full refund. Please allow 5-7 business days for the refund to appear in your account.</p>
												
												<p>Please call us at <b>888-885-5259</b> to check the status of your reservation <b>if you do not receive a response after 48 hours <font style="text-decoration:underline;">OR</font> if your requested check in is within 24 hours.</b>We will be happy to assist you with your reservation.</p>
												
												<p><b>Please contact us prior to making alternate arrangements as open requests may confirm at any time up to the check in date.</b></p>
											<?php
										}
									?>
									<?php
								}
							?>
							
							<p><b style="font-size:18px; display:block; text-align:center;">NEED INSURANCE?</b></p>
												<p>We (<strong>HotelsDifferently</strong><sup>sm</sup>) do NOT offer travel insurance, however you can purchase travel insurance from a third party insurance company. Please note that we do not directly sell any types of insurance; we are solely an Affiliate to a third party insurance company.</p>
							
							<a style="display:inline-block; vertical-align:top; max-width:200px;" href="http://www.dpbolvw.net/click-8137399-10892804-1466615700000" target="_blank">
								<img style="display:block; width:100%; height:auto;" src="http://www.ftjcfx.com/image-8137399-10892804-1466615700000" width="125" height="125" alt="Allianz Travel Insurance" border="0"/>
							</a>
							<a style="display:inline-block; vertical-align:top; max-width:200px;" href="http://www.jdoqocy.com/click-8137399-11779657-1466617745000" target="_blank">
								<img style="display:block; width:100%; height:auto;" src="http://www.lduhtrp.net/image-8137399-11779657-1466617745000" width="300" height="250" alt="Allianz Travel Insurance" border="0"/>
							</a>
							<a style="display:inline-block; vertical-align:top; max-width:200px;" href="http://www.tkqlhce.com/click-8137399-11176385-1466616611000" target="_blank">
								<img style="display:block; width:100%; height:auto;" src="http://www.tqlkg.com/image-8137399-11176385-1466616611000" width="300" height="250" alt="" border="0"/>
							</a>
							<div class="media" style="padding:0px 15px; text-align:center;">
                                <strong>Download your invoice here:</strong>
                            </div>
							
							<div class="text-center" style="text-align:center;">
								<a  target="_blank" href="<?php echo base_url(); ?>front/quote_request/pdf/<?php echo $res_id; ?>"><img src="http://iconbug.com/data/5b/507/52ff0e80b07d28b590bbc4b30befde52.png" width="50" height="50" /></a>
							</div>
							
							
                            
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url(); ?>public/affilliate_theme/plugins/select2/select2.min.js" type="text/javascript"></script>

<script type="text/javascript">
	var i = 0;
	var totvalue = "0";
	var applycopon = 0;
	var chkcouon = 0;
	function applyCoupon()
	{
		counti = 0;
		$('.coupon').each(function(){
			if($(this).children().is(':checked') == true)
			{
				counti++;
			}
		});
		if(counti > 0)
		{
			$('#applyCpoBtn').hide();
			$('#cancelCpoBtn').show();
			applycopon == 1;
			applyCouponprice();
			$('#showallcouponDiv').hide();
		}
	}
	
	function cancelCoupon()
	{
		$('#showallcouponDiv').show();
		$('#form_coupon').remove();
		$('.coupon').each(function(){
			if($(this).children().is(':checked') == true)
			{
				$(this).children().attr('checked' , false);
			}
		});
		$('#applyCpoBtn').show();
		$('#cancelCpoBtn').hide();
		$('#couponli').hide();
		applyCouponprice();
		applycopon == 0;
	}
	
	function applyCouponprice()
	{
		<?php if($page_step == 1){ ?>
			showTotalPrice();
		<?php }else{
			?>
			return false;
			var coupon = 0;
			$('.coupon').each(function(){
				if($(this).children().is(':checked') == true)
				{
					$('#form_coupon').remove();
					$('form').append('<input type="hidden" value="'+$(this).children().val()+'" name="coupons" id="form_coupon" />');
					ctype = $(this).children().attr('ht-ctype');
					if(ctype == 2)
					{
						price = $(this).children().attr('ht-price');
					}
					else
					{
						price = $(this).children().attr('ht-price');
						price = (price * total_room_cost) / 100;
						price = price.toFixed(2);
						$('.total_parcentage_amount').val(price);
					}
					price = parseFloat(price);
					coupon = price;
					$('#couponli').show();
					$('#coupon_val').text("-$"+price);	
				}
			});
			total_price = total_price - coupon;
			$("#total_price_html").html(total_price);
			<?php
		}?>
	}
</script>
<script>
    function showTotalPrice()
    {
        total_price = 0;
        total_tax = 0;
        $("input:checked").each(function () 
		{
			if($(this).attr("class") == 'bedding_chkboxes')
			{
				var price = $(this).attr("price");
				total_price += parseFloat(price);
				var tax = $(this).attr("total_tax");
				total_tax += parseFloat(tax);
			}
        });
		allStr = "";
		$("input:checked").each(function () 
		{
			if($(this).attr("class") == 'bedding_chkboxes')
			{
				allStr += $(this).attr("rom_pref_string")+'@';
			}
		});
		$('.room_pref_string').val(allStr);
		total_room_cost = total_price - total_tax;
		<?php if($page_step == 1){ ?>
		var coupon = 0;
		if(chkcouon == 1)
		{
			$('.coupon').each(function(){
				if($(this).children().is(':checked') == true)
				{
					$('#form_coupon').remove();
					$('form').append('<input type="hidden" value="'+$(this).children().val()+'" name="coupons" id="form_coupon" />');
					ctype = $(this).children().attr('ht-ctype');
					if(ctype == 2)
					{
						price = $(this).children().attr('ht-price');
					}
					else
					{
						price = $(this).children().attr('ht-price');
						price = (price * total_room_cost) / 100;
						price = price.toFixed(2);
						$('.total_parcentage_amount').val(price);
					}
					coupon = price = parseFloat(price);
					$('#couponli').show();
					$('#coupon_val').text("$"+price);	
				}
			});
		}
		var counttotCop = 0;
		$('.coupon').each(function()
		{
			minamont = $(this).children().attr('min-amount');
			if(total_price >= parseFloat(minamont))
			{
				counttotCop++;
			}
			else
			{
				$(this).remove();
			}
		});
		if(counttotCop > 0)
		{
			$('#coupounID').show();
		}
		alretotal_room_cost = total_room_cost;
		total_room_cost = total_room_cost - coupon;
		if(total_room_cost < 0 )
		{
			coupon = alretotal_room_cost;
			total_room_cost = 0;
		}
		total_price = total_price - coupon;
		<?php } ?>
        total_room_cost = total_room_cost.toFixed(2);
        total_price = total_price.toFixed(2);
        total_tax = total_tax.toFixed(2);
        total_room_cost = "$" + total_room_cost;
        total_price = "$" + total_price;
        total_tax = "$" + total_tax;
        if(total_price != '$0.00')
		{
            $("#total_price_html").html(total_price);
			alretotal_room_cost = alretotal_room_cost.toFixed(2);
			alretotal_room_cost = "$" + alretotal_room_cost;	
            $("#total_room_cost").html(alretotal_room_cost);
            $(".total_room_acost").val(alretotal_room_cost);
            $("#total_tax_fees").html(total_tax);
             $(".total_price_html").val(total_price);
			 totvalue = total_price;
            $(".total_room_cost").val(total_room_cost);
            $(".total_tax_fees").val(total_tax);
        }
        
    }

    function initialiseStarRating()
    {
        $(document).ready(
                function ()
                {
                    $(".srRating").rating({
                        disabled: true,
                        size: 'xs',
                        showClear: false,
                        hoverEnabled: false,
                        starCaptions: {
                            1: '1 Star Hotel',
                            2: '2 Star Hotel',
                            3: '3 Star Hotel',
                            4: '4 Star Hotel',
                            5: '5 Star Hotel'
                        },
                        starCaptionClasses: function (val) {
                            if (val == 0) 
							{
                                return 'capText';
                            } 
							else if (val < 3) 
							{
                                return 'capText';
                            } 
							else 
							{
                                return 'capText';
                            }
                        }
                    });
                }
        );
    }
</script>

<script>
	$('#country').change(function(){
		$('#state').html('');
		$.post("<?php echo base_url(); ?>front/tourico_api/get_cities/"+$(this).val(),
		{
			contryid: $(this).val()
		},
		function(data, status){
			$('#state').html(data);
		});
	});
    var hotel_list = <?php echo json_encode($hotel_list) ?>;
    var hotel_id = <?php echo $hotel_id ?>;
    var room_id = <?php echo $room_id ?>;
    var room_type_id = <?php echo $room_type_id ?>;
    var number_of_rooms = "<?php echo $number_of_rooms ?>";
    var adult_array = <?php echo json_encode($adult_array) ?>;
    var child_array = <?php echo json_encode($child_array) ?>;
    var supplements = <?php echo json_encode($supp) ?>;
    var previosForm = <?php echo json_encode($input_data_book_step2) ?>;
    $(document).ready(
            function ()
            {
                total_adult = 0;
                for (i = 0; i < adult_array.length; i++)
                {
                    total_adult += parseInt(adult_array[i]);
                }

                total_child = 0;
                for (i = 0; i < child_array.length; i++)
                {
                    total_child += parseInt(child_array[i]);
                }
                adult_children_html = total_adult + " / " + total_child;

                $(".num_adult_children").html(adult_children_html);


                number_of_rooms = parseInt(number_of_rooms);
                $(".number_of_rooms").html(number_of_rooms);

                room_preference_table_html = "";
                final_html = "";
				var arr = [];
                for (room_no_index = 1; room_no_index <= number_of_rooms; room_no_index++)
                {
					firstnamep = "";
					lastnamep = "";
					phonep = "";
					emailp = "";
					splReqp = "";
					optradiop = "";
					if(previosForm == null)
					{
						firstnamep = "";
					}
					else
					{
						ar =  previosForm.roomInfo;
						if(ar.hasOwnProperty(room_no_index))
						{
							firstnamep = previosForm.roomInfo[room_no_index].firstname;
							lastnamep = previosForm.roomInfo[room_no_index].lastname;
							phonep = previosForm.roomInfo[room_no_index].phone;
							emailp = previosForm.roomInfo[room_no_index].email;
							splReqp = previosForm.roomInfo[room_no_index].splReq;
							optradiop = previosForm.roomInfo[room_no_index].optradio;
						}
						
					}
                    for (i = 0; i < hotel_list.length; i++)
                    {
                        room_type_list = hotel_list[i]['room_type_list'];
                        hotel_address  = hotel_list[i]['hotel_address'];
                        $(".hotel_location").html(hotel_address);
                        for (j = 0; j < room_type_list.length; j++)
                        {
                            _room_id      = room_type_list[j]['RoomId'];
                            _room_type_id = room_type_list[j]['RoomTypeId'];
                            _supplement = room_type_list[j]['supplemtnts'];
							
                            if ((_room_id == room_id) && (_room_type_id == room_type_id))
                            {								
                                occupancy_lists = room_type_list[j]['occupancy_list'];
                                room_type_category = room_type_list[j]['name'];
                                $(".room_type_category").html(room_type_category);
                                room_preference_table_html = "";
                                for (var prop in occupancy_lists)
                                {
                                    available_list = occupancy_lists[prop]['available_list'];
									var copchk = 0;
									if(optradiop != '')
									{
										for (kk = 0; kk < available_list.length; kk++)
										{	
											total_price = available_list[kk]['total_price'];
											actual_total_price = available_list[kk]['actual_total_price'];
											occpNo = available_list[kk]['occpNo'];
											if(optradiop == total_price + '|'+actual_total_price+'|'+occpNo)
											{
												copchk++;
											}	
										}
									}
									if(copchk == 0)
									{
										optradiop = '';
									}
                                    for (k = 0; k < available_list.length; k++)
                                    {
                                        bedding = available_list[k]['bedding'];
										occpNo = available_list[k]['occpNo'];
                                        total_price = available_list[k]['total_price'];
                                        actual_total_price = available_list[k]['actual_total_price'];
                                        total_tax = available_list[k]['per_night_tax'];
                                        bedding_str = "";
                                        bed_split = bedding.split(",");
                                        bedding_str = bed_split[0] + " people " + bed_split[1] + " bed ";
                                        if(optradiop == '')
										{
											if (k == 0)
											{
												checked_str = "checked";
											} else
											{
												checked_str = "";
											}
										}
										else
										{
											if(optradiop == total_price + '|'+actual_total_price)
											{
												checked_str = "checked";
											}
											else
											{
												checked_str = "";
											}
										}
										
                                        room_preference_table_html += '<tr>' +
                                                '<td>' +
                                                '<input  ' + 'total_tax = "' + total_tax + '"' + checked_str + ' price="' + total_price + '" value="' + total_price + '|'+actual_total_price+'|'+occpNo+'" class="bedding_chkboxes" rom_pref_string="'+bedding_str+'" type="radio" name="data[roomInfo]['+room_no_index+'][optradio]">' + bedding_str + '' +
                                                '</td>' +
                                                '<td>$' + total_price + '</td>' +
                                                //'<td>$' + available_list[k]['per_night_price'] + '</td>' +
                                                '</tr>';
                                    }
                                }
                                var supp_str = '';
                                for(key in supplements)
								{
									supp_str += '<p>'+supplements[key]['suppName']+' - $'+supplements[key]['publishPrice']+'</p>';
									supp_str +=  '<input type="hidden" name="data[roomInfo]['+room_no_index+'][supp_data]" value="'+supplements[key]['suppId']+'|'+supplements[key]['price']+'|'+supplements[key]['supptType']+'">';
                                }
								for(key in _supplement)
								{
									if(_supplement[key]['suppIsMandatory'] == 'true')
									{
										supp_str +=  '<input type="hidden" name="data[roomInfo]['+room_no_index+'][supp_data]['+key+']" value="'+_supplement[key]['suppId']+'|'+_supplement[key]['price']+'|'+_supplement[key]['supptType']+'|'+_supplement[key]['suppChargeType']+'|'+_supplement[key]['SuppAgeGroup']+'">';
									}
                                }
								
								if(room_type_list[j]['isAvailable'] == 'false')
								{
									arr.push('Room ' + room_no_index);
								}
								
                                final_html += '<p><b>' + 'Room  ' + room_no_index + '</b></p>' +
                                            '<div class="form-group">' +
                                            '<label class="control-label col-sm-3"> First Name:</label>' +
                                            '<div  class="col-sm-9">' +
                                            '<input type="text" name="data[roomInfo]['+room_no_index+'][firstname]" class="form-control input-medium" id="" value="'+firstnamep+'" required>' +
                                            '</div>' +
                                            '</div>' +
                                            '<div class="form-group">' +
                                            '<label class="control-label col-sm-3"> Last Name:</label>' +
                                            '<div  class="col-sm-9">' +
                                            '<input type="text" name="data[roomInfo]['+room_no_index+'][lastname]" class="form-control input-medium" id="" value="'+lastnamep+'" required>' +
                                            '</div>' +
                                            '</div>' +
                                            '<div class="form-group">' +
                                            '<label class="control-label col-sm-3"> Phone:</label>' +
                                            '<div  class="col-sm-9">' +
                                            '<input type="text" name="data[roomInfo]['+room_no_index+'][phone]" class="form-control input-medium" id="" value="'+phonep+'" required>' +
                                            '</div>' +
                                            '</div>' +
                                            '<!--<div class="form-group">' +
                                            '<label class="control-label col-sm-3"> Email:</label>' +
                                            '<div  class="col-sm-9">' +
                                            '<input type="email" name="data[roomInfo]['+room_no_index+'][email]" class="form-control input-medium" id="" value="'+emailp+'" required>' +
                                            '</div>' +
                                            '</div>-->' +
                                            '<!--<div class="alert-box" role="alert">' +
                                            '<h4>Supplements paid by customer directly to the hoteldfgdf</h4>' +
                                            supp_str +
                                            '<span class="supp_down_text">This amount does not include local taxes and should be paid directly to the hotel upon check out</span>' +
                                            '</div>-->' +
                                            '<div style="margin-top: 15px;">' +
                                            '<table class="table table-bordered table-responsive">' +
                                            '<thead>' +
                                            '<tr>' +
                                            '<th>Room preferences</th>' +
                                            '<th></th>' +
                                            '</tr>' +
                                            '</thead>' +
                                            '<tbody>' +
                                            '<tr><td colspan="2">Bedding preferences are subject to supplier availability, but we will do our very best to assist with your request.</td></tr>' +
                                            room_preference_table_html;
										if(supp_str != '')
										{
											final_html += '<tr>' +
												'<td colspan="2" style="padding: 15px;">' +
												'<b>Supplements paid by customer directly to the hotel</b>' +
												supp_str +
												'<span>This amount does not include local taxes and should be paid directly to the hotel upon check out</span>' +
												'</td>' +
												'</tr>';
										}
										final_html += '</tbody>' +
                                            '</table>' +
                                            '</div>' +
                                            '<div class="clearfix">' +
                                            '<label class="control-label">Special Requests (not guaranteed)</label>' +
                                            '<textarea type="text" name="data[roomInfo]['+room_no_index+'][splReq]" class="form-control input-medium">'+splReqp+'</textarea>' +
                                            '</div>';
                                //$("#traveller_information_body").html(final_html);
                                $("#room_booking_data").html(final_html);
                                $(".bedding_chkboxes").click(
									function ()
									{
										showTotalPrice();
									}
                                );
                            }
                        }

                    }
                }
				if(arr.length > 0 )
				{
					var final_h = "";
					if(number_of_rooms != arr.length)
					{
						final_h += "<div class='alert alert-warning'>";
						for(var li = 0 ; li < arr.length ; li++)
						{
							if(li == 0)
							{
								final_h += arr[li];
							}
							else
							{
								final_h += " , "+arr[li];
							}
						}
						if(arr.length == 1)
						{
							final_h += " is On Request.</div>";
						}
						else
						{
							final_h += " are On Request.</div>";
						}
						
					}
					final_h += '<p><b> Your reservation can not be confirmed at this time.</b></p>'+
								'<br><p>Due to this amazing deal you are trying to secure, the allotment of rooms at this hotel have been sold out; however; our team of dedicated professionals negotiating with the hotel on your behalf to secure additional rooms out our advertised rate. Upon booking this reservation, you will receive a reservation number for status identification purposes only. </p>'+
								'<br><p>Within the next 48 business hours the hotel will reply with a definite confirmation or decline of this reservation. </p>';
					/*'<br><p>During this process,<b> your credit card will not be charged.</b></p>'+
						'<br><p>Once the request is confirmed by the hotel, you card will be charged and we will send you an email with notification of the confirmation. Should the request be declined by the hotel, you will receive an email with notification of the decline.</p>'+
						'<p><b>Credit cards will only be charged upon receipt of confirmation from the hotel.</b></p>'+
						
						'<br><p>Please call us at <b>888-885-5259</b> to check the status of your reservation <b>if you do not receive a response after 48 hours or if your requested check in is within 24 hours.</b>We will be happy to assist you with your reservation.</p>'+
						'<br><p style="color:red;"><b>Please contact us prior to making alternate arrangements as open requests may confirm at any time up to the check in date. </b></p>'
						*/		
					$("#room_booking_data").prepend( final_h );
					
				}
                showTotalPrice();
            }
    );
    initialiseStarRating();
</script>
<script type="text/javascript">
	$(document).ready(function(){
		chkcouon = 1;
		showTotalPrice();
	});
</script>
<?php 
	if($page_step != 1 || isset($success))
	{
		?>
		<script type="text/javascript">
			$(document).ready(function(){
				$('#couponli').hide();
				$('#lastcouponli').hide();
			});
		</script>
		<?php
	}
?>