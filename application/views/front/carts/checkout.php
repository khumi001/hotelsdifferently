
<div class="width-row">
	<div class="col-md-8">
		<div>
		  	<!-- Nav tabs -->
		  	<ul class="nav nav-tabs" role="tablist">
		    	<li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Travel information</a></li>
		    	<li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Reviews</a></li>
		  	</ul>

		  	<!-- Tab panes -->
		  	<div class="tab-content">
		    	<div role="tabpanel" class="tab-pane active" id="home">
		    		<div class="tab-content wizTab " id="traveller_information_body">  <!-- step-content -->
                        <div role="tabpanel" class="tab-pane active clearfix" id="ti"> 
                            <h4>Traveler Information</h4>
							<p>Please provide one main contact for each room.</p> 
                            <div class="tabpanel_item"> 
                                <form method="post" action="http://beta.hotelsdifferently.com/request_quote?stp=s" id="quote_request" class="form-horizontal form-bordered">
                                    <input value="$186.89" name="data[total_room_cost]" class="total_room_cost" type="hidden">
                                    <input value="$186.89" name="data[total_room_acost]" class="total_room_acost" type="hidden">
                                    <input value="$0.00" name="data[total_supp_amount]" class="total_supp_amount" type="hidden">
                                    <input value="$22.41" name="data[total_tax_fees]" class="total_tax_fees" type="hidden">
                                    <input value="$209.30" name="data[total_price_html]" class="total_price_html" type="hidden">
                                    <input value="" name="data[total_parcentage_amount]" class="total_parcentage_amount" type="hidden">
                                    <input value="2 people 1 bed @" name="data[room_pref_string]" class="room_pref_string" type="hidden">
                                    <span id="room_booking_data"><p><b>Room  1</b></p><div class="form-group"><label class="control-label col-sm-3"> First Name:</label><div class="col-sm-9"><input name="data[roomInfo][1][firstname]" class="form-control input-medium" id="" value="" required="" type="text"></div></div><div class="form-group"><label class="control-label col-sm-3"> Last Name:</label><div class="col-sm-9"><input name="data[roomInfo][1][lastname]" class="form-control input-medium" id="" value="" required="" type="text"></div></div><div class="form-group"><label class="control-label col-sm-3"> Phone:</label><div class="col-sm-9"><input name="data[roomInfo][1][phone]" class="form-control input-medium" id="" value="" required="" type="text"></div></div><!--<div class="form-group"><label class="control-label col-sm-3"> Email:</label><div  class="col-sm-9"><input type="email" name="data[roomInfo][1][email]" class="form-control input-medium" id="" value="" required></div></div>--><!--<div class="alert-box" role="alert"><h4>Supplements paid by customer directly to the hoteldfgdf</h4><span class="supp_down_text">This amount does not include local taxes and should be paid directly to the hotel upon check out</span></div>--><div style="margin-top: 15px;"><table class="table table-bordered table-responsive"><thead><tr><th>Room preferences</th><th></th></tr></thead><tbody><tr><td colspan="2">Bedding preferences are subject to supplier availability, but we will do our very best to assist with your request.</td></tr><tr><td><input total_tax="22.41" checked="" price="209.3" value="209.3|190.27|0|0" k="0" room_index="1" class="bedding_chkboxes" rom_pref_string="2 people 1 bed " name="data[roomInfo][1][optradio]" type="radio">2 people 1 bed </td><td>$209.3</td></tr></tbody></table></div><div class="clearfix"><label class="control-label">Special Requests (not guaranteed)</label><textarea type="text" name="data[roomInfo][1][splReq]" class="form-control input-medium"></textarea></div></span>
                                    <div class="clearfix mt20 actions">
                                        <button type="submit" class="btn btn-primary pull-right">Continue</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                   	</div>
		    	</div><!--End of tab 1-->
		    	<div role="tabpanel" class="tab-pane" id="profile">
		    		<h1>Tab 2 </h1>
		    	</div>
		  	</div><!--End of .tab-content-->
		</div><!--End of tabs-->
	</div>
	<div class="col-sm-4">
            <div class="wizWized">
                <div class="sWhead">
                    <h4>Reservation Details </h4>
                </div>
                <div class="clearfix">
                    <div class="media" style="padding:0px 15px;">
                        <a class="pull-left" href="#">
                            <img style="width:100px; margin-bottom: 0px;" class="media-object img-responsive thumbnail" src="http://image1.urlforimages.com/Images/1988/100x100/332508914.jpg" alt="image">
                        </a>
                        <div class="media-body">
                            <div class="panel-title">
                                <b>Americas Best Value Inn - Demo</b>
                            </div>
                            <div class="star-rating rating-xs rating-disabled"><div class="rating-container rating-gly-star" data-content=""><div class="rating-stars" data-content="" style="width: 40%;"></div><input name="rating" id="inputRating" class="srRating hidden form-control hide" value="2" type="text"></div><div class="caption"><span class="capText">2 Star Hotel</span></div></div>
                            <address class="hotel_location">Demo - 167 East Tropicana Avenue Las Vegas Nevada 89109</address>
                        </div>
                    </div>
                    <div class="bookingContent">
                        <ul class="list-unstyled">
                            <li role="separator" class="divider"></li>
                            <li><b>Rooms: </b><span class="number_of_rooms">1</span></li>
                            <li><b>Room Type: </b><span class="room_type_category">Standard</span></li>
                            <li><b>Adult(s)/Child(ren): </b><span class="num_adult_children">2 / 0</span></li>
                            <li role="separator" class="divider"></li>
                            <li><b>Check In: </b><span>10/15/2016</span></li>
                            <li><b>Check Out: </b><span>10/20/2016</span></li>
                            <li role="separator" class="divider"></li>
                            <li class="text"><b>Room Cost:</b><span id="total_room_cost">$186.89</span></li>
                            <li class="text total_supp_amount_li" style="display: none;"><b>Supplement:</b><span id="total_supp_amount">$0.00</span></li>
                            <li class="text"><b>Taxes/Fees:</b><span id="total_tax_fees">$22.41</span></li>
							                            <li id="couponli" style="display:none;"><b>Coupon:</b><span id="coupon_val"></span></li>
                            <li role="separator" class="divider"></li>
                            <li class="text-danger"><b>Total:</b><span id="total_price_html">$209.30</span></li>
							<li role="separator" class="divider"></li>
							<li id="coupounID">
															</li>
                        </ul>
                    </div>
                </div>
            </div>
    </div>
</div>