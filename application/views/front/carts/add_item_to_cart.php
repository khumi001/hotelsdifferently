<?php	if( empty($cart_list) ){?>

			<span>Cart is Empty</span>    
    
<?php	}else{?>

            <ul class="list-unstyled cart-list">
            <?php foreach($cart_list as $option_id=>$cart){
					foreach($cart as $date=>$details){?>
                        <li>
                            <div class="cart-detail" id="cart_<?php echo $option_id;?>_<?php echo $date;?>">
                                <h5><a href=""><?php echo $details['optionTitle'];?> </a></h5>
                                <h5>Cost: <?php echo $details['total_price'];?> <?php echo strtoupper($details['currency']);?></h5>
                                <h5>
                                    <a data-toggle="modal" href="#cartDetails_<?php echo $option_id;?>_<?php echo $date;?>">Details</a> <span>|</span> 
                                    <a href="javascript:;" onclick="removeItemFromCart('<?php echo $option_id;?>', '<?php echo $date;?>')" class="text-danger">Remove</a>
                                </h5>
                                
				                <div class="modal fade" id="cartDetails_<?php echo $option_id;?>_<?php echo $date;?>">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title"><?php echo $details['activityName'];?></h4>
                                            </div>
                                            <div class="modal-body">
                                                <ul class="list-unstyled pdetail">
                                                    <h4 style=" margin: -5px 0px 5px 5px;"><?php echo $details['optionTitle'];?></h4>
                                                    <li><strong>Date Selected: </strong> <?php echo $details['activity_date'];?></li>
                                                    <li><strong>No of Adults: </strong><?php echo $details['no_of_adults'];?></li>
                                                    <li><strong>No of Children: </strong><?php echo $details['no_of_childs'];?></li>
                                					<li><strong>Price (Adult): </strong><?php echo $details['adult_price'];?> (<?php echo strtoupper($details['currency']);?>) / person</li>
                                                    <li><strong>Price (Child): </strong><?php echo $details['child_price'];?>(<?php echo strtoupper($details['currency']);?>) / person</li>
                                                    <li><strong>Price (Total): </strong><?php echo $details['total_price'];?> <?php echo strtoupper($details['currency']);?></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                                
                            </div>
                        </li>
			<?php	} 
				}?>
            </ul>
        
<?php	}?>