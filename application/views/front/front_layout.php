<!DOCTYPE html>
<html lang="en">
    <?php echo $this->load->view('front/includes/header'); ?>
    <body>
		<!-- Go to www.addthis.com/dashboard to customize your tools --> <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5864bf851ff4218d"></script> 
        <?php echo $this->load->view('front/includes/body_header'); ?>
        <?php echo $this->load->view('front/includes/navigation'); ?>
        <div class="clear"></div>
        <div id="result_container">
            <div class="page_container">
                         <?php // echo $this->load->view('front/includes/breadcrumbs'); ?>
                        <?php
                         echo $this->load->view($page); ?>
                    </div>
                </div>    
        <?php echo $this->load->view('front/includes/body_footer'); ?>
        <?php echo $this->load->view('front/includes/footer'); ?>
    </body>
</html>
