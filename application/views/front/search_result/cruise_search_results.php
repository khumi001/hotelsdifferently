<?php

$cruise_lines_html = GetCruiseLinesHtml($cruise_lines);
$ship_names_checkbox_html = GetShipNamesCheckboxesHtml($cruise_lists);
$port_detination_checkbox_html = GetPortDestinationHtml($cruise_lists);

function GetMinValue($sailing_list)
{
    if(isset($sailing_list['inside_cabin_price']) && floatval($sailing_list['inside_cabin_price']) >0.0)
    {
        $min_price = floatval($sailing_list['inside_cabin_price']);
    }
    else if(isset($sailing_list['ocean_view_price']) && floatval($sailing_list['ocean_view_price']) >0.0)
    {
        $min_price = floatval($sailing_list['ocean_view_price']);
    }
    else if(isset($sailing_list['suit_price']) && floatval($sailing_list['suit_price']) >0.0)
    {
        $min_price = floatval($sailing_list['suit_price']);
    }
    else if(isset($sailing_list['balcony_price']) && floatval($sailing_list['balcony_price']) >0.0)
    {
        $min_price = floatval($sailing_list['balcony_price']);
    }
    else
    {
        $min_price = 0.0;
    }

    if($min_price > floatval($sailing_list['inside_cabin_price'])  &&  (floatval($sailing_list['inside_cabin_price']) > 0.0))
    {
        $min_price = floatval($sailing_list['inside_cabin_price']);
    }
    if($min_price >  floatval($sailing_list['ocean_view_price'])  &&  (floatval($sailing_list['ocean_view_price']) > 0.0))
    {
        $min_price = floatval($sailing_list['ocean_view_price']);
    }
    if($min_price >  floatval($sailing_list['suit_price'])  &&  (floatval($sailing_list['suit_price']) > 0.0))
    {
        $min_price = floatval($sailing_list['suit_price']);
    }
    if($min_price >  floatval($sailing_list['balcony_price'])  &&  (floatval($sailing_list['balcony_price']) > 0.0))
    {
        $min_price = floatval($sailing_list['balcony_price']);
    }

    return $min_price;
}

function GetCruiseLinesHtml($cruise_lines)
{
    $cruise_lines_html = "";
    $cruise_lines_html .= <<<EOM
<div id="cruiseLine" class="clearfix bt1" style="padding: 15px 0px; margin-top: 10px;">
                            <h4 class="tc1" style="margin: 5px 0px 0px 0px;">Cruise Line:</h4>
                            <div class="ASelector">
                                <span> <a class="cruise_lines_select_all">Select All</a> </span>
                                <span class="">|</span>
                                <span> <a class="cruise_lines_clear_all">Clear</a> </span>
                            </div>
                            <table class="table">
                                <tbody>
EOM;

    $cruise_lines_count = count($cruise_lines);

    for($i=0; $i<$cruise_lines_count; $i++)
    {
        $a_cruise_line = $cruise_lines[$i];
        $cruise_lines_html .= <<<EOM
<tr>
                                        <td style="width:10%">
                                            <input class="cruise_lines_chkbox" type="checkbox" value="{$a_cruise_line['tourico_cruise_line_id']}" name="">
                                        </td>
                                        <td style="width:90%">
                                            <label>{$a_cruise_line['cruise_line_name']}</label>
                                        </td>
                                    </tr>
EOM;

    }

    $cruise_lines_html .= <<<EOM
</tbody>
                            </table>
                        </div>
EOM;

return $cruise_lines_html;

}

function GetShipNamesCheckboxesHtml($cruise_lists)
{
    $ship_names_html = "";

    $cruise_lists_count = count($cruise_lists);

    for($i=0; $i<$cruise_lists_count; $i++)
    {

        $a_cruise = $cruise_lists[$i];

        if(!isset($ship_names_container[$a_cruise['ship_name']]))
        {
            $ship_names_container[$a_cruise['ship_name']] = 1;
            $ship_names_html .= <<<EOM
                <tr>
                    <td style="width:10%">
                        <input class="ship_name_checkboxes" type="checkbox" value="{$a_cruise['ship_name']}" name="">
                    </td>
                    <td style="width:90%">
                        <label>{$a_cruise['ship_name']}</label>
                    </td>
                </tr>
EOM;
        }

    }

    return $ship_names_html;
}

function GetPortDestinationHtml($cruise_lists)
{
    $departure_port_checkbox_html = "";
    $port_found_marker = [];

    $cruise_lists_count = count($cruise_lists);
    for($i=0; $i<$cruise_lists_count; $i++)
    {
        $a_cruise_list = $cruise_lists[$i];
        $port_infos = $a_cruise_list['itinerary_segment_list'];
        $port_infos_count = count($port_infos);

        for($j=0; $j<$port_infos_count; $j++)
        {
            $a_port_info = $port_infos[$j];
            if(!isset($port_found_marker[$a_port_info['port_id']]))
            {
                $port_found_marker[$a_port_info['port_id']] = 0;
                $departure_port_checkbox_html .= <<<EOM
                                                <tr>
                                                    <td style="width:10%">
                                                        <input class="destination_port" type="checkbox" value="{$a_port_info['port_id']}" name="">
                                                    </td>
                                                    <td style="width:90%">
                                                        <label>{$a_port_info['port_name']}</label>
                                                    </td>
                                                </tr>
EOM;

            }
        }
    }

    return $departure_port_checkbox_html;
}

?>

<div class="width-row">
    <div class="clearfix">
        <div class="col-sm-4">
            <div class="sWidgets">
                <div class="clearfix">
                    <div class="col-sm-12">
                        <div class="cruiseMainSearch">
                            <form method="post" action="<?php echo base_url()?>view_cruise_search_result" id="cuisesForm">
                                <div class="form-group">
                                    <label class="control-label col-md-12">Where are you sailing?</label>
                                    <input type='hidden' name='cruiseDestinationId' id='cruiseDestinationId'>
                                    <select style='text-align: center;'class="select2me form-control input-xlarge cruiseDestination" name='Destination' id='cruiseDestination'>
                                        <?php
                                        foreach($cruise_list_parent as $a_cruise_list_parent)
                                        {
                                            echo <<<EOM
                                                                                            <option cruise_destination_id='{$a_cruise_list_parent['tourico_destination_id']}'  value='{$a_cruise_list_parent['destination_name']}'>{$a_cruise_list_parent['destination_name']}</option>
EOM;
                                            foreach($cruise_list_child as $a_cruise_list_child)
                                            {
                                                if($a_cruise_list_child['parent_id'] == $a_cruise_list_parent['tourico_destination_id'])
                                                {
                                                    echo <<<EOM
                                                                                                <option cruise_destination_id='{$a_cruise_list_parent['tourico_destination_id']}' value='{$a_cruise_list_child['destination_name']}'> - {$a_cruise_list_child['destination_name']}</option>
EOM;

                                                }
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-12">Departure Month</label>
                                    <select style='text-align: center;'class="select2me form-control input-xlarge" name="cruise_departure_month" id="cruise_departure_month">
                                        <?php
                                        echo $cruise_departure_month_options;
                                        ?>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-12">Cruise Length</label>
                                    <select style='text-align: center;'class="select2me form-control input-xlarge" name="cruise_length" id="cruise_length">
                                        <option value="">Cruise Length</option>

                                        <?php
                                        foreach($cruise_length as $a_cruise_length => $a_cruise_length_desc)
                                            echo "<option value='{$a_cruise_length}'>{$a_cruise_length_desc}</option>";
                                        ?>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn .btn-custom">Search</button>
                                </div>
                            </form>
                        </div>
                        
                        <div class="cruiseSearchOptions bt1">
                            <h4 class="tc1">Discounts:</h4>
                            <h5><strong>Discounted fares for:</strong></h5>
                            <h5>US Residents</h5>
                            <div class="form-group">
                                <label class="control-label col-md-12">Where do you live?</label>
                                <select style='text-align: center;'class="select2me form-control input-xlarge" name='' id=''>
                                    <option value="0">International</option>
                                    <option value="AA">AA</option>
                                    <option value="AE">AE</option>
                                    <option value="AL">Alabama</option>
                                    <option value="AK">Alaska</option>
                                    <option value="AP">AP</option>
                                    <option value="AZ">Arizona</option>
                                    <option value="AR">Arkansas</option>
                                </select>
                            </div>
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td style="width:10%; border-top: none;">
                                            <input type="checkbox" id="" value="" name="">
                                        </td>
                                        <td style="width:90%; border-top: none;">
                                            <label for="">For senior citizens (55+)</label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:10%">
                                            <input type="checkbox" id="" value="12090" name="">
                                        </td>
                                        <td style="width:90%">
                                            <label for="">Returning Passenger</label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:10%">
                                            <input type="checkbox" id="" value="12090" name="">
                                        </td>
                                        <td style="width:90%">
                                            <label for="">Interline</label>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
            <div class="panel panel-default cbg">
                <div class="panel-heading">
                    <h3 class="panel-title bold">Filter Results</h3>
                </div>
                <div class="panel-body">
                    <div class="filters">
                        <div id="priceRange" class="clearfix">
                            <h4 class="tc1" style="margin: 0px 0px 25px 0px;">Price Range</h4>
                            <div class="range1_cruise"></div>
                            <input type="text" id="amount_cruise" readonly>
                        </div>
                        <?php echo $cruise_lines_html ?>
                        <div id="ship" class="clearfix bt1" style="padding: 15px 0px; margin-top: 10px;">
                            <h4 class="tc1" style="margin: 5px 0px 0px 0px;">Ship:</h4>
                            <div class="ASelector">
                                <span> <a class="ship_name_select_all">Select All</a> </span>
                                <span class="">|</span>
                                <span> <a class="ship_name_clear_all">Clear</a> </span>
                            </div>
                            <table class="table">
                                <tbody>
                                    <?php echo $ship_names_checkbox_html ?>
                                </tbody>
                            </table>
                        </div>
                        <div id="departurePorts" class="clearfix bt1" style="padding: 15px 0px; margin-top: 10px;">
                            <h4 class="tc1">Departure Ports:</h4>
                            <div class="ASelector">
                                <span> <a class="departure_port_select_all">Select All</a> </span>
                                <span class="">|</span>
                                <span> <a class="departure_port_clear_all">Clear</a> </span>
                            </div>
                            <table class="table">
                                <tbody>
                                    <?php echo $port_detination_checkbox_html ?>
                                </tbody>
                            </table>
                        </div>
                        <div id="cruiseLength" class="clearfix bt1" style="padding: 15px 0px; margin-top: 10px;">
                            <h4 class="tc1">Cruise Length:</h4>
                            <div class="ASelector">
                                <span> <a class="cruise_length_select_all">Select All</a> </span>
                                <span class="">|</span>
                                <span> <a class="cruise_length_clear_all">Clear</a> </span>
                            </div>
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td style="width:10%;">
                                            <input class="cruise_length_checkbox" id="" name="" type="checkbox" value="3">
                                        </td>
                                        <td class="" style="width:90%;">
                                            3-5 Nights
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:10%;">
                                            <input class="cruise_length_checkbox" id="" name="" type="checkbox" value="6">
                                        </td>
                                        <td class="" style="width:90%;">
                                            6-9 Nights
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:10%;">
                                            <input class="cruise_length_checkbox" id="" name="" type="checkbox" value="10">
                                        </td>
                                        <td class="" style="width:90%;">
                                            10-14 Nights
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:10%;">
                                            <input class="cruise_length_checkbox" id="" name="" type="checkbox" value="15">
                                        </td>
                                        <td class="" style="width:90%;">
                                            15+ Nights
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="cruise_details">
            <?php

            $cruise_lists_count = count($cruise_lists);

            for($i=0; $i<$cruise_lists_count; $i++)
            {
                $a_cruise = $cruise_lists[$i];

                $sailing_list = $a_cruise['sailing_list'];

                $inside_cabin_price = floatval($sailing_list['inside_cabin_price']) <= 0.0 ? "N/A" : $sailing_list['inside_cabin_price'];
                $ocean_view_price = floatval($sailing_list['ocean_view_price']) <= 0.0 ? "N/A" : $sailing_list['ocean_view_price'];
                $suit_price = floatval($sailing_list['suit_price']) <= 0.0 ? "N/A" : $sailing_list['suit_price'];
                $balcony_price = floatval($sailing_list['balcony_price']) <= 0.0 ? "N/A" : $sailing_list['balcony_price'];

                $inside_cabin_per_night = $inside_cabin_price == "N/A" ? "N/A" : round(floatval($inside_cabin_price)/floatval($a_cruise['cruise_length']) , 2);
                $ocean_view_price_per_night = $ocean_view_price == "N/A" ? "N/A" : round(floatval($ocean_view_price)/floatval($a_cruise['cruise_length']) , 2);
                $suit_price_per_night = $suit_price == "N/A" ? "N/A" : round(floatval($suit_price)/floatval($a_cruise['cruise_length']) , 2);
                $balcony_price_per_night = $balcony_price == "N/A" ? "N/A" : round(floatval($balcony_price)/floatval($a_cruise['cruise_length']) , 2);

                $min_value = GetMinValue($sailing_list);

                echo <<<EOM
<div class="col-sm-8" style="float: right;">
            <div class="row sResult" id="content">
                <div class='panel panel-info' id='sResult'>
                    <div class="panel-heading clearfix">
                        <div class="panel-title pull-left">
                            <h4>{$a_cruise['cruise_line_name']}</h4>
                        </div>
                    </div>
                    <div class='panel-body cinfo'>
                        <div class='media clearfix'>
                            <a class='pull-left' href='#'>
                                <div class="companyLogo thumbnail" style="width: 230px;">
                                    <img class='media-object img-responsive' src='{$a_cruise['ship_image']}' alt='image'>
                                </div>
                            </a>
                            <div class='media-body'>
                                <div class="col-md-7 CRD">
                                    <img src="{$a_cruise['cruise_line_logo']}" class="img-responsive" alt="Image" style="padding: 0px 5px;border-radius: 5px; background: white;">
                                    <h5><strong>Ship:</strong> {$a_cruise['ship_name']} </h5>
                                    <ul class="list-unstyled list-inline">
                                        <li>
                                            <a class="ship_details" data-toggle="modal" href='#cpop' ship_id='{$a_cruise['ship_id']}' tab='departure'>Departures</a>
                                        </li>
                                        <li>
                                            <a class="ship_details" data-toggle="modal" href='#cpop' ship_id='{$a_cruise['ship_id']}' tab='itinerary'>Itinerary</a>
                                        </li>
                                        <li>
                                            <a class="ship_details" data-toggle="modal" href='#cpop' ship_id='{$a_cruise['ship_id']}' tab='ports'>Ports</a>
                                        </li>
                                        <li>
                                            <a class="ship_details" data-toggle="modal" href='#cpop' ship_id='{$a_cruise['ship_id']}' tab='photos'>Photos</a>
                                        </li>
                                        <li>
                                            <a class="ship_details" data-toggle="modal" href='#cpop' ship_id='{$a_cruise['ship_id']}' tab='amenities'>Amenities</a>
                                        </li>
                                        <li>
                                            <a class="ship_details" data-toggle="modal" href='#cpop' ship_id='{$a_cruise['ship_id']}' tab='decks'>Decks</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-5">
                                    <div class="row">
                                        <div class="text-center">
                                            <h4 style="margin:5px 0px; color: #EC2A26;">FROM</h4>
                                            <h4>{$min_value}</h4>
                                            <button type="button" class="btn btn-md btn-danger" style="margin-top: 50px;">Book Now</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix promo">
                            <ul class="list-unstyled list-inline promo">
                                <li><h5><strong>Special Offers:</strong></h5></li>
                                <li><a href="#" class="">Great Rates + $50OBC,</a></li>
                                <li><a href="#" class=""> Past Guest Bonus Offer,</a></li>
                                <li><a href="#" class="">Free Upgrades</a></li>
                            </ul>
                        </div>
                        <div class="panel-footer">
                            <div class="table-responsive cruiseTable">
                                <table class="table">
                                    <thead style="background: transparent;">
                                        <tr>
                                            <th>Inside Cabin</th>
                                            <th>Ocean View</th>
                                            <th>Suite View</th>
                                            <th>Balcony Suite</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><strong>{$inside_cabin_price}</strong></td>
                                            <td><strong>{$ocean_view_price}</strong></td>
                                            <td><strong>{$suit_price}</strong></td>
                                            <td><strong>{$balcony_price}</strong></td>
                                        </tr>
                                        <tr>
                                            <td>\${$inside_cabin_per_night}/Night</td>
                                            <td>\${$ocean_view_price_per_night}/Night</td>
                                            <td>\${$suit_price_per_night}/Night</td>
                                            <td>\${$balcony_price_per_night}/Night</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
EOM;


            }


            ?>
        </div>

    </div>
    </div>
    <center class="clearfix">
        <ul class = "pagination">
           <li><a href = "#">&laquo;</a></li>
           <li><a href = "#">1</a></li>
           <li><a href = "#">2</a></li>
           <li><a href = "#">3</a></li>
           <li><a href = "#">4</a></li>
           <li><a href = "#">5</a></li>
           <li><a href = "#">&raquo;</a></li>
        </ul>
    </center>  
</div>

<div class="modal fade cmodal" id="cpop">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Cruise Details</h4>
            </div>
            <div class="modal-body">
                <div class='' id='sResultModal'>
                    <div class="media" id="cruise_media_section">

                    </div>
                    <div class="lowerModalBody cruise">
                        <div role="tabpanel">

                            <ul class="nav nav-tabs nav-justified nav-pills smnav" role="tablist">
                                <li role="presentation" class="active" id="tbpDepartures">
                                    <a href="#" id="tbpDepartures" data-target="#departure" aria-controls="departure" role="tab" data-toggle="tab">Departures</a>
                                </li>
                                <li role="presentation" id="tbpItinerary">
                                    <a href="#"  data-target="#itinerary" aria-controls="itinerary" role="tab" data-toggle="tab">Itinerary</a>
                                </li>
                                <li role="presentation" id="tbpPorts">
                                    <a href="#"  data-target="#ports" aria-controls="ports" role="tab" data-toggle="tab">Ports</a>
                                </li>
                                <li role="presentation"  id="tbpPhotos">
                                    <a href="#" data-target="#pictures" aria-controls="pictures" role="tab" data-toggle="tab">Photos</a>
                                </li>
                                <li role="presentation" id="tbpAmenities">
                                    <a href="#"  data-target="#amenities" aria-controls="amenities" role="tab" data-toggle="tab">Amenities</a>
                                </li>
                                <li role="presentation" id="tbpDecks">
                                    <a href="#" data-target="#decks" aria-controls="decks" role="tab" data-toggle="tab">Decks</a>
                                </li>
                            </ul>

                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active tbcDeparture" id="departure">
                                    <div class="clearfix">
                                        <div class="col-xs-12">
                                            <h3>Departure Dates</h3>
                                            <p>Prices are per person based on double occupancy and are lowest available.</p>
                                            <div class="table-responsive cruiseDetailTable">
                                                <table class="table">
                                                    <thead style="background: transparent;">
                                                        <tr>
                                                            <th>Departs</th>
                                                            <th>Returns</th>
                                                            <th>Inside Cabin</th>
                                                            <th>Ocean View</th>
                                                            <th>Balcony</th>
                                                            <th>Suite</th>
                                                            <th>&nbsp;</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="departure_descriptions">

                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="clearfix detailPromo">
                                                <ul class="list-unstyled list-inline detailPromo">
                                                    <li><h5><strong>Special Offers:</strong></h5></li>
                                                    <li><a href="#" class="">Great Rates + $50OBC,</a></li>
                                                    <li><a href="#" class=""> Past Guest Bonus Offer,</a></li>
                                                    <li><a href="#" class="">Free Upgrades</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div role="tabpanel" class="tab-pane fade tbcItinerary" id="itinerary">
                                    <div class="clearfix">
                                        <div class="jumbotron">
                                            <div class="container" id="map_container">
                                                <h1>Map</h1>
                                                <p>Contents ...</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix">
                                        <table class="table table-hover table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Days</th>
                                                    <th>Port of Call</th>
                                                    <th>Arrival Times</th>
                                                    <th>Departure Times</th>
                                                </tr>
                                            </thead>
                                            <tbody id="itinerary_table_rows">
                                                <tr>
                                                    <td>1</td>
                                                    <td>Miami,Fl</td>
                                                    <td>-----</td>
                                                    <td>5:00 PM</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                
                                <div role="tabpanel" class="tab-pane fade tbcPorts" id="ports">
                                    <div class="clearfix">
                                        <div class="col-md-12">
                                            <ul class="list-unstyled" id="port_descriptions">
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                
                                <div role="tabpanel" class="tab-pane fade tbcPictures" id="pictures">
                                    <div class="clearfix">
                                        <div class="col-sm-6 hidden-xs" id="slider-thumbs">
                                            <!-- Bottom switcher of slider -->
                                            <ul class="list-unstyled" id="ship_images_small">
                                                <li class="col-sm-3">
                                                    <a class="thumbnail" id="carousel-selector-0"><img src="http://placehold.it/150x150&text=zero"></a>
                                                </li>
                                                <li class="col-sm-3">
                                                    <a class="thumbnail" id="carousel-selector-1"><img src="http://placehold.it/150x150&text=1"></a>
                                                </li>
                                                <li class="col-sm-3">
                                                    <a class="thumbnail" id="carousel-selector-2"><img src="http://placehold.it/150x150&text=2"></a>
                                                </li>
                                                <li class="col-sm-3">
                                                    <a class="thumbnail" id="carousel-selector-3"><img src="http://placehold.it/150x150&text=3"></a>
                                                </li>
                                                <li class="col-sm-3">
                                                    <a class="thumbnail" id="carousel-selector-4"><img src="http://placehold.it/150x150&text=4"></a>
                                                </li>
                                                <li class="col-sm-3">
                                                    <a class="thumbnail" id="carousel-selector-5"><img src="http://placehold.it/150x150&text=5"></a>
                                                </li>
                                                <li class="col-sm-3">
                                                    <a class="thumbnail" id="carousel-selector-6"><img src="http://placehold.it/150x150&text=6"></a>
                                                </li>
                                                <li class="col-sm-3">
                                                    <a class="thumbnail" id="carousel-selector-7"><img src="http://placehold.it/150x150&text=7"></a>
                                                </li>
                                                <li class="col-sm-3">
                                                    <a class="thumbnail" id="carousel-selector-8"><img src="http://placehold.it/150x150&text=8"></a>
                                                </li>
                                                <li class="col-sm-3">
                                                    <a class="thumbnail" id="carousel-selector-9"><img src="http://placehold.it/150x150&text=9"></a>
                                                </li>
                                                <li class="col-sm-3">
                                                    <a class="thumbnail" id="carousel-selector-10"><img src="http://placehold.it/150x150&text=10"></a>
                                                </li>
                                                <li class="col-sm-3">
                                                    <a class="thumbnail" id="carousel-selector-11"><img src="http://placehold.it/150x150&text=11"></a>
                                                </li>
                                                <li class="col-sm-3">
                                                    <a class="thumbnail" id="carousel-selector-12"><img src="http://placehold.it/150x150&text=12"></a>
                                                </li>
                                                <li class="col-sm-3">
                                                    <a class="thumbnail" id="carousel-selector-13"><img src="http://placehold.it/150x150&text=13"></a>
                                                </li>
                                                <li class="col-sm-3">
                                                    <a class="thumbnail" id="carousel-selector-14"><img src="http://placehold.it/150x150&text=14"></a>
                                                </li>
                                                <li class="col-sm-3">
                                                    <a class="thumbnail" id="carousel-selector-15"><img src="http://placehold.it/150x150&text=15"></a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="col-xs-12" id="slider">
                                                <div class="row">
                                                    <div class="col-sm-12" id="carousel-bounding-box">
                                                        <div class="carousel slide" id="slPicture">
                                                            <!-- Carousel items -->
                                                            <div class="carousel-inner" id="ship_image_large">
                                                                <div class="active item" data-slide-number="0"><img src="http://placehold.it/470x480&text=zero"></div>
                                                                <div class="item" data-slide-number="1"><img src="http://placehold.it/470x480&text=1"></div>
                                                                <div class="item" data-slide-number="2"><img src="http://placehold.it/470x480&text=2"></div>
                                                                <div class="item" data-slide-number="3"><img src="http://placehold.it/470x480&text=3"></div>
                                                                <div class="item" data-slide-number="4"><img src="http://placehold.it/470x480&text=4"></div>
                                                                <div class="item" data-slide-number="5"><img src="http://placehold.it/470x480&text=5"></div>
                                                                <div class="item" data-slide-number="6"><img src="http://placehold.it/470x480&text=6"></div>
                                                                <div class="item" data-slide-number="7"><img src="http://placehold.it/470x480&text=7"></div>
                                                                <div class="item" data-slide-number="8"><img src="http://placehold.it/470x480&text=8"></div>
                                                                <div class="item" data-slide-number="9"><img src="http://placehold.it/470x480&text=9"></div>
                                                                <div class="item" data-slide-number="10"><img src="http://placehold.it/470x480&text=10"></div>
                                                                <div class="item" data-slide-number="11"><img src="http://placehold.it/470x480&text=11"></div>
                                                                <div class="item" data-slide-number="12"><img src="http://placehold.it/470x480&text=12"></div>
                                                                <div class="item" data-slide-number="13"><img src="http://placehold.it/470x480&text=13"></div>
                                                                <div class="item" data-slide-number="14"><img src="http://placehold.it/470x480&text=14"></div>
                                                                <div class="item" data-slide-number="15"><img src="http://placehold.it/470x480&text=15"></div>
                                                            </div>
                                                            <!-- Carousel nav -->
                                                            <a class="left carousel-control" href="#slPicture" role="button" data-slide="prev">
                                                                <span class="glyphicon glyphicon-chevron-left"></span>
                                                            </a>
                                                            <a class="right carousel-control" href="#slPicture" role="button" data-slide="next">
                                                                <span class="glyphicon glyphicon-chevron-right"></span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div role="tabpanel" class="tab-pane fade" id="map">
                                </div>
                                
                                <div role="tabpanel" class="tab-pane fade tbcAmenities" id="amenities">
                                    <div class="info">
                                        <div class="clearfix">
                                            <div class="col-xs-12">
                                                <h3>Ship Amenities</h3>
                                            </div>
                                        </div>
                                        <div class="clearfix">
                                            <div class="col-sm-6" id="ship_cabin_features">
                                                <h4 class="info-title">Cabin Features:</h4>
                                                <ul class="list-unstyled list-inline">
                                                    <li></li>
                                                </ul>
                                            </div>
                                            <div class="col-sm-6" id="ship_features">
                                                <h4 class="info-title">Ship Features:</h4>
                                                <ul class="list-unstyled list-inline">
                                                    <li></li>
                                                </ul>
                                            </div>
                                            <div class="col-sm-6" id="ship_classes_and_services">
                                            <h4 class="info-title">Classes and Services:</h4>                      
                                                <ul class="list-unstyled list-inline">
                                                    <li></li>
                                                </ul>
                                            </div>
                                            <div class="col-sm-6" id="ship_recreation">
                                                <h4 class="info-title">Recreation:</h4>
                                                <ul class="list-unstyled list-inline">
                                                    <li></li>
                                                </ul>
                                            </div>
                                            <div class="col-sm-6" id="ship_dining">
                                                <h4 class="info-title">Dining:</h4>
                                                <ul class="list-unstyled list-inline">
                                                    <li></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div role="tabpanel" class="tab-pane fade tbcDecks" id="decks">

                                <div class="clearfix">
                                    <div class="col-sm-8 topPart">
                                        <div class="form-group" id="deck_selection_container">
                                            <label class="control-label">View Deck Plan:</label>
                                            <select style='text-align: center; margin-left:10px;width:75%;'class="select2me form-control input-xlarge deck_selections_options"  name='' id='deck_selections'>

                                            </select>
                                        </div>
                                        <div class="form-group main" id="main_deck_images">

                                        </div>
                                        <div class="form-group">
                                            <div class="table-responsive">
                                                <table class="table table-hover table-bordered" id="deck_details">

                                                </table>
                                            </div>
                                        </div>
                                        <div class="form-group" id="ship_public_area">
                                            <div class="clearfix"><h3>Public Areas</h3></div>
                                            <div class="clearfix">
                                                <h4 class="info-title">Panorama Buffet</h4>
                                                <p>
                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores fuga debitis 
                                                    perferendis sapiente tenetur, earum iusto tempora totam repudiandae beatae omnis, 
                                                    maiores maxime optio error ducimus qui suscipit enim eum.
                                                </p>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4" id="deck_image">
                                        <img src="https://book.cruisedirect.com/content/images/Cruise/8/78/Deck4/Deck7.png" class="img-responsive" alt="Image">

                                        <img src="https://book.cruisedirect.com/content/images/Cruise/8/78/Deck4/Deck7.png" class="img-responsive" alt="Image">
                                    </div>
                                    <div id="deck_part_image_container">

                                    </div>
                                </div>
                                <div class="clearfix">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    <script src="<?php echo base_url(); ?>public/assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>

    <script src="<?php echo base_url(); ?>public/affilliate_theme/plugins/checkincheckout/bootstrap-datepicker.js" type="text/javascript"></script>

    <script src="https://maps.googleapis.com/maps/api/js"></script>

    <script>


        function initialize(portArray,mapElementId)
        {
            var totalLongitude  = 0;
            var totalLattitude = 0;
            //var pathArray = [];
            for(var index=0;index<portArray.length;index++)
            {
                totalLongitude+= parseInt(portArray[index].longitude);
                totalLattitude+= parseInt(portArray[index].latitude);
            }
            var midLongitude = totalLongitude/portArray.length;
            var midLatitude = totalLattitude/portArray.length;
            var mapCanvas = document.getElementById(mapElementId);
            console.log(midLongitude);
            console.log(midLatitude);
            var mapOptions =
            {
                center: {lat: midLatitude, lng: midLongitude},
                zoom: 2
            }

            var map = new google.maps.Map(mapCanvas, mapOptions);

            var linepointList = [];
            for(var index=0;index<portArray.length;index++)
            {
                var marker = new google.maps.Marker({
                    position: {lat: parseFloat(portArray[index].latitude), lng: parseFloat(portArray[index].longitude)},
                    map: map,
                    title: portArray[index].port_name
                });

                linepointList.push({lat: parseFloat(portArray[index].latitude), lng: parseFloat(portArray[index].longitude)})
            }

            var flightPath = new google.maps.Polyline({
                path: linepointList,
                geodesic: true,
                strokeColor: '#FF0000',
                strokeOpacity: 1.0,
                strokeWeight: 2
            });

            flightPath.setMap(map);
        }


        function ShowDeckInfoByDeckNumber(selected_deck_number)
        {
            deck_lists = current_selected_ship_details.ship_detail.deck_list;

            //console.log(current_selected_ship_details);
            //selected_deck_number = $(this).val();

            deck_part_images = "";
            deck_image_html = "";

            deck_list_reverse_image = deck_lists.length - 1;
            for(deck_index=0; deck_index<deck_lists.length; deck_index++)
            {
                if(selected_deck_number == deck_lists[deck_index].deck_number)
                {
                    deck_part_images += '<img src="' + deck_lists[deck_list_reverse_image]['location_image'].on  + '" class="img-responsive" alt="Image">';
                }
                else
                {
                    deck_part_images += '<img src="' + deck_lists[deck_list_reverse_image]['location_image'].off  + '" class="img-responsive" alt="Image">';
                }

                deck_list_reverse_image--;

                if(selected_deck_number == deck_lists[deck_index].deck_number)
                {
                    deck_image_html += '<img src="' + deck_lists[deck_index].deck_image + '" class="img-responsive" alt="Image">';

                    public_area = deck_lists[deck_index].public_area;

                    public_area_html = "";

                    if(public_area.length > 0)
                    {
                        public_area_html += '<div class="clearfix"><h3>Public Areas</h3></div>' +
                        '<div class="clearfix">';
                    }

                    for(public_area_index=0; public_area_index<public_area.length; public_area_index++)
                    {
                        public_area_html += '<h4 class="info-title">' + public_area[public_area_index].name + '</h4>';
                        if(public_area[public_area_index].image != undefined)
                        {
                            public_area_html += '<img src="' + public_area[public_area_index].image + '" class="img-responsive" alt="Image">';

                        }
                    }
                    $("#ship_public_area").html(public_area_html);

                    deck_details_html = '<thead>' +
                    '<tr>' +
                    '<th style="width:15%;">Legends</th>' +
                    '<th>Description</th>' +
                    '</tr>' +
                    '</thead>' +
                    '<tbody>';

                    // Deck category

                    deck_category = deck_lists[deck_index].deck_categroy;
                    for(deck_index=0; deck_index<deck_category.length; deck_index++)
                    {

                        deck_details_html += '<tr>' +
                        '<td><img src="' + deck_category[deck_index].image + '" class="img-responsive" alt="Image"></td>' +
                        '<td>' + deck_category[deck_index].name + '</td>' +
                        '</tr>';

                    }

                    deck_details_html += '</tbody>';

                    $("#deck_details").html(deck_details_html);
                }


            }

            $("#deck_image").html(deck_image_html);
            $("#main_deck_images").html(deck_part_images);
        }
    </script>

    <script>

        var deck_select_2;
        var cruise_list;
        cruise_list = <?php echo json_encode($cruise_lists);?>;

        max_price = 0.0;
        min_price = 20000000.0;

        global_min_price = min_price;
        global_max_price = max_price;

        for(i=0; i<cruise_list.length; i++)
        {
            var sailing_list = cruise_list[i]['sailing_list'];

            if(parseFloat(sailing_list['balcony_price']) > 0 && !isNaN(parseFloat(sailing_list['balcony_price'])))
            {
                if(min_price > parseFloat(sailing_list['balcony_price']))
                {
                    min_price = parseFloat(sailing_list['balcony_price']);
                }
                if(max_price < parseFloat(sailing_list['balcony_price']))
                {
                    max_price = parseFloat(sailing_list['balcony_price']);
                }
            }

            if(parseFloat(sailing_list['inside_cabin_price']) > 0 && !isNaN(parseFloat(sailing_list['inside_cabin_price'])))
            {
                if(min_price > parseFloat(sailing_list['inside_cabin_price']))
                {
                    min_price = parseFloat(sailing_list['inside_cabin_price']);
                }
                if(max_price < parseFloat(sailing_list['inside_cabin_price']))
                {
                    max_price = parseFloat(sailing_list['inside_cabin_price']);
                }
            }

            if(parseFloat(sailing_list['ocean_view_price']) > 0 && !isNaN(parseFloat(sailing_list['ocean_view_price'])))
            {
                if(min_price > parseFloat(sailing_list['ocean_view_price']))
                {
                    min_price = parseFloat(sailing_list['ocean_view_price']);
                }
                if(max_price < parseFloat(sailing_list['ocean_view_price']))
                {
                    max_price = parseFloat(sailing_list['ocean_view_price']);
                }
            }

            if(parseFloat(sailing_list['suit_price']) > 0 && !isNaN(parseFloat(sailing_list['suit_price'])))
            {
                if(min_price > parseFloat(sailing_list['suit_price']))
                {
                    min_price = parseFloat(sailing_list['suit_price']);
                }
                if(max_price < parseFloat(sailing_list['suit_price']))
                {
                    max_price = parseFloat(sailing_list['suit_price']);
                }
            }

            if(global_max_price < max_price)
            {
                global_max_price = max_price;
            }
            if(global_min_price > min_price)
            {
                global_min_price = min_price;
            }
        }

        max_price_slider = global_max_price;
        min_price_slider = global_min_price;

        $(".cruise_length_select_all").click(
            function()
            {
                $(".cruise_length_checkbox").each(
                    function()
                    {
                        $(".cruise_length_checkbox").prop("checked" ,true);
                    }
                );

                ShowCruiseDetails(cruise_list);
            }
        );

        $(".cruise_length_clear_all").click(
            function()
            {
                $(".cruise_length_checkbox").each(
                    function()
                    {
                        $(".cruise_length_checkbox").prop("checked" , false);
                    }
                );

                ShowCruiseDetails(cruise_list);
            }
        );

        $(".cruise_length_checkbox").click(
            function()
            {
                ShowCruiseDetails(cruise_list);
            }
        );

        $(".destination_port").click(
            function()
            {
                ShowCruiseDetails(cruise_list);
            }
        );

        $(".ship_name_checkboxes").click(
            function()
            {
                ShowCruiseDetails(cruise_list);
            }
        );

        $(".cruise_lines_chkbox").click(
            function()
            {
                ShowCruiseDetails(cruise_list);
            }
        );

        $(".departure_port_select_all").click(
            function()
            {
                $(".destination_port").each(
                    function()
                    {
                        $(".destination_port").prop("checked" ,true);
                    }
                );

                ShowCruiseDetails(cruise_list);
            }
        );

        $(".departure_port_clear_all").click(
            function()
            {
                $(".destination_port").each(
                    function()
                    {
                        $(".destination_port").prop("checked" , false);
                    }
                );

                ShowCruiseDetails(cruise_list);
            }
        );

        $(".cruise_lines_select_all").click(
            function()
            {
                $(".cruise_lines_chkbox").each(
                    function()
                    {
                        $(".cruise_lines_chkbox").prop("checked" ,true);
                    }
                );

                ShowCruiseDetails(cruise_list);
            }
        );

        $(".cruise_lines_clear_all").click(
            function()
            {
                $(".cruise_lines_chkbox").each(
                    function()
                    {
                        $(".cruise_lines_chkbox").prop("checked" , false);
                    }
                );

                ShowCruiseDetails(cruise_list);
            }
        );

        $(".ship_name_select_all").click(
            function()
            {
                $(".ship_name_checkboxes").each(
                    function()
                    {
                        $(".ship_name_checkboxes").prop("checked" ,true);
                    }
                );

                ShowCruiseDetails(cruise_list);
            }
        );

        $(".ship_name_clear_all").click(
            function()
            {
                $(".ship_name_checkboxes").each(
                    function()
                    {
                        $(".ship_name_checkboxes").prop("checked" , false);
                    }
                );

                ShowCruiseDetails(cruise_list);
            }
        );

        function ShowCruiseDetails(cruise_list)
        {
            var cruise_details = "";
            var cruise_lines_filter = [];
            var ship_name_filter = [];
            var destination_port_filter = [];
            var cruise_length_filter = [];

            $(".cruise_lines_chkbox:checked").each(
                function()
                {
                    cruise_lines_filter.push($(this).attr("value"));
                }
            );

            $(".ship_name_checkboxes:checked").each(
                function()
                {
                    ship_name_filter.push($(this).attr("value"));
                }
            );

            $(".destination_port:checked").each(
                function()
                {
                    destination_port_filter.push($(this).attr("value"));
                }
            );

            $(".cruise_length_checkbox:checked").each(
                function()
                {
                    cruise_length_filter.push($(this).attr("value"));
                }
            );


            for(i=0; i<cruise_list.length; i++)
            {
                var cruise_line_id_matched = 0;
                var ship_name_matched = 0;
                var destination_port_matched = 0;
                var cruise_length_matched = 0;
                var price_range_matched = 0;

                max_price = 0.0;
                min_price = 20000000.0;

                var sailing_list = cruise_list[i]['sailing_list'];

                if(parseFloat(sailing_list['balcony_price']) > 0 && !isNaN(parseFloat(sailing_list['balcony_price'])))
                {
                    if(min_price > parseFloat(sailing_list['balcony_price']))
                    {
                        min_price = parseFloat(sailing_list['balcony_price']);
                    }
                    if(max_price < parseFloat(sailing_list['balcony_price']))
                    {
                        max_price = parseFloat(sailing_list['balcony_price']);
                    }
                }

                if(parseFloat(sailing_list['inside_cabin_price']) > 0 && !isNaN(parseFloat(sailing_list['inside_cabin_price'])))
                {
                    if(min_price > parseFloat(sailing_list['inside_cabin_price']))
                    {
                        min_price = parseFloat(sailing_list['inside_cabin_price']);
                    }
                    if(max_price < parseFloat(sailing_list['inside_cabin_price']))
                    {
                        max_price = parseFloat(sailing_list['inside_cabin_price']);
                    }
                }

                if(parseFloat(sailing_list['ocean_view_price']) > 0 && !isNaN(parseFloat(sailing_list['ocean_view_price'])))
                {
                    if(min_price > parseFloat(sailing_list['ocean_view_price']))
                    {
                        min_price = parseFloat(sailing_list['ocean_view_price']);
                    }
                    if(max_price < parseFloat(sailing_list['ocean_view_price']))
                    {
                        max_price = parseFloat(sailing_list['ocean_view_price']);
                    }
                }

                if(parseFloat(sailing_list['suit_price']) > 0 && !isNaN(parseFloat(sailing_list['suit_price'])))
                {
                    if(min_price > parseFloat(sailing_list['suit_price']))
                    {
                        min_price = parseFloat(sailing_list['suit_price']);
                    }
                    if(max_price < parseFloat(sailing_list['suit_price']))
                    {
                        max_price = parseFloat(sailing_list['suit_price']);
                    }
                }

                if((min_price >= min_price_slider) && (max_price <= max_price_slider))
                {
                    price_range_matched = 1;
                }

                for(cruise_length_filter_index=0; cruise_length_filter_index<cruise_length_filter.length; cruise_length_filter_index++)
                {
                    if(cruise_length_filter[cruise_length_filter_index] == cruise_list[i]['cruise_length'])
                    {
                        cruise_length_matched = 1;
                    }
                }

                if(0 == cruise_length_filter.length)
                {
                    cruise_length_matched = 1;
                }

                var destination_ports = cruise_list[i]['itinerary_segment_list'];
                for(k=0; k<destination_ports.length; k++)
                {
                    for(j=0; j<destination_port_filter.length; j++)
                    {
                        if (destination_ports[k]['port_id'] == destination_port_filter[j]) {
                            destination_port_matched = 1;
                        }
                    }
                }

                if(0 == destination_port_filter.length)
                {
                    destination_port_matched = 1;
                }

                for(j=0; j<cruise_lines_filter.length; j++)
                {
                    if(cruise_lines_filter[j] == cruise_list[i]['cruise_line_id'])
                    {
                        cruise_line_id_matched = 1;
                    }
                }

                if(0 == cruise_lines_filter.length)
                {
                    cruise_line_id_matched = 1;
                }

                for(j=0; j<ship_name_filter.length; j++)
                {
                    if(ship_name_filter[j] == cruise_list[i]['ship_name'])
                    {
                        ship_name_matched = 1;
                    }
                }

                if(0 == ship_name_filter.length)
                {
                    ship_name_matched = 1;
                }

                if((1 == price_range_matched) && (1 == cruise_length_matched) && (1 == cruise_line_id_matched) && (1 == ship_name_matched) && (1 == destination_port_matched))
                {
                    var sailing_list = cruise_list[i]['sailing_list'];

                    inside_cabin_price = ((0 < parseFloat(cruise_list[i]['sailing_list']['inside_cabin_price']))  &&   !isNaN(parseFloat(cruise_list[i]['sailing_list']['inside_cabin_price']))) ? parseFloat(cruise_list[i]['sailing_list']['inside_cabin_price']) : "N/A";
                    ocean_view_price = ((0 < parseFloat(cruise_list[i]['sailing_list']['ocean_view_price']))  &&   !isNaN(parseFloat(cruise_list[i]['sailing_list']['ocean_view_price']))) ? parseFloat(cruise_list[i]['sailing_list']['ocean_view_price']) : "N/A";
                    suit_price = ((0 < parseFloat(cruise_list[i]['sailing_list']['suit_price']))  &&   !isNaN(parseFloat(cruise_list[i]['sailing_list']['suit_price']))) ? parseFloat(cruise_list[i]['sailing_list']['suit_price']) : "N/A";
                    balcony_price = ((0 < parseFloat(cruise_list[i]['sailing_list']['balcony_price']))  &&   !isNaN(parseFloat(cruise_list[i]['sailing_list']['balcony_price']))) ? parseFloat(cruise_list[i]['sailing_list']['balcony_price']) : "N/A";

                    inside_cabin_per_night = (inside_cabin_price == "N/A") ? "N/A" : parseFloat(inside_cabin_price / parseFloat(cruise_list[i]['cruise_length'])).toFixed(2);
                    ocean_view_price_per_night = (ocean_view_price== "N/A") ? "N/A" : parseFloat(ocean_view_price/ parseFloat(cruise_list[i]['cruise_length'])).toFixed(2);
                    suit_price_per_night = (suit_price== "N/A") ? "N/A" : parseFloat(suit_price/ parseFloat(cruise_list[i]['cruise_length'])).toFixed(2);
                    balcony_price_per_night = (balcony_price== "N/A") ? "N/A" : parseFloat(balcony_price/ parseFloat(cruise_list[i]['cruise_length'])).toFixed(2);

                    cruise_details += '<div class="col-sm-8">' +
                                            '<div class="row sResult" id="content">' +
                                                '<div class="panel panel-info" id="sResult">' +
                                                    '<div class="panel-heading clearfix">' +
                                                        '<div class="panel-title pull-left">' +
                                                            '<h4>' + cruise_list[i]['cruise_line_name'] + '</h4>' +
                                                        '</div>' +
                                                    '</div>' +
                                                '<div class="panel-body cinfo">' +
                                                    '<div class="media clearfix">' +
                                                        '<a class="pull-left" href="#">' +
                                                            '<div class="companyLogo thumbnail" style="width: 230px;">' +
                                                                '<img class="media-object img-responsive" src="' + cruise_list[i]['ship_image'] + '" alt="image">' +
                                                            '</div>' +
                                                        '</a>' +
                                                    '<div class="media-body">' +
                                                        '<div class="col-md-7 CRD">' +
                                                        '<img src="' + cruise_list[i]['cruise_line_logo'] + '" class="img-responsive" alt="Image" style="padding: 0px 5px;border-radius: 5px; background: white;">' +
                                                        '<h5><strong>Ship:</strong>' + cruise_list[i]['ship_name'] + '</h5>' +
                                                        '<ul class="list-unstyled list-inline">' +
                                                            '<li>' +
                                                            '<a data-toggle="modal" href="#cpop">Departures</a>' +
                                                            '</li>' +
                                                            '<li>' +
                                                            '<a data-toggle="modal" href="#cpop">Itinerary</a>' +
                                                            '</li>' +
                                                            '<li>' +
                                                            '<a data-toggle="modal" href="#cpop">Ports</a>' +
                                                            '</li>' +
                                                            '<li>' +
                                                            '<a data-toggle="modal" href="#cpop">Photos</a>' +
                                                            '</li>' +
                                                            '<li>' +
                                                            '<a data-toggle="modal" href="#cpop">Amenities</a>' +
                                                            '</li>' +
                                                            '<li>' +
                                                            '<a data-toggle="modal" href="#cpop">Decks</a>' +
                                                            '</li>' +
                                                            '</ul>' +
                                                            '</div>' +
                                                            '<div class="col-md-5">' +
                                                            '<div class="row">' +
                                                            '<div class="text-center">' +
                                                            '<h4 style="margin:5px 0px; color: #EC2A26;">FROM</h4>' +
                                                            '<h4>' + min_price + '</h4>' +
                                                            '<button type="button" class="btn btn-md btn-danger" style="margin-top: 50px;">Book Now</button>' +
                                                            '</div>' +
                                                            '</div>' +
                                                            '</div>' +
                                                            '</div>' +
                                                            '</div>' +
                                                            '<div class="clearfix promo">' +
                                                            '<ul class="list-unstyled list-inline promo">' +
                                                            '<li><h5><strong>Special Offers:</strong></h5></li>' +
                                                            '<li><a href="#" class="">Great Rates + $50OBC,</a></li>' +
                                                            '<li><a href="#" class=""> Past Guest Bonus Offer,</a></li>' +
                                                            '<li><a href="#" class="">Free Upgrades</a></li>' +
                                                            '</ul>' +
                                                            '</div>' +
                                                            '<div class="panel-footer">' +
                                                            '<div class="table-responsive cruiseTable">' +
                                                            '<table class="table">' +
                                                            '<thead style="background: transparent;">' +
                                                            '<tr>' +
                                                            '<th>Inside Cabin</th>' +
                                                            '<th>Ocean View</th>' +
                                                            '<th>Suite View</th>' +
                                                            '<th>Balcony Suite</th>' +
                                                            '</tr>' +
                                                            '</thead>' +
                                                            '<tbody>' +
                                                            '<tr>' +
                                                            '<td><strong>' + inside_cabin_price + '</strong></td>' +
                                                            '<td><strong>' + ocean_view_price + '</strong></td>' +
                                                            '<td><strong>' + suit_price + '</strong></td>' +
                                                            '<td><strong>' + balcony_price + '</strong></td>' +
                                                            '</tr>' +
                                                            '<tr>' +
                                                            '<td>' + inside_cabin_per_night + '/Night</td>' +
                                                            '<td>' + ocean_view_price_per_night + '/Night</td>' +
                                                            '<td>' + suit_price_per_night + '/Night</td>' +
                                                            '<td>' + balcony_price_per_night + '/Night</td>' +
                                                            '</tr>' +
                                                            '</tbody>' +
                                                            '</table>' +
                                                            '</div>' +
                                                            '</div>' +
                                                            '</div>' +
                                                            '</div>' +
                                                            '</div>' +
                                                            '</div>';

                }

            }

            $("#cruise_details").html(cruise_details);

            max_price_slider = global_max_price;
            min_price_slider = global_min_price;

        }
   // console.log(cruise_details);

</script>
    <script src="<?php echo base_url(); ?>public/affilliate_theme/javascripts/custom/sResultC_cruise.js" type="text/javascript"></script>
<script>
    $(document).ready(
        function()
        {
            //deck_select_2
            $('.select2me').select2('val', [deck_select_2]);


            $("#cruise_media_section").on('click' , '.choose_departure_date' ,
                function(event)
                {
                    $("#tbpDepartures").addClass("active");
                    $("#tbpDecks").removeClass("active");

                    $(".tbcDecks").removeClass("active in");
                    $(".tbcDeparture").addClass("active in");


                }
            );


            $(".choose_departure_date").click(
                function()
                {
                    $("#tbpDepartures").removeClass("active");
                    $("#tbpDecks").removeClass("active");
                    alert("click");


                }
            );

        }
    );
    $(".cruiseDestination").change(
        function()
        {
            cruiseDestId = $('option:selected', this).attr("cruise_destination_id");
            $("#cruiseDestinationId").val(cruiseDestId);
        }
    );
</script>

    <script>
        var current_deck_info = [];
        var cruise_search_ajax_path = "<?php echo base_url('view_cruise_details')?>";
        var selected_ship_id;
        var current_selected_ship_details;

        console.log("cruise details path : ");
        console.log(cruise_search_ajax_path);

        $('#cruise_details').on('click', '.ship_details', function(event)
            {
                var ship_id = $(this).attr('ship_id');
                var tab_clicked = $(this).attr('tab');

                if(tab_clicked == 'departure')
                {
                    $("#tbpDepartures").addClass("active");

                    $("#tbpItinerary").removeClass("active");
                    $("#tbpPorts").removeClass("active");
                    $("#tbpPhotos").removeClass("active");
                    $("#tbpAmenities").removeClass("active");
                    $("#tbpDecks").removeClass("active");

                    $(".tbcDeparture").addClass("active in");

                    $(".tbcPictures").removeClass("active in");
                    $(".tbcPorts").removeClass("active in");
                    $(".tbcItinerary").removeClass("active in");
                    $(".tbcAmenities").removeClass("active in");
                    $(".tbcDecks").removeClass("active in");
                }
                else if(tab_clicked == 'itinerary')
                {
                    $("#tbpItinerary").addClass("active");

                    $("#tbpDepartures").removeClass("active");
                    $("#tbpPorts").removeClass("active");
                    $("#tbpPhotos").removeClass("active");
                    $("#tbpAmenities").removeClass("active");
                    $("#tbpDecks").removeClass("active");

                    $(".tbcItinerary").addClass("active in");

                    $(".tbcPorts").removeClass("active in");
                    $(".tbcPictures").removeClass("active in");
                    $(".tbcDeparture").removeClass("active in");
                    $(".tbcDecks").removeClass("active in");
                    $(".tbcAmenities").removeClass("active in");
                }
                else if(tab_clicked == 'ports')
                {
                    $("#tbpPorts").addClass("active");

                    $("#tbpItinerary").removeClass("active");
                    $("#tbpDepartures").removeClass("active");
                    $("#tbpPhotos").removeClass("active");
                    $("#tbpAmenities").removeClass("active");
                    $("#tbpDecks").removeClass("active");


                    $("#tbcPorts").addClass("active in");

                    $(".tbcPictures").removeClass("active in");
                    $(".tbcItinerary").removeClass("active in");
                    $(".tbcDeparture").removeClass("active in");
                    $(".tbcDecks").removeClass("active in");
                    $(".tbcAmenities").removeClass("active in");
                }
                else if(tab_clicked == 'photos')
                {
                    $("#tbpPhotos").addClass("active");

                    $("#tbpItinerary").removeClass("active");
                    $("#tbpPorts").removeClass("active");
                    $("#tbpDepartures").removeClass("active");
                    $("#tbpAmenities").removeClass("active");
                    $("#tbpDecks").removeClass("active");

                    $(".tbcPictures").addClass("active in");

                    $(".tbcPorts").removeClass("active in");
                    $(".tbcItinerary").removeClass("active in");
                    $(".tbcDeparture").removeClass("active in");
                    $(".tbcDecks").removeClass("active in");
                    $(".tbcAmenities").removeClass("active in");
                }
                else if(tab_clicked == 'amenities')
                {
                    $("#tbpAmenities").addClass("active");

                    $("#tbpItinerary").removeClass("active");
                    $("#tbpPorts").removeClass("active");
                    $("#tbpPhotos").removeClass("active");
                    $("#tbpDepartures").removeClass("active");
                    $("#tbpDecks").removeClass("active");


                    $(".tbcAmenities").addClass("active in");

                    $(".tbcDecks").removeClass("active in");
                    $(".tbcDeparture").removeClass("active in");
                    $(".tbcItinerary").removeClass("active in");
                    $(".tbcPorts").removeClass("active in");
                    $(".tbcPictures").removeClass("active in");
                }
                else if(tab_clicked == 'decks')
                {
                    $("#tbpDecks").addClass("active");

                    $("#tbpItinerary").removeClass("active");
                    $("#tbpPorts").removeClass("active");
                    $("#tbpPhotos").removeClass("active");
                    $("#tbpAmenities").removeClass("active");
                    $("#tbpDepartures").removeClass("active");


                    $(".tbcDecks").addClass("active in");

                    $(".tbcAmenities").removeClass("active in");
                    $(".tbcDeparture").removeClass("active in");
                    $(".tbcItinerary").removeClass("active in");
                    $(".tbcPorts").removeClass("active in");
                    $(".tbcPictures").removeClass("active in");
                }


                jQuery.post( cruise_search_ajax_path, {ship_id: ship_id}).done(
                    function( data ) {

                        current_selected_ship_details = data;

                        console.log("cruise list : ");
                        console.log(cruise_list);

                        console.log(" ship list ");
                        console.log(data);

                        for(cruise_list_index=0; cruise_list_index<cruise_list.length; cruise_list_index++)
                        {
                            if(ship_id == cruise_list[cruise_list_index]['ship_id'])
                            {
                                $("#").html(cruise_list[cruise_list_index]['ship_image']);


                                a_cruise_html = "";

                              a_cruise_html += '<a class="pull-left">'+
                                     '<img style="margin-bottom: 0px;" class="media-object img-responsive thumbnail mh120" src="' + cruise_list[cruise_list_index]['ship_image'] + '" alt="image">' +
                                     '</a>' +
                                     '<div class="media-body entity">' +
                                     '<div class="panel-title">' +
                                     '<h4>' + cruise_list[cruise_list_index]['cruise_itinerary_name'] + '</h4>' +
                                     '</div>' +
                                     '<img src="' + cruise_list[cruise_list_index]['cruise_line_logo']  + '" class="img-responsive" alt="Image">' +
                                     '<a class="ship_details btn btn-danger btn-sm choose_departure_date" href="#" data-target="#departure" aria-controls="departure" role="tab" data-toggle="tab">Choose Departure Date</a>' +
                                     '</div>';

                                $("#cruise_media_section").html(a_cruise_html);

                                port_html = "";

                                itinerary_html = "";
                                itinerary_list = cruise_list[cruise_list_index].itinerary_segment_list;
                                var port_array = [];
                                for(itinerary_list_index=0; itinerary_list_index<itinerary_list.length; itinerary_list_index++)
                                {
                                    itinerary_html += '<tr>' +
                                    '<td>' + itinerary_list[itinerary_list_index]['day_no'] + '</td>' +
                                    '<td>' + itinerary_list[itinerary_list_index]['port_name'] + '</td>' +
                                    '<td>' + itinerary_list[itinerary_list_index]['arrival'] + '</td>' +
                                    '<td>' + itinerary_list[itinerary_list_index]['departure'] + '</td>' +
                                    '</tr>';

                                    port_html +=   '<li>' +
                                                   '<div class="media">' +
                                                   '<a class="pull-left" href="#">'+
                                                   '<img style="width: 195px;" class="media-object img-responsive thumbnail" src="' + itinerary_list[itinerary_list_index]['port_image'] + '" alt="Photo">' +
                                                   '</a>' +
                                                   '<div class="media-body" style="display:inline;">' +
                                                   '<h4 class="media-heading">' + itinerary_list[itinerary_list_index]['port_name'] + '</h4>' +
                                                   '<p>' + itinerary_list[itinerary_list_index]['port_description'] + '</p>' +
                                                   '</div>';

                                                    port_array[itinerary_list_index] = [];
                                                    port_array[itinerary_list_index]['longitude'] = itinerary_list[itinerary_list_index]['longitude'];
                                                    port_array[itinerary_list_index]['latitude'] = itinerary_list[itinerary_list_index]['latitude'];
                                                    port_array[itinerary_list_index]['port_name'] = itinerary_list[itinerary_list_index]['port_name'];


                                }
                                initialize(port_array , 'map_container');

                                sailing_list = cruise_list[cruise_list_index].sailing_list;

                                var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
                                var firstDate = new Date(sailing_list.arrival_date);
                                var secondDate = new Date(sailing_list.deapture_date);

                                var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)))

                                departures_html = "";
                                departures_html +=  '<tr>' +
                                                    '<td>' + sailing_list.deapture_date + '</td>' +
                                                    '<td>' + sailing_list.arrival_date + '</td>' +
                                                    '<td><strong>$' + sailing_list.inside_cabin_price + '</strong></td>' +
                                                    '<td><strong>$' + sailing_list.ocean_view_price + '</strong></td>' +
                                                    '<td><strong>$' + sailing_list.balcony_price + '</strong></td>' +
                                                    '<td><strong>$' + sailing_list.suit_price + '</strong></td>' +
                                                    '<td><button type="button" class="btn btn-danger btn-sm">Select</button></td>' +
                                                    '</tr>' +
                                                    '<tr>' +
                                                    '<td>&nbsp;</td>' +
                                                    '<td>&nbsp;</td>' +
                                                    '<td>$' + parseFloat(sailing_list.inside_cabin_price)/diffDays + '/Night</td>' +
                                                    '<td>$' + parseFloat(sailing_list.ocean_view_price)/diffDays + '/Night</td>' +
                                                    '<td>$' + parseFloat(sailing_list.balcony_price)/diffDays + '/Night</td>' +
                                                    '<td>$' + parseFloat(sailing_list.suit_price)/diffDays + '/Night</td>' +
                                                    '<td>&nbsp;</td>' +
                                                    '</tr>';

                            }
                        }

                        $("#itinerary_table_rows").html(itinerary_html);
                        $("#port_descriptions").html(port_html);
                        $("#departure_descriptions").html(departures_html);

                        ship_details = $.parseJSON(data.trim());
                        current_selected_ship_details = ship_details;

                        aminities = ship_details.ship_detail.aminities;
                        image_list = ship_details.ship_detail.image_list; //big_image_url
                        deck_list = ship_details.ship_detail.deck_list;


                        deck_selection_html = "";

                        for(dec_index=0; dec_index<deck_list.length; dec_index++)
                        {
                                deck_selection_html += '<option value="' + deck_list[dec_index].deck_number + '">' + deck_list[dec_index].deck_name + '</option>';

                        }
                        $("#deck_selections").html(deck_selection_html);
                        deck_select_2 = deck_list[0].deck_number;
                        ShowDeckInfoByDeckNumber(deck_select_2);


                        ship_image_html = "";
                        for(image_list_index=0; image_list_index<image_list.length; image_list_index++)
                        {
                            ship_image_html += '<li class="col-sm-3">'+
                            '<a class="thumbnail" id="carousel-selector-' + image_list_index + '">' +
                            '<img src="' + image_list[image_list_index]['thumb_image_url'] + '">' +
                            '</a>' +
                            '</li>';


                            if(0 == image_list_index)
                            {
                                ship_image_large_html = '<div class="active item" data-slide-number="0">' +
                                '<img src="' + image_list[image_list_index]['big_image_url'] + '"></div>';
                            }
                            else
                            {
                                ship_image_large_html += '<div class="item" data-slide-number="' + image_list_index + '">' +
                                '<img src="' + image_list[image_list_index]['big_image_url'] + '"></div>';
                            }
                        }

                        $("#ship_images_small").html(ship_image_html);
                        $("#ship_image_large").html(ship_image_large_html);

                        cabin_features_list = aminities['Cabin Features'];
                        cabin_features_str = '<h4 class="info-title">Cabin Features:</h4>'+
                                        '<ul class="list-unstyled list-inline">';
                        for(cabin_feature_index=0; cabin_feature_index<cabin_features_list.length; cabin_feature_index++)
                        {
                            cabin_features_str += '<li>' + cabin_features_list[cabin_feature_index] + '</li>';
                        }
                        cabin_features_str += '</ul>';

                        $("#ship_cabin_features").html(cabin_features_str);

                        classes_and_services_list = aminities['Classes and Services'];
                        classes_services_html = '<h4 class="info-title">Classes and Services:</h4>' +
                                                '<ul class="list-unstyled list-inline">';
                        for(classes_services_index=0; classes_services_index<classes_and_services_list.length; classes_services_index++)
                        {
                            classes_services_html += '<li>' + classes_and_services_list[classes_services_index] + '</li>';
                        }

                        classes_services_html += '</ul>';
                        $("#ship_classes_and_services").html(classes_services_html);

                        ship_features_list = aminities['Ship Features'];
                        ship_features_html = '<h4 class="info-title">Ship Features:</h4>' +
                                             '<ul class="list-unstyled list-inline">;'

                        for(ship_features_index=0; ship_features_index<ship_features_list.length; ship_features_index++)
                        {
                            ship_features_html += '<li>' + ship_features_list[ship_features_index] + '</li>';

                        }
                        ship_features_html += "</ul>";
                        $("#ship_features").html(ship_features_html);

                        ship_recreation_list = aminities.Recreation;
                        ship_recreation_html = '<h4 class="info-title">Recreation:</h4>' +
                                               '<ul class="list-unstyled list-inline">' ;
                        for(recreation_index=0; recreation_index<ship_recreation_list.length; recreation_index++)
                        {
                            ship_recreation_html += '<li>' + ship_recreation_list[recreation_index] + '</li>';
                        }
                        ship_recreation_html += '</ul>';
                        $("#ship_recreation").html(ship_recreation_html);


                        ship_dining_list = aminities.Dining;
                        ship_dining_html = '<h4 class="info-title">Dining:</h4>' +
                                            '<ul class="list-unstyled list-inline">';
                        for(dining_index=0; dining_index<ship_dining_list.length; dining_index++)
                        {
                            ship_dining_html += '<li>' + ship_dining_list[dining_index] + '</li>' ;
                        }
                        ship_dining_html += "</ul>";
                        $("#ship_dining").html(ship_dining_html);

                    });
            }
        );

        $(document).ready(
            function()
            {
                /*$(".choose_departure_date_button").click(
                    function()
                    {
                        $('#cpop').modal('hide');

                        $("#tbpDepartures").addClass("active");

                        $("#tbpItinerary").removeClass("active");
                        $("#tbpPorts").removeClass("active");
                        $("#tbpPhotos").removeClass("active");
                        $("#tbpAmenities").removeClass("active");
                        $("#tbpDecks").removeClass("active");

                        $(".tbcDeparture").addClass("active in");

                        $(".tbcPictures").removeClass("active in");
                        $(".tbcPorts").removeClass("active in");
                        $(".tbcItinerary").removeClass("active in");
                        $(".tbcAmenities").removeClass("active in");
                        $(".tbcDecks").removeClass("active in");

                        $('#cpop').modal('hide');

                    }
                );
                */


                $(".deck_selections_options").click(
                    function()
                    {
                        selected_deck_number = $(this).val();

                        ShowDeckInfoByDeckNumber(selected_deck_number);

/*

                        deck_lists = current_selected_ship_details.ship_detail.deck_list;

                        console.log(current_selected_ship_details);
                        selected_deck_number = $(this).val();

                        deck_part_images = "";
                        deck_image_html = "";

                        deck_list_reverse_image = deck_lists.length - 1;
                        for(deck_index=0; deck_index<deck_lists.length; deck_index++)
                        {
                            if(selected_deck_number == deck_lists[deck_index].deck_number)
                            {
                                deck_part_images += '<img src="' + deck_lists[deck_list_reverse_image]['location_image'].on  + '" class="img-responsive" alt="Image">';
                            }
                            else
                            {
                                deck_part_images += '<img src="' + deck_lists[deck_list_reverse_image]['location_image'].off  + '" class="img-responsive" alt="Image">';
                            }

                            deck_list_reverse_image--;

                            if(selected_deck_number == deck_lists[deck_index].deck_number)
                            {
                                deck_image_html += '<img src="' + deck_lists[deck_index].deck_image + '" class="img-responsive" alt="Image">';

                                public_area = deck_lists[deck_index].public_area;

                                public_area_html = "";

                                if(public_area.length > 0)
                                {
                                    public_area_html += '<div class="clearfix"><h3>Public Areas</h3></div>' +
                                    '<div class="clearfix">';
                                }

                                for(public_area_index=0; public_area_index<public_area.length; public_area_index++)
                                {
                                    public_area_html += '<h4 class="info-title">' + public_area[public_area_index].name + '</h4>';
                                    if(public_area[public_area_index].image != undefined)
                                    {
                                        public_area_html += '<img src="' + public_area[public_area_index].image + '" class="img-responsive" alt="Image">';

                                    }
                                }
                                $("#ship_public_area").html(public_area_html);

                                deck_details_html = '<thead>' +
                                                       '<tr>' +
                                                            '<th style="width:15%;">Legends</th>' +
                                                            '<th>Description</th>' +
                                                       '</tr>' +
                                                    '</thead>' +
                                                    '<tbody>';

                                // Deck category

                                deck_category = deck_lists[deck_index].deck_categroy;
                                for(deck_index=0; deck_index<deck_category.length; deck_index++)
                                {

                                    deck_details_html += '<tr>' +
                                          '<td><img src="' + deck_category[deck_index].image + '" class="img-responsive" alt="Image"></td>' +
                                          '<td>' + deck_category[deck_index].name + '</td>' +
                                          '</tr>';

                                }

                                deck_details_html += '</tbody>';

                                $("#deck_details").html(deck_details_html);
                            }


                        }

                        $("#deck_image").html(deck_image_html);
                        $("#main_deck_images").html(deck_part_images);

                        */

                    }
                );

            }
        );


    </script>

    <style>

        #map_container {
            height: 350px;
        }
    </style>