<style>
@import url('https://fonts.googleapis.com/css?family=PT+Serif');
#myModal .modal-content {
  height: auto;
  min-height: 100%;
  border-radius: 0;
}
	.discount_tr{
		font-weight: bold;
	}
	.discount_tr > td {
		border: none !important;
	}
	.speciel_deal_discount {
		background: #d43f3a;
		color: #fff;
		padding: 5px 5px ;
		border-radius: 3px;
		margin: 0 8px 0 0;
		box-shadow: 0 0 12px 0 rgba(212,63, 58, 0.50);
	}
	.data_row{
		width: 100%;
		background: #fff;
		overflow: hidden;
		padding: 14px 0px;
	}
	.price_col{
		font-weight: bold;
		color: rgb(40, 158, 105);
		margin-top: 7px;
	}
	.detail_col{
		padding:0px;
	}
	.detail_col .col-md-4{
		padding:0px;
	}
		
#accordian-acc{
	max-width:750px;
	margin:20px auto;
	position:relative;
}
#accordian-acc:after{
	display:block;
	clear:both;
	content:'';
}
.Journey-content{
	width:100%;
}
.MultiInfo{
    margin-top: 51px !important;
}

.JourneyInfo{
	float:left;
	width:80px;
}
.JourneyInfo span{
	color: #289e69;
	font-size: 18px;
	font-weight: 700;
	line-height: 24px;
	margin-right: 5px;
}
.MultiInfo span{
    font-size: 24px;
}
.TripInfo{
	display:inline-block;
	vertical-align:middle;
	width:80%;
}
.TripInfoIn{
	padding: 12px 0px;
}
.TripInfo .pic-holder{
	display:inline-block;
	vertical-align:middle;
	width: 30px;
}
.TripInfo .pic-holder img{
	width: 15px;
	height: 15px;
}
.company-name{
	display:inline-block;
	vertical-align:top;
	color:#8e9699;
	font-size:12px;
	line-height:16px;
	overflow: hidden;
	text-overflow: ellipsis;
	white-space: nowrap;
}
.TripInfoField_dateField{
	display:inline-block;
	vertical-align:middle;
	margin:0 0 0 10px;
}
.TripInfoField-time{
	font-size:13px;
	color:#30363d;
}
.TripInfoField-date{
	color: #8e9699;
	font-size: 12px;
	padding-top: 5px;
}
.TripInfoField_flightField{
	display:inline-block;
	vertical-align:middle;
	margin:0 0 0 10px;
}
.TripInfoField-flight-duration{
	font-size:13px;
	color:#30363d;
}
.TripInfoField-airport-codes{
	color: #8e9699;
	font-size: 12px;
	padding-top: 5px;
}
.Journey-content .fa-chevron-down{
	color: #8e9699;
	float:right;
	margin:7px 0 0;
}
#accordian-acc .panel-default > .panel-heading{
	padding:0;
	background:#fff;
}
#accordian-acc .panel-default > .panel-heading a{
	padding:25px 15px;
	display:block;
}
#accordian-acc .panel-default > .panel-heading a:focus,
#accordian-acc .panel-default > .panel-heading a:hover{
	text-decoration:none;
	outline:none;
}
#accordian-acc .panel-group .panel{
	border-radius:0;
	box-shadow:0 1px 3px rgba(0, 0, 0, 0.14);
	-moz-box-shadow:0 1px 3px rgba(0, 0, 0, 0.14);
	-webkit-box-shadow:0 1px 3px rgba(0, 0, 0, 0.14);
	transition:box-shadow 0.3s ease-out 0s;
}
#accordian-acc .panel-group .panel:hover,
#accordian-acc .panel-group .panel:focus{
	box-shadow:0 2px 7px 0 rgba(0, 0, 0, 0.32);
	-moz-box-shadow:0 2px 7px 0 rgba(0, 0, 0, 0.32);
	-webkit-box-shadow:0 2px 7px 0 rgba(0, 0, 0, 0.32);
}
#accordian-acc .panel-body{
	background:#fafbfc;
}
#accordian-acc {
	margin: -2px 0 -11px;
}
.TripDays{

}
.TripDays li{
	    display: inline-block;
    width: 127px;
    text-align: center;
    border: 1px dotted #ddd;
    margin: 0px 4px;
    font-size: 14px;
    font-weight: bold;
}
.TripDays li:nth-child(even){
	border: none;
}
.NewTripDetail{
	position:relative;
}
.MultiTrip{
    width: 90%;
    float: left;
    margin-right: 18px;
}

.NewTripDetail-title{
	position:relative;
	margin:0 0 20px;
}
.NewTripDetail-title h3{
	font-size:18px;
	color:#30363d;
	display:inline-block;
	vertical-align:middle;
	margin:0;
}
.NewTripDetail-title .txt-dep-min{
	display:inline-block;
	vertical-align:bottom;
	color:#79818a;
	font-size:12px;
	line-height:15px;
}
.Part {
	border-left: 4px solid #c0c8d1;
	padding-left: 20px;
	position: relative;
}
.Part:after{
	display:block;
	clear:both;
	content:'';
}
.Part-departure{
	border-bottom:1px dotted #ccc;
	margin:0 0 5px;
}
.Part-departure:after{





	display:block;
	clear:both;
	content:'';
}
.Part .Part-departure::before, .Part .Part-arrival::before {
	background-color: #c0c8d1;
	border-radius: 50%;
	content: "";
	display: block;
	height: 14px;
	left: -9px;
	position: absolute;
	width: 14px;
}
.Part .Part-departure::before {
	top: 0;
}
.Part-time{
	float:left;
}
.Part-place{
	float:right;
}
.Part-time time:first-child{
	font-weight:bold;
	font-size:14px;
	color:#000;
}
.Part-time time{
	color: #79818a;
	margin-left: 5px;
	font-size:13px;
}
.Part-place{
	font-size:12px;
	color:#30363d;
}
.Part-place span{
	color:#79818a;
}
.Part .Part-departure::before, .Part .Part-arrival::before {
    background-color: #c0c8d1;
    border-radius: 50%;
    content: "";
    display: block;
    height: 14px;
    left: -9px;
    position: absolute;
    width: 14px;
}
.Part .Part-arrival::before {
    bottom: 0;
}
.Part-com {
    left: -13px;
    position: absolute;
    top: 38px;
}
.Part-com img {
    background-color: #fff;
    height: 22px;
    width: 22px;
}
.Part-arrival{
	border-bottom:1px dotted #ccc;
	margin:0 0 15px;
}
.Part-arrival:after{
	display:block;
	clear:both;
	content:'';
}
.Part-body{
	padding:15px 0;
}
.Part-body-row{
	font-size:12px;
}
.NewJourneyDetail-bookingBtn{
	display:block;
	background:#0097a9;
	color:#fff;
	border:1px solid #007785;
	font-weight:bold;
	border-radius:3px;
	font-size:14px;
	padding:15px 12px;
	margin:15px 0 0;
	overflow: hidden;
}
.NewJourneyDetail-bookingBtn:hover,
.NewJourneyDetail-bookingBtn:focus{
	text-decoration:none;
	color:#fff;
	opacity:0.85;
}
.NewTripLayover {
    border-left: 2px dotted #c0c8d1;
    left: 1px;
    padding: 35px 0 35px 20px;
    position: relative;
    margin: 0 0 7px;
	font-size:12px;
}

input.slider-range-input{
	border: 0;
    margin-top: 10px;
    text-align: center;
    width: 100%;
}
.control-label.col-md-12.heading-style {
    background: #c3e6fc none repeat scroll 0 0;
    border-radius: 4px 4px 0 0;
    font-size: 17px;
    height: auto;
    margin: 1px 0 8px;
    padding: 7px;
	font-family: 'PT Serif', serif;
    font-weight: 700;

}
.heading-style {
    background: #c3e6fc none repeat scroll 0 0;
    border-radius: 4px 4px 0 0;
    font-size: 17px;
    height: auto;
    margin: 1px 0 8px;
    padding: 7px;
}
.sWidgets{
	padding:0 0 10px 0;
	margin: 0 0 6px;
}
..radio{
	padding-left: 18px; 
}
.radio > label {
    margin: 0 21px 0 -39px;
}
.radio > label .onewayreturnradio {
    position: relative;
    right: 3px;
    top: 1px;
}
.control-label.col-md-12 {
    margin: 6px 0;
}
.heading-style-sr {
    background: #c3e6fc none repeat scroll 0 0 !important;
    border-radius: 4px 4px 0 0;
    font-size: 17px !important;
    height: auto;
    margin: 1px 0 8px;
    padding: 10px !important;
    text-align: center;
    font-family: 'PT Serif', serif;
    font-weight: 700;
}
.panel{
	border:none !important;
}
.select-width{
	margin: -10px 0 0;
    width: 63%;
}
.btn-rest{
	background: red none repeat scroll 0 0;
    border: 1px solid red;
    color: #fff;
    width: 100px;
    display: block;
    text-align: center;
    margin:10px auto 0px;
    font-weight: 600;
    text-transform: capitalize;
}
.panel-title.bold{
	font-size: 17px !important;
	font-family: 'PT Serif', serif;
	font-weight: 700;
	
}
.smaller-font {
    font-size: 14px;
}
.tc1 {
    font-size: 17px;
}
.load_more {
    background: rgba(0, 0, 0, 0) linear-gradient(to bottom, #71d1ff 0%, #22b3f4 4%, #198fef 100%) repeat scroll 0 0;
    border: medium none;
    border-radius: 6px;
    box-shadow: 0 1px 3px rgba(0, 0, 0, 0.4);
    color: #fff;
    display: block;
    font-family: "FuturaStd-Book";
    font-size: 14px;
    margin: 0 200px 10px;
    padding: 10px 15px;
    text-shadow: 1px 1px 2px #000;
    text-transform: capitalize;
}
.Sorting {
    margin: 0.3rem 0 0.85rem;
    overflow: hidden;
}
.Sorting .Sorting-content {
    /*padding-left: 20px;
    padding-right: 20px;*/
    -moz-box-direction: normal;
    -moz-box-orient: horizontal;
    -moz-box-pack: start;
    box-sizing: border-box;
    display: flex;
    flex-direction: row;
    justify-content: flex-start;
    overflow-y: hidden;
}
.Sorting .Sorting-typeWrapper:first-child {
    padding-left: 0;
}
.Sorting .Sorting-typeWrapper {
    min-width: 168px;
    padding-bottom: 3px;
    padding-top: 1px;
    width: 33.33%;
}
.Sorting .Sorting-typeContent {
    -moz-box-direction: normal;
    -moz-box-flex: 1;
    -moz-box-orient: vertical;
    -moz-box-pack: justify;
    background-color: #f6f6f6;
    box-shadow: 0 1px 3px rgba(0, 0, 0, 0.14), 0 1px 0 #edeff2 inset;
    box-sizing: border-box;
    color: #79818a;
    cursor: pointer;
    display: flex;
    flex-direction: column;
    flex-grow: 1;
    height: 69px;
    justify-content: space-between;
    padding: 12px 18px 17px 21px;
}
.Sorting .Sorting-title {
    -moz-box-pack: justify;
    display: flex;
    font-size: 14px;
    justify-content: space-between;
    line-height: 20px;
}
.spTypo-medium {
    font-weight: 500;
}
.Sorting .Sorting-title {
    font-size: 14px;
    line-height: 20px;
}
.Sorting .Sorting-journeyInfo {
    display: flex;
    font-size: 12px;
}
.Sorting .Sorting-journeyInfo .Sorting-price {
    color: #30363d;
}
.spTypo-medium {
    font-weight: 500;
}
.Sorting .Sorting-journeyInfo .Sorting-duration {
    color: #79818a;
    margin-left: 10px;
}
.Sorting .Sorting-typeContent.Sorting-typeActive {
    background-color: #fff;
    color: #0097a9;
}
.search_filter_banner{
	width: 100%;
	padding: 15px;
	text-align: center;
	background-color:#c3e6fc; 
	height: 70px;
}
.search_filter_banner > p {
    margin: 0;
    text-transform: capitalize;
    text-shadow: 0 3px 17px #eee;
    font-family: Comic Sans MS;
     color: #666;
}
.slideUp {
    color: #666;
    text-transform: uppercase;
    font-family: Comic Sans MS;
}

</style>
<div class="width-row">
    <div class="clearfix">
    
        <div class="col-sm-4">
            <div class="sWidgets">
                <!--<form method="post" id="hotel_search_all_options_form"> -->
                <form action="<?php echo base_url(); ?>view_search_flights" method="get" id="flight_request">
                    
                    <div class="clearfix">
                        <div class="form-group">
                            <label class="control-label col-md-12 heading-style">Where are you flying?</label>
							<div class="col-md-12 text-center">
								<div class="radio">
									<label><input onclick="filterData();" class="onewayreturnradio" type="radio" name="optradio" value="1" <?php if(isset($searchData) && isset($searchData['optradio']) && $searchData['optradio'] == 1){ echo "checked";} ?>>One-way</label>
								</div>
								<div class="radio">
									<label><input class="onewayreturnradio" type="radio" name="optradio" value="2" <?php if(isset($searchData) && isset($searchData['optradio']) && $searchData['optradio'] == 2){ echo "checked";} ?>>Round-trip</label>
								</div>
							</div>
                            <div class="col-md-12">
								<label class="control-label col-md-12">From</label>
								<input type="text" <?php if(isset($searchData) && isset($searchData['Flight_from'])){ echo 'value="'.$searchData['Flight_from'].'"';} ?> name="Flight_from" id="listing_Flight_from" class="form-control" style="height: 33px; text-align: center;"  onfocus="this.placeholder = ''">
								<input type='hidden' name='Flight_fromID' id='listing_Flight_fromID' <?php if(isset($searchData) && isset($searchData['Flight_fromID'])){ echo 'value="'.$searchData['Flight_fromID'].'"';} ?>>
                            </div>
							<div class="col-md-12">
								<label class="control-label col-md-12">To</label>
								<input type="text" <?php if(isset($searchData) && isset($searchData['Flight_to'])){ echo 'value="'.$searchData['Flight_to'].'"';} ?> name="Flight_to" id="listing_Flight_to" class="form-control" style="height: 33px; text-align: center;"  onfocus="this.placeholder = ''">
								<input type='hidden' name='Flight_toID' id='listing_Flight_toID' <?php if(isset($searchData) && isset($searchData['Flight_toID'])){ echo 'value="'.$searchData['Flight_toID'].'"';} ?>>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix">
                        <div class="col-md-6">							
                            <div class="form-group">
                                <label class="control-label col-md-12">Departure</label>
                                <div class="">
									<input type="text" name="departure_date" <?php if(isset($searchData) && isset($searchData['departure_date'])){ echo 'value="'.$searchData['departure_date'].'"';} ?>   class="form-control icon-pick departure_date" id="listing_departure_date" style="text-align: left;">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-12">Return</label>
                                <div class="">
                                    <input type="text" name="return_date" <?php if(isset($searchData) && isset($searchData['return_date'])){ echo 'value="'.$searchData['return_date'].'"';} ?> class="form-control icon-pick return_date" id="listing_return_date" style="text-align: left;">
                                </div>
                            </div>
                        </div>
                        
						<div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-12">Stops</label>
                                <div class="">
                                    <select onchange="filterData();" id="maxstopovers" class="sort_hotel select2me form-control input-xlarge numberOfChild select2-offscreen" name="maxstopovers" style="text-align: center;" tabindex="-1">
										<option value="" <?php if(!isset($searchData)){ echo "selected";} ?>>Any</option>
										<option value="0" <?php if(isset($searchData) && isset($searchData['maxstopovers'])){ if($searchData['maxstopovers'] == 0)echo "selected";} ?>>Nonstop (direct flights)</option>
										<option value="1" <?php if(isset($searchData) && isset($searchData['maxstopovers'])){ if($searchData['maxstopovers'] == 1)echo "selected";} ?>>Up to 1 stop</option>
										<option value="2" <?php if(isset($searchData) && isset($searchData['maxstopovers'])){ if($searchData['maxstopovers'] == 2)echo "selected";} ?>>Up to 2 stops</option>
									</select>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-12">Passengers</label>
                                <div class="">
                                    <select onchange="filterData();" id="pnum" class="select2me form-control input-xlarge  select2-offscreen" name="pnum" style="text-align: center;" tabindex="-1">
										<?php for($i=1; $i<=9; $i++){?>
                                        <option value="<?php echo $i;?>" ><?php echo $i;?></option>
                                        <?php }?>
                                        
									</select>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <?php /*<div class="clearfix">
                        <div class="col-xs-2" style="margin-top:20px;float:right;margin-right:50px;">
                            <button type="submit" id="search_hotel_with_all_options" class="btn .btn-custom-m .btn-custom search_hotel_with_all_options" style="background: #009688;border: 1px solid #009688;color:  #fff;">Search</button>
                        </div>
                    </div> */?><!-- clearfix -->
                    
                </form>
            </div>
            
            <div class="panel panel-default cbg">
                <div class="panel-heading heading-style-sr">
                    <h3 class="panel-title bold">Filter Results</h3>
                </div>
                <div class="panel-body">
                    <div id="LocationAndLandmarks">
                        <div id="locationsLandmarksFilterContent">
							
							<div class="clearfix">
                                <h4 class="tc1">Price range</h4>
                                <div class="col-md-12">
                                	<div class="col-md-12" id="price-range-slider"></div>
                                    <input type="text" id="price_range" readonly value="" class="slider-range-input" />
                                </div>
                            </div>
                            
                            <div class="clearfix">
                                <h4 class="tc1">Take-off <span class="smaller-font">(Departure)</span></h4>
                                <div class="col-md-12">
                                	<div class="col-md-12" id="dtime-range-slider"></div>
                                    <input type="text" id="dtime_range" readonly value="" class="slider-range-input">
                                </div>
                            </div>
                            
                            <div class="clearfix">
                                <h4 class="tc1">Landing <span class="smaller-font">(Departure)</span></h4>
                                <div class="col-md-12">
                                	<div class="col-md-12" id="atime-range-slider"></div>
                                    <input type="text" id="atime_range" readonly value="" class="slider-range-input">
                                </div>
                            </div>
                            
                            <div class="clearfix return_filters">
                                <h4 class="tc1">Take-off <span class="smaller-font">(Return)</span></h4>
                                <div class="col-md-12">
                                	<div class="col-md-12" id="ret-dtime-range-slider"></div>
                                    <input type="text" id="ret_dtime_range" readonly value="" class="slider-range-input">
                                </div>
                            </div>
                            
                            <div class="clearfix return_filters">
                                <h4 class="tc1">Landing <span class="smaller-font">(Return)</span></h4>
                                <div class="col-md-12">
                                	<div class="col-md-12" id="ret-atime-range-slider"></div>
                                    <input type="text" id="ret_atime_range" readonly value="" class="slider-range-input">
                                </div>
                            </div>

                            <div class="clearfix">
                            	<div class="col-md-12">
                            		<a href="javascript:;" onclick="reset_filters();" class="btn btn-rest">reset</a>
                            	</div>
                            </div>

                        </div>
                    </div>
                </div>
			</div>
            
            
	        <?php /*?><div>
                <div class="col-md-12">
                    <div class="col-md-12">
                        <label class="col-md-5" for="price_range">Price:</label>
                        <input class="col-md-7" type="text" id="price_range" readonly value="" style="border:0; background-color:#c3e6fc;" />
                    </div>
                    <div class="col-md-12" id="price-range-slider"></div>
                </div>
                <div class="clearfix"></div>
                
                <div class="col-md-12">
                    <div class="col-md-12">
                        <label class="col-md-5" for="dtime_range">Take-off:</label>
                        <input class="col-md-7" type="text" id="dtime_range" readonly value="" style="border:0; background-color:#c3e6fc;">
                    </div>
                    <div class="col-md-12" id="dtime-range-slider"></div>
                </div>
                <div class="clearfix"></div>
                
                <div class="col-md-12">
                    <div class="col-md-12">
                        <label class="col-md-5"  for="atime_range">Landing:</label>
                        <input class="col-md-7"  type="text" id="atime_range" readonly value="" style="border:0; background-color:#c3e6fc;">
                    </div>
                    <div class="col-md-12" id="atime-range-slider"></div>
                </div>
            </div><?php */?>
            
        </div>
        
        <div class="col-md-8 col-sm-8">
            <!-- <div class="hotel_serch_tab_btn active">
                <a href="#">Flight search result</a>
            </div> -->
		</div>
        
		<div class="col-md-8 col-sm-8">
            <div class="row">
                <!-- <div class="search_result_filter" style="min-height: 55px;">
                    <div style="" id="total_hotels_found_id"></div>
                    
                    <div class="col-md-4" style="text-align: right; margin-top: 18px;">
                        <p>Sort by :</p>
                    </div>
                    
	                <div class="col-md-8">
	                    <div class="form-group">
	                        <div class="select-width">

	                            <div class=" select2-container select2me form-control input-xlarge numberOfChild" id="s2id_numberOfChild" style="text-align: center;">
									<input class="select2-focusser select2-offscreen" type="text" id="s2id_autogen3">
	                            </div>
	                            
	                            <select onchange="filterData();" class="sort_hotel select2me form-control input-xlarge numberOfChild select2-offscreen" name="sortby" id="sortby" style="text-align: center;" tabindex="-1">
	                                <option value="price_1">Price ( Low to high )</option>
	                                <option value="price_0">Price ( High to low )</option>
	                                <option value="quality_1">Recommended ( Low to high )</option>
	                                <option value="quality_0">Recommended ( High to low )</option>
	                                <option value="duration_1">Duration ( Low to high )</option>
	                                <option value="duration_0">Duration ( High to low )</option>
	                            </select>
	                            
	                        </div>
	                    </div>
	                </div>    
                </div> -->
                <div class="search_filter_banner">
                	<p>What makes our booking platform different from others?</p>
                	<a href="#" class=""  data-toggle="modal" data-target="">click here to learn more!</a>
                </div>
                <div class="Sorting">
                	<input type="hidden" name="sortby" id="sortby" value="price_1" />
	                <div class="Sorting-content">
		                <div class="Sorting-typeWrapper">
                            <div class="Sorting-typeContent">
                            	<a href="javascript:;" onclick="updateSort(this, 'quality_0');">
                                    <span class="Sorting-title spTypo-medium">
                                        <span class="">Recommended</span>
                                        <i class="fa fa-thumbs-up"></i>
                                    </span>
                                    <div class="Sorting-journeyInfo">
                                        <span class="Sorting-price spTypo-medium">$ 75</span><span class="Sorting-duration">2h 58m</span>
                                    </div>
                                </a>
                            </div>
			            </div>
                        
		                <div class="Sorting-typeWrapper">
							<div class="Sorting-typeContent Sorting-typeActive">
                                <a href="javascript:;" onclick="updateSort(this, 'price_1');">
                                    <span class="Sorting-title spTypo-medium">
                                        <span class="">Cheapest</span>
                                        <i class="fa fa-usd"></i>
                                    </span>
                                    <div class="Sorting-journeyInfo">
                                        <span class="Sorting-price spTypo-medium">$ 75</span><span class="Sorting-duration">3h 03m</span>
                                    </div>
                                </a>
							</div>
		                </div>
                        
		                <div class="Sorting-typeWrapper">
                            <div class="Sorting-typeContent">
                            	<a href="javascript:;" onclick="updateSort(this, 'duration_1');">
                                    <span class="Sorting-title spTypo-medium">
                                        <span class="">Shortest</span>
                                        <i class="fa fa-clock-o"></i>
                                    </span>
                                    <div class="Sorting-journeyInfo">
                                        <span class="Sorting-price spTypo-medium">$ 128</span><span class="Sorting-duration">2h 31m</span>
                                    </div>
                                </a>
                            </div>
                            
		                </div>
	                </div>
                </div>
            </div>
            
            <div id="content" class="row">
		<?php 
			if($total_flights<=0){ 
				//die('0');
				echo '<div>No flights found.</div>';
			}
			
			if($checkReturn){
		
				$counter = ($page_no-1) * $pagination_limit;
				
				foreach($flights as $flight){
					
					$nightsflag = true;
					$dtime = explode(',', $flight['dTime']);
					$atime = explode(',', $flight['aTime']);
					
					$retdtime = explode(',', $flight['retdTime']);
					$retatime = explode(',', $flight['retaTime']);
		?>
				<div id="accordian-acc">
					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingOne">
								<div class="accordion-heading">
									<h4 class="panel-title">
										<a class="accordion-toggle uncollapsebtn collapsed" data-toggle="collapse" data-parent="#accordion2" data-target="#collapseID<?php echo $counter;?>">
											<div class="Journey-content">
												<div class="JourneyInfo MultiInfo">
													<span class="">$ <?php echo $flight['price'];?></span>
												</div>
												<div class="TripInfo">
													
                                                    <div class="TripInfoIn">
                                                        <div class="pic-holder">
                                                        	<?php foreach($flight['departure_airlines'] as $aline){?>
                                                        	<img src="https://images.kiwi.com/airlines/32/<?php echo ( isset($airlines[$aline]) ? $airlines[$aline]['logo'] : $aline);?>.png" alt="" height="20px" width="20px">
                                                            <?php }?>
                                                        </div>
                                                        <div class="company-name">
                                                        	<?php
															$lctr = 0; 
																$final_string = '';
																foreach($flight['departure_airlines'] as $aline){
																	if($lctr > 0){
																		$final_string .= ', ';
																	}
																	$final_string .= ( isset($airlines[$aline]) ? $airlines[$aline]['name'] : $aline);
                                                            		$lctr++;
                                                             	}
															?>
                                                            <span title="<?php echo $final_string;?>">
                                                            	<?php
																echo substr($final_string,0, 15);
																if($final_string > 15){
																	echo '...';
																}?>
                                                            </span>
                                                        </div>
                                                        <div class="TripInfoField_dateField">
                                                            <div class="TripInfoField-time">
                                                                <time class="" datetime="2016-11-11T23:00:00+00:00"><?php echo $dtime[1];?></time>					-					
                                                                <time class="" datetime="2016-11-11T23:50:00+00:00"><?php echo $atime[1];?></time>
                                                            </div>
                                                            <div class="TripInfoField-date">
                                                                <time class="" datetime="2016-11-11T23:00:00+00:00"><?php echo $dtime[0];?></time>
                                                            </div>
                                                        </div>
                                                        <div class="TripInfoField_flightField">
                                                            <div class="TripInfoField-flight-duration">
                                                                <?php echo $flight['fly_duration'];?>
                                                            </div>
                                                            <div class="TripInfoField-airport-codes">
                                                                <span class="name-and-code">
                                                                    <?php echo $flight['cityFrom']?> ( <?php echo $flight['flyFrom']?> )
                                                                </span>
                                                                <span class="flight-arrow">
                                                                    <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                                                                </span>
                                                                <span class="name-and-code">

                                                                    <?php echo $flight['cityTo']?> ( <?php echo $flight['flyTo']?> )
                                                                </span>
                                                            </div>
                                                        </div>
													</div>
                                                    
                                                    <div class="TripDays">
                                                        <ul>
                                                            <li></li>
                                                            <li>
                                                                <?php echo $flight['nightsInDest'];?> 
                                                                Night<?php echo ($flight['nightsInDest'] > 1) ? 's' : '';?> In 
                                                                <?php echo $flight['cityTo'];?>
                                                            </li>																<li></li>
                                                        </ul>
                                                    </div>
                                                    
                                                    <div class="TripInfoIn">
                                                        <div class="pic-holder">
                                                        	<?php foreach($flight['return_airlines'] as $aline){?>
	                                                        	<img src="https://images.kiwi.com/airlines/32/<?php echo (isset($airlines[$aline]) ? $airlines[$aline]['logo'] : $aline );?>.png" alt="" height="20px" width="20px">
                                                            <?php }?>
                                                        </div>
                                                        <div class="company-name">
                                                        	<?php
															$lctr = 0; 
																$final_string = '';
																foreach($flight['return_airlines'] as $aline){
																	if($lctr > 0){
																		$final_string .= ', ';
																	}
																	$final_string .= ( isset($airlines[$aline]) ? $airlines[$aline]['name'] : $aline);
                                                            		$lctr++;
                                                             	}
															?>
                                                            <span title="<?php echo $final_string;?>">
                                                            	<?php
																echo substr($final_string,0, 15);
																if($final_string > 15){
																	echo '...';
																}?>
                                                            </span>
                                                        </div>
                                                        <div class="TripInfoField_dateField">
                                                            <div class="TripInfoField-time">
                                                                <time class="" datetime="2016-11-11T23:00:00+00:00"><?php echo $retdtime[1];?></time>					-					
                                                                <time class="" datetime="2016-11-11T23:50:00+00:00"><?php echo $retatime[1];?></time>
                                                            </div>
                                                            <div class="TripInfoField-date">
                                                                <time class="" datetime="2016-11-11T23:00:00+00:00"><?php echo $retdtime[0];?></time>
                                                            </div>
                                                        </div>
                                                        <div class="TripInfoField_flightField">
                                                            <div class="TripInfoField-flight-duration">
                                                                <?php echo $flight['return_duration'];?>
                                                            </div>
                                                            <div class="TripInfoField-airport-codes">
                                                                <span class="name-and-code">
                                                                    <?php echo $flight['cityTo']?> ( <?php echo $flight['flyTo']?> )
                                                                </span>
                                                                <span class="flight-arrow">
                                                                    <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                                                                </span>
                                                                <span class="name-and-code">
                                                                    <?php echo $flight['cityFrom']?> ( <?php echo $flight['flyFrom']?> )
                                                                </span>
                                                            </div>
                                                        </div>
													</div>    
		
												</div>
												<i class="fa fa-chevron-down MultiInfo" aria-hidden="true"></i>
											</div>
										</a>
									</h4>
								</div>
							</div>
									
							<div id="collapseID<?php echo $counter;?>" class="accordion-body allcollapse collapse" style="height: 0px;">
								<div class="panel-body">
                                	<div class="col-md-6">
                                    	<div class="NewTripDetail MultiTrip departure">
                                        
                                            <div class="NewTripDetail-title">
                                                <h3>Departure</h3>
                                                <span class="txt-dep-min"><?php echo $flight['fly_duration'];?></span>
                                            </div>
								<?php
									$switchFlag = false; 
									$prev_atime = 0;
									foreach($flight['route'] as $route){
										
										$timediff = abs($route->dTimeUTC - $prev_atime);
										$overlay_hours = floor($timediff / 3600);
										
										$prev_atime = $route->aTimeUTC;
										
										$overlay_time = '';
										if($overlay_hours > 0){
											$overlay_time .= $overlay_hours.'h ';
										}
										
										$overlay_minutes = floor(($timediff / 60) % 60);
										if($overlay_minutes > 0){
											$overlay_time .= $overlay_minutes.'m';
										}
										
										$dtime = explode(',', $route->dTimeFormated);
										$atime = explode(',', $route->aTimeFormated);
										
										$first_return = false;
										if( !$switchFlag && $route->return ){
											$first_return = true;
											echo '		</div>
													</div>
													
													<div class="col-md-6">
														<div class="NewTripDetail MultiTrip departure">
															<div class="NewTripDetail-title">
																<h3>Return</h3>
																<span class="txt-dep-min">'.$flight['return_duration'].'</span>
															</div>
												';
											$switchFlag = true;
										}
										
								?>
                                			<?php if( !$first_return && ($route->bags_recheck || $flight['guarantee']) ){?>
                                			<div  class="NewTripLayover">
                                            	<?php if($route->bags_recheck ){?>
                                                    <i class="fa fa-clock-o"></i> Layover in <?php echo $route->cityFrom?> for <?php echo $overlay_time;?>.
                                                    <div><i class="fa fa-briefcase"></i> Collect and recheck your baggage.</div>
                                                <?php }?>
                                                <?php if($flight['guarantee']){?>
                                                	<div><i class="fa fa-briefcase"></i> <a href="javascript:;" data-toggle="modal" data-target="#myModal" >kiwi.com guarantee</a></div>
                                                <?php }?>
											</div>
                                                
                                            <?php }?>
                                            <div class="Part">
                                                <div class="Part-departure">
                                                    <div class="Part-time">
                                                        <time class="" datetime="2016-11-11T23:00:00+00:00"><?php echo $dtime[1];?></time>
                                                        <time class="" datetime="2016-11-11T23:00:00+00:00"><?php echo $dtime[0];?></time>
                                                    </div>
                                                    <div class="Part-place">
                                                        <?php echo $route->cityFrom?> <span><?php echo $route->flyFrom?></span>
                                                    </div>
                                                </div>
                                                <div class="Part-com">
                                                    <img src="https://images.kiwi.com/airlines/32/<?php echo ( isset($airlines[$route->airline]) ? $airlines[$route->airline]['logo'] : $route->airline);?>.png" alt="" height="22" width="22">
                                                    </div>
                                                    <div class="Part-body">
                                                        <div class="Part-body-row">
                                                            <time class="" datetime="55:00Z"><?php echo $route->bags_recheck_required;?></time>
                                                        </div>
                                                    </div>
                                                    <div class="Part-arrival">
                                                        <div class="Part-time">
                                                            <time class="" datetime="2016-11-11T23:00:00+00:00"><?php echo $atime[1];?></time>
                                                            <time class="" datetime="2016-11-11T23:00:00+00:00"><?php echo $atime[0];?></time>
                                                        </div>
                                                        <div class="Part-place">
                                                            <?php echo $route->cityTo?> <span><?php echo $route->flyTo?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                    
								<?php }?>
                                		</div>
									</div>
                                    <div class="clearfix"></div>
                                	
									<a target="_blank" href="https://www.kiwi.com/us/booking?passengers=1&price=<?php echo $flight['price'];?>&token=<?php echo $flight['booking_token'];?>" class="NewJourneyDetail-bookingBtn" >Book this flight for $ <?php echo $flight['price'];?></a>
									
								</div>
							</div>
						
						</div>
					</div>
				</div>
		<?php	
					$counter++;
				}
		
			}else{
				$counter = ($page_no-1) * $pagination_limit;
				foreach($flights as $flight){
					$dtime = explode(',', $flight['route'][0]->dTimeFormated);
					$atime = explode(',', $flight['route'][0]->aTimeFormated);
		?>
				<div id="accordian-acc">
				
					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingOne">
								<div class="accordion-heading">
									<h4 class="panel-title">
										<a class="accordion-toggle uncollapsebtn collapsed" data-toggle="collapse" data-parent="#accordion2" data-target="#collapseID<?php echo $counter;?>">						
											<div class="Journey-content">
												<div class="JourneyInfo">
													<span class="">$ <?php echo $flight['price'];?></span>
												</div>
												<div class="TripInfo">
													<div class="pic-holder">
		<img src="https://images.kiwi.com/airlines/32/<?php echo ( isset($airlines[$flight['route'][0]->airline]) ? $airlines[$flight['route'][0]->airline]['logo'] : $flight['route'][0]->airline); ?>.png" alt="" height="20px" width="20px">
													</div>
													<div class="company-name">
														<span><?php echo ( isset($airlines[$flight['route'][0]->airline]) ? $airlines[$flight['route'][0]->airline]['name'] : $flight['route'][0]->airline); ?></span>
													</div>
													<div class="TripInfoField_dateField">
														<div class="TripInfoField-time">
															<time class="" datetime="2016-11-11T23:00:00+00:00"><?php echo $dtime[1];?></time>
															<time class="" datetime="2016-11-11T23:50:00+00:00"><?php echo $atime[1];?></time>
														</div>
														<div class="TripInfoField-date">
															<time class="" datetime="2016-11-11T23:00:00+00:00"><?php echo $dtime[0];?></time>
														</div>
													</div>
														   
													<div class="TripInfoField_flightField">
														<div class="TripInfoField-flight-duration"><?php echo $flight['fly_duration']?></div>	
														<div class="TripInfoField-airport-codes">
															<span class="name-and-code">
																<?php echo $flight['route'][0]->cityFrom?> ( <?php echo $flight['route'][0]->flyFrom?> )
															</span>
															<span class="flight-arrow">
																<i class="fa fa-long-arrow-right" aria-hidden="true"></i>
															</span>										
															<span class="name-and-code">
																<?php echo $flight['route'][0]->cityTo?> ( <?php echo $flight['route'][0]->flyTo?> )
															</span>									
														</div>    
													</div>						
												</div>
												
												<i class="fa fa-chevron-down" aria-hidden="true"></i>
											</div>
										</a>
									</h4>
								</div>
							</div>
							
							<div id="collapseID<?php echo $counter;?>" class="accordion-body allcollapse collapse" style="height: auto;">
								<div class="panel-body">					
									<div class="NewTripDetail">						
									
										<div class="NewTripDetail-title">							
											<h3>Departure</h3>							
											<span class="txt-dep-min"><?php echo $flight['fly_duration'];?></span>						
										</div>						
										
								<?php	foreach($flight['route'] as $route){
											$dtime = explode(',', $route->dTimeFormated);
											$atime = explode(',', $route->aTimeFormated);?>
											
											<div class="Part">							
												<div class="Part-departure">								
													<div class="Part-time">									
														<time class="" datetime="2016-11-11T23:00:00+00:00"><?php echo $dtime[1];?></time>									
														<time class="" datetime="2016-11-11T23:00:00+00:00"><?php echo $dtime[0];?></time>								
													</div>								
													<div class="Part-place">
														<?php echo $route->cityFrom?> <span><?php echo $route->flyFrom?></span>
													</div>
												</div>							
											
												<div class="Part-com">								
													<img src="https://images.kiwi.com/airlines/32/<?php echo ( isset($airlines[$route->airline]) ? $airlines[$route->airline]['logo'] : $route->airline);?>.png" alt="" height="22" width="22">
												</div>							
											
												<div class="Part-body">								
													<div class="Part-body-row">									
														<time class="" datetime="55:00Z"><?php echo $route->bags_recheck_required?></time>								
													</div>							
												</div>							
											
												<div class="Part-arrival">								
													<div class="Part-time">									
														<time class="" datetime="2016-11-11T23:00:00+00:00"><?php echo $atime[1];?></time>									
														<time class="" datetime="2016-11-11T23:00:00+00:00"><?php echo $atime[0];?></time>								
													</div>								
													<div class="Part-place">
														<?php echo $route->cityTo?> <span><?php echo $route->flyTo?></span>
													</div>							
												</div>						
											</div>	
								<?php	}?>                    
										                                    				
										<a target="_blank" href="https://www.kiwi.com/us/booking?passengers=1&price=<?php echo $flight['price'];?>&token=<?php echo $flight['booking_token'];?>" class="NewJourneyDetail-bookingBtn">Book this flight for $ <?php echo $flight['price'];?></a>
									</div>				  
								</div>				
							</div>			
							
						</div>
					</div>
				
				</div>
		<?php	
					$counter++;
				}
			}
		?>
			</div>
            
            <div class="row" id="loadingData" style="display:none">Loading...</div>
            <a href="javascript:;" class="btn load_more" id="load_more" onclick="get_flights_data();">Load More flights</a>
        </div>
       
        <!-- clearfix -->
	</div>
</div>

<div class="loader"  style="display: none">
    <div class="center">
        <img alt="" src="<?php echo base_url(); ?>public/spin.gif" />
    </div>
</div>


<div id="myModal" class="modal fade">
<div class="modal-dialog modal-lg">
    <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Kiwi.com Guarantee</h4>
            </div>
            <div class="modal-body">
                <div class="spCard spShadow-half" data-reactid="45">
                	<span class="" data-reactid="46">
                    <p>In order to bring you the best possible service , we offer our customers an exclusive and unique flight change, delay and cancellation policy, called the Kiwi.com Guarantee (or Assistant Services).<br>The aim and scope of our Kiwi.com Guarantee service program is to make your journey as smooth and comfortable as possible. To further this purpose, we ensure you that in situations when your flight(s) is (are) rescheduled, delayed or cancelled, we shall upon activation of the Kiwi.com Guarantee, offer you a reasonable alternative flight(s) or other means of transportation to your destination, or a refund of the price you paid for any unused flight(s).</p>
					<p>However, please note that the Kiwi.com Guarantee may only apply in cases when your flight(s) is (are) rescheduled, delayed or cancelled due to factors, which can occur in the standard course of air transportation. As such the Kiwi.com Guarantee can be applied limitedly in situations of the impact of force majeure. Furthermore, the Kiwi.com Guarantee cannot be applied to cases where you make any changes to the flight schedule(s) without our prior approval.<br>The Kiwi.com Guarantee is split into two categories, based on their severity:</p>
                    <strong>Scenario A (More than 48 hours before departure)</strong>
					<div>
      					<p>More than 48 hours before departure – The Kiwi.com Guarantee applies only to a change or cancellation of any flight(s) announced more than 48 hours prior to the departure of your first concerned flight, which may negatively impact your ability to reach your destination, or your arrival to a destination is postponed by more than 24 hours after the original scheduled arrival, i.e. due to a flight change(s) or cancellation(s) you would either miss your flight connection(s) or your flight(s) would be cancelled.</p>
                        <p>In this case, if you learn about a change or cancellation of your flight(s) under Scenario A you must inform us of such change or cancellation without undue delay either by telephone, e‐mail or via the online chat service available on our website. Should you fail to inform us about such a flight change(s) or cancellation(s) without undue delay, you will not be entitled to the Kiwi.com Guarantee. In the case we are informed first about the change or cancellation of your flight(s) according to Scenario A, we will contact you within a reasonable time after we learn about such a flight change(s) or cancellation(s). After we are informed about the flight change(s) or cancellation(s) according to Scenario A, we will offer you one of the following solutions, solely at our discretion:</p>
                        <ul class="list">
                        	<li class="list--item __indented __bulleted">We will search for alternative transportation to your destination. Should we find a reasonable alternative we will offer you an alternative flight(s) or other means of transportation to your destination entirely at our expense. In case we are unable to buy the offered alternative flight(s), or other means of transportation for you online, you can buy the respective ticket(s) for the alternative transportation, as agreed upon both by you and us, at the airport and we will refund you the price of any such ticket(s). Please note, that any extra services or further upgrades to the agreed upon alternative ticket(s) will not be covered by us.</li>
                            <li class="list--item __indented __bulleted">We will offer you a refund of the price you paid for all the unused flights en route to your destination: this solution is most likely to be offered in the case that we are unable to offer you a reasonable alternative transportation.</li>
					</ul>
                    
					<p>You are obliged to reply to us promptly after receiving information about the offered solution(s), but in all cases, at the latest within 24 hours of sending our first notice offering alternative option(s) to you, or within any shorter reasonable time before the time of the scheduled departure of the first following flight. After the lapse of this 24-hour term given for a reply, this offer is null and void, thus you will subsequently not be entitled to the Kiwi.com Guarantee.</p>
				</div>
                <strong>Scenario B (Less than 48 hours before departure)</strong>
                <div>
                	<p>Less than 48 hours before departure ‐ applies only to a flight delay(s) or a cancellation(s) of a flight(s) announced less than 48 hours prior to the departure of the first concerned flight, which may negatively impact your ability to reach your destination, or if your arrival to the destination is postponed by more than 24 hours after the original scheduled arrival, i.e. due to a flight delay(s) or cancellation(s) you would either miss your flight connection(s) or your flight(s) would be cancelled.</p>
                    <p>In this case, if you learn about a flight delay(s) or cancellation(s) of your flight under Scenario B, you are obliged to inform us of such a flight delay(s) or cancellation(s) without undue delay, either by telephone or via the online chat service available on our website. Should you fail to inform us about such a flight delay(s) or cancellation(s) without undue delay, you will not be entitled to the Kiwi.com Guarantee. In the case that we are informed first about the flight delay(s) or cancellation(s) of your flight under Scenario B, we will contact you within a reasonable time, after we learn of it.</p>
                    <p>After we are informed about the flight delay(s) or cancellation(s) under Scenario B, you may select from one of the following solutions at your discretion:</p>
					<ul class="list">
                    	<li class="list--item __indented __bulleted">We will search for an alternative transportation to your destination and should we find a reasonable alternative, we will offer you an alternative flight(s) or other means of transportation to your destination entirely at our expense. In the case that we are unable to buy the offered alternative flight(s) or other means of transportation for you online, you can buy the respective ticket(s), as agreed upon by both you and us, at the airport and we will refund you the price of the respective ticket(s). In the case that we are unable to find a reasonable alternative flight(s) or other means of transportation, due to a disproportionate price difference between the potential alternative transportation and the original price for the unused flights, we may agree with you on our proportional contribution to the costs associated with a mutually agreed upon alternative transportation. This option will be determined on a case by case basis. Please note that any extra services or upgrades to the agreed upon alternative ticket(s) will not be covered by us. In exceptional cases when your flight(s) is cancelled or you miss the flight connection(s) due to a flight delay and you are unable to contact us in this matter, you may purchase the ticket(s) for the alternative transportation without prior agreement between you and us and we will refund you the price of such ticket(s) for alternative transportation up to twice the original price of the unused flight(s) en route to your destination. Please note that you are entitled to this refund only after you provide us with sufficient written proof of your purchase of the ticket(s) for the alternative transportation to your destination along with the proof of the original flight delay or cancellation of your original flight.</li>
                        <li class="list--item __indented __bulleted">We will refund you the price you paid for all the unused flights en route to your destination. You should opt for this solution in the case that we are unable to offer you a reasonable alternative transportation. You are obliged to reply to us of your choice promptly after receiving our information about the offered solution, but in all cases at the latest within 24 hours of sending our notice, or within any shorter reasonable time before the time of the scheduled departure of the first following flight. After the lapse of this 24 hour term for replies the offer expires and you are no longer to the Kiwi.com Guarantee.</li>
					</ul>
				</div>
                <p><em>Multi‐destination routes</em> - Should there be a flight delay, change or cancellation of the flight(s) that will impact all of your flights on one section of your journey, we will try to find an alternative route to your interim destination. Should this not be possible, and you cannot begin your journey and have not taken any of the booked flights within your route, then a refund for all flights on that segment of the journey can be applied.</p>
                <p><em>Communication and notices</em> - All responses to any offer of an alternative must be received by us no later than 48 hours after the official and marked time of sending said notices. Should you fail to respond to such notices, or your response is not made within the aforementioned 48 hour time period, please be aware that your claim to the Kiwi.com Guarantee is no longer valid.</p>
                <p><em>Kiwi.com Additional Services</em> - The services specified below may apply when Scenario B occurs within our Kiwi.com Guarantee. These services can be activated by our Customer Support department when you contact them due to a flight delay or cancellation. However please note that you have no legitimate claim for the provision of these additional services because any of these services are solely at our discretion and need to be pre‐agreed once you contact our Customer Services department. Please note as well that in the case of the activation of these additional services you will need to cover the expenses for utilization of these additional services and provide us with a receipt(s) as proof of your expenses. We will refund your costs on the basis of such receipts up to the amount specified below.</p>
                <ul class="list">
                	<li class="list--item __indented">
                    	<em>Overnight Accommodation</em> ‐ This option may become available to you if your flight has been delayed or cancelled at short notice, leaving you stranded at the airport for the night and your alternative flight connects after midnight (00:00) and you would need to stay at the airport for more than 8 hours. Accommodation vouchers can also be granted if you have accepted a change offered by us for a flight departing the next day. It may be possible to book a hotel room and claim a total reimbursement of up to 50€ per passenger, regardless of the number of nights.</li>
					<li class="list--item __indented">
                    	<em>Alternative Transport</em> – Should we be unable to book you a flight connection because a) all reasonable flights are sold out and/or b) no such journey exists for the day in question, we may look at other airports in the area for alternative flight options. Should this occur, and under the condition that all costs are pre‐agreed through our Customer Support department, we may contribute towards your cost of the chosen method of transportation for the transfer between airports. This may include train, coach or taxi and will include all passengers booked on your itinerary through us. The maximum upper limit for this alternative transport will be 100€; inclusive of all passengers.</li>
                        <li class="list--item __indented">
                        	<em>Meal &amp; Beverage Compensation</em> ‐ Should your flight connection be delayed by more than 4 hours, we are willing to cover the cost of refreshments, up to a total of €10 per passenger booked on your itinerary through us. </li>
					</ul>
                    <p>Should you be offered any of the aforementioned alternatives by the airline and/or airport, we are absolved of all responsibility to further compensate and/or reimburse you.</p>
  </span>
				</div>
			</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
    </div>
</div>
<!-- Modal -->
<div id="search_filter-popup" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <p>Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<script src="<?php echo base_url(); ?>public/assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
	$(".search_filter_banner > a").delay(3000).addClass("slideUp");
});

var current_page = 1;
var start_date_pt = '<?php echo date('F d, Y H:i:s'); ?>';
var endPagination = false;
var reset_flag = false;

function reset_filters(){
	reset_flag = true;
	current_page = 0;
	endPagination = false;

	$('#departure_date').val("<?php echo $searchData['departure_date']; ?>");
	$('#return_date').val("<?php echo $searchData['return_date']; ?>");

	$('#listing_Flight_from').val("<?php if(isset($searchData) && isset($searchData['Flight_from'])){ echo $searchData['Flight_from'];} ?>");
	$('#listing_Flight_fromID').val("<?php if(isset($searchData) && isset($searchData['Flight_fromID'])){ echo $searchData['Flight_fromID'];} ?>");
	$('#listing_Flight_to').val("<?php if(isset($searchData) && isset($searchData['Flight_to'])){ echo $searchData['Flight_to'];} ?>");
	$('#listing_Flight_toID').val("<?php if(isset($searchData) && isset($searchData['Flight_toID'])){ echo $searchData['Flight_toID'];} ?>");

	$('#pnum').val(1);
	$('#maxstopovers').val("");

	$("#atime-range-slider").slider("values", 0, 0);
	$("#atime-range-slider").slider("values", 1, 24);
	$( "#atime_range" ).val( "00:00 to 23:59");

	$("#dtime-range-slider").slider("values", 0, 0);
	$("#dtime-range-slider").slider("values", 1, 24);
	$( "#dtime_range" ).val( "00:00 to 23:59");

	$("#ret-atime-range-slider").slider("values", 0, 0);
	$("#ret-atime-range-slider").slider("values", 1, 24);
	$( "#ret_atime_range" ).val( "00:00 to 23:59");
	
	$("#ret-dtime-range-slider").slider("values", 0, 0);
	$("#ret-dtime-range-slider").slider("values", 1, 24);
	$( "#ret_dtime_range" ).val( "00:00 to 23:59");

	$("#price-range-slider").slider("values", 0, 0);
	$("#price-range-slider").slider("values", 1, 9999);
	$( "#price_range" ).val( "$0 - $9999");

	filterData();
	reset_flag = false;
}

function filterData(){
	current_page = 0;
	endPagination = false;
	$('#content').html('');
	$('.loader').show();
	get_flights_data();
}

function get_flights_data(){

	$('#loadingData').show();
	
	var optradio = $('.onewayreturnradio:checked').val();
	var Flight_from = $('#listing_Flight_from').val();
	var Flight_fromID = $('#listing_Flight_fromID').val();
	var Flight_to = $('#listing_Flight_to').val();
	var Flight_toID = $('#listing_Flight_toID').val();
	var departure_date = $('#listing_departure_date').val();
	var return_date = $('#listing_return_date').val();
	var maxstopovers = $('#maxstopovers').val();
	var pnum = $('#pnum').val();
	
	var sortby = $('#sortby').val();
	sortby = sortby .split("_");
	
	var price_range = $('#price_range').val();
	price_range = price_range.replace(/\$/g, '');
	price_range = price_range.split(' - ');
	
	var dtime_range = $('#dtime_range').val();
	dtime_range = dtime_range.split(" to ");
	if( dtime_range[0] == '00:00' && dtime_range[1] == '23:59'){
		dtime_range[0] = null;
		dtime_range[1] = null;
	}
	
	var atime_range = $('#atime_range').val();
	atime_range  = atime_range .split(" to ");
	if( atime_range[0] == '00:00' && atime_range[1] == '23:59'){
		atime_range[0] = null;
		atime_range[1] = null;
	}
	
	var ret_dtime_range = $('#ret_dtime_range').val();
	ret_dtime_range = ret_dtime_range.split(" to ");
	if( ret_dtime_range[0] == '00:00' && ret_dtime_range[1] == '23:59'){
		ret_dtime_range[0] = null;
		ret_dtime_range[1] = null;
	}
	
	var ret_atime_range = $('#ret_atime_range').val();
	ret_atime_range  = ret_atime_range .split(" to ");
	if( ret_atime_range[0] == '00:00' && ret_atime_range[1] == '23:59'){
		ret_atime_range[0] = null;
		ret_atime_range[1] = null;
	}
	
	var preferred_days = $('#preferred_days').val();
	var onlyWorkingDays = null;
	var onlyWeekends = null;
	if( preferred_days == 'onlyWorkingDays'){
		onlyWorkingDays = 1;
	}else if(preferred_days == 'onlyWeekends'){
		onlyWeekends = 1;
	}
	
	$.get('<?php echo base_url(); ?>get_flights_data', {
			page_no:current_page + 1,
			optradio:optradio,
			Flight_from:Flight_from,
			Flight_fromID:Flight_fromID,
			Flight_to:Flight_to,
			Flight_toID:Flight_toID,
			departure_date:departure_date,
			return_date:return_date,
			maxstopovers:maxstopovers,
			pnum:pnum,
			sort:sortby[0],
			asc:sortby[1],
			price_from: price_range[0],
			price_to: price_range[1],
			
			dtimefrom: dtime_range[0],
			dtimeto: dtime_range[1],
			atimefrom: atime_range[0],
			atimeto: atime_range[1],


			returndtimefrom: ret_dtime_range[0],
			returndtimeto: ret_dtime_range[1],
			returnatimefrom: ret_atime_range[0],
			returnatimeto: ret_atime_range[1],

			onlyWorkingDays:onlyWorkingDays,
			onlyWeekends:onlyWeekends
			
		}, 
		function(data){
		
			if(data != '0'){
				$('#content').append(data);
				$('#load_more').show();
			}else{
				endPagination = true;
				$('#load_more').hide();
			}
			current_page++;
			$('#loadingData').hide();
			$('.loader').hide();
	});
	
}

var callTimer = null;
var timer = setInterval(function () {
        scrollOK = true;
    }, 2000),
    scrollOK = true;//setup flag to check if it's OK to run the event handler

<?php if($total_flights > 0){?>

/*$(window).scroll(function(){
	
	if (scrollOK && !endPagination ) {
        scrollOK = false;
		
		if ( $(window).scrollTop() >= ( $(document).height() - $(window).height() - 500 ) ){
		
			if(callTimer){
				clearTimeout(callTimer);
			}
			
			$('#loadingData').show();
			callTimer = setTimeout(get_flights_data(), 500);
		}
	}
	
});*/

/*$(window).bind('scroll', function () {

    if (scrollOK && !endPagination ) {
        scrollOK = false;
		
	var document_height = $('.footer_scroll').offset().top - 800;
	var current_scroller_position = $(this).scrollTop() + $(this).height();
		
        if (current_scroller_position > document_height ) {
			
			if(callTimer){
				clearTimeout(callTimer);
			}
			
			$('#loadingData').show();
			callTimer = setTimeout(get_flights_data(), 2000);
        }
    }
});*/
<?php }?>

function pad (str, max) {
  str = str.toString();
  return str.length < max ? pad("0" + str, max) : str;
}


$(document).ready(function(e) {

	<?php if(!$checkReturn){?>
			$('.return_filters').hide();
	<?php }?>

	$( "#price-range-slider" ).slider({
		range: true,
		min: 0,
		max: 9999,
		values: [ 0, 9999 ],
		change:function(){
			if(callTimer){
				clearTimeout(callTimer);
			}
			
			if( !reset_flag ){
				callTimer = setTimeout(filterData(), 2000);
			}
		},
		slide: function( event, ui ) {
			$( "#price_range" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
		}
	});
	
	$( "#price_range" ).val( "$0 - $9999");
	
	$( "#dtime-range-slider" ).slider({
		range: true,
		min: 0,
		max: 24,
		step: 1,
		values: [ 0, 24 ],
		change:function(){
			if(callTimer){
				clearTimeout(callTimer);
			}
			
			if( !reset_flag ){
				callTimer = setTimeout(filterData(), 2000);
			}
		},
		slide: function( event, ui ) {
			var r1 = pad(ui.values[ 0 ], 2) + ':00';
			var r2 = pad(ui.values[ 1 ], 2) + ':00';
			
			if(ui.values[ 0 ] == 24){r1 = '00:00';}
			if(ui.values[ 1 ] == 24 || ui.values[ 1 ] == 0 ){r2 = '23:59';}
			
			$( "#dtime_range" ).val( r1 + " to " + r2);
		}
	});
	
	$( "#dtime_range" ).val( "00:00 to 23:59");
	
	$( "#atime-range-slider" ).slider({
		range: true,
		min: 0,
		max: 24,
		step: 1,
		values: [ 0, 24 ],
		change:function(){
			if(callTimer){
				clearTimeout(callTimer);
			}
			
			if( !reset_flag ){
				callTimer = setTimeout(filterData(), 2000);
			}
		},
		slide: function( event, ui ) {
			var r1 = pad(ui.values[ 0 ], 2) + ':00';
			var r2 = pad(ui.values[ 1 ], 2) + ':00';
			
			if(ui.values[ 0 ] == 24){r1 = '00:00';}
			if(ui.values[ 1 ] == 24 || ui.values[ 1 ] == 0 ){r2 = '23:59';}
			
			$( "#atime_range" ).val( r1 + " to " + r2);			
		}
	});
	
	$( "#atime_range" ).val( "00:00 to 23:59");
	
	//return range sliders
	$( "#ret-dtime-range-slider" ).slider({
		range: true,
		min: 0,
		max: 24,
		step: 1,
		values: [ 0, 24 ],
		change:function(){
			if(callTimer){
				clearTimeout(callTimer);
			}
			
			if( !reset_flag ){
				callTimer = setTimeout(filterData(), 2000);
			}
		},
		slide: function( event, ui ) {
			var r1 = pad(ui.values[ 0 ], 2) + ':00';
			var r2 = pad(ui.values[ 1 ], 2) + ':00';
			
			if(ui.values[ 0 ] == 24){r1 = '00:00';}
			if(ui.values[ 1 ] == 24 || ui.values[ 1 ] == 0 ){r2 = '23:59';}
			
			$( "#ret_dtime_range" ).val( r1 + " to " + r2);
		}
	});
	
	$( "#ret_dtime_range" ).val( "00:00 to 23:59");
	
	$( "#ret-atime-range-slider" ).slider({
		range: true,
		min: 0,
		max: 24,
		step: 1,
		values: [ 0, 24 ],
		change:function(){
			if(callTimer){
				clearTimeout(callTimer);
			}
			
			if( !reset_flag ){
				callTimer = setTimeout(filterData(), 2000);
			}
		},
		slide: function( event, ui ) {
			var r1 = pad(ui.values[ 0 ], 2) + ':00';
			var r2 = pad(ui.values[ 1 ], 2) + ':00';
			
			if(ui.values[ 0 ] == 24){r1 = '00:00';}
			if(ui.values[ 1 ] == 24 || ui.values[ 1 ] == 0 ){r2 = '23:59';}
			
			$( "#ret_atime_range" ).val( r1 + " to " + r2);			
		}
	});
	
	$( "#ret_atime_range" ).val( "00:00 to 23:59");

	/*
	$('.ls-modal').on('click', function(e){
		e.preventDefault();
		var href_url = $(this).attr('href');
		
		$('#myModal').find('.modal-body').html('<p>loading...</p>');
		$('#myModal').modal('show');
		
		
		$.ajaxPrefilter( function (options) {
		  if (options.crossDomain && jQuery.support.cors) {
			var http = (window.location.protocol === 'http:' ? 'http:' : 'https:');
			options.url = http + '//cors-anywhere.herokuapp.com/' + options.url;
			//options.url = "http://cors.corsproxy.io/url=" + options.url;
		  }
		});
		
		$.get(
			href_url,
			function (data) {
				$('#myModal').find('.modal-body').html(data);
			}
		);
		
	});*/
	
});

function updateSort(elem, sortby){
	$('#sortby').val(sortby);
	$('.Sorting-typeContent').removeClass('Sorting-typeActive');
	$(elem).parent().addClass('Sorting-typeActive');
	filterData();
}
</script>
