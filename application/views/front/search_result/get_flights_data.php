<?php 
	if($total_flights<=0){ die('0');}    
	
    if($checkReturn){

        $counter = ($page_no-1) * $pagination_limit;
        
        foreach($flights as $flight){
            
            $nightsflag = true;
            $dtime = explode(',', $flight['dTime']);
            $atime = explode(',', $flight['aTime']);
            
            $retdtime = explode(',', $flight['retdTime']);
            $retatime = explode(',', $flight['retaTime']);
?>
        <div id="accordian-acc">
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <div class="accordion-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle uncollapsebtn collapsed" data-toggle="collapse" data-parent="#accordion2" data-target="#collapseID<?php echo $counter;?>">
                                    <div class="Journey-content">
                                        <div class="JourneyInfo MultiInfo">
                                            <span class="">$ <?php echo $flight['price'];?></span>
                                        </div>
                                        <div class="TripInfo">
                                            
                                            <div class="TripInfoIn">
                                                <div class="pic-holder">
                                                    <?php foreach($flight['departure_airlines'] as $aline){?>
                                                    <img src="https://images.kiwi.com/airlines/32/<?php echo ( isset($airlines[$aline]) ? $airlines[$aline]['logo'] : $aline);?>.png" alt="" height="20px" width="20px">
                                                    <?php }?>
                                                </div>
                                                <div class="company-name">
                                                    <?php
                                                    $lctr = 0; 
                                                        $final_string = '';
                                                        foreach($flight['departure_airlines'] as $aline){
                                                            if($lctr > 0){
                                                                $final_string .= ', ';
                                                            }
                                                            $final_string .= ( isset($airlines[$aline]) ? $airlines[$aline]['name'] : $aline);
                                                            $lctr++;
                                                        }
                                                    ?>
                                                    <span title="<?php echo $final_string;?>">
                                                        <?php
                                                        echo substr($final_string,0, 15);
                                                        if($final_string > 15){
                                                            echo '...';
                                                        }?>
                                                    </span>
                                                </div>
                                                <div class="TripInfoField_dateField">
                                                    <div class="TripInfoField-time">
                                                        <time class="" datetime="2016-11-11T23:00:00+00:00"><?php echo $dtime[1];?></time>					-					
                                                        <time class="" datetime="2016-11-11T23:50:00+00:00"><?php echo $atime[1];?></time>
                                                    </div>
                                                    <div class="TripInfoField-date">
                                                        <time class="" datetime="2016-11-11T23:00:00+00:00"><?php echo $dtime[0];?></time>
                                                    </div>
                                                </div>
                                                <div class="TripInfoField_flightField">
                                                    <div class="TripInfoField-flight-duration">
                                                        <?php echo $flight['fly_duration'];?>
                                                    </div>
                                                    <div class="TripInfoField-airport-codes">
                                                        <span class="name-and-code">
                                                            <?php echo $flight['cityFrom']?> ( <?php echo $flight['flyFrom']?> )
                                                        </span>
                                                        <span class="flight-arrow">
                                                            <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                                                        </span>
                                                        <span class="name-and-code">

                                                            <?php echo $flight['cityTo']?> ( <?php echo $flight['flyTo']?> )
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="TripDays">
                                                <ul>
                                                    <li></li>
                                                    <li>
                                                        <?php echo $flight['nightsInDest'];?> 
                                                        Night<?php echo ($flight['nightsInDest'] > 1) ? 's' : '';?> In 
                                                        <?php echo $flight['cityTo'];?>
                                                    </li>																<li></li>
                                                </ul>
                                            </div>
                                            
                                            <div class="TripInfoIn">
                                                <div class="pic-holder">
                                                    <?php foreach($flight['return_airlines'] as $aline){?>
                                                        <img src="https://images.kiwi.com/airlines/32/<?php echo (isset($airlines[$aline]) ? $airlines[$aline]['logo'] : $aline );?>.png" alt="" height="20px" width="20px">
                                                    <?php }?>
                                                </div>
                                                <div class="company-name">
                                                    <?php
                                                    $lctr = 0; 
                                                        $final_string = '';
                                                        foreach($flight['return_airlines'] as $aline){
                                                            if($lctr > 0){
                                                                $final_string .= ', ';
                                                            }
                                                            $final_string .= ( isset($airlines[$aline]) ? $airlines[$aline]['name'] : $aline);
                                                            $lctr++;
                                                        }
                                                    ?>
                                                    <span title="<?php echo $final_string;?>">
                                                        <?php
                                                        echo substr($final_string,0, 15);
                                                        if($final_string > 15){
                                                            echo '...';
                                                        }?>
                                                    </span>
                                                </div>
                                                <div class="TripInfoField_dateField">
                                                    <div class="TripInfoField-time">
                                                        <time class="" datetime="2016-11-11T23:00:00+00:00"><?php echo $retdtime[1];?></time>					-					
                                                        <time class="" datetime="2016-11-11T23:50:00+00:00"><?php echo $retatime[1];?></time>
                                                    </div>
                                                    <div class="TripInfoField-date">
                                                        <time class="" datetime="2016-11-11T23:00:00+00:00"><?php echo $retdtime[0];?></time>
                                                    </div>
                                                </div>
                                                <div class="TripInfoField_flightField">
                                                    <div class="TripInfoField-flight-duration">
                                                        <?php echo $flight['return_duration'];?>
                                                    </div>
                                                    <div class="TripInfoField-airport-codes">
                                                        <span class="name-and-code">
                                                            <?php echo $flight['cityTo']?> ( <?php echo $flight['flyTo']?> )
                                                        </span>
                                                        <span class="flight-arrow">
                                                            <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                                                        </span>
                                                        <span class="name-and-code">
                                                            <?php echo $flight['cityFrom']?> ( <?php echo $flight['flyFrom']?> )
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>    

                                        </div>
                                        <i class="fa fa-chevron-down MultiInfo" aria-hidden="true"></i>
                                    </div>
                                </a>
                            </h4>
                        </div>
                    </div>
                            
                    <div id="collapseID<?php echo $counter;?>" class="accordion-body allcollapse collapse" style="height: 0px;">
                        <div class="panel-body">
                            <div class="col-md-6">
                                <div class="NewTripDetail MultiTrip departure">
                                
                                    <div class="NewTripDetail-title">
                                        <h3>Departure</h3>
                                        <span class="txt-dep-min"><?php echo $flight['fly_duration'];?></span>
                                    </div>
                        <?php
                            $switchFlag = false; 
                            $prev_atime = 0;
                            foreach($flight['route'] as $route){
                                
                                $timediff = abs($route->dTimeUTC - $prev_atime);
                                $overlay_hours = floor($timediff / 3600);
                                
                                $prev_atime = $route->aTimeUTC;
                                
                                $overlay_time = '';
                                if($overlay_hours > 0){
                                    $overlay_time .= $overlay_hours.'h ';
                                }
                                
                                $overlay_minutes = floor(($timediff / 60) % 60);
                                if($overlay_minutes > 0){
                                    $overlay_time .= $overlay_minutes.'m';
                                }
                                
                                $dtime = explode(',', $route->dTimeFormated);
                                $atime = explode(',', $route->aTimeFormated);
                                
                                $first_return = false;
                                if( !$switchFlag && $route->return ){
                                    $first_return = true;
                                    echo '		</div>
                                            </div>
                                            
                                            <div class="col-md-6">
                                                <div class="NewTripDetail MultiTrip departure">
                                                    <div class="NewTripDetail-title">
                                                        <h3>Return</h3>
                                                        <span class="txt-dep-min">'.$flight['return_duration'].'</span>
                                                    </div>
                                        ';
                                    $switchFlag = true;
                                }
                                
                        ?>
                                    <?php if( !$first_return && ($route->bags_recheck || $flight['guarantee']) ){?>
                                    <div  class="NewTripLayover">
                                        <?php if($route->bags_recheck ){?>
                                            <i class="fa fa-clock-o"></i> Layover in <?php echo $route->cityFrom?> for <?php echo $overlay_time;?>.
                                            <div><i class="fa fa-briefcase"></i> Collect and recheck your baggage.</div>
                                        <?php }?>
                                        <?php if($flight['guarantee']){?>
                                            <div><i class="fa fa-briefcase"></i> <a href="javascript:;" data-toggle="modal" data-target="#myModal" >kiwi.com guarantee</a></div>
                                        <?php }?>
                                    </div>
                                        
                                    <?php }?>
                                    <div class="Part">
                                        <div class="Part-departure">
                                            <div class="Part-time">
                                                <time class="" datetime="2016-11-11T23:00:00+00:00"><?php echo $dtime[1];?></time>
                                                <time class="" datetime="2016-11-11T23:00:00+00:00"><?php echo $dtime[0];?></time>
                                            </div>
                                            <div class="Part-place">
                                                <?php echo $route->cityFrom?> <span><?php echo $route->flyFrom?></span>
                                            </div>
                                        </div>
                                        <div class="Part-com">
                                            <img src="https://images.kiwi.com/airlines/32/<?php echo ( isset($airlines[$route->airline]) ? $airlines[$route->airline]['logo'] : $route->airline);?>.png" alt="" height="22" width="22">
                                            </div>
                                            <div class="Part-body">
                                                <div class="Part-body-row">
                                                    <time class="" datetime="55:00Z"><?php echo $route->bags_recheck_required;?></time>
                                                </div>
                                            </div>
                                            <div class="Part-arrival">
                                                <div class="Part-time">
                                                    <time class="" datetime="2016-11-11T23:00:00+00:00"><?php echo $atime[1];?></time>
                                                    <time class="" datetime="2016-11-11T23:00:00+00:00"><?php echo $atime[0];?></time>
                                                </div>
                                                <div class="Part-place">
                                                    <?php echo $route->cityTo?> <span><?php echo $route->flyTo?></span>
                                                </div>
                                            </div>
                                        </div>
                            
                        <?php }?>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            
                            <a target="_blank" href="https://www.kiwi.com/us/booking?passengers=<?php echo $searchData['pnum'];?>&price=<?php echo $flight['price'];?>&token=<?php echo $flight['booking_token'];?>" class="NewJourneyDetail-bookingBtn" >Book this flight for $ <?php echo $flight['price'];?></a>
                            
                        </div>
                    </div>
                
                </div>
            </div>
        </div>
<?php	
            $counter++;
        }

    }else{
        $counter = ($page_no-1) * $pagination_limit;
        foreach($flights as $flight){
            $dtime = explode(',', $flight['route'][0]->dTimeFormated);
            $atime = explode(',', $flight['route'][0]->aTimeFormated);
?>
        <div id="accordian-acc">
        
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <div class="accordion-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle uncollapsebtn collapsed" data-toggle="collapse" data-parent="#accordion2" data-target="#collapseID<?php echo $counter;?>">						
                                    <div class="Journey-content">
                                        <div class="JourneyInfo">
                                            <span class="">$ <?php echo $flight['price'];?></span>
                                        </div>
                                        <div class="TripInfo">
                                            <div class="pic-holder">
<img src="https://images.kiwi.com/airlines/32/<?php echo ( isset($airlines[$flight['route'][0]->airline]) ? $airlines[$flight['route'][0]->airline]['logo'] : $flight['route'][0]->airline); ?>.png" alt="" height="20px" width="20px">
                                            </div>
                                            <div class="company-name">
                                                <span><?php echo ( isset($airlines[$flight['route'][0]->airline]) ? $airlines[$flight['route'][0]->airline]['name'] : $flight['route'][0]->airline); ?></span>
                                            </div>
                                            <div class="TripInfoField_dateField">
                                                <div class="TripInfoField-time">
                                                    <time class="" datetime="2016-11-11T23:00:00+00:00"><?php echo $dtime[1];?></time>
                                                    <time class="" datetime="2016-11-11T23:50:00+00:00"><?php echo $atime[1];?></time>
                                                </div>
                                                <div class="TripInfoField-date">
                                                    <time class="" datetime="2016-11-11T23:00:00+00:00"><?php echo $dtime[0];?></time>
                                                </div>
                                            </div>
                                                   
                                            <div class="TripInfoField_flightField">
                                                <div class="TripInfoField-flight-duration"><?php echo $flight['fly_duration']?></div>	
                                                <div class="TripInfoField-airport-codes">
                                                    <span class="name-and-code">
                                                        <?php echo $flight['route'][0]->cityFrom?> ( <?php echo $flight['route'][0]->flyFrom?> )
                                                    </span>
                                                    <span class="flight-arrow">
                                                        <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                                                    </span>										
                                                    <span class="name-and-code">
                                                        <?php echo $flight['route'][0]->cityTo?> ( <?php echo $flight['route'][0]->flyTo?> )
                                                    </span>									
                                                </div>    
                                            </div>						
                                        </div>
                                        
                                        <i class="fa fa-chevron-down" aria-hidden="true"></i>
                                    </div>
                                </a>
                            </h4>
                        </div>
                    </div>
                    
                    <div id="collapseID<?php echo $counter;?>" class="accordion-body allcollapse collapse" style="height: auto;">
                        <div class="panel-body">					
                            <div class="NewTripDetail">						
                            
                                <div class="NewTripDetail-title">							
                                    <h3>Departure</h3>							
                                    <span class="txt-dep-min"><?php echo $flight['fly_duration'];?></span>						
                                </div>						
                                
                        <?php	foreach($flight['route'] as $route){
                                    $dtime = explode(',', $route->dTimeFormated);
                                    $atime = explode(',', $route->aTimeFormated);?>
                                    
                                    <div class="Part">							
                                        <div class="Part-departure">								
                                            <div class="Part-time">									
                                                <time class="" datetime="2016-11-11T23:00:00+00:00"><?php echo $dtime[1];?></time>									
                                                <time class="" datetime="2016-11-11T23:00:00+00:00"><?php echo $dtime[0];?></time>								
                                            </div>								
                                            <div class="Part-place">
                                                <?php echo $route->cityFrom?> <span><?php echo $route->flyFrom?></span>
                                            </div>
                                        </div>							
                                    
                                        <div class="Part-com">								
                                            <img src="https://images.kiwi.com/airlines/32/<?php echo ( isset($airlines[$route->airline]) ? $airlines[$route->airline]['logo'] : $route->airline);?>.png" alt="" height="22" width="22">
                                        </div>							
                                    
                                        <div class="Part-body">								
                                            <div class="Part-body-row">									
                                                <time class="" datetime="55:00Z"><?php echo $route->bags_recheck_required?></time>								
                                            </div>							
                                        </div>							
                                    
                                        <div class="Part-arrival">								
                                            <div class="Part-time">									
                                                <time class="" datetime="2016-11-11T23:00:00+00:00"><?php echo $atime[1];?></time>									
                                                <time class="" datetime="2016-11-11T23:00:00+00:00"><?php echo $atime[0];?></time>								
                                            </div>								
                                            <div class="Part-place">
                                                <?php echo $route->cityTo?> <span><?php echo $route->flyTo?></span>
                                            </div>							
                                        </div>						
                                    </div>	
                        <?php	}?>                    
                                                                                    
                                <a target="_blank" href="https://www.kiwi.com/us/booking?passengers=<?php echo $searchData['pnum'];?>&price=<?php echo $flight['price'];?>&token=<?php echo $flight['booking_token'];?>" class="NewJourneyDetail-bookingBtn">Book this flight for $ <?php echo $flight['price'];?></a>
                            </div>				  
                        </div>				
                    </div>			
                    
                </div>
            </div>
        
        </div>
<?php	
            $counter++;
        }
    }
?>