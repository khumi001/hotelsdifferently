<div class="width-row">
    <div class="clearfix">
        <div class="col-sm-4">

            <div class="wizWized">
                <div class="sWhead">
                    <h4>Reservation Details</h4>
                </div>
                <div class="clearfix">
                    <div class="media" style="padding:0px 15px;">
                        <a class="pull-left" href="#">
                            <img style="width:100px; margin-bottom: 0px;" class="media-object img-responsive thumbnail" src="https://image2.urlforimages.com/CarImages/1/ZDUSECAR.jpg" alt="image">
                        </a>
                        <div class="media-body">
                            <div class="panel-title">
                               Car - Hyundai Accent Or Similar
                            </div>
                            <span>Economy</span>                            
                        </div>
                    </div>
                    <div class="bookingContent">
                        <ul class="list-unstyled">
                            <li role="separator" class="divider"></li>
                            <li><strong>Automatic Transmission</strong></li>
                            <li role="separator" class="divider"></li>
                            <li><strong>Pick-up Location & Time</strong></li>
                            <li>Las Vegas Apo</li>
                            <li>Dec 24, 2015 &nbsp;  11:00 AM</li>
                            <li role="separator" class="divider"></li>
                            <li><strong>Drop-off Location & Time</strong></li>
                            <li>Las Vegas Apo</li>
                            <li>Dec 26, 2015 &nbsp; 11:00 AM</li>
                            <li role="separator" class="divider"></li>
                            <li>Car Price:   <span>$55.75</span></li>
                            <li>Tax:  <span>$26.36</span></li>
                            <li class="text-danger">Amount due at booking time: <span>$82.11</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="row">
                <div role="tabpanel" class="wizard" data-initialize="wizard" id="">
                    <!-- Nav tabs -->
                    <div class="steps-container">
                    <ul class="nav nav-tabs nav-justified nav-pills wizTH  steps" role="tablist">
                        <li role="presentation" class="active" data-step="1" data-name="ti">
                            <a href="#ti" aria-controls="ti" role="tab" data-toggle="tab">Traveler Information</a>
                        </li>
                        <li role="presentation" data-step="2" data-name="bi">
                            <a href="#bi" aria-controls="bi" role="tab" data-toggle="tab">Billing Information</a>
                        </li>
                        <li role="presentation" data-step="3" data-name="rc">
                            <a href="#rc" aria-controls="rc" role="tab" data-toggle="tab">Review & Confirm</a>
                        </li>
                    </ul>
                    </div>
                    
                    <!-- Tab panes -->
                    <div class="tab-content wizTab ">  <!-- step-content -->
                        
                        <div role="tabpanel" class="tab-pane active clearfix" id="ti" data-step="1">
                            
                            <h4>Traveler Information</h4>
                            <p>Please provide all required driver information below.</p>  
                            

                            <form method="post" id="quote_request" class="form-horizontal form-bordered">
                                    <div class="form-group">
                                        
                                        <label class="control-label col-sm-3">Select Traveler:</label>
                                        <div class="col-sm-9">
                                            <select class="select2me form-control input-xlarge" name="cardType" id="" required>
                                                <option class="BF_combo_text" value="">Select Traveler</option>
                                                <option class="BF_combo_text" value="">Szilard Vegas</option>
                                                <option class="BF_combo_text" value="">James Shewmaker</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-sm-3"> First Name:</label>
                                        <div  class="col-sm-9">
                                           <input type='text' name='firstname' class="form-control input-medium" id='' value='' required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-3"> Last Name:</label>
                                        <div  class="col-sm-9">
                                           <input type='text' name='lastname' class="form-control input-medium" id='' value='' required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-3"> Title:</label>
                                        <div  class="col-sm-9">
                                           <select class="select2me form-control input-xlarge" name="title" id="" required>
                                               <option class="BF_combo_text" value="3" selected="selected">MR</option><option class="BF_combo_text" value="4">MRS</option><option class="BF_combo_text" value="2">MISS</option><option class="BF_combo_text" value="5">MS</option><option class="BF_combo_text" value="6">Master</option><option class="BF_combo_text" value="1">DR</option><option class="BF_combo_text" value="7">Reverend</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-3"> Phone:</label>
                                        <div  class="col-sm-3">
                                           <select class="select2me form-control input-xlarge" name="numHeader" id="" required>
                                                
                                            </select>
                                        </div>
                                        <div  class="col-sm-6">
                                           <input type='number' name='phone' class="form-control input-medium" id='' value='' required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-3"> Age:</label>
                                        <div  class="col-sm-9">
                                           <input type='number' name='age' class="form-control input-medium" id='' value='' required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-3"> Email:</label>
                                        <div  class="col-sm-9">
                                           <input type='email' name='email' class="form-control input-medium" id='' value='' required>
                                        </div>
                                    </div>
                                    <div class="divider"></div>

                                    <h4>Flight Information <small>(optional)</small></h4>
                                    <p>(If you'll be arriving on a flight, please provide your flight details)</p>
                                    <div class="form-group">
                                        <label class="control-label col-sm-3">Airline:</label>
                                        <div  class="col-sm-9">
                                           <select class="select2me form-control input-xlarge" name="airline" id="" required>
                                        
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-3"> Flight Number:</label>
                                        <div  class="col-sm-9">
                                           <input type='number' name='fnum' class="form-control input-medium" id='' value=''>
                                        </div>
                                    </div>
                                    <div class="divider"></div>  
                                    <h4>Other Requests</h4>
                                    <div class="clearfix">
                                        <label class="control-label">Special Requests (not guaranteed)</label>
                                        <textarea type='text' name='splReq' class="form-control input-medium" value=''></textarea>
                                    </div>

                                    <div class="clearfix mt20 actions">
                                        <button type="submit" class="btn btn-primary pull-right">Continue</button>
                                    </div>
                            </form>


                        </div>
                        <div role="tabpanel" class="tab-pane clearfix" id="bi">

                        <h4>Billing Information</h4>
                        <form method="post" id="quote_request" class="form-horizontal form-bordered" data-step="2">

                            <div class="form-group">
                                <label class="control-label col-sm-3"> First Name:</label>
                                <div  class="col-sm-9">
                                   <input type='text' name='firstname' class="form-control input-medium" id='' value='' required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-3"> Last Name:</label>
                                <div  class="col-sm-9">
                                   <input type='text' name='lastname' class="form-control input-medium" id='' value='' required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3"> Phone:</label>
                                <div  class="col-sm-9">
                                   <input type='text' name='phone' class="form-control input-medium" id='' value='' required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">Country:</label>
                                <div  class="col-sm-9">
                                   <select class="select2me form-control input-xlarge" name="country" id="" required>
                                    
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">State:</label>
                                <div  class="col-sm-9">
                                   
                                   <select class="select2me form-control input-xlarge" name="state" id="" required>
                                    
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">Zip:</label>
                                <div  class="col-sm-9">
                                   <input type='text' name='zip' class="form-control input-medium" id='' value='' required>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="control-label col-sm-3">City:</label>
                                <div  class="col-sm-9">
                                   <input type='text' name='city' class="form-control input-medium" id='' value='' required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">Street:</label>
                                <div  class="col-sm-9">
                                   <input type='text' name='Street' class="form-control input-medium" id='' value='' required>
                                </div>
                            </div>


                            <h4>Payment Method</h4>
                            <div class="payment">
                                <span class="fa fa-cc-mastercard"></span>
                                <span class="fa fa-cc-visa"></span>
                                <span class="fa fa-cc-amex"></span>
                                <span class="fa fa-cc-jcb"></span>
                                <span class="fa fa-cc-discover"></span>
                                
                            </div>
                            <span>Your information is safe and secure</span>
                            <p>Your credit card will be charged on the following date: <span class="text-primary">12/21/2015</span></p>

                            <div class="form-group">
                                <label class="control-label col-sm-3">Card Type:</label>
                                <div  class="col-sm-9">
                                    <select class="select2me form-control input-xlarge" name="cardType" id="" required>
                                        <option class="" value="">Select</option>
                                        <option class="" value="51|true">Visa</option>
                                        <option class="" value="39|true" selected="selected">MasterCard</option>
                                        <option class="" value="31|true">Discover</option>
                                        <option class="" value="125|true">JCB</option>
                                        <option class="" value="11|true">AmericanExpress</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">Card Number:</label>
                                <div  class="col-sm-9">
                                   <input type='text' name='cardNumber' class="form-control input-medium" id='' value='' required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">Expiration:</label>
                                <div  class="col-sm-9">
                                   <div class="row">
                                       <div class="col-sm-6">
                                            <select class="select2me form-control input-xlarge" name="day" id="" required>
                                                <option class="" value="-1" selected="selected">month</option>
                                                <option class="" value="01">January</option>
                                                <option class="" value="02">February</option>
                                                <option class="" value="03">March</option>
                                                <option class="" value="04">April</option>
                                                <option class="" value="05">May</option>
                                                <option class="" value="06">June</option>
                                                <option class="" value="07">July</option>
                                                <option class="" value="08">August</option>
                                                <option class="" value="09">September</option>
                                                <option class="" value="10">October</option>
                                                <option class="" value="11">November</option>
                                                <option class="" value="12">December</option>
                                            </select>
                                       </div>
                                       <div class="col-sm-6">
                                            <select class="select2me form-control input-xlarge" name="year" id="" required>
                                                <option class="" value="-1" selected="selected">year</option>
                                                <option class="" value="2015">2015</option>
                                                <option class="" value="2016">2016</option>
                                                <option class="" value="2017">2017</option>
                                                <option class="" value="2018">2018</option>
                                                <option class="" value="2019">2019</option>
                                                <option class="" value="2020">2020</option>
                                                <option class="" value="2021">2021</option>
                                                <option class="" value="2022">2022</option>
                                                <option class="" value="2023">2023</option>
                                                <option class="" value="2024">2024</option>
                                                <option class="" value="2025">2025</option>
                                                <option class="" value="2026">2026</option>
                                                <option class="" value="2027">2027</option>
                                                <option class="" value="2028">2028</option>
                                                <option class="" value="2029">2029</option>
                                            </select>
                                        </div>
                                   </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">Security Code:</label>
                                <div  class="col-sm-9">
                                   <input type='text' name='' class="form-control input-medium" id='' value='' required>
                                </div>
                            </div>

                            <div class="clearfix mt20">
                                <button type="submit" class="btn btn-primary pull-right">Continue</button>
                            </div>
   
 
                        </form>

    


                        </div>
                        <div role="tabpanel" class="tab-pane clearfix" id="rc" data-step="3">
                            <h4>Review & Confirmation</h4>

                            <div class="media" style="padding:0px 15px;">
                                <a class="pull-left" href="#">
                                    <img style="width:100px; margin-bottom: 0px;" class="media-object img-responsive thumbnail" src="https://image2.urlforimages.com/CarImages/1/ZDUSECAR.jpg" alt="image">
                                </a>
                                <div class="media-body">
                                    <div class="panel-title">
                                        Car - Hyundai Accent Or Similar
                                    </div>
                                    <img src="https://www.lmtclub.com/Images/CarCompanies/logo-Budget.gif" class="img-responsive" alt="Image">
                                </div>
                            </div>
                            <div class="bookingContent">
                                <ul class="list-unstyled">
                                    <li role="separator" class="divider"></li>
                                    <li><strong>Pick-Up Station</strong></li>
                                    <li>Address:Mccarran Intl Airport 7135 Gilespie</li>
                                    <li>Phone:702-736-1212</li>
                                    <li><strong>Drop-Off Station</strong>></li>
                                    <li>Same as Pick-Up station</li>
                                    <li role="separator" class="divider"></li>
                                    <li><strong>Status:  <span>Confirmed</span></strong></li>
                                    <li>Car Class   <span>Economy</span></li>
                                    <li>A/C <span>Yes</span></li>
                                    <li>Transmission    <span>Automatic Transmission</span></li>
                                    <li>Occupancy   <span>5</span></li>
                                    <li>Luggage <span>1 large + 1 small</span></li>
                                    <li role="separator" class="divider"></li>
                                    <li><strong>Program Inclusions</strong></li>  
                                    <li>Airport Concession Fee, Local Tax, Customer Fac. Fee, County Surcharge, State Surcharge, Veh. License Fee, Unlimited Mileage</li>
                                    <li role="separator" class="divider"></li>
                                    <li>Pick-Up:<span> McCarran International Airport - LAS - Dec 24, 2015 - 11:00 AM</span></li>
                                    <li>Drop-Off:<span>    McCarran International Airport - LAS - Dec 26, 2015 - 11:00 AM</span></li>
                                    <li>Daily Rate:<span>  $41.06</span></li>
                                </ul>
                                <div class="divider"></div>


                                <h4>Driver Information</h4>
                                <ul class="list-unstyled">
                                    <li role="separator" class="divider"></li>
                                    <li>Driver : <span>MR. James Shewmaker</span></li>
                                    <li>Driver's Age: <span>52</span></li>
                                    <li>Phone1: <span>-4043142902</span></li>
                                    <li role="separator" class="divider"></li>
                                </ul>
                                
                                <h4>Summary of Billing Information</h4>
                                <ul class="list-unstyled">
                                    <li class="text-danger">Price:<span><strong>$113.25</strong></span></li>
                                    <li>Your credit card will be charged on the following date:<span><strong>Dec 23, 2015</strong></span></li>
                                    <li role="separator" class="divider"></li>
                                    <li>Name:<span>James Shewmaker</span></li>
                                    <li>Credit Card:<span> Mastercard</span></li>
                                    <li>Credit Card Number:<span>XXXXXXXXXX5100</span></li>
                                    <li>Expiration:<span>February, 2016</span></li>
                                    <li>Address:<span>asd ,asd ,Arizona 23154 USA</span></li>
                                    <li>Phone:<span>1-4043142902</span></li>
                                </ul>
                                <div class="divider"></div>
                                <h4>Terms & Conditions</h4>
                                <iframe width="100%" style="border:1px solid #E5E5E5;" src="terms.html"></iframe>
                                <form method="post" id="quote_request" class="form-horizontal form-bordered">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" value="">
                                            I have read and agree with the Terms & Conditions stated above
                                        </label>
                                    </div>
                                    <div class="clearfix mt20">
                                        <button type="submit" class="btn btn-primary pull-right">Finish</button>
                                    </div>
                                </form>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>