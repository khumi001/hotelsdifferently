<div class="width-row">
	<div class="wizTab">
    <?php
        if(isset($success))
        {
            ?>
                <div class="alert alert-success">
                    Good job! Your reservation has been successfully made and we forwarded it to the hotel.
                </div>
            <?php
                if($hotel_info['avalibilty'] == 'Available')
                {
                    ?>    
                    <p style="color:#F00; font-weight:bold;">
                        <span style="text-decoration:underline;">IMPORTANT :</span> This page Contains your VOUCHER and you need to bring it with your Hotel as it can be requested where the service take place! 
                    </p>
                <?php
                }
                else
                {
                    ?>
                    <p style="color:#F00; font-weight:bold;">
                        <span style="text-decoration:underline;">IMPORTANT :</span> Once your reservations is confirmed, you will receive the voucher. In order to ensure a smooth check-in experience, please take the voucher with you because the hotel may request it upon check-in! 
                    </p>

                <?php
                    }
                ?>
                <p>
                    Your payment of <strong><?php echo $hotel->paid;?></strong> was successfully charged and your charges will appear as <span style="color:#F00; font-weight:bold;">“WholsaleHtLs8882872307” </span> on your statement.
                </p>

             <?php
                if($hotel_info['avalibilty'] == 'Available')
                {
                    ?>
                        <!--<p>
                        If your check-in is in <b>less than 72 hours</b>, then your reservation is sent to the hotel immediately but every hotel has different ways and timeframes for processing incoming reservations and hotels usually prioritize incoming bookings by arrival date. Therefore if your check-in date is for a future date, your reservation <b>might</b> not show up in their system <b>for up to 5 days</b> from your booking.
                        </p>-->
                    <p>
                        Please note that once you paid for your reservation, it is immediately confirmed and your room is guaranteed (with the exception of ON REQUEST bookings). However, every hotel processes and inputs incoming reservations differently, therefore it may happen that you contact the hotel and they may not see your booking until closer to your arrival. <b>Rest assured that your room is secured</b> the moment your reservation processes and a <b><?php echo DOMAIN_NAME;?> confirmation number</b> is issued. Should you have any questions or concerns please do not hesitate to let us know via our Contact Us menu or the toll-free number that is listed on your reservation confirmation.
                    </p>
                        <br>
                        <p>Should you encounter any problems with this reservation, please send us a message immediately via the <b>"Contact Us"</b> page and select <b>"Problems with booking"</b> so that we can rectify any problems you may have experienced. Those emails enjoy high priority and you can expect a very fast reply. Alternatively, you are also welcome to call us at <b>888-287-2307</b> and our customer service representatives will be happy to assist you <b>24 hours a day, 7 days a week, 365 days a year.</b></p>
                        <div class="media" style="padding:0px 15px; text-align:center;">
                            <strong>Download your voucher(s) and invoice(s):</strong>
                        </div> 
                        <div class="text-center" style="text-align:center;">
                            <a  target="_blank" href="<?php echo base_url(); ?>front/quote_request/pdf/<?php echo $booking_id; ?>"><img src="https://iconbug.com/data/5b/507/52ff0e80b07d28b590bbc4b30befde52.png" width="50" height="50" /></a>
                            <?php if($this->session->userdata['valid_user']['var_usertype'] == 'TF'){?>
                            <a  target="_blank" href="<?php echo base_url(); ?>front/quote_request/voucher/<?php echo $booking_id; ?>"><img src="https://iconbug.com/data/5b/507/52ff0e80b07d28b590bbc4b30befde52.png" width="50" height="50" /></a>
                            <?php }?>
                        </div>
                    <?php
                }
                else
                {
                    ?>
                    <strong style="color:#F00; font-weight:bold;font-size:20px;text-align: center;display: block; margin: 0 0 10px;">Your reservation can not be confirmed at this time.</strong>
                    <p>Due to this amazing deal you are trying to secure, the allotment of rooms at this hotel have been sold out; however; our team of dedicated professionals negotiating with the hotel on your behalf to secure additional rooms out our advertised rate. Upon booking this reservation, you will receive a reservation number for status identification purposes only.  </p>
                    <strong style="margin: 0 0 10px;display: block;">Your reservation number is: <?php echo $hotel->codeID;?></strong>
                    <p>Within the next 48 business hours the hotel will reply with a definite confirmation or decline of this reservation.</p>
                    <p style="font-weight:bold;color: red;">If your reservation date is within 3 days, we highly encourage you to perform a follow-up message immediately via our CONTACT US menu and manually request an expedited review of the On Request booking because we would need to contact our source immediately in order to get a speedy reply of confirmation or decline of your booking.</p>
                    <p>Once the request is confirmed by the hotel, you will receive an email with notification of the confirmation along with your voucher. Should the request be declined by the hotel, you will receive an email with notification of the decline and a full refund. Please allow 5-7 business days for the refund to appear in your account.</p>
                    <p>Please call us at<strong> 888-287-2307</strong> to check the status of your reservation <strong>if you do not receive a response after 48 hours <ins>OR</ins> if your requested check in is within 24 hours.</strong> We will be happy to assist you with your reservation.</p>
                    <strong>Please contact us prior to making alternate arrangements as open requests may confirm at any time up to the check in date.</strong>
                    <?php
                }
            ?>
            <?php
        }
    ?>
   <!--<div class="media" style="padding:0px 15px; text-align:center;">
        <strong>Download your invoice here:</strong>
    </div> -->
   <!--  <div class="text-center" style="text-align:center;">
        <a  target="_blank" href="<?php //echo base_url(); ?>front/quote_request/pdf/<?php //echo $booking_id; ?>"><img src="https://iconbug.com/data/5b/507/52ff0e80b07d28b590bbc4b30befde52.png" width="50" height="50" /></a>
    </div> -->
    <p style="border:1px solid #000; margin:15px 0;"></p>
    
    <p><b style="font-size:14px; display:block; text-align:center;">NEED A TICKET FOR AN EVENT OR CONCERT?</b></p>
    <p>We are proud to be affiliated with TicketLiquidator which is one of the biggest names in the event ticket industry. On many occasions, you can obtain tickets for events that are even sold out. Type in a Venue, an Event or a Performer and see how much you can save!</p>
    
     <script type="text/javascript" src="https://tickettransaction2.com/Brokers/01504-011/WidgetJS/scriptsretail-stringutils.js"></script> <script type="text/javascript">var widgetCSS=document.createElement("link"); widgetCSS.setAttribute("rel", "stylesheet"); widgetCSS.setAttribute("type", "text/css"); widgetCSS.setAttribute("href", "https://tickettransaction2.com/Brokers/01504-011/WidgetCSS/Style.css"); document.getElementsByTagName("head")[0].appendChild(widgetCSS); </script><p></p>
<div id="TL_Search_Widget" style="width: 100%; border: 1px solid rgb(218, 212, 200);">
<div id="tlsw_header"> <span>Find Tickets</span><input id="tlsw_searchTxt" value="Search by Artist or Event" onblur="if(this.value=='') this.value=this.defaultValue;" onfocus="if(this.value==this.defaultValue) this.value='';" type="text"><p></p>
<div id="tlsw_searchBtn" style="background-image: url(&quot;//s3.amazonaws.com/ticketliquidator/affiliates/widgetMaker/SearchWidget/green_btn.png&quot;);" onclick="tn_SubmitSearch()"> <script type="text/javascript">function tn_KeyDownHandler(e){if (e==null) e=window.event; if (e.keyCode==13) tn_SubmitSearch();}document.getElementById('tlsw_searchTxt').onkeypress=tn_KeyDownHandler; </script> </div>
</div>
<ul id="tlsw_content" class="suggestionsArea"></ul>
<div id="tlsw_scripts"><script type="text/javascript">function tn_fill_top_events() {var tlsw_content = new Array();for (var tn_counter = 0; tn_counter < tn_top_performers.length; tn_counter++)tlsw_content.push('<li class="tlsw_suggestion" onmouseover="this.style.backgroundPosition=\'bottom right\';" onmouseout="this.style.backgroundPosition=\'top right\';" style="background-image:url(<?php echo base_url();?>/public/affilliate_theme/img/gray-bg.png);"><a target= "_blank"href="//www.jdoqocy.com/click-8137399-10858765?sid=&amp;url=http:\/\/www.ticketliquidator.com/tix/' + Fixer(tn_top_performers[tn_counter]) + '-tickets.aspx"><span class="tlsw_linktext">' + tn_top_performers[tn_counter] + '</span></a></li>');document.getElementById('tlsw_content').innerHTML = tlsw_content.join('');}</script><script type="text/javascript" src="//tickettransaction.com/?bid=1&amp;tid=top_sellers&amp;javaarray=true&amp;listsize=5"></script><script type="text/javascript">function tn_SubmitSearch() {var SearchPhrase = document.getElementById('tlsw_searchTxt').value.trim().replace(/\s/g, '-');window.open('//www.jdoqocy.com/click-8137399-10858765?sid=&amp;url=' + 'http://www.ticketliquidator.com/tix/' + SearchPhrase + '-tickets.aspx');}</script></div>
<div id="tlsw_footer"> <a id="tlsw_footer_link" href="//www.jdoqocy.com/click-8137399-10858765?sid=&amp;url=http://www.ticketliquidator.com/default.aspx" target="_blank"> <img src="//s3.amazonaws.com/ticketliquidator/affiliates/widgetMaker/SearchWidget/logo-white.png"> </a> </div>
</div>
    
    
    <p style="border:1px solid #000; margin:15px 0;"></p>
    <p><b style="font-size:14px; display:block; text-align:center;">NEED INSURANCE?</b></p>
                        <p>We (<strong><?php echo SITE_NAME;?></strong>) do NOT offer travel insurance, however you can purchase travel insurance from a third party insurance company. Please note that we do not directly sell any types of insurance; we are solely an Affiliate to a third party insurance company.</p>
    <p style="text-align:center;">
        <a style="display:inline-block; vertical-align:top; max-width:200px;" href="http://www.dpbolvw.net/click-8137399-10892804-1466615700000" rel="nofollow" target="_blank">
            <img style="display:block; width:100%; height:auto;" src="<?php echo base_url();?>/public/affilliate_theme/img/image-8137399-10892804-1466615700000.png" width="125" height="125" alt="Allianz Travel Insurance" border="0"/>
        </a>
        <a style="display:inline-block; vertical-align:top; max-width:200px;" href="http://www.jdoqocy.com/click-8137399-11779657-1466617745000" rel="nofollow" target="_blank">
            <img style="display:block; width:100%; height:auto;" src="<?php echo base_url();?>/public/affilliate_theme/img/image-8137399-11779657-1466617745000.png" width="300" height="250" alt="Allianz Travel Insurance" border="0"/>
        </a>
        <a style="display:inline-block; vertical-align:top; max-width:200px;" href="http://www.tkqlhce.com/click-8137399-11176385-1466616611000" rel="nofollow" target="_blank">
            <img style="display:block; width:100%; height:auto;" src="<?php echo base_url();?>/public/affilliate_theme/img/image-8137399-11176385-1466616611000.png" width="300" height="250" alt="" border="0"/>
        </a>
    </p>
    <p style="border:1px solid #000; margin:15px 0;"></p>
    <h3 style="font-size:17px; margin:0 0 10px; font-weight:bold; text-align:center;">NEED A RIDE?</h3>
    <h3 style="font-size:14px; margin:0 0 10px; font-weight:bold; text-align:center;color:#e00303;"><a href="https://www.uber.com/invite/8z1j5"  rel="nofollow" target="_blank"><img src="<?php echo base_url(); ?>public/assets/img/uber-logo.png" alt="uber" ></a></h3>
    <h2 style="font-size:14px; margin:0 0 10px; font-weight:bold; text-align:center;">
<span style="color:#e00303;">GET $15 OFF OR MORE</span> depending on your location by signing up through the banner above or enter Promo Code <span style="color:#e00303;">8z1j5</span> in the app!
    </h2>
    <!--<p style="margin:0 0 15px; text-align:center;"><a style="color:#e00303;" href="https://www.uber.com/invite/8z1j5">SIGNUP FOR UBER HERE!  </a></p>-->
    
    
    <h3 style="font-size:14px; margin:0 0 10px; font-weight:bold; text-align:center;color:#e00303;"><a href="https://www.lyft.com/invite/SZILARD527631" rel="nofollow" target="_blank"><img src="<?php echo base_url(); ?>public/assets/img/lyft-logo.png" alt="LYFT" ></a></h3>
    <h2 style="font-size:14px; margin:0 0 10px; font-weight:bold; text-align:center;">GET <span style="color:#e00303;">$50 OFF</span> towards your first rides by signing up through the banner above!</h2>
    <!--<p style="text-align:center;"><a style="color:#e00303;" href="https://www.lyft.com/invite/SZILARD527631">SIGNUP FOR LYFT HERE! </a></p>-->
    <!--<p style="font-size:13px;">You must be new to Lyft and in an eligible market to qualify. See the Terms and Conditions for eligible markets and other restrictions.</p>-->
    
    </div>
</div>
