<div class="width-row">
    <div class="clearfix">
        <div class="col-sm-4">
            <div class="wizWized">
                <div class="sWhead">
                    <h4>Reservation Details</h4>
                </div>
                <div class="clearfix">
                    <div class="media" style="padding:0px 15px;">
                        <a class="pull-left" href="#">
                            <img style="width:100px; margin-bottom: 0px;" class="media-object img-responsive thumbnail" src="http://image1.urlforimages.com/Images/1231501/100x100/475487017.jpg" alt="image">
                        </a>
                        <div class="media-body">
                            <div class="panel-title">
                               Cruise - 4 Nights Bahamas Cruise from Miami - Roundtrip
                            </div>
                            
                        </div>
                    </div>

                    <div class="bookingContent">
                        <ul class="list-unstyled">
                            <li role="separator" class="divider"></li>
                            <li>Cruise Line:<span>Celebrity Cruises</span></li>
                            <li>Ship:<span>Celebrity Constellation</span></li>
                            <li>Departs:<span>Dec 31, 2015</span></li>
                            <li>Returns:<span>Jan 04, 2016</span></li>
                            <li role="separator" class="divider"></li>
                            <li>Port of Departure: <span>Fort Lauderdale, Florida</span></li>
                            <li>Cabin Category: <span>2A - Veranda Stateroom</span></li>
                            <li>Cabin Number: <span>7143</span></li>
                            <li>Deck: <span>Vista</span></li>
                            <li>Passengers: <span>1 Adult(s), 1 Children</span></li>
                            <li role="separator" class="divider"></li>
                            <li>
                                <p>Included Promotions:</p>
                                <a href="">Go Big with free Classic Beverage Package</a>
                            </li>
                            <li role="separator" class="divider"></li>
                            <li>Price:<span>$2460.80</span></li>
                            <li>Taxes/Fees:<span>$206.40</span></li>
                            <li class="text-danger">Total:<span>$2667.20</span></li>
                        </ul>




                    </div>


                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="row">
                <div role="tabpanel" class="wizard" data-initialize="wizard" id="">
                    <!-- Nav tabs -->
                    <div class="steps-container">
                    <ul class="nav nav-tabs nav-justified nav-pills wizTH  steps" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#passengers" aria-controls="passengers" role="tab" data-toggle="tab">Select Passengers</a>
                        </li>
                        <li role="presentation"  data-name="cc">
                            <a href="#cc" aria-controls="cc" role="tab" data-toggle="tab">Cabin Category</a>
                        </li>
                        <li role="presentation"  data-name="cac">
                            <a href="#cac" aria-controls="cac" role="tab" data-toggle="tab">Choose a cabin</a>
                        </li>
                        <li role="presentation" data-name="ti">
                            <a href="#ti" aria-controls="ti" role="tab" data-toggle="tab">Traveler Information</a>
                        </li>
                        <li role="presentation"  data-name="bi">
                            <a href="#bi" aria-controls="bi" role="tab" data-toggle="tab">Billing Information</a>
                        </li>
                        <li role="presentation" data-name="rc">
                            <a href="#rc" aria-controls="rc" role="tab" data-toggle="tab">Review & Confirm</a>
                        </li>
                    </ul>
                    </div>
                    
                    <!-- Tab panes -->
                    <div class="tab-content wizTab ">  <!-- step-content -->

                        <div role="tabpanel" class="tab-pane clearfix active" id="passengers">
                            <h4>How many passengers will be in your stateroom?</h4>

                            <form method="post" id="quote_request" class="form-horizontal form-bordered">
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Adult(s):</label>
                                    <div  class="col-sm-4">
                                       <select class="select2me form-control input-xlarge" name="cardType" id="" required>
                                            <option class="" value="">Select</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            
                                        </select>
                                    </div>
                                    <label class="control-label col-sm-2">Child(ren):</label>
                                    <div  class="col-sm-4">
                                       <select class="select2me form-control input-xlarge" name="cardType" id="" required>
                                            <option class="" value="">Select</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                        </select>
                                    </div>
                                </div>

                                <h4>Age of passengers</h4>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Adult(1):</label>
                                    <div  class="col-sm-4">
                                       <select class="select2me form-control input-xlarge mb10" name="cardType" id="" required>
                                            <option class="" value="">Select</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>                                            
                                        </select>
                                    </div>
                                    <label class="control-label col-sm-2">Adult(1):</label>
                                    <div  class="col-sm-4">
                                       <select class="select2me form-control input-xlarge mb10" name="cardType" id="" required>
                                            <option class="" value="">Select</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-sm-2">Child(1):</label>
                                    <div  class="col-sm-4">
                                       <select class="select2me form-control input-xlarge mb10" name="cardType" id="" required>
                                            <option class="" value="">Select</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>                                            
                                        </select>
                                    </div>
                                    <label class="control-label col-sm-2">Child(2):</label>
                                    <div  class="col-sm-4">
                                       <select class="select2me form-control input-xlarge mb10" name="cardType" id="" required>
                                            <option class="" value="">Select</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix mt20">
                                    <button type="submit" class="btn btn-primary pull-right">Continue</button>
                                </div>
                            

                            </form>
                            
                        </div>
                        
                        <div role="tabpanel" class="tab-pane clearfix" id="cc">
                            <div class="panel panel-info">
                                  <div class="panel-heading">
                                        <h4 class="panel-title">Balcony:</h4>
                                  </div>
                                  <div class="panel-body">
                                        <div class="media">
                                            <a class="pull-left" href="#">
                                                <img class="media-object" src="http://image2.urlforimages.com/Cruises/CEL/Ships/CS/CT/S1_S.jpg" alt="Image">
                                            </a>
                                            <div class="media-body">
                                                <div class="media-heading">
                                                    <img src="http://image2.urlforimages.com/Cruises/CEL/CC/W.gif" alt="">
                                                    <a href=""> Suite guarantee</a> <span class="pull-right">$874.58/ passenger</span>
                                                </div>
                                                <p style="font-size: 12px;">Please note that this is a "guarantee" rate. Guests who book a suite cabin guarantee are guaranteed a suite and will receive the cabin assignment when checking in for the cruise. Guarantee cabin may be located anywhere on this ship and are not associated with a more...</p>
                                            </div>
                                        </div>
                                        <div class="clearfix promo">
                                            <ul class="list-unstyled list-inline promo">
                                                <li><h5><strong>Special Offers:</strong></h5></li>
                                                <li><a href="#" class="">Great Rates + $50OBC</a></li>
                                            </ul>
                                        </div>
                                  </div>
                                  <div class="panel-footer">
                                    <div class="table-responsive cruiseTable">
                                        <table class="table">
                                            <thead style="background: transparent;">
                                                <tr>          
                                                    <th style="width: 5%;">Rate Type</th>
                                                    <th>Per Passenger</th>
                                                    <th>Cabin Rate</th>
                                                    <th>&nbsp;</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Lowest Available Fare</td>
                                                    <td>$386</td>
                                                    <td>$578</td>
                                                    <td><button class="btn btn-danger btn-xs">Select</button></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>
                        
                        <div role="tabpanel" class="tab-pane clearfix" id="cac">
                            <div class="clearfix">
                                    <div class="col-sm-8 topPart">

                                        <div class="form-group">
                                            <h4>Deck 11</h4>
                                            <img src="https://book.cruisedirect.com/content/images/Cruise/8/78/maindeck.png" class="img-responsive" alt="Image">
                                        </div>
                                        <div class="form-group">
                                            <div class="table-responsive">
                                                <h4>Select your cabin number:</h4>
                                                <table class="table table-hover table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th style="width:15%;">Cabin:</th>
                                                            <th>Deck:</th>
                                                            <th>&nbsp;</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>1120</td>
                                                            <td>Deck 11</td>
                                                            <td><button  class="btn btn-danger btn-xs">Select</button></td>
                                                        </tr>
                                                        
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-sm-4">
                                        <img src="https://book.cruisedirect.com/content/images/Cruise/8/78/Deck4/Deck7.png" class="img-responsive" alt="Image">
                                    </div>


                                </div>
                        </div>

                        <div role="tabpanel" class="tab-pane clearfix" id="ti">
                            <h4>Traveler Information</h4>  
                            <form method="post" id="quote_request" class="form-horizontal form-bordered">
                                <div class="form-group">
                                    
                                    <label class="control-label col-sm-3">Select Traveler:</label>
                                    <div class="col-sm-9">
                                        <select class="select2me form-control input-xlarge" name="cardType" id="" required>
                                            <option class="BF_combo_text" value="">Select Traveler</option>
                                            <option class="BF_combo_text" value="">Szilard Vegas</option>
                                            <option class="BF_combo_text" value="">James Shewmaker</option>
                                        </select>
                                    </div>
                                </div>

                            <h5>Passenger # 1: (Adult, Main Contact)</h5>
                                <div class="form-group">
                                    <label class="control-label col-sm-3"> First Name:</label>
                                    <div  class="col-sm-9">
                                       <input type='text' name='firstname' class="form-control input-medium" id='' value='' required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3"> Last Name:</label>
                                    <div  class="col-sm-9">
                                       <input type='text' name='lastname' class="form-control input-medium" id='' value='' required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3"> Title:</label>
                                    <div  class="col-sm-9">
                                       <select class="select2me form-control input-xlarge" name="title" id="" required>
                                           <option class="BF_combo_text" value="3" selected="selected">MR</option><option class="BF_combo_text" value="4">MRS</option><option class="BF_combo_text" value="2">MISS</option><option class="BF_combo_text" value="5">MS</option><option class="BF_combo_text" value="6">Master</option><option class="BF_combo_text" value="1">DR</option><option class="BF_combo_text" value="7">Reverend</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3"> Phone:</label>
                                    <div  class="col-sm-3">
                                       <select class="select2me form-control input-xlarge" name="numHeader" id="" required>
                                            
                                        </select>
                                    </div>
                                    <div  class="col-sm-6">
                                       <input type='number' name='phone' class="form-control input-medium" id='' value='' required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3">Email:</label>
                                    <div  class="col-sm-9">
                                       <input type='email' name='email' class="form-control input-medium" id='' value='' required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3">Gender:</label>
                                    <div  class="col-sm-9">
                                       <select class="select2me form-control input-xlarge" name="gender" id="" required>
                                            <option class="BF_combo_text" value="1" selected="selected">Male</option><option class="BF_combo_text" value="2">Female</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3">Date Of Birth:</label>
                                    <div  class="col-sm-3">
                                       <select class="select2me form-control input-xlarge" name="date" id="" required>
                                            
                                        </select>
                                    </div>
                                    <div  class="col-sm-3">
                                       <select class="select2me form-control input-xlarge" name="month" id="" required>
                                            
                                        </select>
                                    </div>
                                    <div  class="col-sm-3">
                                       <select class="select2me form-control input-xlarge" name="year" id="" required>
                                            
                                        </select>
                                    </div>
                                </div> 
                                <div class="form-group">
                                    <label class="control-label col-sm-3">Country of Citizenship:</label>
                                    <div  class="col-sm-9">
                                       <select class="select2me form-control input-xlarge" name="country" id="" required>
                                        
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3">State of Residence:</label>
                                    <div  class="col-sm-9">
                                       <select class="select2me form-control input-xlarge" name="state" id="" required>
                                        
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3">Past Passenger:</label>
                                    <div  class="col-sm-9">
                                       <input type='text' name='ppassenger' class="form-control input-medium" id='' value='' required>
                                    </div>
                                </div>

                                <div class="divider"></div>

                                <h4>Dining Options</h4>
                                <div class="form-group">
                                    <label class="control-label col-sm-3">Dining Seating:</label>
                                    <div  class="col-sm-9">
                                       <select class="select2me form-control input-xlarge" name="state" id="" required>
                                        
                                        </select>
                                    </div>
                                </div>
                                <div class="divider"></div>

                                <div class="clearfix mt20">
                                    <button type="submit" class="btn btn-primary pull-right">Continue</button>
                                </div>
                            </form>
                        </div>
                        <div role="tabpanel" class="tab-pane clearfix" id="bi">
                            <h4>Billing Information</h4>
                            <form method="post" id="quote_request" class="form-horizontal form-bordered" data-step="2">
                                <div class="form-group">
                                    <label class="control-label col-sm-3"> First Name:</label>
                                    <div  class="col-sm-9">
                                       <input type='text' name='firstname' class="form-control input-medium" id='' value='' required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3"> Last Name:</label>
                                    <div  class="col-sm-9">
                                       <input type='text' name='lastname' class="form-control input-medium" id='' value='' required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3"> Phone:</label>
                                    <div  class="col-sm-9">
                                       <input type='text' name='phone' class="form-control input-medium" id='' value='' required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3">Country:</label>
                                    <div  class="col-sm-9">
                                       <select class="select2me form-control input-xlarge" name="country" id="" required>
                                        
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3">State:</label>
                                    <div  class="col-sm-9">
                                       
                                       <select class="select2me form-control input-xlarge" name="state" id="" required>
                                        
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3">Zip:</label>
                                    <div  class="col-sm-9">
                                       <input type='text' name='zip' class="form-control input-medium" id='' value='' required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3">City:</label>
                                    <div  class="col-sm-9">
                                       <input type='text' name='city' class="form-control input-medium" id='' value='' required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3">Street:</label>
                                    <div  class="col-sm-9">
                                       <input type='text' name='Street' class="form-control input-medium" id='' value='' required>
                                    </div>
                                </div>
                                <h4>Payment Method</h4>
                                <div class="payment">
                                    <span class="fa fa-cc-mastercard"></span>
                                    <span class="fa fa-cc-visa"></span>
                                    <span class="fa fa-cc-amex"></span>
                                    <span class="fa fa-cc-jcb"></span>
                                    <span class="fa fa-cc-discover"></span>
                                </div>
                                <span>Your information is safe and secure</span>
                                <p>Your credit card will be charged on the following date: <span class="text-primary">12/21/2015</span></p>
                                <div class="form-group">
                                    <label class="control-label col-sm-3">Card Type:</label>
                                    <div class="col-sm-9">
                                        <select class="select2me form-control input-xlarge" name="cardType" id="" required>
                                            <option class="" value="">Select</option>
                                            <option class="" value="51|true">Visa</option>
                                            <option class="" value="39|true" selected="selected">MasterCard</option>
                                            <option class="" value="31|true">Discover</option>
                                            <option class="" value="125|true">JCB</option>
                                            <option class="" value="11|true">AmericanExpress</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3">Card Number:</label>
                                    <div  class="col-sm-9">
                                       <input type='text' name='cardNumber' class="form-control input-medium" id='' value='' required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3">Expiration:</label>
                                    <div  class="col-sm-9">
                                       <div class="row">
                                           <div class="col-sm-6">
                                                <select class="select2me form-control input-xlarge" name="day" id="" required>
                                                    <option class="" value="-1" selected="selected">month</option>
                                                    <option class="" value="01">January</option>
                                                    <option class="" value="02">February</option>
                                                    <option class="" value="03">March</option>
                                                    <option class="" value="04">April</option>
                                                    <option class="" value="05">May</option>
                                                    <option class="" value="06">June</option>
                                                    <option class="" value="07">July</option>
                                                    <option class="" value="08">August</option>
                                                    <option class="" value="09">September</option>
                                                    <option class="" value="10">October</option>
                                                    <option class="" value="11">November</option>
                                                    <option class="" value="12">December</option>
                                                </select>
                                           </div>
                                           <div class="col-sm-6">
                                                <select class="select2me form-control input-xlarge" name="year" id="" required>
                                                    <option class="" value="-1" selected="selected">year</option>
                                                    <option class="" value="2015">2015</option>
                                                    <option class="" value="2016">2016</option>
                                                    <option class="" value="2017">2017</option>
                                                    <option class="" value="2018">2018</option>
                                                    <option class="" value="2019">2019</option>
                                                    <option class="" value="2020">2020</option>
                                                    <option class="" value="2021">2021</option>
                                                    <option class="" value="2022">2022</option>
                                                    <option class="" value="2023">2023</option>
                                                    <option class="" value="2024">2024</option>
                                                    <option class="" value="2025">2025</option>
                                                    <option class="" value="2026">2026</option>
                                                    <option class="" value="2027">2027</option>
                                                    <option class="" value="2028">2028</option>
                                                    <option class="" value="2029">2029</option>
                                                </select>
                                            </div>
                                       </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3">Security Code:</label>
                                    <div  class="col-sm-9">
                                       <input type='text' name='' class="form-control input-medium" id='' value='' required>
                                    </div>
                                </div>

                                <div class="clearfix mt20">
                                    <button type="submit" class="btn btn-primary pull-right">Continue</button>
                                </div>
                            </form>
                        </div>
                        <div role="tabpanel" class="tab-pane clearfix" id="rc">
                            <h4>Review & Confirmation</h4>
                            <div class="media" style="padding:0px 15px;">
                                <a class="pull-left" href="#">
                                    <img style="width:100px; margin-bottom: 0px;" class="media-object img-responsive thumbnail" src="http://image1.urlforimages.com/Images/1231501/100x100/475487017.jpg" alt="image">
                                </a>
                                <div class="media-body">
                                    <div class="panel-title">
                                       Cruise - 4 Nights Bahamas Cruise from Miami - Roundtrip
                                    </div>
                                    <p>Norwegian Cruise Line</p>
                                    <p>Norwegian Sky</p>
                                </div>
                            </div>
                            <div class="bookingContent">
                                <ul class="list-unstyled">
                                    <li role="separator" class="divider"></li>
                                    <li>Cruise Line:<span>Norwegian Cruise Line</span></li>
                                    <li>Ship Name:<span>  Norwegian Sky</span></li>
                                    <li>Start Date:<span> Dec 31, 2015</span></li>
                                    <li>End Date:<span>   Jan 04, 2016</span></li>
                                    <li>Port of Departure:<span>  Miami, Fl</span></li>
                                    <li>Cabin Category:<span> OX - OceanView Porthole Window</span></li>
                                    <li>Cabin Number:<span>   4001</span></li>
                                    <li>Deck:<span>   Biscayne</span></li>
                                    <li>Passengers:<span> 2 Adult(s), 0 Children</span></li>
                                </ul>
                                <div class="divider"></div>
                                <h4>Cancellation Policy</h4>
                                <iframe width="100%" style="border:1px solid #E5E5E5;" src="https://image2.urlforimages.com/CS/$Templates/thf-ref$/@Websites/TouricoHolidays@/Modules/Customizable/Terms/En/termsAnsConditions.htm" class="BF_Terms"></iframe>   
                                <div class="divider"></div>
                                <h4>Passengers Information</h4>
                                <ul class="list-unstyled">
                                    <li role="separator" class="divider"></li>
                                    <li>Passenger # 1:(Adult, Main Contact)</li>
                                    <li>Name: <span>Szilard Vegas</span></li>
                                    <li>Gender: <span>Male</span></li>
                                    <li>Citizenship: <span>Tajikistan</span></li>
                                    <li>Date Of Birth: <span>Feb 23, 1940</span></li>
                                    <li>Phone1: <span>-7027120680</span></li>
                                    <li role="separator" class="divider"></li>
                                    <li>Passenger # 2:</li>
                                    <li>Name: <span>Terry Allie</span></li>
                                    <li>Gende: <span>rMale</span></li>
                                    <li>Citizenship: <span>USA</span></li>
                                    <li>State of Residence: <span>AA</span></li>
                                    <li>Date Of Birth: <span>Dec 23, 1942</span></li>
                                </ul>
                                <div class="divider"></div>
                                <h4>Summary of Billing Information</h4>
                                <ul class="list-unstyled">
                                    <li class="text-danger">Price:<span><strong>$113.25</strong></span></li>
                                    <li>Your credit card will be charged on the following date:<span><strong>Dec 23, 2015</strong></span></li>
                                    <li role="separator" class="divider"></li>
                                    <li>Name:<span>    James Shewmaker</span></li>
                                    <li>Credit Card:<span> Mastercard</span></li>
                                    <li>Credit Card Number:<span>  XXXXXXXXXX5100</span></li>
                                    <li>Expiration:<span>  February, 2016</span></li>
                                    <li>Address:<span> asd ,asd ,Arizona 23154 USA</span></li>
                                    <li>Phone:<span>   1-4043142902</span></li>
                                </ul>
                                <div class="divider"></div>
                                <h4>Terms & Conditions</h4>
                                <iframe width="100%" style="border:1px solid #E5E5E5;" src="terms.html"></iframe>
                                <form method="post" id="quote_request" class="form-horizontal form-bordered">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" value="">
                                            I have read and agree with the Terms & Conditions stated above
                                        </label>
                                    </div>
                                    <div class="clearfix mt20">
                                        <button type="submit" class="btn btn-primary pull-right">Finish</button>
                                    </div>
                                </form>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



 