<?php

error_reporting(E_ALL);

ini_set('display_errors', 1);

ini_set('memory_limit','512M');

$datetime1 = new DateTime($check_in);

$datetime2 = new DateTime($check_out);

$interval = $datetime1->diff($datetime2);

$totdays = $interval->days;

$days = array();



for($di = 0 ; $di < $totdays ; $di++)

{

    $days[$di] = date('D', strtotime("+".$di." days", strtotime($check_in)) );

}



$days = json_encode($days);



$hotel_location_name_from_db = $location_names;

$location_name_from_db_count = count($location_names);

$first_nine_values = array();



$location_names_count = count($location_names);



$location_html = '';



for ($i = 0; $i < $location_names_count; $i++) {

    $location_html .= <<<EOM

                        <tr>



                            <td style="width:10%">

                                  <input type="checkbox" id="{$location_names[$i]}" value="{$location_names[$i]}" name="" class="location_names_checkbox">

                             </td>

                            <td style="width:90%">

                                  <label for="{$location_names[$i]}">{$location_names[$i]}</label>

                            </td>

                        </tr>

EOM;

}



$hotel_list_count = count($hotel_list);

$hotel_location_name = array();

for ($j = 0; $j < $hotel_list_count; $j++) {

    $a_hotel = $hotel_list[$j];



    if (isset($hotel_location_name[$a_hotel['hotel_location_name']]))

    {

        $hotel_location_name[$a_hotel['hotel_location_name']] ++;

    }

    else

    {

        $hotel_location_name[$a_hotel['hotel_location_name']] = 1;

    }

}

$desarray = array();

$rangede = array();

foreach($hotel_list as $key => $singlehot)

{

    $desarray[] = array("miles" => $singlehot['distanceFromAirport']);

    $rangede[$key] = $singlehot['distanceFromAirport'];

}

if(count($rangede) > 0)

{

    $minrrand = array_keys($rangede, min($rangede));

}

else

{

    $minrrand = array();

}

if(isset($minrrand[0]))

{

    $minrrand = $rangede[$minrrand[0]];

}

else

{

    $minrrand = 0;

}

if(count($rangede) > 0)

{

    $maxmrrang = array_keys($rangede, max($rangede));

}

else

{

    $maxmrrang = array();

}

if(isset($maxmrrang[0]))

{

    $maxmrrang = $rangede[$maxmrrang[0]];

}

else

{

    $maxmrrang = 0;

}



$hotel_location_chk_html = <<<EOM

                            <tr>

                                <td>

                                  <h5>Las Vegas Locations</h5>

                                                <table class="table">

                                                    <tbody>

EOM;



$hotel_location_chk_html = "";

$hotel_location_other_chk_html = "";

$hotel_location_other_not_in_db_chk_html = "";

if(count($hotel_location_name) > 0)

{

    arsort($hotel_location_name);

}

$counter = 0;

foreach ($hotel_location_name as $key => $value) {

    $counter++;

    $location_found_in_db = 0;

    for ($i = 0; $i < $location_name_from_db_count; $i++) {

        if ($location_names[$i] == $key) {

            $location_found_in_db = 1;

        }

    }

    if (1 == $location_found_in_db) {

        if ($counter < 9) {

            $first_nine_values[] = $key;



            $hotel_location_chk_html .= <<<EOM

                                <tr>

                                    <td style="width:10%">

                                        <input type="checkbox" class="hotel_search_location_chkbox" value="{$key}" >

                                    </td>

                                    <td style="width:90%">

                                        <label for="">{$key}</label>

                                    </td>

                                </tr

EOM;

        } else {

            $hotel_location_other_chk_html = <<<EOM

                                <tr>

                                    <td style="width:10%">

                                        <input type="checkbox" id="hotel_location_other_chk" class="hotel_search_location_chkbox" value="">

                                    </td>

                                    <td style="width:90%">

                                        <label for="">Other location</label>

                                    </td>

                                </tr

EOM;

        }

    } else {

        if (strlen($key) > 0) {





            $hotel_location_other_not_in_db_chk_html .= <<<EOM

                                <tr>

                                    <td style="width:10%">

                                        <input type="checkbox" class="hotel_search_location_chkbox" value="{$key}">

                                    </td>

                                    <td style="width:90%">

                                        <label for="">{$key}</label>

                                    </td>

                                </tr

EOM;

        }

    }



    $location_found_in_db = 0;

}



$hotel_brands_info = array();

for ($i = 0; $i < $hotel_list_count; $i++) {

    $an_hotel_list = $hotel_list[$i];



    $brandName = $an_hotel_list['hotel_brand_name'];



    if (isset($hotel_brands_info[$brandName])) {

        $hotel_brands_info[$brandName][0] ++;



        if ($hotel_brands_info[$brandName][1] > $an_hotel_list['only_min_price_inc_tax']) {

            $hotel_brands_info[$brandName][1] = $an_hotel_list['only_min_price_inc_tax'];

        }

        if ($hotel_brands_info[$brandName][2] < $an_hotel_list['only_min_price_inc_tax']) {

            $hotel_brands_info[$brandName][2] = $an_hotel_list['only_min_price_inc_tax'];

        }

    } else {

        $hotel_brands_info[$brandName][0] = 1;

        $hotel_brands_info[$brandName][1] = $an_hotel_list['only_min_price_inc_tax'];

        $hotel_brands_info[$brandName][2] = $an_hotel_list['only_min_price_inc_tax'];

    }

}

$hotel_info_1d = array();

foreach ($hotel_brands_info as $index => $hotel_array) {

    $hotel_info_1d[$index] = $hotel_array[0];

}

if(count($hotel_info_1d) > 0)

    arsort($hotel_info_1d);



$hotel_brand_show = 0;

$hotel_brand_html = "";

$hotel_info_1d_count = count($hotel_info_1d);

foreach ($hotel_info_1d as $index => $value) {

    $hotel_brand_show++;

    $brnd =  number_format((float)$hotel_brands_info[$index][1], 2, '.', '');

    $brndname = $index ;

    if(trim($brndname) == '')

    {

        $brndname = 'unknown';

    }

    $hotel_brand_html .= <<<EOM

                            <tr>

                                <td class="">

                                    <input id="" name="" type="checkbox" class="hotel_brand_checkbox" value="{$index}"> </td>

                                <td class="">

                                    <label for="">

                                    <span class="">({$hotel_brands_info[$index][0]})</span> </label>

                                </td>

                                <td class="" style="width:60%;">

                                    {$brndname}

                                </td>

                                <td class="">

                                    <label for="" id="">\${$brnd} <!-- - \${$brnd} --></label>

                                </td>

                            </tr>

EOM;

}

$aminities_count_list = array();

for ($i = 0; $i < $hotel_list_count; $i++) {

    $an_hotel_list = $hotel_list[$i];



    $amenities = $an_hotel_list['aminities'];



    $aminities_count = count($amenities);



    for ($j = 0; $j < $aminities_count; $j++) {

        if (isset($aminities_count_list[$amenities[$j]])) {

            $aminities_count_list[$amenities[$j]] ++;

        } else {

            $aminities_count_list[$amenities[$j]] = 1;

        }

    }

}

if(count($aminities_count_list) > 0)

    arsort($aminities_count_list);



$show_first_num = 0;

$amenities_left_panel_html = "";



foreach ($aminities_count_list as $amenities_name => $amenities_count) {

    $show_first_num++;

    if ($show_first_num > 10)

        break;



    if ($amenities_count > 0) {

        $amenities_left_panel_html .= <<<EOM

                            <tr>

                                <td class="">

                                    <input id="" name="" type="checkbox" value="{$amenities_name}" class="amenities_checkbox">

                                </td>

                                <td class="">

                                    <label for=""><span class="">({$amenities_count})</span></label>

                                </td>

                                <td class="" style="width:60%;">

                                    {$amenities_name}

                                </td>

                            </tr>





EOM;

    }

}



function GetStarValues($hotelSet, &$max_value, &$min_value) {

    $temp = $hotelSet['room_type_list'][0]['occupancy_list'];
    $hotel_price = 0;

    foreach ($temp as $t){

        $hotel_price = $hotel_price + $t['available_list'][0]['total_price_inc_tax'];

    }

    $hotel_price = floatval($hotel_price);

    if ($max_value < $hotel_price) {

        $max_value = $hotel_price;

    }



    if ($min_value > $hotel_price) {

        $min_value = $hotel_price;

    }

}



$star_infos = array();

for ($index = 0; $index < 9; $index++)

{

    array_push($star_infos, 0);

    array_push($star_infos, 0);

    array_push($star_infos, 20000000);

}



$hotel_list_count = count($hotel_list);

$one_star_count = $one_half_star_count = $two_star_count = $two_half_star_count = $three_star_count = $three_half_star_count = $four_star_count =  $four_half_star_count = $five_star_count = 0;

for ($i = 0; $i < $hotel_list_count; $i++)

{

    $a_hotel = $hotel_list[$i];



    if ('5' == $a_hotel['star_level'])

    {

        $star_infos[0] ++;

        GetStarValues($a_hotel, $star_infos[1], $star_infos[2]);



        $five_star_count++;

        GetStarValues($a_hotel, $five_star_max, $five_star_min);

    }

    if ('4.5' == $a_hotel['star_level'])

    {

        $star_infos[3] ++;

        GetStarValues($a_hotel, $star_infos[4], $star_infos[5]);



        $four_half_star_count++;

        GetStarValues($a_hotel, $four_half_star_max, $four_half_star_min);

    }

    if ('4' == $a_hotel['star_level'])

    {

        $star_infos[6] ++;

        GetStarValues($a_hotel, $star_infos[7], $star_infos[8]);



        $four_star_count++;

        GetStarValues($a_hotel, $four_star_max, $four_star_min);

    }

    else if ('3.5' == $a_hotel['star_level'])

    {

        $star_infos[9] ++;

        GetStarValues($a_hotel, $star_infos[10], $star_infos[11]);



        $three_half_star_count++;

        GetStarValues($a_hotel, $three_half_star_max, $three_half_star_min);

    }

    else if ('3' == $a_hotel['star_level'])

    {

        $star_infos[12] ++;

        GetStarValues($a_hotel, $star_infos[13], $star_infos[14]);



        $three_star_count++;

        GetStarValues($a_hotel, $three_star_max, $three_star_min);

    }

    else if ('2.5' == $a_hotel['star_level'])

    {

        $star_infos[15] ++;

        GetStarValues($a_hotel, $star_infos[16], $star_infos[17]);



        $two_half_star_count++;

        GetStarValues($a_hotel, $two_half_star_max, $two_half_star_min);

    }

    else if ('2' == $a_hotel['star_level'])

    {

        $star_infos[18] ++;

        GetStarValues($a_hotel, $star_infos[19], $star_infos[20]);



        $two_star_count++;

        GetStarValues($a_hotel, $two_star_max, $two_star_min);

    }

    else if ('1.5' == $a_hotel['star_level'])

    {

        $star_infos[21] ++;

        GetStarValues($a_hotel, $star_infos[22], $star_infos[23]);



        $one_half_star_count++;

        GetStarValues($a_hotel, $one_half_star_max, $one_half_star_min);

    }

    else if ('1' == $a_hotel['star_level'])

    {

        $star_infos[24] ++;

        GetStarValues($a_hotel, $star_infos[25], $star_infos[26]);

        $one_star_count++;

        GetStarValues($a_hotel, $one_star_max, $one_star_min);

    }

}



//echo '<pre>';print_r($star_infos);exit;



$star_desc = "";

$rating_counter = 5.5;

for ($i = 0; $i < 27; $i+=3) {

    $rating_counter -= 0.5;



    $formatted_rating_counter = $rating_counter - intval($rating_counter);

    if ($formatted_rating_counter > 0) {

        $formatted_rating_counter = $rating_counter;

    } else {

        $formatted_rating_counter = intval($rating_counter);

    }



    if ($star_infos[$i] > 0)

    {

        $info2nd = 	number_format((float)$star_infos[$i + 1], 2, '.', '');

        $info1st = 	number_format((float)$star_infos[$i + 2], 2, '.', '');

        $star_desc .= <<<EOM

                        <tr>

                            <td class="NH_filter_stars_line">

                                <input id="chkstarRatingFilter{$formatted_rating_counter}" name="chkstarRatingFilter" type="checkbox" value="{$rating_counter}" class="hotel_star_ratings">

                            </td>

                            <td class="NH_filter_stars_line">

                                <label for="chkstarRatingFilter{$formatted_rating_counter}">

                                <span class="NH_filter_number_pad">({$star_infos[$i]})</span> </label>

                            </td>

                            <td class="NH_filter_stars_line">

                                <input type="text" value="{$formatted_rating_counter}" class="srRating2 hidden">

                            </td>

                            <td class="NH_filter_stars_line NH_textAlign" width="100%">

                                <label for="chkstarRatingFilter{$formatted_rating_counter}" id="lblstarRatingFilter{$formatted_rating_counter}">\${$info1st} - \${$info2nd}</label>

                            </td>

                        </tr>

EOM;

    }

}



?>

<style>

    .discount_tr {

        /* border-bottom: 1px solid #ddd;

         border-top: 1px solid #ddd;*/

        font-weight: bold;

    }

    .discount_tr > td {

        border: none !important;

    }

    .speciel_deal_discount {

        background: #d43f3a;

        color: #fff;

        padding: 5px 5px ;

        border-radius: 3px;

        margin: 0 8px 0 0;

        box-shadow: 0 0 12px 0 rgba(212,63, 58, 0.50);

    }

</style>

<div class="width-row">

<div class="clearfix">

<div class="col-sm-4">

<div class="sWidgets">

<!--<form method="post" id="hotel_search_all_options_form"> -->

<form action="<?php echo base_url(); ?>view_search_result" method="get" id="quote_request">

<div class="clearfix">

    <div class="form-group">

        <label class="control-label col-md-12">Where are you going?</label>

        <div class="col-md-12">

            <input type="text" name="locationName" value="<?php echo $locationName ?>" id="locationName" class="form-control" style="height: 33px; text-align: center;" >

            <input type='hidden' name='locationId' value="<?php echo $locationId ?>" id='locationId'>

        </div>

    </div>

</div>

<div class="clearfix">

    <div class="col-md-6">

        <div class="form-group">

            <label class="control-label col-md-12">Check-in</label>

            <div class="">

                <input type="text" name="check_in" value="<?php echo $check_in ?>"  class="form-control icon-pick check_in" id="check_in" style="text-align: left;">

            </div>

        </div>

    </div>

    <div class="col-md-6">

        <div class="form-group">

            <label class="control-label col-md-12">Check-out</label>

            <div class="">

                <input type="text" name="check_out" value="<?php echo $check_out ?>" class="form-control icon-pick check_out" id="check_out" style="text-align: left;">

            </div>

        </div>

    </div>

</div>



<div class="clearfix">

    <div class="col-md-12">

        <div class="form-group">

            <label class="control-label col-md-12">Rooms</label>

            <div class="">

                <select class="select2me form-control input-xlarge rooms" name='roomNo' id='roomNo' style="text-align: center;">

                    <?php

                    for ($index = 1; $index <= 9; $index++) {

                        if ($roomNo == $index)

                            $selected_room = 'selected';

                        else {

                            $selected_room = '';

                        }

                        echo "<option value='{$index}' " . $selected_room . ">{$index}</option>";

                    }

                    ?>

                </select>

            </div>

        </div>

    </div>

</div>



<div class="clearfix"  id="hotel_rooms_id">

    <?php

    if (empty($roomNo))

        $roomNo = 1;

    $room_actual_num = 0;

    for ($num_rooms = 0; $num_rooms < $roomNo; $num_rooms++) {

        $room_actual_num++;

        ?>

        <div class="clearfix">

            <div class="col-sm-3" style="margin-right:20px;">

                <p style="margin: 36px 0px 0px 0px;text-align: right">Room<?php echo $room_actual_num ?></p>

            </div>

            <div class="col-sm-4">

                <div class="form-group">

                    <label class="control-label col-sm-4">Adults</label>

                    <div class="sm-4">

                        <select class="select2me form-control input-xlarge" name="numberOfAdults[]" id="numberOfAdults" style="text-align: center;">

                            <?php for ($adults_index = 0; $adults_index < 7; $adults_index++): ?>

                                <option value="<?php echo $adults_index ?>" <?php if ($adults_index == $numberOfAdults[$num_rooms]) echo 'selected' ?>><?php echo $adults_index ?></option>

                            <?php endfor; ?>

                        </select>

                    </div>

                </div>

            </div>

            <div class="col-sm-4">

                <div class="form-group">

                    <label class="control-label col-sm-12">Children</label>

                    <div class="">

                        <select class="select2me form-control input-xlarge numberOfChild" name="numberOfChild[]" id="numberOfChild" style="text-align: center;">

                            <?php for ($child_index = 0; $child_index < 4; $child_index++): ?>

                                <option for_room="<?php echo $room_actual_num ?>" value="<?php echo $child_index ?>" <?php if ($child_index == $numberOfChild[$num_rooms]) echo 'selected'; ?>><?php echo $child_index ?></option>

                            <?php endfor; ?>

                        </select>

                    </div>

                </div>

            </div>

        </div>

    <?php } ?>

</div>

<br/>

<div class="clearfix chAges" <?php if (empty($childAge)) { ?> style="display:none" <?php } else { ?>style="display:block" <?php } ?>>

    <h4 class="children_ages_heading" style="margin: 5px 0px 5px 15px;">Children Ages:</h4>

    <?php

    $childAgesArCount = 0;

    $chpos = 0;

    for ($chidAgesRoomNo = 1; $chidAgesRoomNo < 10; $chidAgesRoomNo++):

        ?>

        <div class="clearfix" id="room<?php echo $chidAgesRoomNo ?>">

            <?php

            if($chidAgesRoomNo <= $roomNo)

            {

                $nuofch = $numberOfChild[$chidAgesRoomNo -1];

                ?>

                <div class="col-sm-12">

                    Room <?php echo $chidAgesRoomNo; ?>

                </div>

                <?php

                if (!empty($childAge[$childAgesArCount]))

                    for ($child_row = 0; $child_row < $nuofch; $child_row++)

                    {

                        ?>

                        <div class="col-sm-4">



                            <label class="control-label">Child <?php echo $child_row+1; ?></label>

                            <div class="form-group">

                                <select id="childAge" class="select2me form-control input-xlarge rooms" style="text-align: center;" name="childAge[]">

                                    <?php for ($child_age_index = 1; $child_age_index < 18; $child_age_index++): ?>

                                        <option value="<?php echo $child_age_index ?>" <?php if ($child_age_index == $childAge[$childAgesArCount]) echo 'selected';

                                        else echo ''; ?>><?php echo $child_age_index ?></option>

                                    <?php endfor; ?>

                                </select>

                            </div>

                        </div>

                        <?php

                        $childAgesArCount++;

                    }

            }

            ?>

        </div>

    <?php endfor; ?>

</div>





<div class="clearfix">

    <label class="control-label col-md-12">OPTIONAL FIELDS:</label>

    <div class="col-md-12">

        <div class="form-group">

            <label class="control-label col-md-12">Hotel Name</label>

            <div class="">

                <input type="text" name="hotelName" value="<?php echo $hotelName; ?>" id="hotelName" class="form-control" style="height: 33px; text-align: center;" >

            </div>

        </div>

    </div>

    <div class="col-md-12">

        <div class="">

            <label class="control-label col-md-12">Star rating</label>

            <div class="">

                <select style='text-align: center;'class="select2me form-control input-xlarge" name='hotelRating' id='hotelRating'>

                    <option value="0" <?php if ($hotelRating == 0) echo 'selected' ?>>Show All</option>

                    <option value="2" <?php if ($hotelRating == 2) echo 'selected' ?>>2 stars or better</option>

                    <option value="3" <?php if ($hotelRating == 3) echo 'selected' ?>>3 stars or better</option>

                    <option value="4" <?php if ($hotelRating == 4) echo 'selected' ?>>4 stars or better</option>

                    <option value="5" <?php if ($hotelRating == 5) echo 'selected' ?>>5 stars only</option>

                </select>

            </div>

        </div>

    </div>

    <div class="col-xs-2" style="margin-top:20px;float:right;margin-right:50px;">

        <button type="submit" id="search_hotel_with_all_options" class="btn .btn-custom-m .btn-custom search_hotel_with_all_options" style="background: #009688;border: 1px solid #009688;color:  #fff;">Search</button>

    </div>

</div><!-- clearfix -->

</form>

</div>

<div class="panel panel-default cbg">

<div class="panel-heading">

    <h3 class="panel-title bold">Filter Results</h3>

</div>

<div class="panel-body">

<div id="LocationAndLandmarks">

<div id="locationsLandmarksFilterContent">

<div id="priceRange" class="clearfix">

    <h4 class="tc1">Price Range</h4>

    <div class="col-md-12">

        <div class="range1"></div>

        <input type="text" id="amount" readonly>

    </div>

</div>

<div id="ratingFilter" class="clearfix bt1" style="padding: 0px 0px; margin-top: 10px;">

    <div id="starRatingFilterContent">

        <h4 class="tc1" style="float: left;">Rating</h4>

        <div class="ASelector" style="float: right;">

            <span> <a class="hotel_search_select_all">Select All</a> </span>

            <span class="">|</span>

            <span> <a class="">Clear</a> </span>

        </div>

        <table class="table">

            <tbody>

            <?php echo $star_desc ?>

            </tbody>

        </table>

    </div>

</div>



<div id="nameSearchFilter" class="clearfix bt1" style="padding: 0px 0px; margin-top: 10px;">

    <h4 class="tc1" style="margin-bottom:10px;">Search by hotel name</h4>

    <div class="clearfix">

        <div class="col-xs-8">

            <div class="row">

                <div id="" class="form-group">

                    <input type="text" name="" id="input" class="form-control input-xlarge search_by_hotel_name_text_box" value="" >

                </div>

            </div>

        </div>

        <div class="col-xs-2">

            <button type="button" id="search_by_hotel_name_button" class="btn .btn-custom search_by_hotel_name_button" style="background: #009688;border: 1px solid #009688;color:  #fff;">Search</button>

        </div>

    </div>

    <div class="clearfix">



    </div>

</div>

<!--<div id="relationToLocations" class="clearfix bt1" style="padding: 0px 0px; margin-top: 10px;">

                                <table class="table">

                                    <tbody>

                                        <tr>

                                            <td>

												<div class="row">

													<?php /*

														$pos = strpos($locationName, ',');

														$loname = substr($locationName,0,$pos);

													*/?>

													<div class="col-md-6"><h5><?php /*echo $loname; */?> Locations</h5></div>

                                                    <div class="col-md-6">

                                                        <div class="ASelector"  style="float: right;">

                                                            <span> <a class="hotel_location_select_all">Select All</a> </span>

                                                            <span class="">|</span>

                                                            <span> <a class="hotel_location_clear_all">Clear</a> </span>

                                                        </div>

                                                    </div>

												</div>

                                                <table class="table">

                                                    <tbody>

														<?php /*echo $hotel_location_chk_html */?>

                                                    </tbody>

                                                </table>

                                            </td>

                                        </tr>

                                        <?php

/*                                        if (0 < strlen($hotel_location_other_chk_html)) {

                                            */?>

                                            <tr>

                                                <td>

                                                    <h5>Other</h5>

                                                    <table class="table">

                                                        <tbody>

															<?php /*echo $hotel_location_other_chk_html */?>

                                                        </tbody>

                                                    </table>

                                                </td>

                                            </tr>

                                        <?php /*} */?>

                                        <?php

/*                                        if (strlen($hotel_location_other_not_in_db_chk_html) > 0) {

                                            */?>

                                            <tr>

                                                <td>

                                                    <h5>Other</h5>

                                                    <table class="table">

                                                        <tbody>

															<?php /*echo $hotel_location_other_not_in_db_chk_html */?>

                                                        </tbody>

                                                    </table>

                                                </td>

                                            </tr>

										<?php /*} */?>

                                    </tbody>

                                </table>

                            </div>

                            <div id="brands" class="clearfix bt1" style="padding: 0px 0px; margin-top: 10px;">

                                <div id="brandFilterContent">

                                    <h4 class="tc1"  style="float: left;">Brands</h4>

                                    <div class="ASelector"  style="float: right;">

                                        <span> <a class="hotel_brand_select_all">Select All</a> </span>

                                        <span class="">|</span>

                                        <span> <a class="hotel_brand_clear_all">Clear</a> </span>

                                    </div>

                                    <table class="table">

                                        <tbody>

											<?php /*echo $hotel_brand_html */?>

                                        </tbody>

                                    </table>

                                </div>

                            </div>-->

</div>

</div>

</div>

</div>

</div>

<div class="col-md-4">

    <div class="hotel_serch_tab_btn active">

        <a href="<?php echo $switch_tab_hotel_url ?>">Hotel search result</a>

    </div>

</div>

<div class="col-md-4">

    <div class="hotel_serch_tab_btn">

        <a href="<?php echo $switch_tab_hotel_map_url ?>">Hotel map view</a>

    </div>

</div>

<div class="col-sm-8">

    <div class="row">

        <div class="search_result_filter">

            <div style="width: 63%;padding: 0px 15px;text-align: left; margin-top: 26px; position: absolute;" id="total_hotels_found_id">

                <?php

                if(count($hotel_list) > 0)

                {

                    ?>

                    <p>We found <b style="font-size: 15px;"><?php echo count($hotel_list); ?></b> hotels for you!</p>

                <?php

                }

                else

                {

                    ?>

                    <p>No result found!</p>

                <?php

                }

                ?>

            </div>

            <div style="width: 63%;text-align: right; margin-top: 26px; position: absolute;">

                <p>Sort by :</p>

            </div>

            <div class="form-group" style="width: 33%;margin-left: 67%;">

                <div class="">

                    <div class=" select2-container select2me form-control input-xlarge numberOfChild" id="s2id_numberOfChild" style="text-align: center;">

                        <input class="select2-focusser select2-offscreen" type="text" id="s2id_autogen3">

                    </div>

                    <select class="sort_hotel select2me form-control input-xlarge numberOfChild select2-offscreen" name="numberOfChild[]" id="numberOfChild" style="text-align: center;" tabindex="-1">

                        <option value="1">Price ( Low to high )</option>

                        <option value="2">Price ( High to low )</option>

                        <option value="3">Star rating ( high to low )</option>

                    </select>

                </div>

            </div>

        </div>

    </div>

</div>

<div class="col-sm-8" id="rate_policy">

<div class="row sResult" id="content"">

<script type="text/javascript">

    var dynamic_occupancy_lists = [];

</script>

<?php
$overAllPricesSet = array();
$initalHotels = getSortDataByUniqueSessionID($uniqueSession);
foreach($initalHotels as $i => $singlehotel)

{

    //echo $roomNo;

    //pre($singlehotel);

    $temp = $singlehotel['room_type_list'][0]['occupancy_list'];

    $my_price = 0;

    foreach ($temp as $t){

        $my_price = $my_price + $t['available_list'][0]['total_price_inc_tax'];

    }

    $room_type_list = $singlehotel['room_type_list'];

    $room_type_rows_html = "";

    $global_min = 2000002;

    $count = 1;

    $temp_prices = array();



    foreach($room_type_list as $room_type_index => $sin_room_type_list)

    {

        //echo $room_type_index;

        //pre($sin_room_type_list['occupancy_list']);



        $temp_count = count($sin_room_type_list['occupancy_list']);



        $temp_sum = 0;

        if(count($sin_room_type_list['occupancy_list'])==$roomNo){

            foreach($sin_room_type_list['occupancy_list'] as $temp_key => $temp_val){

                $temp_sum = $temp_sum + $sin_room_type_list['occupancy_list'][$temp_key]['available_list'][0]['total_price_inc_tax'];

            }

        } else {

            $tc = 0;

            foreach($sin_room_type_list['occupancy_list'] as $temp_key => $temp_val){

                $temp_sum = $temp_sum + $sin_room_type_list['occupancy_list'][$temp_key]['available_list'][0]['total_price_inc_tax'];

                $tc++;

            }

//                echo $tc;

//                echo '<br>';

            $temp_sum = $temp_sum/$tc*$roomNo;

        }

        //echo $temp_sum;

        array_push($temp_prices,$temp_sum);

        //echo '<br>';



    }

    //print_r($temp_prices);
    $overAllPricesSet[] = $temp_prices;

    $price_index = 0;

    foreach($room_type_list as $room_type_index => $sin_room_type_list) {

        //echo $room_type_index;

        $lowest_price = number_format($temp_prices[0], 2);

        $discount = $sin_room_type_list['discount'];

        $room_type_index_for_lowest_price = 0;

        $occupancy_lists = $sin_room_type_list['occupancy_list'];

        ?>

        <script type="text/javascript">

            dynamic_occupancy_lists['<?php echo $singlehotel['hotelId'].$i.$room_type_index; ?>'] = <?php echo json_encode($occupancy_lists); ?>;

        </script>

        <?php

        $temp_RoomId = $sin_room_type_list['RoomId'];

        $temp_RoomTypeId = $sin_room_type_list['RoomTypeId'];

        $hasfree = false;

        $hasfreebreakfast = "";

        $freesup = "";

        foreach ($sin_room_type_list['supp'] as $single_supp) {

            if ($single_supp['price'] == "0.00") {

                $freesup .= "<tr class='discount_tr collapse demo_".$singlehotel['hotelId']."'><td colspan='5'><img src='https://www.concretereading.co.uk/images/tick.svg' width='20' height='20' />Free " . $single_supp['suppName'] . "</td></tr>";

            }

        }


        $min_price = 2000;

        $addedboardbases = array();

        foreach ($occupancy_lists as $snigle_occupancy_lists) {

            $available_lists = $snigle_occupancy_lists['available_list'];

            foreach ($available_lists as $single_available_lists) {

                foreach ($single_available_lists['BoardBases'] as $single_BoardBases) {

                    if (

                        $single_BoardBases['bbPublishPrice'] == 0 &&

                        !in_array($single_BoardBases['bbId'], $addedboardbases)

                    ) {

                        array_push($addedboardbases, $single_BoardBases['bbId']);

                        $hasfreebreakfast .= "<tr class='discount_tr collapse demo_".$singlehotel['hotelId']."'><td colspan='5'><img src='https://www.concretereading.co.uk/images/tick.svg' width='20' height='20' />Free " . $single_BoardBases['bbName'] . "</td></tr>";

                        $hasfree = true;

                    }

                }

                if ($min_price > $sin_room_type_list['occupancy_list']['Room 1']['available_list'][0]['total_price_inc_tax']) {

                    $min_price = number_format($my_price, 2);

                    $room_type_index_for_lowest_price = $room_type_index;

                }

                $min_price = number_format($temp_prices[$price_index], 2);

            }

        }

        $price_index++;

        if ($global_min > $min_price) {

            $global_min = $min_price;

            $lowest_roomId = $temp_RoomId;

            $lowest_roomTypeId = $temp_RoomTypeId;

            $modal_book_now_url = base_url() . 'request_quote?hotelId=' . $singlehotel['hotelId'] . '&roomId=' . $lowest_roomId . '&roomTypeId=' . $lowest_roomTypeId . '&stp=1';

        }

        $room_type_availability = ($sin_room_type_list['isAvailable'] == "true") ? "Available" : "On Request";

        $roomId = $sin_room_type_list['RoomId'];

        $roomTypeId = $sin_room_type_list['RoomTypeId'];

        $HotelType = $sin_room_type_list['HotelType'];

        $hotel_supp_str = '';

        foreach ($sin_room_type_list['supp'] as $hotelSupIndex => $singl_supp) {

            $hotel_supp_str .= '<input type="hidden" name="sup[' . $hotelSupIndex . '][suppId]" value="' . $singl_supp['suppId'] . '">' .

                '<input type="hidden" name="sup[' . $hotelSupIndex . '][suppName]" value="' . $singl_supp['suppName'] . '">' .

                '<input type="hidden" name="sup[' . $hotelSupIndex . '][supptType]" value="' . $singl_supp['supptType'] . '">' .

                '<input type="hidden" name="sup[' . $hotelSupIndex . '][suppIsMandatory]" value="' . $singl_supp['suppIsMandatory'] . '">' .

                '<input type="hidden" name="sup[' . $hotelSupIndex . '][suppChargeType]" value="' . $singl_supp['suppChargeType'] . '">' .

                '<input type="hidden" name="sup[' . $hotelSupIndex . '][price]" value="' . $singl_supp['price'] . '">' .

                '<input type="hidden" name="sup[' . $hotelSupIndex . '][publishPrice]" value="' . $singl_supp['publishPrice'] . '">';

        }

        $addrestext = '';

        if (isset($room_type_list['location']['@attributes'])) {

            $addrestext = '<input type="hidden" value="' . $room_type_list['location']['@attributes']['address'] . '" name="address"/>' .

                '<input type="hidden" value="' . $room_type_list['location']['@attributes']['city'] . '" name="city"/>' .

                '<input type="hidden" value="' . $room_type_list['location']['@attributes']['country'] . '" name="country"/>' .

                '<input type="hidden" value="' . $room_type_list['location']['@attributes']['destinationCode'] . '" name="destinationCode"/>' .

                '<input type="hidden" value="' . $room_type_list['location']['@attributes']['destinationId'] . '" name="destinationId"/>' .

                '<input type="hidden" value="' . $room_type_list['location']['@attributes']['latitude'] . '" name="latitude"/>' .

                '<input type="hidden" value="' . $room_type_list['location']['@attributes']['longitude'] . '" name="longitude"/>' .

                '<input type="hidden" value="' . $room_type_list['location']['@attributes']['searchingCity'] . '" name="searchingCity"/>' .

                '<input type="hidden" value="' . $room_type_list['location']['@attributes']['state'] . '" name="state"/>' .

                '<input type="hidden" value="' . $room_type_list['location']['@attributes']['zip'] . '" name="zip"/>';

        }

        $discountstr = "";

        if ($hasfree) {

            $discountstr .= $hasfreebreakfast;

        }

        if ($freesup) {

            $discountstr .= $freesup;

        }
        //echo '<pre>';print_r($discount);echo '</pre>';
        if (count($discount) > 0) {

            if ($discount['discType'] == "ProgressivePromotion") {

                if ($discount['name'] != "") {

                    $discountstr .= "<tr class='discount_tr collapse demo_".$singlehotel['hotelId']."'><td colspan='5'><span class='speciel_deal_discount'>" . $discount['name'] . "</span> Get " . $discount['value'] . "% Discount on booking from " . $discount['from'] . " to " . $discount['to'] . "</td></tr>";

                } else {

                    $discountstr .= "<tr class='discount_tr collapse demo_".$singlehotel['hotelId']."'><td colspan='5'>Get " . $discount['value'] . "% Discount on booking from " . $discount['from'] . " to " . $discount['to'] . "</td></tr>";

                }

            } else if ($discount['discType'] == "TL_DISCOUNT") {

                //$discountstr .= "<tr class='discount_tr'><td colspan='5'><span class='speciel_deal_discount'>Discount</span>$".$discount['amount']." will be applied on total amount</td></tr>";

                $discountstr .= "";

            } else {
                $discountstr .= "<tr class='discount_tr collapse demo_".$singlehotel['hotelId']."'><td colspan='5'>Discount from " . $discount['from'] . " to " . $discount['to'] . " pay for " . $discount['pay'] . " nights stay for " . $discount['stay'] . " nights</td></tr>";

            }

        }

        if (count($room_type_list) > 1) {
            if($room_type_index == 0){
                $room_type_rows_html .= '<tr>' .

                    '<td>' . $sin_room_type_list['name'] . '</td>' .

                    '<td>' . $room_type_availability . '</td>' .

                    '<td class="popover-link"><a onclick="rateAndPolicyFunc(this);" days="' . $totalDays . '" minR="' . $min_price . '" facilities="' . $singlehotel['facilities'] . '" occupancy_list="' . htmlentities(json_encode($occupancy_lists), ENT_QUOTES, 'UTF-8') . '" hotelId="' . $singlehotel['hotelId'] . '" RoomTypeId="' . $roomTypeId . '" HotelType="' . $HotelType . '" first_index="' . $i . '" second_index="' . $room_type_index . '" href="javascript:void(0)">Rates & Policies</a></td>' .

                    '<td>$' . $min_price . '</td><td><form target="_blank" action="' . base_url() . 'request_quote?stp=f" name="' . $room_type_index . '" id="hotel' . $singlehotel['hotelId'] . $room_type_index . '" method="post">' .

                    '<input type="hidden" value="' . $sin_room_type_list['name'] . '" name="room_type_category"/>' .

                    '<input type="hidden" value="' . $singlehotel['hotelId'] . '" name="hotelId"/>' .

                    '<input type="hidden" value="' . $HotelType . '" name="hotelType"/>' .

                    '<input type="hidden" value="' . $min_price . '" name="minPrice"/>' .

                    '<input type="hidden" value="' . $singlehotel['voucher_remarks'] . '" name="voucher_remarks"/>' .

                    '<input type="hidden" value="' . $locationName . '" name="locationName"/>' .

                    '<input type="hidden" value="' . $check_in . '" name="from"/>' .

                    '<input type="hidden" value="' . $check_out . '" name="top"/>' .

                    '<input type="hidden" value="' . $uniqueSession . '" name="uniqueSession"/>' .

                    '<input type="hidden" value="' . $locationId . '" name="locationId"/>' .

                    '<input type="hidden" value="' . count($numberOfAdults) . '" name="adultsNo"/>' .

                    '<input type="hidden" value="' . count($numberOfChild) . '" name="childrenNo"/>' .

                    '<input type="hidden" value="' . $singlehotel['hotelName'] . '" name="hotelName"/>' .

                    '<input type="hidden" value="' . $singlehotel['star_level'] . '" name="starRating"/>' .

                    '<input type="hidden" value="' . $roomTypeId . '" name="roomTypeId"/>' .

                    '<input type="hidden" value="' . $room_type_availability . '" name="avalibilty"/>' . $addrestext .

                    '<input type="hidden" value="' . $roomId . '" name="roomId"/>' . $hotel_supp_str .

                    '<input class="btn btn-danger btn-xs" name="" type="submit" value="Book now!"></form></td>' .

                    '</tr>' . $discountstr;
            }else{
                $room_type_rows_html .= '<tr class="collapse demo_'.$singlehotel['hotelId'].'">' .

                    '<td>' . $sin_room_type_list['name'] . '</td>' .

                    '<td>' . $room_type_availability . '</td>' .

                    '<td class="popover-link"><a onclick="rateAndPolicyFunc(this);" days="' . $totalDays . '" minR="' . $min_price . '" facilities="' . $singlehotel['facilities'] . '" occupancy_list="' . htmlentities(json_encode($occupancy_lists), ENT_QUOTES, 'UTF-8') . '" hotelId="' . $singlehotel['hotelId'] . '" RoomTypeId="' . $roomTypeId . '" HotelType="' . $HotelType . '" first_index="' . $i . '" second_index="' . $room_type_index . '" href="javascript:void(0)">Rates & Policies</a></td>' .

                    '<td>$' . $min_price . '</td><td><form target="_blank" action="' . base_url() . 'request_quote?stp=f" name="' . $room_type_index . '" id="hotel' . $singlehotel['hotelId'] . $room_type_index . '" method="post">' .

                    '<input type="hidden" value="' . $sin_room_type_list['name'] . '" name="room_type_category"/>' .

                    '<input type="hidden" value="' . $singlehotel['hotelId'] . '" name="hotelId"/>' .

                    '<input type="hidden" value="' . $HotelType . '" name="hotelType"/>' .

                    '<input type="hidden" value="' . $min_price . '" name="minPrice"/>' .

                    '<input type="hidden" value="' . $singlehotel['voucher_remarks'] . '" name="voucher_remarks"/>' .

                    '<input type="hidden" value="' . $locationName . '" name="locationName"/>' .

                    '<input type="hidden" value="' . $check_in . '" name="from"/>' .

                    '<input type="hidden" value="' . $check_out . '" name="top"/>' .

                    '<input type="hidden" value="' . $uniqueSession . '" name="uniqueSession"/>' .

                    '<input type="hidden" value="' . $locationId . '" name="locationId"/>' .

                    '<input type="hidden" value="' . count($numberOfAdults) . '" name="adultsNo"/>' .

                    '<input type="hidden" value="' . count($numberOfChild) . '" name="childrenNo"/>' .

                    '<input type="hidden" value="' . $singlehotel['hotelName'] . '" name="hotelName"/>' .

                    '<input type="hidden" value="' . $singlehotel['star_level'] . '" name="starRating"/>' .

                    '<input type="hidden" value="' . $roomTypeId . '" name="roomTypeId"/>' .

                    '<input type="hidden" value="' . $room_type_availability . '" name="avalibilty"/>' . $addrestext .

                    '<input type="hidden" value="' . $roomId . '" name="roomId"/>' . $hotel_supp_str .

                    '<input class="btn btn-danger btn-xs" name="" type="submit" value="Book now!"></form></td>' .

                    '</tr>' . $discountstr;
            }

        } else {
            $room_type_rows_html .= '<tr>' .

                '<td>' . $sin_room_type_list['name'] . '</td>' .

                '<td>' . $room_type_availability . '</td>' .

                '<td class="popover-link"><a onclick="rateAndPolicyFunc(this);" days="' . $totalDays . '" minR="' . $min_price . '" facilities="' . $singlehotel['facilities'] . '" occupancy_list="' . htmlentities(json_encode($occupancy_lists), ENT_QUOTES, 'UTF-8') . '" hotelId="' . $singlehotel['hotelId'] . '" RoomTypeId="' . $roomTypeId . '" HotelType="' . $HotelType . '" first_index="' . $i . '" second_index="' . $room_type_index . '" href="javascript:void(0)">Rates & Policies</a></td>' .

                '<td>$' . $min_price . '</td><td><form target="_blank" action="' . base_url() . 'request_quote?stp=f" name="' . $room_type_index . '" id="hotel' . $singlehotel['hotelId'] . $room_type_index . '" method="post">' .

                '<input type="hidden" value="' . $sin_room_type_list['name'] . '" name="room_type_category"/>' .

                '<input type="hidden" value="' . $singlehotel['hotelId'] . '" name="hotelId"/>' .

                '<input type="hidden" value="' . $HotelType . '" name="hotelType"/>' .

                '<input type="hidden" value="' . $min_price . '" name="minPrice"/>' .

                '<input type="hidden" value="' . $singlehotel['voucher_remarks'] . '" name="voucher_remarks"/>' .

                '<input type="hidden" value="' . $locationName . '" name="locationName"/>' .

                '<input type="hidden" value="' . $check_in . '" name="from"/>' .

                '<input type="hidden" value="' . $check_out . '" name="top"/>' .

                '<input type="hidden" value="' . $uniqueSession . '" name="uniqueSession"/>' .

                '<input type="hidden" value="' . $locationId . '" name="locationId"/>' .

                '<input type="hidden" value="' . count($numberOfAdults) . '" name="adultsNo"/>' .

                '<input type="hidden" value="' . count($numberOfChild) . '" name="childrenNo"/>' .

                '<input type="hidden" value="' . $singlehotel['hotelName'] . '" name="hotelName"/>' .

                '<input type="hidden" value="' . $singlehotel['star_level'] . '" name="starRating"/>' .

                '<input type="hidden" value="' . $roomTypeId . '" name="roomTypeId"/>' .

                '<input type="hidden" value="' . $room_type_availability . '" name="avalibilty"/>' . $addrestext .

                '<input type="hidden" value="' . $roomId . '" name="roomId"/>' . $hotel_supp_str .

                '<input class="btn btn-danger btn-xs" name="" type="submit" value="Book now!"></form></td>' .

                '</tr>' . $discountstr;

        }
    }

    if (count($room_type_list) > 1) {
        $room_type_rows_html .= '<tr class="collapseTrigger" style="cursor:pointer;text-align: center;" data-toggle="collapse" data-target=".demo_' . $singlehotel['hotelId'] . '"><td colspan="5"><b>Click here to view more rooms</b></td></tr>';
    }
    $lowest_roomId = 10;

    $lowest_roomTypeId = 10;

    $modal_book_now_url = base_url() . 'request_quote?hotelId=' . $singlehotel['hotelId'] . '&roomId=' . $lowest_roomId . '&roomTypeId=' . $lowest_roomTypeId.'&stp=1';

    ?>

    <div class="panel panel-info" id="sResult">

        <div class="panel-heading clearfix">

            <div class="panel-title pull-left"><h4><?php echo $singlehotel['hotelName']; ?></h4></div>

            <div class="panel-option pull-right">

                <input type="text" name="rating" id="inputRating" class="srRating hidden" value="<?php echo $singlehotel['star_level']; ?>">

            </div>

        </div>

        <div class="panel-body">

            <div class="media">

                <a tab_name="picture" href="javascript:void(0)" class="pull-left hotel_search_details" hotel-type="<?php echo $singlehotel['hotelType']; ?>" hotel_id="<?php echo $singlehotel['hotelId']; ?>" modal_book_now_button="<?php echo $modal_book_now_url; ?>" min_price="<?php echo $lowest_price; ?>">

                    <img style="width: 100px;height: 100px;" id="hotelthumb<?php echo $singlehotel['hotelId']; ?>" hotel_location_zip="<?php echo $singlehotel['hotel_location_zip']; ?>" hotel_location_city="<?php echo $singlehotel['hotel_location_city']; ?>" hotel_location_country="<?php echo $singlehotel['hotel_location_country']; ?>" hotel_location_state="<?php echo $singlehotel['hotel_location_state']; ?>" hotel_location_name="<?php echo $singlehotel['hotel_location_name']; ?>" longitude="<?php echo $singlehotel['longitude']; ?>" latitude="<?php echo $singlehotel['latitude']; ?>"  hotelName="<?php echo $singlehotel['hotelName']; ?>" class="media-object img-responsive thumbnail hotel_search_details" src="<?php echo $singlehotel['hotelThumbUrl']; ?>" alt="image">

                </a>

                <div class="media-body">

                    <div class="descrip">

                        <ul class="list-unstyled list-inline">

                            <li><a tab_name="information" href="javascript:void(0)" hotel-type="<?php echo $singlehotel['hotelType']; ?>" form-id="hotel<?php echo $singlehotel['hotelId']; ?>" class="hotel_search_details" hotel_id="<?php echo $singlehotel['hotelId']; ?>" modal_book_now_button="<?php echo $modal_book_now_url; ?>" min_price="<?php echo $lowest_price; ?>"><span class="glyphicon glyphicon-info-sign hotel_search_details"></span>Information</a></li>

                            <li><a tab_name="amenities" class="hotel_search_details" hotel-type="<?php echo $singlehotel['hotelType']; ?>" form-id="hotel<?php echo $singlehotel['hotelId']; ?>" href="javascript:void(0)" hotel_id="<?php echo $singlehotel['hotelId']; ?>" modal_book_now_button="<?php echo $modal_book_now_url; ?>" min_price="<?php echo $lowest_price; ?>"><span class="fa fa-tasks"></span>Amenities</a></li>

                            <li><a tab_name="map" href="javascript:void(0)" hotel-type="<?php echo $singlehotel['hotelType']; ?>" class="hotel_search_details" form-id="hotel<?php echo $singlehotel['hotelId']; ?>" hotel_id="<?php echo $singlehotel['hotelId']; ?>" modal_book_now_button="<?php echo $modal_book_now_url; ?>" min_price="<?php echo $lowest_price; ?>"><span class="glyphicon glyphicon-map-marker hotel_search_details"></span>Map</a></li>

                            <li><a tab_name="picture" href="javascript:void(0)" hotel-type="<?php echo $singlehotel['hotelType']; ?>" class="hotel_search_details" form-id="hotel<?php echo $singlehotel['hotelId']; ?>" hotel_id="<?php echo $singlehotel['hotelId']; ?>" modal_book_now_button="<?php echo $modal_book_now_url; ?>" min_price="<?php echo $lowest_price; ?>"><span class="glyphicon glyphicon-picture hotel_search_details"></span>Pictures</a></li>

                        </ul>

                    </div>

                    <div class="media-price" >
                        <h4 style="text-align: center; margin:5px 0px; color: #EC2A26;">TOTAL</h4> <h4>$<?php echo $lowest_price; ?></h4>
                    </div>

                </div>

            </div>

        </div>

        <div class="panel-lower-body"></div>

        <div class="panel-footer">

            <div class="table-responsive">

                <table class="table">

                    <thead style="background: transparent;">

                    <tr><th>Room Type</th><th>Status</th><th>Policy</th><th>Total</th><th>&nbsp;</th></tr>

                    </thead>

                    <tbody>

                    <?php echo $room_type_rows_html; ?>

                    </tbody>

                </table>

            </div>

        </div>

    </div>

<?php

}
//echo '<pre>';print_r($overAllPricesSet);exit;
?>

</div>

</div>

</div> <!-- clearfix -->

</div>

<div id="popover" class="hide">

    <div id="popover-scroll"style="background: #FDFEFF;width: 680px;height: 350px;box-shadow: 0px 0px 10px 0px #999;    border: 1px solid #999;overflow: auto;">

        <div class="hotel-rate-and-policy">
            <div class="header-policy">
                <a href="javascript:void(0)"  class="pull-right close-listing"><i class="fa fa-times"></i></a>
                <h5 style="font-weight: 700;margin-top: 3px;">Rate & Policies</h5>
            </div>

            <table class="table table-bordered">

                <thead>

                <tr id="rate_policies_header">

                    <th></th>

                    <th>Total</th>

                    <th>Tue</th>

                </tr>

                </thead>

                <tbody id="rate_policies_body">

                <tr>

                    <td>Room 1</td>

                    <td>$50.00</td>

                    <td>$50.00(available)</td>

                </tr>

                <tr>

                    <td>Room 2</td>

                    <td>$50.00</td>

                    <td>$50.00(available)</td>

                </tr>

                <tr>

                    <td>Room 3</td>

                    <td>$50.00</td>

                    <td>$50.00(available)</td>

                </tr>

                <tr>

                    <td>Room 4</td>

                    <td>$50.00</td>

                    <td>$50.00(available)</td>

                </tr>

                <tr>

                    <td>Room 5</td>

                    <td>$50.00</td>

                    <td>$50.00(available)</td>

                </tr>

                </tbody>

            </table>

            <div class="hotel-policy-and-facility">

                <h5>Room Cancellation Policy</h5>


                <p id="cancellation_policy_text"></p>



                <!--<h5>Room Facilities</h5>

                <p id = 'facility_lists'>Air Condition, High Speed Internet Access, Private Bath, T.V.</p>-->

            </div>

        </div>

    </div>

</div>

<div class="loader"  style="display: none">

    <div class="center">

        <img alt="" src="<?php echo base_url(); ?>public/spin.gif" />

    </div>

</div>

<div class="modal fade cmodal" id="pop">

<div class="modal-dialog modal-lg">

<div class="modal-content">

<div class="modal-header">

    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

    <h4 class="modal-title">Hotel Details</h4>

</div>

<div class="modal-body">

    <div class='' id='sResultModal'>

        <div class="media">

            <div id="hotel_thumbnail_html">



            </div>

            <div class="media-body">

                <div class="col-md-8">

                    <div class="panel-title" id="hotel_name">

                        <h4>Hotel Name</h4>

                    </div>

                    <div id="hotel_rating">

                        <input type="text" name="rating" id="inputRating" class="srRating hidden tt" value='4'>

                    </div>

                    <address id="hotel_address">

                        <strong>some thing to highlight</strong><br>

                        3325 Las Vegas Boulevard South<br>

                        The Strip<br>

                        Las Vegas NV 89109 USA<br>

                        <abbr title="Phone">P:</abbr><div id="hotel_phone"></div>  if any?

                    </address>

                </div>

                <div class="modalMediaPrice col-md-4" id="modal_book_now">

                    <h4 style="margin:5px 0px; color: #EC2A26;">FROM</h4>

                    <h4></h4>

                    <a class="btn btn-danger btn-sm"  href=''>Book now!</a>

                </div>

            </div>

        </div>

        <div class="lowerModalBody">

            <div role="tabpanel">

                <!-- Nav tabs -->

                <ul class="nav nav-tabs nav-justified nav-pills smnav" role="tablist">

                    <li role="presentation" id="hotel_search_information_tab">

                        <a href="#" data-target="#information" aria-controls="information" role="tab" data-toggle="tab">Information</a>

                    </li>

                    <li role="presentation" id="hotel_search_amenities_tab">

                        <a href="#" data-target="#ame" aria-controls="ame" role="tab" data-toggle="tab">Amenities</a>

                    </li>

                    <li role="presentation" id="hotel_search_pictures_tab">

                        <a href="#" data-target="#pictures" aria-controls="pictures" role="tab" data-toggle="tab">Pictures</a>

                    </li>

                    <li role="presentation" id="hotel_search_map_tab">

                        <a href="#" class="map_tab_clicked" data-target="#map" tab_name="map" aria-controls="map" role="tab" data-toggle="tab">Map</a>

                    </li>

                </ul>

                <div class="tab-content">

                    <div role="tabpanel" class="tab-pane fade in active" id="information">

                        <div class="info">

                            <div class="clearfix">

                                <div class="col-sm-6" id="information_tab_1">



                                </div>

                                <div class="col-sm-6" id="information_tab_2">



                                </div>



                            </div>



                        </div>

                    </div>

                    <div role="tabpanel" class="tab-pane fade" id="ame">

                        <div class="clearfix">

                            <ul class="list-unstyled list-inline tags" id="hotel_by_id_aminities">

                            </ul>

                        </div>

                    </div>

                    <div role="tabpanel" class="tab-pane fade" id="pictures">

                        <div class="clearfix">

                            <div class="col-sm-offset-1 col-sm-10 col-sm-offset-1 slider_mb">

                                <div class="col-xs-12" id="slider">
                                    <div class="row">
                                        <div class="col-sm-12" id="carousel-bounding-box">
                                            <div id="myCarousel">
                                                <!-- Carousel items -->
                                                <div class="carousel-inner" id="image_slider">
                                                    <div class="item">
                                                        <img class="img-responsive" src="http://www.touricoholidays.com/en/Modules/customizable/images/photo-not-available-small.gif" alt="default image">
                                                    </div>
                                                </div>
                                                <!-- Carousel nav -->
                                                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                                                    <span class="glyphicon glyphicon-chevron-left"></span>
                                                </a>
                                                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                                                    <span class="glyphicon glyphicon-chevron-right"></span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div><!--/Slider-->

                            <div class="col-sm-12 hidden-xs" id="slider-thumbs">

                                <!-- Bottom switcher of slider -->

                                <ul class="list-unstyled" id="all_slider_images">

                                </ul>

                            </div>

                        </div>

                    </div>

                    <div role="tabpanel" class="tab-pane fade"  style="width:850px; height:250px;" id="map"></div>

                </div>

            </div>

        </div>

    </div>

</div>

</div>

</div>

</div>

<script src="https://maps.googleapis.com/maps/api/js?key=<?=MAP_KEY?>"></script>

<script type="text/javascript">

    $(document).ready(function(){
        /*$(document).on('shown.bs.collapse','.collapse', function () {
         alert(1);
         $(this).parent().children().last().find('td').html("<b>Click here to hide rooms</b>");
         });

         $('body').on('hidden.bs.collapse','.collapse', function (e) {
         alert(2)
         $(this).parent().children().last().find('td').html("<b>Click here to view more rooms</b>");
         });*/
        $(document).on('click','.collapseTrigger', function () {
            var checker = $(this).attr('class');
            if(checker == "collapseTrigger collapsed"){
                var obj = $(this).find('td');
                setTimeout(function (){
                    obj.html("<b>Click here to view more rooms</b>");
                }, 300);
            }else{

                $(this).find('td').html("<b>Click here to hide rooms</b>");
            }

        });
    });
    //For price range

    var locationId = "<?php echo ($locationId); ?>";

    var locationName = "<?php echo ($locationName); ?>";

    var numberOfAdults = "<?php echo count($numberOfAdults); ?>";

    var numberOfChild = "<?php echo count($numberOfChild); ?>";

    var totalHotel = <?php echo count($hotel_list); ?>;

    var check_in = <?php echo json_encode($check_in); ?>;

    var check_out = <?php echo json_encode($check_out); ?>;

    var from = "<?php echo $check_in; ?>";

    var to = "<?php echo $check_out; ?>";

    var perPageHotel = 10;

    var currentPage = 1;

    var uniqueSession = "<?php echo $uniqueSession;?>";

    var roomNo = "<?php echo $roomNo;?>";

    var min_price_slider = <?php echo getMinPriceByUniqueSessionID($uniqueSession);?>;

    var max_price_slider = <?php echo getMaxPriceByUniqueSessionID($uniqueSession);?>;
    //var min_price_slider = <?php //echo getMinPriceByUniqueSessionID($overAllPricesSet);?>;

    //var max_price_slider = <?php //echo getMaxPriceByUniqueSessionID($overAllPricesSet);?>;

    var min_dest_slider , max_dest_slider = 0;

    var hotel_search_id_ajax_path = "<?php echo base_url('view_hotel_search_id') ?>";

    var searchFilterAndMoreResult = "<?php echo base_url('searchFilterAndMoreResult') ?>";

    var base_url = "<?php echo base_url() ?>";

    var jsonDays = <?php echo json_encode($days); ?>;

    var start_date_pt = '<?php echo date('F d, Y H:i:s'); ?>';

    var nowTemp = new Date(start_date_pt);

    var houres = nowTemp.getHours();

    if (houres > 15)

    {

        var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate() + 4, 0, 0, 0, 0);

    }

    else

    {

        var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate() + 4, 0, 0, 0, 0);

    }

</script>