<div class="width-row">
	<div class="clearfix">

		<div class="panel panel-info" id="details">
			<div class="panel-heading clearfix">
				<div class="panel-title pull-left">
					<h4>Car Details</h4>
				</div>
				<div class="panel-option pull-right">
					<?php
                    echo <<<EOM
    <img src="{$company_logo}" class="img-responsive" alt="Image">
EOM;
                    ?>
				</div>
			</div>
			<div class="panel-body">
				<div class="clearfix">
					<div class="col-xs-12 col-md-4">
						<div class="panel panel-default car-info">
							<div class="panel-image">
                                <?php

                                echo <<<EOM
                                <img src="{$select_station[0]['carThumb']}" class="img-responsive" alt="Image">
EOM;

                                ?>
							</div>
							<div class="panel-footer">
                                <?php
                                $maxPassengers = $select_station[0]['maxPassengers'];
                                $luggageLarge = $select_station[0]['luggageLarge'];
                                $luggageSmall = $select_station[0]['luggageSmall'];

                                if('true' == $select_station[0]['ac'])
                                {
                                    $ac = "AC , ";
                                }
                                else
                                {
                                    $ac = "";
                                }

                                if("Automatic Transmission" == $select_station[0]['transmission'])
                                {
                                    $transmission = "Automatic";
                                }
                                else
                                {
                                    $transmission = "Manual";
                                }

                                echo <<<EOM
                                <p>{$ac} {$transmission}</p>
								<p>
									<i class="fa fa-user fa-lg"></i>&nbsp;<span>{$maxPassengers}</span><i class="fa fa-plus"></i>
									<i class="fa fa-briefcase  fa-lg"></i>&nbsp;<span>{$luggageLarge}</span><i class="fa fa-plus"></i>
									<i class="fa fa-briefcase "></i>&nbsp;<span>{$luggageSmall}</span>
								</p>
EOM;
                                ?>
							</div>
						</div>
					</div>

                    <?php

                    echo <<<EOM
<div class="col-xs-12 col-md-8 details">
						<h4 style="margin:2px 0px 11px 0px;">{$car_class} : {$select_station[0]['carName']}</h4>
						<p>Car example: Kia Rio</p>
						<ul class="list-unstyled">
							<li class="clearfix">
								<div class="col-xs-12">
									<div class="row">
										<h4>Pick-Up:</h4>
										<p><strong>Date/Time: </strong>{$pickup_date} - {$pickup_hour} AM</p>
										<p><strong>Address: </strong>{$pickup_location}</p>
										<p><a data-toggle="modal" href='#detailPopPickUp'>Hours of operation</a></p>
									</div>
								</div>
							</li>
							<li class="clearfix">
								<div class="col-xs-12">
									<div class="row">
										<h4>Drop-Off:</h4>
										<p><strong>Date/Time: </strong>{$drop_off_date} - {$drop_off_hour}</p>
										<p><strong>Address: </strong>{$drop_off_location}</p>
										<p><a data-toggle="modal" href='#detailPopDropOff'>Hours of operation</a></p>
									</div>
								</div>
							</li>
						</ul>
					</div>
EOM;


                    ?>
<?php
        <<<EOM

EOM;

?>

				</div>
				<div class="clearfix" id="cprogram">
					<div class="col-md-12">
						<h4 class="carPTitle">Car Program</h4>
                        <?php
                            $car_program_info = $select_station[1]['car_program_info'];
                            $car_program_info_count = count($car_program_info);
                            for($i=0; $i<$car_program_info_count; $i++)
                            {
                                $program_name = $car_program_info[$i]['program_name'];
                                $inclusions = $car_program_info[$i]['program_includes'];
                                echo <<<EOM
						<ul class="list-group">
							<li class="list-group-item">
								<span class="pull-right"></span>
                                <p><strong>{$program_name}</strong></p>
                                <p><strong>Inclusions: </strong></p>
EOM;
                                foreach($inclusions as $a_inclusion)
                                {
                                    echo  <<<EOM
                                            {$a_inclusion['desc']}
EOM;
                                }
                                echo <<<EOM
<p><a href="#detailProgram" data-toggle="modal">View Program Details</a></p>
							</li>
						</ul>
EOM;

                            }

                        ?>

					</div>
				</div>
<!-- 				<div class="clearfix">
					<div class="col-md-7" id="splFields">
						<h4 class="carPTitle">Special Equipment & Mandatory Fees Breakdown</h4>
						<p>Special equipment items are not guaranteed (subject to availability)</p>
						<p>Items selected are charged by the rental company during pickup</p>
						<ul class="list-group">
							<li class="list-group-item">
								<p><strong>Mandatory Fees: paid at the counter </strong></p>
							</li>
							<li class="list-group-item">
								<div class="checkbox">
									<input type="checkbox" value=""  checked>
									<p>CONSOLIDATED FACILITY CHG 3.75/DAY</p>
								</div>
							</li>
							<li class="list-group-item">
								<div class="checkbox">
									<input type="checkbox" value=""  checked>
									<p>CONC REC 10 PCT</p>
								</div>
							</li>
							<li class="list-group-item">
								<div class="checkbox">
									<input type="checkbox" value=""  checked>
									<p>CLARK COUNTY TAX 2 PCT</p>
								</div>
							</li>
							<li class="list-group-item">
								<div class="pull-right">
									$19.98
								</div>
								<div class="pull-right cselect">
									<select id="" class="form-control">
										<option class="" value="0" selected="selected">0</option>
										<option class="" value="1">1</option>
										<option class="" value="2">2</option>
										<option class="" value="3">3</option>
									</select>
								</div>
								<div class="checkbox">
									<input type="checkbox" value="" >
									<p>Toddler car seat</p>
								</div>
							</li>
							<li class="list-group-item">
								<div class="pull-right">0$</div>
								<p>Estimated amount due at pickup</p>
								<p>Special equipment prices do not include taxes</p>
							</li>
						</ul>
					</div>
					<div class="col-md-5" id="splPrice">
						<h4>Price Details</h4>
						<table class="table">
							<tbody>
								<tr>
									<td>Car Price:</td>
									<td>$191.51</td>
								</tr>
								<tr>
									<td>Amount due at booking time:</td>
									<td>$191.51</td>
								</tr>
								
								<tr>
									<td>Estimated amount due at pickup: Special equipment prices do not include taxes</td>
									<td>$0</td>
								</tr>
								
								<tr>
									<td>
										&nbsp;
									</td>
									<td>
										<button type="button" class="btn btn-danger btn-sm">Book Now</button>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div> -->
				<div class="clearfix">
					<div class="col-md-12" id="rules">
                        <?php
                        echo '<h4 class="carPTitle">Rules & Restrictions</h4>';

                        foreach($rules_regulations as $a_rules_regulations) {
                            if (isset($a_rules_regulations['title'])) {
                                echo <<<EOM
<section>
							<h5><strong>{$a_rules_regulations['title']}</strong></h5>
							<p>{$a_rules_regulations['description']}</p>
						</section>
EOM;
                            }
                        }
                        ?>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</div>
<?php
echo <<<EOM
<div class="modal fade" id="detailPopPickUp">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Hours Of Operation</h4>
			</div>
			<div class="modal-body">
				<h5 class="car_hours">{$pickup_location}</h5>
				<table class="table">
					<tbody>
						<tr>
							<td>
								Pick-Up:
							</td>
							<td>
								{$select_station[0]['operationHoursPickUp']}
							</td>
						</tr>
						<tr>
							<td>
								Drop-Off:
							</td>
							<td>
								{$select_station[0]['operationHoursDropOff']}
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
EOM;
?>

<?php
echo <<<EOM
<div class="modal fade" id="detailPopDropOff">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Hours Of Operation</h4>
			</div>
			<div class="modal-body">
				<h5 class="car_hours">{$pickup_location}</h5>
				<table class="table">
					<tbody>
						<tr>
							<td>
								Pick-Up:
							</td>
							<td>
								{$select_station[0]['operationHoursPickUp']}
							</td>
						</tr>
						<tr>
							<td>
								Drop-Off:
							</td>
							<td>
								{$select_station[0]['operationHoursDropOff']}
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
EOM;
?>

<div class="modal fade" id="detailProgram">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Inclusions</h4>
			</div>
			<div class="modal-body">
					<ul class="list-unstyled pdetail">
                        <?php

                            $car_program_info = $select_station[1]['car_program_info'];
                            $car_program_info_count = count($car_program_info);
                            for($i=0; $i<$car_program_info_count; $i++)
                            {
                                $inclusions = $car_program_info[$i]['program_includes'];
                                foreach($inclusions as $a_inclusion)
                                {
                                    echo <<<EOM
                                        <li>{$a_inclusion['desc']}</li>
EOM;

                                }
                            }
                        ?>
					</ul>
			</div>
		</div>
	</div>
</div>