<style>
    #map {
        width: 100%;
        height: 400px;
        background-color: #CCC;
    }

    .discount_tr{
        font-weight: bold;
    }
    .discount_tr > td {
        border: none !important;
    }
    .speciel_deal_discount {
        background: #d43f3a;
        color: #fff;
        padding: 5px 5px ;
        border-radius: 3px;
        margin: 0 8px 0 0;
        box-shadow: 0 0 12px 0 rgba(212,63, 58, 0.50);
    }

</style>
<!--<script src="https://maps.googleapis.com/maps/api/js"></script>-->
<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set('memory_limit', '512M');
date_default_timezone_set('America/Los_Angeles');
$datetime1 = new DateTime($check_in);
$datetime2 = new DateTime($check_out);
$interval = $datetime1->diff($datetime2);
$totdays = $interval->days;
$days = array();

for ($di = 0; $di < $totdays; $di++) {
    $days[$di] = date('D', strtotime("+" . $di . " days", strtotime($check_in)));
}

$days = json_encode($days);

$hotel_location_name_from_db = $location_names;
$location_name_from_db_count = count($location_names);
$first_nine_values = array();

$location_names_count = count($location_names);

$location_html = '';

for ($i = 0; $i < $location_names_count; $i++) {
    $location_html .= <<<EOM
                        <tr>
                     
                            <td style="width:10%">
                                  <input type="checkbox" id="{$location_names[$i]}" value="{$location_names[$i]}" name="" class="location_names_checkbox">
                             </td>
                            <td style="width:90%">
                                  <label for="{$location_names[$i]}">{$location_names[$i]}</label>
                            </td>
                        </tr>
EOM;
}

$hotel_list_count = count($hotel_list);
$hotel_location_name = array();
for ($j = 0; $j < $hotel_list_count; $j++) {
    $a_hotel = $hotel_list[$j];

    if (isset($hotel_location_name[$a_hotel['hotel_location_name']])) {
        $hotel_location_name[$a_hotel['hotel_location_name']] ++;
    } else {
        $hotel_location_name[$a_hotel['hotel_location_name']] = 1;
    }
}
$desarray = array();
$rangede = array();
foreach ($hotel_list as $key => $singlehot) {
    $desarray[] = array("miles" => $singlehot['distanceFromAirport']);
    $rangede[$key] = $singlehot['distanceFromAirport'];
}
if (count($rangede) > 0) {
    $minrrand = array_keys($rangede, min($rangede));
} else {
    $minrrand = array();
}
if (isset($minrrand[0])) {
    $minrrand = $rangede[$minrrand[0]];
} else {
    $minrrand = 0;
}
if (count($rangede) > 0) {
    $maxmrrang = array_keys($rangede, max($rangede));
} else {
    $maxmrrang = array();
}
if (isset($maxmrrang[0])) {
    $maxmrrang = $rangede[$maxmrrang[0]];
} else {
    $maxmrrang = 0;
}

$hotel_location_chk_html = <<<EOM
                            <tr>
                                <td>
                                  <h5>Las Vegas Locations</h5>
                                                <table class="table">
                                                    <tbody>
EOM;

$hotel_location_chk_html = "";
$hotel_location_other_chk_html = "";
$hotel_location_other_not_in_db_chk_html = "";
if (count($hotel_location_name) > 0) {
    arsort($hotel_location_name);
}
// Show first 9 locations under locations
// Show show others under others.
$counter = 0;
foreach ($hotel_location_name as $key => $value) {
    $counter++;
    $location_found_in_db = 0;
    for ($i = 0; $i < $location_name_from_db_count; $i++) {
        if ($location_names[$i] == $key) {
            $location_found_in_db = 1;
        }
    }
    if (1 == $location_found_in_db) {
        if ($counter < 9) {
            $first_nine_values[] = $key;

            $hotel_location_chk_html .= <<<EOM
                                <tr>
                                    <td style="width:10%">
                                        <input type="checkbox" class="hotel_search_location_chkbox" value="{$key}" >
                                    </td>
                                    <td style="width:90%">
                                        <label for="">{$key}</label>
                                    </td>
                                </tr
EOM;
        } else {
            $hotel_location_other_chk_html = <<<EOM
                                <tr>
                                    <td style="width:10%">
                                        <input type="checkbox" id="hotel_location_other_chk" class="hotel_location_other_chk" value="">
                                    </td>
                                    <td style="width:90%">
                                        <label for="">Other location</label>
                                    </td>
                                </tr
EOM;
        }
    } else {
        if (strlen($key) > 0) {


            $hotel_location_other_not_in_db_chk_html .= <<<EOM
                                <tr>
                                    <td style="width:10%">
                                        <input type="checkbox" class="hotel_search_location_chkbox" value="{$key}">
                                    </td>
                                    <td style="width:90%">
                                        <label for="">{$key}</label>
                                    </td>
                                </tr
EOM;
        }
    }

    $location_found_in_db = 0;
}

$hotel_brands_info = array();
for ($i = 0; $i < $hotel_list_count; $i++) {
    $an_hotel_list = $hotel_list[$i];

    $brandName = $an_hotel_list['hotel_brand_name'];

    if (isset($hotel_brands_info[$brandName])) {
        $hotel_brands_info[$brandName][0] ++;

        if ($hotel_brands_info[$brandName][1] > $an_hotel_list['only_min_price']) {
            $hotel_brands_info[$brandName][1] = $an_hotel_list['only_min_price'];
        }
        if ($hotel_brands_info[$brandName][2] < $an_hotel_list['only_min_price']) {
            $hotel_brands_info[$brandName][2] = $an_hotel_list['only_min_price'];
        }
    } else {
        $hotel_brands_info[$brandName][0] = 1;
        $hotel_brands_info[$brandName][1] = $an_hotel_list['only_min_price'];
        $hotel_brands_info[$brandName][2] = $an_hotel_list['only_min_price'];
    }
}
$hotel_info_1d = array();
foreach ($hotel_brands_info as $index => $hotel_array) {
    $hotel_info_1d[$index] = $hotel_array[0];
}
if (count($hotel_info_1d) > 0)
    arsort($hotel_info_1d);

$hotel_brand_show = 0;
$hotel_brand_html = "";
$hotel_info_1d_count = count($hotel_info_1d);
foreach ($hotel_info_1d as $index => $value) {
    $hotel_brand_show++;
    $brnd = number_format((float) $hotel_brands_info[$index][1], 2, '.', '');
    $brndname = $index;
    if (trim($brndname) == '') {
        $brndname = 'unknown';
    }
    $hotel_brand_html .= <<<EOM
                            <tr>
                                <td class="">
                                    <input id="" name="" type="checkbox" class="hotel_brand_checkbox" value="{$index}"> </td>
                                <td class="">
                                    <label for="">
                                    <span class="">({$hotel_brands_info[$index][0]})</span> </label>
                                </td>
                                <td class="" style="width:60%;">
                                    {$brndname}
                                </td>
                                <td class="">
                                    <label for="" id="">\${$brnd} <!-- - \${$brnd} --></label>
                                </td>
                            </tr>
EOM;
}
$aminities_count_list = array();
for ($i = 0; $i < $hotel_list_count; $i++) {
    $an_hotel_list = $hotel_list[$i];

    $amenities = $an_hotel_list['aminities'];

    $aminities_count = count($amenities);

    for ($j = 0; $j < $aminities_count; $j++) {
        if (isset($aminities_count_list[$amenities[$j]])) {
            $aminities_count_list[$amenities[$j]] ++;
        } else {
            $aminities_count_list[$amenities[$j]] = 1;
        }
    }
}
if (count($aminities_count_list) > 0)
    arsort($aminities_count_list);

$show_first_num = 0;
$amenities_left_panel_html = "";

foreach ($aminities_count_list as $amenities_name => $amenities_count) {
    $show_first_num++;
    if ($show_first_num > 10)
        break;

    if ($amenities_count > 0) {
        $amenities_left_panel_html .= <<<EOM
                            <tr>
                                <td class="">
                                    <input id="" name="" type="checkbox" value="{$amenities_name}" class="amenities_checkbox">
                                </td>
                                <td class="">
                                    <label for=""><span class="">({$amenities_count})</span></label>
                                </td>
                                <td class="" style="width:60%;">
                                    {$amenities_name}
                                </td>
                            </tr>


EOM;
    }
}

function GetStarValues($hotel_price, &$max_value, &$min_value) {
    $hotel_price = floatval($hotel_price);
    if ($max_value < $hotel_price) {
        $max_value = $hotel_price;
    }

    if ($min_value > $hotel_price) {
        $min_value = $hotel_price;
    }
}

$star_infos = array();

for ($index = 0; $index < 9; $index++) {
    array_push($star_infos, 0);
    array_push($star_infos, 0);
    array_push($star_infos, 20000000);
}

$hotel_list_count = count($hotel_list);
$one_star_count = $one_half_star_count = $two_star_count = $two_half_star_count = $three_star_count = $three_half_star_count = $four_star_count = $four_half_star_count = $five_star_count = 0;
for ($i = 0; $i < $hotel_list_count; $i++) {
    $a_hotel = $hotel_list[$i];

    if ('5' == $a_hotel['star_level']) {
        $star_infos[0] ++;
        GetStarValues($a_hotel['min_price'], $star_infos[1], $star_infos[2]);

        $five_star_count++;
        GetStarValues($a_hotel['min_price'], $five_star_max, $five_star_min);
    }
    if ('4.5' == $a_hotel['star_level']) {
        $star_infos[3] ++;
        GetStarValues($a_hotel['min_price'], $star_infos[4], $star_infos[5]);

        $four_half_star_count++;
        GetStarValues($a_hotel['min_price'], $four_half_star_max, $four_half_star_min);
    }
    if ('4' == $a_hotel['star_level']) {
        $star_infos[6] ++;
        GetStarValues($a_hotel['min_price'], $star_infos[7], $star_infos[8]);

        $four_star_count++;
        GetStarValues($a_hotel['min_price'], $four_star_max, $four_star_min);
    } else if ('3.5' == $a_hotel['star_level']) {
        $star_infos[9] ++;
        GetStarValues($a_hotel['min_price'], $star_infos[10], $star_infos[11]);

        $three_half_star_count++;
        GetStarValues($a_hotel['min_price'], $three_half_star_max, $three_half_star_min);
    } else if ('3' == $a_hotel['star_level']) {
        $star_infos[12] ++;
        GetStarValues($a_hotel['min_price'], $star_infos[13], $star_infos[14]);

        $three_star_count++;
        GetStarValues($a_hotel['min_price'], $three_star_max, $three_star_min);
    } else if ('2.5' == $a_hotel['star_level']) {
        $star_infos[15] ++;
        GetStarValues($a_hotel['min_price'], $star_infos[16], $star_infos[17]);

        $two_half_star_count++;
        GetStarValues($a_hotel['min_price'], $two_half_star_max, $two_half_star_min);
    } else if ('2' == $a_hotel['star_level']) {
        $star_infos[18] ++;
        GetStarValues($a_hotel['min_price'], $star_infos[19], $star_infos[20]);

        $two_star_count++;
        GetStarValues($a_hotel['min_price'], $two_star_max, $two_star_min);
    } else if ('1.5' == $a_hotel['star_level']) {
        $star_infos[21] ++;
        GetStarValues($a_hotel['min_price'], $star_infos[22], $star_infos[23]);

        $one_half_star_count++;
        GetStarValues($a_hotel['min_price'], $one_half_star_max, $one_half_star_min);
    } else if ('1' == $a_hotel['star_level']) {
        $star_infos[24] ++;
        GetStarValues($a_hotel['min_price'], $star_infos[25], $star_infos[26]);

        $one_star_count++;
        GetStarValues($a_hotel['min_price'], $one_star_max, $one_star_min);
    }
}

$star_desc = "";
$rating_counter = 5.5;
for ($i = 0; $i < 27; $i+=3) {
    $rating_counter -= 0.5;

    $formatted_rating_counter = $rating_counter - intval($rating_counter);
    if ($formatted_rating_counter > 0) {
        $formatted_rating_counter = $rating_counter;
    } else {
        $formatted_rating_counter = intval($rating_counter);
    }

    if ($star_infos[$i] > 0) {
        $info2nd = number_format((float) $star_infos[$i + 1], 2, '.', '');
        $info1st = number_format((float) $star_infos[$i + 2], 2, '.', '');
        $star_desc .= <<<EOM
                        <tr>
                            <td class="NH_filter_stars_line">
                                <input id="chkstarRatingFilter{$formatted_rating_counter}" name="chkstarRatingFilter" type="checkbox" value="{$rating_counter}" class="hotel_star_ratings">
                            </td>
                            <td class="NH_filter_stars_line">
                                <label for="chkstarRatingFilter{$formatted_rating_counter}">
                                <span class="NH_filter_number_pad">({$star_infos[$i]})</span> </label>
                            </td>
                            <td class="NH_filter_stars_line">
                                <input type="text" value="{$formatted_rating_counter}" class="srRating2 hidden">
                            </td>
                            <td class="NH_filter_stars_line NH_textAlign" width="100%">
                                <label for="chkstarRatingFilter{$formatted_rating_counter}" id="lblstarRatingFilter{$formatted_rating_counter}">\${$info1st} - \${$info2nd}</label>
                            </td>
                        </tr>
EOM;
    }
}
?>
<div class="width-row">
    <div class="clearfix">
        <div class="col-md-6">
            <div class="hotel_serch_tab_btn">
                <a href="<?php echo $switch_tab_hotel_url ?>">Hotel search result</a>
            </div>
        </div>
        <div class="col-md-6">
            <div class="hotel_serch_tab_btn active">
                <a href="<?php echo $switch_tab_hotel_map_url ?>">Hotel map view</a>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="row">
                <div class="search_result_filter">
                    <div style="width: 63%;padding: 0px 15px;text-align: left; margin-top: 26px; position: absolute;" id="total_hotels_found_id">
                    </div>
                    <div style="width: 63%;text-align: right; margin-top: 26px; position: absolute;">
                        <p>Sort by :</p>
                    </div>

                    <div class="form-group" style="width: 33%;    margin-left: 67%;">
                        <div class="">
                            <div class=" select2-container select2me form-control input-xlarge numberOfChild" id="s2id_numberOfChild" style="text-align: center;">
                                <input class="select2-focusser select2-offscreen" type="text" id="s2id_autogen3"></div>
                            <select class="sort_hotel select2me form-control input-xlarge numberOfChild select2-offscreen" name="numberOfChild[]" style="text-align: center;" tabindex="-1">
                                <option value="1">Price ( Low to high )</option>
                                <option value="2">Price ( High to low )</option>
                                <option value="3">Star rating ( high to low )</option>
                            </select>
                        </div>
                    </div>
                    <div style="border-top: 1px solid #22B3F4; padding: 15px 0px 0px 0px;margin-left: 5px;height: 40px;">

                        <div class="row">
                            <div class="col-sm-1"><p>Rating</p></div>
                            <div class="col-sm-1" style="display: inline-flex;"><input class="hotel_search_select_all" type="checkbox">All</div>
                            <div class="col-sm-1" style="display: inline-flex;    width: 115px;"><input class="hotel_star_ratings" type="checkbox" value="5"><input type="text" value="5" class="srRating2 hidden"></div>
                            <div class="col-sm-1" style="display: inline-flex;    width: 115px;"><input class="hotel_star_ratings" value="4.5" type="checkbox"><input type="text" value="4.5" class="srRating2 hidden"></div>
                            <div class="col-sm-1" style="display: inline-flex;    width: 115px;"><input class="hotel_star_ratings" value="4"  type="checkbox"><input type="text" value="4" class="srRating2 hidden"></div>
                            <div class="col-sm-1" style="display: inline-flex;    width: 115px;"><input class="hotel_star_ratings" value="3.5"   type="checkbox"><input type="text" value="3.5" class="srRating2 hidden"></div>
                            <div class="col-sm-1" style="display: inline-flex;    width: 115px;"><input class="hotel_star_ratings" value="3"   type="checkbox"><input type="text" value="3" class="srRating2 hidden"></div>
                            <div class="col-sm-1" style="display: inline-flex;    width: 115px;"><input class="hotel_star_ratings" value="2.5"   type="checkbox"><input type="text" value="2.5" class="srRating2 hidden"></div>
                            <div class="col-sm-1" style="display: inline-flex;    width: 115px;"><input class="hotel_star_ratings" value="2"   type="checkbox"><input type="text" value="2" class="srRating2 hidden"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="row">
                <div class="hotel_on_map_view">
                    <div class="col-md-8">
                        <!--<div id="hotel_map_modal" style="width: 100%; height: 500px;"></div>-->
                        <div id="map"></div>
                    </div>
                    <div class="col-md-4" id="map_hotel_name_checkboxes" style="overflow-y: scroll;max-height: 400px;">

                        <div class="table-responsive table-custom">
                            <h5>Hotel Name</h5>

                            <table class="table  table-condensed">
                                <tbody id="map_hotel_checkboxes_table_rows">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12">

            <div class="row sResult" id="content">

                <?php
                $index = 0;
                $baseUrl = base_url();
                ?>
            </div>
        </div>
    </div> <!-- clearfix -->

</div>


<div id="popover" class="hide">
    <div id="popover-scroll"style="background: #FDFEFF;width: 450px;height: 350px;box-shadow: 0px 0px 10px 0px #999;    border: 1px solid #999;overflow: scroll;">
        <div class="hotel-rate-and-policy">
            <a href="javascript:void(0)" style="float: right;color: #333;"><i class="fa fa-times"></i></a>
            <h5 style="font-weight: 700;margin-top: 3px;">Rate & Policies</h5>
            <table class="table table-bordered">
                <thead>
                    <tr id="rate_policies_header">
                        <th>Room no.</th>
                        <th>Total</th>
                        <th>Tue</th>
                    </tr>
                </thead>
                <tbody id="rate_policies_body">
                    <tr>
                        <td>Room 1</td>
                        <td>$50.00</td>
                        <td>$50.00(available)</td>
                    </tr>
                    <tr>
                        <td>Room 2</td>
                        <td>$50.00</td>
                        <td>$50.00(available)</td>
                    </tr>
                    <tr>
                        <td>Room 3</td>
                        <td>$50.00</td>
                        <td>$50.00(available)</td>
                    </tr>
                    <tr>
                        <td>Room 4</td>
                        <td>$50.00</td>
                        <td>$50.00(available)</td>
                    </tr>
                    <tr>
                        <td>Room 5</td>
                        <td>$50.00</td>
                        <td>$50.00(available)</td>
                    </tr>
                </tbody>
            </table>
            <div class="hotel-policy-and-facility">
                <h5>Room Cancellation Policy</h5>
                <p id="cancellation_policy_text"></p>

                <!--<h5>Room Facilities</h5>
                <p id = 'facility_lists'>Air Condition, High Speed Internet Access, Private Bath, T.V.</p>-->
            </div>
        </div>
    </div>
</div>

<div class="loader"  style="display: none">
    <div class="center">
        <img alt="" src="<?php echo base_url(); ?>public/spin.gif" />
    </div>
</div>

<div class="modal fade cmodal" id="pop">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Hotel Details</h4>
            </div>
            <div class="modal-body">
                <div class='' id='sResultModal'>
                    <div class="media">
                        <div id="hotel_thumbnail_html">

                        </div>
                        <div class="media-body">
                            <div class="col-md-8">
                                <div class="panel-title" id="hotel_name">
                                    <h4>Hotel Name</h4>
                                </div>
                                <div id="hotel_rating">
                                    <input type="text" name="rating" id="inputRating" class="srRating hidden tt" value='4'>
                                </div>

                                <address id="hotel_address">
                                    <strong>some thing to highlight</strong><br>
                                    3325 Las Vegas Boulevard South<br>
                                    The Strip<br>
                                    Las Vegas NV 89109 USA<br>
                                    <abbr title="Phone">P:</abbr><div id="hotel_phone"></div>  if any?
                                </address>
                            </div>
                            <div class="modalMediaPrice col-md-4" id="modal_book_now">
                                <h4 style="margin:5px 0px; color: #EC2A26;">FROM</h4>
                                <h4>$75 / night</h4>
                                <a class="btn btn-danger btn-sm"  href='{$baseUrl}request_quote?hotelId={$aHotel['hotelId']}'>Book now!</a>
                            </div>

                        </div>
                    </div>

                    <div class="lowerModalBody">
                        <div role="tabpanel">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs nav-justified nav-pills smnav" role="tablist">
                                <li role="presentation" id="hotel_search_information_tab">
                                    <a href="#" data-target="#information" aria-controls="information" role="tab" data-toggle="tab">Information</a>
                                </li>
                                <li role="presentation" id="hotel_search_amenities_tab">
                                    <a href="#" data-target="#ame" aria-controls="ame" role="tab" data-toggle="tab">Amenities</a>
                                </li>
                                <li role="presentation" id="hotel_search_pictures_tab">
                                    <a href="#" data-target="#pictures" aria-controls="pictures" role="tab" data-toggle="tab">Pictures</a>
                                </li>
                                <li role="presentation" id="hotel_search_map_tab">
                                    <a href="#" class="map_tab_clicked" data-target="#map_modal_tab" aria-controls="map" role="tab" data-toggle="tab">Map</a>
                                </li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">

                                <div role="tabpanel" class="tab-pane fade in active" id="information">
                                    <div class="info">
                                        <div class="clearfix">
                                            <div class="col-sm-6" id="information_tab_1">

                                            </div>
                                            <div class="col-sm-6" id="information_tab_2">

                                            </div>

                                        </div>

                                    </div>
                                </div>

                                <div role="tabpanel" class="tab-pane fade" id="ame">
                                    <div class="clearfix">
                                        <ul class="list-unstyled list-inline tags" id="hotel_by_id_aminities">
                                        </ul>
                                    </div>
                                </div>

                                <div role="tabpanel" class="tab-pane fade" id="pictures">
                                    <div class="clearfix">
                                        <div class="col-sm-offset-1 col-sm-10 col-sm-offset-1 slider_mb">
                                            <div class="col-xs-12" id="slider">
                                                <!-- Top part of the slider -->
                                                <div class="row">
                                                    <div class="col-sm-12" id="carousel-bounding-box">
                                                        <div class="carousel slide" id="slPicture">
                                                            <!-- Carousel items -->
                                                            <div class="carousel-inner" id = "image_slider">
                                                                <div class="active item" data-slide-number="0"><img src="https://placehold.it/470x480&text=zero"></div>
                                                                <div class="item" data-slide-number="1"><img src="https://placehold.it/470x480&text=1"></div>
                                                                <div class="item" data-slide-number="2"><img src="https://placehold.it/470x480&text=2"></div>
                                                                <div class="item" data-slide-number="3"><img src="https://placehold.it/470x480&text=3"></div>
                                                                <div class="item" data-slide-number="4"><img src="https://placehold.it/470x480&text=4"></div>
                                                                <div class="item" data-slide-number="5"><img src="https://placehold.it/470x480&text=5"></div>
                                                                <div class="item" data-slide-number="6"><img src="https://placehold.it/470x480&text=6"></div>
                                                                <div class="item" data-slide-number="7"><img src="https://placehold.it/470x480&text=7"></div>
                                                                <div class="item" data-slide-number="8"><img src="https://placehold.it/470x480&text=8"></div>
                                                                <div class="item" data-slide-number="9"><img src="https://placehold.it/470x480&text=9"></div>
                                                                <div class="item" data-slide-number="10"><img src="https://placehold.it/470x480&text=10"></div>
                                                                <div class="item" data-slide-number="11"><img src="https://placehold.it/470x480&text=11"></div>
                                                                <div class="item" data-slide-number="12"><img src="https://placehold.it/470x480&text=12"></div>
                                                                <div class="item" data-slide-number="13"><img src="https://placehold.it/470x480&text=13"></div>
                                                                <div class="item" data-slide-number="14"><img src="https://placehold.it/470x480&text=14"></div>
                                                                <div class="item" data-slide-number="15"><img src="https://placehold.it/470x480&text=15"></div>
                                                            </div>
                                                            <!-- Carousel nav -->
                                                            <a class="left carousel-control" href="#slPicture" role="button" data-slide="prev">
                                                                <span class="glyphicon glyphicon-chevron-left"></span>
                                                            </a>
                                                            <a class="right carousel-control" href="#slPicture" role="button" data-slide="next">
                                                                <span class="glyphicon glyphicon-chevron-right"></span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div><!--/Slider-->
                                        <div class="col-sm-12 hidden-xs" id="slider-thumbs">
                                            <!-- Bottom switcher of slider -->
                                            <ul class="list-unstyled" id="all_slider_images">
                                                <li class="col-sm-3">
                                                    <a class="thumbnail" id="carousel-selector-0"><img src="https://placehold.it/150x150&text=zero"></a>
                                                </li>
                                                <li class="col-sm-3">
                                                    <a class="thumbnail" id="carousel-selector-1"><img src="https://placehold.it/150x150&text=1"></a>
                                                </li>
                                                <li class="col-sm-3">
                                                    <a class="thumbnail" id="carousel-selector-2"><img src="https://placehold.it/150x150&text=2"></a>
                                                </li>
                                                <li class="col-sm-3">
                                                    <a class="thumbnail" id="carousel-selector-3"><img src="https://placehold.it/150x150&text=3"></a>
                                                </li>
                                                <li class="col-sm-3">
                                                    <a class="thumbnail" id="carousel-selector-4"><img src="https://placehold.it/150x150&text=4"></a>
                                                </li>
                                                <li class="col-sm-3">
                                                    <a class="thumbnail" id="carousel-selector-5"><img src="https://placehold.it/150x150&text=5"></a>
                                                </li>
                                                <li class="col-sm-3">
                                                    <a class="thumbnail" id="carousel-selector-6"><img src="https://placehold.it/150x150&text=6"></a>
                                                </li>
                                                <li class="col-sm-3">
                                                    <a class="thumbnail" id="carousel-selector-7"><img src="https://placehold.it/150x150&text=7"></a>
                                                </li>
                                                <li class="col-sm-3">
                                                    <a class="thumbnail" id="carousel-selector-8"><img src="https://placehold.it/150x150&text=8"></a>
                                                </li>
                                                <li class="col-sm-3">
                                                    <a class="thumbnail" id="carousel-selector-9"><img src="https://placehold.it/150x150&text=9"></a>
                                                </li>
                                                <li class="col-sm-3">
                                                    <a class="thumbnail" id="carousel-selector-10"><img src="https://placehold.it/150x150&text=10"></a>
                                                </li>
                                                <li class="col-sm-3">
                                                    <a class="thumbnail" id="carousel-selector-11"><img src="https://placehold.it/150x150&text=11"></a>
                                                </li>
                                                <li class="col-sm-3">
                                                    <a class="thumbnail" id="carousel-selector-12"><img src="https://placehold.it/150x150&text=12"></a>
                                                </li>
                                                <li class="col-sm-3">
                                                    <a class="thumbnail" id="carousel-selector-13"><img src="https://placehold.it/150x150&text=13"></a>
                                                </li>
                                                <li class="col-sm-3">
                                                    <a class="thumbnail" id="carousel-selector-14"><img src="https://placehold.it/150x150&text=14"></a>
                                                </li>
                                                <li class="col-sm-3">
                                                    <a class="thumbnail" id="carousel-selector-15"><img src="https://placehold.it/150x150&text=15"></a>
                                                </li>
                                            </ul>
                                        </div>


                                    </div>

                                </div>
                                <div role="tabpanel" class="tab-pane fade"  style="width:850px; height:250px;" id="map_modal_tab">

                                </div>

                            </div>
                        </div>
                    </div>

                </div>

            </div>

        </div>
    </div>
</div>



<script src="<?php echo base_url(); ?>public/assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>

<script src="<?php echo base_url(); ?>public/affilliate_theme/plugins/checkincheckout/bootstrap-datepicker.js" type="text/javascript"></script>

<script>

        $(document).ready(
                function ()
                {
                    $(".map_tab_clicked").click(function ()
                    {
                        initializeMapForSingleHotel(global_portArray, global_map_name);
                    }
                    );
                }
        );
</script>

<script>

    $(document).ready(
            function ()
            {
                $(".map_tab_clicked").click(function ()
                {
                    initializeMapForSingleHotel(global_portArray, global_map_name);
                }
                );
            }
    );
</script>

<script>
    var hotel_search_results;
    hotel_search_results = <?php echo json_encode($hotel_list); ?>;
    check_in = <?php echo json_encode($check_in); ?>;
    check_out = <?php echo json_encode($check_out); ?>;

    current_page = 1;
    perpage = parseInt("<?php echo $pagination_per_page ?>");
    perpage = 10;

    show_only_first_10 = 1;
    current_page = 1;
</script>

<script>
    $(document).ready(
            function ()
            {
                $(".sort_hotel").change(function () {
                    alert("sdfsdhjfsd");

                    selected_value = $(this).attr("value");

                    if ("1" == selected_value)
                    {
                        hotel_search_results.sort(function (x, y)

                        {
                            float_x = parseFloat(x['only_min_price']);
                            float_y = parseFloat(y['only_min_price']);

                            return float_x - float_y;
                        }

                        );
                    } else if ("2" == selected_value)
                    {
                        hotel_search_results.sort(function (x, y)

                        {
                            float_x = parseFloat(x['only_min_price']);
                            float_y = parseFloat(y['only_min_price']);

                            return float_y - float_x;
                        }

                        );
                    } else if ("3" == selected_value)
                    {
                        hotel_search_results.sort(function (x, y)

                        {
                            return y['star_level'].localeCompare(x['star_level']);
                        }

                        );
                    }

                    current_page = 1;
                    show_only_first_10 = 1;
                    showHotelSearchResults();
                }
                );
            }
    )

</script>

<script>
    $(document).ready(
            function ()
            {

                $("#search_hotel_with_all_options").click(function () {

                    $.post("<?php echo base_url(); ?>" + "ajax/view_search_result", $("#hotel_search_all_options_form").serialize()).done(function (data) {

                        var jsonData = JSON.parse(data.trim());
                        if (jsonData.hotelFound)
                        {
                            hotel_search_results = jsonData.hotel_list;
                            showHotelSearchResults();
                        } else
                        {
                            alert("No hotel was found with your choice.");
                            hotel_search_results = [];
                            showHotelSearchResults();
                        }
                        //var destinationId = josnData.tourico_destination_id;
                        // var multiplierInPercentage = josnData.multiplier_in_percentage;
                        // $("#percentile-"+destinationId).html( multiplierInPercentage+"%" );

                        // $("#carPriceSetModal").modal({"show":false});
                        //$("#profitPercentage").val("");
                    });
                    //return false;
                });
            }
    );
</script>

<script>
    var divRoomLabel = 2;
    var firstColumn = "sm-4";
    var secondColumn = "sm-12";
</script>

<!-- When rooms drop down is changed, show adult and children number drowp down-->
<script src="<?php echo base_url(); ?>public/hotel_search/show_children_ages_drop_down.js" type="text/javascript"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=<?=MAP_KEY?>"></script>

<script>
    var firstColumnChangeRoom = "md-4";
    var secondColumnChangeRoom = "md-4";
    var thirdColumnChangeRoom = "md-12";
    var clearFixDivStartChangeRoom = "";
    var clearFixDivEndChangeRoom = "";
</script>
<!-- When rooms drop down is changed, show adult and children number drowp down-->
<script src="<?php echo base_url(); ?>public/hotel_search/changing_rooms.js" type="text/javascript"></script>

<script>
    var marker_list = [];
    var map;
    var flightPath;
    var hotel_checked;
    var linepointList;
    var hotel_checked_lat;
    var hotel_checked_lon;

    // Sets the map on all markers in the array.
    function setMarkerByHotelId() {

        hotel_checked = [];
        linepointList = [];
        hotel_checked_lat = [];
        hotel_checked_lon = [];

        //flightPath.setMap(null);

        $(".filer_map_by_hotel_name:checked").each(
                function ()
                {
                    hotel_checked.push($(this).attr("value"));
                    hotel_checked_lat.push(parseFloat($(this).attr("latitude")));
                    hotel_checked_lon.push(parseFloat($(this).attr("longitude")));
                }
        );

        for (var i = 0; i < marker_list.length; i++)
        {
            marker_list[i].setMap(null);
        }

        for (var i = 0; i < marker_list.length; i++)
        {
            var hotel_matched_marker = 0;

            for (j = 0; j < hotel_checked.length; j++)
            {
                if (hotel_checked[j] == marker_list[i].title)
                {
                    hotel_matched_marker = 1;

                }
            }

            if (0 == hotel_checked.length)
            {
                hotel_matched_marker = 1;
            }

            if (1 == hotel_matched_marker)
            {

                linepointList.push({lat: hotel_checked_lat[j], lng: hotel_checked_lon[j]});

                marker_list[i].setMap(map);
            }
        }


        /*
         flightPath = new google.maps.Polyline({
         path: linepointList,
         geodesic: true,
         strokeColor: '#FF0000',
         strokeOpacity: 1.0,
         strokeWeight: 2
         });
         
         flightPath.setMap(map);
         */
    }

    var global_portArray;
    var global_map_name;

    function initializeMapForSingleHotel(portArray, mapElementId)
    {
        global_portArray = portArray;
        global_map_name = mapElementId;

        //create empty LatLngBounds object
        var bounds = new google.maps.LatLngBounds();
        var infowindow2 = new google.maps.InfoWindow();

        var totalLongitude = 0;
        var totalLattitude = 0;


        //var pathArray = [];
        for (var index = 0; index < portArray.length; index++)
        {
            portArray[index].longitude
            totalLongitude += parseFloat(portArray[index].longitude);
            totalLattitude += parseFloat(portArray[index].latitude);
            //console.log(portArray[index].longitude +  '   ' +  parseFloat(portArray[index].latitude));
        }

        var midLongitude = totalLongitude / portArray.length;
        var midLatitude = totalLattitude / portArray.length;

        console.log("mid centers of google map");
        console.log(midLongitude);
        console.log(midLatitude);

        //mapElementId = "hotel_map_modal";
        var mapCanvas = document.getElementById(mapElementId);

        var mapOptions =
                {
                    center: {lat: midLongitude, lng: midLatitude},
                    zoom: 12
                }

        map = new google.maps.Map(mapCanvas, mapOptions);
        //google.maps.event.trigger(map, "resize");

        google.maps.event.addListenerOnce(map, 'idle', function () {
            // do something only the first time the map is loaded
            google.maps.event.trigger(map, "resize");
            //latLang = map.getCenter();
            // map.setCenter(latLang);

            map.setCenter(new google.maps.LatLng(midLongitude, midLatitude));

        });
        var marker = [];
        for (var index = 0; index < portArray.length; index++) {
            marker[index] = new google.maps.Marker({
                position: {lat: parseFloat(portArray[index].latitude), lng: parseFloat(portArray[index].longitude)},
                map: map,
                title: portArray[index].port_name,
                index2: index
            });

            google.maps.event.addListener(marker[index], 'click', (function (index) {
                return function () {
                    infowindow[index].open(map, marker[index]);
                }

            })(index));
        }
    }


    function initialize(portArray, mapElementId)
    {
        //create empty LatLngBounds object
        var bounds = new google.maps.LatLngBounds();
        var infowindow2 = new google.maps.InfoWindow();

        var totalLongitude = 0;
        var totalLattitude = 0;
        //var pathArray = [];
        for (var index = 0; index < portArray.length; index++)
        {
            totalLongitude = parseFloat(portArray[index].longitude);
            totalLattitude = parseFloat(portArray[index].latitude);
            //console.log(portArray[index].longitude +  '   ' +  parseFloat(portArray[index].latitude));
        }

        var midLongitude = totalLongitude; ///portArray.length;
        var midLatitude = totalLattitude; ///portArray.length;

        var mapCanvas = document.getElementById(mapElementId);

        var mapOptions =
                {
                    center: {lat: midLatitude, lng: midLongitude},
                    zoom: 10
                }

        map = new google.maps.Map(mapCanvas, mapOptions);
        //google.maps.event.trigger(map, "resize");

        google.maps.event.addListenerOnce(map, 'idle', function () {
            // do something only the first time the map is loaded
            google.maps.event.trigger(map, "resize");
            //latLang = map.getCenter();
            // map.setCenter(latLang);

            map.setCenter(new google.maps.LatLng(midLatitude, midLongitude));

        });


        linepointList = [];

        var infowindow = [];
        var marker = [];

        for (var index = 0; index < portArray.length; index++)
        {
            var contentString = '<div id="content" style="width:250px; height:auto;">' +
                    '<h4 id="firstHeading" class="firstHeading">' + portArray[index].port_name + '</h4>' +
                    '<div id="bodyContent">' +
                    '<div style="width:100%;"><p>' +
                    portArray[index].location.address +
                    '</div>' +
                    '<div style="width:100%;"><p>' +
                    '<p><b>Price :</b> $' + portArray[index].only_min_price + '</p>' +
                    '</div>' +
                    '<p>' + portArray[index].book_btn + '</p>' +
                    '</div>' +
                    '</div>' +
                    '</div>';

            infowindow[index] = new google.maps.InfoWindow({
                content: contentString
            });

            marker[index] = new google.maps.Marker({
                position: {lat: parseFloat(portArray[index].latitude), lng: parseFloat(portArray[index].longitude)},
                map: map,
                title: portArray[index].port_name,
                index2: index,
                icon: portArray[index].map_icon

            });

            google.maps.event.addListener(marker[index], 'click', (function (index) {
                return function ()
                {
                    infowindow[index].open(map, marker[index]);
                }

            })(index));


            //infowindow.open(map,marker);

            //linepointList.push({lat: parseFloat(portArray[index].latitude), lng: parseFloat(portArray[index].longitude)})

            marker_list.push(marker[index]);
        }




        //}

        // port_array = [];
        // initialize(port_array , '');
        //  google.maps.event.addDomListener(window, 'load', initialize);
    }
</script>

<script>
    var start_date_pt = '<?php echo date('F d, Y H:i:s'); ?>';
    var nowTemp = new Date(start_date_pt);
    var houres = nowTemp.getHours();
    //      console.log(houres);
    if (houres > 15)
    {
        var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate() + 4, 0, 0, 0, 0);
    } else {
        var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate() + 4, 0, 0, 0, 0);
    }

    function testMap()
    {
        test = [];
        test[0] = [];
        test[0]['longitude'] = "-115.15615300000000";
        test[0]['latitude'] = "36.10802700000000";
        //initializeMapForSingleHotel(test,"map");
    }


    $(document).ready(function ()
    {

        testMap();

        var checkin = $('#check_in').datepicker({
            onRender: function (date) {

                //   console.log(date);
                if (date.valueOf() < now.valueOf())
                {
                    return 'disabled';
                } else if (date.valueOf() == now.valueOf())
                {
                    return 'pluseone';
                } else
                {
                    return '';
                }
                //return date.valueOf() < now.valueOf() ? 'disabled' : '';

            }
        }).on('changeDate', function (ev) {

            var newDate = new Date(ev.date);

            newDate.setDate(newDate.getDate() + 1);
            $('#check_out').val("")
            checkout.setValue(newDate);

            $(this).datepicker('hide');
            var check_in = $('#check_in').val();
            var check_out = $('#check_out').val();
            check_in = moment($('#check_in').val());
            check_out = moment($('#check_out').val());

            var diff = check_out.diff(check_in, 'days');
            $('.nights').text(diff);
            $('.nights').val(diff);

            $('#check_out')[0].focus();


        }).data('datepicker');

        var checkout = $('#check_out').datepicker({
            onRender: function (date) {

                //   console.log(date);
                if (date.valueOf() < now.valueOf())
                {
                    return 'disabled';
                } else if (date.valueOf() == now.valueOf())
                {
                    return 'pluseone';
                } else
                {
                    return '';
                }
                //return date.valueOf() < now.valueOf() ? 'disabled' : '';

            }
        }).on('changeDate', function (ev) {


            checkout.hide();
            var check_in = $('#check_in').val();
            var check_out = $('#check_out').val();
            if (check_in != '' && check_out != '') {
                check_in = moment($('#check_in').val());
                check_out = moment($('#check_out').val());
                //alert($('#check_out').val());
                var diff = check_out.diff(check_in, 'days');
                $('.nights').text(diff);
                $('.nights').val(diff);


                /*
                 var newDate = new Date(ev.date);
                 
                 newDate.setDate(newDate.getDate() + 1);
                 $('#check_out').val("")
                 checkout.setValue(newDate);
                 
                 //$(this).datepicker('hide');
                 var check_in = $('#check_in').val();
                 var check_out = $('#check_out').val();
                 check_in = moment($('#check_in').val());
                 check_out = moment($('#check_out').val());
                 
                 var diff = check_out.diff(check_in, 'days');
                 $('.nights').text(diff);
                 $('.nights').val(diff);
                 
                 $('#check_out')[0].focus();
                 */
            }


        }).data('datepicker');
    }
    );
    /*
     var checkout = $('#check_in').datepicker({
     onRender: function (date) {
     
     //   console.log(date);
     if (date.valueOf() < now.valueOf())
     {
     return 'disabled';
     }
     else if (date.valueOf() == now.valueOf())
     {
     return 'pluseone';
     }
     else
     {
     return '';
     }
     //return date.valueOf() < now.valueOf() ? 'disabled' : '';
     
     }
     }).on('changeDate', function (ev) {
     
     $(this).datepicker('hide');
     var check_in = $('#check_in').val();
     var check_out = $('#check_out').val();
     if (check_in != '' && check_out != '') {
     check_in = moment($('#check_in').val());
     check_out = moment($('#check_out').val());
     //alert($('#check_out').val());
     var diff = check_out.diff(check_in, 'days');
     $('.nights').text(diff);
     $('.nights').val(diff);
     
     
     }).data('datepicker');
     
     var checkin = $('#check_out').datepicker({
     onRender: function (date) {
     
     //   console.log(date);
     if (date.valueOf() < now.valueOf())
     {
     return 'disabled';
     }
     else if (date.valueOf() == now.valueOf())
     {
     return 'pluseone';
     }
     else
     {
     return '';
     }
     //return date.valueOf() < now.valueOf() ? 'disabled' : '';
     
     }
     }).on('changeDate', function (ev) {
     
     /* var newDate = new Date(ev.date);
     
     newDate.setDate(newDate.getDate() + 1);
     $('#check_out').val("")
     checkout.setValue(newDate);
     
     checkin.hide();
     var check_in = $('#check_in').val();
     var check_out = $('#check_out').val();
     check_in = moment($('#check_in').val());
     check_out = moment($('#check_out').val());
     
     var diff = check_out.diff(check_in, 'days');
     $('.nights').text(diff);
     $('.nights').val(diff);
     
     $('#check_out')[0].focus();*/


    //}).data('datepicker');


</script>

<script>
    var hotel_search_id_ajax_path = "<?php echo base_url('view_hotel_search_id') ?>";
    var locationName = "<?php echo $locationName; ?>";
    var check_in = "<?php echo $check_in; ?>";
    var check_out = "<?php echo $check_out; ?>";
    var uniqueSession = "<?php echo $_SESSION['uniqueSession']; ?>";
    var locationId = "<?php echo $locationId; ?>";
    var numberOfAdults = "<?php echo count($numberOfAdults); ?>";
    var numberOfChild = "<?php echo count($numberOfChild); ?>";

    //console.log("activity details path : ");
    $(document).ready(function ()
    {
        $('#content').on('click', '.hotel_search_details', function (event)
        {
            var hotel_id2 = $(this).attr('hotel_id');
            var tab_clicked = $(this).attr('tab_name');
            var book_now_url = $(this).attr('modal_book_now_button');
            var per_night_fee = $(this).attr('min_price');
            var hotel_type = $(this).attr('hotel-type');

            hotel_zip = "";
            hotel_city = "";
            hotel_country = "";
            hotel_state = "";
            hotel_location_name = "";

            for (i = 0; i < hotel_search_results.length; i++)
            {

                if (hotel_id2 == hotel_search_results[i]['hotelId'])
                {
                    hotel_zip = hotel_search_results[i]['hotel_location_zip'];
                    hotel_city = hotel_search_results[i]['hotel_location_city'];
                    hotel_country = hotel_search_results[i]['hotel_location_country'];
                    hotel_state = hotel_search_results[i]['hotel_location_state'];
                    hotel_location_name = hotel_search_results[i]['hotel_location_name'];
                }
            }

            var book_now_section;
            book_now_section = '<h4 style="margin:5px 0px; color: #EC2A26;">TOTAL</h4>' +
                    '<h4>$' + parseFloat(per_night_fee).toFixed(2) + '</h4>' +
                    '<a class="btn btn-danger btn-sm"  href="' + book_now_url + '">Book now!</a>';
            $("#modal_book_now").html(book_now_section);

            if (tab_clicked == "information")
            {
                $("#hotel_search_information_tab").addClass("active");

                $("#hotel_search_amenities_tab").removeClass("active");
                $("#hotel_search_map_tab").removeClass("active");
                $("#hotel_search_pictures_tab").removeClass("active");

                $("#information").addClass("active in");
                $("#ame").removeClass("active in");
                $("#pictures").removeClass("active in");
                $("#map_modal_tab").removeClass("active in");
            } else if (tab_clicked == "amenities")
            {
                $("#hotel_search_amenities_tab").addClass("active");

                $("#hotel_search_information_tab").removeClass("active");
                $("#hotel_search_map_tab").removeClass("active");
                $("#hotel_search_pictures_tab").removeClass("active");

                $("#information").removeClass("active in");
                $("#ame").addClass("active in");
                $("#pictures").removeClass("active in");
                $("#map_modal_tab").removeClass("active in");

            } else if (tab_clicked == "map")
            {
                $("#hotel_search_map_tab").addClass("active");

                $("#hotel_search_amenities_tab").removeClass("active");
                $("#hotel_search_information_tab").removeClass("active");
                $("#hotel_search_pictures_tab").removeClass("active");

                $("#information").removeClass("active in");
                $("#ame").removeClass("active in");
                $("#pictures").removeClass("active in");
                $("#map_modal_tab").addClass("active in");

            } else if (tab_clicked == "picture")
            {
                $("#hotel_search_pictures_tab").addClass("active");

                $("#hotel_search_map_tab").removeClass("active");
                $("#hotel_search_amenities_tab").removeClass("active");
                $("#hotel_search_information_tab").removeClass("active");

                $("#information").removeClass("active in");
                $("#ame").removeClass("active in");
                $("#pictures").addClass("active in");
                $("#map_modal_tab").removeClass("active in");

            }

            $(".loader").show();
            jQuery.post(hotel_search_id_ajax_path, {hotel_type: hotel_type, hotel_id: hotel_id2}).done(function (data) {

                $(".loader").hide();

                var jsonData = $.parseJSON(data);
                var hotel_by_id = jsonData.hotel_desc_by_id;

                console.log("hotel by id :");
                console.log(hotel_by_id);

                hotel_by_id_map_coordinates = [];

                for (i = 0; i < hotel_search_results.length; i++)
                {
                    if (hotel_search_results[i].hotelId == hotel_id2)
                    {
                        hotel_thumb_url = '<a class="pull-left" href="' + hotel_search_results[i].hotelThumbUrl + '">' +
                                '<img class="media-object img-responsive thumbnail mh145" src="' + hotel_search_results[i].hotelThumbUrl + '" alt="image">' +
                                '</a>';

                        $("#hotel_thumbnail_html").html(hotel_thumb_url);

                        hotel_by_id_map_coordinates[0] = [];
                        hotel_by_id_map_coordinates[0]['longitude'] = hotel_search_results[i].longitude;
                        hotel_by_id_map_coordinates[0]['latitude'] = hotel_search_results[i].latitude;
                        hotel_by_id_map_coordinates[0]['port_name'] = hotel_search_results[i].hotelName;

                        break;
                    }
                }

                initializeMapForSingleHotel(hotel_by_id_map_coordinates, "map_modal_tab");
                //initializeMapForSingleHotel(hotel_by_id_map_coordinates,"map2");

                // Information tab
                var location = hotel_by_id.location_desc;
                var facilities = hotel_by_id.facilities_desc;
                var sports_entertainment = hotel_by_id.sports_entertainment_desc;
                var payments = hotel_by_id.payment_desc;
                var rooms = hotel_by_id.room_desc;
                var meals = hotel_by_id.meals_desc;

                var info_tab = "";
                var information_column_counter = 0;

                if ("" != location)
                {
                    info_tab += '<h4 class="info-title">Location</h4>' +
                            '<ul class="list-unstyled">' +
                            '<li>' + location + '</li>' +
                            '</ul>';
                    information_column_counter++;
                }

                if ("" != facilities)
                {
                    info_tab += '<h4 class="info-title">Facilities</h4>' +
                            '<ul class="list-unstyled">' +
                            '<li>' + facilities + '</li>' +
                            '</ul>';
                    information_column_counter++;
                }

                if ("" != sports_entertainment)
                {
                    info_tab += '<h4 class="info-title">Sports/Entertainment</h4>' +
                            '<ul class="list-unstyled">' +
                            '<li>' + sports_entertainment + '</li>' +
                            '</ul>';
                    information_column_counter++;
                }

                if (information_column_counter == 3)
                {
                    $("#information_tab_1").html(info_tab);
                    info_tab = "";
                }

                if ("" != payments)
                {
                    info_tab += '<h4 class="info-title">Payment</h4>' +
                            '<ul class="list-unstyled">' +
                            '<li>' + payments + '</li>' +
                            '</ul>';
                }

                if (information_column_counter == 3)
                {
                    $("#information_tab_1").html(info_tab);
                    info_tab = "";
                }

                if ("" != rooms)
                {
                    info_tab += '<h4 class="info-title">Rooms</h4>' +
                            '<ul class="list-unstyled">' +
                            '<li>' + rooms + '</li>' +
                            '</ul>';
                }

                if (information_column_counter == 3)
                {
                    $("#information_tab_1").html(info_tab);
                    info_tab = "";
                }

                if ("" != meals)
                {
                    info_tab += '<h4 class="info-title">Facilities</h4>' +
                            '<ul class="list-unstyled">' +
                            '<li>' + meals + '</li>' +
                            '</ul>';
                }

                if (information_column_counter > 3)
                {
                    $("#information_tab_2").html(info_tab);
                    info_tab = "";
                } else
                {
                    $("#information_tab_1").html(info_tab);
                    info_tab = "";
                }

                var amenities_arr = hotel_by_id.aminities;
                var images_arr = hotel_by_id.images;

                var amenities_html = "";
                for (i = 0; i < amenities_arr.length; i++)
                {
                    amenities_html += '<li>' + amenities_arr[i] + '</li>';
                }
                $("#hotel_by_id_aminities").html(amenities_html);

                var images_path = "";
                var slide_switcher_path = "";
                if (images_arr.length > 0)
                {
                    images_path = '<div class="active item" data-slide-number="0"><img src="' + images_arr[0] + '"></div>';

                    slide_switcher_path += '<li class="col-sm-3">' +
                            '<a class="thumbnail" id="carousel-selector-0"><img src="' + images_arr[0] + '"></a>' +
                            '</li>';

                    for (i = 1; i < images_arr.length; i++)
                    {
                        images_path += '<div class="item" data-slide-number="' + i + '"><img src="' + images_arr[i] + '"></div>';

                        slide_switcher_path += '<li class="col-sm-3">' +
                                '<a class="thumbnail" id="carousel-selector-' + i + '"><img src="' + images_arr[i] + '"></a>' +
                                '</li>';
                    }

                }

                $("#image_slider").html(images_path);
                $("#all_slider_images").html(slide_switcher_path);

                var hotel_name = hotel_by_id.hotelName;
                $("#hotel_name").html('<h4>' + hotel_name + '</h4>');

                var star_level = hotel_by_id.star_level;
                $("#hotel_rating").html('<input type="text" name="rating" id="inputRating" class="srRating hidden tt form-control hide" value="' + star_level + '">');

                $(".srRating").rating({
                    disabled: true,
                    size: 'xs',
                    showClear: false,
                    hoverEnabled: false,
                    starCaptions: {
                        1: '1 Star Hotel',
                        2: '2 Star Hotel',
                        3: '3 Star Hotel',
                        4: '4 Star Hotel',
                        5: '5 Star Hotel'
                    },
                    starCaptionClasses: function (val) {
                        if (val == 0) {
                            return 'capText';
                        } else if (val < 3) {
                            return 'capText';
                        } else {
                            return 'capText';
                        }
                    }
                });


                var address = hotel_by_id.address;

                address = '<strong>' + hotel_by_id.address + '</strong><br>' + hotel_location_name + '<br>' +
                        '' + hotel_city + ' ' + hotel_state + ' ' + hotel_country + '  ' + hotel_zip + '<br>';


                $("#hotel_address").html(address);

                var hotel_phone = hotel_by_id.hotel_phone;
                $("#hotel_phone").html(hotel_phone);

                $('#pop').modal({
                    show: true,
                    remote: '/myNestedContent'
                });

            });


        }
        );
    }
    );


</script>

<script>
    jQuery(document).ready(function () {

        jQuery("#locationName").autocomplete({
            source: function (request, response) {
                jQuery.getJSON(baseurl + "get_destination_suggestion", {term: jQuery("#locationName").val()},
                        response);
            },
            minLength: 3,
            select: function (event, ui) {
                $("#locationId").val(ui.item.id);

            }
        });
    });
</script>

<script>

    jQuery(document).ready(function () {

        $(window).scroll(function () {
            if ($(window).scrollTop() + $(window).height() > $('.footer_scroll').offset().top) {

                show_only_first_10 = 0;
                current_page++;
                showHotelSearchResults();
            }
        });

        jQuery(".show_hotel_map").click(
                function ()
                {
                    showHotelSearchResults();

                }
        );

        $(".map_hotel_name_checkboxes").click(
                function ()
                {
                    alert($(this).attr("value"));
                    setMarkerByHotelId();
                }
        );

        $("#map_hotel_name_checkboxes").on("click", ".filer_map_by_hotel_name", function (event)
        {
            setMarkerByHotelId();
        }

        );
    });
</script>

<script>

    //console.log(hotel_search_results);

    var base_url = "<?php echo base_url(); ?>";

    var min_price_slider = parseFloat(hotel_search_results[0]['only_min_price']);
    var max_price_slider = parseFloat(hotel_search_results[0]['only_min_price']);

    for (i = 0; i < hotel_search_results.length; i++)
    {
        if (min_price_slider > parseFloat(hotel_search_results[i]['only_min_price']))
        {
            min_price_slider = parseFloat(hotel_search_results[i]['only_min_price']);
        }
        if (max_price_slider < parseFloat(hotel_search_results[i]['only_min_price']))
        {
            max_price_slider = parseFloat(hotel_search_results[i]['only_min_price']);
        }
    }

    //console.log(max_price_slider);
    //console.log(min_price_slider);

    max_price_slider = Math.ceil(max_price_slider);
    min_price_slider = Math.floor(min_price_slider);

    //console.log(max_price_slider);
    //console.log(min_price_slider);

    $(".location_names_checkbox").click(
            function ()
            {
                showHotelSearchResults();
            }
    );


    $(".search_by_hotel_name_button").click(
            function ()
            {
                showHotelSearchResults();
            }
    );

    $(".hotel_star_ratings").click(
            function ()
            {
                current_page = 1;
                show_only_first_10 = 1;
                showHotelSearchResults();
            }
    );

    $(".hotel_search_select_all").click(
            function ()
            {
                is_checked = $(this).is(":checked");

                $(".hotel_star_ratings").each(
                        function ()
                        {
                            if (is_checked)
                            {
                                $('.hotel_star_ratings').prop('checked', true);
                            } else
                            {
                                $('.hotel_star_ratings').prop('checked', false);
                            }
                        }
                );

                current_page = 1;
                show_only_first_10 = 1;
                showHotelSearchResults();

            }
    );

    $(".hotel_brand_select_all").click(
            function ()
            {
                $(".hotel_brand_checkbox").each(
                        function ()
                        {
                            $('.hotel_brand_checkbox').prop('checked', true);
                        }
                );

                showHotelSearchResults();
            }
    );

    $(".hotel_brand_clear_all").click(
            function ()
            {
                $(".hotel_brand_checkbox").each(
                        function ()
                        {
                            $('.hotel_brand_checkbox').prop('checked', false);
                        }
                );

                showHotelSearchResults();
            }
    );

    $(".amenities_checkbox").click(
            function ()
            {
                showHotelSearchResults();
            }
    );

    $(".hotel_brand_checkbox").click(
            function ()
            {
                showHotelSearchResults();
            }
    );

    $(".amenities_select_all").click(
            function ()
            {
                $(".amenities_checkbox").each(
                        function ()
                        {
                            $(".amenities_checkbox").prop("checked", true);
                        }
                );

                showHotelSearchResults();
            }
    );

    $(".amenities_clear_all").click(
            function ()
            {
                $(".amenities_checkbox").each(
                        function ()
                        {
                            $(".amenities_checkbox").prop("checked", false);
                        }
                );

                showHotelSearchResults();
            }
    );

    function showHotelSearchResults()
    {
        var hotel_search_location_match_db_filter = [];
        var hotel_star_filter = [];
        var hotel_aminities_filter = [];
        var hotel_brands_filter = [];
        var hotel_location_names_filter = [];
        var hotel_search_text = "";
        var hotel_location_names = "";

        //pagination varibales
        var current_pagination_page_index = 0;

        if (current_page < 1)
        {
            current_page = 1;
        }

        from_page = (current_page - 1) * perpage;

        to_page = from_page + perpage;

        $(".hotel_search_location_chkbox:checked").each(
                function ()
                {
                    hotel_search_location_match_db_filter.push($(this).attr("value"));
                }
        );

        $(".location_names_checkbox:checked").each(
                function ()
                {
                    hotel_location_names_filter.push($(this).attr("value"));
                }
        );

        var search_hotel_name = $(".search_by_hotel_name_text_box").val();
        $(".hotel_brand_checkbox:checked").each(
                function ()
                {
                    hotel_brands_filter.push($(this).attr("value"));
                }
        );

        $(".hotel_star_ratings:checked").each(
                function ()
                {
                    hotel_star_filter.push($(this).attr("value"));
                }
        );

        $(".amenities_checkbox:checked").each(
                function ()
                {
                    hotel_aminities_filter.push($(this).attr("value"));
                }
        );
        hotel_long_lat = [];
        map_hotel_name_checkboxes = "";
        total_data = hotel_search_results.length;
        page_upper_limit = Math.ceil(total_data / perpage);
        if (current_page > page_upper_limit)
        {
            return;
        }
        var pagination_button_html = "";
        if (current_page >= 3)
        {
            for (pagination_index = current_page - 2; pagination_index < current_page; pagination_index++)
            {
                pagination_button_html += '<li><a class="hotel_search_pagination" current_page="' + pagination_index + '" href = "#">' + pagination_index + '</a></li>';
            }
        } else if (current_page == 2)
        {
            pagination_button_html += '<li><a class="hotel_search_pagination" current_page="' + 1 + '" href = "#">' + 1 + '</a></li>';
        }
        pagination_button_html += '<li class="active"><a class="hotel_search_pagination" current_page="' + current_page + '" href = "#">' + current_page + '</a></li>';
        total_data = hotel_search_results.length;
        data_per_page = perpage;
        remaining_data = total_data - data_per_page * current_page;
        show_next_data = remaining_data % data_per_page;
        page_upper_limit = Math.ceil(total_data / data_per_page);
        //page_upper_limit = last_page + ((total_data / data_per_page > 0) ? 1 : 0);
        for (pagination_index = 0; pagination_index <= data_per_page; pagination_index++)
        {
            if ((pagination_index > 1) || ((current_page + pagination_index + 1) > page_upper_limit))
                break;

            next_page_index = current_page + pagination_index + 1;
            pagination_button_html += '<li><a class="hotel_search_pagination" current_page="' + next_page_index + '" href = "#">' + next_page_index + '</a></li>';
        }
        if (current_page >= 2)
        {
            pagination_button_html = '<li><a class="hotel_search_pagination" current_page="previous_page" href = "#">&laquo;</a></li>' + pagination_button_html;
        } else
        {
            pagination_button_html = '<li class="disabled"><a class="hotel_search_pagination" current_page="previous_page" href = "#">&laquo;</a></li>' + pagination_button_html;
        }
        if (current_page == page_upper_limit)
        {
            pagination_button_html += '<li class="disabled"><a class="hotel_search_pagination disabled" current_page="next_page" href = "#">&raquo;</a></li>';
        } else
        {
            pagination_button_html += '<li><a class="hotel_search_pagination" current_page="next_page" href = "#">&raquo;</a></li>';
        }
        pagination_button_html = '<ul class = "pagination pagination_buttons">' + pagination_button_html;
        pagination_button_html += '</ul>';
        total_result_found = hotel_search_results.length;
        $("#total_hotels_found_id").html('<p>We found <b style="font-size: 15px;">' + total_result_found + '</b> hotels for you!</p>');
        filtered_map_array_index = 0;
        total_hotels_found = 0;
        //console.log(hotel_search_results);
        for (i = 0; i < hotel_search_results.length; i++)
        {
            var hotel_star_matched = 0;
            var hotel_aminities_matched = 0;
            var hotel_brand_matched = 0;
            var hotel_name_matched = 0;
            var hotel_location_name_matched = 0;
            var hotel_search_location_match_db_filter_matched = 0;

            var location_other_tab_isChecked = $('#hotel_location_other_chk').is(':checked');

            if (location_other_tab_isChecked)
            {
                find_in_location_db = 0;
                find_in_first_nine_item = 0;
                for (location_index = 0; location_index < location_names_from_db.length; location_index++)
                {
                    if (hotel_search_results[i]['hotel_location_name'] == location_names_from_db[location_index])
                    {
                        find_in_location_db = 1;
                    }
                }

                for (location_index = 0; location_index < first_nine_location_names.length; location_index++)
                {
                    if (hotel_search_results[i]['hotel_location_name'] == first_nine_location_names[location_index])
                    {
                        find_in_first_nine_item = 1;
                    }
                }

                alert(find_in_first_nine_item);

                if ((0 == find_in_first_nine_item) && (1 == find_in_location_db))
                {
                    show_other_location_condition = 1;
                } else
                {
                    show_other_location_condition = 0;
                }

            } else
            {
                show_other_location_condition = 1;
            }

            for (location_name_filter_index = 0; location_name_filter_index < hotel_location_names_filter.length; location_name_filter_index++)
            {
                if (hotel_location_names_filter[location_name_filter_index] == hotel_search_results[i]['hotel_location_name'])
                {
                    hotel_location_name_matched = 1;
                }
            }
            if (hotel_location_names_filter.length == 0)
            {
                hotel_location_name_matched = 1;
            }

            var hotel_name = $(".search_by_hotel_name_text_box").val();
            if ("" == hotel_name)
            {
                hotel_name_matched = 1;
            } else
            {
                if (-1 != hotel_search_results[i]['hotelName'].search(hotel_name))
                {
                    hotel_name_matched = 1;
                }
            }

            for (j = 0; j < hotel_search_location_match_db_filter.length; j++)
            {
                if (hotel_search_results[i]['hotel_location_name'] == hotel_search_location_match_db_filter[j])
                {
                    hotel_search_location_match_db_filter_matched = 1;
                }
            }
            if (0 == hotel_search_location_match_db_filter.length)
            {
                hotel_search_location_match_db_filter_matched = 1;
            }
            for (j = 0; j < hotel_brands_filter.length; j++)
            {
                if (hotel_search_results[i]['hotel_brand_name'] == hotel_brands_filter[j])
                {
                    hotel_brand_matched = 1;
                }
            }
            if (0 == hotel_brands_filter.length)
            {
                hotel_brand_matched = 1;
            }

            for (j = 0; j < hotel_star_filter.length; j++)
            {
                if (hotel_search_results[i]['star_level'] == hotel_star_filter[j])
                {
                    hotel_star_matched = 1;
                }
            }

            if ((0 == hotel_star_filter.length))
            {
                hotel_star_matched = 1;
            }

            aminities_arr = hotel_search_results[i]['aminities'];
            for (k = 0; k < aminities_arr.length; k++)
            {
                for (j = 0; j < hotel_aminities_filter.length; j++) {

                    if (aminities_arr[k] == hotel_aminities_filter[j]) {
                        hotel_aminities_matched = 1;
                    }
                }
            }

            if (0 == hotel_aminities_filter.length)
            {
                hotel_aminities_matched = 1;
            }

            price_matched = 0;
            if (parseFloat(hotel_search_results[i]['only_min_price']) >= parseFloat(min_price_slider) && parseFloat(hotel_search_results[i]['only_min_price']) <= parseFloat(max_price_slider))
            {
                price_matched = 1;
            }

            if ((1 == show_other_location_condition) && (1 == hotel_search_location_match_db_filter_matched) && (1 == hotel_location_name_matched) && (1 == hotel_name_matched) && (1 == price_matched) && (1 == hotel_star_matched) && (1 == hotel_aminities_matched) && (1 == hotel_brand_matched))
            {
                total_hotels_found++;
                current_pagination_page_index++;

                if ((current_pagination_page_index >= from_page) && (current_pagination_page_index < to_page))
                {
                    room_type_list = hotel_search_results[i]['room_type_list'];
                    hotel_id = hotel_search_results[i]['hotelId'];
                    voucher_remarks = hotel_search_results[i]['voucher_remarks'];
                    hotel_location_zip = hotel_search_results[i]['hotel_location_zip'];
                    hotel_location_city = hotel_search_results[i]['hotel_location_city'];
                    hotel_location_country = hotel_search_results[i]['hotel_location_country'];
                    hotel_location_name = hotel_search_results[i]['hotel_location_name'];
                    hotelName = hotel_search_results[i]['hotelName'];

                    room_type_rows_html = "";
                    var global_min = 2000002;
                    var lowest_count = 1;
                    var lowest_count_for_book = 1;
                    for (room_type_index = 0; room_type_index < room_type_list.length; room_type_index++)
                    {
                        discount = room_type_list[room_type_index]['discount'];
                        room_type_index_for_lowest_price = 0;
                        occupancy_lists = room_type_list[room_type_index]['occupancy_list'];
                        temp_RoomId = room_type_list[room_type_index]['RoomId'];
                        temp_RoomTypeId = room_type_list[room_type_index]['RoomTypeId'];
                        hasfree = false;
                        hasfreebreakfast = "";
                        freesup = "";
                        for (var frsupi = 0; frsupi < room_type_list[room_type_index]['supp'].length; frsupi++)
                        {
                            if (room_type_list[room_type_index]['supp'][frsupi].price == "0.00")
                            {
                                freesup += "<tr class='discount_tr'><td colspan='5'><img src='https://www.concretereading.co.uk/images/tick.svg' width='20' height='20' />Free " + room_type_list[room_type_index]['supp'][frsupi].suppName + "</td></tr>";
                            }
                        }
                        var min_price = 2000000;
                        var addedboardbases = [];
                        for (var prop in occupancy_lists)
                        {
                            var tot_rooms = Object.keys(occupancy_lists).length;
                            var roomNo = '<?=$_GET['roomNo']?>';
                            var temp_price = 0;
//                            for(temp_i = 1; temp_i <= tot_rooms; temp_i++){
//                                temp_price = temp_price + occupancy_lists['Room '+temp_i]['available_list'][0]['total_price_inc_tax'];
//                            }
                                if(roomNo == tot_rooms){
                                    for (var key in occupancy_lists){
                                        temp_price = temp_price + occupancy_lists[key]['available_list'][0]['total_price_inc_tax'];
                                    }
                                } else {
                                    var tc = 0;
                                    for (var key in occupancy_lists){
                                        temp_price = temp_price + occupancy_lists[key]['available_list'][0]['total_price_inc_tax'];
                                        tc++;
                                    }
                                    temp_price = temp_price / tc * roomNo;
                                }

                                
                            temp_price = parseFloat(temp_price).toFixed(2);
//                            console.log(temp_price);
//                            console.log('end');
//                            console.log(occupancy_lists['Room 1']['available_list'][0]['total_price_inc_tax']);
//                            console.log(occupancy_lists['Room 2']['available_list'][0]['total_price_inc_tax']);
//                            console.log(occupancy_lists['Room 3']['available_list'][0]['total_price_inc_tax']);
//                            console.log('...............................................................');
                            if (lowest_count == 1) {
                                var lowest_price = occupancy_lists['Room 1']['available_list'][0]['total_price_inc_tax'];
                                lowest_price = temp_price;
                                lowest_count++;
                            }

                            available_lists = occupancy_lists[prop]['available_list'];
                            for (available_list_index = 0; available_list_index < available_lists.length; available_list_index++)
                            {
                                for (var bbi = 0; bbi < available_lists[available_list_index]['BoardBases'].length; bbi++)
                                {
                                    /*
                                     if(
                                     available_lists[available_list_index]['BoardBases'][bbi]['bbPublishPrice'] == 0 && 
                                     available_lists[available_list_index]['BoardBases'][bbi]['bbName'] == "Continental Breakfast"
                                     )
                                     */
                                    if (
                                            available_lists[available_list_index]['BoardBases'][bbi]['bbPublishPrice'] == 0 &&
                                            $.inArray(available_lists[available_list_index]['BoardBases'][bbi]['bbId'], addedboardbases) == -1
                                            )
                                    {
                                        addedboardbases.push(available_lists[available_list_index]['BoardBases'][bbi]['bbId']);
                                        hasfreebreakfast += "<tr class='discount_tr'><td colspan='5'><img src='https://www.concretereading.co.uk/images/tick.svg' width='20' height='20' />Free " + available_lists[available_list_index]['BoardBases'][bbi]['bbName'] + "</td></tr>";
                                        hasfree = true;
                                    }
                                }
                                if (min_price > parseFloat(available_lists[available_list_index]['per_night_price']))
                                {
                                    min_price = parseFloat(available_lists[available_list_index]['per_night_price']).toFixed(2);
                                    room_type_index_for_lowest_price = room_type_index;

                                }
                                min_price = temp_price;
                            }
                        }



                        if (global_min > min_price)
                        {
                            global_min = min_price;
                            lowest_roomId = temp_RoomId;
                            lowest_roomTypeId = temp_RoomTypeId;
                            modal_book_now_url = base_url + 'request_quote?hotelId=' + hotel_search_results[i]['hotelId'] + '&roomId=' + lowest_roomId + '&roomTypeId=' + lowest_roomTypeId + '&stp=1';
                        }
                        room_type_availability = (room_type_list[room_type_index]['isAvailable'] == "true") ? "Available" : "On Request";
                        roomId = room_type_list[room_type_index]['RoomId'];
                        roomTypeId = room_type_list[room_type_index]['RoomTypeId'];
                        HotelType = room_type_list[room_type_index]['HotelType'];
                        hotel_supp_str = '';
                        for (hotelSupIndex = 0; hotelSupIndex < hotel_search_results[i]['room_type_list'][room_type_index]['supp'].length; hotelSupIndex++)
                        {
                            hotel_supp_str += '<input type="hidden" name="sup[' + hotelSupIndex + '][suppId]" value="' + hotel_search_results[i]['room_type_list'][room_type_index]['supp'][0]['suppId'] + '">' +
                                    '<input type="hidden" name="sup[' + hotelSupIndex + '][suppName]" value="' + hotel_search_results[i]['room_type_list'][room_type_index]['supp'][0]['suppName'] + '">' +
                                    '<input type="hidden" name="sup[' + hotelSupIndex + '][supptType]" value="' + hotel_search_results[i]['room_type_list'][room_type_index]['supp'][0]['supptType'] + '">' +
                                    '<input type="hidden" name="sup[' + hotelSupIndex + '][suppIsMandatory]" value="' + hotel_search_results[i]['room_type_list'][room_type_index]['supp'][0]['suppIsMandatory'] + '">' +
                                    '<input type="hidden" name="sup[' + hotelSupIndex + '][suppChargeType]" value="' + hotel_search_results[i]['room_type_list'][room_type_index]['supp'][0]['suppChargeType'] + '">' +
                                    '<input type="hidden" name="sup[' + hotelSupIndex + '][price]" value="' + hotel_search_results[i]['room_type_list'][room_type_index]['supp'][0]['price'] + '">' +
                                    '<input type="hidden" name="sup[' + hotelSupIndex + '][publishPrice]" value="' + hotel_search_results[i]['room_type_list'][room_type_index]['supp'][0]['publishPrice'] + '">';
                        }
                        var addrestext = '';
                        if (typeof hotel_search_results[i]['location'][['@attributes']] != 'undefined')
                        {
                            addrestext = '<input type="hidden" value="' + hotel_search_results[i]['location']['@attributes']['address'] + '" name="address"/>' +
                                    '<input type="hidden" value="' + hotel_search_results[i]['location']['@attributes']['city'] + '" name="city"/>' +
                                    '<input type="hidden" value="' + hotel_search_results[i]['location']['@attributes']['country'] + '" name="country"/>' +
                                    '<input type="hidden" value="' + hotel_search_results[i]['location']['@attributes']['destinationCode'] + '" name="destinationCode"/>' +
                                    '<input type="hidden" value="' + hotel_search_results[i]['location']['@attributes']['destinationId'] + '" name="destinationId"/>' +
                                    '<input type="hidden" value="' + hotel_search_results[i]['location']['@attributes']['latitude'] + '" name="latitude"/>' +
                                    '<input type="hidden" value="' + hotel_search_results[i]['location']['@attributes']['longitude'] + '" name="longitude"/>' +
                                    '<input type="hidden" value="' + hotel_search_results[i]['location']['@attributes']['searchingCity'] + '" name="searchingCity"/>' +
                                    '<input type="hidden" value="' + hotel_search_results[i]['location']['@attributes']['state'] + '" name="state"/>' +
                                    '<input type="hidden" value="' + hotel_search_results[i]['location']['@attributes']['zip'] + '" name="zip"/>';
                        }
                        discountstr = "";
                        if (hasfree)
                        {
                            discountstr += hasfreebreakfast;
                        }
                        if (freesup)
                        {
                            discountstr += freesup;
                        }
                        if (Object.keys(discount).length > 0)
                        {
                            if (discount.discType == "ProgressivePromotion")
                            {
                                if (discount.name != "")
                                {
                                    discountstr += "<tr class='discount_tr'><td colspan='5'><span class='speciel_deal_discount'>" + discount.name + "</span> Get " + discount.value + "% Discount on booking from " + discount.from + " to " + discount.to + "</td></tr>";
                                } else
                                {
                                    discountstr += "<tr class='discount_tr'><td colspan='5'>Get " + discount.value + "% Discount on booking from " + discount.from + " to " + discount.to + "</td></tr>";
                                }
                            }
                            else if(discount.discType == "TL_DISCOUNT"){
                             //discountstr += "<tr class='discount_tr'><td colspan='5'><span class='speciel_deal_discount'>Discount </span>$"+discount.amount+" will be applied on total amount</td></tr>";
                             }
                            else
                            {
                                discountstr += "<tr class='discount_tr'><td colspan='5'>Discount from " + discount.from + " to " + discount.to + " pay for " + discount.pay + " nights stay for " + discount.stay + " nights</td></tr>";
                            }
                        }
                        room_type_rows_html += '<tr>' +
                                '<td>' + room_type_list[room_type_index]['name'] + '</td>' +
                                '<td>' + room_type_availability + '</td>' +
                                '<td class="popover-link"><a minR="' + min_price + '" first_index="' + i + '" second_index="' + room_type_index + '" href="javascript:void(0)">Rates & Policies</a></td>' +
                                '<td>$' + temp_price + '</td><td><form target="_blank" action="' + base_url + 'request_quote?stp=f" name="' + room_type_index + '" id="hotel' + hotel_search_results[i]['hotelId'] + room_type_index + '" method="post">' +
                                '<input type="hidden" value="' + room_type_list[room_type_index]['name'] + '" name="room_type_category"/>' +
                                '<input type="hidden" value="' + hotel_search_results[i]['hotelId'] + '" name="hotelId"/>' +
                                '<input type="hidden" value="' + HotelType + '" name="hotelType"/>' +
                                '<input type="hidden" value="' + occupancy_lists['Room 1']['available_list'][0]['total_price_inc_tax'] + '" name="minPrice"/>' +
                                '<input type="hidden" value="' + voucher_remarks + '" name="voucher_remarks"/>' +
                                '<input type="hidden" value="' + locationName + '" name="locationName"/>' +
                                '<input type="hidden" value="' + check_in + '" name="from"/>' +
                                '<input type="hidden" value="' + check_out + '" name="top"/>' +
                                '<input type="hidden" value="' + uniqueSession + '" name="uniqueSession"/>' +
                                '<input type="hidden" value="' + locationId + '" name="locationId"/>' +
                                '<input type="hidden" value="' + numberOfAdults + '" name="adultsNo"/>' +
                                '<input type="hidden" value="' + numberOfChild + '" name="childrenNo"/>' +
                                '<input type="hidden" value="' + hotelName + '" name="hotelName"/>' +
                                '<input type="hidden" value="' + hotel_search_results[i]['star_level'] + '" name="starRating"/>' +
                                '<input type="hidden" value="' + roomTypeId + '" name="roomTypeId"/>' +
                                '<input type="hidden" value="' + room_type_availability + '" name="avalibilty"/>' + addrestext +
                                '<input type="hidden" value="' + roomId + '" name="roomId"/>' + hotel_supp_str +
                                '<input class="btn btn-danger btn-xs" name="" type="submit" value="Book now!"></form></td>' +
                                '</tr>' + discountstr;
                                
                                if (lowest_count_for_book == 1) {
                                var book_btn = '<form target="_blank" action="' + base_url + 'request_quote?stp=f" name="' + room_type_index + '" id="hotel' + hotel_search_results[i]['hotelId'] + room_type_index + '" method="post">' +
                                '<input type="hidden" value="' + room_type_list[room_type_index]['name'] + '" name="room_type_category"/>' +
                                '<input type="hidden" value="' + hotel_search_results[i]['hotelId'] + '" name="hotelId"/>' +
                                '<input type="hidden" value="' + HotelType + '" name="hotelType"/>' +
                                '<input type="hidden" value="' + occupancy_lists['Room 1']['available_list'][0]['total_price_inc_tax'] + '" name="minPrice"/>' +
                                '<input type="hidden" value="' + voucher_remarks + '" name="voucher_remarks"/>' +
                                '<input type="hidden" value="' + locationName + '" name="locationName"/>' +
                                '<input type="hidden" value="' + check_in + '" name="from"/>' +
                                '<input type="hidden" value="' + check_out + '" name="top"/>' +
                                '<input type="hidden" value="' + uniqueSession + '" name="uniqueSession"/>' +
                                '<input type="hidden" value="' + locationId + '" name="locationId"/>' +
                                '<input type="hidden" value="' + numberOfAdults + '" name="adultsNo"/>' +
                                '<input type="hidden" value="' + numberOfChild + '" name="childrenNo"/>' +
                                '<input type="hidden" value="' + hotelName + '" name="hotelName"/>' +
                                '<input type="hidden" value="' + hotel_search_results[i]['star_level'] + '" name="starRating"/>' +
                                '<input type="hidden" value="' + roomTypeId + '" name="roomTypeId"/>' +
                                '<input type="hidden" value="' + room_type_availability + '" name="avalibilty"/>' + addrestext +
                                '<input type="hidden" value="' + roomId + '" name="roomId"/>' + hotel_supp_str +
                                '<input class="btn btn-danger btn-xs" name="" type="submit" value="Book now!"></form>';
                                
                                lowest_count_for_book++;
                            }
                    }
                    /*distance_from_airport = hotel_search_results[i]['distanceFromAirport'];
                     distance_from_airport = parseFloat(distance_from_airport).toFixed(2);*/
                    hotel_search_results[i]['only_min_price'] = parseFloat(hotel_search_results[i]['only_min_price']);

                    hotel_search_text += '<div class="panel panel-info" id="sResult">' +
                            '<div class="panel-heading clearfix">' +
                            '<div class="panel-title pull-left">' +
                            '<h4>' + hotel_search_results[i]['hotelName'] + '</h4>' +
                            '</div>' +
                            '<div class="panel-option pull-right">' +
                            '<input type="text" name="rating" id="inputRating" class="srRating hidden" value="' + hotel_search_results[i]['star_level'] + '">' +
                            '</div>' +
                            '</div>' +
                            '<div class="panel-body">' +
                            '<div class="media">' +
                            '<a tab_name="picture" href="javascript:void(0)" class="pull-left hotel_search_details" hotel_id="' + hotel_search_results[i]['hotelId'] + '" modal_book_now_button="' + modal_book_now_url + '" min_price="' + hotel_search_results[i]['only_min_price'].toFixed(2) + '">' +
                            '<img style="width: 100px;height: 100px;" class="media-object img-responsive thumbnail hotel_search_details" src="' + hotel_search_results[i]['hotelThumbUrl'] + '"" alt="image">' +
                            '</a>' +
                            '<div class="media-body">' +
                            '<div class="descrip">' +
                            '<ul class="list-unstyled list-inline">' +
                            '<li>' +
                            '<a tab_name="information" href="javascript:void(0)" hotel-type="' + hotel_search_results[i]['hotelType'] + '" form-id="hotel' + hotel_search_results[i]['hotelId'] + '" class="hotel_search_details" hotel_id="' + hotel_search_results[i]['hotelId'] + '" modal_book_now_button="' + modal_book_now_url + '" min_price="' + lowest_price + '"><span class="glyphicon glyphicon-info-sign hotel_search_details"></span>Information</a>' +
                            '</li>' +
                            '<li>' +
                            '<a tab_name="amenities" class="hotel_search_details" hotel-type="' + hotel_search_results[i]['hotelType'] + '" form-id="hotel' + hotel_search_results[i]['hotelId'] + '" href="javascript:void(0)" hotel_id="' + hotel_search_results[i]['hotelId'] + '" modal_book_now_button="' + modal_book_now_url + '" min_price="' + lowest_price + '"><span class="fa fa-tasks"></span>Amenities</a>' +
                            '</li>' +
                            '<li>' +
                            '<a tab_name="map" href="javascript:void(0)" class="hotel_search_details" hotel-type="' + hotel_search_results[i]['hotelType'] + '" form-id="hotel' + hotel_search_results[i]['hotelId'] + '" hotel_id="' + hotel_search_results[i]['hotelId'] + '" modal_book_now_button="' + modal_book_now_url + '" min_price="' + lowest_price + '"><span class="glyphicon glyphicon-map-marker hotel_search_details"></span>Map</a>' +
                            '</li>' +
                            '<li>' +
                            '<a tab_name="picture" href="javascript:void(0)" class="hotel_search_details" hotel-type="' + hotel_search_results[i]['hotelType'] + '" form-id="hotel' + hotel_search_results[i]['hotelId'] + '" hotel_id="' + hotel_search_results[i]['hotelId'] + '" modal_book_now_button="' + modal_book_now_url + '" min_price="' + lowest_price + '"><span class="glyphicon glyphicon-picture hotel_search_details"></span>Pictures</a>' +
                            '</li>' +
                            '</ul>' +
                            '<!--<ul class="list-unstyled list-inline">' +
                            '<li><a href="#"><span class="fb"></span></a></li>' +
                            '<li><a href="#"><span class="tw"></span></a></li>' +
                            '<li><a href="#"><span class="mail"></span></a></li>' +
                            '</ul>-->' +
                            '</div>' +
                            '<div class="media-price">' +
                            '<h4 style="text-align: center; margin:5px 0px; color: #EC2A26;">TOTAL</h4>' +
                            '<h4>\$' + parseFloat(lowest_price).toFixed(2) + '</h4>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '<div class="panel-lower-body">' +
                            '</div>' +
                            '<div class="panel-footer">' +
                            '<div class="table-responsive">' +
                            '<table class="table">' +
                            '<thead style="background: transparent;">' +
                            '<tr>' +
                            '<th>Room Type</th>' +
                            '<th>Status</th>' +
                            '<th>Policy</th>' +
                            '<th>Total</th>' +
                            '<th>&nbsp;</th>' +
                            '</tr>' +
                            '</thead>' +
                            '<tbody>' +
                            room_type_rows_html +
                            '</tbody>' +
                            '</table>' +
                            '</div>' +
                            '</div>' +
                            '</div>';
                }

                show_num_hotel_str = '<p>We found <b style="font-size: 15px;">' + total_hotels_found + '</b> hotels for you!</p>';
                $("#total_hotels_found_id").html(show_num_hotel_str);

                map_icon_index = filtered_map_array_index + 1;

                hotel_long_lat[filtered_map_array_index] = [];
                hotel_long_lat[filtered_map_array_index]['latitude'] = hotel_search_results[i]['latitude'];
                hotel_long_lat[filtered_map_array_index]['longitude'] = hotel_search_results[i]['longitude'];
                hotel_long_lat[filtered_map_array_index]['port_name'] = hotel_search_results[i]['hotelName'];
                hotel_long_lat[filtered_map_array_index]['only_min_price'] = parseFloat(lowest_price).toFixed(2);
                hotel_long_lat[filtered_map_array_index]['location'] = hotel_search_results[i]['location'];
                hotel_long_lat[filtered_map_array_index]['rooms'] = hotel_search_results[i]['rooms'];
                hotel_long_lat[filtered_map_array_index]['book_btn'] = book_btn;
                hotel_long_lat[filtered_map_array_index]['map_icon'] = 'https://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=' + map_icon_index + '|FE6256|000000'

                map_hotel_name_checkboxes += '<tr>' +
                        '<td style="width:10%">' +
                        '<div class="checker"><span><img src="' + 'https://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=' + map_icon_index + '|FE6256|000000' + '"></span></div>' +
                        '</td>' +
                        '<td style="width:10%">' +
                        '<div class="checker"><span><input latitude="' + hotel_search_results[i]['latitude'] + '" longitude="' + hotel_search_results[i]['longitude'] + '" class="filer_map_by_hotel_name" type="checkbox" value="' + hotel_search_results[i]['hotelName'] + '"></span></div>' +
                        '</td>' +
                        '<td style="width:80%">' +
                        '<label for="chklocationsFilter12797">' + hotel_search_results[i]['hotelName'] + '</label>' +
                        '</td>' +
                        '</tr>';
                filtered_map_array_index++;
            }
        }

        initialize(hotel_long_lat, "map");

        var bindedElement = $(hotel_search_text);
        bindedElement.find(".srRating").rating({
            disabled: true,
            size: 'xs',
            showClear: false,
            hoverEnabled: false,
            starCaptions: {
                1: '1 Star Hotel',
                2: '2 Star Hotel',
                3: '3 Star Hotel',
                4: '4 Star Hotel',
                5: '5 Star Hotel'
            },
            starCaptionClasses: function (val) {
                if (val == 0) {
                    return 'capText';
                } else if (val < 3) {
                    return 'capText';
                } else {
                    return 'capText';
                }
            }

        });

        bindedElement.find(".popover-link a").click(
                function ()
                {
                    var pos = $(this).offset();

                    $("#popover").removeClass('hide').offset({top: pos.top - $("#popover").height(), left: pos.left - ($(this).width() * 3)});
                    /*
                     $("#popover").removeClass('hide').offset({top: pos.top - $("#popover").height()});
                     */
                    first_index = parseInt($(this).attr("first_index"));
                    second_index = parseInt($(this).attr("second_index"));
                    minR = parseFloat($(this).attr("minR"));
                    hotel_id = hotel_search_results[first_index]['hotelId'];
                    HotelType = hotel_search_results[first_index]['room_type_list'][second_index]['HotelType'];
                    room_type_id = hotel_search_results[first_index]['room_type_list'][second_index]['RoomTypeId'];
                    $("#cancellation_policy_text").html(" ");
                    if (HotelType == 'TL') {
                        $.post(base_url + "ajax/TL_HotelPolicy", {
                            room_type_id: room_type_id, minR: minR
                        }).done(function (data) {
                            $("#cancellation_policy_text").html(data);
                        });
                    } else {
                        $.post("<?php echo base_url(); ?>" + "ajax/getCancellationPoliciesBeforeReservation", {
                            hotel_id: hotel_id,
                            room_type_id: room_type_id,
                            check_in: check_in,
                            check_out: check_out
                        }).done(function (data) {
                            var jsonData = JSON.parse(data.trim());
                            policy_messages = "";
                            cancellation_policies_list = jsonData.cancellation_policies_list;
                            for (policy_index = 0; policy_index < cancellation_policies_list.length; policy_index++) {
                                policy_messages += cancellation_policies_list[policy_index].message;
                                policy_messages += "</br></br>";
                            }
                            $("#cancellation_policy_text").html(policy_messages);
                        });
                    }
                    occupancy_lists = hotel_search_results[first_index]['room_type_list'][second_index]['occupancy_list'];
                    facilities = hotel_search_results[first_index]['room_type_list'][second_index]['facilities'];
                    facility_str = "";
                    //Edit by raheel 6-12-2016
                    /*if (facilities[0] != undefined)
                     facility_str += facilities[0]['name'];
                     for (facility_index = 1; facility_index < facilities.length; facility_index++)
                     {
                     facility_str += " , " + facilities[facility_index]['name'];
                     }*/
                    $("facility_lists").html(facility_str);
                    sort_occupancy_list = [];
                    for (var prop in occupancy_lists)
                    {
                        sort_occupancy_list.push([prop, occupancy_lists[prop]['room_sequnce']]);
                    }
                    sort_occupancy_list.sort(
                            function (x, y)
                            {
                                return parseInt(x[1]) - parseInt(y[1]);
                            }
                    );
                    header = "<th></th><th>Total</th>";
                    var totDays = JSON.parse('<?php echo $days; ?>');
                    for (ocu_index = 0; ocu_index < 1; ocu_index++)
                    {
                        prop = sort_occupancy_list[ocu_index][0];
                        availability_list = occupancy_lists[prop]['available_list'];
                        per_day_price_list = availability_list[0]['per_day_price'];
                        var numbeday = 0;
                        if (totDays.length > 7)
                        {
                            numbeday = 7;
                        } else
                        {
                            numbeday = totDays.length;
                        }
                        for (per_day_index = 0; per_day_index < numbeday; per_day_index++)
                        {
                            header += '<th>' + totDays[per_day_index] + '</th>';
                        }
                    }
                    $("#rate_policies_header").html(header);
                    col = "";
                    for (ocu_index = 0; ocu_index < sort_occupancy_list.length; ocu_index++)
                    {
                        prop = sort_occupancy_list[ocu_index][0];
                        if (totDays.length > 7)
                        {
                            col += '<tr><td colspan=' + 9 + '><b>' + prop + '</b></td></tr>';
                        } else
                        {
                            col += '<tr><td colspan=' + (totDays.length + 2) + '><b>' + prop + '</b></td></tr>';
                        }
                        availability_list = occupancy_lists[prop]['available_list'];
                        bedding_text = "";
                        for (available_index = 0; available_index < availability_list.length; available_index++)
                        {
                            bedding = availability_list[available_index]['bedding'];
                            bedding_arr = bedding.split(",");
                            if (bedding_arr[1] == 0) {
                                bedding_text += bedding_arr[0] + " Adults";
                            } else {
                                bedding_text += bedding_arr[0] + " Adults<br>" + bedding_arr[1] + " Bed </br>";
                            }

                            var toweek = totDays.length / 7;
                            toweek = parseInt(toweek);
                            var rowgap = 0;
                            if (totDays.length % 7 == 0)
                            {
                                rowgap = toweek;
                            } else
                            {
                                rowgap = toweek + 1;
                            }
                            col += '<td rowspan=' + (rowgap) + '>' + bedding_text + '</td>';
                            bedding_text = "";
                            bedding = availability_list[available_index]['bedding'];
                            bedding_arr = bedding.split(",");
                            col += '<td rowspan=' + (rowgap) + '>$' + parseFloat(availability_list[available_index]['total_price_inc_tax']).toFixed(2) + '</td>';
                            per_day_price_list = availability_list[available_index]['per_day_price_inc_tax'];
                            per_day_availability_list = availability_list[available_index]['per_day_availability'];
                            ib = 1;
                            for (per_day_index = 0; per_day_index < per_day_price_list.length; per_day_index++)
                            {
                                availability_text = "";
                                if (per_day_availability_list[per_day_index] == 'true')
                                {
                                    availability_text = "Available";
                                } else
                                {
                                    availability_text = "On Request";
                                }

                                if (per_day_index / 7 == ib)
                                {
                                    col += "</tr><tr>";
                                    ib++;
                                }
                                col += "<td align='center'>";
                                col += availability_text + '<br>$' + parseFloat(per_day_price_list[per_day_index]).toFixed(2);
                                col += "</td>";
                            }
                            col = '<tr>' + col + '</tr>';
                        }
                    }
                    $("#rate_policies_body").html(col);
                });

        $(".hotel-rate-and-policy a").click(function (event) {
            $("#popover").addClass('hide')
        });

        if (show_only_first_10 == 0)
        {
            $("#content").append(bindedElement);
        } else
        {
            $("#content").html(bindedElement);
        }

        $("#map_hotel_checkboxes_table_rows").html(map_hotel_name_checkboxes);

    }

</script>

<script>
    $(document).ready(function () {
        showHotelSearchResults();
    });
</script>
<script src="<?php echo base_url(); ?>public/affilliate_theme/javascripts/custom/sResultC.js" type="text/javascript"></script>