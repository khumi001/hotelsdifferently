<script>
    var car_search_results = "<?php json_encode($car_list);  ?>";
    /*console.log(car_search_results); */
</script>

<div class="width-row">
    <form class="" action="<?php echo base_url(); ?>view_activity_search_result" method="post" id="carsForm" >
    <div class="clearfix">
        <div class="col-sm-4">
            <div class="sWidgets">
                <div id="" class="clearfix mb10">
                    <label class="control-label col-md-12">Search Car In:</label>
                    <label class="radio-inline" style="display:none;">
                        <div class="radio" style="display:none;"><input type="radio" name="airportOrCity" value="Airport"></div>Airport
                    </label>
                    <label class="radio-inline" style="margin-left:35%;">
                        <div class="radio"><input type="radio" name="airportOrCity" value="City" checked="checked"></div>City
                    </label>
                </div>
                <div class="clearfix">
                    <div class="form-group">
                        <label class="control-label col-md-12">Pickup Location</label>
                        <div class="col-md-12">
                            <input type="text" name="pickupLocationName" id="pickupLocationName" class="form-control" placeholder="Pickup Location" onfocus="this.placeholder = ''">
                            <input type='hidden' name='pickupLocationId' id='pickupLocationId'>
                        </div>
                    </div>
                </div>
                <div class="clearfix">
                    <div class="form-group">
                        <label class="control-label col-md-12">Drop-off Location</label>
                        <div class="col-md-12">
                            <input type="text" name="dropOfLocationName" id="dropOfLocationName" class="form-control" placeholder="Drop-off Location" onfocus="this.placeholder = ''">
                            <input type='hidden' name='dropOfLocationId' id=''>
                        </div>
                    </div>
                </div>

                <div class="clearfix">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-12">Pick-up Date</label>
                            <input type="text" name="pickup_date" placeholder="Date" class="form-control icon-pick pick_up_date" id="pick_up_date" >
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-12">Pick-up Time</label>
                            <select class="select2me form-control input-xlarge" name="pickup_time" id="">
                            <option value="">Time</option>
                            <option value="0">0:00</option>
                            <option value="1">1:00</option>
                            <option value="2">2:00</option>
                            <option value="3">3:00</option>
                            <option value="4">4:00</option>
                            <option value="5">5:00</option>
                            <option value="6">6:00</option>
                            <option value="7">7:00</option>
                            <option value="8">8:00</option>
                            <option value="9">9:00</option>
                            <option value="10">10:00</option>
                            <option value="11">11:00</option>
                            <option value="12">12:00</option>
                            <option value="13">13:00</option>
                            <option value="14">14:00</option>
                            <option value="15">15:00</option>
                            <option value="16">16:00</option>
                            <option value="17">17:00</option>
                            <option value="18">18:00</option>
                            <option value="19">19:00</option>
                            <option value="20">20:00</option>
                            <option value="21">21:00</option>
                            <option value="22">22:00</option>
                            <option value="23">23:00</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="clearfix">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-12">Drop-off Date</label>
                            <input type="text" name="drop_off_date" placeholder="Date" class="drop_off_date form-control icon-pick" id="drop_off_date" >
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                        <label class="control-label col-md-12">Drop-off Time</label>
                        <select class="select2me form-control input-xlarge" name="drop_off_time" id="">
                        <option value="">Time</option>
                        <option value="0">0:00</option>
                        <option value="1">1:00</option>
                        <option value="2">2:00</option>
                        <option value="3">3:00</option>
                        <option value="4">4:00</option>
                        <option value="5">5:00</option>
                        <option value="6">6:00</option>
                        <option value="7">7:00</option>
                        <option value="8">8:00</option>
                        <option value="9">9:00</option>
                        <option value="10">10:00</option>
                        <option value="11">11:00</option>
                        <option value="12">12:00</option>
                        <option value="13">13:00</option>
                        <option value="14">14:00</option>
                        <option value="15">15:00</option>
                        <option value="16">16:00</option>
                        <option value="17">17:00</option>
                        <option value="18">18:00</option>
                        <option value="19">19:00</option>
                        <option value="20">20:00</option>
                        <option value="21">21:00</option>
                        <option value="22">22:00</option>
                        <option value="23">23:00</option>
                        </select>
                        </div>
                    </div>
                </div>

                <button type="submit" class="btn btn-info btn-sm" style="margin-left:200px; margin-top:20px;">Search Cars</button>

            </div>
            </form>
            <div class="panel panel-default cbg">
                <div class="panel-heading">
                    <h3 class="panel-title bold">Filter Results</h3>
                </div>
                <div class="panel-body">
                    <div id="carFilterContent">
                        <div id="fBycompany" class="clearfix" style="padding: 0px 0px;">
                            <div id="companyFilterContent">
                                <h4 class="tc1">Filter by company</h4>
                                <table class="table">
                                    <tbody>
                                    <?php
                                    $car_companies = array();
                                    foreach($car_list as $a_car_list)
                                    {
                                        $car_company_name = $a_car_list['carCompanyName'];

                                        if(isset($car_companies[$car_company_name]))
                                        {
                                            $car_companies[$car_company_name] += 1;
                                        }
                                        else
                                        {
                                            $car_companies[$car_company_name] = 1;
                                        }
                                    }

                                    foreach($car_companies as $a_car_company_name => $car_id)
                                    {
                                    echo <<<EOM
<tr>
                                            <td class="">
                                                <input class="filter_by_company company_checkboxes" id="" name="" type="checkbox" value="{$a_car_company_name}">
                                            </td>
                                            <td class="" style="width:70%;">
                                                {$a_car_company_name}
                                            </td>
                                            <td class="">
                                                <label for="" id="">{$car_id}</label>
                                            </td>
                                        </tr>
EOM;
                                    }

                                    ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div id="fByCarClass" class="clearfix" style="padding: 0px 0px; margin-top: 10px;">
                            <div id="carClass">
                                <h4 class="tc1">Filter by car class</h4>
                                <table class="table">
                                    <tbody>
                                    <?php
                                    $car_class = array();
                                    foreach($car_list as $a_car_list)
                                    {
                                        $class = $a_car_list['class'];

                                        if(isset($car_class[$class]))
                                        {
                                            $car_class[$class] += 1;
                                        }
                                        else
                                        {
                                            $car_class[$class] = 1;
                                        }
                                    }

                                    foreach($car_class as $class_name => $total_class) {
                                       echo <<<EOM
<tr>
                                            <td class="">
                                                <input class="car_class company_checkboxes" id="" name="" type="checkbox" value="{$class_name}">
                                            </td>

                                            <td class="" style="width:70%;">
                                                {$class_name}
                                            </td>
                                            <td class="">
                                                <label for="" id="">{$total_class}</label>
                                            </td>
                                        </tr>
EOM;
                                    }               ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="row sResult" id="content">
               
                <div class="panel panel-info" id="sort">
                    <div class="panel-heading clearfix">
                        <div class="panel-title pull-left">
                            <h4>Sort By: 
                            <button type="button" class="btn btn-info btn-sm sort_car_price company_checkboxes">Price</button>
                            <button type="button" class="btn btn-info btn-sm sort_car_class company_checkboxes">Class</button>
                            <button type="button" class="btn btn-info btn-sm sort_car_rental_company company_checkboxes">Rental Company</button>
                        </h4>
                        </div>
                        <div class="panel-option pull-right">
                            
                        </div>
                    </div>
                </div>
<?php
//var_dump($car_list);

echo  '<div id="car_search_details">';
                foreach($car_list as $a_car)
                {
                    $product_id_link = base_url() . 'view_car_details/ViewCarDetails/' . base64_encode($a_car['productId']) . '/' . $a_car['carCompanyId']. '/' . $a_car['class'] . '/' . $a_car['pick_up_station_id'];

                    echo
                    <<<EOM
                    <div class="panel panel-info" id="sResult">
                    <div class="panel-heading clearfix">
                        <div class="panel-title pull-left">
                            <h4>{$a_car['class']}: {$a_car['carName']}</h4>
                        </div>
                        <div class="panel-option pull-right">

                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="media">
                            <a class="pull-left" href="#">
                                <img class="media-object img-responsive thumbnail w170" src="{$a_car['carThumb']}" alt="image">
                            </a>
                            <div class="media-body">
                            <div class="col-md-7">
                                <div class="car-info">
                                    <img src="{$a_car['company_logo_url']}" class="img-responsive" alt="Image">
                                    <h5>
                                        <i class="fa fa-user fa-lg"></i>&nbsp;<span>{$a_car['maxPassengers']}</span><i class="fa fa-plus"></i>
                                        <i class="fa fa-briefcase  fa-lg"></i>&nbsp;<span>{$a_car['luggageLarge']}</span><i class="fa fa-plus"></i>
                                        <i class="fa fa-briefcase "></i>&nbsp;<span>{$a_car['luggageSmall']}</span>
                                    </h5>
                                    <h5>Automatic, A/C</h5>
                                    <h5>Status: {$a_car['status']}</h5>
                                </div>

                            </div>
                            <div class="col-md-5 mt5">
                                <div class="row">
                                    <table class="table">
                                        <tbody>
                                            <tr>
                                                <td style="border-top:none;">Daily Rate:</td>
                                                <td style="border-top:none;">9$</td>
                                            </tr>
                                            <tr>
                                                <td>Total Price</td>
                                                <td>37$</td>
                                            </tr>

                                        </tbody>
                                    </table>
                                    <button class="btn btn-danger btn-xs btn-block"><a href="{$product_id_link}">Car Details</a></button>
                                </div>

                            </div>


                            </div>
                        </div>
                    </div>
                    <div class="panel-lower-body">
                    </div>

                </div>

EOM;



                }

echo  '</div>';
?>


            </div>
        </div>
    </div>
    <center class="clearfix">
        <ul class = "pagination">
           <li><a href = "#">&laquo;</a></li>
           <li><a href = "#">1</a></li>
           <li><a href = "#">2</a></li>
           <li><a href = "#">3</a></li>
           <li><a href = "#">4</a></li>
           <li><a href = "#">5</a></li>
           <li><a href = "#">&raquo;</a></li>
        </ul>
    </center>  
</div>

<script src="<?php echo base_url(); ?>public/assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>public/affilliate_theme/plugins/checkincheckout/bootstrap-datepicker.js" type="text/javascript"></script>

<script>

    var car_search_results = <?php echo json_encode($car_list);?>;

    $('.sort_car_price').click(function(){

            car_search_results.sort(function(x,y)

                {
                    return x['minAvgPrice'] < y['minAvgPrice'];
                }

            );

        }
    );

    $('.sort_car_class').click(function(){

            //console.log(car_search_results);
            car_search_results.sort(function(x,y)

                {
                    return x['class'].localeCompare(y['class']);
                }

            );
            /*console.log(car_search_results);*/
        }
    );

    $('.sort_car_rental_company').click(function(){

            car_search_results.sort(function(x,y)

                {
                    return x['carCompanyName'].localeCompare(y['carCompanyName']);
                }

            );

        }
    );

    $('.company_checkboxes').click(function(){

            var company_filter = [];
            var car_class_filter = [];

            $(".filter_by_company:checked").each(function(){
                //alert($(this).attr("value"));

                company_filter.push($(this).attr("value"));
            });

            $(".car_class:checked").each(function(){
                //alert($(this).attr("value"));

                car_class_filter.push($(this).attr("value"));
            });


            //console.log("car search results :");
            //console.log(car_search_results);
            var car_details = "";
            for(i=0; i<car_search_results.length; i++)
            {
                search_obj = car_search_results[i];

                product_id_link = '<?php echo base_url() ?>' + 'view_car_details/ViewCarDetails/' +  search_obj["productId_64"] + '/' + search_obj['carCompanyId'] + '/' + search_obj['class'] + '/' + search_obj['pick_up_station_id'];

                var company_found = 0;
                var car_class_found = 0;

                for(j=0; j<company_filter.length; j++) {
                    if (company_filter[j] == search_obj['carCompanyName']) {
                        company_found = 1;
                    }
                }

                for(j=0; j<car_class_filter.length; j++)
                {
                    //debugger;
                    if(car_class_filter[j] == search_obj['class'])
                    {
                        car_class_found = 1;
                    }
                }

                if(0 == company_filter.length)
                {
                    company_found = 1;
                }

                if(0 == car_class_filter.length)
                {
                    car_class_found = 1;
                }

                if((company_found == 1) && (1 == car_class_found))
                {
                    var transmission;
                    var ac = "";

                    if("Automatic Transmission" == search_obj['transmission'])
                    {
                        transmission = "Automatic";
                    }
                    else
                    {
                        transmission = "Manual";
                    }
                    if(true == search_obj['ac'])
                    {
                        ac = "A/C"
                    }

                    car_details += '<div class="panel panel-info" id="sResult">' +
                    '<div class="panel-heading clearfix">' +
                    '<div class="panel-title pull-left">' +
                    '<h4>' + search_obj['class'] + ':' +
                     search_obj['carCompanyName'] + '</h4>' +
                    '</div>' +
                    '<div class="panel-option pull-right">' +
                    '</div>' +
                    '</div>' +
                    '<div class="panel-body">' +
                    '<div class="media">' +
                    '<a class="pull-left" href="#">'  +
                    '<img class="media-object img-responsive thumbnail w170" src="'+ search_obj['carThumb'] + '" alt="image">' +
                    '</a>' +
                    '<div class="media-body">'  +
                    '<div class="col-md-7">'  +
                    '<div class="car-info">'  +
                    '<img src="' + search_obj['company_logo_url'] + '" class="img-responsive" alt="Image">'  +
                    '<h5>'  +
                    '<i class="fa fa-user fa-lg"></i>&nbsp;<span>' + search_obj['maxPassengers']  + '</span><i class="fa fa-plus"></i>'  +
                    '<i class="fa fa-briefcase  fa-lg"></i>&nbsp;<span>'+ search_obj['luggageLarge'] + '</span><i class="fa fa-plus"></i>'  +
                    '<i class="fa fa-briefcase "></i>&nbsp;<span>'+ search_obj['luggageSmall']  +'</span>'  +
                    '</h5>'  +
                    '<h5>' + transmission + ', ' + ac + '</h5>'  +
                    '<h5>Status: ' + search_obj['status'] + '</h5>'  +
                    '</div>'  +
                    '</div>'  +
                    '<div class="col-md-5 mt5">'  +
                    '<div class="row">'  +
                    '<table class="table">'  +
                    '<tbody>' +
                    '<tr>'  +
                    '<td style="border-top:none;">Daily Rate:</td>'  +
                    '<td style="border-top:none;">9$</td>'  +
                    '</tr>'  +
                    '<tr>'   +
                    '<td>Total Price</td>'   +
                    '<td>37$</td>'   +
                    '</tr>'  +
                    '</tbody>'  +
                    '</table>'  +
                    '<button class="btn btn-danger btn-xs btn-block"><a href ="' + product_id_link + '"> Car Details ' + '</a>' + '</button>'  +
                    '</div>'+
                    '</div>'  +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '<div class="panel-lower-body">' +
                    '</div>' +
                    '</div>';
                }

            }

            $('#car_search_details').html(car_details);
        }
    );


    var checkin = $('.drop_off_date').datepicker({
        onRender: function (date) {
            now = new Date();
            //   console.log(date);
            if (date.valueOf() < now.valueOf())
            {
                return 'disabled';
            }
            else if (date.valueOf() == now.valueOf())
            {
                return 'pluseone';
            }
            else
            {
                return '';
            }
            //return date.valueOf() < now.valueOf() ? 'disabled' : '';

        }
    }).on('changeDate', function (ev) {

        /* var newDate = new Date(ev.date);

         newDate.setDate(newDate.getDate() + 1);
         $('#check_out').val("")
         checkout.setValue(newDate);

         checkin.hide();
         var check_in = $('#check_in').val();
         var check_out = $('#check_out').val();
         check_in = moment($('#check_in').val());
         check_out = moment($('#check_out').val());

         var diff = check_out.diff(check_in, 'days');
         $('.nights').text(diff);
         $('.nights').val(diff);

         $('#check_out')[0].focus();*/


    }).data('datepicker');

    var checkin = $('.pick_up_date').datepicker({
        onRender: function (date) {
            now = new Date();
            //   console.log(date);
            if (date.valueOf() < now.valueOf())
            {
                return 'disabled';
            }
            else if (date.valueOf() == now.valueOf())
            {
                return 'pluseone';
            }
            else
            {
                return '';
            }
            //return date.valueOf() < now.valueOf() ? 'disabled' : '';

        }
    }).on('changeDate', function (ev) {

        /* var newDate = new Date(ev.date);

         newDate.setDate(newDate.getDate() + 1);
         $('#check_out').val("")
         checkout.setValue(newDate);

         checkin.hide();
         var check_in = $('#check_in').val();
         var check_out = $('#check_out').val();
         check_in = moment($('#check_in').val());
         check_out = moment($('#check_out').val());

         var diff = check_out.diff(check_in, 'days');
         $('.nights').text(diff);
         $('.nights').val(diff);

         $('#check_out')[0].focus();*/


    }).data('datepicker');

</script>