<?php
$uniqueS = $uniqueSession;
foreach ($hotel_list as $i => $singlehotel) {
    $temp = $singlehotel['room_type_list'][0]['occupancy_list'];
    $my_price = 0;
    foreach ($temp as $t) {
        $my_price = $my_price + $t['available_list'][0]['total_price_inc_tax'];
    }
    $room_type_list = $singlehotel['room_type_list'];
    $room_type_rows_html = "";
    $global_min = 2000002;
    $count = 1;
    $temp_prices = array();
    foreach ($room_type_list as $room_type_index => $sin_room_type_list) {
        //echo $room_type_index;
        //pre($sin_room_type_list);
        $temp_count = count($sin_room_type_list['occupancy_list']);
        $temp_sum = 0;
        if (count($sin_room_type_list['occupancy_list']) == $roomNo) {
            foreach ($sin_room_type_list['occupancy_list'] as $temp_key => $temp_val) {
                $temp_sum = $temp_sum + $sin_room_type_list['occupancy_list'][$temp_key]['available_list'][0]['total_price_inc_tax'];
            }
        } else {
            $tc = 0;
            foreach ($sin_room_type_list['occupancy_list'] as $temp_key => $temp_val) {
                $temp_sum = $temp_sum + $sin_room_type_list['occupancy_list'][$temp_key]['available_list'][0]['total_price_inc_tax'];
                $tc++;
            }
            //                echo $tc;
            //                echo '<br>';
            $temp_sum = $temp_sum / $tc * $roomNo;
        }
        array_push($temp_prices, $temp_sum);
        //echo '<br>';
    }
    $price_index = 0;
    foreach ($room_type_list as $room_type_index => $sin_room_type_list) {
        $lowest_price = number_format($temp_prices[0], 2);
        $discount = $sin_room_type_list['discount'];
        $room_type_index_for_lowest_price = 0;
        $occupancy_lists = $sin_room_type_list['occupancy_list'];
        ?>
        <script type="text/javascript">
            dynamic_occupancy_lists['<?php echo $singlehotel['hotelId'].$i.$room_type_index; ?>'] = <?php echo json_encode($occupancy_lists); ?>;
        </script>
        <?php
        $temp_RoomId = $sin_room_type_list['RoomId'];
        $temp_RoomTypeId = $sin_room_type_list['RoomTypeId'];
        $hasfree = false;
        $hasfreebreakfast = "";
        $freesup = "";
        foreach ($sin_room_type_list['supp'] as $single_supp) {
            if ($single_supp['price'] == "0.00") {
                $freesup .= "<tr class='discount_tr collapse demo_".$singlehotel['hotelId']."'><td colspan='5'><img src='https://www.concretereading.co.uk/images/tick.svg' width='20' height='20' />Free " . $single_supp['suppName'] . "</td></tr>";
            }
        }

        $min_price = 2000000;
        $addedboardbases = array();
        foreach ($occupancy_lists as $snigle_occupancy_lists) {
            $available_lists = $snigle_occupancy_lists['available_list'];
            foreach ($available_lists as $single_available_lists) {
                foreach ($single_available_lists['BoardBases'] as $single_BoardBases) {
                    if (
                        $single_BoardBases['bbPublishPrice'] == 0 &&
                        !in_array($single_BoardBases['bbId'], $addedboardbases)
                    ) {
                        array_push($addedboardbases, $single_BoardBases['bbId']);
                        $hasfreebreakfast .= "<tr class='discount_tr collapse demo_".$singlehotel['hotelId']."'><td colspan='5'><img src='https://www.concretereading.co.uk/images/tick.svg' width='20' height='20' />Free " . $single_BoardBases['bbName'] . "</td></tr>";
                        $hasfree = true;
                    }
                }
                if ($min_price > $sin_room_type_list['occupancy_list']['Room 1']['available_list'][0]['total_price_inc_tax']) {
                    $min_price = number_format($my_price, 2);
                    $room_type_index_for_lowest_price = $room_type_index;
                }
                $min_price = number_format($temp_prices[$price_index], 2);
            }
        }
        $price_index++;
        if ($global_min > $min_price) {
            $global_min = $min_price;
            $lowest_roomId = $temp_RoomId;
            $lowest_roomTypeId = $temp_RoomTypeId;
            $modal_book_now_url = base_url() . 'request_quote?hotelId=' . $singlehotel['hotelId'] . '&roomId=' . $lowest_roomId . '&roomTypeId=' . $lowest_roomTypeId . '&stp=1';
        }
        $room_type_availability = ($sin_room_type_list['isAvailable'] == "true") ? "Available" : "On Request";
        $roomId = $sin_room_type_list['RoomId'];
        $roomTypeId = $sin_room_type_list['RoomTypeId'];
        $HotelType = $sin_room_type_list['HotelType'];
        $hotel_supp_str = '';
        foreach ($sin_room_type_list['supp'] as $hotelSupIndex => $singl_supp) {
            $hotel_supp_str .= '<input type="hidden" name="sup[' . $hotelSupIndex . '][suppId]" value="' . $singl_supp['suppId'] . '">' .
                '<input type="hidden" name="sup[' . $hotelSupIndex . '][suppName]" value="' . $singl_supp['suppName'] . '">' .
                '<input type="hidden" name="sup[' . $hotelSupIndex . '][supptType]" value="' . $singl_supp['supptType'] . '">' .
                '<input type="hidden" name="sup[' . $hotelSupIndex . '][suppIsMandatory]" value="' . $singl_supp['suppIsMandatory'] . '">' .
                '<input type="hidden" name="sup[' . $hotelSupIndex . '][suppChargeType]" value="' . $singl_supp['suppChargeType'] . '">' .
                '<input type="hidden" name="sup[' . $hotelSupIndex . '][price]" value="' . $singl_supp['price'] . '">' .
                '<input type="hidden" name="sup[' . $hotelSupIndex . '][publishPrice]" value="' . $singl_supp['publishPrice'] . '">';
        }
        $addrestext = '';
        if (isset($room_type_list['location']['@attributes'])) {
            $addrestext = '<input type="hidden" value="' . $room_type_list['location']['@attributes']['address'] . '" name="address"/>' .
                '<input type="hidden" value="' . $room_type_list['location']['@attributes']['city'] . '" name="city"/>' .
                '<input type="hidden" value="' . $room_type_list['location']['@attributes']['country'] . '" name="country"/>' .
                '<input type="hidden" value="' . $room_type_list['location']['@attributes']['destinationCode'] . '" name="destinationCode"/>' .
                '<input type="hidden" value="' . $room_type_list['location']['@attributes']['destinationId'] . '" name="destinationId"/>' .
                '<input type="hidden" value="' . $room_type_list['location']['@attributes']['latitude'] . '" name="latitude"/>' .
                '<input type="hidden" value="' . $room_type_list['location']['@attributes']['longitude'] . '" name="longitude"/>' .
                '<input type="hidden" value="' . $room_type_list['location']['@attributes']['searchingCity'] . '" name="searchingCity"/>' .
                '<input type="hidden" value="' . $room_type_list['location']['@attributes']['state'] . '" name="state"/>' .
                '<input type="hidden" value="' . $room_type_list['location']['@attributes']['zip'] . '" name="zip"/>';
        }
        $discountstr = "";
        if ($hasfree) {
            $discountstr .= $hasfreebreakfast;
        }
        if ($freesup) {
            $discountstr .= $freesup;
        }
        //echo '<pre>';print_r($discount);echo '</pre>';
        if (count($discount) > 0) {
            if ($discount['discType'] == "ProgressivePromotion") {
                if ($discount['name'] != "") {
                    $discountstr .= "<tr class='discount_tr collapse demo_".$singlehotel['hotelId']."'><td colspan='5'><span class='speciel_deal_discount'>" . $discount['name'] . "</span> Get " . $discount['value'] . "% Discount on booking from " . $discount['from'] . " to " . $discount['to'] . "</td></tr>";
                } else {
                    $discountstr .= "<tr class='discount_tr collapse demo_".$singlehotel['hotelId']."'><td colspan='5'>Get " . $discount['value'] . "% Discount on booking from " . $discount['from'] . " to " . $discount['to'] . "</td></tr>";
                }
            } else if ($discount['discType'] == "TL_DISCOUNT") {
                //$discountstr .= "<tr class='discount_tr collapse demo_".$singlehotel['hotelId']."'><td colspan='5'><span class='speciel_deal_discount'>Discount</span>$".$discount['amount']." will be applied on total amount</td></tr>";
            } else {
                $discountstr .= "<tr class='discount_tr collapse demo_".$singlehotel['hotelId']."'><td colspan='5'>Discount from " . $discount['from'] . " to " . $discount['to'] . " pay for " . $discount['pay'] . " nights stay for " . $discount['stay'] . " nights</td></tr>";
            }
        }

        if (count($room_type_list) > 1) {
            if($room_type_index == 0){
                $room_type_rows_html .= '<tr>' .
                    '<td>' . $sin_room_type_list['name'] . '</td>' .
                    '<td>' . $room_type_availability . '</td>' .
                    //'<td class="popover-link"><a onclick="rateAndPolicyFunc(this);"  facilities="'.$singlehotel['facilities'].'" occupancy_list="'.htmlentities(json_encode($occupancy_lists), ENT_QUOTES, 'UTF-8').'" hotelId="'.$singlehotel['hotelId'].'" RoomTypeId="'.$roomTypeId.'" first_index="' . $i . '" second_index="' . $room_type_index . '" href="javascript:void(0)">Rates & Policies</a></td>'.
                    '<td class="popover-link"><a onclick="rateAndPolicyFunc(this);" minR="' . $min_price . '" facilities="' . $singlehotel['facilities'] . '" occupancy_list="' . htmlentities(json_encode($occupancy_lists), ENT_QUOTES, 'UTF-8') . '" hotelId="' . $singlehotel['hotelId'] . '" RoomTypeId="' . $roomTypeId . '" HotelType="' . $HotelType . '" first_index="' . $i . '" second_index="' . $room_type_index . '" href="javascript:void(0)">Rates & Policies</a></td>' .
                    '<td>$' . $min_price . '</td><td><form target="_blank" action="' . base_url() . 'request_quote?stp=f" name="' . $room_type_index . '" id="hotel' . $singlehotel['hotelId'] . $room_type_index . '" method="post">' .
                    '<input type="hidden" value="' . $sin_room_type_list['name'] . '" name="room_type_category"/>' .
                    '<input type="hidden" value="' . $singlehotel['hotelId'] . '" name="hotelId"/>' .
                    '<input type="hidden" value="' . $HotelType . '" name="hotelType"/>' .
                    '<input type="hidden" value="' . $min_price . '" name="minPrice"/>' .
                    '<input type="hidden" value="' . $uniqueS . '" name="uniqueSession"/>' .
                    '<input type="hidden" value="' . $singlehotel['voucher_remarks'] . '" name="voucher_remarks"/>' .
                    '<input type="hidden" value="' . $locationName . '" name="locationName"/>' .
                    '<input type="hidden" value="' . $check_in . '" name="from"/>' .
                    '<input type="hidden" value="' . $check_out . '" name="top"/>' .
                    '<input type="hidden" value="' . $locationId . '" name="locationId"/>' .
                    '<input type="hidden" value="' . $numberOfAdults . '" name="adultsNo"/>' .
                    '<input type="hidden" value="' . $numberOfChild . '" name="childrenNo"/>' .
                    '<input type="hidden" value="' . $singlehotel['hotelName'] . '" name="hotelName"/>' .
                    '<input type="hidden" value="' . $singlehotel['star_level'] . '" name="starRating"/>' .
                    '<input type="hidden" value="' . $roomTypeId . '" name="roomTypeId"/>' .
                    '<input type="hidden" value="' . $room_type_availability . '" name="avalibilty"/>' . $addrestext .
                    '<input type="hidden" value="' . $roomId . '" name="roomId"/>' . $hotel_supp_str .
                    '<input class="btn btn-danger btn-xs" name="" type="submit" value="Book now!"></form></td>' .
                    '</tr>' . $discountstr;
            }else{
                $room_type_rows_html .= '<tr class="collapse demo_'.$singlehotel['hotelId'].'">' .
                    '<td>' . $sin_room_type_list['name'] . '</td>' .
                    '<td>' . $room_type_availability . '</td>' .
                    //'<td class="popover-link"><a onclick="rateAndPolicyFunc(this);"  facilities="'.$singlehotel['facilities'].'" occupancy_list="'.htmlentities(json_encode($occupancy_lists), ENT_QUOTES, 'UTF-8').'" hotelId="'.$singlehotel['hotelId'].'" RoomTypeId="'.$roomTypeId.'" first_index="' . $i . '" second_index="' . $room_type_index . '" href="javascript:void(0)">Rates & Policies</a></td>'.
                    '<td class="popover-link"><a onclick="rateAndPolicyFunc(this);" minR="' . $min_price . '" facilities="' . $singlehotel['facilities'] . '" occupancy_list="' . htmlentities(json_encode($occupancy_lists), ENT_QUOTES, 'UTF-8') . '" hotelId="' . $singlehotel['hotelId'] . '" RoomTypeId="' . $roomTypeId . '" HotelType="' . $HotelType . '" first_index="' . $i . '" second_index="' . $room_type_index . '" href="javascript:void(0)">Rates & Policies</a></td>' .
                    '<td>$' . $min_price . '</td><td><form target="_blank" action="' . base_url() . 'request_quote?stp=f" name="' . $room_type_index . '" id="hotel' . $singlehotel['hotelId'] . $room_type_index . '" method="post">' .
                    '<input type="hidden" value="' . $sin_room_type_list['name'] . '" name="room_type_category"/>' .
                    '<input type="hidden" value="' . $singlehotel['hotelId'] . '" name="hotelId"/>' .
                    '<input type="hidden" value="' . $HotelType . '" name="hotelType"/>' .
                    '<input type="hidden" value="' . $min_price . '" name="minPrice"/>' .
                    '<input type="hidden" value="' . $uniqueS . '" name="uniqueSession"/>' .
                    '<input type="hidden" value="' . $singlehotel['voucher_remarks'] . '" name="voucher_remarks"/>' .
                    '<input type="hidden" value="' . $locationName . '" name="locationName"/>' .
                    '<input type="hidden" value="' . $check_in . '" name="from"/>' .
                    '<input type="hidden" value="' . $check_out . '" name="top"/>' .
                    '<input type="hidden" value="' . $locationId . '" name="locationId"/>' .
                    '<input type="hidden" value="' . $numberOfAdults . '" name="adultsNo"/>' .
                    '<input type="hidden" value="' . $numberOfChild . '" name="childrenNo"/>' .
                    '<input type="hidden" value="' . $singlehotel['hotelName'] . '" name="hotelName"/>' .
                    '<input type="hidden" value="' . $singlehotel['star_level'] . '" name="starRating"/>' .
                    '<input type="hidden" value="' . $roomTypeId . '" name="roomTypeId"/>' .
                    '<input type="hidden" value="' . $room_type_availability . '" name="avalibilty"/>' . $addrestext .
                    '<input type="hidden" value="' . $roomId . '" name="roomId"/>' . $hotel_supp_str .
                    '<input class="btn btn-danger btn-xs" name="" type="submit" value="Book now!"></form></td>' .
                    '</tr>' . $discountstr;
            }
        }else {
            $room_type_rows_html .= '<tr>' .
                '<td>' . $sin_room_type_list['name'] . '</td>' .
                '<td>' . $room_type_availability . '</td>' .
                //'<td class="popover-link"><a onclick="rateAndPolicyFunc(this);"  facilities="'.$singlehotel['facilities'].'" occupancy_list="'.htmlentities(json_encode($occupancy_lists), ENT_QUOTES, 'UTF-8').'" hotelId="'.$singlehotel['hotelId'].'" RoomTypeId="'.$roomTypeId.'" first_index="' . $i . '" second_index="' . $room_type_index . '" href="javascript:void(0)">Rates & Policies</a></td>'.
                '<td class="popover-link"><a onclick="rateAndPolicyFunc(this);" minR="' . $min_price . '" facilities="' . $singlehotel['facilities'] . '" occupancy_list="' . htmlentities(json_encode($occupancy_lists), ENT_QUOTES, 'UTF-8') . '" hotelId="' . $singlehotel['hotelId'] . '" RoomTypeId="' . $roomTypeId . '" HotelType="' . $HotelType . '" first_index="' . $i . '" second_index="' . $room_type_index . '" href="javascript:void(0)">Rates & Policies</a></td>' .
                '<td>$' . $min_price . '</td><td><form target="_blank" action="' . base_url() . 'request_quote?stp=f" name="' . $room_type_index . '" id="hotel' . $singlehotel['hotelId'] . $room_type_index . '" method="post">' .
                '<input type="hidden" value="' . $sin_room_type_list['name'] . '" name="room_type_category"/>' .
                '<input type="hidden" value="' . $singlehotel['hotelId'] . '" name="hotelId"/>' .
                '<input type="hidden" value="' . $HotelType . '" name="hotelType"/>' .
                '<input type="hidden" value="' . $min_price . '" name="minPrice"/>' .
                '<input type="hidden" value="' . $uniqueS . '" name="uniqueSession"/>' .
                '<input type="hidden" value="' . $singlehotel['voucher_remarks'] . '" name="voucher_remarks"/>' .
                '<input type="hidden" value="' . $locationName . '" name="locationName"/>' .
                '<input type="hidden" value="' . $check_in . '" name="from"/>' .
                '<input type="hidden" value="' . $check_out . '" name="top"/>' .
                '<input type="hidden" value="' . $locationId . '" name="locationId"/>' .
                '<input type="hidden" value="' . $numberOfAdults . '" name="adultsNo"/>' .
                '<input type="hidden" value="' . $numberOfChild . '" name="childrenNo"/>' .
                '<input type="hidden" value="' . $singlehotel['hotelName'] . '" name="hotelName"/>' .
                '<input type="hidden" value="' . $singlehotel['star_level'] . '" name="starRating"/>' .
                '<input type="hidden" value="' . $roomTypeId . '" name="roomTypeId"/>' .
                '<input type="hidden" value="' . $room_type_availability . '" name="avalibilty"/>' . $addrestext .
                '<input type="hidden" value="' . $roomId . '" name="roomId"/>' . $hotel_supp_str .
                '<input class="btn btn-danger btn-xs" name="" type="submit" value="Book now!"></form></td>' .
                '</tr>' . $discountstr;
        }
    }

    if (count($room_type_list) > 1) {
        $room_type_rows_html .= '<tr class="collapseTrigger" style="cursor:pointer;text-align: center;" data-toggle="collapse" data-target=".demo_' . $singlehotel['hotelId'] . '"><td colspan="5"><b>Click here to view more rooms</b></td></tr>';
    }

    $lowest_roomId = 10;
    $lowest_roomTypeId = 10;
    $modal_book_now_url = base_url() . 'request_quote?hotelId=' . $singlehotel['hotelId'] . '&roomId=' . $lowest_roomId . '&roomTypeId=' . $lowest_roomTypeId . '&stp=1';
    ?>
    <div class="panel panel-info" id="sResult">
        <div class="panel-heading clearfix">
            <div class="panel-title pull-left"><h4><?php echo $singlehotel['hotelName']; ?></h4></div>
            <div class="panel-option pull-right">
                <input type="text" name="rating" id="inputRating" class="srRating hidden"
                       value="<?php echo $singlehotel['star_level']; ?>">
            </div>
        </div>
        <div class="panel-body">
            <div class="media">
                <a tab_name="picture" href="javascript:void(0)" class="pull-left hotel_search_details"
                   hotel-type="<?php echo $singlehotel['hotelType']; ?>" hotel_id="<?php echo $singlehotel['hotelId']; ?>"
                   modal_book_now_button="<?php echo $modal_book_now_url; ?>"
                   min_price="<?php echo $lowest_price; ?>">
                    <img style="width: 100px;height: 100px;" id="hotelthumb<?php echo $singlehotel['hotelId']; ?>"
                         hotel_location_zip="<?php echo $singlehotel['hotel_location_zip']; ?>"
                         hotel_location_city="<?php echo $singlehotel['hotel_location_city']; ?>"
                         hotel_location_country="<?php echo $singlehotel['hotel_location_country']; ?>"
                         hotel_location_state="<?php echo $singlehotel['hotel_location_state']; ?>"
                         hotel_location_name="<?php echo $singlehotel['hotel_location_name']; ?>"
                         longitude="<?php echo $singlehotel['longitude']; ?>"
                         latitude="<?php echo $singlehotel['latitude']; ?>"
                         hotelName="<?php echo $singlehotel['hotelName']; ?>"
                         class="media-object img-responsive thumbnail hotel_search_details"
                         src="<?php echo $singlehotel['hotelThumbUrl']; ?>" alt="image">
                </a>

                <div class="media-body">
                    <div class="descrip">
                        <ul class="list-unstyled list-inline">
                            <li><a tab_name="information" href="javascript:void(0)"
                                   hotel-type="<?php echo $singlehotel['hotelType']; ?>"
                                   form-id="hotel<?php echo $singlehotel['hotelId']; ?>" class="hotel_search_details"
                                   hotel_id="<?php echo $singlehotel['hotelId']; ?>"
                                   modal_book_now_button="<?php echo $modal_book_now_url; ?>"
                                   min_price="<?php echo $lowest_price; ?>"><span
                                        class="glyphicon glyphicon-info-sign hotel_search_details"></span>Information</a>
                            </li>
                            <li><a tab_name="amenities" class="hotel_search_details"
                                   hotel-type="<?php echo $singlehotel['hotelType']; ?>"
                                   form-id="hotel<?php echo $singlehotel['hotelId']; ?>" href="javascript:void(0)"
                                   hotel_id="<?php echo $singlehotel['hotelId']; ?>"
                                   modal_book_now_button="<?php echo $modal_book_now_url; ?>"
                                   min_price="<?php echo $lowest_price; ?>"><span
                                        class="fa fa-tasks"></span>Amenities</a></li>
                            <li><a tab_name="map" href="javascript:void(0)" class="hotel_search_details"
                                   hotel-type="<?php echo $singlehotel['hotelType']; ?>"
                                   form-id="hotel<?php echo $singlehotel['hotelId']; ?>"
                                   hotel_id="<?php echo $singlehotel['hotelId']; ?>"
                                   modal_book_now_button="<?php echo $modal_book_now_url; ?>"
                                   min_price="<?php echo $lowest_price; ?>"><span
                                        class="glyphicon glyphicon-map-marker hotel_search_details"></span>Map</a></li>
                            <li><a tab_name="picture" href="javascript:void(0)" class="hotel_search_details"
                                   hotel-type="<?php echo $singlehotel['hotelType']; ?>"
                                   form-id="hotel<?php echo $singlehotel['hotelId']; ?>"
                                   hotel_id="<?php echo $singlehotel['hotelId']; ?>"
                                   modal_book_now_button="<?php echo $modal_book_now_url; ?>"
                                   min_price="<?php echo $lowest_price; ?>"><span
                                        class="glyphicon glyphicon-picture hotel_search_details"></span>Pictures</a>
                            </li>
                        </ul>
                    </div>
                    <div class="media-price"><h4 style="text-align: center; margin:5px 0px; color: #EC2A26;">TOTAL</h4>
                        <h4>$<?php echo $lowest_price; ?></h4></div>
                </div>
            </div>
        </div>
        <div class="panel-lower-body"></div>
        <div class="panel-footer">
            <div class="table-responsive">
                <table class="table">
                    <thead style="background: transparent;">
                    <tr>
                        <th>Room Type</th>
                        <th>Status</th>
                        <th>Policy</th>
                        <th>Total</th>
                        <th>&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php echo $room_type_rows_html; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
<?php
}
?>

<!--<script type="text/javascript">

    $(document).ready(function(){
        $(document).on('shown.bs.collapse','.collapse', function () {
            alert(1);
            $(this).parent().children().last().find('td').html("<b>Click here to hide rooms</b>");
        });

        $(document).on('hidden.bs.collapse','.collapse', function () {
            alert(2)
            $(this).parent().children().last().find('td').html("<b>Click here to view more rooms</b>");
        });
    });
</script>-->