<style>
            h2,h1,h3,h4,h5 {
                margin: 0;
                padding: 0;
            }
            .bg-furture{
                background: rgba(255, 255, 255, 0.74);;
                height: auto;
                width:100%;
                padding: 15px 50px;
                margin: 0 0 30px;
            }
            .heading-text h1 {
                color: #fff;
                font-weight: 600;
                text-transform: capitalize;
                 margin: 30px 0 0;
            }
            .heading-text {
                display: block;
                margin: 0 0 60px;
                overflow: hidden;
                text-align: center;
            }
            .bg-furture h2 {
                border-bottom: 1px solid #999;
                color: #3d7db6;
                display: block;
                font-size: 20px;
                padding: 0 0 5px;
                margin: 0 0 10px;
                text-transform: uppercase;
            }
            table {
              margin: 20px auto;
              width: 100%;  
              padding: 10px;
              display: block;
             background-color: #fff;
            }
            .table-header th {
                border: 1px solid #efefef !important;
                font-size: 12px;
                padding: 13px 4px;
                text-align: center;
                width: 6%;
            }
            .table-header th:first-child {
                width: 30%;
            }
            .table-body td {
                border: 1px solid #efefef !important;
                font-size: 12px;
                font-weight: 600;
                padding: 7px 5px;
                text-transform: capitalize;
            }
            .table-body td img {
              display: block;
              margin: 0 auto;
              max-width: 25px;
            }
            .table-body .color-red {
                color: red;
                text-transform: uppercase;
            }
            .table-body .discont-td{
                color:red;
                text-align: center;
            }
			.revenue-bar {
              margin: 10px 0 20px;
              width: 44.5%;  
              padding: 10px;
              display: block;
             background-color: #fff;
            }
            .revenue-bar th{
            border: 1px solid #efefef !important;
            padding:  5px;
            text-align: center;
            }
            .revenue-bar td{
             border: 1px solid #efefef !important;
            font-weight: 600;
            padding:5px;
            }
            .revenue-bar td:last-child{
               color: red;
               font-weight: 600;
            }
            .revenue-bar th:last-child{
               color: red;
               font-weight: 600;
            }
            .revenue-bar li span{
                display: block;
                font-weight: 600;
            }
            .revenue-bar li:last-child{
               color: red;
               font-weight: 600;
            }
            .font-color-red{
                color: red;
                font-weight: 600;
            }
        </style>
<div class="container ">
            <!-- <div class="heading-text">
                <h1>Incentives</h1>
            </div> -->
            <div class="bg-furture">
                <h2>Incentives:</h2>
                <div class="prcing-sec">
                    <p><strong><?php echo DOMAIN_NAME;?></strong> offers great incentives for its TOP revenue generators out there. Besides saving money for your company and its employees -<i>whether it is for business or leisure</i> - we want to make sure that your loyalty doesn’t go unnoticed or taken for granted. Consider this our way of saying:  <strong>Thank you for sticking with us!</strong> </p>
                    <p>Every calendar year we assess your company’s revenue for that calendar year (we calculate revenue in a very simple way: Dollar amount your company and its employees spent combined MINUS any refunds in one calendar year) and should your company and employees combined qualify based on  <strong>revenue</strong> in a single calendar year, please use the Contact Us menu and request reduced rates due to your volume purchases.</p>
                    <p>Once your account is approved for discount tier pricing, you will be able to take advantage of the tier discount for the next calendar year. The discount rate does not apply to services that are not sold directly by <strong> <?php echo DOMAIN_NAME;?>, </strong> such as FLIGHTS, EVENTS and TRAVEL INSURANCE services. <strong><?php echo DOMAIN_NAME;?></strong> reserves the right to void this offer without any prior notice.</p>
                    <table class="table-responsive table-bordered">
                        <tr class="table-header">
                            <th>&nbsp;</th>
                            <th>&lt;$1M</th>
                            <th>$1M - $2,5M</th>
                            <th>$2,5M - $5M</th>
                            <th>$5M - $10M</th>
                            <th>&gt;$10M</th>
                        </tr> 
                       <!--  <tr class="table-body">
                            <td class="color-red">Free membership</td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                        </tr> -->
                        <tr class="table-body">
                            <td>Instant online invoicing</td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                        </tr> 
                        <tr class="table-body">
                            <td>3D secure payment processing</td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                        </tr>
                        <tr class="table-body">
                            <td>24/7/365 Toll-free phone support</td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                        </tr>
                        <tr class="table-body">
                            <td>24/7/365 Email customer support</td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                        </tr> 
                        <tr class="table-body">
                            <td>Encrypted site with SSL encrypted technology</td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                        </tr> 
                        <tr class="table-body">
                            <td>1% discount on our pricing by using bitCoin payments</td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                        </tr> 
                       <!--  <tr class="table-body">
                            <td>Custom-tailored Travel Professional markup panel</td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                        </tr>  -->
                        <!-- <tr class="table-body">
                            <td>Eligibility for our annual FREE luxury trip giveaway</td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                        </tr> --> 
                       <!--  <tr class="table-body">
                            <td>1% discount on our alternative payment platform for USA accounts</td>
                             <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                             <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                        </tr>  -->
                        <tr class="table-body">
                            <td>Monthly Skype consultation with the CEO of <?php echo DOMAIN_NAME;?></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/cancel.png" alt="logo"/></td>
                            <td><!-- 30 Minutes --> <img src="<?php echo base_url(); ?>public/assets/img/cancel.png" alt="logo"/></td>
                            <td class="text-center">30 Minutes</td>
                            <td class="text-center">30 Minutes</td>
                            <td class="text-center">60 Minutes</td>
                        </tr> 
                        <tr class="table-body">
                            <td>Additional tier discount for the following year</td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/cancel.png" alt="logo"/></td>
                            <td class="discont-td">1%</td>
                            <td class="discont-td">1.5%</td>
                            <td class="discont-td">2%</td>
                            <td class="discont-td">Contact us for special pricing</td>
                        </tr> 
                       <!--  <tr class="table-body">
                            <td>Dedicated customer service representative</td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/cancel.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/cancel.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/cancel.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                            <td><img src="<?php echo base_url(); ?>public/assets/img/rounded-check-mark.png" alt="logo"/></td>
                        </tr> -->
                    </table>
                    <!-- <p>
                    	Every calendar year we assess every travel professional’s produced revenue (we calculate revenue in a very simple way: Dollar amount your agency or agents spent combined MINUS any refunds in one calendar year). Should your account qualify based on <b>revenue</b> in a single calendar year, please use the Contact Us menu and request reduced rates due to your volume purchases.
                    </p> -->
                   <!--  <p>Want to know which position your company takes place? Please see your LIVE ranking below:</p>
                    <table class="table-responsive table-bordered">
                    	<tr class="table-header">
                        	<th>Company Rank</th>
                            <th>Amount Spent</th>
                            <th>Amount Refunded</th>
                            <th>REVENUE</th>
                        </tr>
                        <tr class="table-body">
                            <td><b>1</b></td>
                            <td>$120,000.00</td>
                            <td>$20,000.00</td>
                            <td>$100,000.00</td>
                        </tr>
                        <tr class="table-body">
                            <td><b>2</b></td>
                            <td>$100,000.00</td>
                            <td>$20,000.00</td>
                            <td>$80,000.00</td>
                        </tr>
                    </table> -->
                    <!-- <p>
                        Once your account is approved – after our manual review process – for discount tier pricing, it will not have to requalify the subsequent years and you will be able to take advantage of the <span style="color:red">tier discount for the <b>LIFETIME</b> of your account</span>. The discount rate does not apply to services that are not sold through <?php echo DOMAIN_NAME;?>, such as FLIGHTS, EVENTS and TRAVEL INSURANCE services. <b><?php echo DOMAIN_NAME;?></b> reserves the right to void this discount should we determine abuse of any kind.
                    </p> -->
                   <!--  <p>Want to know how much revenue you have generated so far? Take a look below:</p> -->
                    
                   <!--  <table class="revenue-bar">
                            <tbody><tr>
                                <th>Year</th>
                                <th>Amount spent</th> 
                                <th>Amount refunded </th> 
                                <th>REVENUE</th> 
                            </tr>
                            <tr>
                                <td>2017</td>
                                <td>$123,123.22</td>
                                <td>$23,123.22</td>
                                <td>$100,000.00</td>
                            </tr>
                    </tbody></table> -->
                    
                    
                </div>
            </div>  
        </div>