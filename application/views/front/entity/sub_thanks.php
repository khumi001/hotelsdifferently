<div class="width-row">
    <div class="main_cont">
        <div class="alert alert-success">
            <strong>CONGRATULATIONS!</strong> Thank you for your registration. We sent out an email to the email address you specified and once you confirm it by clicking on the link, you will be able to login and start using your account immediately.
        </div>
        <p>Thank you for choosing Wholesale Hotels Group and we look forward to doing business with you!</p>
    </div>
</div>
