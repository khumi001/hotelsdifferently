<style>
h2, h1, h3, h4, h5 {
	margin: 0;
	padding: 0;
}
.bg-furture {
	background: rgba(255, 255, 255, 0.74);
	;
	height: auto;
	width: 100%;
	padding: 15px 50px;
}
.heading-text h1 {
	color: #fff;
	font-weight: 600;
	text-transform: capitalize;
	margin: 30px 0 0;
}
.heading-text {
	display: block;
	margin: 0 0 60px;
	overflow: hidden;
	text-align: center;
}
.bg-furture h2 {
	border-bottom: 1px solid #999;
	color: #3d7db6;
	display: block;
	font-size: 20px;
	padding: 0 0 5px;
	margin: 0 0 10px;
	text-transform: uppercase;
}
.furture-form-style label {
	font-weight: normal;
	margin: 0 0 10px;
	text-transform: capitalize;
}
.form-style {
	display: block;
	margin: 0 0 10px;
	border-radius: 0px;
	font-size: 12px;
	text-align: center;
	padding: 0 2px;
}
.phone-lable {
	display: block;
	width: 100%;
}
.no-padding {
	display: block;
	padding: 0;
	margin: 0;
}
.col-md-4.no-padding {
	padding: 0 5px 0 0;
}
.col-md-3.no-padding {
	padding: 0 5px 0 0;
}
.phone-lable {
	display: block;
	font-weight: normal;
	margin: 0 0 10px;
	width: 100%;
}
.compny-information {
	margin: 70px 0 0;
	padding: 0 15px;
}
.submit-btn-furture {
	background: rgba(0, 0, 0, 0) linear-gradient(to bottom, #71d1ff 0%, #22b3f4 4%, #198fef 100%) repeat scroll 0 0;
	border: medium none;
	border-radius: 6px;
	box-shadow: 0 1px 3px rgba(0, 0, 0, 0.4);
	color: #fff;
	font-family: "FuturaStd-Book";
	font-size: 16px;
	padding: 10px 25px;
	text-shadow: 1px 1px 2px #000;
	text-transform: uppercase;
	margin: 30px 0 30px;
}
.bg-furture .fa-info-circle {
	cursor: pointer;
	vertical-align: super;
	font-size: 11px;
	color: #0d9de3;
}
.bg-furture .fa-info-circle:hover {
	color: #0b73a5;
}
.brder-radius-none {
	border-radius: 0px;
}
select {
	border-radius: 0 !important;
	font-size: 12px !important;
}
.form-wizard .active .step .number {
	background-color: #35aa47;
	color: #fff;
}
.form-wizard .step .number {
	background-color: #eee;
	border-radius: 50% !important;
	display: inline-block;
	font-size: 16px;
	font-weight: 300;
	height: 45px;
	margin-right: 10px;
	padding: 11px 15px 13px;
	text-align: center !important;
	width: 45px;
}
</style>
<style scoped>
		   .tooltip-inner {max-width: 600px;width: 600px;}
		   .pricacy-width{width: 100%; overflow: auto; height: 200px;}
		   a.tooltips {position: relative;display: inline;}
		   .username1 span{visibility: hidden;display: none;}
		   .email1 span{
		   visibility: hidden;
		   display: none;
		   }
		   .tooltips span {
		   position: absolute;
		   color: #FFFFFF;
		   background: #000000;
		   height: 30px;
		   line-height: 30px;
		   text-align: center;
		   visibility: hidden;
		   border-radius: 6px;
		   }.tooltips span::after {
		   border-left: 8px solid transparent;
		   border-right: 8px solid transparent;
		   border-top: 8px solid #000000;
		   content: "";
		   height: 0;
		   left: 50%;
		   margin-left: -8px;
		   position: absolute;
		   top: 86%;
		   width: 0;
		   }.username1.tooltips span {
		   bottom: 38px;
		   display: block;
		   left: 0;
		   margin-left: 16px;
		   opacity: 0.8;
		   visibility: visible;
		   z-index: 999;
		   width:155px;
		   }.email1.tooltips span {
		   bottom: 38px;
		   display: block;
		   left: 0;
		   margin-left: 16px;
		   opacity: 0.8;
		   visibility: visible;
		   z-index: 999;
		   width:255px;
		   }
		   #capatcha {
		   margin: 0 28%;
		   display: block
		   }
		</style>
<div class="container ">
  <?php /*?><div class="heading-text">
    <h1>My Detail</h1>
  </div><?php */?>
  <div class="bg-furture">
    <h2>Company Details:</h2>
    <div class="alert alert-danger display-hide" id="response_div" style="display:none">
      <button class="close" data-close="alert"></button>
      <span id="response_div_text"> </span> </div>
    <div class="tab-content">
      <div class="tab-pane active" id="tab1">
        <form method="post" id="travel_professional_signup">
          <div class="row">
            <div class="col-md-4 col-sm-4 ">
              <div class="form-group furture-form-style">
                <label>Company Name:</label>
                <input type="text" readonly value="<?=$user_info['company_name']?>" class="form-control form-style" name="company_name" >
              </div>
            </div>
            <div class="col-md-4 col-sm-4">
              <div class="form-group furture-form-style">
                <label>Company Website:</label>
                <input type="text" readonly class="form-control form-style" name="website" value="<?=$user_info['website']?>" >
              </div>
            </div>
            <div class="col-md-4 col-sm-4">
              <label class="phone-lable">Company phone : </label>
              <div class="col-md-3 no-padding">
                <div class="form-group furture-form-style">
                  <input type="text" class="form-control form-style" placeholder="Country" value="<?=$user_info['phone_country_code']?>" maxlength="4" name="phone_country" >
                </div>
              </div>
              <div class="col-md-4 no-padding">
                <div class="form-group furture-form-style">
                  <input type="text"  placeholder="Number" class="form-control form-style" name="phone_number" value="<?=$user_info['phone_number']?>" >
                </div>
              </div>
              <div class="col-md-5 no-padding">
                <div class="form-group furture-form-style">
                  <input placeholder="Extension" type="text" class="form-control form-style" name="phone_ex" value="<?=$user_info['phone_ex']?>" >
                </div>
              </div>
            </div>
            <div class="col-md-4 col-sm-4">
              <div class="form-group furture-form-style">
                <label>Country:</label>
                <select class="form-control input-medium select2me brder-radius-none" disabled>
                  <option><?=$user_info['country_name']?></option>
                </select>
              </div>
            </div>
            <div class="col-md-4 col-sm-4">
              <div class="form-group furture-form-style">
                <label>State:</label>
                <select class="form-control input-medium select2me brder-radius- " name="state" id="state">
                </select>
              </div>
            </div>
            <div class="col-md-4 col-sm-4 ">
              <div class="form-group furture-form-style">
                <label>City</label>
                <input type="text" class="form-control form-style" name="city" value="<?=$user_info['city']?>" >
              </div>
            </div>
            <div class="col-md-4 col-sm-4 ">
              <div class="form-group furture-form-style">
                <label>Street:</label>
                <input type="text" class="form-control form-style" name="street" value="<?=$user_info['street']?>" >
              </div>
            </div>
            <div class="col-md-4 col-sm-4 ">
              <div class="form-group furture-form-style">
                <label>Desired account</label>
                  <select disabled class="form-control input-medium select2me brder-radius- " id="how_many_people" name="how_many_people">
                      <?php foreach($how_many_people_arr as $how_many_people){
                          if($user_info['company_peoples'] == $how_many_people['value']){
                          ?>
                          <option value="<?=$how_many_people['value']?>"><?=$how_many_people['name']?></option>
                      <?php }}?>
                  </select>
                <!-- <input type="text" class="form-control form-style" name="company_owner" value="<?=$user_info['company_owner']?>" > -->
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="compny-information">
              <h2>Contact Information</h2>
            </div>
            
            <div class="">
            	<div class="col-md-4 col-sm-4 ">
              <div class="form-group furture-form-style">
                <label>Full Name:</label>
                <input type="text" class="form-control form-style" name="contact_full_name" value="<?=$user_info['contact_full_name']?>" >
              </div>
            </div>
                <div class="col-md-4 col-sm-4 ">
                  <div class="form-group furture-form-style">
                    <label>Email:</label><!--<i class="inf-email fa fa-info-circle" data-original-title="" title=""></i>-->
                    <input type="text" readonly class="form-control form-style" name="contact_l_name" value="<?=$user_info['email']?>" >
                  </div>
                </div>
                <div class="col-md-4 col-sm-4 cell-phone">
                  <label class="phone-lable">Cell phone </label>
                  <div class="col-md-3 no-padding">
                    <div class="form-group furture-form-style">
                      <input type="text" placeholder="Country" class="form-control form-style" name="contact_country" value="<?=$user_info['contact_country_code']?>" >
                    </div>
                  </div>
                  <div class="col-md-9 no-padding">
                    <div class="form-group furture-form-style">
                      <input type="text" placeholder="Number" class="form-control form-style" name="contact_phone" value="<?=$user_info['contact_phone']?>" >
                    </div>
                  </div>
                </div>
            </div>
            <div class="col-md-12 col-sm-12">
            <div class="col-md-4 col-sm-4 ">
                  <div class="form-group furture-form-style">
                    <strong>Account number: <span style="color:red"><?=$user_info['var_accountid']?></span></strong>
                  </div>
                </div>
            	<div class="col-md-4 col-sm-4 ">
              <div class="form-group furture-form-style">
                <label>Member since: </label>
                	<strong style="color:green"><?=date(DISPLAY_DATE_FORMAT,strtotime($user_info['entity_activate_time']))?></strong>
              </div>
            </div>
            <div class="col-md-4 col-sm-4 ">
              <div class="form-group furture-form-style">
                <label>Membership expiration:  </label>
                  <strong style="color:red"><?php if($user_info['isLifeTime']) echo "LIFETIME";else echo date(DISPLAY_DATE_FORMAT,strtotime($user_info['endDate']));//date("M, d Y", strtotime($user_info['entity_activate_time']." + 90 days"));?></strong>
              </div>
            </div>
            </div>
            <div class="col-md-12 col-sm-12" style="text-align:center">
              <button type="button" class="submit-btn-furture" onClick="validateForm()">Update <i class="fa fa-refresh fa-lg fa-spin loadin_ajax" style="color: #ffffff;display:none;"></i></button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<div aria-hidden="false" role="dialog" class="modal fade in" id="myModal_autocomplete" style="display: none;">
    <div class="success_login_main_cont margin-top-20">
        <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="fa fa-times"></i></button>
            <!--<h4 class="modal-title">Registration Successfully</h4>-->
            <h3 class="form-title">Information update Successfully</h3>
        </div>
        <div style="" class="">
            <p style="padding: 10px">Thanks your information has been updated successfully!</p>
        </div>
        <div style="padding: 3px;" class="modal-footer">
            <div style="text-align:center">
                <button type="button" style="padding: 7px 20px;" data-dismiss="modal" class="btn default_btn clickme">OK</button>
            </div>
        </div>
    </div>
</div>
<script>
			$(document).ready(function(e) {
        $('.inf-phone').tooltip({title:'Country - Phone - Extension',placement:'top'});				
				$('.inf-email').tooltip({title:'Please note that ALL RESERVATION CONFIRMATIONS made under this account will be sent to this email address by default. You will have an option upon checkout to send a copy to your own email address as well.',placement:'top'});
				getStates('<?=$user_info['country']?>');
			});
		    
			function getStates(countryId){
				$('#state').html('');
				$.getJSON( "states/"+countryId, function( data ) {
					$.each( data, function( key, val ) {
						var selected = '';
						if($.trim(val.id) == '<?=$user_info['state']?>'){
							selected = 'selected="selected"';	
						}
						var str = '<option '+selected+' value="'+val.id+'">'+decodeEntities(val.name)+'</option>';
						$('#state').append(str);
					});
				});
			}
			
			function decodeEntities(encodedString) {
				var textArea = document.createElement('textarea');
				textArea.innerHTML = encodedString;
				return textArea.value;
			}
			function validateForm(){
				$('.has-error').removeClass('has-error');
				var error = false;
				
				if($.trim($('input[name="phone_country"]').val()) == '' ){
					$('input[name="phone_country"]').parent('.form-group').addClass('has-error');
					error = true;
				}
				if(!$.isNumeric($('input[name="phone_country"]').val())){
					$('input[name="phone_country"]').parent('.form-group').addClass('has-error');
					error = true;
				}
				
				if($.trim($('input[name="phone_number"]').val()) == ''){
					$('input[name="phone_number"]').parent('.form-group').addClass('has-error');
					error = true;
				}
				if(!$.isNumeric($('input[name="phone_number"]').val())){
					$('input[name="phone_number"]').parent('.form-group').addClass('has-error');
					error = true;
				}
				
				if($.trim($('input[name="city"]').val()) == ''){
					$('input[name="city"]').parent('.form-group').addClass('has-error');
					error = true;
				}
				if($.trim($('input[name="street"]').val()) == ''){
					$('input[name="street"]').parent('.form-group').addClass('has-error');
					error = true;
				}

				if($.trim($('input[name="contact_full_name"]').val()) == ''){
					$('input[name="contact_full_name"]').parent('.form-group').addClass('has-error');
					error = true;
				}
				
				if($.trim($('input[name="contact_country"]').val()) == ''){
					$('input[name="contact_country"]').parent('.form-group').addClass('has-error');
					error = true;
				}
				if(!$.isNumeric($('input[name="contact_country"]').val())){
					$('input[name="contact_country"]').parent('.form-group').addClass('has-error');
					error = true;
				}
				
				if($.trim($('input[name="contact_phone"]').val()) == ''){
					$('input[name="contact_phone"]').parent('.form-group').addClass('has-error');
					error = true;
				}
				if(!$.isNumeric($('input[name="contact_phone"]').val())){
					$('input[name="contact_phone"]').parent('.form-group').addClass('has-error');
					error = true;
				}
				if(error == false){
					submitform();
				}else{
					//$('#response_div').show().removeClass('alert-success').addClass('alert-danger');;
					//$('#response_div_text').html('Please correct the following errors');
					
					$('html, body').animate({
						scrollTop: $("#response_div").offset().top
					}, 500);
				}
			}
			function submitform(){
				$('.loadin_ajax').show();
				var dataString = $('#travel_professional_signup').serialize();
				$.ajax({
					type: "POST",
					url: "<?=site_url('front/entity/profile_update_post')?>",
					data: dataString,
					cache: false,
					success: function(result){
						$('.loadin_ajax').hide();
						if($.trim(result) == 'success'){
							//$('#response_div').show().removeClass('alert-danger').addClass('alert-success');
							//$('#response_div_text').html('Information update successfully.');
							$("#myModal_autocomplete").modal('show');
							$('#response_div').hide();
						}else{
							//$('#response_div').show();
							//$('#response_div_text').html(result);
						}
						$('html, body').animate({
							scrollTop: $("#response_div").offset().top
						}, 500);
					},
					error: function (request, status, error) {}
				});
			}
	    </script> 
