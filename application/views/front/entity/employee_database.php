<?php 
$allSession  = $this->session->all_userdata();
//echo '<pre>';print_r($allSession).'</pre>';
?>
<style>
h2, h1, h3, h4, h5 {
	margin: 0;
	padding: 0;
}
.bg-furture {
	background: rgba(255, 255, 255, 0.74);
	;
	height: auto;
	width: 100%;
	padding: 15px 50px;
}
.heading-text h1 {
	color: #fff;
	font-weight: 600;
	text-transform: capitalize;
	margin: 30px 0 0;
}
.heading-text {
	display: block;
	margin: 0 0 60px;
	overflow: hidden;
	text-align: center;
}
.bg-furture h2 {
	border-bottom: 1px solid #999;
	color: #3d7db6;
	display: block;
	font-size: 20px;
	padding: 0 0 5px;
	margin: 0 0 10px;
	text-transform: uppercase;
}
.furture-form-style label {
	font-weight: normal;
	margin: 0 0 10px;
	text-transform: capitalize;
}
.form-style {
	display: block;
	margin: 0 0 10px;
	border-radius: 0px;
	font-size: 12px;
	text-align: center;
	padding: 0 2px;
}
.phone-lable {
	display: block;
	width: 100%;
}
.no-padding {
	display: block;
	padding: 0;
	margin: 0;
}
.col-md-4.no-padding {
	padding: 0 5px 0 0;
}
.col-md-3.no-padding {
	padding: 0 5px 0 0;
}
.phone-lable {
	display: block;
	font-weight: normal;
	margin: 0 0 10px;
	width: 100%;
}
.compny-information {
	margin: 70px 0 0;
	padding: 0 15px;
}
.submit-btn-furture {
	background: rgba(0, 0, 0, 0) linear-gradient(to bottom, #71d1ff 0%, #22b3f4 4%, #198fef 100%) repeat scroll 0 0;
	border: medium none;
	border-radius: 6px;
	box-shadow: 0 1px 3px rgba(0, 0, 0, 0.4);
	color: #fff;
	font-family: "FuturaStd-Book";
	font-size: 16px;
	padding: 10px 25px;
	text-shadow: 1px 1px 2px #000;
	text-transform: uppercase;
	margin: 30px 0 30px;
}
.bg-furture .fa-info-circle {
	cursor: pointer;
	vertical-align: super;
	font-size: 11px;
	color: #0d9de3;
}
.bg-furture .fa-info-circle:hover {
	color: #0b73a5;
}
.brder-radius-none {
	border-radius: 0px;
}
select {
	border-radius: 0 !important;
	font-size: 12px !important;
}
.form-wizard .active .step .number {
	background-color: #35aa47;
	color: #fff;
}
.form-wizard .step .number {
	background-color: #eee;
	border-radius: 50% !important;
	display: inline-block;
	font-size: 16px;
	font-weight: 300;
	height: 45px;
	margin-right: 10px;
	padding: 11px 15px 13px;
	text-align: center !important;
	width: 45px;
}
.margin-bottom-link{
	margin: 10px 0 0;
}
.table-emply-sec {
  background: #fff none repeat scroll 0 0;
  display: block;
  margin: 15px 0;
  padding: 10px;
  width: 100%;
}
.table-emply-sec th {
  width: 25%;
}
</style>
<style scoped>
		   .tooltip-inner {max-width: 600px;width: 600px;}
		   .pricacy-width{width: 100%; overflow: auto; height: 200px;}
		   a.tooltips {position: relative;display: inline;}
		   .username1 span{visibility: hidden;display: none;}
		   .email1 span{
		   visibility: hidden;
		   display: none;
		   }
		   .tooltips span {
		   position: absolute;
		   color: #FFFFFF;
		   background: #000000;
		   height: 30px;
		   line-height: 30px;
		   text-align: center;
		   visibility: hidden;
		   border-radius: 6px;
		   }.tooltips span::after {
		   border-left: 8px solid transparent;
		   border-right: 8px solid transparent;
		   border-top: 8px solid #000000;
		   content: "";
		   height: 0;
		   left: 50%;
		   margin-left: -8px;
		   position: absolute;
		   top: 86%;
		   width: 0;
		   }.username1.tooltips span {
		   bottom: 38px;
		   display: block;
		   left: 0;
		   margin-left: 16px;
		   opacity: 0.8;
		   visibility: visible;
		   z-index: 999;
		   width:155px;
		   }.email1.tooltips span {
		   bottom: 38px;
		   display: block;
		   left: 0;
		   margin-left: 16px;
		   opacity: 0.8;
		   visibility: visible;
		   z-index: 999;
		   width:255px;
		   }
		   #capatcha {
		   margin: 0 28%;
		   display: block
		   }
		</style>
<div class="container ">
  <div class="bg-furture">
    <h2>EMPLOYEE DATABASE:</h2>
    <div class="tab-content">
      <div class="tab-pane active" id="tab1">
      	<?php if(isset($allSession['valid_account']) && $allSession['valid_account'] != ''){ ?>
        	
            <strong>CONGRATULATIONS!</strong> You may now distribute the following link among your employees to sign up for <strong>FREE</strong> on <strong style="margin-bottom: 10px;"> WHotelsGroup.com : <a href="<?=site_url()?>front/entity/entity-sub-signup/<?=$allSession['valid_account']?>" target="_blank"><?=site_url()?>front/entity/entity-sub-signup/<?=$allSession['valid_account']?></a></strong>
        	
             <div class="row">
             	<div class="col-md-12 col-sm-12 ">
             		<div class="table-emply-sec">
		            	<table class="table table-bordered table-emply">
		                	<tr>
		                    	<th>Full name</th>
		                        <th>Email</th>
		                        <th>Enrollment date</th>
		                        <th>Action</th>
		                    </tr>
		                    <?php 
								if(isset($child_users) && sizeof($child_users->result()) > 0){
									foreach($child_users->result() as $subUserRow){
							?>
		                    	<tr id="tr_<?=$subUserRow->int_glcode?>">
		                        	<td><?=$subUserRow->var_fname.' '.$subUserRow->var_lname?></td>
		                            <td><?=$subUserRow->var_email?></td>
		                            <td><?=date(DISPLAY_DATE_FORMAT,strtotime($subUserRow->entity_activate_time))?></td>
		                            <td><div class="pages_links"><a href="javascript:void(0)" onClick="deleteIt('<?=$subUserRow->int_glcode?>')" class="login_link main_links">Delete</a></div></td>
		                        </tr>
		                    <?			
									}
								}else{?>
		                        <tr>
		                        	<td colspan="4">Sorry! No User found.</td>
		                        </tr>
		                    
		                    <? } ?>
		                </table>
		            </div>    
                </div>
             </div>   
            
        <? }else{ ?>
        <form method="post" id="travel_professional_signup">
          <div class="row">
            <div class="col-md-5 col-sm-5 ">
              <div class="form-group furture-form-style <?=isset($has_error) && !empty($has_error) ? 'has-error':''?>">
                <label>Please enter your 7-digit authorization code.</label>
                <input type="text" required value="" class="form-control form-style" name="auth_code" >
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12 col-sm-12" style="text-align:left">
              <button type="submit" class="submit-btn-furture">Submit<i class="fa fa-refresh fa-lg fa-spin loadin_ajax" style="color: #ffffff;display:none;"></i></button>
            </div>
          </div>
        </form>
        <?php } ?>
      </div>
    </div>
  </div>
</div>
<div aria-hidden="false" role="dialog" class="modal fade in" id="myModal_autocomplete" style="display: none;">
    <div class="success_login_main_cont margin-top-20">
        <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="fa fa-times"></i></button>
            <!--<h4 class="modal-title">Registration Successfully</h4>-->
            <h3 class="form-title">User Deleted Successfully</h3>
        </div>
        <div style="" class="">
        </div>
        <div style="padding: 3px;" class="modal-footer">
            <div style="text-align:center">
                <button type="button" style="padding: 7px 20px;" data-dismiss="modal" class="btn default_btn clickme">OK</button>
            </div>
        </div>
    </div>
</div> 
<div aria-hidden="false" role="dialog" class="modal fade in" id="deleteModal" style="display: none;">
    <div class="success_login_main_cont margin-top-20">
        <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="fa fa-times"></i></button>
        </div>
        <div style="" class="">
            <p style="padding: 10px">Are you sure you want to delete this user?</p>
        </div>
        <div style="padding: 3px;" class="modal-footer">
            <div style="text-align:center">
            <button type="button" style="padding: 7px 20px;" class="btn default_btn clickme" onClick="deleteUserPost()">YES <i class="fa fa-refresh fa-lg fa-spin loadin_ajax" style="color: #ffffff;display:none;"></i></button>
                <button type="button" style="padding: 7px 20px;" data-dismiss="modal" class="btn default_btn clickme">No</button>
            </div>
        </div>
    </div>
</div> 
<script>
var deletUserId;
function deleteIt(uid){
	deletUserId = uid;
	$("#deleteModal").modal('show');	
}
function deleteUserPost(){
	
	var dataString = 'uid='+deletUserId;
	$('.loadin_ajax').show();
	$.ajax({
		type: "POST",
		url: "<?=site_url('front/entity/delete_sub_user')?>",
		data: dataString,
		cache: false,
		success: function(result){
			//alert(result);
			$('.loadin_ajax').hide();
			if($.trim(result) == 'success'){
				$('#tr_'+deletUserId).remove();
				$("#myModal_autocomplete").modal('show');
				$("#deleteModal").modal('hide');
			}
		},
		error: function (request, status, error) {}
	});	
}
</script>