<style>
h2, h1, h3, h4, h5 {
	margin: 0;
	padding: 0;
}
.bg-furture {
	background: rgba(255, 255, 255, 0.74);
	;
	height: auto;
	width: 100%;
	padding: 15px 50px;
}
.heading-text h1 {
	color: #fff;
	font-weight: 600;
	text-transform: capitalize;
	margin: 30px 0 0;
}
.heading-text {
	display: block;
	margin: 0 0 60px;
	overflow: hidden;
	text-align: center;
}
.bg-furture h2 {
	border-bottom: 1px solid #999;
	color: #3d7db6;
	display: block;
	font-size: 20px;
	padding: 0 0 5px;
	margin: 0 0 10px;
	text-transform: uppercase;
}
.furture-form-style label {
	font-weight: normal;
	margin: 0 0 10px;
	text-transform: capitalize;
}
.form-style {
	display: block;
	margin: 0 0 10px;
	border-radius: 0px;
	font-size: 12px;
	text-align: center;
	padding: 0 2px;
}
.phone-lable {
	display: block;
	width: 100%;
}
.no-padding {
	display: block;
	padding: 0;
	margin: 0;
}
.col-md-4.no-padding {
	padding: 0 5px 0 0;
}
.col-md-3.no-padding {
	padding: 0 5px 0 0;
}
.phone-lable {
	display: block;
	font-weight: normal;
	margin: 0 0 10px;
	width: 100%;
}
.compny-information {
	margin: 70px 0 0;
	padding: 0 15px;
}
.submit-btn-furture {
	background: rgba(0, 0, 0, 0) linear-gradient(to bottom, #71d1ff 0%, #22b3f4 4%, #198fef 100%) repeat scroll 0 0;
	border: medium none;
	border-radius: 6px;
	box-shadow: 0 1px 3px rgba(0, 0, 0, 0.4);
	color: #fff;
	font-family: "FuturaStd-Book";
	font-size: 16px;
	padding: 10px 25px;
	text-shadow: 1px 1px 2px #000;
	text-transform: uppercase;
	margin: 30px 0 30px;
}
.bg-furture .fa-info-circle {
	cursor: pointer;
	vertical-align: super;
	font-size: 11px;
	color: #0d9de3;
}
.bg-furture .fa-info-circle:hover {
	color: #0b73a5;
}
.brder-radius-none {
	border-radius: 0px;
}
select {
	border-radius: 0 !important;
	font-size: 12px !important;
}
.form-wizard .active .step .number {
	background-color: #35aa47;
	color: #fff;
}
.form-wizard .step .number {
	background-color: #eee;
	border-radius: 50% !important;
	display: inline-block;
	font-size: 16px;
	font-weight: 300;
	height: 45px;
	margin-right: 10px;
	padding: 11px 15px 13px;
	text-align: center !important;
	width: 45px;
}
</style>
<style scoped>
		   .tooltip-inner {max-width: 600px;width: 600px;}
		   .pricacy-width{width: 100%; overflow: auto; height: 200px;}
		   a.tooltips {position: relative;display: inline;}
		   .username1 span{visibility: hidden;display: none;}
		   .email1 span{
		   visibility: hidden;
		   display: none;
		   }
		   .tooltips span {
		   position: absolute;
		   color: #FFFFFF;
		   background: #000000;
		   height: 30px;
		   line-height: 30px;
		   text-align: center;
		   visibility: hidden;
		   border-radius: 6px;
		   }.tooltips span::after {
		   border-left: 8px solid transparent;
		   border-right: 8px solid transparent;
		   border-top: 8px solid #000000;
		   content: "";
		   height: 0;
		   left: 50%;
		   margin-left: -8px;
		   position: absolute;
		   top: 86%;
		   width: 0;
		   }.username1.tooltips span {
		   bottom: 38px;
		   display: block;
		   left: 0;
		   margin-left: 16px;
		   opacity: 0.8;
		   visibility: visible;
		   z-index: 999;
		   width:155px;
		   }.email1.tooltips span {
		   bottom: 38px;
		   display: block;
		   left: 0;
		   margin-left: 16px;
		   opacity: 0.8;
		   visibility: visible;
		   z-index: 999;
		   width:255px;
		   }
		   #capatcha {
		   margin: 0 28%;
		   display: block
		   }
		</style>
<div class="container ">
  <div class="main_cont">
        <div class="pagetitle margin-bottom-20">
            <h1>Verification</h1>
        </div>
        <div class="row">
            <div class="col-md-12" id="form_wizard_1">
                <p><?php echo $msg;?></p>
            </div>
        </div>
    </div>
</div> 
