
<div class="width-row">
	<div class="main_cont">

        <div class="alert alert-success">
          <strong>CONGRATULATIONS!</strong> You have successfully submitted your request to enroll on <strong>whotelsgroup.com</strong>.
        </div>
        
        <p>
            <span style="text-decoration:underline; font-weight:bold;">What’s next? :</span>We are going to manually review your application and if everything looks accurate, then we are going to send you an email with a link for you to activate your account. Once we send out the link, you will have <strong>72 hours</strong> to click on it. If you fail to complete the signup process you need to enroll on our site again. You can expect to hear from us <strong>within 3 business days</strong> but if you do not, please send us a message.
		</p>
        <p>Should you have any questions in the meantime please do not hesitate to send us a message via the <a href="<?php echo base_url(); ?>user/legal-corner/contactus" target="_blank">Contact Us</a> tab. </p>
	    <p>Thank you for choosing <strong>whotelsgroup.com</strong> where we are Making hotels happen for YOU!</p>
    
        </p>                

    </div>
</div>
