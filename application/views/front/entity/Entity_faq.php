<div class="page_container">
   <div class="width-row margin-top-20">
      <div class="width-row" style="display:none;padding: 10px 0 0 8px;">
         <div itemscope="" itemtype="http://schema.org/Person">
            <span itemprop="name">Hotels Differently</span>
            <span itemprop="company"><?php echo SITE_NAME;?></span>
            <span itemprop="tel">888-287-2307</span>
         </div>
      </div>
      <div class="main_cont">
         <div class="pagetitle margin-bottom-10">
            <h1>Entity FAQ</h1>
         </div>
         <!--    <div class="faqs_title">Account related questions:</div>-->
         <div class="row text-center margin-bottom-10 margin-top-10">
            <div class="form-group col-md-12 text-center margin-bottom-10">
               <label class="control-label col-md-5" style="padding: 6px 0;width: 37%;"> Questions? Type in a keyword (e.g: password):</label>
               <div class="col-md-7" style="padding: 0 5px;">
                  <input name="que_search" id="que_search" class="form-control" type="text">
               </div>
            </div>
         </div>
         <div id="faqsearch">
            <div class="questions">
               <span>Q 01</span>
               I forgot my password, how do I retrieve it?
            </div>
            <div class="answers">
               No worries! To retrieve your password, please go to the homepage, click on the "Login" button, click on the "Forgot your password?" text, type in your email address then click on the "SUBMIT" button and your password should arrive instantly to your inbox. <br><br>
               Does it seem like it has been taking too long and you still have not received your password? Please check your SPAM/JUNK folder or try again. 
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 2</span>
               Will I be able to earn points with my hotel program that I am a Member of?
            </div>
            <div class="answers">
               Every hotel's loyalty program differs greatly and some of them change without any prior notice. It is advisable to ask your hotel's loyalty program to see if you can earn points on your stay if you book your reservation outside of the hotel's own website.
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 3</span>
               Can I book directly with the hotel and receive the same rate? 
            </div>
            <div class="answers">
               We are able to provide you a low rate because we have access to a discounted rates due to our buying power and number of bookings, that are NOT available to the general public. In addition, in some circumstances, we may have packaged amenities (such as a complimentary breakfast) that would not be available to you otherwise. So trying to book directly with the hotel in an attempt to get the same low price directly from them; even if it was available could cause you to lose certain amenities. As the old saying goes: "You have to compare apples to apples and oranges to oranges".
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 4</span>
               What does your newsletter contain?
            </div>
            <div class="answers">
               Our newsletter contains interesting news, such as: <br>
               <ul class="bullet_color" type="square">
                  <li><span class="ul_text">Our biggest savings of the week</span></li>
                  <li><span class="ul_text">Website updates</span></li>
                  <li><span class="ul_text">Coupons that can be used for future bookings!</span></li>
               </ul>
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 5</span>
               Do you sell gift cards?
            </div>
            <div class="answers">
               No, at this time we do NOT offer such service, sorry.
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 6</span>
               How do I subscribe for the newsletter?
            </div>
            <div class="answers">
               To subscribe for our newsletter, please go to the homepage and on the top left section you will see the newsletter subscription. Enter your email address and then confirm it once you receive the confirmation email. As soon as you confirm it, your email address will be added and you will get our newsletter from then on.
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 7</span>
               How do I unsubscribe from the newsletter?
            </div>
            <div class="answers">
               If you would like to stop receiving newsletters from us, you can easily unsubscribe by clicking on the "unsubscribe" hyperlink that is included at the bottom of our newsletters.
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 8</span>
               How secure is the payment on this website?
            </div>
            <div class="answers">
               Our payment Site is <b>EXTREMELY SECURE!</b> Our address bar has the green bar, which means we implemented SSL + Extended Validation which is currently one of the most secure system out there. For more information about the SSL+EV, please click <a style="font-weight:bold;" href="https://www.hotelsdifferently.com/user/legal-corner/security-statement" target="_blank">HERE</a> to access our <b>Security Statement.</b> <br><br>
               For our payment processor we use PayPal<sup>TM</sup> and Stripe<sup>TM</sup> that are the most trusted payment processors in the world right now and when you submit your highly sensitive payment information, you will be directed to their payment site to ensuring that your payment information is in safe hands.<br> 
               <b style="color:#ff0000">We DO NOT maintain any credit or debit card information in our systems; it is maintained ONLY with the payment processor!</b>
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 9</span>
               Do you offer travel insurance?
            </div>
            <div class="answers">
               <p>We (<b><?php echo SITE_NAME;?></b><sup>sm</sup>) do NOT offer travel insurance, however you can purchase travel insurance from a third-party insurance company. Please note that we do not directly sell any types of insurance; we are solely an Affiliate to a third-party insurance company. Although we are not insurers, we have made it easy for you to obtain more information about travel insurance by clicking on Travel Insurance on our Home Page. Here you will not only find useful information, but a link to what we believe is one of the best Travel Insurance providers and a company with whom we are proud to be affiliated: Allianz Global Assistance. Should you choose to use Allianz Global Assistance, please understand that their insurance policy, and not our Terms and Conditions, govern your travel insurance protection.</p>
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 10</span>
               How do you calculate the total revenue for each calendar year?
            </div>
            <div class="answers">
               At the end of every calendar year, we assess total revenue in a simple way: <b>Amount spent</b> (this includes the combined amount spent within your corporate account which includes all the employees spending) <b>MINUS</b> the <b>total amount of refunds</b> we issued to said accounts which <b>equals</b> to the <b>TOTAL REVENUE.</b> 
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 11</span>
               How much does it cost to have an account with <?php echo DOMAIN_NAME;?>?
            </div>
            <div class="answers">
              The account for qualified entities vary depending on the number of employees the business entity has. <strong><?php echo DOMAIN_NAME;?> </strong> is a paid membership based site which sells memberships to individual users, therefore the website makes money mostly on selling memberships while providing one of the most competitive rates in the market through its extremely low markup.
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 12</span>
               How can I add additional employees?
            </div>
            <div class="answers">
               Should you wish to add additional employees, you need to send the signup link you received via email during after successful enrollment. Alternatively, you can also obtain the link after login by clicking on the <strong>EMPLOYEE DATABASE </strong> menu. Enter your admin code and then you can access your signup link. In case you have issues retrieving your admin code, just shoot us an email from the email account you registered your master account and we will gladly resend it to you. 
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 13</span>
               My employee account was deleted! Why?
            </div>
            <div class="answers">
               In case your employee account was deleted on <strong><?php echo DOMAIN_NAME;?>, </strong> please contact your company’s travel representative or the person in charge of enrollment who has the enrollment code in his/her possession because such person is eligible to remove employees from the database on <strong><?php echo DOMAIN_NAME;?>. </strong>
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 14</span>
               When will my account qualify for a tier discount?
            </div>
            <div class="answers">
              If your combined revenue has reached any amount <strong> above $1,000,000 </strong> in a calendar year, then you need to request your tier discount manually by sending us a message. We will review your request and in case your account is eligible, it will be awarded with a tier discount for all your enrolled employee accounts. <strong><?php echo DOMAIN_NAME;?> </strong> reserves the right to void this offer without any prior notice.
            </div>
            <div class="ques_border"></div>
            <div class="questions">
               <span>Q 15</span>
               My name has changed and so did on my credit / debit card. How can I change it?
            </div>
            <div class="answers">
              Should you wish to change your name on your account, you need to request it through an admin account. Please contact your company’s travel representative or the person in charge of enrollment who has the enrollment code in his/her possession because such person is eligible to add or remove employee database on <?php echo DOMAIN_NAME;?>. In case of name change, you may need to supply supporting documentation (e.g: legal name change document) to your admin and your admin would need to create a new account for you.
            </div>
            <div class="ques_border"></div>
         </div>
      </div>
   </div>
</div>
<script>
$(document).ready(function() {
 faq.init();
});
var faq = function (){
var fqa_serach = function (){
    $('#que_search').keyup(function(){
      var text = $('#que_search').val();
      //alert(text);
      $('.answers').show();  
      $('.questions').show();
      $('.ques_border').show();
      
      $('.answers:not(:contains(' + text + '))').hide();
      $('.answers:not(:contains(' + text + '))').prev('.questions').hide();
      $('.answers:not(:contains(' + text + '))').next('.ques_border').hide();
      
      
    });
   
}
return {
init: function() {
   fqa_serach();
}
};
}();
</script>