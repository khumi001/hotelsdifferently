<!DOCTYPE html>
<html>
    <?php echo $this->load->view('front/includes/header'); ?>
    <body>
		<!-- Go to www.addthis.com/dashboard to customize your tools --> <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5864bf851ff4218d"></script> 
		<script type="text/javascript">
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-87962950-1', 'auto');
		  ga('send', 'pageview');

		</script>
        <?php echo $this->load->view('front/includes/body_header'); ?>
        <?php // echo $this->load->view('affiliate/includes/navigation'); ?>
        <div class="clear"></div>
        <div id="result_container">
            <div class="page_container">
                <div class="width-row margin-top-10">
                    <div class="container">
                         <?php // echo $this->load->view('affiliate/includes/breadcrumbs'); ?>
                        <?php echo $this->load->view($page); ?>
                    </div>
                </div>   
            </div>
        </div>   
        <?php echo $this->load->view('front/includes/body_footer'); ?>
        <?php echo $this->load->view('front/includes/footer'); ?>
    </body>
</html>
