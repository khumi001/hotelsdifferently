
<div class="width-row" style="background-color: rgba(255, 255, 255, 0.74) none repeat scroll 0 0;">
	<div class="main_cont">
        <strong class="text-center" style="text-align:center;display:block;margin:0 0 15px;">Welcome to Wholesale Hotels Group</strong>
        <div class="" style="margin: 10px 0;">
          <b>CONGRATULATIONS!</b> You have successfully submitted your request to enroll as a Travel Professional on <b>WHotelsGroup.com.</b>
        </div>
        
        <p>
            <span style="text-decoration:; font-weight:bold;display: block;">What’s next? </span> We are going to manually review your application and if everything looks accurate, then we are going to send you an email with a link for you to sign a few documents. Once we send you the link, you will have <strong>30 days</strong> to complete the signup process. If you fail to complete the signup process you need to enroll on our site again.You can expect to hear from us <strong>within 3 business days </strong>but if you do not, please send us a message.
		</p>
        <p style="margin: 17px 0;">Should you have any questions in the meantime please do not hesitate to send us a message via the <a href="<?php echo base_url(); ?>user/legal-corner/contactus" target="_blank" style="color:#333;">Contact Us</a> tab. </p>
	    <p style="margin: 0 0 60px;">Thank you for choosing <b>WHotelsGroup.com</b> where we are <i>Where better deals are made for YOU!</i></p>
        </p>                
    </div>
</div>
