<style>
    .closeok {
        color: #000;
        float: center;
        font-size: 21px;
        font-weight: 700;
        line-height: 1;
        opacity: 0.2;
        text-shadow: 0 1px 0 #fff;
    }
	.change-pass-form{
		max-width:700px;
		margin:0 auto;
	}
	.change-pass-page h1{
		text-align:center;
	}
</style>

<div class="width-row margin-top-20 change-pass-page">
    <div class="main_cont">
        <div class="pagetitle margin-bottom-10">
            <h1>CHANGE PASSWORD</h1>
        </div>
        <div class="row">
            <form action="#" method="post" class="form-horizontal form-bordered" id="edit_pass">
								<div class="alert alert-danger display-hide" id="response_div" style="display:none">
						      <button class="close" data-close="alert"></button>
						      <span id="response_div_text"> </span> 
						    </div>
		            
		            <div class="form change-pass-form">
                    <div class="form-group">
                        <label class="control-label col-md-6">Current Password:</label>
                        <div  class="col-md-6">
                            <input type="password" name="old_password" id="old_password" class="form-control oldpass" onCopy="return false" onDrag="return false"  onPaste="return false" onDrop="return false" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-6">New Password: <a class="tooltips" data-placement="top" data-original-title="Your password is CaSe SeNsItIvE, must be at least 8 characters and must contain at least one letter and one number."><sup><i class="fa fa-info-circle"></i></sup></a></label>
                        <div  class="col-md-6">
                            <input type="password" name="new_password" id="newpassword" class="form-control newpass" onCopy="return false" onDrag="return false"  onPaste="return false" onDrop="return false" autocomplete="off" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-6">New Password Confirmation:</label>
                        <div  class="col-md-6">
                            <input type="password" name="re_password" class="form-control confirmpass" onPaste="return false" onDrag="return false" onDrop="return false" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-4"></div>
                        <div class="col-md-8">
                        	<button type="button" class="default_btn" onClick="validateForm()">Update Password<i class="fa fa-refresh fa-lg fa-spin loadin_ajax" style="color: #ffffff;display:none;"></i></button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<script>
			function validateForm(){
				$('.has-error').removeClass('has-error');
				var error = false;
				
				if($.trim($('input[name="old_password"]').val()) == '' ){
					$('input[name="old_password"]').parents('.form-group:first').addClass('has-error');
					error = true;
				}
				if($.trim($('input[name="new_password"]').val()) == ''){
					$('input[name="new_password"]').parents('.form-group:first').addClass('has-error');
					error = true;
				}
				
				if($.trim($('input[name="re_password"]').val()) == ''){
					$('input[name="re_password"]').parents('.form-group:first').addClass('has-error');
					error = true;
				}
				
				
				if($('input[name="re_password"]').val() != '' && $('input[name="new_password"]').val() != ''){

					if($('input[name="re_password"]').val() != $('input[name="new_password"]').val()){

						$('input[name="re_password"]').parents('.form-group:first').addClass('has-error');
						$('input[name="new_password"]').parents('.form-group:first').addClass('has-error');
						error = true;
					}

				}
				
				if(error == false){
					submitform();
				}else{
					$('#response_div').show().removeClass('alert-success').addClass('alert-danger');;
					$('#response_div_text').html('Please correct the following errors');
					$('html, body').animate({
						scrollTop: $("#response_div").offset().top
					}, 500);
				}
			}

			function submitform(){
				$('.loadin_ajax').show();
				var dataString = $('#password_update_form').serialize();
				$.ajax({
					type: "POST",
					url: "<?=site_url('front/travel_professional/password_update_post')?>",
					data: dataString,
					cache: false,
					success: function(result){
						$('.loadin_ajax').hide();
						if($.trim(result) == 'success'){
							$('#response_div').show().removeClass('alert-danger').addClass('alert-success');
							$('#response_div_text').html('password update successfully.');
						}else{
							$('#response_div').show().removeClass('alert-success').addClass('alert-danger');
							$('#response_div_text').html(result);
						}
						$('html, body').animate({
							scrollTop: $("#response_div").offset().top
						}, 500);
					},
					error: function (request, status, error) {}
				});
			}
	    </script> 
