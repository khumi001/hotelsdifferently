<style>
h2, h1, h3, h4, h5 {
	margin: 0;
	padding: 0;
}
.bg-furture {
	background: rgba(255, 255, 255, 0.74) none repeat scroll 0 0;
    height: auto;
    margin: 0 auto;
    padding: 15px 50px;
    width: 80%;
}
.heading-text h1 {
	color: #fff;
	font-weight: 600;
	text-transform: capitalize;
	margin: 30px 0 0;
}
.heading-text {
	display: block;
	margin: 0 0 60px;
	overflow: hidden;
	text-align: center;
}
.submit-btn-furture {
	background: rgba(0, 0, 0, 0) linear-gradient(to bottom, #71d1ff 0%, #22b3f4 4%, #198fef 100%) repeat scroll 0 0;
	border: medium none;
	border-radius: 6px;
	box-shadow: 0 1px 3px rgba(0, 0, 0, 0.4);
	color: #fff;
	font-family: "FuturaStd-Book";
	font-size: 16px;
	padding: 10px 25px;
	text-shadow: 1px 1px 2px #000;
	text-transform: uppercase;
	margin: 0 0 30px;
}

.table-borderless > tbody > tr > td,
.table-borderless > tbody > tr > th,
.table-borderless > tfoot > tr > td,
.table-borderless > tfoot > tr > th,
.table-borderless > thead > tr > td,
.table-borderless > thead > tr > th {
    border: none;
}
.submit-btn-furture.red-btn {
  background: red none repeat scroll 0 0;
}
</style>
<div class="container">
  <!-- <div class="heading-text">
    <h1>AGENT MARKUP</h1>
  </div> -->
  <div class="bg-furture">
    <div class="alert alert-danger display-hide" id="response_div" style="display:none">
      <button class="close" data-close="alert"></button>
      <span id="response_div_text"> </span> </div>
    <div class="tab-content">
      <div class="tab-pane active">
        <p style="border-bottom:1px solid gray"><strong style="text-align: center;display: block;padding: 5px;">You can use this page to set your agent markup in your account.</strong></p>
        
       	<!-- <p><strong style="text-decoration:underline">How does it work? </strong></p> -->
        <p>This page is meant for Travel Professionals to set a specific markup rate that is being displayed throughout our site. You can either set a percentage OR a dollar amount. </p>
        <p>Once you set it, you can browse our site and we will display the marked up rate on Hotels, Car rentals, Cruises and Activities and you will be able to show it to others without them knowing how much your actual rate is going to be. However, once you checkout, you will pay the non-marked up rate. </p>
        
        <form method="post" id="agent_markeup_form">
        	
            <div class="row" style="text-align:center">
                <div class="col-md-5 col-sm-5">
                	<p><b>Your markup rate is currently set at:</b></p>
                </div>

                <div class="col-md-6 col-sm-6">
	                <div class="col-md-12 col-sm-12">
	                    <?php 
												$allSession   = $this->session->all_userdata();
												$markup_type  = $allSession['travel_professional_detail']['markup_type'];
												$markup_value = $allSession['travel_professional_detail']['markup_value'];
											?>

	                    <table class="table table-responsive table-borderless">
	                    	<tr>
	                    			<td>
	                    					<input type="radio" id="precentage_radio" onClick="checkmarkup(this.value)" <?=$markup_type == '%'?'checked="checked"':''?> name="markup_type" value="%">
	                    			</td>
	                    			<td>
	                    				<input type="text" placeholder="Percentage" value="<?=($markup_type == '%' && !empty($markup_value) )?number_format($markup_value,2):''?>" onkeyup="calculateMarkup('percentage_value');" name="percentage_value" id="percentage_value" class="int_only"> %
	                    			</td>
	                    	</tr>
	                    	<tr>
	                    			<td>
	                    					<input type="radio"  id="dollar_radio" onClick="checkmarkup(this.value)" <?=$markup_type == '$'?'checked="checked"':''?> name="markup_type" value="$">
	                    			</td>
	                    			<td>
	                    				<input type="text" placeholder="Dollars" value="<?=($markup_type == '$' && !empty($markup_value) )?number_format($markup_value,2):''?>" onkeyup="calculateMarkup('dollar_value');" name="dollar_value" id="dollar_value" class="int_only"> $
	                    			</td>
	                    	</tr>

	                    </table>

	                </div>

									<div class="col-md-12 col-sm-12">
	                    <div class="form-group furture-form-style">
	                     	
	                         <button type="button" class="submit-btn-furture red-btn" onclick="resetForm()">Reset</button>

	                         <button type="button" class="submit-btn-furture" onclick="submitform()">Apply <i class="fa fa-refresh fa-lg fa-spin loadin_ajax" style="color: #ffffff;display:none;"></i></button>
	                      	  
	                    </div>
	                </div>

	              </div>

            </div>
            
            
        </form>
        
        <strong>Example:</strong> If something on our site is $100.00 then we will display such rate as <strong id="markupAmount">$100.00</strong> until you reach the checkout page.</p>
        
      </div>
    </div>
  </div>
</div>


<div aria-hidden="false" role="dialog" class="modal fade in" id="myModal_success" style="display: none;">
    <div class="success_login_main_cont margin-top-20">
        <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="fa fa-times"></i></button>
            <!--<h4 class="modal-title">Registration Successfully</h4>-->
            <h3 class="form-title">SUCCESS</h3>
        </div>
        <div style="" class="">
            <p style="padding: 10px">Your agent markup has been successfully updated!</p>
        </div>
        <div style="padding: 3px;" class="modal-footer">
            <div style="text-align:center">
                <button type="button" style="padding: 7px 20px;" data-dismiss="modal" class="btn default_btn clickme">OK</button>
            </div>
        </div>
    </div>
</div>

<div aria-hidden="false" role="dialog" class="modal fade in" id="myModal_success" style="display: none;">
    <div class="success_login_main_cont margin-top-20">
        <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="fa fa-times"></i></button>
            <!--<h4 class="modal-title">Registration Successfully</h4>-->
            <h3 class="form-title">Error Updating Agent Markup</h3>
        </div>
        <div style="" class="">
            <p style="padding: 10px">something went wrong. Please try again!</p>
        </div>
        <div style="padding: 3px;" class="modal-footer">
            <div style="text-align:center">
                <button type="button" style="padding: 7px 20px;" data-dismiss="modal" class="btn default_btn clickme">OK</button>
            </div>
        </div>
    </div>
</div>

<script>
	$(document).ready(function(e) {
		checkmarkup('<?=$markup_type?>');
		$('.int_only').on('keyup',function(e){
			this.value = this.value.replace(/[^0-9\.]/g,'');
		})

		$("#percentage_value").maskMoney({prefix: '', allowNegative: false, thousands: ',', decimal: '.', affixesStay: false});
		$("#dollar_value").maskMoney({prefix: '', allowNegative: false, thousands: ',', decimal: '.', affixesStay: false});

  });

  function calculateMarkup(markup_type){
  	var markamount = 100;
  	if(markup_type == 'percentage_value'){
  		var percVal = parseFloat( $('#percentage_value').val().replace(/\,/g, ' ')  );
  		markamount += percVal;
  	}else{
  		var dollVal = parseFloat($('#dollar_value').val().replace(/\,/g, ' ') );
  		markamount += dollVal;
  	}
  	//$('#markupAmount').html(Math.round( ( (markamount / 100) * 100 ), 2));

  	if( isNaN(markamount) ){
  		markamount = 0;
  	}

  	$('#markupAmount').html('$' + markamount.toFixed(2));
  }


	function checkmarkup(_type){
		if(_type == '%'){
			$('#percentage_value').prop('disabled', false);
			$('#dollar_value').prop('disabled', true);
			$('#dollar_value').val('');
		}

		if(_type == '$'){
			$('#dollar_value').prop('disabled', false);
			$('#percentage_value').prop('disabled', true);
			$('#percentage_value').val('');
		}

	}
	function resetForm(){
		checkmarkup('%');
		$("#precentage_radio").attr('checked', 'checked');
		$("#dollar_radio").attr('checked', false);
		$('#percentage_value').val('');
		$('#dollar_value').val('');
		$('#markupAmount').html('$100.00');
	}
	function submitform(){
		$('.loadin_ajax').show();
		var dataString = $('#agent_markeup_form').serialize();
		$.ajax({
			type: "POST",
			url: "<?=site_url('front/travel_professional/agent_markup_post')?>",
			data: dataString,
			cache: false,
			success: function(result){
				$('.loadin_ajax').hide();
				if($.trim(result) == 'success'){
					//$('#response_div').show().removeClass('alert-danger').addClass('alert-success');
					$("#myModal_success").modal('show');
				}else{
					//$('#response_div').show();
					//$('#response_div_text').html(result);
					$("#myModal_error").modal('show');
				}
			},
			error: function (request, status, error) {}
		});
	}
</script>
