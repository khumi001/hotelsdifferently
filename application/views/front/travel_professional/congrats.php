
<div class="width-row">
	<div class="wizTab">

        <div class="alert alert-success">
          CONGRATULATIONS! YOU ARE IN!
        </div>
        <p>
            In order to search and book reservations, please enter your debit or credit card number that you provided during your signup and filled out the authorization form for.
		</p>
        <p>
        <span style="text-decoration:underline; font-weight:bold;">Your debit / credit card number:</span> [Editable field, 16 DIGITS MAX and last 4 digits are AUTO-FILLED by Admin upon approval that is non-editable]. SAVE (button). Upon clicking on SAVE button, please display popup: Are you sure you want to save this card XXXX-XXXX-XXXX-XXXX? Once you save it, you will not be able to change it until you fill out an authorization for another card.
        </p>
        <p>
        YES/NO. Upon clicking on YES, please use STRIPE to save it under a digital wallet (tokenization method). 
Once there is a card saved, please ALLOW searches and bookings.
        </p>
        

    </div>
</div>
