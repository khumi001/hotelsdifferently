<div class="width-row">

    <div class="col-md-4 col-sm-4">
    
        <div class="wizWized">
            <div class="sWhead">
                <h4>Reservation Details </h4>
            </div>
            <div class="clearfix">
            <?php
				$overAllTotal = 0;
            	foreach($cart_list as $option_id=>$cart){
                	foreach($cart as $date=>$details){
						$overAllTotal += $details['total_price'];
			?>
                    
                    <div class="media" style="padding:0px 15px;">
                        <a class="pull-left" href="#">
                            <img style="width:100px; margin-bottom: 0px;" class="media-object img-responsive thumbnail" src="<?php echo $details['thumbUrl']?>" alt="image">
                        </a>
                        <div class="media-body">
                            <div class="panel-title">
                                <b><?php echo $details['activityName'];?></b>
                            </div>
                            <address class="hotel_location"><?php echo $details['optionTitle'];?></address>
                        </div>
                    </div>
                    
                    <div class="bookingContent">
                        <ul class="list-unstyled">
                            <li role="separator" class="divider"></li>
                            <?php if( !empty($details['no_of_adults']) ){?>
                            <li><b>No of Adults: </b><span class="room_type_category"><?php echo $details['no_of_adults'];?></span></li>
                            <li><b>No of Children: </b><span class="num_adult_children"><?php echo $details['no_of_childs'];?></span></li>
                            <?php }?>

                            <?php if( !empty($details['no_of_units']) ){?>
                            <li><b>No of Units: </b><span class="room_type_category"><?php echo $details['no_of_units'];?></span></li>
                            <?php }?>

                            <li role="separator" class="divider"></li>
                            <li><b>Date: </b><span><?php echo $date;?></span></li>
                            <li role="separator" class="divider"></li>

                            <?php if( !empty($details['no_of_adults']) ){?>
                            <li class="text"><b>Price (Adult):</b><span id="total_room_cost">$<?php echo $details['adult_price'];?> / person</span></li>
                            <li class="text"><b>Price (Child):</b><span id="total_room_cost">$<?php echo $details['child_price'];?> / child</span></li>
                            <?php }?>

                            <?php if( !empty($details['no_of_units']) ){?>
                            <li class="text"><b>Price (Unit):</b><span id="total_room_cost">$<?php echo $details['unit_price'];?> / unit</span></li>
                            <?php }?>

                            <li role="separator" class="divider"></li>
                            <li class="text-danger"><b>Sub Total:</b><span id="total_price_html">$<?php echo $details['total_price'];?></span></li>
                            <li role="separator" class="divider"></li>
                        </ul>
                    </div>
			<?php	}
				}?>
                <div class="bookingContent">
					<ul class="list-unstyled">
                    	<li class="text-danger"><b>Total:</b><span id="total_price_html">$<?php echo $overAllTotal;?></span></li>
                    </ul>
				</div>    
            </div>
        </div>
    </div> 

	<div class="col-md-8 col-sm-8">
		<div> 
			<!-- Nav tabs -->
			<ul class="nav nav-tabs c-tabs" role="tablist">
            	<li role="presentation"><a href="<?php echo base_url(); ?>checkout">Traveler Information</a></li>
                <li role="presentation" class="active"><a href="#">Billing Information</a></li>
				<li role="presentation"><a href="#">Review & Confirm</a></li>
			</ul>
			
			<!-- Tab panes -->
			<div class="tab-content">
            <form method="post" id="billing_form">
            
				<div role="tabpanel" class="tab-pane active" id="home">
					<div class="tab-content wizTab " id="traveller_information_body"> <!-- step-content -->
                    
                       <h4>Billing Information</h4>
                        <div class="row">
                            <?php 
                                if(isset($error)) 
                                {
                                    ?>
                                    <div class="alert alert-danger">
                                      <?php echo $error; ?>
                                    </div>
                                    <?php
                                }
                            ?>
                        </div>
                        
                        <div id="billingInfo">
                            <div class="form-group">
                                <label class="control-label col-sm-3"> First Name:</label>
                                <div  class="col-sm-9">
                                    <input type='text' name='cart_billing[firstname]' class="form-control input-medium" id='' value="<?php echo isset($cart_billing['firstname']) ? $cart_billing['firstname'] : $userInfo->var_fname;?>" required>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3"> Last Name:</label>
                                <div  class="col-sm-9">
                                    <input type='text' name='cart_billing[lastname]' class="form-control input-medium" id='' value="<?php echo isset($cart_billing['lastname']) ? $cart_billing['lastname'] : $userInfo->var_lname;?>" required>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3"> Phone:</label>
                                <div  class="col-sm-9">
                                    <input type='text' name='cart_billing[phone]' class="form-control input-medium" id='' value="<?php echo isset($cart_billing['phone']) ? $cart_billing['phone'] : $userInfo->var_phone;?>" required>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">Country:</label>
                                <div  class="col-sm-9">
								<?php 
									$country_id =  isset($cart_billing['country']) ? $cart_billing['country'] : 192;
								?>
                                    <select class="select2me form-control input-xlarge" id="country" name="cart_billing[country]" required="required">
									<?php foreach($countries as $contr){?>
                                            <option value="<?php echo $contr->id; ?>" 
                                                <?php if($contr->id == $country_id) echo 'selected';?> >
                                                	<?php echo $contr->name; ?>
                                            </option>
									<?php }?>
                                    </select>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">State:</label>
                                <div  class="col-sm-9">

                                    <select id="state" class="select2me form-control input-xlarge" name="cart_billing[state]" required>
                                       
                                       <?php 
                                            echo $states;
                                        ?>
                                    </select>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">Zip:</label>
                                <div  class="col-sm-9">
                                    <input type='text' name='cart_billing[zip]' class="form-control input-medium" id='' value='<?php echo isset($cart_billing['zip']) ? $cart_billing['zip'] : $userInfo->var_zip;?>' required>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">City:</label>
                                <div  class="col-sm-9">
                                    <input type='text' name='cart_billing[city]' class="form-control input-medium" id='' value='<?php echo isset($cart_billing['city']) ? $cart_billing['city'] : $userInfo->var_city;?>' required>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">Street:</label>
                                <div  class="col-sm-9">
                                    <input type='text' name='cart_billing[street]' class="form-control input-medium" id='' value='<?php echo isset($cart_billing['street']) ? $cart_billing['street'] : $userInfo->var_address1;?>' required>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            
                            <h4>Payment Method</h4>
                            <div class="payment">
                                <span class="fa fa-cc-mastercard"></span>
                                <span class="fa fa-cc-visa"></span>
                                <span class="fa fa-cc-amex"></span>
                                <span class="fa fa-cc-jcb"></span>
                                <span class="fa fa-cc-discover"></span>
                            </div>
                            <span>Your information is safe and secure</span>
                            <div class="form-group">
                                <label class="control-label col-sm-3">Card Number:</label>
                                <div  class="col-sm-9">
                                    <input type='text' name='cart_billing[cardNumber]' class="form-control input-medium" id='' value='' required>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">Expiration:</label>
                                <div  class="col-sm-9">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <select class="select2me form-control input-xlarge" name="cart_billing[month]" id="" required>
                                                <option class="" value="-1" selected="selected">month</option>
                                                <option class="" value="01">January</option>
                                                <option class="" value="02">February</option>
                                                <option class="" value="03">March</option>
                                                <option class="" value="04">April</option>
                                                <option class="" value="05">May</option>
                                                <option class="" value="06">June</option>
                                                <option class="" value="07">July</option>
                                                <option class="" value="08">August</option>
                                                <option class="" value="09">September</option>
                                                <option class="" value="10">October</option>
                                                <option class="" value="11">November</option>
                                                <option class="" value="12">December</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-6">
                                            <select class="select2me form-control input-xlarge" name="cart_billing[year]" id="" required>
                                                <option class="" value="-1" selected="selected">year</option>
                                                <?php
													$cYear = date('Y');
                                                	for($i=0; $i < 15; $i++){?>
                                                	<option class="" value="<?php echo $cYear+$i;?>"><?php echo $cYear+$i;?></option>
                                                <?php 
													}?>
                                            </select>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">Security Code:</label>
                                <div  class="col-sm-9">
                                    <input type='text' name='cart_billing[security_code]' class="form-control input-medium" id='' value='' required>
                                    <strong>Don’t worry! You will be able to review all the details of your charges before you pay. We just need all this information so we can get you to the next page :)</strong>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                
						<div class="clearfix mt20 actions">
							<a style="float:right;background:#198fef ; float:left; color:#fff; padding:8px 15px; border-radius:3px; text-decoration:none; font-weight:bold;" class="btn-copy" href="<?php echo base_url(); ?>checkout">Back</a>
							<button type="submit" class="btn btn-primary pull-right">Continue</button>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>                
  
			</form>
			</div>
			<!--End of .tab-content--> 
			
		</div>
		<!--End of tabs--> 
	</div>
</div>

<script>

$(document).ready(function(e) {
    
	$("#billing_form").validate({
        rules: {
<?php	foreach($cart_list as $option_id=>$cart){
			foreach($cart as $date=>$details){?>
				// simple rule, converted to {required:true}
				"TravelerInfo[<?php echo $option_id?>][<?php echo $date?>][firstname]": "required",
				"TravelerInfo[<?php echo $option_id?>][<?php echo $date?>][lastname]": "required",
				"TravelerInfo[<?php echo $option_id?>][<?php echo $date?>][phone]": "required",
<?php		}
		}?>
        }
    });
	
});

$('#country').change(function(){
	$('#state').html('');
	$.post("<?php echo base_url(); ?>front/tourico_api/get_cities/"+$(this).val(),
	{
		contryid: $(this).val()
	},
	function(data, status){
		$('#state').html(data);
	});
});

function copyData(itemNo){
	var prev_itemNo = parseInt(itemNo) - 1;
	$('#firstname_'+itemNo).val( $('#firstname_'+prev_itemNo).val() );
	$('#lastname_'+itemNo).val( $('#lastname_'+prev_itemNo).val() );
	$('#phone_'+itemNo).val( $('#phone_'+prev_itemNo).val() );
}
</script>