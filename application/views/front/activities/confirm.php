<style type="text/css">
    .font-clr{
        color: #333 !important;
        font-weight: normal;
    }
</style>
<div class="width-row margn-none">
	<div class="col-md-4 col-sm-4">
    
        <div class="wizWized">
            <div class="sWhead">
                <h4>Reservation Details </h4>
            </div>
            <div class="clearfix">
            <?php
				$overAllTotal = 0;
            	foreach($cart_list as $option_id=>$cart){
                	foreach($cart as $date=>$details){
						$overAllTotal += $details['total_price'];
			?>
                    
                    <div class="media" style="padding:0px 15px;">
                        <a class="pull-left" href="#">
                            <img style="width:100px; margin-bottom: 0px;" class="media-object img-responsive thumbnail" src="<?php echo $details['thumbUrl']?>" alt="image">
                        </a>
                        <div class="media-body">
                            <div class="panel-title">
                                <b><?php echo $details['activityName'];?></b>
                            </div>
                            <address class="hotel_location"><?php echo $details['optionTitle'];?></address>
                        </div>
                    </div>
                    
                    <div class="bookingContent">
                        <ul class="list-unstyled">
                            <li role="separator" class="divider"></li>
                            
                            <?php if( !empty($details['no_of_adults']) ){?>
                            <li><b>No of Adults: </b><span class="room_type_category"><?php echo $details['no_of_adults'];?></span></li>
                            <li><b>No of Children: </b><span class="num_adult_children"><?php echo $details['no_of_childs'];?></span></li>
                            <?php }?>

                            <?php if( !empty($details['no_of_units']) ){?>
                            <li><b>No of Units: </b><span class="room_type_category"><?php echo $details['no_of_units'];?></span></li>
                            <?php }?>

                            <li role="separator" class="divider"></li>
                            <li><b>Date: </b><span><?php echo $date;?></span></li>
                            <li role="separator" class="divider"></li>

                            <?php if( !empty($details['no_of_adults']) ){?>
                            <li class="text"><b>Price (Adult):</b><span id="total_room_cost">$<?php echo $details['adult_price'];?> / person</span></li>
                            <li class="text"><b>Price (Child):</b><span id="total_room_cost">$<?php echo $details['child_price'];?> / child</span></li>
                            <?php }?>

                            <?php if( !empty($details['no_of_units']) ){?>
                            <li class="text"><b>Price (Unit):</b><span id="total_room_cost">$<?php echo $details['unit_price'];?> / unit</span></li>
                            <?php }?>

                            <li role="separator" class="divider"></li>
                            <li class="text-danger"><b>Sub Total:</b><span id="total_price_html">$<?php echo $details['total_price'];?></span></li>
                            <li role="separator" class="divider"></li>
                        </ul>
                    </div>
			<?php	}
				}?>
                <div class="bookingContent">
					<ul class="list-unstyled">
                    	<li class="text-danger"><b>Total:</b><span id="total_price_html">$<?php echo $overAllTotal;?></span></li>
                    </ul>
				</div>    
            </div>
        </div>
    </div>
	<div class="col-md-8 col-sm-8">
		<div> 
			
			<!-- Nav tabs -->
			<ul class="nav nav-tabs c-tabs" role="tablist">
            	<li role="presentation"><a href="<?php echo base_url(); ?>checkout">Traveler Information</a></li>
                <li role="presentation" ><a href="<?php echo base_url(); ?>/front/carts/billing">Billing Information</a></li>
				<li role="presentation" class="active"><a href="#">Review & Confirm</a></li>
			</ul>
			
			<div class="tab-content">
            <form method="post" id="confirmForm">
            	<input type="hidden" value="1" name="submitted" />
				<div role="tabpanel" class="tab-pane active" id="home">
                    
                	<div class="wizTab">
                    	<div role="tabpanel" class="tab-pane active clearfix" id="ti">
							<h4>Review & Confirm</h4>
                            <p>Please review and confirm the details.</p>
                            <?php if( !empty($error) ){?>
                                <div style="color:red;">Alert : <?php echo $error?></div>
                            <?php }?>

                            <?php
                            foreach($cart_list as $option_id=>$cart){
                                foreach($cart as $date=>$details){
                                    if($details['error']){?>
                                        <div style="color:red; font-weight:bold;">Alert : <?php echo $details['error_message']?></div>
                                <?php }
                                }
                            }
                            ?>
                        </div>

		<?php	$totalAmount = 0;
				foreach($cart_list as $option_id=>$cart){
                    foreach($cart as $date=>$details){
						$totalAmount += $details['total_price'];
		?>
						<h3>
                        	<a class="font-clr" data-toggle="modal" href="#packageFullDetails" activity_id="<?php echo $details['activityId']?>" onclick="updateViewContent(<?php echo $details['activityId']?>);"> <?php echo $details['activityName'];?> </a>
                        </h3>
						<div class="pic-row">
							<div class="pic-holder">
								<img src="<?php echo $details['thumbUrl']?>" alt="" />
							</div>
							<div class="pic-text">
								<p><strong><?php echo $details['optionTitle'];?></strong></p>
							</div>
						</div>
                        <hr />
						<div class="">
                       
							<div class="custom-rw">
								<div class="col-md-3"><strong>Activity:</strong></div>
								<div class="col-md-8"><?php echo $details['optionTitle'];?></div>
							</div>
							<div class="custom-rw">
								<div class="col-md-3"><strong>Activity Date:</strong></div>
								<div class="col-md-8"><?php echo date('F, j, Y', strtotime($details['activity_date']));?></div>
							</div>

                            <?php if( !empty($details['no_of_adults']) ){?>
							<div class="custom-rw">
								<div class="col-md-3"><strong>Adults:</strong></div>
								<div class="col-md-8"><?php echo $details['no_of_adults'];?> (Age 11+) &nbsp; Price: $<?php echo number_format($details['adult_price'], 2);?> (per person)</div>
							</div>
                            <div class="custom-rw">
								<div class="col-md-3"><strong>Children:</strong></div>
								<div class="col-md-8"><?php echo $details['no_of_childs'];?> &nbsp; Price: $<?php echo number_format($details['child_price'], 2);?> (per person)</div>
							</div>
							<?php }?>

                            <?php if( !empty($details['no_of_units']) ){?>
                            <div class="custom-rw">
                                <div class="col-md-3"><strong>Units:</strong></div>
                                <div class="col-md-8"><?php echo $details['no_of_units'];?> &nbsp; Price: $<?php echo number_format($details['unit_price'], 2);?> (per unit)</div>
                            </div>
                            <?php }?>

                            <hr />
							
                            <div class="m-b-15">
								<p class="p-m-0"><strong>Cost: <span class="txt-cost">$<?php echo number_format($details['total_price'], 2);?></span></strong></p>
								<p class="p-m-0"><strong class="txt-cancilation">Cancellation Policy:</strong></p>
								<p>
									<?php foreach($details['cancellation_policy'] as $cancellation_policy){ ?>
										<?php echo $cancellation_policy['message'];?><br />
									<?php }?>
								</p>
							</div>
                            
                            <p>This person must redeem this voucher at the time and location of this activity</p>
                            <hr />
							<div class="">
                                <p>
                                    <strong>Name:</strong> 
                                    <?php echo $details['traveler_info']['firstname'].' '.$details['traveler_info']['lastname'] ;?>
                                </p>
                                <p>
                                	<strong>Phone:</strong>
                                    <?php echo $details['traveler_info']['phone'];?>
                                </p>
                            
                            <?php //reference http://doc.touricoholidays.com/activity-online-flow/mapping/activity-additions/
                                $additionTypes = array('Text', 'TrueFalse', 'Numeric', 'NumericRange');
                            ?>

							<?php	if( !empty($details['activity_additions']) ){
                                        foreach($additionTypes as $adt){
                                            foreach( $details['activity_additions'][$adt] as $additionTypeID=>$actAdd){ ?>
                            				<p>
                                                <strong> <?php echo $actAdd['additionType'];?> :</strong>
                                                <?php echo $actAdd['value'];?>
                                            </p>	
                            <?php		     }
                                        }
                                    }?>
                            
                            <?php   if( !empty($details['passenger_additions']) ){
                                        foreach($additionTypes as $adt){
                                            foreach( $details['passenger_additions'][$adt] as $additionTypeID=>$actAdd){ ?>
                                            <p>
                                                <strong> <?php echo $actAdd['additionType'];?> :</strong>
                                                <?php echo $actAdd['value'];?>
                                            </p>    
                            <?php            }
                                        }
                                    }?>
                            
                                <p>
                                    <strong>Special Requests (not guaranteed):</strong>
                                    <?php echo $details['traveler_info']['special_request'];?>
                                </p>
                            </div>
                            
						</div>
        <?php		}
                }?>
                        <hr />
                		<h4>Summary of Billing Information</h4>

                        <ul class="list-unstyled">
                            <li class="text-danger">Total Cost:<span><strong>$<?php echo number_format($totalAmount, 2);?></strong></span></li>
    <!--                                    <li>Your credit card will be charged on the following date:<span><strong>Dec 23, 2015</strong></span></li>-->
                            <li role="separator" class="divider"></li>
                            <li>Name:<span> <?php echo ucfirst($cart_billing['firstname']).' '.$cart_billing['lastname'] ?></span></li>
    <!--                                    <li>Credit Card:<span> Mastercard</span></li>-->
                            <li>Credit Card Number:<span>  <?php echo "**** **** **** ".substr($cart_billing['cardNumber'], -4); ?></span></li>
                            <li>Expiration:<span>  <?php echo $cart_billing['month'].', '.$cart_billing['year']?></span></li>
                            <li>Address:<span> <?php echo $cart_billing['street'].', '.$cart_billing['city'] .' '.get_country_name_by_id($cart_billing['country'])?></span></li>
                            <li>Phone:<span>   <?php echo $cart_billing['phone'] ?></span></li>
                        </ul>
                
					    <hr />
                        <iframe width="100%" style="border:1px solid #E5E5E5;" src="<?php echo base_url(); ?>terms.html"></iframe>
                        <iframe width="100%" style="border:1px solid #E5E5E5;" src="<?php echo base_url(); ?>terms1.html"></iframe>
                        <form method="post" action="<?php echo base_url() ?>request_quote?stp=cnfrm" id="quote_request" class="form-horizontal form-bordered">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" value="1" name="terms" required>
                                    I have read and agree with the Terms and Conditions as well as the FTC disclosure. 
                                </label>
                            </div>
	                    <div class="clearfix mt20 actions">
							<a style="float:right;background:#198fef ; float:left; color:#fff; padding:8px 15px; border-radius:3px; text-decoration:none; font-weight:bold;" class="btn-copy" href="<?php echo base_url(); ?>front/carts/billing">Back</a>
							<button type="button" class="btn btn-primary pull-right" onclick="$('#confirmForm').submit();">Continue</button>
						</div>
						<div class="clearfix"></div>
                        
                    </div>
					
				</div>
                <!--End of tab 1-->
			</form>
				
			</div>
			<!--End of .tab-content--> 
			
		</div>
		<!--End of tabs--> 
		
	</div>
	
</div>