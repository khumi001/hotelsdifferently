<div class="modal fade" id="packageFullDetails">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Package Details</h4>
            </div>

			<div class="lowerModalBody" id="packageInfo" style="margin-top: 0px;">
                    
                <div role="tabpanel">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs nav-justified nav-pills smnav" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#" data-target="#overView" aria-controls="overView" role="tab" data-toggle="tab">Detail</a>
                        </li>
                        <li role="presentation">
                            <a href="#" data-target="#pictures" aria-controls="pictures" role="tab" data-toggle="tab">Pictures</a>
                        </li>
                        <li role="presentation">
                            <a href="#" data-target="#video" aria-controls="video" role="tab" data-toggle="tab">Video</a>
                        </li>
                        <li role="presentation">
                            <a href="#" data-target="#map" aria-controls="map" role="tab" data-toggle="tab">Map</a>
                        </li>
                        <li role="presentation">
                            <a href="#" data-target="#sChart" aria-controls="sChart" role="tab" data-toggle="tab">Seating</a>
                        </li>
                    </ul>
                    
                    <!-- Tab panes -->
                    <div class="tab-content">

                        <div role="tabpanel" class="tab-pane fade in active" id="overView">
                            <div class="info">

                                <div class="clearfix">
                                    <div class="col-xs-12">
                                        <h4 class="info-title">Highlights / General details</h4>
                                        <p class="info-para" id="highlights_general_details">
                                            Who would you like to meet at the World Famous Madame Tussauds Las Vegas?
                                            Featuring lifelike wax creations of your favorite celebrities Madame Tussauds lets you
                                            “grab hold” of your favorite stars. Go on tour with “Gwen Stefani”.
                                            Step inside our UFC® Octagon™ with "Chuck ‘Iceman’ Liddell". Meet your favorite housewife
                                            “Eva Longoria”. Don the signature bunny ears, and party with “Hef”! Test your basketball
                                            skills by going head-to-chest in our Dunk on “Shaq” experience. Chill with the “Boss Dogg”
                                            and sit in Snoop’s throne. Have your mind freaked by “Criss Angel.” With over 100 lifelike celebrities,
                                            you’ll have the pictures to show everyone you hung out with stars while in Vegas.
                                        </p>
                                    </div>
                                </div>

                                <div class="clearfix">
                                    <div class="col-sm-6">
                                        <h4 class="info-title">Inclusions</h4>
                                        <p class="info-para" id="inclusions">
                                            General Admission – First come first served seat, according to arrival for show
                                            VIP Ticket – Assigned seat at the front of the showroom with the best views for the shows.
                                            Balcony
                                        </p>						
                                    </div>
                                    <div class="col-sm-3">
                                        <h4 class="info-title">Exclusion</h4>
                                        <p class="info-para" id="exclusion">
                                            General Admission – First come first served seat, according to arrival for show
                                            VIP Ticket – Assigned seat at the front of the showroom with the best views for the shows.
                                            Balcony
                                        </p>
                                    </div>
                                    <div class="col-sm-3">
                                        <h4 class="info-title">Duration</h4>
                                        <p class="info-para" id="duration">Approximately 75 minutes.</p>
                                    </div>
        
                                </div>

                                <div class="clearfix">
                                    <div class="col-md-12">
                                        <h4 class="info-title">Hours of Operation</h4>
                                        <p class="info-para" id="hour_of_operation"></p>

                                        <h4 class="info-title">Location</h4>
                                        <p class="info-para" id="location_text">
                                            Address: 3545 Las Vegas Boulevard South, Las Vegas 89109, located at the east end of The LINQ Promenade.
                                        </p>

                                        <h4 class="info-title">Additional Information</h4>
                                        <p class="info-para">
                                            The High Roller has a complimentary valet available adjacent to the ticketing window. There is complementary parking provided in the High Roller parking lot located east of the attraction.
                                            High Roller is ADA and wheelchair accessible.
                                        </p>

                                        <h4 class="info-title">Policies</h4>
                                        <p class="info-para" id="policies">
                                            Complete information, including local telephone numbers at your destination, will be included on 
                                            your Confirmation Voucher.
                                        </p>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="pictures">
                            <div class="clearfix info">
                                <div class="clearfix">
                                    <div class="col-sm-5">
                                        <div id="slider">
                                            <!-- Top part of the slider -->
                                            <div class="row">
                                                <div class="col-sm-12" id="carousel-bounding-box">
                                                    <div class="carousel slide" id="slPicture">
                                                        <!-- Carousel items -->
                                                        <div class="carousel-inner" id="image_sliding">
                                                            <div class="active item" data-slide-number="0"><img src="http://placehold.it/470x480&text=zero"></div>
                                                            <div class="item" data-slide-number="1"><img src="http://placehold.it/470x480&text=1"></div>
                                                            <div class="item" data-slide-number="2"><img src="http://placehold.it/470x480&text=2"></div>
                                                            <div class="item" data-slide-number="3"><img src="http://placehold.it/470x480&text=3"></div>
                                                            <div class="item" data-slide-number="4"><img src="http://placehold.it/470x480&text=4"></div>
                                                            <div class="item" data-slide-number="5"><img src="http://placehold.it/470x480&text=5"></div>
                                                            <div class="item" data-slide-number="6"><img src="http://placehold.it/470x480&text=6"></div>
                                                            <div class="item" data-slide-number="7"><img src="http://placehold.it/470x480&text=7"></div>
                                                            <div class="item" data-slide-number="8"><img src="http://placehold.it/470x480&text=8"></div>
                                                            <div class="item" data-slide-number="9"><img src="http://placehold.it/470x480&text=9"></div>
                                                            <div class="item" data-slide-number="10"><img src="http://placehold.it/470x480&text=10"></div>
                                                        </div>
                                                        <!-- Carousel nav -->
                                                        <a class="left carousel-control" href="#slPicture" role="button" data-slide="prev">
                                                            <span class="glyphicon glyphicon-chevron-left"></span>
                                                        </a>
                                                        <a class="right carousel-control" href="#slPicture" role="button" data-slide="next">
                                                            <span class="glyphicon glyphicon-chevron-right"></span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-7 hidden-xs activityGallery" id="slider-thumbs">
                                        <!-- Bottom switcher of slider -->
                                        <ul class="list-unstyled" id="slide_switcher">
                                            <li class="col-sm-3">
                                                <a class="thumbnail" id="carousel-selector-0"><img src="http://placehold.it/150x150&text=zero"></a>
                                            </li>
                                            <li class="col-sm-3">
                                                <a class="thumbnail" id="carousel-selector-1"><img src="http://placehold.it/150x150&text=1"></a>
                                            </li>
                                            <li class="col-sm-3">
                                                <a class="thumbnail" id="carousel-selector-2"><img src="http://placehold.it/150x150&text=2"></a>
                                            </li>
                                            <li class="col-sm-3">
                                                <a class="thumbnail" id="carousel-selector-3"><img src="http://placehold.it/150x150&text=3"></a>
                                            </li>
                                            <li class="col-sm-3">
                                                <a class="thumbnail" id="carousel-selector-4"><img src="http://placehold.it/150x150&text=4"></a>
                                            </li>
                                            <li class="col-sm-3">
                                                <a class="thumbnail" id="carousel-selector-5"><img src="http://placehold.it/150x150&text=5"></a>
                                            </li>
                                            <li class="col-sm-3">
                                                <a class="thumbnail" id="carousel-selector-6"><img src="http://placehold.it/150x150&text=6"></a>
                                            </li>
                                            <li class="col-sm-3">
                                                <a class="thumbnail" id="carousel-selector-7"><img src="http://placehold.it/150x150&text=7"></a>
                                            </li>
                                            <li class="col-sm-3">
                                                <a class="thumbnail" id="carousel-selector-7"><img src="http://placehold.it/150x150&text=8"></a>
                                            </li>
                                            <li class="col-sm-3">
                                                <a class="thumbnail" id="carousel-selector-7"><img src="http://placehold.it/150x150&text=9"></a>
                                            </li>
                                            <li class="col-sm-3">
                                                <a class="thumbnail" id="carousel-selector-7"><img src="http://placehold.it/150x150&text=10"></a>
                                            </li>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div role="tabpanel" class="tab-pane fade" id="video">
                           <div class="clearfix">
                            <div class="col-md-9">
                            <h4 class="vtitle" id="video_title">Video title of the video</h4>
                                <div class="embed-responsive embed-responsive-16by9" id="youtubeVideoLink">
                                  <iframe class="embed-responsive-item"  width="auto" height="auto" src="https://www.youtube.com/embed/P0N2RKVp6VE" frameborder="0" allowfullscreen></iframe>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <ul class="list-unstyled videoG">
                                    <li><img src="http://i1.ytimg.com/vi/mj5Q2rS9Zyk/default.jpg" class="img-responsive" alt="Image"></li>
                                    <li><img src="http://i1.ytimg.com/vi/mj5Q2rS9Zyk/default.jpg" class="img-responsive" alt="Image"></li>
                                    <li><img src="http://i1.ytimg.com/vi/mj5Q2rS9Zyk/default.jpg" class="img-responsive" alt="Image"></li>
                                </ul>
                            </div>
                           </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="map">
                            map
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="sChart">
                            <div class="clearfix" id="schartOptionSelect">
                                <div class="form-group col-md-6">
                                    <span>Select available date:</span>
                                    <select class="select2me form-control" name="" id=" ">
                                        <option value="12/14/2015 12:00:00 AM">Dec 14 (Mon)</option>
                                        <option value="12/15/2015 12:00:00 AM">Dec 15 (Tue)</option>
                                        <option value="12/16/2015 12:00:00 AM">Dec 16 (Wed)</option>
                                        <option value="12/17/2015 12:00:00 AM">Dec 17 (Thu)</option>
                                        <option value="12/18/2015 12:00:00 AM">Dec 18 (Fri)</option>
                                        <option value="12/21/2015 12:00:00 AM">Dec 21 (Mon)</option>
                                    </select>
                                </div>

                                <div class="form-group col-md-3">
                                    <span>Adult(12+)</span>
                                    <select class="select2me form-control" name="" id=" ">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                        <option value="13">13</option>
                                        <option value="14">14</option>
                                        <option value="15">15</option>
                                        <option value="16">16</option>
                                        <option value="17">17</option>
                                        <option value="18">18</option>
                                        <option value="19">19</option>
                                        <option value="20">20</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-3">
                                    <span>Child(5-12)</span>
                                    <select class="select2me form-control" name="" id=" ">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                        <option value="13">13</option>
                                        <option value="14">14</option>
                                        <option value="15">15</option>
                                        <option value="16">16</option>
                                        <option value="17">17</option>
                                        <option value="18">18</option>
                                        <option value="19">19</option>
                                        <option value="20">20</option>
                                    </select>
                                </div>
                            </div>
                            <div class="clearfix" id="schartOption">
                                <ul class="nav nav-pills nav-stacked col-sm-2">
                                    <li class="active"><a data-toggle="pill" href="#c1">Category A</a></li>
                                    <li><a data-toggle="pill" href="#c2">Category B</a></li>
                                    <li><a data-toggle="pill" href="#c3">Category C</a></li>
                                    <li><a data-toggle="pill" href="#c4">Category C</a></li>
                                  </ul>
                                  
                                  <div class="tab-content">
                                    <div id="c1" class="tab-pane fade in active">
                                        <center>
                                            <img src="http://image1.urlforimages.com/1295505/12320982-KA.png" class="img-responsive" alt="Image">
                                        </center>
                                     </div>

                                    <div id="c2" class="tab-pane fade">
                                        <center>
                                            <img src="http://image1.urlforimages.com/1295505/12320982-KA.png" class="img-responsive" alt="Image">
                                        </center>
                                     </div>

                                    <div id="c3" class="tab-pane fade">
                                        <center>
                                            <img src="http://image1.urlforimages.com/1295505/12320982-KA.png" class="img-responsive" alt="Image">
                                        </center>
                                     </div>

                                    <div id="c4" class="tab-pane fade">
                                        <center>
                                            <img src="http://image1.urlforimages.com/1295505/12320982-KA.png" class="img-responsive" alt="Image">
                                        </center>
                                     </div>

                                  </div>
                            </div>
                        </div>

                    </div>
                </div>
                    
<!-- 			<div class="clearfix">
                    <div class="panel panel-default cbg">
                        <div class="panel-heading">
                            <h3 class="panel-title bold">Activity Cart <span class="fa fa-shopping-cart fa-lg"></span></h3>
                        </div>
                        <div class="panel-body">
                            <span>Cart is Empty</span>
                            <ul class="list-unstyled cart-list">
                                <li>
                                    <div class="cart-detail">
                                        <h5><a href="">The High Roller at The LINQ </a></h5>
                                        <h5>Cost: $450.00</h5>
                                        <h5><a data-toggle="modal" href="#cartDetails">Details</a> <span>|</span> <a href="" class="text-danger">Remove</a></h5>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
            	</div> -->

			</div>

		</div>
	</div>
</div>