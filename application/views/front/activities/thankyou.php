<div class="width-row">
    <div class="tab-content wizTab " id="traveller_information_body">
	<?php	if(isset($success)){?>
                <div class="alert alert-success">
                  Good job! Your booking has been successfully made and we forwarded it to the appropriate tour operators and/or service providers.
                </div>
                
                <p style="color:#F00; font-weight:bold;">
                	<span style="text-decoration:underline;">IMPORTANT :</span> This page contains your VOUCHER and you need to bring it with you to your Activity as it can be requested where the service takes place!
				</p>
                <p>
                    Your payment of <strong>$<?php echo $activity_booking->total_amount;?></strong> was successfully charged and your charges will appear as <span style="color:#F00; font-weight:bold;">“HOTELSDIFF”</span> or <span style="color:#F00; font-weight:bold;">“HOTELSDFRNTL8882872307”</span> on your statement.
                </p>
				<p>
                    If your booked activity is in <strong>less than 3 days</strong>, then your booking is sent to the tour operators/service providers immediately but every tour operator/service provider has different ways and timeframes for processing incoming reservations and they usually prioritize incoming bookings by excursion date. Therefore if your excursion date is for a future date, your booking <strong>might</strong> not show up in their system for <strong>up to 5 days</strong> from your booking.
				</p>
                <br />
				<p>                    
                    Should you encounter any problems with this reservation, please send us a message immediately via the <strong>"Contact Us"</strong> page and select <strong>"Problems with booking"</strong> so that we can rectify any problems you may have experienced. Those emails enjoy high priority and you can expect a very fast reply. Alternatively, you are also welcome to call us at <strong>888-287-2307</strong> and our customer service representatives will be happy to assist you <strong>24 hours a day, 7 days a week, 365 days a year</strong>.
                </p>
    <?php	}?>
        
        
        <div class="media" style="padding:0px 15px; text-align:center;">
            <strong>Download your invoice here:</strong>
        </div>
        
        <div class="text-center" style="text-align:center;">
            <?php foreach($activity_bookings_items as $item){?>
                <a  target="_blank" href="<?php echo base_url(); ?>front/carts/pdf/<?php echo $item->id; ?>"><img src="http://iconbug.com/data/5b/507/52ff0e80b07d28b590bbc4b30befde52.png" width="50" height="50" /></a>
            <?php }?>
            
        </div>
        <p style="border:1px solid #000; margin:15px 0;"></p>
		
		<p><b style="font-size:14px; display:block; text-align:center;">NEED A TICKET FOR AN EVENT OR CONCERT?</b></p>
							<p>We are proud to be affiliated with TicketLiquidator which is one of the biggest names in the event ticket industry. On many occasions, you can obtain tickets for events that are even sold out. Type in a Venue, an Event or a Performer and see how much you can save!</p>
							
							 <script type="text/javascript" src="https://tickettransaction2.com/Brokers/01504-011/WidgetJS/scriptsretail-stringutils.js"></script> <script type="text/javascript">var widgetCSS=document.createElement("link"); widgetCSS.setAttribute("rel", "stylesheet"); widgetCSS.setAttribute("type", "text/css"); widgetCSS.setAttribute("href", "https://tickettransaction2.com/Brokers/01504-011/WidgetCSS/Style.css"); document.getElementsByTagName("head")[0].appendChild(widgetCSS); </script><p></p>
<div id="TL_Search_Widget" style="width: 100%; border: 1px solid rgb(218, 212, 200);">
<div id="tlsw_header"> <span>Find Tickets</span><input id="tlsw_searchTxt" value="Search by Artist or Event" onblur="if(this.value=='') this.value=this.defaultValue;" onfocus="if(this.value==this.defaultValue) this.value='';" type="text"><p></p>
<div id="tlsw_searchBtn" style="background-image: url(&quot;//s3.amazonaws.com/ticketliquidator/affiliates/widgetMaker/SearchWidget/green_btn.png&quot;);" onclick="tn_SubmitSearch()"> <script type="text/javascript">function tn_KeyDownHandler(e){if (e==null) e=window.event; if (e.keyCode==13) tn_SubmitSearch();}document.getElementById('tlsw_searchTxt').onkeypress=tn_KeyDownHandler; </script> </div>
</div>
<ul id="tlsw_content" class="suggestionsArea"></ul>
<div id="tlsw_scripts"><script type="text/javascript">function tn_fill_top_events() {var tlsw_content = new Array();for (var tn_counter = 0; tn_counter < tn_top_performers.length; tn_counter++)tlsw_content.push('<li class="tlsw_suggestion" onmouseover="this.style.backgroundPosition=\'bottom right\';" onmouseout="this.style.backgroundPosition=\'top right\';" style="background-image:url(http:\/\/content.tl-static.com/affiliates/widgetMaker/SearchWidget/gray-bg.png);"><a target= "_blank"href="//www.jdoqocy.com/click-8137399-10858765?sid=&amp;url=http:\/\/www.ticketliquidator.com/tix/' + Fixer(tn_top_performers[tn_counter]) + '-tickets.aspx"><span class="tlsw_linktext">' + tn_top_performers[tn_counter] + '</span></a></li>');document.getElementById('tlsw_content').innerHTML = tlsw_content.join('');}</script><script type="text/javascript" src="//tickettransaction.com/?bid=1&amp;tid=top_sellers&amp;javaarray=true&amp;listsize=5"></script><script type="text/javascript">function tn_SubmitSearch() {var SearchPhrase = document.getElementById('tlsw_searchTxt').value.trim().replace(/\s/g, '-');window.open('//www.jdoqocy.com/click-8137399-10858765?sid=&amp;url=' + 'http://www.ticketliquidator.com/tix/' + SearchPhrase + '-tickets.aspx');}</script></div>
<div id="tlsw_footer"> <a id="tlsw_footer_link" href="//www.jdoqocy.com/click-8137399-10858765?sid=&amp;url=http://www.ticketliquidator.com/default.aspx" target="_blank"> <img src="//s3.amazonaws.com/ticketliquidator/affiliates/widgetMaker/SearchWidget/logo-white.png"> </a> </div>
</div>
		
        <p style="border:1px solid #000; margin:15px 0;"></p>
        
        <p><b style="font-size:14px; display:block; text-align:center;">NEED INSURANCE?</b></p>
        <p>We (<strong>HotelsDifferently</strong><sup>sm</sup>) do NOT offer travel insurance, however you can purchase travel insurance from a third party insurance company. Please note that we do not directly sell any types of insurance; we are solely an Affiliate to a third party insurance company.</p>
        
        <p style="text-align:center;">
            <a style="display:inline-block; vertical-align:top; max-width:200px;" href="http://www.dpbolvw.net/click-8137399-10892804-1466615700000" target="_blank">
                <img style="display:block; width:100%; height:auto;" src="http://www.ftjcfx.com/image-8137399-10892804-1466615700000" width="125" height="125" alt="Allianz Travel Insurance" border="0"/>
            </a>
            <a style="display:inline-block; vertical-align:top; max-width:200px;" href="http://www.jdoqocy.com/click-8137399-11779657-1466617745000" target="_blank">
                <img style="display:block; width:100%; height:auto;" src="http://www.lduhtrp.net/image-8137399-11779657-1466617745000" width="300" height="250" alt="Allianz Travel Insurance" border="0"/>
            </a>
            <a style="display:inline-block; vertical-align:top; max-width:200px;" href="http://www.tkqlhce.com/click-8137399-11176385-1466616611000" target="_blank">
                <img style="display:block; width:100%; height:auto;" src="http://www.tqlkg.com/image-8137399-11176385-1466616611000" width="300" height="250" alt="" border="0"/>
            </a>
        </p>
        <p style="border:1px solid #000; margin:15px 0;"></p>
        
        <h3 style="font-size:17px; margin:0 0 10px; font-weight:bold; text-align:center; color:#000;">NEED A RIDE?</h3>
        
        <h3 style="font-size:14px; margin:0 0 10px; font-weight:bold; text-align:center;color:#e00303;"><a href="https://www.uber.com/invite/8z1j5" target="_blank"><img src="<?php echo base_url(); ?>public/assets/img/uber-logo.png" alt="uber" ></a></h3>
        <h2 style="font-size:14px; margin:0 0 10px; font-weight:bold; text-align:center;color:#000;">
    		<span style="color:#e00303;">GET $15 OFF OR MORE</span> depending on your location by signing up through the banner above or enter Promo Code <span style="color:#e00303;">8z1j5</span> in the app!
        </h2>
        <!--<p style="margin:0 0 15px; text-align:center;"><a style="color:#e00303;" href="https://www.uber.com/invite/8z1j5">SIGNUP FOR UBER HERE!  </a></p>-->
        
        
        <h3 style="font-size:14px; margin:0 0 10px; font-weight:bold; text-align:center;color:#e00303;"><a href="https://www.lyft.com/invite/SZILARD527631" target="_blank"><img src="<?php echo base_url(); ?>public/assets/img/lyft-logo.png" alt="LYFT" ></a></h3>
        <h2 style="font-size:14px; margin:0 0 10px; font-weight:bold; text-align:center;color:#000;">GET <span style="color:#e00303;">$50 OFF</span> towards your first rides by signing up through the banner above!</h2>
        
        
	</div>
</div>