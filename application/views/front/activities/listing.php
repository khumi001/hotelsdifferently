<style type="text/css">
.ASelector div > span {
	float: left;
	margin-right: 10px;
}
.catTitle {
    background: rgba(10, 79, 159, 0.9) none repeat scroll 0 0;
    border-radius: 3px;
    color: white;
    display: block;
    font-family: "FuturaStd-Heavy";
    font-size: 20px;
    font-weight: normal;
    height: 40px;
    line-height: 40px;
    margin-bottom: 0;
    padding-left: 20px;
}
.more_listing_detail {
	border: solid 1px white;
	margin-top: 9px;
	float: right;
	padding: 7px 10px;
	background: white;
}
.activityDetails {
	min-height: 125px;
	overflow: hidden;
}
.affix {
	position: fixed;
	top: 0;
	width: auto;
	z-index: 9997;
}
#myCart.panel-body {
	padding: 15px;
	width:302px;
}
.panel-body {
	padding-bottom: 0;
}
.choice.clearfix {
	padding: 0;
}
.choice.clearfix h5 {
	margin-top: 0;
}
.panel-footer {
	padding-bottom: 0;
	padding-top: 0;
}
</style>
<?php







$categories = $activities;



$categories_count = count($activities);







$categories_left_panel_html = '';



$categories_options_html = '';


$total_activities_count = 0;


for ($i = 0; $i < $categories_count; $i++) {

	$cat_activities_count = count($categories[$i]['activity']);
	$total_activities_count += $cat_activities_count;

	$a_category = $categories[$i];
	$category_name = $a_category['category_name'];
	$category_id = $a_category['category_id'];

    $categories_left_panel_html .= '
		<tr>
			<td style="width:10%;">
				<input class="categoryFiltersCheckbox" id="'.$category_id.'" name="" type="checkbox" value="'.$category_id.'" onclick="filter_results();" checked="checked">
			</td>

			<td style="width:15%; font-weight:bold;">
				('.$cat_activities_count.')
			</td>

			<td>
				<label for="'.$category_id.'">'.$category_name.'</label>
			</td>
		</tr>';

	$categories_options_html .= '<option value="'.$category_id.'">'.$category_name.'</option>'; 

}
?>

<div class="width-row">
	<div class="clearfix">
		<div class="col-sm-4">
			<form class="" action="<?php echo base_url(); ?>activities-listing" method="get" id="carsForm" >
				<div class="sWidgets">
					<div class="clearfix">
						<div class="form-group">
							<label class="control-label col-md-12">Where are you going?</label>
							<div class="col-md-12">
								<input type="text" name="activityLocationName" id="activityLocationName" class="form-control" style="height: 33px; text-align: center;" placeholder="Destination" value="<?php echo $activityLocationName;?>">
								<input type='hidden' name='activityLocationId' id='activityLocationId' value="<?php echo $activityLocationId;?>" />
							</div>
						</div>
					</div>
					<div class="clearfix">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label col-md-12">From</label>
								<div class="">
									<input type="text" name="activity_date_from" placeholder="From" class="form-control icon-pick activity_date_from" id="activity_date_from" style="text-align: left;" value="<?php echo $activity_date_from;?>">
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label col-md-12">To</label>
								<div class="">
									<input type="text" name="activity_date_to" placeholder="To" class="form-control icon-pick activity_date_to" id="activity_date_to" style="text-align: left;" value="<?php echo $activity_date_to;?>">
								</div>
							</div>
						</div>
					</div>
					<div class="clearfix">
						<div class="form-group">
							<label class="control-label col-md-12">Activity Name</label>
							<div class="col-md-12">
								<input type="text" name="activity_name" id="activity_name" class="form-control" placeholder="Activity name" value="<?php echo $activity_name;?>" onkeyup="filter_results();" />
							</div>
						</div>
					</div>
					<div class="clearfix">
						<button type="submit" class="btn .btn-custom-m .btn-custom search_hotel_with_all_options" style="background: #009688;border: 1px solid #009688;color:  #fff; float:right; margin-top:10px; margin-right:15px">Search</button>
					</div>
				</div>
			</form>
			<div class="panel panel-default cbg">
				<div class="panel-heading">
					<h3 class="panel-title bold">Filter Results</h3>
				</div>
				<div class="panel-body">
					<div id="LocationAndLandmarks">
						<div id="activityFilterContent">
							<div id="category" class="clearfix bt1" style="padding: 0px 0px; margin-top:0px; border:0">
								<div id="brandFilterContent"> 
									<!--<h4 class="tc1">Category</h4>-->
									<div class="ASelector" style="float: left;"> <span> <a style="cursor:pointer;" class="hotel_search_select_all" onclick="check_all(1)">Select All</a> </span> <span class="">|</span> <span> <a style="cursor:pointer;" class="" onclick="check_all(0)">Clear</a> </span> </div>
									<table class="table" id="categoryFilters">
										<tbody>
											<?php echo $categories_left_panel_html?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="panel panel-default cbg fixedPosition">
				<div class="panel-heading">
					<h3 class="panel-title bold">Cart <span class="fa fa-shopping-cart fa-lg"></span></h3>
				</div>
				<div class="panel-body" id="myCart">
					<?php	if( empty($cart_list) ){?>
					<span>Cart is Empty</span>
					<?php	}else{?>
					<ul class="list-unstyled cart-list">
						<?php foreach($cart_list as $opt_id=>$cart){
								
                                    foreach($cart as $date=>$details){?>
						<li>
							<div class="cart-detail" id="cart_<?php echo $opt_id;?>_<?php echo $date;?>">
								<h5> <a data-toggle="modal" href="#packageFullDetails" activity_id="<?php echo $details['activityId']?>" onclick="updateViewContent(<?php echo $details['activityId']?>);"> <?php echo $details['activityName'];?> </a> </h5>
								<h5>Package: <?php echo $details['optionTitle'];?></h5>
								<h5>Cost: $<?php echo $details['total_price'];?></h5>
								<h5> <a data-toggle="modal" href="#cartDetails_<?php echo $opt_id;?>_<?php echo $date;?>">Details</a> <span>|</span> <a href="javascript:;" onclick="removeItemFromCart('<?php echo $opt_id;?>', '<?php echo $date;?>')" class="text-danger">Remove</a> </h5>
								<div class="modal fade" id="cartDetails_<?php echo $opt_id;?>_<?php echo $date;?>">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
												<h4 class="modal-title"><?php echo $details['activityName'];?></h4>
											</div>
											<div class="modal-body">
												<ul class="list-unstyled pdetail">
													<h4 style=" margin: -5px 0px 5px 5px;"> <?php echo $details['optionTitle'];?> </h4>
													<li> <strong>Date Selected: </strong> <?php echo $details['activity_date'];?> </li>

													<?php if( !empty($details['no_of_units']) ){?>
																	<li> <strong>No of Units: </strong> <?php echo $details['no_of_units'];?> </li>
																	<li> <strong>Price (Unit): </strong> $<?php echo $details['unit_price'];?> / unit </li>
													<?php }else{?>
																	<li> <strong>No of Adults: </strong> <?php echo $details['no_of_adults'];?> </li>
																	<li> <strong>No of Children: </strong> <?php echo $details['no_of_childs'];?> </li>
																	<li> <strong>Price (Adult): </strong> $<?php echo $details['adult_price'];?> / person </li>
																	<li> <strong>Price (Child): </strong> $<?php echo $details['child_price'];?> / person </li>
													<?php }?>

													<li> <strong>Price (Total): </strong> $<?php echo $details['total_price'];?> </li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
						</li>
						<?php	} 

                                }?>
					</ul>
					<div> <a href="<?php echo base_url('checkout'); ?>" class="btn" style="background: #009688;border: 1px solid #009688;color:  #fff; float:right; margin-top:10px; margin-right:15px;">Checkout</a> </div>
					<?php	}?>
				</div>
			</div>
		</div>
		<div class="col-sm-8">
			<div class="row sResult">
				
				<!--<div class="" style="overflow:hidden;">
					<div class="col-md-6">
						<div class="hotel_serch_tab_btn active">
							<a href="#" style="padding:0;">Activity search result</a>
						</div>
					</div>
					<div class="col-md-6">
						<div class="hotel_serch_tab_btn">
							<a href="#">Activity map view</a>
						</div>
					</div>
				</div>-->

				<div class="panel panel-info" id="sort">
					<div class="panel-heading clearfix">
						<div class="panel-title pull-left">
							<h4>Activities in <?php echo $activityLocationName;?></h4>
						</div>
						<div class="panel-option pull-right"></div>
					</div>
					<div class="panel-body">
						<div class="clearfix">
							<ul class="list-unstyled list-inline arfilter">
								<?php /*?><div class="col-sm-4">
											<li style="margin: 0px 0px 0px 0px;">
												<select style='text-align: center;'class="select2me form-control" name=''>
                                                	<?php echo $categories_options_html;?>
												</select>
											</li>
										</div><?php */?>
								<div class="col-sm-4">
									<li><strong>From:</strong><?php echo $activity_date_from;?></li>
								</div>
								<div class="col-sm-4">
									<li><strong>To:</strong><?php echo $activity_date_to;?></li>
								</div>
								<div class="col-sm-4" style="padding-left: 0;">
									<li class=""> <strong>Results:</strong> <span id="visibleDivcount"><?php echo $total_activities_count;?></span> </li>
								</div>
							</ul>
						</div>
					</div>
				</div>
				<?php
                        // Category loop
                        foreach($categories as $a_category){
							//echo '<pre>';print_r($a_category);exit;
							$category_name = $a_category['category_name'];
							$category_id = $a_category['category_id'];
                            $category_count = count($a_category);
                            $activities = !empty($a_category['activity']) ? $a_category['activity'] : array();
                            $activity_count = count($activities);

                            foreach($activities as $an_activity){
								//echo '<pre>';print_r($an_activity);exit;
                                $name = $an_activity['name'];
                                $desc = $an_activity['description'];
                                $currency = $an_activity['currency'];
                                //$maxChildAge = $an_activity['maxChildAge'];
                                $thumbUrl = $an_activity['thumbURL'];
                                $activity_id = $an_activity['activityId'];
                                $availabilities = !empty($an_activity['activity']) ? $an_activity['activity'] : array();
                                $available_date = "";
								$available_dates = array();
                                $availability_desc = "";
                                $max_adult = "";
                                $max_child = "";

                                foreach($availabilities as $an_availability){

                                    $availability_name = $an_availability['name'];
                                    $optionId = $an_availability['optionId'];
                                    $availability = $an_availability['availability'];
                                    $from_date = $availability['fromDate'];
                                    $to_date = $availability['toDate'];

																		$available_dates[$from_date] = $from_date;
																		$cdate = $from_date;
																		while($cdate < $to_date){
																			$cdate = date('Y-m-d', strtotime('+1 day', strtotime($cdate)));
																			$available_dates[$cdate] = $cdate;	
																		}
																		

									//$available_date .= '<option value="'.$from_date.'">'.$from_date.'</option>';
                                    $max_adult = '';//$availability['maxAdults'];
                                    $max_child = '';//$availability['maxChildren'];
                                    $max_units = '';//$availability['maxUnits'];

                                    if($availability['maxAdults'] > 0){
	                                    for($l=0; $l<=$availability['maxAdults']; $l++){
	                                        $max_adult .= '<option value="'.$l.'">'.$l.'</option>';
	                                    }
	                                  }

                                    if($availability['maxUnits'] > 0){
                                    	for($l=0; $l<=$availability['maxUnits']; $l++){
	                                        $max_units .= '<option value="'.$l.'">'.$l.'</option>';
	                                    }	
                                    }
                                    
                                    if($availability['maxChildren'] > 0){
	                                    for($l=0; $l<=$availability['maxChildren']; $l++){
	                                        $max_child .= '<option value="'.$l.'">'.$l.'</option>';
	                                    }
	                                  }

                                    $adult_price = $availability['adultPrice'];
                                    $child_price = $availability['childPrice'];
                                    $unit_price = $availability['unitPrice'];

                                    $availability_desc .= '
                                        <tr>
                                            <td>
                                                <span>'.$availability_name.'</span>
												<input type="hidden" id="adult_price_'.$optionId.'" value="'.$adult_price.'" />
												<input type="hidden" id="child_price_'.$optionId.'" value="'.$child_price.'" />
												<input type="hidden" id="unit_price_'.$optionId.'" value="'.$unit_price.'" />
												<input type="hidden" id="option_title_'.$optionId.'" value="'.$availability_name.'" />
                                            </td>';

                                     if( !empty($max_adult) ){
                                     		$availability_desc .= '     
                                            <td  style="width: 15%;">$'.$adult_price.'</td>
                                            <td  style="width: 15%;">$'.$child_price.'</td>';

                                     }else if( !empty($max_units) ){
                                     		$availability_desc .= '<td  style="width: 15%;">$'.$unit_price.'</td>';
                                     }

                                     $availability_desc .= '
                                            <td style="width: 15%;"><a class="btn btn-danger btn-xs" href="javascript:;" onclick="addItemToCart('.$optionId.', '.$activity_id.')">Add to Cart</a></td>
                                        </tr>';
                                }

								foreach($available_dates as $dt){
									$available_date .= '<option value="'.$dt.'">'.$dt.'</option>';
								}
?>
				<div class="categories category_<?php echo $category_id?>" >
					<div class="catTitle"> <?php echo $category_name;?> </div>
					<input type="hidden" id="currency_<?php echo $activity_id;?>" value="<?php echo $currency;?>" />
					<input type="hidden" class="activity_names" id="activity_name_<?php echo $activity_id;?>" value="<?php echo $name;?>" />
					<input type="hidden" id="thumb_url_<?php echo $activity_id;?>" value="<?php echo $thumbUrl;?>" />
				</div>
				<div class="result categories category_<?php echo $category_id?>">
					<div class="panel panel-info activity-info" id="sResult">
						<div class="panel-heading clearfix">
							<div class="panel-title pull-left">
								<h4><?php echo $name;?></h4>
							</div>
						</div>
						<div class="panel-body">
							<div class="media"> <a class="pull-left" href="#"> <img class="media-object img-responsive thumbnail w170" src="<?php echo $thumbUrl?>" alt="image"> </a>
								<div class="media-body">
									<div class="col-md-7">
										<p class="activityDetails"> <?php echo $desc?> </p>
									</div>
									<div class="col-md-5" style="float: right">
										<div class="row">
											<div class="clearfix">
												<div class="form-group col-md-12"> <span>Select available date:</span>
													<select class="select2me form-control" name="date_<?php echo $activity_id;?>" id="date_<?php echo $activity_id;?>">
														<?php echo $available_date?>
													</select>
												</div>
											</div>
											<div class="clearfix">
												
												<?php if( !empty($max_units) ){?>
												<div class="form-group col-md-12"> <span>Units</span>
													<select class="select2me form-control" name="unit_<?php echo $activity_id;?>" id="unit_<?php echo $activity_id;?>">
														<?php echo $max_units?>
													</select>
												</div>
												<?php }?>

												<?php if( !empty($max_adult) ){?>
												<div class="form-group col-md-12"> <span>Adult(12+)</span>
													<select class="select2me form-control" name="adult_<?php echo $activity_id;?>" id="adult_<?php echo $activity_id;?>">
														<?php echo $max_adult?>
													</select>
												</div>
												<?php }?>

												<?php if( !empty($max_child) ){?>
												<div class="form-group col-md-12"> <span>Child(5-12)</span>
													<select class="select2me form-control" name="child_<?php echo $activity_id;?>" id="child_<?php echo $activity_id;?>">
														<?php echo $max_child;?>
													</select>
												</div>
												<?php }?>												

											</div>
										</div>
									</div>
									<div class="more_listing_detail"> <a data-toggle="modal" href="#packageFullDetails" onclick="updateViewContent(<?php echo $activity_id?>)">More Details</a> </div>
								</div>
							</div>
						</div>
						<div class="panel-footer">
							<div class="table-responsive">
								<table class="table">
									<thead style="background: transparent;">
										<tr>
											<th>&nbsp;</th>
											
											<?php if( !empty($max_units) ){?>
												<th>Unit</th>
											<?php }?>
											
											<?php if( !empty($max_adult) ){?>
											<th>Adult (12+)</th>
											<th>Child (5-12)</th>
											<?php }?>
											
											<th>&nbsp;</th>
										</tr>
									</thead>
									<tbody>
										<?php echo $availability_desc?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<?php



					   }







					}



					?>
			</div>
		</div>
	</div>
	
	<!--<center class="clearfix">



				<ul class = "pagination">



					<li><a class="pagination_number" href = "#">&laquo;</a></li>



					<li><a class="pagination_number" href = "#">1</a></li>



					<li><a class="pagination_number" href = "#">2</a></li>



					<li><a class="pagination_number" href = "#">3</a></li>



					<li><a class="pagination_number" href = "#">4</a></li>



					<li><a class="pagination_number" href = "#">5</a></li>



					<li><a class="pagination_number" href = "#">&raquo;</a></li>



				</ul>



			</center>--> 
	
</div>

<div class="modal fade" id="packageFullDetails">
	<div class="modal-dialog modal-lg" id="packageFullDetailsEmpty">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Package Details</h4>
			</div>
			<div class="lowerModalBody" style="margin-top: 0px;"> Loading... </div>
		</div>
	</div>
	<div class="modal-dialog modal-lg" id="packageFullDetailsContent" style="display:none;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Package Details</h4>
			</div>
			<div class="lowerModalBody" id="packageInfo" style="margin-top: 0px;">
				<div role="tabpanel"> 
					
					<!-- Nav tabs -->
					
					<ul class="nav nav-tabs nav-justified nav-pills smnav" role="tablist">
						<li role="presentation" id="detailsTab" class="active"> <a href="#" data-target="#overView" aria-controls="overView" role="tab" data-toggle="tab">Detail</a> </li>
						<li role="presentation" id="picturesTab"> <a href="#" data-target="#pictures" aria-controls="pictures" role="tab" data-toggle="tab">Pictures</a> </li>
						<li role="presentation" id="videoTab"> <a href="#" data-target="#video" aria-controls="video" role="tab" data-toggle="tab">Video</a> </li>
						<li role="presentation" id="mapTab"> <a href="#" data-target="#map-canvas" aria-controls="map-canvas" role="tab" data-toggle="tab">Map</a> </li>
						<li role="presentation" id="seatingTab"> <a href="#" data-target="#sChart" aria-controls="sChart" role="tab" data-toggle="tab">Seating</a> </li>
					</ul>
					
					<!-- Tab panes -->
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane fade in active" id="overView">
							<div class="info">
								<div class="clearfix">
									<div class="col-xs-12">
										<h4 class="info-title">Highlights / General details</h4>
										<p class="info-para" id="highlights_general_details"></p>
									</div>
								</div>
								<div class="clearfix">
									<div class="col-sm-12" id="inclusionsDiv">
										<h4 class="info-title">Inclusions</h4>
										<p class="info-para" id="inclusions"></p>
									</div>
									<div class="col-sm-12" id="exclusionDiv">
										<h4 class="info-title">Exclusion</h4>
										<p class="info-para" id="exclusion"></p>
									</div>
									<div class="col-sm-12" id="durationDiv">
										<h4 class="info-title">Duration</h4>
										<p class="info-para" id="duration"></p>
									</div>
								</div>
								<div class="clearfix">
									<div class="col-md-12" id="hour_of_operationDiv">
										<h4 class="info-title">Hours of Operation</h4>
										<p class="info-para" id="hour_of_operation"></p>
									</div>
									<div class="col-md-12" id="location_textDiv">
										<h4 class="info-title">Location</h4>
										<p class="info-para" id="location_text"></p>
									</div>
									<div class="col-md-12" id="additional_informationDiv">
										<h4 class="info-title">Additional Information</h4>
										<p class="info-para" id="additional_information"></p>
									</div>
									<div class="col-md-12" id="participant_restrictionsDiv">
										<h4 class="info-title">Participant Restrictions</h4>
										<p class="info-para" id="participant_restrictions"></p>
									</div>
									<div class="col-md-12" id="policiesDiv">
										<h4 class="info-title">Policies</h4>
										<p class="info-para" id="policies"></p>
									</div>
								</div>
							</div>
						</div>
						<div role="tabpanel" class="tab-pane fade" id="pictures">
							<div class="clearfix info">
								<div class="clearfix">
									<div class="col-sm-5">
										<div id="slider"> 
											
											<!-- Top part of the slider -->
											
											<div class="row">
												<div class="col-sm-12" id="carousel-bounding-box">
													<div class="carousel slide" id="slPicture"> 
														
														<!-- Carousel items -->
														
														<div class="carousel-inner" id="image_sliding"></div>
														
														<!-- Carousel nav --> 
														
														<a class="left carousel-control" href="#slPicture" role="button" data-slide="prev"> <span class="glyphicon glyphicon-chevron-left"></span> </a> <a class="right carousel-control" href="#slPicture" role="button" data-slide="next"> <span class="glyphicon glyphicon-chevron-right"></span> </a> </div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-sm-7 hidden-xs activityGallery" id="slider-thumbs"> 
										
										<!-- Bottom switcher of slider -->
										
										<ul class="list-unstyled" id="slide_switcher">
										</ul>
									</div>
								</div>
							</div>
						</div>
						<div role="tabpanel" class="tab-pane fade" id="video">
							<div class="clearfix">
								<div class="col-md-12">
									<h4 class="vtitle" id="video_title"></h4>
									<div class="embed-responsive embed-responsive-16by9" id="youtubeVideoLink"> </div>
								</div>
							</div>
						</div>
						<div role="tabpanel" class="tab-pane fade" id="map-canvas" style="height: 300px; width: 100%; text-align:center;"></div>
						<div role="tabpanel" class="tab-pane fade" id="sChart">
							<div class="clearfix" id="schartOptionSelect">
								<div class="form-group col-md-6"> <span>Select available date:</span>
									<select class="select2me form-control" name="" id=" ">
										<option value="12/14/2015 12:00:00 AM">Dec 14 (Mon)</option>
										<option value="12/15/2015 12:00:00 AM">Dec 15 (Tue)</option>
										<option value="12/16/2015 12:00:00 AM">Dec 16 (Wed)</option>
										<option value="12/17/2015 12:00:00 AM">Dec 17 (Thu)</option>
										<option value="12/18/2015 12:00:00 AM">Dec 18 (Fri)</option>
										<option value="12/21/2015 12:00:00 AM">Dec 21 (Mon)</option>
									</select>
								</div>
								<div class="form-group col-md-3"> <span>Adult(12+)</span>
									<select class="select2me form-control" name="" id=" ">
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
										<option value="6">6</option>
										<option value="7">7</option>
										<option value="8">8</option>
										<option value="9">9</option>
										<option value="10">10</option>
										<option value="11">11</option>
										<option value="12">12</option>
										<option value="13">13</option>
										<option value="14">14</option>
										<option value="15">15</option>
										<option value="16">16</option>
										<option value="17">17</option>
										<option value="18">18</option>
										<option value="19">19</option>
										<option value="20">20</option>
									</select>
								</div>
								<div class="form-group col-md-3"> <span>Child(5-12)</span>
									<select class="select2me form-control" name="" id=" ">
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
										<option value="6">6</option>
										<option value="7">7</option>
										<option value="8">8</option>
										<option value="9">9</option>
										<option value="10">10</option>
										<option value="11">11</option>
										<option value="12">12</option>
										<option value="13">13</option>
										<option value="14">14</option>
										<option value="15">15</option>
										<option value="16">16</option>
										<option value="17">17</option>
										<option value="18">18</option>
										<option value="19">19</option>
										<option value="20">20</option>
									</select>
								</div>
							</div>
							<div class="clearfix" id="schartOption">
								<ul class="nav nav-pills nav-stacked col-sm-2">
									<li class="active"><a data-toggle="pill" href="#c1">Category A</a></li>
									<li><a data-toggle="pill" href="#c2">Category B</a></li>
									<li><a data-toggle="pill" href="#c3">Category C</a></li>
									<li><a data-toggle="pill" href="#c4">Category C</a></li>
								</ul>
								<div class="tab-content">
									<div id="c1" class="tab-pane fade in active">
										<center>
											<img src="http://image1.urlforimages.com/1295505/12320982-KA.png" class="img-responsive" alt="Image">
										</center>
									</div>
									<div id="c2" class="tab-pane fade">
										<center>
											<img src="http://image1.urlforimages.com/1295505/12320982-KA.png" class="img-responsive" alt="Image">
										</center>
									</div>
									<div id="c3" class="tab-pane fade">
										<center>
											<img src="http://image1.urlforimages.com/1295505/12320982-KA.png" class="img-responsive" alt="Image">
										</center>
									</div>
									<div id="c4" class="tab-pane fade">
										<center>
											<img src="http://image1.urlforimages.com/1295505/12320982-KA.png" class="img-responsive" alt="Image">
										</center>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<!-- 			<div class="clearfix">



                    <div class="panel panel-default cbg">



                        <div class="panel-heading">



                            <h3 class="panel-title bold">Activity Cart <span class="fa fa-shopping-cart fa-lg"></span></h3>



                        </div>



                        <div class="panel-body">



                            <span>Cart is Empty</span>



                            <ul class="list-unstyled cart-list">



                                <li>



                                    <div class="cart-detail">



                                        <h5><a href="">The High Roller at The LINQ </a></h5>



                                        <h5>Cost: $450.00</h5>



                                        <h5><a data-toggle="modal" href="#cartDetails">Details</a> <span>|</span> <a href="" class="text-danger">Remove</a></h5>



                                    </div>



                                </li>



                            </ul>



                        </div>



                    </div>



            	</div> --> 
				
			</div>
		</div>
	</div>
</div>


<!-- div for showing message when item added into cart-->
<div aria-hidden="false" role="dialog" class="modal fade in" id="item_added_to_cart" style="display: none;">
    <div class="success_login_main_cont margin-top-20">
        <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="fa fa-times"></i></button>
            <!--<h4 class="modal-title">Registration Successfully</h4>-->
            <h3 class="form-title">Cart Update</h3>
        </div>
        <div style="" class="">
            <p style="padding: 10px">Added to the cart successfully!</p>
        </div>
        <div style="padding: 3px;" class="modal-footer">
            <div style="text-align:center">
                <button type="button" style="padding: 7px 20px;" data-dismiss="modal" class="btn default_btn" onclick="$('#item_added_to_cart').modal('hide');">OK</button>
            </div>
        </div>
    </div>
</div>

<!-- div for showing message when item added into cart-->
<div aria-hidden="false" role="dialog" class="modal fade in" id="item_added_to_cart_error" style="display: none;">
    <div class="success_login_main_cont margin-top-20">
        <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="fa fa-times"></i></button>
            <!--<h4 class="modal-title">Registration Successfully</h4>-->
            <h3 class="form-title">Cart Update</h3>
        </div>
        <div style="" class="">
            <p style="padding: 10px">Unfortunately, our supplier does not have availability for selected activity. Please reduce the amount of people on the booking or choose other activity in order to proceed.</p>
        </div>
        <div style="padding: 3px;" class="modal-footer">
            <div style="text-align:center">
                <button type="button" style="padding: 7px 20px;" data-dismiss="modal" class="btn default_btn" onclick="$('#item_added_to_cart_error').modal('hide');">OK</button>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>public/assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script> 
<script src="<?php echo base_url(); ?>public/affilliate_theme/plugins/checkincheckout/bootstrap-datepicker.js" type="text/javascript"></script> 
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBx9DtU1hU_3U9wTX5n5dlYury60QJIGD0" type="text/javascript"></script>

<script>
	function addItemToCart(optionId, activityId){
		var activity_date = $('#date_'+activityId).val();

		var no_of_adults = 0;
		if( $('#adult_'+activityId).length ){
			no_of_adults = $('#adult_'+activityId).val();
		}

		var no_of_units = 0;
		if( $('#unit_'+activityId).length ){
			no_of_units = $('#unit_'+activityId).val();
		}

		var no_of_childs = 0;
		if( $('#child_'+activityId).length ){
			no_of_childs = $('#child_'+activityId).val();
		}
		
		if(no_of_units < 1 && no_of_adults < 1 && no_of_childs < 1){
			alert('Please select the amount of Units/ Adults and/or Children first!');
			return;
		}

		if(no_of_adults == 0 && no_of_childs > 0){
			if( !(confirm('are you sure you want to proceed without selecting an Adult?')) ){
				return;
			}
		}

		var currency = $('#currency_'+activityId).val();
		var activityName = encodeURI($('#activity_name_'+activityId).val());
		var thumbUrl = encodeURI($('#thumb_url_'+activityId).val());
		var adult_price = $('#adult_price_'+optionId).val();
		var child_price = $('#child_price_'+optionId).val();
		var unit_price = $('#unit_price_'+optionId).val();

		var optionTitle = encodeURI($('#option_title_'+optionId).val());
		var location = '<?php echo $activityLocationName;?>';
		jQuery.post( "<?php echo base_url('add_activity_to_cart')?>", { 
			location:location,
			optionId: optionId ,
			activityId: activityId,
			activityName: activityName,
			thumbUrl:thumbUrl,
			activity_date: activity_date,
			no_of_adults: no_of_adults,
			no_of_childs: no_of_childs,
			no_of_units:no_of_units,
			adult_price: adult_price,
			child_price: child_price,
			unit_price: unit_price,
			currency: currency,
			optionTitle: optionTitle

		}).done(function( data ) {
			if(data != '0'){
				$('#item_added_to_cart').modal('show');
				//updating cart html
				$('#myCart').html(data);	
			}else{
				$('#item_added_to_cart_error').modal('show');
			}
			
		});
	}

	function removeItemFromCart(optionId, activity_date){

		jQuery.post( "<?php echo base_url('remove_activity_from_cart')?>", { 
			optionId: optionId ,
			activity_date: activity_date

		}).done(function( data ) {
			//updating cart html
			$('#cart_'+optionId+'_'+activity_date).remove();
		});

	}

	function check_all(s_all){

		if( s_all == 1 ){
			$('table#categoryFilters input:checkbox').attr('checked', true);
			$('.categories').show();
		}else{
			$('table#categoryFilters input:checkbox').attr('checked', false);
			$('.categories').hide();
		}

		var visibleDivcount = $(".categories:visible").length/2;
		$('#visibleDivcount').html(visibleDivcount);
	}

	function filter_results(){
		
		$('.categories').hide();
		var activity_name = $.trim($('#activity_name').val());
		
		$('.categoryFiltersCheckbox:checked').each(function(index, element) {
            cat_id = $(element).val();
			$('.category_'+cat_id).show();
        });
		
		if(activity_name != ''){
			
			$('.activity_names').each(function(index, element) {
				keyword = $(element).val();

				if (keyword.toLowerCase().indexOf(activity_name) < 0){
					$(element).parent().hide();
					$(element).parent().next().hide();
				}
			});
			
		}
		
		

		var visibleDivcount = $(".categories:visible").length/2;
		$('#visibleDivcount').html(visibleDivcount);
	}

    var activity_details_path = "<?php echo base_url('view_activity_details')?>"

	function updateViewContent(activity_id2){
		
		$('#packageFullDetailsContent').hide();
		$('#packageFullDetailsEmpty').show();

		jQuery.post( activity_details_path, { activity_id: activity_id2 }).done(function( data ) {

			$('#detailsTab a').trigger('click');

			var jsonData = $.parseJSON(data);
			//console.log(jsonData);
			var highlight_general_details_text = jsonData.activities_by_id.highlights_general_details;
			var inclusions_text = jsonData.activities_by_id.inclusion;
			var exclusion_text = jsonData.activities_by_id.exclusion;
			var hour_of_operation_text = jsonData.activities_by_id.hour_of_operation;
			var duration_text = jsonData.activities_by_id.duration;
			var policies_text = jsonData.activities_by_id.policies;
			var location_text = jsonData.activities_by_id.location;
			var additional_information_text = jsonData.activities_by_id.additional_information_text;
			var participant_restrictions = jsonData.activities_by_id.participant_restrictions;
			var last_restriction_text = jsonData.activities_by_id.last_restriction_text;

			$('#highlights_general_details').html(highlight_general_details_text);
			if( emptyChek(inclusions_text) ){
				$('#inclusions').html(inclusions_text);
				$('#inclusionsDiv').show();
			}else{
				$('#inclusionsDiv').hide();
			}
			
			if( emptyChek(exclusion_text) ){
				$('#exclusion').html(exclusion_text);
				$('#exclusionDiv').show();
			}else{
				$('#exclusionDiv').hide();
			}
			
			if( emptyChek(hour_of_operation_text) ){
				$('#hour_of_operation').html(hour_of_operation_text);
				$('#hour_of_operationDiv').show();
			}else{
				$('#hour_of_operationDiv').hide();
			}
			
			if( emptyChek(duration_text) ){
				$('#duration').html(duration_text);
				$('#durationDiv').show();
			}else{
				$('#durationDiv').hide();
			}
			
			if( emptyChek(policies_text) ){
				$('#policies').html(policies_text);
				$('#policiesDiv').show();
			}else{
				$('#policiesDiv').hide();
			}
			
			if( emptyChek(location_text) ){
				$('#location_text').html(location_text);
				$('#location_textDiv').show();
			}else{
				$('#location_textDiv').hide();
			}
			
			if( emptyChek(additional_information_text) ){
				$('#additional_information').html(additional_information_text);
				$('#additional_informationDiv').show();
			}else{
				$('#additional_informationDiv').hide();
			}
			
			if( emptyChek(participant_restrictions) ){
				$('#participant_restrictions').html(participant_restrictions);
				$('#participant_restrictionsDiv').show();
			}else{
				$('#participant_restrictionsDiv').hide();
			}
			
			if( emptyChek(last_restriction_text) ){
				$('#last_restriction').html(last_restriction_text);
				$('#last_restrictionDiv').show();
			}else{
				$('#last_restrictionDiv').hide();
			}

		   var images = jsonData.activities_by_id.images;
		   var images_path = "";
		   var slide_switcher_path = "";

		   if(images.length > 0){
			   $('#picturesTab').show();

			   images_path = '<div class="active item" data-slide-number="0"><img src="' + images[0] + '"></div>';
			   slide_switcher_path += '<li class="col-sm-3">' + '<a class="thumbnail" id="carousel-selector-0"><img src="' + images[0] + '"></a>' + '</li>';
			   
			   for(i=1; i<images.length; i++){
				   
				   images_path += '<div class="item" data-slide-number="'+ i +'"><img src="' + images[i] + '"></div>';
				   slide_switcher_path += '<li class="col-sm-3">' + '<a class="thumbnail" id="carousel-selector-' + i + '"><img src="' + images[i] + '"></a>' + '</li>';

			   }

			   $('#image_sliding').html(images_path);
			   $('#slide_switcher').html(slide_switcher_path);

		   }else{
			   $('#picturesTab').hide();
		   }

			//videos
			if(jsonData.activities_by_id.movies != null){
				$('#videoTab').show();
				
				var youtubeVideoLink = '';
				if(jsonData.activities_by_id.movies.youtubeId){
					youtubeVideoLink = '<iframe class="embed-responsive-item"  width="560" height="315" src="https://www.youtube.com/embed/' + jsonData.activities_by_id.movies.youtubeId + '" frameborder="0" allowfullscreen></iframe>';
				}else if (jsonData.activities_by_id.movies[0] != null && jsonData.activities_by_id.movies[0].youtubeId != null){
					youtubeVideoLink = '<iframe class="embed-responsive-item"  width="560" height="315" src="https://www.youtube.com/embed/' + jsonData.activities_by_id.movies[0].youtubeId + '" frameborder="0" allowfullscreen></iframe>';
				}
				

				var videoTitle = jsonData.activities_by_id.movies.title;
				$('#youtubeVideoLink').html(youtubeVideoLink);
				$('#video_title').html(videoTitle);

		   }else{
			  $('#videoTab').hide();
		   }

		   //map
		   if(jsonData.activities_by_id.longitude != null && jsonData.activities_by_id.latitude != null){

				$('#mapTab').show();
				var lati = jsonData.activities_by_id.latitude;
				var longi = jsonData.activities_by_id.longitude;

				//$('#map-canvas').html('<iframe src="https://www.google.com/maps/embed/v1/view?key=AIzaSyBx9DtU1hU_3U9wTX5n5dlYury60QJIGD0&center=-'+lati+','+longi+'&zoom=8" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>');

				initMap(lati, longi);

		   }else{

			   $('#mapTab').hide();
			   //$('#map').html('<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d27224.67225415322!2d74.27370725938815!3d31.466874513448897!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3919015f82b0b86f%3A0x2fcaf9fdeb3d02e6!2sJohar+Town%2C+Lahore%2C+Pakistan!5e0!3m2!1sen!2s!4v1475133503192" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>');

		   }
		   
			if(jsonData.activities_by_id.seating){

			}else{
				$('#seatingTab').hide();

			}

			$('#packageFullDetailsContent').show();
			$('#packageFullDetailsEmpty').hide();

		});
	}


var map = null;
var marker = null;
function initMap(lat, lng) {
	
	lat = parseFloat(lat);
	lng = parseFloat(lng);

	console.log(lat+'-'+lng);

    var uluru = {lat: lat, lng: lng};
    map = new google.maps.Map(document.getElementById('map-canvas'), {
      zoom: 13,
      center: uluru
    });

    marker = new google.maps.Marker({
      position: uluru,
      map: map
    });

    map.addListener('center_changed', function() {
      
        map.panTo(marker.getPosition());
      
    });

    marker.addListener('click', function() {
          map.setZoom(8);
          map.setCenter(marker.getPosition());
    });

}


var start_date_pt = '<?php echo date('F d, Y H:i:s'); ?>';

var nowTemp = new Date(start_date_pt);

var houres = nowTemp.getHours();

if (houres > 15)

{

	var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate() + 4, 0, 0, 0, 0);

} else {

	var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate() + 4, 0, 0, 0, 0);

}





$(document).ready(function (){


	$('#mapTab').on('click', function(){
		setTimeout(function(){google.maps.event.trigger(map, 'resize')}, 1000);
	});

	var d = new Date();

        d.setDate(d.getDate() - 1);

        var activityDateFrom = $('#activity_date_from').datepicker({

            onRender: function (date) {

                if (date.valueOf() < d.valueOf())

                {

                    return 'disabled';

                } 

                if (date.valueOf() == now.valueOf())

                {

                    return 'pluseone';

                } 

                else

                {

                    return '';

                }

            }

        }).on('changeDate', function (ev) {

            var newDate = new Date(ev.date);

            newDate.setDate(newDate.getDate() + 1);

            $('#activity_date_to').val("");

            activityDateTo.setValue(newDate);

            $(this).datepicker('hide');

            var activity_date_from = $('#activity_date_from').val();

            var activity_date_to = $('#activity_date_to').val();

            activity_date_from = moment($('#activity_date_from').val());

            activity_date_to = moment($('#activity_date_to').val());

            var diff = activity_date_to.diff(activity_date_from, 'days');

            $('.nights').text(diff);

            $('.nights').val(diff);

            $('#activity_date_to')[0].focus();

        }).data('datepicker');



        var activityDateTo = $('#activity_date_to').datepicker({

            onRender: function (date) {

                var newdate = new Date(activityDateFrom.date);

                var newdate1 = newdate.setDate(newdate.getDate() + 28);

                if (date.valueOf() <= activityDateFrom.date.valueOf())

                {

                    return 'disabled';

                }

            }

        }).on('changeDate', function (ev) {

            activityDateTo.hide();

            var activity_date_from = $('#activity_date_from').val();

            var activity_date_to = $('#activity_date_to').val();

            if (activity_date_from != '' && activity_date_to != '') 

			{

				$('#activityDateToerror').remove();

				var chkin = new Date(activity_date_from);

				var chkout = new Date(activity_date_to);

				if(chkin < chkout)

				{

					

				}

				else

				{

					$('#activity_date_to').val('');

					$('#activity_date_to').after('<label id="activityDateToerror" class="error">Check-out date must be greater then Check-in date.</label>');

					return false;

				}

                activity_date_from = moment($('#activity_date_from').val());

                activity_date_to = moment($('#activity_date_to').val());

                //alert($('#activity_date_to').val());

                var diff = activity_date_to.diff(activity_date_from, 'days');

                $('.nights').text(diff);

                $('.nights').val(diff);

            }

			else

			{

				$('#activity_date_to').val('');

				$('#activity_date_from').focus();

			}

        }).data('datepicker');

		
		filter_results();

});

var stickySidebar = $('.fixedPosition').offset().top;
$(window).scroll(function() {  
    if ($(window).scrollTop() > stickySidebar) {
        $('.fixedPosition').addClass('affix');
    }
    else {
        $('.fixedPosition').removeClass('affix');
    }  
});

function emptyChek(val){
	
	if(typeof(val)  === "undefined"){
    	return false;
	}else{
		if( val.length > 0 ){
			return true
		}
	}
	return false;
}
</script> 
