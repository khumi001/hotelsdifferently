<div class="login_main_cont margin-top-20">
	<div class="width-row" style="display:none;padding: 10px 0 0 8px;">
		<div itemscope itemtype="http://schema.org/Person">
		   <span itemprop="name">Hotels Differently</span>
		   <span itemprop="company">HotelsDifferently LLC</span>
		   <span itemprop="tel">888-287-2307</span>
		</div>
	</div>
    <form class="login-form"  method="post">
        <h3 class="form-title">LOGIN</h3>
        <div class="alert alert-danger display-hide hidden" style="display: none;" id='msg'>
            <button class="close" data-close="alert"></button>
            <span>
                Enter any username and password.
            </span>
        </div>
        <div class="alert alert-danger display-hide  " style="display: none;" id='login'>
            <button class="close" data-close="alert"></button>
            <span>
                Please enter your email address and password.
            </span>
        </div>
        <div class="form-group">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label">Email address</label>
            <div class="margin-bottom-10">
                <input class="form-control" tabindex="2" type="text" id="user" name="username" value="<?php echo get_cookie('username'); ?>" />
            </div>
        </div>
        <div class="form-group ">
            <label class="control-label">Password</label>
            <div class="margin-bottom-10">
                <input class="form-control" tabindex="2" type="password" id="pass" name="password" value="<?php echo get_cookie('password'); ?>" />
            </div>
        </div>
        <div class="form-group">
            <input type="submit" tabindex="3" value="Login" class="btn-login">
			<label class="checkbox">
                <input type="checkbox" tabindex="2" name="remember" value="1" id="remember" <?php if(get_cookie('username') != ''){ echo 'checked="checked"'; } ?> /> Remember me
            </label>
        </div>
        <div class="form-group margin-top-10">
            <div class="">
                <label class="control-label pull-left" style="font-size: 13px;padding: 5px 0;"><a href="javascript:;" tabindex="5" id="forget-password">Forgot your password?</a></label>
            </div>
            <!--<div class="col-md-4" style="margin: -28px 0 0 0;">
                <button type="submit" class="default_btn pull-right" style="padding: 8px 25px;  margin-top: -10px">Login</button>
                
            </div>-->
        </div>
        <div class="form-group margin-top-10 margin-bottom-10 create-account">
            <strong class="signUpNow">
				<span style="color:red;">New here? </span>No worries,
                <a href="<?php echo base_url(); ?>front/home/member-signup">SIGN UP NOW</a>
                which takes 30 seconds and access the best deals online right away!
            </strong>
                <div class="clear"></div>
		</div>
	    <div class="clear"></div>
        <div class="create-account hide" style="padding: 0 15px;">
            <!--<span style="font-size:12px; display: block; text-align: center;">Donâ€™t have an account yet? SIGN UP NOW AS OUR NEW <br /> </span>-->
            <a href="<?php echo base_url(); ?>front/home/member-signup">
                Member
            </a>
        </div>
    </form>
    <!-- END LOGIN FORM -->	
	<!-- BEGIN FORGOT PASSWORD FORM -->
    <form class="forget-form" action="#" method="post" style="display: none;">
        <h3>Forgot password?</h3>
        <p style="text-align: center;">
            Enter your e-mail address below to request your password.
        </p>
        <div class="form-group">
            <div class="input-icon">
                <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email" id="forgetemail"/>
            </div>
        </div>
        <div class="form-actions">
            <button type="button" id="back-btn" class="btn red_btn" style="padding: 5px 35px;">Back </button>
            <button type="submit" class="default_btn pull-right" style="padding: 5px 25px;">Submit</button>
        </div>
    </form>
    <!-- END FORGOT PASSWORD FORM -->
</div>
<div aria-hidden="false" role="dialog" class="modal fade in" id="success_registration" style="display: none;">
    <div class="success_login_main_cont margin-top-20">         
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><em class="fa fa-times"></em></button>
                <!--<h4 class="modal-title">Registration Successfully</h4>-->
                <h3 class="form-title">Success!</h3>
            </div>
               <div style="" class="">
                    <p style="padding: 10px"> We just emailed your password to your email address on file.</p>
                </div>
            <div style="padding: 3px;" class="modal-footer">
                <div style="text-align:center">
                        <button type="button" style="padding: 7px 20px;" data-dismiss="modal" class="btn default_btn clickhome">OK</button>
                    </div>
            </div>           
     </div>                                           
</div>
<div aria-hidden="false" role="dialog" class="modal fade in" id="error_registration" style="display: none;">
    <div class="success_login_main_cont margin-top-20">
         
                <div class="modal-header">
                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><em class="fa fa-times"></em></button>
                    <!--<h4 class="modal-title">Registration Successfully</h4>-->             
                     <h3 class="form-title">Sorry!</h3>
                </div>
                   <div style="" class="">
                        <p style="padding: 10px"> It seems that your email address is not in our database. Please check if you typed your email address properly.</p>
                    </div>
                <div style="padding: 3px;" class="modal-footer">
                    <div style="text-align:center">
                            <button type="button" style="padding: 7px 20px;" data-dismiss="modal" class="btn default_btn clickhome">OK</button>
                        </div>
                </div>
           
     </div>                                           
</div>
<script>
    $(document).ready(function(){
        // resend email
        $(document).on("click",'#signUpResendEmail',function(){
            var email = $("#user").val();
            $.ajax({
                type: "POST",
                url: baseurl + "front/home/signUpResendEmail",
                data: {"email": email},
                success: function (response) {
                    alert(response);
                }
            });
        });
    });
</script>