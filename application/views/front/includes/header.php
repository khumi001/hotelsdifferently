<head>
    <meta charset="utf-8"/>
    <?php
    $title = empty($var_meta_title) ? "Wholesale Hotels Group – Great travel deals for Corporate Travel and Individuals" : $var_meta_title;
    $description = empty($var_meta_description) ? "Get WHOLESALE HOTEL inventories and deals worldwide for Corporate Travel, Non-Profit travel, Government travel, Travel Agents and Individual users." : $var_meta_description;
    $keywords = empty($var_meta_keyword) ? "Wholesale Hotels Group, WHotelsGroup.com, wholesale hotel deals, corporate travel, government travel, non-profit travel, travel agency travel, travel agency hotels, wholesale hotels, Online Travel Agency, best OTAs" : $var_meta_keyword;
    ?>
    <title><?php echo $title ?></title>
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta content="<?php echo $description ?>" name="description"/>
    <meta content="<?php echo $keywords ?>" name="keywords"/>
    
    <meta property="og:site_name" content="Wholesale Hotels Group">
    <meta property="og:title" content="<?php echo $title ?>" />
    <meta property="og:description" content="<?php echo $description ?>" />
    <meta property="og:image" content="<?php echo base_url(); ?>public/homepage.png" />
    <meta property="og:type" content="website" />
    
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:site" content="Wholesale Hotels Group">
    <meta property="twitter:title" content="<?php echo $title;?>">
    <meta property="twitter:description" content="<?php echo $description ;?>"/>
    <meta property="twitter:image" content="<?php echo base_url(); ?>public/homepage.png">
    
    <meta name="google-site-verification" content="ZBUbYCT9lCwyszTdzXzftAqkYQS8o5O6qP8eGRaiQXk" />
    <meta name="p:domain_verify" content="a29fc0e0d9ae02190ff346cc33d9b2b3"/>
    
    <!--<meta name="robots" content="NOODP">
    <meta name="robots" content="nosnippet">-->
    
	<script type="text/javascript">var baseurl = "<?php echo base_url(); ?>";</script>
	<script type="text/javascript">!function(a,b,c,d,e,f,g){a.GoogleAnalyticsObject=e,a[e]=a[e]||function(){(a[e].q=a[e].q||[]).push(arguments)},a[e].l=1*new Date,f=b.createElement(c),g=b.getElementsByTagName(c)[0],f.async=1,f.src=d,g.parentNode.insertBefore(f,g)}(window,document,"script","https://www.google-analytics.com/analytics.js","ga"),ga("create","UA-87962950-1","auto"),ga("send","pageview");</script>
    <link href="<?php echo base_url(); ?>public/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url(); ?>public/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="https://fonts.googleapis.com/css?family=Frijole|Indie+Flower" rel="stylesheet">
    <link href="<?php echo base_url(); ?>public/affilliate_theme/stylesheets/style.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>public/affilliate_theme/stylesheets/custom.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/assets/plugins/data-tables/DT_bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/assets/plugins/bootstrap-toastr/toastr.min.css"/>
    <?php
    if (!empty($css)) {
        foreach ($css as $value) {
            ?>
            <link rel="stylesheet" href="<?php echo base_url(); ?>public/affilliate_theme/stylesheets/<?php echo $value; ?>" />
            <?php
        }
    }
    ?>
    <?php
    if (!empty($css_plugin)) {
        foreach ($css_plugin as $value_plugin) {
            ?>
            <link rel="stylesheet" href="<?php echo base_url(); ?>public/affilliate_theme/plugins/<?php echo $value_plugin; ?>" />
            <?php
        }
    }
    ?> 
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/affilliate_theme/plugins/bootstrap-select/bootstrap-select.min.css" />
    <link href="<?php echo base_url(); ?>public/assets/stylesheets/plugins.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url(); ?>public/assets/stylesheets/jquery.bxslider.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url(); ?>public/assets/stylesheets/custom.css" rel="stylesheet" type="text/css"/>
    <link rel="shortcut icon" href="http://hotels.softech.website/public/affilliate_theme/img/logo.png"/>
	<link href="<?php echo base_url(); ?>public/affilliate_theme/stylesheets/responsive.css" rel="stylesheet" type="text/css">

    <script src="<?php echo base_url(); ?>public/assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
    <!--Social code start-->
    <!--<script src="https://apis.google.com/js/platform.js" async defer></script>
    <script>(function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id))
                return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.9";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
    <g:plusone></g:plusone><br>
    <div class="g-follow" data-href="https://plus.google.com/110825285648121458703" data-rel="relationshipType"></div><br>
    <div id="fb-root"></div>
    <div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div><br>
    <div class="fb-follow" data-href="https://www.facebook.com/zuck" data-layout="standard" data-size="small" data-show-faces="true"></div>-->
    <!--Social Code end-->
</head>