<header>
    <div class="row">
        <div class="col-md-2 margin-top-12">
            <form action="#" method="post" id="newsletter_form" class="newsletter-form">
                <div class="form-group">
                    <label class="control-label">Signup for Newsletter</label>
                    <input type="text" class="form-control" name="newsletter" id="newsletter" value="" />
                    <input type="submit" name="btn_submit" value="submit" />
                </div>
            </form>
        </div>
        <div class="col-md-2 clearfix"></div>
        <div class="col-md-4 text-center logo">
            <a href="<?php echo preg_replace('{/$}', '', base_url());?>" title="cheap hotel booking site" style="margin:0 0;display: block;">
		   	<span class="main-logo " >
		   	<img width="315" height="118" alt="cheap hotel booking site" src="<?php echo base_url(); ?>public/affilliate_theme/img/logo.png">
		   	</span>
            </a>
        </div>
        <div class="col-md-4 margin-top-12">
            <?php // print_r($this->session->userdata);
            if(isset($this->session->userdata['valid_user']['var_usertype']) && ($this->session->userdata['valid_user']['var_usertype'] =='U' || @$this->session->userdata['valid_affiliate']['var_usertype'] =='AF'))
            {?>
                <div class="pages_links margin-top-15">
                    <a href="<?php echo base_url(); ?>front/home/logout" class="login_link main_links">Logout</a>

                    <a href="<?php echo base_url(); ?>user/legal-corner/aboutus" class="main_links  newmargin-links">About Us</a>
                    <a href="<?php echo base_url(); ?>user/legal-corner/coupons" class="main_links newmargin-links">Coupons</a>
                    <a href="<?php echo base_url(); ?>user/legal-corner/travelinsurance" class="main_links newmargin-links" >Travel Insurance</a>
                    <a href="<?php echo base_url(); ?>user/legal-corner/faqs" class="main_links">FAQ</a>
                    <!--<a href="<?php /*echo base_url(); */?>blog" class="main_links" target="_blank">BLOG</a>-->
                </div>

            <?php

            }
            elseif(isset($this->session->userdata['valid_user']['var_usertype']) && $this->session->userdata['valid_user']['var_usertype'] =='TF'){?>
                <div class="pages_links margin-top-15">
                    <a href="<?php echo base_url(); ?>front/home/logout" class="login_link main_links">Logout</a>

                    <a href="<?php echo base_url(); ?>user/legal-corner/aboutus" class="main_links  newmargin-links">About Us</a>
                    <a href="<?php echo base_url(); ?>user/legal-corner/coupons" class="main_links newmargin-links">Coupons</a>
                    <a href="<?php echo base_url(); ?>user/legal-corner/travelinsurance" class="main_links newmargin-links" >Travel Insurance</a>
                    <a href="<?php echo base_url(); ?>front/travel_professional/faqs" class="main_links">FAQ</a>
                    <!--<a href="<?php /*echo base_url(); */?>blog" class="main_links">BLOG</a>-->
                </div>
            <?php	}
            elseif(isset($this->session->userdata['valid_user']['var_usertype']) && $this->session->userdata['valid_user']['var_usertype'] =='EN'){?>
                <div class="pages_links margin-top-15">
                    <a href="<?php echo base_url(); ?>front/home/logout" class="login_link main_links">Logout</a>

                    <a href="<?php echo base_url(); ?>user/legal-corner/aboutus" class="main_links  newmargin-links">About Us</a>
                    <a href="<?php echo base_url(); ?>user/legal-corner/coupons" class="main_links newmargin-links">Coupons</a>
                    <a href="<?php echo base_url(); ?>user/legal-corner/travelinsurance" class="main_links newmargin-links" >Travel Insurance</a>
                    <a href="<?php echo base_url(); ?>front/entity/faqs" class="main_links">FAQ</a>
                    <!--<a href="<?php /*echo base_url(); */?>blog" class="main_links">BLOG</a>-->
                </div>
            <?php	}
            else{
                ?>
                <div class="pages_links margin-top-10 pull-right">
                    <a href="<?php echo base_url(); ?>user/legal-corner/aboutus" class="main_links  newmargin-links">About Us</a>
                    <a href="<?php echo base_url(); ?>user/legal-corner/coupons" class="main_links newmargin-links">Coupons</a>
                    <a href="<?php echo base_url(); ?>user/legal-corner/travelinsurance" class="main_links newmargin-links" >Travel Insurance</a>
                    <a href="<?php echo base_url(); ?>user/legal-corner/faqs" class="main_links newmargin-links">FAQ</a>
                    <a href="<?php echo base_url(); ?>front/home/member-signup" class="main_links newmargin-links">Signup</a>
                    <?php
                    // if($this->page_name != 'Login'){
                    ?>
                    <a href="<?php echo base_url(); ?>login" class="main_links login_link newmargin-links">Login</a>
                    <?php //}
                    ?>
                </div>
            <?php } ?>
        </div>
    </div>

</header>
<div aria-hidden="false" role="dialog" class="modal fade in display_off" id="newsletter_modal">
    <div class="success_login_main_cont margin-top-20">
        <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><em class="fa fa-times"></em></button>
            <h3 class="form-title">NEWSLETTER</h3>
        </div>
        <div class="newsletter_message">
            <p>
                Thank you for subscribing to our newsletter! Please check your inbox for an activation link we sent you to start receiving newsletters for our hottest deals!
            </p>
        </div>
        <div class="modal-footer">
            <div class="text-align-center" >
                <button type="button" data-dismiss="modal" class="btn default_btn clickme padding-7-20">OK</button>
            </div>
        </div>
    </div>
</div>
<div aria-hidden="false" role="dialog" class="modal fade in display_off" id="newsletter_modal_error" >
    <div class="success_login_main_cont margin-top-20">
        <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><em class="fa fa-times"></em></button>
            <h3 class="form-title">NEWSLETTER</h3>
        </div>
        <div class="">
            <p class="margin-bottom-0 padding-10">
                Sorry but this email address is already registered in our system!
            </p>

        </div>
        <div class="modal-footer margin-top-0 padding-3">
            <div class="text-align-center">
                <button type="button" data-dismiss="modal" class="btn default_btn clickme padding-7-20">OK</button>
            </div>
        </div>
    </div>
</div>
<div aria-hidden="false" role="dialog" class="modal fade in display_off" id="newsletter_already_user" >
    <div class="success_login_main_cont margin-top-20">
        <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><em class="fa fa-times"></em></button>
            <h3 class="form-title">NEWSLETTER</h3>
        </div>
        <div>
            <p class="padding-10 margin-bottom-0" >
                Sorry but this email address is already registered in our system!
            </p>

        </div>
        <div class="modal-footer padding-3 margin-top-0">
            <div class="text-align-center">
                <button type="button" data-dismiss="modal" class="padding-7-20 btn default_btn clickme">OK</button>
            </div>
        </div>
    </div>
</div>