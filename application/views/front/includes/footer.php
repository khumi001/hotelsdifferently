<!--[if IE]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->      

<script src="<?php echo base_url(); ?>public/assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>public/affilliate_theme/plugins/jquery-ui/jquery-ui.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>public/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>public/assets/plugins/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>public/assets/plugins/jquery.bxslider.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/plugins/bootstrap-toastr/toastr.min.js"></script>
<script src="<?php echo base_url(); ?>public/assets/plugins/gritter/js/jquery.gritter.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>public/assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>public/assets/javascripts/core/app.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>public/assets/javascripts/core/datatable.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/plugins/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/plugins/data-tables/DT_bootstrap.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/affilliate_theme/plugins/ajaxfileupload.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/affilliate_theme/plugins/jquery.form.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/affilliate_theme/javascripts/affiliate_common.js"></script>
<script src="<?php echo base_url(); ?>public/assets/plugins/bootstrap-toastr/toastr.min.js" type="text/javascript"></script>
 <?php
    if (!empty($js_plugin)) {
        foreach ($js_plugin as $value) {
            ?>
            <script src="<?php echo base_url(); ?>public/affilliate_theme/plugins/<?php echo $value ?>" type="text/javascript"></script>
            <?php
        }
    }
    if (!empty($js)) {
        foreach ($js as $value) {
            ?>
            <script src="<?php echo base_url(); ?>public/affilliate_theme/javascripts/<?php echo $value ?>" type="text/javascript"></script>
            <?php
        }
    }

 if (!empty($roomChange)) {
     foreach ($roomChange as $value) {
         ?>
         <script src="<?php echo base_url(); ?>public/hotel_search/<?php echo $value ?>" type="text/javascript"></script>
     <?php
     }
 }
    ?>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.12/datatables.min.js"></script>
	<script id="remove_js" src="<?php echo base_url(); ?>public/assets/javascripts/core/datatable_apply.js" type="text/javascript"></script>
<script>
    jQuery(document).ready(function() {
        App.init();
        <?php
           if (!empty($init)) {
               foreach ($init as $value) {
                   echo $value.';' ;
               }
           }
           ?>
    });
</script>
<script type="text/javascript">
    jQuery(window).load(function() {
        //$("#loading").delay(2000).fadeOut(500);
        jQuery("#loading-center").click(function() {
            jQuery("#loading").fadeOut(500);
        })
    });
    //slider
    $(document).ready(function(){
      $('.bxslider').bxSlider();
    });

    $('.bxslider').bxSlider({
      buildPager: function(slideIndex){
        switch(slideIndex){
          case 0:
            return '<img src="<?php echo base_url(); ?>public/assets/img/d_page.png" class="thumbnails" />';
          case 1:
            return '<img src="<?php echo base_url(); ?>public/assets/img/d_page.png" class="thumbnails" />';
          case 2:
            return '<img src="<?php echo base_url(); ?>public/assets/img/d_page.png" class="thumbnails" />';
          case 3:
            return '<img src="<?php echo base_url(); ?>public/assets/img/d_page.png" class="thumbnails" />';
          case 4:
            return '<img src="<?php echo base_url(); ?>public/assets/img/d_page.png" class="thumbnails" />';
          case 5:
            return '<img src="<?php echo base_url(); ?>public/assets/img/d_page.png" class="thumbnails" />';
        }
      }
    });

</script>