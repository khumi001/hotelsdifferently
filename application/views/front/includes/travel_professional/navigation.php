 <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
	<span class="sr-only">Toggle navigation</span>
	<span class="icon-bar"></span>
	<span class="icon-bar"></span>
	<span class="icon-bar"></span>
</button>
<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	<div class="nav">
		<div class="width-row">
			<ul>
				<li <?php if ($this->page_name == 'Home') { echo 'class="active_menu"';} ?> >
					<a href="<?php echo base_url('/'); ?>">Home</a>
                    
				</li>
				<li class="has_children">
            <a href="#">MY ACCOUNT</a>
            <ul class="sub-menu">
							<li <?php if ($this->page_name == 'account_info') { echo 'class="active_menu"';} ?>><a href="<?php echo base_url('front/travel_professional/account_info'); ?>">MY INFO</a></li>
							<li <?php if($this->page_name == "Change Password"){ echo "class='active_menu'"; } ?>><a href="<?php echo base_url(); ?>front/travel_professional/password_update">Change Password</a></li>
						  <li <?php if ($this->page_name == 'agent markup') { echo 'class="active_menu"';} ?>><a href="<?=base_url('front/travel_professional/agent_markup');?>">AGENT MARKUP</a></li>
						</ul>
				</li>
				<li class="has_children">
            <a href="#">Reservations</a>
            <ul class="sub-menu">
	            <li <?php if($this->page_name == "Pending Reservations"){ echo "class='active_menu'"; } ?>>
	            	<a href="<?php echo base_url(); ?>user/reservation/request_submitted">Pending Reservations</a>
	            </li>
							<li <?php if($this->page_name == "Confirmed Reservations"){ echo "class='active_menu'"; } ?>>
								<a href="<?php echo base_url(); ?>user/reservation/confirm_reservation">Hotel Reservations</a>
							</li>
							<li <?php if($this->page_name == "Activities Reservations"){ echo "class='active_menu'"; } ?>>
								<a href="<?php echo base_url(); ?>user/reservation/activities_reservations">Activities Reservations</a>
							</li>
							<li <?php if($this->page_name == "Activities Reservations"){ echo "class='active_menu'"; } ?>>
								<a href="<?=base_url('front/travel_professional/incentives');?>">INCENTIVES</a>
							</li>
							<li <?php if($this->page_name == "Faq"){ echo "class='active_menu'"; } ?>><a href="<?php echo base_url(); ?>front/travel_professional/faqs">Travel Professional FAQ</a></li>
						</ul>
				</li>
        <li>
        	<li <?php if($this->page_name == "Coupons"){ echo "class='active_menu'" ; } ?>><a href="<?php echo base_url(); ?>user/legal-corner/coupons">Coupons</a></li>
        </li>
        <li <?php if($this->page_name == "EVENT TICKETS"){ echo "class='active_menu'" ; } ?>><a href="<?php echo base_url(); ?>user/legal-corner/event_tickets">EVENT TICKETS</a></li>
				<li <?php if($this->page_name == "Travel Insurance"){ echo "class='active_menu'" ; } ?>><a href="<?php echo base_url(); ?>user/legal-corner/travelinsurance">Travel Insurance</a></li>
				<li class="has_children <?php if($this->parent_menu == 'legalcorner'){ echo 'active_menu';}?>">
					<a href="#">Legal Corner</a>
					 <ul class="sub-menu">
						<li <?php if($this->page_name == "Terms and Conditions"){ echo "class='active_menu'"; } ?>><a href="<?php echo base_url(); ?>user/legal-corner/terms_condition">Terms and Conditions</a></li>
						<li <?php if($this->page_name == "Privacy Policy"){ echo "class='active_menu'"; } ?>><a href="<?php echo base_url(); ?>user/legal-corner/privacy_policy">Privacy Policy</a></li>
						<li <?php if($this->page_name == "Intellectual Property Policy"){ echo "class='active_menu'"; } ?>><a href="<?php echo base_url(); ?>user/legal-corner/property_policy">Intellectual Property Policy</a></li>
						<li <?php if($this->page_name == "DMCA Policy"){ echo "class='active_menu'"; } ?>><a href="<?php echo base_url(); ?>user/legal-corner/dmca_policy">DMCA Policy</a></li>
						<li <?php if($this->page_name == "Bitcoin Policy"){ echo "class='active_menu'"; } ?>><a href="<?php echo base_url(); ?>user/legal-corner/bitcoin_policy">Bitcoin Policy</a></li>
						<li <?php if($this->page_name == "We Care for Your Security"){ echo "class='active_menu'"; } ?>><a href="<?php echo base_url(); ?>user/legal-corner/security_statement">Security Statement</a></li>
					 </ul>  
				</li>
				<li <?php if($this->page_name == "Contact Us"){ echo "class='active_menu'" ; } ?>><a href="<?php echo base_url(); ?>user/contactus">Contact Us</a></li>
			</ul>
		</div>
	</div>
</div>