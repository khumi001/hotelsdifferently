<?php 
$this->parent_menu = isset($this->parent_menu) ? $this->parent_menu : null;

if( !empty($this->session->userdata['valid_user']['var_usertype']) && $this->session->userdata['valid_user']['var_usertype'] =='U'){ ?>
 <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
	<span class="sr-only">Toggle navigation</span>
	<span class="icon-bar"></span>
	<span class="icon-bar"></span>
	<span class="icon-bar"></span>
</button>
<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	<div class="nav">
		<div class="width-row">
			<ul>
				<li <?php if($this->page_name == "Home"){ echo "class='active_menu'" ; } ?>><a href="<?php echo base_url(); ?>user/home">Home</a>
				<li class="has_children <?php if($this->parent_menu == 'myaccount'){ echo 'active_menu';}?>">
					<a href="#">My Account</a>
					<ul class="sub-menu">
						<li <?php if($this->page_name == "My Info"){ echo "class='active_menu'"; } ?>><a href="<?php echo base_url(); ?>user/myaccount/info">My Info</a></li>
						<li <?php if($this->page_name == "My Coupons"){ echo "class='active_menu'"; } ?>><a href="<?php echo base_url(); ?>user/myaccount/coupons">My Coupons</a></li>
						
						<li <?php if($this->page_name == "REFER A FRIEND"){ echo "class='active_menu'"; } ?>><a href="<?php echo base_url(); ?>user/myaccount/refer_friend">Refer a Friend</a></li>
						
						<li <?php if($this->page_name == "Change Password"){ echo "class='active_menu'"; } ?>><a href="<?php echo base_url(); ?>user/myaccount/change_pass">Change Password</a></li>
						<li <?php if($this->page_name == "Change Email Address"){ echo "class='active_menu'"; } ?>><a href="<?php echo base_url(); ?>user/myaccount/change_email">Change Email Address</a></li>
					</ul>    
				</li>
				<li class="has_children <?php if($this->parent_menu == 'reservation'){ echo 'active_menu';}?>">
					<a href="#">Reservations</a>
					<ul class="sub-menu">
						<li <?php if($this->page_name == "Pending Reservations"){ echo "class='active_menu'"; } ?>><a href="<?php echo base_url(); ?>user/reservation/request_submitted">Pending Reservations</a></li>
						<!--
						<li <?php if($this->page_name == "Quotes"){ echo "class='active_menu'"; } ?>><a href="<?php echo base_url(); ?>user/reservation/quotes">Quotes</a></li>
						-->
						<li <?php if($this->page_name == "Confirmed Reservations"){ echo "class='active_menu'"; } ?>><a href="<?php echo base_url(); ?>user/reservation/confirm_reservation">Hotel Reservations</a></li>
						<li <?php if($this->page_name == "Confirmed Reservations"){ echo "class='active_menu'"; } ?>><a href="<?php echo base_url(); ?>user/reservation/confirm_reservation">Activity Reservations</a></li>
						<li <?php if($this->page_name == "Faq"){ echo "class='active_menu'"; } ?>><a href="<?php echo base_url(); ?>user/legal-corner/faqs">FAQ</a></li>
					</ul>  
				</li>
				<li <?php if($this->page_name == "Coupons"){ echo "class='active_menu'" ; } ?>><a href="<?php echo base_url(); ?>user/legal-corner/coupons">Coupons</a></li>
				<li <?php if($this->page_name == "EVENT TICKETS"){ echo "class='active_menu'" ; } ?>><a href="<?php echo base_url(); ?>user/legal-corner/event_tickets">EVENT TICKETS</a></li>
				<li <?php if($this->page_name == "Travel Insurance"){ echo "class='active_menu'" ; } ?>><a href="<?php echo base_url(); ?>user/legal-corner/travelinsurance">Travel Insurance</a></li>
				<li class="has_children <?php if($this->parent_menu == 'legalcorner'){ echo 'active_menu';}?>">
					<a href="#">Legal Corner</a>
					 <ul class="sub-menu">
						<li <?php if($this->page_name == "Terms and Conditions"){ echo "class='active_menu'"; } ?>><a href="<?php echo base_url(); ?>user/legal-corner/terms_condition">Terms and Conditions</a></li>
						<li <?php if($this->page_name == "Privacy Policy"){ echo "class='active_menu'"; } ?>><a href="<?php echo base_url(); ?>user/legal-corner/privacy_policy">Privacy Policy</a></li>
						<li <?php if($this->page_name == "Intellectual Property Policy"){ echo "class='active_menu'"; } ?>><a href="<?php echo base_url(); ?>user/legal-corner/property_policy">Intellectual Property Policy</a></li>
						<li <?php if($this->page_name == "DMCA Policy"){ echo "class='active_menu'"; } ?>><a href="<?php echo base_url(); ?>user/legal-corner/dmca_policy">DMCA Policy</a></li>
						<li <?php if($this->page_name == "Bitcoin Policy"){ echo "class='active_menu'"; } ?>><a href="<?php echo base_url(); ?>user/legal-corner/bitcoin_policy">Bitcoin Policy</a></li>
						<li <?php if($this->page_name == "We Care for Your Security"){ echo "class='active_menu'"; } ?>><a href="<?php echo base_url(); ?>user/legal-corner/security_statement">Security Statement</a></li>
					 </ul>  
				</li>
				<li <?php if($this->page_name == "Contact Us"){ echo "class='active_menu'" ; } ?>><a href="<?php echo base_url(); ?>user/contactus">Contact Us</a></li>
			</ul>
		</div>
	</div>
</div>
<?php } ?>

<?php if( !empty($this->session->userdata['valid_affiliate']['var_usertype']) && $this->session->userdata['valid_affiliate']['var_usertype'] =='AF'){ ?>
 <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
	<span class="sr-only">Toggle navigation</span>
	<span class="icon-bar"></span>
	<span class="icon-bar"></span>
	<span class="icon-bar"></span>
</button>
<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	<div class="nav">
		<div class="width-row">
			<ul>
				<li <?php if ($this->page_name == 'Home') { echo 'class="active_menu"';} ?>>
					<a href="<?php echo base_url(); ?>affiliate/home">Home</a>
				</li>
				<li <?php if ($this->page_name == 'Account Info') { echo 'class="active_menu"';} ?>>
					<a href="<?php echo base_url(); ?>affiliate/home/account_info">Account Info</a>
				</li>
				<li <?php if ($this->page_name == 'Banner Link') { echo 'class="active_menu"';} ?>>
					<a href="<?php echo base_url(); ?>affiliate/banner_link">Banner/Link</a>
				</li>
				<li <?php if ($this->page_name == 'Payout Preference') { echo 'class="active_menu"';} ?>>
					<a href="<?php echo base_url(); ?>affiliate/payout_prefrence">Payout Preference</a>
				</li>
				<li <?php if ($this->page_name == 'Statistics') { echo 'class="active_menu"';} ?>>
					<a href="<?php echo base_url(); ?>affiliate/statistics">Statistics</a>
				</li>
				<li <?php if ($this->page_name == 'Payout History') { echo 'class="active_menu"';} ?>>
					<a href="<?php echo base_url(); ?>affiliate/payout_history">Payout History</a>
				</li>
				<li <?php if ($this->page_name == 'Affiliate Guide') { echo 'class="active_menu"';} ?>>
					<a href="<?php echo base_url(); ?>affiliate/affiliate_guide">Affiliate Guide</a>
				</li>
				<li <?php if ($this->page_name == 'Contact Us') { echo 'class="active_menu"';} ?>>
					<a href="<?php echo base_url(); ?>affiliate/contact_us">Contact Us</a>
				</li>
			</ul>
		</div>
	</div>
</div>
<?php } ?>

<?php if( !empty($this->session->userdata['valid_user']['var_usertype']) && $this->session->userdata['valid_user']['var_usertype'] =='TF'){ ?>
 
 	<?php echo $this->load->view('front/includes/travel_professional/navigation'); ?>
    
<?php } ?>
<?php if( !empty($this->session->userdata['valid_user']['var_usertype']) && $this->session->userdata['valid_user']['var_usertype'] =='EN' && $this->session->userdata['valid_user']['entity_parent_account'] == 0){ ?>

    <?php echo $this->load->view('front/includes/entity/navigation'); ?>

<?php } ?>
<?php if( !empty($this->session->userdata['valid_user']['var_usertype']) && $this->session->userdata['valid_user']['var_usertype'] =='EN' && $this->session->userdata['valid_user']['entity_parent_account'] != 0){ ?>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <div class="nav">
            <div class="width-row">
                <ul>
                    <li <?php if($this->page_name == "Home"){ echo "class='active_menu'" ; } ?>><a href="<?php echo base_url(); ?>user/home">Home</a>
                    <li class="has_children <?php if($this->parent_menu == 'myaccount'){ echo 'active_menu';}?>">
                        <a href="#">My Account</a>
                        <ul class="sub-menu">
                            <li <?php if($this->page_name == "My Info"){ echo "class='active_menu'"; } ?>><a href="<?php echo base_url(); ?>user/myaccount/info">My Info</a></li>
                            <li <?php if($this->page_name == "My Coupons"){ echo "class='active_menu'"; } ?>><a href="<?php echo base_url(); ?>user/myaccount/coupons">My Coupons</a></li>

                            <li <?php if($this->page_name == "REFER A FRIEND"){ echo "class='active_menu'"; } ?>><a href="<?php echo base_url(); ?>user/myaccount/refer_friend">Refer a Friend</a></li>

                            <li <?php if($this->page_name == "Change Password"){ echo "class='active_menu'"; } ?>><a href="<?php echo base_url(); ?>user/myaccount/change_pass">Change Password</a></li>
                            <li <?php if($this->page_name == "Change Email Address"){ echo "class='active_menu'"; } ?>><a href="<?php echo base_url(); ?>user/myaccount/change_email">Change Email Address</a></li>
                        </ul>
                    </li>
                    <li class="has_children <?php if($this->parent_menu == 'reservation'){ echo 'active_menu';}?>">
                        <a href="#">Reservations</a>
                        <ul class="sub-menu">
                            <li <?php if($this->page_name == "Pending Reservations"){ echo "class='active_menu'"; } ?>><a href="<?php echo base_url(); ?>user/reservation/request_submitted">Pending Reservations</a></li>
                            <!--
						<li <?php if($this->page_name == "Quotes"){ echo "class='active_menu'"; } ?>><a href="<?php echo base_url(); ?>user/reservation/quotes">Quotes</a></li>
						-->
                            <li <?php if($this->page_name == "Confirmed Reservations"){ echo "class='active_menu'"; } ?>><a href="<?php echo base_url(); ?>user/reservation/confirm_reservation">Hotel Reservations</a></li>
                            <li <?php if($this->page_name == "Confirmed Reservations"){ echo "class='active_menu'"; } ?>><a href="<?php echo base_url(); ?>user/reservation/confirm_reservation">Activity Reservations</a></li>
                            <li <?php if($this->page_name == "Faq"){ echo "class='active_menu'"; } ?>><a href="<?php echo base_url(); ?>user/reservation/faqs">FAQ</a></li>
                        </ul>
                    </li>
                    <li <?php if($this->page_name == "Coupons"){ echo "class='active_menu'" ; } ?>><a href="<?php echo base_url(); ?>user/legal-corner/coupons">Coupons</a></li>
                    <li <?php if($this->page_name == "EVENT TICKETS"){ echo "class='active_menu'" ; } ?>><a href="<?php echo base_url(); ?>user/legal-corner/event_tickets">EVENT TICKETS</a></li>
                    <li <?php if($this->page_name == "Travel Insurance"){ echo "class='active_menu'" ; } ?>><a href="<?php echo base_url(); ?>user/legal-corner/travelinsurance">Travel Insurance</a></li>
                    <li class="has_children <?php if($this->parent_menu == 'legalcorner'){ echo 'active_menu';}?>">
                        <a href="#">Legal Corner</a>
                        <ul class="sub-menu">
                            <li <?php if($this->page_name == "Terms and Conditions"){ echo "class='active_menu'"; } ?>><a href="<?php echo base_url(); ?>user/legal-corner/terms_condition">Terms and Conditions</a></li>
                            <li <?php if($this->page_name == "Privacy Policy"){ echo "class='active_menu'"; } ?>><a href="<?php echo base_url(); ?>user/legal-corner/privacy_policy">Privacy Policy</a></li>
                            <li <?php if($this->page_name == "Intellectual Property Policy"){ echo "class='active_menu'"; } ?>><a href="<?php echo base_url(); ?>user/legal-corner/property_policy">Intellectual Property Policy</a></li>
                            <li <?php if($this->page_name == "DMCA Policy"){ echo "class='active_menu'"; } ?>><a href="<?php echo base_url(); ?>user/legal-corner/dmca_policy">DMCA Policy</a></li>
                            <li <?php if($this->page_name == "Bitcoin Policy"){ echo "class='active_menu'"; } ?>><a href="<?php echo base_url(); ?>user/legal-corner/bitcoin_policy">Bitcoin Policy</a></li>
                            <li <?php if($this->page_name == "We Care for Your Security"){ echo "class='active_menu'"; } ?>><a href="<?php echo base_url(); ?>user/legal-corner/security_statement">Security Statement</a></li>
                        </ul>
                    </li>
                    <li <?php if($this->page_name == "Contact Us"){ echo "class='active_menu'" ; } ?>><a href="<?php echo base_url(); ?>user/contactus">Contact Us</a></li>
                </ul>
            </div>
        </div>
    </div>

<?php } ?>

