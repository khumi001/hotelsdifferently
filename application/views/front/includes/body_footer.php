<!-- BEGIN FOOTER -->
<footer class="footer_scroll">
    <div class="width-row row padding-left-right-20 margin-bottom-15">
        <div class="col-md-5 foot_wid">
            <h2 class="foot_title">Information</h2>
            <div class="policy_link">
                <a href="<?php echo base_url(); ?>user/legal-corner/aboutus">About Us</a>
				<!-- <a href="<?php echo base_url();?>user/legal-corner/ourfuture">Our Future</a> -->
				<!-- <a href="<?php echo base_url(); ?>blog" class="main_links" target="_blank">Blog</a> -->
                <a href="http://hotels.softech.website/user/legal-corner/coupons">Individual Members</a>
                <a href="http://hotels.softech.website/user/legal-corner/coupons">Travel Professionals</a>
                <a href="http://hotels.softech.website/user/legal-corner/coupons">Business Entities</a>
                <a href="http://hotels.softech.website/user/legal-corner/coupons">Non-Profit & Government Entities</a>
				<a href="<?php echo base_url();?>user/legal-corner/coupons">Coupons</a>
				<!-- <a href="<?php echo base_url();?>user/legal-corner/event-tickets">Event Tickets</a> -->
				<a href="<?php echo base_url();?>user/legal-corner/travelinsurance">Travel Insurance</a>
                <a href="<?php echo base_url(); ?>user/contactus">Contact Us</a>
            </div>
        </div>
        <div class="col-md-5 foot_wid">
            <h2 class="foot_title">Legal Corner</h2>
            <div class="policy_link">
                <a href="<?php echo base_url(); ?>user/legal-corner/terms-condition">Terms and Conditions</a>
                <a href="<?php echo base_url(); ?>user/legal-corner/privacy-policy">Privacy Policy</a>
                <a href="<?php echo base_url(); ?>user/legal-corner/property-policy">Intellectual Property Policy</a>
                <a href="<?php echo base_url(); ?>user/legal-corner/dmca-Policy">DMCA Policy</a>
                <a href="<?php echo base_url(); ?>user/legal-corner/bitcoin-policy">Bitcoin Policy</a>
                <a href="<?php echo base_url();?>user/legal-corner/security-statement">Security Statement</a>
            </div>
        </div>
        <div class="col-md-2 foot_wid">
            <h2 class="foot_title">Follow Us On</h2>
            <div class="sc_links">
                <div class="sc_links">
                    <a href="https://www.facebook.com/whotelsgroup" target="_blank" class="fb_link">Facebook</a>
                    <a href="https://www.twitter.com/wholesalehotels" class="tw_link" target="_blank">Twitter</a>
                    <a href="https://plus.google.com/u/0/116826620841541368640" target="_blank" class="gp_link">Google+</a>
                    <a href="https://www.pinterest.com/wholesalehotelsgroup/ " class="pn_link" target="_blank">Pinterest</a>
                    <a href="https://www.instagram.com/wholesalehotelsgroup/" class="ig_link" target="_blank">Instagram</a>
                    <a href="https://www.linkedin.com/company/wholesale-hotels-group " class="linke_link" target="_blank">LinkedIn</a>
                    <!--Need to change style.css-->
                </div>
            </div>
        </div>
        <!--        <div class="foot_4">
                    
                </div>-->
        <div class="clear"></div>
    </div>
    <div class="width-row copy-right" style="padding: 10px 0 0 8px;">
        <span class="copy-span" style="float:left; display: block; height: 40px; line-height: 40px"> &#169; Copyright 2012-2017 by Wholesale Hotels Group. All Rights Reserved.</span>
        <div class="footer-icons" style="float:right;">
             <span class="itan_footer">
                <img src="<?php echo base_url(); ?>public/assets/img/asta_logo.jpg" alt="asta" width="58" height="40">
            </span>
	   		<span style=""><img src="<?php echo base_url(); ?>public/assets/img/itan-logo.png" height="45" width="45" alt="titan"></span>
			<span style="display:inline-block;vertical-align:middle;max-width:60px; margin:0 5px;padding:0 5px;background:#fff;"><a style="display:inline-block; vertical-align:top; max-width:200px;" href="http://www.tkqlhce.com/click-8137399-10395450-1443636871000" target="_blank" rel="nofollow">
			<img  style="display:block; width:100%;border:0px; height:auto;" src="<?php echo base_url();?>/public/affilliate_theme/img/travelocity.png" width="120" height="90" alt="travelcity"/></a></span>
			<span style=""><img width="40" height="40" src="<?php echo base_url(); ?>public/assets/img/paypal_without_bg.png" alt="Paypal"></span>
            <span>
                <a href="http://www.scamadviser.com/is-hotelsdifferently.com-safe.html" target="_blank" rel="nofollow">
                    <img src="https://www.hotelsdifferently.com//public/assets/img/seal1_150.png" alt="seal" width="45" height="45">
                </a>
            </span>
            <span style=""><img  height="26" width="76"  src="<?php echo base_url(); ?>public/assets/img/comodo.png" alt="comodo"></span>
        </div>
        <div style="clear: both"></div>
    </div>

<!-- Loader -->
<div id="loading">
	<div id="loading-center">
		<div class="hotel-gif">
			<img src="<?php echo base_url(); ?>public/uploads/logo_gif.gif" alt="" />
			<p>We are working on this really hard… Please wait!</p>
		</div>
	</div>
</div>
</footer>
<!-- END FOOTER -->
