<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */
// ------------------------------------------------------------------------

/**
 * CodeIgniter Calendar Class
 *
 * This class enables the creation of calendars
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		ExpressionEngine Dev Team
 * @link		http://codeigniter.com/user_guide/libraries/calendar.html
 */
class Myckeditor {

    var $CI;
    var $account;
    var $subscription;
    var $hostPageURL;

    /**
     * Constructor
     *
     * Loads the calendar language file and sets the default time reference
     */
    public function __construct($config = array()) {
        //echo "eneter in my library";
        $this->CI = & get_instance();

        log_message('debug', "Calendar Class Initialized");
    }

    function load_ckeditor($ckid, $ckdata="") {
        require_once('lib/ckeditor/ckeditor_load_js.php');
        include ('lib/ckeditor/ckeditor_load.php');
    }

    // --------------------------------------------------------------------
}

// END CI_Calendar class11

/* End of file Calendar.php */
/* Location: ./system/libraries/Calendar.php */















