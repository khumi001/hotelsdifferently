<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */
// ------------------------------------------------------------------------

/**
 * CodeIgniter Calendar Class
 *
 * This class enables the creation of calendars
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		ExpressionEngine Dev Team
 * @link		http://codeigniter.com/user_guide/libraries/calendar.html
 */
class Myrecurly {

    var $CI;
    var $account;
    var $subscription;
    var $hostPageURL;

    /**
     * Constructor
     *
     * Loads the calendar language file and sets the default time reference
     */
    public function __construct($config = array()) {
        //echo "eneter in my library";
        $this->CI = & get_instance();

        log_message('debug', "Calendar Class Initialized");

        // Require all Recurly classes
        require_once('lib/recurly.php');

// Required for the API
        Recurly_Client::$apiKey = 'f9d2e92884414d4a912a7df2bd2a9871';

// Optional for Recurly.js:
        Recurly_js::$privateKey = 'a2575afe213e4e8688263a691cdd3839';

        
//        // Required for the API
//        Recurly_Client::$apiKey = '14ed5bf57f7b44d89abedf790a1340ff';
//
//// Optional for Recurly.js:
//        Recurly_js::$privateKey = '3cadb0a8f5e9486799ebdb693b411177';
        
        
        if ($_SERVER['HTTP_HOST'] != "localhost") {
            //$this->hostPageURL = "https://technowave.recurly.com/subscribe/gold-plan";
            $this->hostPageURL = "https://anypepper.recurly.com/subscribe/earlyestimator";
        } else {
            $this->hostPageURL = "https://technowave.recurly.com/subscribe/silver";
        }
    }

    function gethostPageURL($param) {
        $paramData = array();
        if (!empty($param)) {
            foreach ($param as $key => $value) {
                $paramData[] = $key . "=" . $value;
            }
            $paramString = implode("&", $paramData);
            return $this->hostPageURL . "?" . $paramString;
        } else {
            return $this->hostPageURL;
        }
    }

    function getAccountDetail($accountcode) {

        $accounts = Recurly_AccountList::getActive();
        foreach ($accounts as $account) {
            //print "Account: $account\n";
            // var_dump($account);
            //echo "<br><br><br><pre>";
            var_dump($account);
            if ($account->account_code == $accountcode) {
                print_r($account->hosted_login_token);
            }
        }
        return $account;
    }

    function getSubscriptionDetail($accountcode) {
        $resultArray = array();
        $subscriptions = Recurly_SubscriptionList::getForAccount($accountcode);

        $oldActiveDate = "";

        foreach ($subscriptions as $subscription) {
            // print "Subscription: $subscription\n";
//            echo "<br><br><br>";
//            echo "<pre>";
//            var_dump($subscription);
            //echo "<pre>";var_dump($subscription);
            //$expiredate=$subscription->expires_at;
            //var_dump($expiredate);$expiredate->format('Y-m-d H:i:s');exit;
            $newActiveDate = $subscription->activated_at->format('Y-m-d H:i:s');
            if ((strtotime($oldActiveDate) < strtotime($newActiveDate)) || empty($oldActiveDate)) {
                if (!empty($subscription->expires_at)) {
                    $resultArray['expires_at'] = $subscription->expires_at->format('Y-m-d H:i:s');
                }
                //exit;
                $resultArray['uuid'] = $subscription->uuid;
                $resultArray['state'] = $subscription->state;
                $resultArray['active_at'] = $newActiveDate;
                $oldActiveDate=$newActiveDate;
            }
        }
        //  print_r($resultArray);exit;
        return $resultArray;
    }

    function cancelSubscritption($uuid) {
        $subscription = Recurly_Subscription::get($uuid);
        
        try {
            $subscription->cancel();
        } catch (Exception $e) {
            
        }
       // echo "<pre>";var_dump($subscription);exit;
        
        $resultArray['uuid'] = $subscription->uuid;
        $resultArray['state'] = $subscription->state;
        $resultArray['active_at'] = $subscription->activated_at->format('Y-m-d H:i:s');
        $resultArray['expires_at'] = $subscription->expires_at->format('Y-m-d H:i:s');
      //  print_r($resultArray);
    //  exit;
      
        return $resultArray;
    }

    // --------------------------------------------------------------------
}

// END CI_Calendar class11

/* End of file Calendar.php */
/* Location: ./system/libraries/Calendar.php */















