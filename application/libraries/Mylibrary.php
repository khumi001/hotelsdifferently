<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */
// ------------------------------------------------------------------------

/**
 * CodeIgniter Calendar Class
 *
 * This class enables the creation of calendars
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		ExpressionEngine Dev Team
 * @link		http://codeigniter.com/user_guide/libraries/calendar.html
 */
class Mylibrary {

    var $CI;
    var $marialStatus;

    /**
     * Constructor
     *
     * Loads the calendar language file and sets the default time reference
     */
    public function __construct($config = array()) {
        //echo "eneter in my library";
        $this->CI = & get_instance();

        log_message('debug', "Calendar Class Initialized");

        $this->marialStatus[0] = "Select Parent(s) Marital Status";
        $this->marialStatus[1] = "Married";
        $this->marialStatus[2] = "Widowed";
        $this->marialStatus[3] = "Single";
        $this->marialStatus[4] = "Divorced/Separated";
    }
//    public function checkLogin()
//    {
//         if (!isset($this->session->userdata['valid_user'])) {
//            redirect('account/login');
//        }
//    }

    public function getCombo($param = array()) {

        //        echo "<pre>";
        //        print_r($result);
        //        echo "</pre>";
        //echo "data";exit;

        $id = $param['id'];
        $name = ($param['multiple'] == "true") ? $param['id'] . "[]" : $param['id'];
        $style = !empty($param['style']) ? "style=\"{$param['style']}\"" : "";
        $class = !empty($param['class']) ? "class=\"{$param['style']}\"" : "";

        if (!empty($param['option'])) {
            $optionHtml = "";
//            print_r($param['selected']);
            foreach ($param['option'] as $key => $value) {
                $selected = (in_array($key, $param['selected'])) ? "selected" : "";
                $optionHtml.="<option value=" . $key . " $selected>{$value}</option>";
            }
        }

        $display_output = "<select name='$name' id='$id' $class>" . $optionHtml;
        $display_output.="</select>";
        return $display_output;
    }

    function getState($param) {
        $resultArray = array();
        $result = $this->CI->toval->idtorowarray('chr_delete', 'N', 'ee_state_mst');
        $resultArray["option"][0] = "Select State";
        foreach ($result as $key => $value) {
            $resultArray["option"][$value['int_glcode']] = $value['var_name'];
        }
        if (!empty($param['selected'])) {
            $resultArray["selected"] = explode(",", $param['selected']);
        }
        return $resultArray;
        //$resultArray["option"]=
    }
    function getStateFromCode($code) {
        $resultArray = array();
        $result = $this->CI->toval->idtorowarray('var_code', $code, 'ee_state_mst');
        
        //print_r($result);exit;
        return $result[0]['int_glcode'];
        //$resultArray["option"]=
    }
     function getMarrageCode($code) {
        $result= 0;
        switch($code)
        {
            case "Married":
                $result=1;
                break;
            case "Widowed":
                $result=1;
                break;
            case "Single":
                $result=1;
                break;
            case "Divorced/Separated":
                $result=1;
                break;
            default:
                $result=0;
                break;
            
        }
        
        //print_r($result);exit;
        return $result;
        //$resultArray["option"]=
    }

    function getMaritalStatus($param) {
        $resultArray = array();
        $result = $this->CI->toval->idtorowarray('chr_delete', 'N', 'ee_state_mst');

        $resultArray["option"] = $this->marialStatus;


        if (!empty($param['selected'])) {
            $resultArray["selected"] = explode(",", $param['selected']);
        }
        return $resultArray;
        //$resultArray["option"]=
    }

    function maritalStatusDetail($id) {
        return $this->marialStatus[$id];
    }

    // --------------------------------------------------------------------
}

// END CI_Calendar class11

/* End of file Calendar.php */
/* Location: ./system/libraries/Calendar.php */