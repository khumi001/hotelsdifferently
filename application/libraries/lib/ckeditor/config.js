﻿/*
Copyright (c) 2003-2011, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config )
{
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	
//	CKEDITOR.stylesSet.add( 'body_content',
//	[
//		// Block-level styles
//		{ name : 'Body Content', element : 'div', styles : {'font-family':'12px Arial','color':'#636870','line-height': '18px', 'text-align':'justify' } }
//	]);
	
	config.extraPlugins = "cmsdata";
	config.toolbar = 'MyToolbar';
	//config.stylesSet = 'body_content';
	
	//config.contentsCss =  'http://nserver/caymanexplorer/html/theme/default/css/style.css';
	//config.bodyClass = 'inner-txt';
	config.scayt_autoStartup = true;
        //alert(config.scayt_sLang);
        config.scayt_sLang ="en_GB";
       // alert(config.scayt_sLang);
        
	config.toolbar_Default=
	[
		['Source','-','Save','NewPage','Preview','-','Templates'],
		['Cut','Copy','Paste','PasteText','PasteFromWord','-','Print', 'SpellChecker', 'Scayt'],
		['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
	   // ['Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField'],
		'/',
		['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
		['NumberedList','BulletedList','-','Outdent','Indent','Blockquote','CreateDiv'],
		['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
		['BidiLtr', 'BidiRtl'],
		['Link','Unlink','Anchor'],
		['Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe'],
		'/',
		['Styles','Format','Font','FontSize'],
		['TextColor','BGColor'],
		['Maximize', 'ShowBlocks','-','About']
	];
	
	config.toolbar_Basic =
	[
		['Source','Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink','Anchor'],
		['Cut','Copy','Paste','PasteText','PasteFromWord','-','Print', 'Scayt'],
		['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
		['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
		['Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe'],
		'/',
		['Styles','Format','Font','FontSize'],
		//['TextColor','BGColor','cmsdata'],
		['TextColor','BGColor']
	];

};
