<?php
    class Cruise_destination_model extends CI_Model
    {
        function __construct()
        {
            parent::__construct();
        }
        function SynchronizeCruiseDestinationWithApi()
        {
            $processCruiseAPIObj = new ProcessCruiseAPI();
            $fetchedCruiseDestnationList = $processCruiseAPIObj->GetCruiseDestinationList();
            
            $query = $this->db->from('dz_tourico_cruise_destination')->get();
            $dbCruiseDestinationList = $query->result_array();
            
            foreach( $fetchedCruiseDestnationList as $aFetchedCruiseDestnation )
            {
                $found = false;
                foreach($dbCruiseDestinationList as $aDbCruiseDestination)
                {
                    if($aDbCruiseDestination['tourico_destination_id']==$aFetchedCruiseDestnation['destination_id'])
                    {
                        $found = true;
                        break;
                    }
                }
                
                if($found)
                {
                    $data = array(
                                    "destination_name" =>  $aFetchedCruiseDestnation['destination_name'],
                                    "parent_id" =>  $aFetchedCruiseDestnation['parent_id']
                                );
                    $where = array( "tourico_destination_id"    =>  $aFetchedCruiseDestnation['destination_id'] );
                    
                    $this->db->update('dz_tourico_cruise_destination', $data, $where);
                }
                else
                {
                    $data = array(
                                    "destination_name" =>  $aFetchedCruiseDestnation['destination_name'],
                                    "parent_id" =>  $aFetchedCruiseDestnation['parent_id'],
                                    "tourico_destination_id"    =>  $aFetchedCruiseDestnation['destination_id']
                                );
                    $this->db->insert('dz_tourico_cruise_destination', $data); 
                }
            }
        }
    }
?>