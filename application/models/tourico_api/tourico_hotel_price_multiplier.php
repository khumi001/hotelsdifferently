<?php

class Tourico_hotel_price_multiplier extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    private function addMarkup($originalPrice,$markup){
        $markupPrice = ($originalPrice / 100) * $markup;
        return round($originalPrice + $markupPrice,2);
    }

    // add markup to the policy AMOUNT type
    function ProcessPriceForPolicy($amount)
    {
        if ($amount > 0) {
            $query = $this->db->from('markups')->where('userType', $this->session->userdata['valid_user']['var_usertype'])->get();
            $markupRes = $query->row_array();
            if (count($markupRes) > 0) {
                $markup = $markupRes['percentage'];
                return $this->addMarkup($amount,$markup);
            }
        }
    }

    function ProcessPriceOfHotel($hotelDetailList) {
        if(count($hotelDetailList) > 0 ) {
            $query = $this->db->from('markups')->where('userType', $this->session->userdata['valid_user']['var_usertype'])->get();
            $markupRes = $query->row_array();
            if (count($markupRes) > 0) {
                $markup = $markupRes['percentage'];


                ini_set('memory_limit', '1000M');
                foreach ($hotelDetailList as &$aHotelDetail) {

                    /*if($aHotelDetail['hotelName'] == "Circus Circus Hotel, Casino & Theme Park - Demo"){
                        echo $markup;
                        echo '<pre>';print_r($aHotelDetail);
                        echo '--------------------------------';
                    }*/

                    $aHotelDetail['only_min_price_inc_tax'] = $this->addMarkup($aHotelDetail['only_min_price'] + $aHotelDetail['only_min_price_tax'], $markup);
                    foreach ($aHotelDetail['room_type_list'] as &$aRoomType) {
                        foreach ($aRoomType['occupancy_list'] as &$anOccupancy) {
                            foreach ($anOccupancy['available_list'] as &$anAvailability) {

                                //$anAvailability['per_night_price'] = round($anAvailability['per_night_price'] * ( 1 + $priceMultiplier ), 2);
                                //$anAvailability['per_night_tax'] = round($anAvailability['per_night_tax'] * ( 1 + $priceMultiplier ), 2);
                                $anAvailability['per_night_price_inc_tax'] = $this->addMarkup($anAvailability['per_night_price'] + $anAvailability['per_night_tax'],$markup);

                                $anAvailability['actual_total_price'] = $anAvailability['total_price'];
                                //$anAvailability['total_price'] = round($anAvailability['total_price'] * ( 1 + $priceMultiplier ), 2);
                                //$anAvailability['total_tax'] = round($anAvailability['total_tax'] * ( 1 + $priceMultiplier ), 2);

                                $anAvailability['total_price_inc_tax'] = $this->addMarkup($anAvailability['total_price'] + $anAvailability['total_tax'],$markup);

                                foreach ($anAvailability['per_day_price'] as $index => &$perDayPrice) {
                                    $anAvailability['per_day_price_inc_tax'][$index] = $this->addMarkup($perDayPrice + $anAvailability['per_night_tax'],$markup);
                                }

                            }
                        }
                    }

                    /*if($aHotelDetail['hotelName'] == "Circus Circus Hotel, Casino & Theme Park - Demo") {
                        echo '<pre>';
                        print_r($aHotelDetail);
                        exit;
                    }*/
                }
            }
        }
        //echo '<pre>';print_r($hotelDetailList);exit;
        return $hotelDetailList;
    }

    function ProcessPriceOfHotelDetail($hotelDetailList) {
        if(count($hotelDetailList) > 0 )
        {
            $query = $this->db->from('markups')->where('userType', $this->session->userdata['valid_user']['var_usertype'])->get();
            $markupRes = $query->row_array();
            if (count($markupRes) > 0) {
                $markup = $markupRes['percentage'];
                ini_set('memory_limit', '1000M');
                foreach ($hotelDetailList as &$aHotelDetail) {
                    $aHotelDetail['only_min_price_inc_tax'] = $this->addMarkup($aHotelDetail['only_min_price'] + $aHotelDetail['only_min_price_tax'],$markup);

                    foreach ($aHotelDetail['room_type_list'] as &$aRoomType) {
                        foreach ($aRoomType['occupancy_list'] as &$anOccupancy) {
                            foreach ($anOccupancy['available_list'] as &$anAvailability) {

                                //$anAvailability['per_night_price'] = round($anAvailability['per_night_price'] * ( 1 + $priceMultiplier ), 2);
                                //$anAvailability['per_night_tax'] = round($anAvailability['per_night_tax'] * ( 1 + $priceMultiplier ), 2);
                                $anAvailability['per_night_price_inc_tax'] = $this->addMarkup($anAvailability['per_night_price'],$markup);

                                $anAvailability['actual_total_price'] = $anAvailability['total_price'];
                                $anAvailability['total_price'] = $this->addMarkup($anAvailability['total_price'],$markup);
                                $anAvailability['total_tax'] = $this->addMarkup($anAvailability['total_tax'] ,$markup);

                                $anAvailability['total_price_inc_tax'] = $anAvailability['total_price'] + $anAvailability['total_tax'];

                                foreach ($anAvailability['per_day_price'] as $index => &$perDayPrice) {
                                    $perDayPrice = $this->addMarkup(($perDayPrice + $anAvailability['per_night_tax']) ,$markup);
                                    $anAvailability['per_day_price_inc_tax'][$index] = $perDayPrice;
                                }
                            }
                        }
                    }
                }
            }
        }
        //echo '<pre>';print_r($hotelDetailList);exit;
        return $hotelDetailList;
    }

    function ProcessPriceMultiplier($hotelDetailList) {
        $hotelIdArr = array();
        foreach ($hotelDetailList as $aHotelDetail)
            $hotelIdArr[] = $aHotelDetail['hotelId'];

        $query = $this->db->from('dz_tourico_hotel_price_multiplier')->where_in('tourico_hotel_id', $hotelIdArr)->get();
        $hotDealList = $query->result_array();
        $returningDetailList = array();
        foreach ($hotelDetailList as $aHotelDetail) {
            $price = null;
            foreach ($hotDealList as $aHotDeal) {
                if ($aHotDeal['tourico_hotel_id'] == $aHotelDetail['hotelId']) {
                    $price = $aHotDeal['multiplier_in_percentage'];
                    break;
                }
            }
            $aHotelDetail['multiplier_in_percentage'] = $price;
            $returningDetailList[] = $aHotelDetail;
        }
        return $returningDetailList;
    }
	
	
	function checkPerOnSingleHotel($hotelID)
	{
		$per = 0;
		$query = $this->db->from('dz_tourico_hotel_price_multiplier')->where('tourico_hotel_id', $hotelID)->get();
		$multiplierList = $query->result_array();
		$res = $this->db->where('utype' , 1)->get('dz_tourico_hotel_price_multiplier')->result();
		$datetime = false;
		if(count($res) > 0)
		{
			$per = $res[0]->multiplier_in_percentage;
			$datetime = $res[0]->update_time;
		}
		if(count($multiplierList) > 0)
		{
			$multiplierList[0]['update_time'];
			$multiplierList[0]['multiplier_in_percentage'];
			if($datetime)
			{
				if(strtotime($multiplierList[0]['update_time']) > strtotime($datetime))
				{
					$per = $multiplierList[0]['multiplier_in_percentage'];
				}
			}
			else
			{
				$per = $multiplierList[0]['multiplier_in_percentage'];
			}
		}
		return $per;
	}
	
    /*function ProcessPriceOfHotel($hotelDetailList) {
    	//echo '<pre>';print_r($hotelDetailList);exit;
        $hotelIdArr = array();
        foreach ($hotelDetailList as $aHotelDetail)
		{
			$hotelIdArr[] = $aHotelDetail['hotelId'];
		}
		
		if(count($hotelDetailList) > 0 )
		{
			$query = $this->db->from('dz_tourico_hotel_price_multiplier')->where_in('tourico_hotel_id', $hotelIdArr)->get();
			$multiplierList = $query->result_array();
			$res = $this->db->where('utype' , 1)->get('dz_tourico_hotel_price_multiplier')->result();
			$datetime = false;
			if(count($res) > 0)
			{
				$pprice = $res[0]->multiplier_in_percentage;
				$datetime = $res[0]->update_time;
			}
			else
			{
				$pprice = 0.00;
			}

			ini_set('memory_limit','1000M');
			foreach ($hotelDetailList as &$aHotelDetail) 
			{
				$priceMultiplier = $pprice / 100;
				foreach ($multiplierList as $aMultiplier) 
				{
					if ($aMultiplier['tourico_hotel_id'] == $aHotelDetail['hotelId']) 
					{
						if($datetime)
						{
							if(strtotime($aMultiplier['update_time']) > strtotime($datetime))
							{
								$priceMultiplier = $aMultiplier['multiplier_in_percentage'] / 100;
							}
						}
						else
						{
							$priceMultiplier = $aMultiplier['multiplier_in_percentage'] / 100;
						}
						break;
					}
				}

				//$aHotelDetail['only_min_price'] = round($aHotelDetail['only_min_price'] * ( 1 + $priceMultiplier ), 2);
				//$aHotelDetail['only_min_price_tax'] = $aHotelDetail['only_min_price_tax'] * ( 1 + $priceMultiplier );
				$aHotelDetail['only_min_price_inc_tax'] = round(($aHotelDetail['only_min_price']+$aHotelDetail['only_min_price_tax']) * ( 1 + $priceMultiplier ), 2);

				foreach ($aHotelDetail['room_type_list'] as &$aRoomType) {
					foreach ($aRoomType['occupancy_list'] as &$anOccupancy) {
						foreach ($anOccupancy['available_list'] as &$anAvailability) {

							$anAvailability['per_night_price'] = round($anAvailability['per_night_price'] * ( 1 + $priceMultiplier ), 2);
							$anAvailability['per_night_tax'] = round($anAvailability['per_night_tax'] * ( 1 + $priceMultiplier ), 2);
							$anAvailability['per_night_price_inc_tax'] = $anAvailability['per_night_price'] + $anAvailability['per_night_tax'];

							$anAvailability['actual_total_price'] = $anAvailability['total_price'];
							$anAvailability['total_price'] = round($anAvailability['total_price'] * ( 1 + $priceMultiplier ), 2);
							$anAvailability['total_tax'] = round($anAvailability['total_tax'] * ( 1 + $priceMultiplier ), 2);

							$anAvailability['total_price_inc_tax'] = $anAvailability['total_price'] + $anAvailability['total_tax'];
						   
							foreach ($anAvailability['per_day_price'] as $index=>&$perDayPrice){
								$perDayPrice = round($perDayPrice * ( 1 + $priceMultiplier ), 2);

								$anAvailability['per_day_price_inc_tax'][$index] = $perDayPrice + $anAvailability['per_night_tax'];
							}

						}
					}
				}
			}	
		}
				//echo '<pre>';print_r($hotelDetailList);exit;
        return $hotelDetailList;
    }*/

    function ProcessPriceOfHotel_depreciated($hotelDetailList) {
        $hotelIdArr = array();
        foreach ($hotelDetailList as $aHotelDetail)
        {
            $hotelIdArr[] = $aHotelDetail['hotelId'];
        }

        if(count($hotelDetailList) > 0 )
        {
            $query = $this->db->from('dz_tourico_hotel_price_multiplier')->where_in('tourico_hotel_id', $hotelIdArr)->get();
            $multiplierList = $query->result_array();
            $res = $this->db->where('utype' , 1)->get('dz_tourico_hotel_price_multiplier')->result();
            $datetime = false;
            if(count($res) > 0)
            {
                $pprice = $res[0]->multiplier_in_percentage;
                $datetime = $res[0]->update_time;
            }
            else
            {
                $pprice = 0.00;
            }

            ini_set('memory_limit','1000M');
            foreach ($hotelDetailList as &$aHotelDetail)
            {
                $priceMultiplier = $pprice / 100;
                foreach ($multiplierList as $aMultiplier)
                {
                    if ($aMultiplier['tourico_hotel_id'] == $aHotelDetail['hotelId'])
                    {
                        if($datetime)
                        {
                            if(strtotime($aMultiplier['update_time']) > strtotime($datetime))
                            {
                                $priceMultiplier = $aMultiplier['multiplier_in_percentage'] / 100;
                            }
                        }
                        else
                        {
                            $priceMultiplier = $aMultiplier['multiplier_in_percentage'] / 100;
                        }
                        break;
                    }
                }

                $priceMultiplier = round($priceMultiplier,2);

                /*if($aHotelDetail['hotelName'] == "Sam's Town Hotel - Demo"){
                    echo $priceMultiplier;
                   echo 1; echo '<pre>';print_r($aHotelDetail);exit;
                }*/

                //$aHotelDetail['only_min_price'] = round($aHotelDetail['only_min_price'] * ( 1 + $priceMultiplier ), 2);
                //$aHotelDetail['only_min_price_tax'] = $aHotelDetail['only_min_price_tax'] * ( 1 + $priceMultiplier );
                $aHotelDetail['only_min_price_inc_tax'] = round(($aHotelDetail['only_min_price']+$aHotelDetail['only_min_price_tax']) * ( 1 + $priceMultiplier ), 2);

                foreach ($aHotelDetail['room_type_list'] as &$aRoomType) {
                    foreach ($aRoomType['occupancy_list'] as &$anOccupancy) {
                        foreach ($anOccupancy['available_list'] as &$anAvailability) {

                            //$anAvailability['per_night_price'] = round($anAvailability['per_night_price'] * ( 1 + $priceMultiplier ), 2);
                            //$anAvailability['per_night_tax'] = round($anAvailability['per_night_tax'] * ( 1 + $priceMultiplier ), 2);
                            $anAvailability['per_night_price_inc_tax'] = round(($anAvailability['per_night_price']+ $anAvailability['per_night_tax']) * ( 1 + $priceMultiplier ), 2);

                            $anAvailability['actual_total_price'] = $anAvailability['total_price'];
                            //$anAvailability['total_price'] = round($anAvailability['total_price'] * ( 1 + $priceMultiplier ), 2);
                            //$anAvailability['total_tax'] = round($anAvailability['total_tax'] * ( 1 + $priceMultiplier ), 2);

                            $anAvailability['total_price_inc_tax'] = round(($anAvailability['total_price']+ $anAvailability['total_tax']) * ( 1 + $priceMultiplier ), 2);;

                            foreach ($anAvailability['per_day_price'] as $index=>&$perDayPrice){
                                $anAvailability['per_day_price_inc_tax'][$index] = round(($perDayPrice+ $anAvailability['per_night_tax']) * ( 1 + $priceMultiplier ), 2);
                            }

                        }
                    }
                }
            }
        }
        //echo '<pre>';print_r($hotelDetailList);exit;
        return $hotelDetailList;
    }

    function ProcessPriceOfHotelDetail_depreciated($hotelDetailList) {
        //echo '<pre>';print_r($hotelDetailList);exit;
        $hotelIdArr = array();
        foreach ($hotelDetailList as $aHotelDetail)
        {
            $hotelIdArr[] = $aHotelDetail['hotelId'];
        }

        if(count($hotelDetailList) > 0 )
        {
            $query = $this->db->from('dz_tourico_hotel_price_multiplier')->where_in('tourico_hotel_id', $hotelIdArr)->get();
            $multiplierList = $query->result_array();
            $res = $this->db->where('utype' , 1)->get('dz_tourico_hotel_price_multiplier')->result();
            $datetime = false;
            if(count($res) > 0)
            {
                $pprice = $res[0]->multiplier_in_percentage;
                $datetime = $res[0]->update_time;
            }
            else
            {
                $pprice = 0.00;
            }

            ini_set('memory_limit','1000M');
            foreach ($hotelDetailList as &$aHotelDetail)
            {
                $priceMultiplier = $pprice / 100;
                foreach ($multiplierList as $aMultiplier)
                {
                    if ($aMultiplier['tourico_hotel_id'] == $aHotelDetail['hotelId'])
                    {
                        if($datetime)
                        {
                            if(strtotime($aMultiplier['update_time']) > strtotime($datetime))
                            {
                                $priceMultiplier = $aMultiplier['multiplier_in_percentage'] / 100;
                            }
                        }
                        else
                        {
                            $priceMultiplier = $aMultiplier['multiplier_in_percentage'] / 100;
                        }
                        break;
                    }
                }

                $priceMultiplier = round($priceMultiplier,2);

                //$aHotelDetail['only_min_price'] = round($aHotelDetail['only_min_price'] * ( 1 + $priceMultiplier ), 2);
                //$aHotelDetail['only_min_price_tax'] = $aHotelDetail['only_min_price_tax'] * ( 1 + $priceMultiplier );
                $aHotelDetail['only_min_price_inc_tax'] = round(($aHotelDetail['only_min_price']+$aHotelDetail['only_min_price_tax']) * ( 1 + $priceMultiplier ), 2);

                foreach ($aHotelDetail['room_type_list'] as &$aRoomType) {
                    foreach ($aRoomType['occupancy_list'] as &$anOccupancy) {
                        foreach ($anOccupancy['available_list'] as &$anAvailability) {

                            //$anAvailability['per_night_price'] = round($anAvailability['per_night_price'] * ( 1 + $priceMultiplier ), 2);
                            //$anAvailability['per_night_tax'] = round($anAvailability['per_night_tax'] * ( 1 + $priceMultiplier ), 2);
                            $anAvailability['per_night_price_inc_tax'] = round($anAvailability['per_night_price'] * ( 1 + $priceMultiplier ), 2);

                            $anAvailability['actual_total_price'] = $anAvailability['total_price'];
                            $anAvailability['total_price'] = round($anAvailability['total_price'] * ( 1 + $priceMultiplier ), 2);
                            $anAvailability['total_tax'] = round($anAvailability['total_tax'] * ( 1 + $priceMultiplier ), 2);

                            $anAvailability['total_price_inc_tax'] = $anAvailability['total_price'] + $anAvailability['total_tax'];

                            foreach ($anAvailability['per_day_price'] as $index=>&$perDayPrice){
                                $perDayPrice = round(($perDayPrice+$anAvailability['per_night_tax']) * ( 1 + $priceMultiplier ), 2);
                                $anAvailability['per_day_price_inc_tax'][$index] = $perDayPrice ;
                            }
                        }
                    }
                }
            }
        }
        //echo '<pre>';print_r($hotelDetailList);exit;
        return $hotelDetailList;
    }
    
    function SaveMultiplier($hotelId, $profitPercentage,$hotel_name = '') {
        $CI = & get_instance();
        $CI->load->model('admin/admin_log_model');
        $query = $this->db->from('dz_tourico_hotel_price_multiplier')->where('tourico_hotel_id', $hotelId)->get();
        if ($query->num_rows() > 0) 
		{
            $previous_multiplier = $query->row()->multiplier_in_percentage;
            $data = array(
                "multiplier_in_percentage" => $profitPercentage,
				"update_time" => date('Y-m-d H:i:s')
            );
            $where = array("tourico_hotel_id" => $hotelId);

            $this->db->update('dz_tourico_hotel_price_multiplier', $data, $where);
            //edit by tayyab for acativity log

            $log = array(
                'log_type' => 'hotel_multiplier_updated',
                'log_type_fk_id' => $hotelId, // hotel id
                'log_desc' => $hotel_name.' Price updated from ' . $previous_multiplier . ' to ' . $profitPercentage
            );
            $CI->admin_log_model->addlog($log);
        } 
		else 
		{


            $data_array = array(
                "multiplier_in_percentage" => $profitPercentage,
                "tourico_hotel_id" => $hotelId,
				"update_time" => date('Y-m-d H:i:s')
            );
            $this->db->insert('dz_tourico_hotel_price_multiplier', $data_array);
            $insert_id = $this->db->insert_id();
            
                $log = array(
                    'log_type' => 'new_hote_multiplier_added',
                    'log_type_fk_id' =>  $hotelId,// hotel id
                    'log_desc' => 'A new multiplier is added for '.$hotel_name
                );
                $CI->admin_log_model->addlog($log);
           
        }
    }

}

?>