<?php
    class Destination_model extends CI_Model
    {
        function __construct()
        {
            parent::__construct();
        }
        function SynchronizeWithApi()
        {
            $hotelSearchAPIResponseProcessObj = new HotelSearchAPIResponseProcess();
            $APIDestinationList = $hotelSearchAPIResponseProcessObj->GetDestinationList();

            $dbDestinationRes = $this->db->get('dz_tourico_destination');
            $dbDestinationList = $dbDestinationRes->result_array();
            foreach($APIDestinationList as $anAPIDestination)
            {
                $matchFound = false;
                foreach( $dbDestinationList as $aDbDestination )
                {
                    if($aDbDestination['tourico_destination_id']==$anAPIDestination['destination_id'])
                    {
                        if(
                            $aDbDestination['destination_name']!=$anAPIDestination['destination_name']
                            || $aDbDestination['destination_code']!=$anAPIDestination['destination_code']
                            || $aDbDestination['city']!=$anAPIDestination['city_name']
                            || $aDbDestination['location_name']!=$anAPIDestination['location_name']
                            || $aDbDestination['city_latitude']!=$anAPIDestination['city_latitude']
                            || $aDbDestination['city_longitude']!=$anAPIDestination['city_longitude']
                            || $aDbDestination['country_name']!=$anAPIDestination['country_name']
                        )
                        {
                            $data = array(
                               'destination_name' => $anAPIDestination['destination_name'],
                               'destination_code' => $anAPIDestination['destination_code'],
                               'city' => $anAPIDestination['city_name'],
                               'location_name' => $anAPIDestination['location_name'],
                               'city_latitude' => $anAPIDestination['city_latitude'],
                               'city_longitude' => $anAPIDestination['city_longitude'],
                               'country_name' => $anAPIDestination['country_name'],
                               'parent_destination_id'     =>  $anAPIDestination['parent_destination_id']
                            );
                            $this->db->where('tourico_destination_id', $anAPIDestination['destination_id']);
                            $this->db->update('dz_tourico_destination', $data);
                            //echo $this->db->last_query(). '<br />';
                        }
                        $matchFound = true;
                        break;
                    }
                }
                //var_dump($matchFound);
                if(!$matchFound)
                {
                    $data = array(
                        'tourico_destination_id' => $anAPIDestination['destination_id'],
                        'destination_name' => $anAPIDestination['destination_name'],
                        'destination_code' => $anAPIDestination['destination_code'],
                        'city' => $anAPIDestination['city_name'],
                        'location_name' => $anAPIDestination['location_name'],
                        'city_latitude' => $anAPIDestination['city_latitude'],
                        'city_longitude' => $anAPIDestination['city_longitude'],
                        'country_name' => $anAPIDestination['country_name'],
                        'parent_destination_id'     =>  $anAPIDestination['parent_destination_id']
                    );
                    $this->db->insert('dz_tourico_destination', $data);
                }
            }
        }
    
        function GetDestinationDetailById($destinationId)
        {
            $limit=1;
            $offset=0;
            $query = $this->db->get_where('dz_tourico_destination', array('tourico_destination_id' => $destinationId), $limit, $offset);
            return $query->row_array();
        }

        function GetDestinationCodeByDestinationName($destination_name)
        {
            $query = $this->db->get_where('dz_tourico_destination', array('destination_name' => $destination_name));
            return $query->row_array();
        }
        
        function GetSuggestionListByText($suggestion)
        {
            $this->db->where('status' , 0)->like('destination_name', $suggestion); 
            $query = $this->db->get('dz_tourico_destination');
            return $query->result_array();
        }

        function GetSuggestionListByTextFromTL($suggestion)
        {
            $this->db->like('city_name', $suggestion);
            $this->db->or_like('stateName', $suggestion);
            $this->db->or_like('dz_travellenda_cities.country_name', $suggestion);
            $this->db->join('dz_travellenda_counties', 'dz_travellenda_cities.country_code = dz_travellenda_counties.country_code');
            $query = $this->db->get('dz_travellenda_cities');
            return $query->result_array();
        }

        function GetCruiseSuggestionListByText($suggestion)
        {
            $this->db->like('destination_name', $suggestion);
            $query = $this->db->get('dz_tourico_cruise_destination');
            return $query->result_array();
        }

        function GetCruiseSuggestionLists()
        {
            $query = $this->db->get('dz_tourico_cruise_destination');
            return $query->result_array();
        }

        function GetDestinationListByParentId($parent_id)
        {
            $query = $this->db->get_where('dz_tourico_destination', array('parent_destination_id' => $parent_id));
            return $query->result_array();
        }
    }
?>