<?php
    class Cruise_line_model extends CI_Model
    {
        function __construct()
        {
            parent::__construct();
        }
        function SynchronizeCruiseLineWithApi()
        {
            $processCruiseAPIObj = new ProcessCruiseAPI();
            $fetchedCruiseLineList = $processCruiseAPIObj->GetCruiseLineList();
            
            $query = $this->db->from('dz_tourico_cruise_line')->get();
            $dbCruiseLineList = $query->result_array();
            foreach( $fetchedCruiseLineList as $aFetchedCruiseLine )
            {
                $found = false;
                foreach($dbCruiseLineList as $aDbCruiseLine)
                {
                    if($aDbCruiseLine['tourico_cruise_line_id']==$aFetchedCruiseLine['cruise_line_id'])
                    {
                        $found = true;
                        break;
                    }
                }
                if ($found)
                {
                    $data = array(
                                    "cruise_line_name" =>  $aFetchedCruiseLine['destination_name']
                                );
                    $where = array( "tourico_cruise_line_id"    =>  $aFetchedCruiseLine['cruise_line_id'] );
                    
                    $this->db->update('dz_tourico_cruise_line', $data, $where);
                }
                else
                {
                    $data = array(
                                    "cruise_line_name" =>  $aFetchedCruiseLine['destination_name'],
                                    "tourico_cruise_line_id"    =>  $aFetchedCruiseLine['cruise_line_id']
                                );
                    $this->db->insert('dz_tourico_cruise_line', $data); 
                }
            }
        }
    }
?>