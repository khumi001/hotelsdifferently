<?php
    class Car_price_multiplier_model extends CI_Model
    {
        function __construct()
        {
            parent::__construct();
        }
        
        function GetCarDestinationListWithPriceMutltiplier()
        {
            $this->db->select('*');
            $this->db->from('dz_tourico_destination');
            $this->db->join('dz_tourico_car_price_multiplier', 'dz_tourico_destination.tourico_destination_id = dz_tourico_car_price_multiplier.tourico_destination_id');
            $query = $this->db->get();
            $carLocationMultiplierList = $query->result_array();
            return $carLocationMultiplierList;
        }
        
        function SaveMultiplier( $carDestinationId, $profitPercentile )
        {
            $query = $this->db->from('dz_tourico_car_price_multiplier')->where('tourico_destination_id', $carDestinationId)->get();
            if ($query->num_rows() > 0)
            {
                $data = array(
                                "multiplier_in_percentage"  =>  $profitPercentile
                            );
                $where = array( "tourico_destination_id"  =>  $carDestinationId );
                
                $this->db->update('dz_tourico_car_price_multiplier', $data, $where);
            }
            else
            {
                $data = array(
                                "multiplier_in_percentage"  =>  $profitPercentile,
                                "tourico_destination_id"  =>  $carDestinationId
                            );
                $this->db->insert('dz_tourico_car_price_multiplier', $data); 
            }
        }
    }
?>