<?php

class quote_model extends CI_Model
{

    function add_req_quote($data)
    {
        date_default_timezone_set(AMERICA_LOS_ANGLES_TIME_ZONE);
        $fkuser = $this->session->userdata['valid_user']['id'];

        if ($data['commentvalue'] == '0') {
            $data['comments'] = '';
        }
        $req_quote = array(
            'var_city' => $data['city'],
            'var_checkin' => date('Y-m-d', strtotime($data['from'])),
            'var_checkout' => date('Y-m-d', strtotime($data['top'])),
            'fk_coupon' => $data['coupans'],
            'var_night' => $data['nights'],
            'var_room' => $data['rooms'],
            'var_adult' => $data['adults'],
            'var_child' => $data['children'],
            'var_hotelname' => $data['citname_hotely'],
            'var_rating' => $data['star_rating'],
            'var_nameofreservation' => $data['nor_fname'] . ' ' . $data['nor_lname'],
            'var_comments' => $data['comments'],
            'var_fkuser' => $fkuser,
            'var_points' => '0',
            'dt_reuested_date' => date('Y-m-d H:i:s'),
            'dt_updated_date' => date('Y-m-d H:i:s'),
            'dt_created_date' => date('Y-m-d H:i:s'),
        );
        date_default_timezone_set(AMERICA_LOS_ANGLES_TIME_ZONE);
        $this->db->insert('dz_quote', $req_quote);

        $data_array = array(
            'var_status' => 'D',
            'var_updated_date' => date('Y-m-d H:i:s'),
        );
        $this->db->where('int_glcode', $req_quote['fk_coupon']);
        $this->db->update('dz_coupons', $data_array);
        return TRUE;
    }

    function randomString($length = 8)
    {
        $str = "";
        $characters = array_merge(range('A', 'Z'), range('0', '9'));
        $max = count($characters) - 1;
        for ($i = 0; $i < $length; $i++) {
            $rand = mt_rand(0, $max);
            $str .= $characters[$rand];
        }
        $this->db->where('codeID', $str);
        $res = $this->db->get('book_hotal')->result();
        if (count($res) == 0) {
            return $str;
        } else {
            $this->randomString();
        }
    }

    function randomString1($length = 9)
    {
        $str = "";
        $characters = array_merge(range('A', 'Z'), range('0', '9'));
        $max = count($characters) - 1;
        for ($i = 0; $i < $length; $i++) {
            $rand = mt_rand(0, $max);
            $str .= $characters[$rand];
        }
        $this->db->where('confirm_no', $str);
        $res = $this->db->get('booking_detail')->result();
        if (count($res) == 0) {
            return $str;
        } else {
            $this->randomString1();
        }
    }


    function AddQuoteFromTouricoAPI($data, $ch = false, $serarray = false)
    {
        date_default_timezone_set(AMERICA_LOS_ANGLES_TIME_ZONE);
        $user = $this->session->userdata['valid_user']['id'];
        $sessData = $_SESSION;
        $randNum = $this->randomString();
        $coupon = 0;
        $usedCoupon = array();
        if (isset($_SESSION['coupons'])) {
            $chres = $this->db->where('int_glcode', $_SESSION['coupons'])->get('dz_coupons')->result();
            if (count($chres) > 0) {
                $coup = $chres[0];
                $fkuser = $this->session->userdata['valid_user']['id'];
                if ($coup->fk_user == $fkuser && $coup->var_status == 'A') {
                    if ($coup->var_coupan_type == 1) {
                        $coupon = $sessData['input_data_book_step2']['total_parcentage_amount'];
                    } else {
                        $coupon = $coup->var_couponvalue;
                    }
                    $d = array(
                        'var_status' => 'U'
                    );
                    $this->db->where('int_glcode', $_SESSION['coupons'])->update('dz_coupons', $d);
                }
                $coup->var_couponvalue = $coupon;
                $usedCoupon = $coup;
            }
        }
        $hotelId = $sessData['input_data_book_step1']['hotelId'];
        $this->load->model('tourico_api/tourico_hotel_price_multiplier');
        $percentage = $this->tourico_hotel_price_multiplier->checkPerOnSingleHotel($hotelId);
        $cardinfo = $sessData['input_data_book_card'];

        //$cardno = substr($cardinfo['cardNumber'], -4);
        //unset($cardinfo['cardNumber']);

        $lastfour = $cardinfo['lastfour'];

        $stripeseri = unserialize($serarray);
        $brand = "";
        if (isset($stripeseri['source']['brand'])) {
            $brand = $stripeseri['source']['brand'];
        }
        $hotelId = $sessData['input_data_book_step1']['hotelId'];
        $this->db->select_max('hotel_bkg');
        $hotelmax = $this->db->where('hotelId', $hotelId)->get('book_hotal')->result();
        $hotelbkgcount = $hotelmax[0]->hotel_bkg + count($sessData['input_data_book_step2']['roomInfo']);
        $bhot = array(
            "uid" => $user,
            "card_brand" => $brand,
            "stripePaymentResponse" => $serarray,
            "codeID" => $randNum,
            "Type" => 'Hotel',
            "hotelName" => $sessData['input_data_book_step1']['hotelName'],
            "Location" => $sessData['input_data_book_step1']['locationName'],
            "checkIn" => date('Y-m-d', strtotime($sessData['input_data_book_step1']['from'])),
            "checkOut" => date('Y-m-d', strtotime($sessData['input_data_book_step1']['top'])),
            "rooms" => count($sessData['input_data_book_step2']['roomInfo']),
            "date_time" => date('Y-m-d h:i:s'),
            "stripe_charge" => $ch,
            "hotel_info" => serialize($sessData['input_data_book_step1']),
            "payment_info" => serialize($sessData['input_data_book_step2']),
            "cancel_pol" => serialize($_SESSION['cancellation_policies_list']),
            "supplements" => serialize($_SESSION['supp']),
            "coupons" => $coupon,
            "raw" => serialize($data),
            "lastfour" => $lastfour,
            "cardinfo" => serialize($cardinfo),
            "percentage" => $percentage,
            "hotelLocation" => $sessData['input_data_book_step2']['hotelLocation'],
            "hotel_bkg" => $hotelbkgcount,
            "hotelId" => $hotelId,
        );
        $this->db->insert('book_hotal', $bhot);
        $id = $this->db->insert_id();
        $status = "Confirm";
        $bookno = "";
        if ($id) {
            $reservations = $data['BookHotelV3Response']['BookHotelV3Result']['ResGroup']['Reservations'];
            $i = 0;
            foreach ($reservations as $res) {
                $i++;
                if (isset($res['@attributes'])) {
                    $attr = $res['@attributes'];
                    $status = $attr['status'];
                    $ProductInfo = $res['ProductInfo'];
                    $proAttr = $ProductInfo['@attributes'];
                    $RoomExtraInfo = $ProductInfo['RoomExtraInfo'];
                    $data1 = array(
                        "confirm_no" => $this->randomString1(),
                        "order" => $i,
                        "uid" => $user,
                        "roomType" => $proAttr['roomType'],
                        "roominfo" => serialize($res),
                        "book_id" => $id,
                        "reservationId" => $attr['reservationId'],
                        "fromDate" => $attr['fromDate'],
                        "toDate" => $attr['toDate'],
                        "totalTax" => $attr['totalTax'],
                        "price" => $attr['price'],
                        "totalPublishTax" => $attr['totalPublishTax'],
                        "publishPrice" => $attr['publishPrice'],
                        "isPublish" => $attr['isPublish'],
                        "currency" => $attr['currency'],
                        "status" => $attr['status'],
                        "numOfAdults" => $attr['numOfAdults'],
                        "numOfChildren" => $attr['numOfChildren'],
                        "note" => $attr['note'],
                        "tranNum" => $attr['tranNum'],
                        "RoomExtraInfo" => serialize($RoomExtraInfo),
                        'hotelType' => "TO",
                        "policyData"=>""
                    );
                    $this->db->insert('booking_detail', $data1);
                } else {
                    foreach ($res as $re) {
                        $attr = $re['@attributes'];
                        $status = $attr['status'];
                        $ProductInfo = $re['ProductInfo'];
                        $proAttr = $ProductInfo['@attributes'];
                        $RoomExtraInfo = $ProductInfo['RoomExtraInfo'];
                        $data1 = array(
                            "confirm_no" => $this->randomString1(),
                            "uid" => $user,
                            "order" => $i,
                            "roomType" => $proAttr['roomType'],
                            "roominfo" => serialize($re),
                            "book_id" => $id,
                            "reservationId" => $attr['reservationId'],
                            "fromDate" => $attr['fromDate'],
                            "toDate" => $attr['toDate'],
                            "totalTax" => $attr['totalTax'],
                            "price" => $attr['price'],
                            "totalPublishTax" => $attr['totalPublishTax'],
                            "publishPrice" => $attr['publishPrice'],
                            "isPublish" => $attr['isPublish'],
                            "currency" => $attr['currency'],
                            "status" => $attr['status'],
                            "numOfAdults" => $attr['numOfAdults'],
                            "numOfChildren" => $attr['numOfChildren'],
                            "note" => $attr['note'],
                            "tranNum" => $attr['tranNum'],
                            "RoomExtraInfo" => serialize($RoomExtraInfo),
                            'hotelType' => "TO",
                            "policyData"=>""
                        );
                        $this->db->insert('booking_detail', $data1);
                    }

                }
            }
        }

        $book_num = $data['BookHotelV3Response']['BookHotelV3Result']['ResGroup']['@attributes']['tranNum'];
        $paid = $sessData['input_data_book_step2']['total_price_html'];
        $onreq_status = 0;
        if ($status != 'Confirm') {
            $onreq_status = 1;
        }
        $da = array(
            "book_status" => $status,
            "book_num" => $book_num,
            "onreq_status" => $onreq_status,
            "paid" => $paid
        );
        $this->db->where('id', $id)->update('book_hotal', $da);
        $this->load->helper('pdf');
        $user1 = getUserById($user);
        $this->load->library('email');
        $this->email->set_mailtype("html");
        $this->email->from(NOREPLY, SITE_NAME);
        $this->email->to($user1->var_email);

        // if you modify email content here change it to in controller/admin/pen_res too
        if ($status == 'Confirm') {
            tcpdf($id, true);
            $this->email->attach('application/helpers/tcpdf/examples/files/Invoice.pdf');
            if($user1->chr_user_type == "TF") {
                travelProVoucher($id, $user1, true);
                $this->email->attach('application/helpers/tcpdf/examples/files/voucher.pdf');
            }
            //unlink('application/helpers/tcpdf/examples/files/Invoice.pdf');
            $this->email->subject('Reservation Confirmation');
            /*$mail_body = '<p style="color:#fff;margin-bottom:10px;">Dear ' . $user1->var_fname . '</p>';
            $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Thank you for using <b>'.SITE_NAME.'<sup>sm</sup></b>.</p>';
            $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Your reservation for ' . $sessData['input_data_book_step1']['hotelName'] . ' for  for check-in ' . date(DISPLAY_DATE_FORMAT, strtotime($sessData['input_data_book_step1']['from'])) . ' and check-out ' . date(DISPLAY_DATE_FORMAT, strtotime($sessData['input_data_book_step1']['top'])) . ' in ' . $sessData['input_data_book_step1']['locationName'] . ' is now confirmed successfully and your confirmation number is <b>' . $randNum . '</b></p>';
            $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Your payment of <b>Amount that was paid</b> was successfully charged and your charges will appear as "<b>HOTELSDIFF</b>" or "<b>HOTELSDFRNTL8882872307</b>" on your statement. </p>';

            $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Again, thank you for choosing us and please always give us a try before booking your next trip!</p>';
            $mail_body .= '
                <p style="border:1px solid #000;"></p>
                <p><b style="font-size:14px; display:block; text-align:center; color:#fff;">NEED A TICKET FOR AN EVENT OR CONCERT?</b></p>
                <p style="color:#fff;">We are proud to be affiliated with TicketLiquidator which is one of the biggest names in the event ticket industry. On many occasions, you can obtain tickets for events that are even sold out. Type in a Venue, an Event or a Performer and see how much you can save!</p>

                <p><a href="https://www.ticketliquidator.com/search?q=search-by-artist-or-event&ref=cj&utm_source=cj&utm_medium=aff&utm_campaign=8137399&xtor=AL-168-[cj]-[8137399]" target="_blank"> <img style="display:block; width:100%; height:auto; " src="' . base_url() . 'public/assets/img/find-tickets.jpg" alt=""></a></p>

                <p style="border:1px solid #000;"></p>
                <p><b style="font-size:14px; display:block; text-align:center; color:#fff;">NEED INSURANCE?</b></p>
                <p style="color:#fff;">We (<strong>'.SITE_NAME.'</strong><sup>sm</sup>) do NOT offer travel insurance, however you can purchase travel insurance from a third party insurance company. Please note that we do not directly sell any types of insurance; we are solely an Affiliate to a third party insurance company.</p>
            ';
            $mail_body .= '<a style="display:inline-block; verticle-align:top; max-width:150px;" href="http://www.dpbolvw.net/click-8137399-10892804-1466615700000" target="_top">
                                <img style="display:block; width:100%; height:auto;" src="http://www.ftjcfx.com/image-8137399-10892804-1466615700000" width="125" height="125" alt="Allianz Travel Insurance" border="0"/>
                            </a>
                            <a style="display:inline-block; verticle-align:top; max-width:150px;" href="http://www.jdoqocy.com/click-8137399-11779657-1466617745000" target="_top">
                                <img style="display:block; width:100%; max-width:150px; height:auto;" src="http://www.lduhtrp.net/image-8137399-11779657-1466617745000" width="300" height="100%" alt="Allianz Travel Insurance" border="0"/>
                            </a>
                            <a style="display:inline-block; verticle-align:top; max-width:150px;" href="http://www.tkqlhce.com/click-8137399-11176385-1466616611000" target="_top">
                                <img style="display:block; width:100%; max-width:150px; height:auto;" src="http://www.tqlkg.com/image-8137399-11176385-1466616611000" width="300" height="100%" alt="" border="0"/>
                            </a>';

            $mail_body .= '
                <p style="border:1px solid #000;"></p>
                <h3 style="font-size:17px; margin:0 0 10px; font-weight:bold; text-align:center; color:#fff;">NEED A RIDE?</h3>
                <h3 style="font-size:14px; margin:15px 0 10px; font-weight:bold; text-align:center;color:#e00303;"><a href="https://www.uber.com/invite/8z1j5" target="_blank"><img src="' . base_url() . 'public/assets/img/uber-logo.png" alt="uber"></a></h3>
                            <h2 style="font-size:13px; margin:0 0 10px; color:#fff;">GET <span style="color:#fff; font-weight:bold;">$15 OFF OR MORE</span>depending on your location by signing up through the banner above! (or enter Promo Code <span style="color:#fff; font-weight:bold;">8z1j5</span> in the app)</h2>



                            <h3 style="font-size:14px; margin:0 0 10px; font-weight:bold; text-align:center;color:#e00303;"><a href="https://www.lyft.com/invite/SZILARD527631" target="_blank"><img src="' . base_url() . 'public/assets/img/lyft-logo.png" alt="LYFT"></a></h3>
                            <h2 style="font-size:13px; margin:0 0 10px; color:#fff;">GET <span style="color:#fff; font-weight:bold;">$50 OFF</span> towards your first rides by signing up through the banner above!</h2>

            ';


            $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Sincerely,</br>';
            $mail_body .= '<p style="color:#fff;margin-bottom:10px;"><b>'.SITE_NAME.'<sup>sm</sup></b><br>
                <small style="font-style: italic;">Making hotels happen for you!</small><b><sup>sm</sup></b>
                </p>';*/
            $mail_body = '<p style="color:#fff;margin-bottom:10px;">Dear ' . $user1->var_fname . '</p>';
            $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Thank you for using <b>'.SITE_NAME.'<sup>sm</sup></b>.</p>';
            $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Your reservation for ' . $sessData['input_data_book_step1']['hotelName'] . '  for check-in ' . date(DISPLAY_DATE_FORMAT, strtotime($sessData['input_data_book_step1']['from'])) . ' and check-out ' . date(DISPLAY_DATE_FORMAT, strtotime($sessData['input_data_book_step1']['top'])) . ' in ' . $sessData['input_data_book_step1']['locationName'] . ' is now confirmed successfully and your confirmation number is <b>' . $randNum . '</b></p>';
            $mail_body .= 'Your payment of '.$paid.' for your membership was successfully charged and your charges will appear as “<b>HOTELSDIFF</b>” or “<b>HOTELSDFRNTL8882872307</b>” and a receipt has been sent to you as well.';
            $mail_body .= '<p style="color:#fff !important;margin-bottom:10px;">Again, thank you for choosing us and please always give us a try before booking your next trip!</p>';
            $mail_body .= '<p style="margin-bottom:10px;color:#fff !important;">Sincerely,</br>';
            $mail_body .= '<p style="color:#fff;margin-bottom:10px;"><b>'.SITE_NAME.'<sup>sm</sup></b><br>
                <small style="font-style: italic;">Making hotels happen for you!</small><b><sup>sm</sup></b>
                </p>';
            $mail_body .= '<p style="color:#fff;margin-bottom:10px;">PS: This inbox is NOT monitored, please do NOT send us any emails here.,</br>';
            $mail_body .= '
                <p style="border:1px solid #000;"></p>
                <p><b style="font-size:14px; display:block; text-align:center; color:#fff;">NEED A TICKET FOR AN EVENT OR CONCERT?</b></p>
                <p style="color:#fff;">We are proud to be affiliated with TicketLiquidator which is one of the biggest names in the event ticket industry. On many occasions, you can obtain tickets for events that are even sold out. Type in a Venue, an Event or a Performer and see how much you can save!</p>

                <p><a href="https://www.ticketliquidator.com/search?q=search-by-artist-or-event&ref=cj&utm_source=cj&utm_medium=aff&utm_campaign=8137399&xtor=AL-168-[cj]-[8137399]" target="_blank"> <img style="display:block; width:100%; height:auto; " src="' . base_url() . 'public/assets/img/find-tickets.jpg" alt=""></a></p>

                <p style="border:1px solid #000;"></p>
                <p><b style="font-size:14px; display:block; text-align:center; color:#fff;">NEED INSURANCE?</b></p>
                <p style="color:#fff;">We (<strong>'.SITE_NAME.'</strong><sup>sm</sup>) do NOT offer travel insurance, however you can purchase travel insurance from a third party insurance company. Please note that we do not directly sell any types of insurance; we are solely an Affiliate to a third party insurance company.</p>
            ';
            $mail_body .= '<a style="display:inline-block; verticle-align:top; max-width:150px;" href="http://www.dpbolvw.net/click-8137399-10892804-1466615700000" target="_top">
                                <img style="display:block; width:100%; height:auto;" src="http://www.ftjcfx.com/image-8137399-10892804-1466615700000" width="125" height="125" alt="Allianz Travel Insurance" border="0"/>
                            </a>
                            <a style="display:inline-block; verticle-align:top; max-width:150px;" href="http://www.jdoqocy.com/click-8137399-11779657-1466617745000" target="_top">
                                <img style="display:block; width:100%; max-width:150px; height:auto;" src="http://www.lduhtrp.net/image-8137399-11779657-1466617745000" width="300" height="100%" alt="Allianz Travel Insurance" border="0"/>
                            </a>
                            <a style="display:inline-block; verticle-align:top; max-width:150px;" href="http://www.tkqlhce.com/click-8137399-11176385-1466616611000" target="_top">
                                <img style="display:block; width:100%; max-width:150px; height:auto;" src="http://www.tqlkg.com/image-8137399-11176385-1466616611000" width="300" height="100%" alt="" border="0"/>
                            </a>';

            $mail_body .= '
                <p style="border:1px solid #000;"></p>
                <h3 style="font-size:17px; margin:0 0 10px; font-weight:bold; text-align:center; color:#fff;">NEED A RIDE?</h3>
                <h3 style="font-size:14px; margin:15px 0 10px; font-weight:bold; text-align:center;color:#e00303;"><a href="https://www.uber.com/invite/8z1j5" target="_blank"><img src="' . base_url() . 'public/assets/img/uber-logo.png" alt="uber"></a></h3>
                            <h2 style="font-size:13px; margin:0 0 10px; color:#fff;">GET <span style="color:#fff; font-weight:bold;">$15 OFF OR MORE</span>depending on your location by signing up through the banner above! (or enter Promo Code <span style="color:#fff; font-weight:bold;">8z1j5</span> in the app)</h2>



                            <h3 style="font-size:14px; margin:0 0 10px; font-weight:bold; text-align:center;color:#e00303;"><a href="https://www.lyft.com/invite/SZILARD527631" target="_blank"><img src="' . base_url() . 'public/assets/img/lyft-logo.png" alt="LYFT"></a></h3>
                            <h2 style="font-size:13px; margin:0 0 10px; color:#fff;">GET <span style="color:#fff; font-weight:bold;">$50 OFF</span> towards your first rides by signing up through the banner above!</h2>

            ';
        } else {
            $this->email->subject('Booking confirmation pending');
            $mail_body = '<p style="color:#fff;margin-bottom:10px;">Dear ' . $user1->var_fname . '</p>';
            $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Thank you for using <b>'.SITE_NAME.'<sup>sm</sup></b>.</p>';
            $mail_body .= '<p style="color:#fff;margin-bottom:10px;"><b>Your reservation can not be confirmed at this time.</b></p>';
            $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Due to this amazing deal you are trying to secure, the allotment of rooms at this hotel have been sold out; however; our team of dedicated professionals negotiating with the hotel on your behalf to secure additional rooms out our advertised rate. </p>';
            $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Within the next 48 business hours the hotel will reply with a definite confirmation or decline of this reservation. </p>';
            $mail_body .= '<p style="font-weight:bold;color:#fff;margin-bottom:10px;">If your reservation date is within 3 days, we highly encourage you to perform a follow-up message immediately via our CONTACT US menu and manually request an expedited review of the On Request booking because we would need to contact our source immediately in order to get a speedy reply of confirmation or decline of your booking.</p>';
            $mail_body .= '<p style="color:#fff;margin-bottom:10px;">During this process,<b> your reservation status will be PENDING.</b></p>';
            $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Your Booking number (for identification purposes only) is: <b>'.$randNum.'</b></p>';
            $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Once the request is confirmed by the hotel, you will receive a confirmation email by <b>'.SITE_NAME.'<sup>sm</sup></b>. Should the request be declined by the hotel, you will receive an email with notification of the decline.</p>';
            $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Please call us at <a  href="tel:8882872307" style="color:#fff; text-decoration:none;font-weight:bold;">888-287-2307</a> to check the status of your reservation <b>if you do not receive a response after 48 hours <u>OR</u> if your requested check in is within 24 hours.</b></p>';
            $mail_body .= '<p style="color:#fff;margin-bottom:10px;">We will be happy to assist you with your reservation.</p>';
            $mail_body .= '<p style="color:#fff;margin-bottom:10px; font-weight:bold;">Please contact us prior to making alternate arrangements as open requests may confirm at any time up to the check in date. </p>';
            $mail_body .= '<p style="color:#fff important;margin-bottom:10px; font-size:10px;">Again, thank you for choosing us and please always give us a try before booking your next trip!</p>';
            $mail_body .= '<p style="color:#fff;margin-bottom:10px; font-style:italic;">Sincerely,</br>';
            $mail_body .= '<p style="color:#fff;margin-bottom:10px;"><b>'.SITE_NAME.'<sup>sm</sup></b><br>
                <small style="font-style: italic;">Making hotels happen for you!</small><b><sup>sm</sup></b>
                </p>';
        }

        $data['mail_body'] = $mail_body;
        $message = $this->load->view('admin/account/genral_emailtemplate', $data, TRUE);
//        echo $message;exit;
        $this->email->message($message);
        $this->email->send();

        return array("id" => $id, "usedCoupon" => $usedCoupon);

    }

    function AddQuoteFromTLAPI($data_for_one_hotel_xml, $get_data_book_step2,$hotelDetails,$data, $ch = false, $serarray = false)
    {
        /*ini_set('display_errors', 1);
        error_reporting(E_ALL);*/
        date_default_timezone_set(AMERICA_LOS_ANGLES_TIME_ZONE);
        $user = $this->session->userdata['valid_user']['id'];
        $sessData = $_SESSION;
        $randNum = $this->randomString();
        $coupon = 0;
        $usedCoupon = array();
        if (isset($_SESSION['coupons'])) {
            $chres = $this->db->where('int_glcode', $_SESSION['coupons'])->get('dz_coupons')->result();
            if (count($chres) > 0) {
                $coup = $chres[0];
                $fkuser = $this->session->userdata['valid_user']['id'];
                if ($coup->fk_user == $fkuser && $coup->var_status == 'A') {
                    if ($coup->var_coupan_type == 1) {
                        $coupon = $sessData['input_data_book_step2']['total_parcentage_amount'];
                    } else {
                        $coupon = $coup->var_couponvalue;
                    }
                    $d = array(
                        'var_status' => 'U'
                    );
                    $this->db->where('int_glcode', $_SESSION['coupons'])->update('dz_coupons', $d);
                }
                $coup->var_couponvalue = $coupon;
                $usedCoupon = $coup;
            }
        }
        $hotelId = $sessData['input_data_book_step1']['hotelId'];
        $this->load->model('tourico_api/tourico_hotel_price_multiplier');
        $percentage = $this->tourico_hotel_price_multiplier->checkPerOnSingleHotel($hotelId);
        $cardinfo = $sessData['input_data_book_card'];

        //$cardno = substr($cardinfo['cardNumber'], -4);
        //unset($cardinfo['cardNumber']);

        $lastfour = $cardinfo['lastfour'];

        $stripeseri = unserialize($serarray);
        $brand = "";
        if (isset($stripeseri['source']['brand'])) {
            $brand = $stripeseri['source']['brand'];
        }
        $hotelId = $sessData['input_data_book_step1']['hotelId'];
        $this->db->select_max('hotel_bkg');
        $hotelmax = $this->db->where('hotelId', $hotelId)->get('book_hotal')->result();
        $hotelbkgcount = $hotelmax[0]->hotel_bkg + count($sessData['input_data_book_step2']['roomInfo']);
        $poli = array(
            array(
                'message'=>$sessData['policyData']
            )
        );
        $bhot = array(
            "uid" => $user,
            "card_brand" => $brand,
            "stripePaymentResponse" => $serarray,
            "codeID" => $randNum,
            "Type" => 'Hotel',
            "hotelName" => $sessData['input_data_book_step1']['hotelName'],
            "Location" => $sessData['input_data_book_step1']['locationName'],
            "checkIn" => date('Y-m-d', strtotime($sessData['input_data_book_step1']['from'])),
            "checkOut" => date('Y-m-d', strtotime($sessData['input_data_book_step1']['top'])),
            "rooms" => count($sessData['input_data_book_step2']['roomInfo']),
            "date_time" => date('Y-m-d h:i:s'),
            "stripe_charge" => $ch,
            "hotel_info" => serialize($sessData['input_data_book_step1']),
            "payment_info" => serialize($sessData['input_data_book_step2']),
            "cancel_pol" => serialize($poli),
            "supplements" => serialize($_SESSION['supp']),
            "coupons" => $coupon,
            "raw" => serialize($data),
            "lastfour" => $lastfour,
            "cardinfo" => serialize($cardinfo),
            "percentage" => $percentage,
            "hotelLocation" => $sessData['input_data_book_step2']['hotelLocation'],
            "hotel_bkg" => $hotelbkgcount,
            "hotelId" => $hotelId,
        );
        $this->db->insert('book_hotal', $bhot);
        $id = $this->db->insert_id();
        //echo '<pre>';print_r($id);exit;
        $status = "Confirm";
        $bookno = "";
        $reservations = $data['Body']['HotelBooking'];
        if ($id) {
            /*$data1 = array(
                "confirm_no" => $this->randomString1(),
                "order" => 1,
                "uid" => $user,
                "roomType" => $hotelDetails['room_type_list'][0]['roomTypeCategory'],
                "roominfo" => serialize($sessData['input_data_book_step1']),
                "book_id" => $id,
                "reservationId" => $reservations['BookingReference'],
                "fromDate" => date('Y-m-d', strtotime($sessData['input_data_book_step1']['from'])),
                "toDate" => date('Y-m-d', strtotime($sessData['input_data_book_step1']['top'])),
                "totalTax" => 0,
                "price" => $reservations["TotalPrice"],
                "totalPublishTax" => 0,
                "publishPrice" => $reservations["TotalPrice"],
                "isPublish" => true,
                "currency" => $reservations["Currency"],
                "status" => $reservations['BookingStatus'] == 'Confirmed' ? 'Confirm' : $reservations['BookingStatus'],
                "numOfAdults" => $hotelDetails['total_adult'],
                "numOfChildren" => $hotelDetails['total_child'],
                "note" => $reservations['YourReference'],
                "tranNum" => $reservations['BookingReference'],
                "RoomExtraInfo" => serialize($sessData['input_data_book_step1']),
                'hotelType' => "TL",
                "policyData"=>$sessData['policyData']
            );
            $this->db->insert('booking_detail', $data1);*/
            $WHG_CONF = $this->randomString1();
            for($k=0;$k<$data_for_one_hotel_xml['number_of_rooms'];$k++) {
                $status = $reservations['BookingStatus'] == 'Confirmed' ? 'Confirm' : "Request";
                $priceAmount = explode("|",$get_data_book_step2['roomInfo'][$k+1]['optradio']);
                $price = $priceAmount[0];
                $data1 = array(
                    "confirm_no" => $WHG_CONF."-R".($k+1),
                    "order" => $k+1,
                    "uid" => $user,
                    "roomType" => $hotelDetails['room_type_list'][0]['roomTypeCategory'],
                    "roominfo" => serialize($sessData['input_data_book_step1']),
                    "book_id" => $id,
                    "reservationId" => $reservations['BookingReference'],
                    "fromDate" => date('Y-m-d', strtotime($sessData['input_data_book_step1']['from'])),
                    "toDate" => date('Y-m-d', strtotime($sessData['input_data_book_step1']['top'])),
                    "totalTax" => 0,
                    "price" => $price,
                    "totalPublishTax" => 0,
                    'sourceID'=>15,
                    "publishPrice" => $price,
                    "isPublish" => true,
                    "currency" => $reservations["Currency"],
                    "status" => $reservations['BookingStatus'] == 'Confirmed' ? 'Confirm' : "Request",
                    "numOfAdults" => $data_for_one_hotel_xml['adult_number'][$k],
                    "numOfChildren" => $data_for_one_hotel_xml['child_number'][$k],
                    "note" => $reservations['YourReference'],
                    "tranNum" => $reservations['BookingReference'],
                    "RoomExtraInfo" => serialize($sessData['input_data_book_step1']),
                    'hotelType' => "TL",
                    "policyData" => $sessData['policyData']
                );
                $this->db->insert('booking_detail', $data1);
            }

        }

        $book_num = $reservations['BookingReference'];
        $paid = $sessData['input_data_book_step2']['total_price_html'];
        $onreq_status = 0;
        if ($status != 'Confirm') {
            $onreq_status = 1;
        }
        $da = array(
            "book_status" => $status,
            "book_num" => $book_num,
            "onreq_status" => $onreq_status,
            "paid" => $paid
        );
        $this->db->where('id', $id)->update('book_hotal', $da);
        $this->load->helper('pdf');
        $user1 = getUserById($user);
        $this->load->library('email');
        $this->email->set_mailtype("html");
        $this->email->from(NOREPLY, SITE_NAME);
        $this->email->to($user1->var_email);

        if ($status == 'Confirm') {
            tcpdf($id, true);
            $this->email->attach('application/helpers/tcpdf/examples/files/Invoice.pdf');
            if($user1->chr_user_type == "TF") {
                travelProVoucher($id, $user1, true);
                $this->email->attach('application/helpers/tcpdf/examples/files/voucher.pdf');
            }
            //unlink('application/helpers/tcpdf/examples/files/Invoice.pdf');
            $this->email->subject('Reservation Confirmation');
            $mail_body = '<p style="color:#fff;margin-bottom:10px;">Dear ' . $user1->var_fname . '</p>';
            $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Thank you for using <b>'.SITE_NAME.'<sup>sm</sup></b>.</p>';
            $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Your reservation for ' . $sessData['input_data_book_step1']['hotelName'] . '  for check-in ' . date(DISPLAY_DATE_FORMAT, strtotime($sessData['input_data_book_step1']['from'])) . ' and check-out ' . date(DISPLAY_DATE_FORMAT, strtotime($sessData['input_data_book_step1']['top'])) . ' in ' . $sessData['input_data_book_step1']['locationName'] . ' is now confirmed successfully and your confirmation number is <b>' . $randNum . '</b></p>';
            $mail_body .= 'Your payment of '.$paid.' for your membership was successfully charged and your charges will appear as “<b>HOTELSDIFF</b>” or “<b>HOTELSDFRNTL8882872307</b>” and a receipt has been sent to you as well.';
            $mail_body .= '<p style="color:#fff !important;margin-bottom:10px;">Again, thank you for choosing us and please always give us a try before booking your next trip!</p>';
            $mail_body .= '<p style="margin-bottom:10px;color:#fff !important;">Sincerely,</br>';
            $mail_body .= '<p style="color:#fff;margin-bottom:10px;"><b>'.SITE_NAME.'<sup>sm</sup></b><br>
                <small style="font-style: italic;">Making hotels happen for you!</small><b><sup>sm</sup></b>
                </p>';
            $mail_body .= '<p style="color:#fff;margin-bottom:10px;">PS: This inbox is NOT monitored, please do NOT send us any emails here.,</br>';
            $mail_body .= '
                <p style="border:1px solid #000;"></p>
                <p><b style="font-size:14px; display:block; text-align:center; color:#fff;">NEED A TICKET FOR AN EVENT OR CONCERT?</b></p>
                <p style="color:#fff;">We are proud to be affiliated with TicketLiquidator which is one of the biggest names in the event ticket industry. On many occasions, you can obtain tickets for events that are even sold out. Type in a Venue, an Event or a Performer and see how much you can save!</p>

                <p><a href="https://www.ticketliquidator.com/search?q=search-by-artist-or-event&ref=cj&utm_source=cj&utm_medium=aff&utm_campaign=8137399&xtor=AL-168-[cj]-[8137399]" target="_blank"> <img style="display:block; width:100%; height:auto; " src="' . base_url() . 'public/assets/img/find-tickets.jpg" alt=""></a></p>

                <p style="border:1px solid #000;"></p>
                <p><b style="font-size:14px; display:block; text-align:center; color:#fff;">NEED INSURANCE?</b></p>
                <p style="color:#fff;">We (<strong>'.SITE_NAME.'</strong><sup>sm</sup>) do NOT offer travel insurance, however you can purchase travel insurance from a third party insurance company. Please note that we do not directly sell any types of insurance; we are solely an Affiliate to a third party insurance company.</p>
            ';
            $mail_body .= '<a style="display:inline-block; verticle-align:top; max-width:150px;" href="http://www.dpbolvw.net/click-8137399-10892804-1466615700000" target="_top">
                                <img style="display:block; width:100%; height:auto;" src="http://www.ftjcfx.com/image-8137399-10892804-1466615700000" width="125" height="125" alt="Allianz Travel Insurance" border="0"/>
                            </a>
                            <a style="display:inline-block; verticle-align:top; max-width:150px;" href="http://www.jdoqocy.com/click-8137399-11779657-1466617745000" target="_top">
                                <img style="display:block; width:100%; max-width:150px; height:auto;" src="http://www.lduhtrp.net/image-8137399-11779657-1466617745000" width="300" height="100%" alt="Allianz Travel Insurance" border="0"/>
                            </a>
                            <a style="display:inline-block; verticle-align:top; max-width:150px;" href="http://www.tkqlhce.com/click-8137399-11176385-1466616611000" target="_top">
                                <img style="display:block; width:100%; max-width:150px; height:auto;" src="http://www.tqlkg.com/image-8137399-11176385-1466616611000" width="300" height="100%" alt="" border="0"/>
                            </a>';

            $mail_body .= '
                <p style="border:1px solid #000;"></p>
                <h3 style="font-size:17px; margin:0 0 10px; font-weight:bold; text-align:center; color:#fff;">NEED A RIDE?</h3>
                <h3 style="font-size:14px; margin:15px 0 10px; font-weight:bold; text-align:center;color:#e00303;"><a href="https://www.uber.com/invite/8z1j5" target="_blank"><img src="' . base_url() . 'public/assets/img/uber-logo.png" alt="uber"></a></h3>
                            <h2 style="font-size:13px; margin:0 0 10px; color:#fff;">GET <span style="color:#fff; font-weight:bold;">$15 OFF OR MORE</span>depending on your location by signing up through the banner above! (or enter Promo Code <span style="color:#fff; font-weight:bold;">8z1j5</span> in the app)</h2>



                            <h3 style="font-size:14px; margin:0 0 10px; font-weight:bold; text-align:center;color:#e00303;"><a href="https://www.lyft.com/invite/SZILARD527631" target="_blank"><img src="' . base_url() . 'public/assets/img/lyft-logo.png" alt="LYFT"></a></h3>
                            <h2 style="font-size:13px; margin:0 0 10px; color:#fff;">GET <span style="color:#fff; font-weight:bold;">$50 OFF</span> towards your first rides by signing up through the banner above!</h2>

            ';
        } else {
            $this->email->subject('Booking confirmation pending');
            $mail_body = '<p style="color:#fff;margin-bottom:10px;">Dear ' . $user1->var_fname . '</p>';
            $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Thank you for using <b>'.SITE_NAME.'<sup>sm</sup></b>.</p>';
            $mail_body .= '<p style="color:#fff;margin-bottom:10px;"><b>Your reservation can not be confirmed at this time.</b></p>';
            $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Due to this amazing deal you are trying to secure, the allotment of rooms at this hotel have been sold out; however; our team of dedicated professionals negotiating with the hotel on your behalf to secure additional rooms out our advertised rate. </p>';
            $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Within the next 48 business hours the hotel will reply with a definite confirmation or decline of this reservation. </p>';
            $mail_body .= '<p style="font-weight:bold;color:#fff;margin-bottom:10px;">If your reservation date is within 3 days, we highly encourage you to perform a follow-up message immediately via our CONTACT US menu and manually request an expedited review of the On Request booking because we would need to contact our source immediately in order to get a speedy reply of confirmation or decline of your booking.</p>';
            $mail_body .= '<p style="color:#fff;margin-bottom:10px;">During this process,<b> your reservation status will be PENDING.</b></p>';
            $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Your Booking number (for identification purposes only) is: <b>'.$randNum.'</b></p>';
            $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Once the request is confirmed by the hotel, you will receive a confirmation email by <b>'.SITE_NAME.'<sup>sm</sup></b>. Should the request be declined by the hotel, you will receive an email with notification of the decline.</p>';
            $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Please call us at <a  href="tel:8882872307" style="color:#fff; text-decoration:none;font-weight:bold;">888-287-2307</a> to check the status of your reservation <b>if you do not receive a response after 48 hours <u>OR</u> if your requested check in is within 24 hours.</b></p>';
            $mail_body .= '<p style="color:#fff;margin-bottom:10px;">We will be happy to assist you with your reservation.</p>';
            $mail_body .= '<p style="color:#fff;margin-bottom:10px; font-weight:bold;">Please contact us prior to making alternate arrangements as open requests may confirm at any time up to the check in date. </p>';
            $mail_body .= '<p style="color:#fff;margin-bottom:10px; font-size:10px;">Again, thank you for choosing us and please always give us a try before booking your next trip!</p>';
            $mail_body .= '<p style="color:#fff;margin-bottom:10px; font-style:italic;">Sincerely,</br>';
            $mail_body .= '<p style="color:#fff;margin-bottom:10px;"><b>'.SITE_NAME.'<sup>sm</sup></b><br>
                <small style="font-style: italic;">Making hotels happen for you!</small><b><sup>sm</sup></b>
                </p>';
        }

        $data['mail_body'] = $mail_body;
        $message = $this->load->view('admin/account/genral_emailtemplate', $data, TRUE);
        $this->email->message($message);
        $this->email->send();

        return array("id" => $id, "usedCoupon" => $usedCoupon);

    }

    /*function AddQuoteFromTouricoAPI($data)
    {
        date_default_timezone_set(AMERICA_LOS_ANGLES_TIME_ZONE);
        $fkuser = $this->session->userdata['valid_user']['id'];

       if($data['commentvalue'] == '0')
       {
           $data['comments'] = '';
       }

       if(isset($_POST['insertHotelNameManually']))
           $hotelIdhotelId = 0;
       else
           $hotelId = $data['hotelId'];
       $req_quote = array(
           'var_city' => $data['locationName'],
           'var_checkin' => date('Y-m-d',strtotime($data['from'])),
           'var_checkout' => date('Y-m-d',strtotime($data['top'])),
           'fk_coupon' => $data['coupans'],
           'tourico_hotel_id' => $hotelId,
           'tourico_destination_id'  => $data['locationId'],
           'var_night' => $data['nights'],
           'var_room' => $data['roomNo'],
           'var_adult' => $data['adultsNo'],
           'var_child' => $data['childrenNo'],
           'var_hotelname' => $data['hotelName'],
           'var_rating' => $data['starRating'],
           'var_nameofreservation' => $data['nor_fname'].' '.$data['nor_lname'],
           'var_comments' => $data['comments'],
           'var_fkuser' => $fkuser,
           'var_points' =>'0',
           'dt_reuested_date' => date('Y-m-d H:i:s'),
           'dt_updated_date' => date('Y-m-d H:i:s'),
           'dt_created_date' => date('Y-m-d H:i:s'),
      );
       date_default_timezone_set(AMERICA_LOS_ANGLES_TIME_ZONE);
       $this->db->insert('dz_quote', $req_quote);

       $data_array = array(
           'var_status' =>'D',
           'var_updated_date' =>date('Y-m-d H:i:s'),
       );
       $this->db->where('int_glcode',$req_quote['fk_coupon']);
       $this->db->update('dz_coupons',$data_array);
       return TRUE;
    }*/
}

?>
