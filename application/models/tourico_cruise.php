<?php
class Tourico_Cruise extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    function GetCruiseLength()
    {
        return array(
                '1-2' => '1-2 Nights',
                '3-5' => '3-5 Nights',
                '6-9' => '6-9 Nights',
                '10-14' => '10-14 Nights',
                '15+' => '15+ Nights'

        );
    }

    function GetCruiseLines()
    {
        $query = $this->db->get('dz_tourico_cruise_line');
        return $query->result_array();
    }

    function GetCruisePorts()
    {
        $query = $this->db->get('dz_tourico_cruise_port');
        return $query->result_array();
    }
}
?>