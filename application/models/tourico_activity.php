<?php
class Tourico_Activity extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    function GetActivitiesNameIdPair()
    {
        return array(
            "0"      =>     "All Activities",
            "694"    =>  	"T.H.E. Pass",
            "585"    =>  	"Sightseeing Tours",
            "584"    =>  	"Boat Tours",
            "587"    =>	    "Air Tours",
            "586"    =>	    "Theater & Concerts",
            "588"    =>  	"Attraction Passes",
            "582"    =>     "Dinner Show",
            "583"    =>     "Recreational Rentals",
            "592"    =>  	"Theme Parks",
            "622"    =>     "Multiple Day Guided Tours",
            "598"    =>     "Packages & Combo Tours",
            "580"    =>     "Shopping Tours",
            "589"    =>     "Restaurants",
            "595"    =>     "Watersports",
            "631"    =>     "Golf",
            "609"    =>     "Ski",
            "601"    =>     "Shared Transfer",
            "602"    =>     "Private Transfer",
            "742"    =>     "Tours",
            "728"    =>     "Museum",
            "853"    =>     "Sporting Events",
            "693"    =>     "Other",
            "1087"   =>     "V.I.P. Transfers",
            "853"    =>     "Sporting Events",
            "853"    =>     "Sporting Events"

        );
    }
}
?>