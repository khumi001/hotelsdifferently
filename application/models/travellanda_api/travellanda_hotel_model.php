<?php
    class Travellanda_hotel_model extends CI_Model
    {
        function __construct()
        {
            parent::__construct();
			$this->load->helper('travellanda');
        }
		function cacheAllCitiesDataInDB()
		{
			ini_set('max_execution_time', 1000);
			ini_set('max_input_time', 1000);
			$travellensda = new TravellandaAPICore;
			$this->db->truncate('dz_travellenda_cities');
			$cities = $travellensda->GetCities("");
			$countries = $cities['Body']['Countries']['Country'];
			$datains = array();
			foreach($countries as $sincountry)
			{
				$CountryCode = $sincountry['CountryCode'];
				$CountryName = '';
				$count_id = '';
				$citiestList = $sincountry['Cities']['City'];
				foreach($citiestList as $city)
				{
					$CityId = $city['CityId'];
					$CityName = $city['CityName'];
					$data1 = array(
						"country_code" => $CountryCode,
						"country_name" => $CountryName,
						"city_id" => $CityId,
						"count_id" => $count_id,
						"city_name" => $CityName,
                        'stateCode' => $city['StateCode']
					);
					$datains[] = $data1;
				}
			}
			$this->db->insert_batch('dz_travellenda_cities' , $datains);
			$this->db->query('UPDATE  `dz_travellenda_cities` INNER JOIN  `dz_travellenda_counties` USING (  `country_code` ) SET dz_travellenda_cities.count_id = dz_travellenda_counties.id ,  dz_travellenda_cities.country_name = dz_travellenda_counties.country_name');
		}
		
		function cacheCitiesDataInDB($page = 1)
		{
			ini_set('max_execution_time', 500);
			$travellensda = new TravellandaAPICore;
			$tcountries = $this->db->get('dz_travellenda_counties')->result();
			$totcount = count($tcountries);
			$limit = 10;
			$nextpage = $page+1;
			echo "Total Page=".$totcount/$limit."<br>";
			if($page < $totcount/$limit)
			{
				echo "Next Page=".$nextpage."<br>";
			}
			$start = ($page * $limit) - $limit;
			$countries = $this->db->limit($limit , $start)->get('dz_travellenda_counties')->result();
			$CountryCodes = array();
			foreach($countries as $singcount)
			{
				$CountryCode = $singcount->country_code;
				$CountryName = $singcount->country_name;
				$count_id = $singcount->id;
				$cities = $travellensda->GetCities($CountryCode);
				$citiestList = $cities['Body']['Countries']['Country']['Cities']['City'];
				foreach($citiestList as $city)
				{
					$CityId = $city['CityId'];
					$CityName = $city['CityName'];
					$result = $this->db->where('country_code' , $CountryCode)->where('city_id' , $CityId)->get('dz_travellenda_cities')->result();
					if(count($result) == 0)
					{
						$data1 = array(
							"country_code" => $CountryCode,
							"country_name" => $CountryName,
							"city_id" => $CityId,
							"count_id" => $count_id,
							"city_name" => $CityName
						);
						$this->db->insert('dz_travellenda_cities' , $data1);
					}
				}
			}
		}
		
		function cacheHotelsDataInDBByPage($page = 1)
		{
			ini_set('max_execution_time', 1000);
			ini_set('max_input_time', 1000);
			$travellensda = new TravellandaAPICore;
			//$this->db->truncate('dz_travellenda_hotels');
			$tcountries = $this->db->get('dz_travellenda_counties')->result();
			$totcount = count($tcountries);
			$limit = 10;
			$nextpage = $page+1;
			echo "Total Page=".$totcount/$limit."<br>";
			if($page < $totcount/$limit)
			{
				echo "Next Page=".$nextpage."<br>";
				echo "Next Page Url=<a href='http://hbeta.matrix-intech.com/front/tourico_api/cacheHotelsDataInDBByPage/".$nextpage."'>".$nextpage."</a><br>";
			}
			$start = ($page * $limit) - $limit;
			$countries = $this->db->limit($limit , $start)->get('dz_travellenda_counties')->result();
			$CountryCodes = array();
			$datains = array();
			foreach($countries as $singcount)
			{
				$CountryCode = $singcount->country_code;
				$CountryName = $singcount->country_name;
				$count_id = $singcount->id;
				$hotels = $travellensda->GetHotels($CountryCode);
				$hotelstList = $hotels['Body']['Hotels']['Hotel'];
				foreach($hotelstList as $hotel)
				{
					$CityId = $hotel['CityId'];
					$HotelId = $hotel['HotelId'];
					$HotelName = $hotel['HotelName'];
					$data1 = array(
						"hotel_name" => $HotelName,
						"city_id" => $CityId,
						"count_id" => $count_id,
						"hotel_id" => $HotelId
					);
					$datains[] = $data1;
				}
			}
			if(count($datains) > 0)
			$this->db->insert_batch('dz_travellenda_hotels' , $datains);
		}
		
		function cacheCountirsDataInDB()
		{
			$travellensda = new TravellandaAPICore;
			$countries = $travellensda->GetCountries();
			if(isset($countries['Body']['Countries']['Country']))
			{
				$countList = $countries['Body']['Countries']['Country'];
				foreach($countList as $singcountry)
				{
					$CountryCode = $singcountry['CountryCode'];
					$CountryName = $singcountry['CountryName'];
					$result = $this->db->where('country_code' , $CountryCode)->where('country_name' , $CountryName)->get('dz_travellenda_counties')->result();
					$count_id = 0;
					if(count($result) == 0)
					{
						$data1 = array(
							"country_code" => $CountryCode,
							"country_name" => $CountryName
						);
						$this->db->insert('dz_travellenda_counties' , $data1);
						$count_id = $this->db->insert_id();
					}
					else
					{
						$count_id = $result[0]->id;
					}
				}
			}
		}
		
	}
?>