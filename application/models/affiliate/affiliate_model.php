<?php
class Affiliate_model extends CI_Model
{
    function paymentinfo($data)
    {      
        $id = $this->session->userdata['valid_affiliate']['id'];
        $this->db->where('fk_user',$id);
        
        $row = $this->db->get('dz_paymount_prefrence')->num_rows();
        if($row == '0')
        { 
            $data_array = array(
                'fk_user' => $id,
//                'var_firstname' => $data['firstname'],
//                'var_lastname' => $data['lastname'],
                'var_fullname' => $data['full_name'],
                'var_phonenumber' => $data['phone_no'],
                'var_city' => $data['city'],
                'var_state' => $data['state'],
                'var_country' => $data['country'],
                'var_paypalemail' => $data['pay_email'],
                'var_paymentmethod' => $data['method_payment'],
                'var_money' => $data['send_money'],          
                'dt_updateddate' => date('Y-m-d h:i:s'),
            );        
            $this->db->insert('dz_paymount_prefrence',$data_array);    
            
            $data_array1 = array(
                'var_payment' => $data['method_payment'],
            );        
            $this->db->where('int_glcode',$id);
            $this->db->update('dz_user',$data_array1);   
            return TRUE;            
        }
        else
        {  
            $data_array = array(
                'fk_user' => $id,
//                'var_firstname' => $data['firstname'],
//                'var_lastname' => $data['lastname'],
                'var_fullname' => $data['full_name'],
                'var_phonenumber' => $data['phone_no'],
                'var_city' => $data['city'],
                'var_state' => $data['state'],
                'var_country' => $data['country'],
                'var_paypalemail' => $data['pay_email'],
                'var_paymentmethod' => $data['method_payment'],
                'var_money' => $data['send_money'],          
                'dt_updateddate' => date('Y-m-d h:i:s'),
            );        
            $this->db->where('fk_user',$id);
            $this->db->update('dz_paymount_prefrence',$data_array);   
            
             $data_array1 = array(
                'var_payment' => $data['method_payment'],
            );        
            $this->db->where('int_glcode',$id);
            $this->db->update('dz_user',$data_array1);   
          //  return TRUE; 
            
            return TRUE;
        }        
    }
}
?>
