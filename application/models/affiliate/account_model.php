<?php

class Account_model extends CI_Model {

    function edit_info($data) {
        date_default_timezone_set('America/Los_Angeles');
        if( $data['country'] == "United States")
        {
             $data['state'] =  $data['state'];
        }
        elseif($data['country'] == "Canada"){
            $data['state'] =  $data['state2'];
        }
        else{
            $data['state'] = "";
        }
       // print_r($data);
     //   exit();
        $arr = array(
            'var_email' => $data['email'],
            'var_phone' => $data['phone1'] . '-' . $data['phone2'] . '-' . $data['phone3'],
            'var_company' => $data['company1'],
            'var_title' => $data['title1'],
            'var_address1' => $data['address'],
            'var_city' => $data['city'],
            'var_country' => $data['country'],
            'var_state' => $data['state'],
            'var_zip' => $data['zipcode'],
            'var_newsletter' => $data['news_letter'],
            'var_websiteurl' => $data['web_url'],
            'var_visitors' => $data['visitors']
        );

        $this->db->where('int_glcode', $this->userid);
        $this->db->update('dz_user', $arr);
        return TRUE;
    }

    function change_pass($data) {
        date_default_timezone_set('America/Los_Angeles');
        $oldpass = $data['old_pass'];
        $this->db->where('int_glcode', $this->userid);
        $this->db->where('var_password', base64_encode($oldpass));
        $query = $this->db->get('dz_user');
        if ($query->num_rows() == 1) {
            $content = array(
                'var_password' => base64_encode($this->input->post('new_pass')),
            );
            $this->db->where('int_glcode', $this->userid);
            $row = $this->db->update('dz_user', $content);
            if ($row) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    
    function update_prefrence($data){
       
        $data_array = array(
        'var_email'=>$data['email'],
        'var_phone'=>$data['phone1'] . '-' . $data['phone2'] . '-' . $data['phone3'],
        'var_payment'=>$data['method_payment'],
        );
        $this->db->where('int_glcode', $this->userid);
        $this->db->update('dz_user', $data_array);
        return TRUE;
    }

}

?>
