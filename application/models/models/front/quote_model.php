<?php
 class quote_model extends CI_Model{
     
     function add_req_quote($data)
     { 
        date_default_timezone_set('America/Los_Angeles');
        $fkuser = $this->session->userdata['valid_user']['id'];
       
        if($data['commentvalue'] == '0')
        {
            $data['comments'] = '';
        }
        $req_quote = array(
            'var_city' => $data['city'],
            'var_checkin' => date('Y-m-d',strtotime($data['from'])),
            'var_checkout' => date('Y-m-d',strtotime($data['top'])),
            'fk_coupon' => $data['coupans'],
            'var_night' => $data['nights'],
            'var_room' => $data['rooms'],
            'var_adult' => $data['adults'],
            'var_child' => $data['children'],
            'var_hotelname' => $data['citname_hotely'],
            'var_rating' => $data['star_rating'],
            'var_nameofreservation' => $data['nor_fname'].' '.$data['nor_lname'],
            'var_comments' => $data['comments'],
            'var_fkuser' => $fkuser,
            'var_points' =>'0',
            'dt_reuested_date' => date('Y-m-d H:i:s'),
            'dt_updated_date' => date('Y-m-d H:i:s'),
            'dt_created_date' => date('Y-m-d H:i:s'),
       );
        date_default_timezone_set('America/Los_Angeles');
        $this->db->insert('dz_quote', $req_quote);
        
        $data_array = array(
            'var_status' =>'D',
            'var_updated_date' =>date('Y-m-d H:i:s'),
        );
        $this->db->where('int_glcode',$req_quote['fk_coupon']);
        $this->db->update('dz_coupons',$data_array);
        return TRUE;
     }
     
     function AddQuoteFromTouricoAPI($data)
     {
         date_default_timezone_set('America/Los_Angeles');
         $fkuser = $this->session->userdata['valid_user']['id'];
       
        if($data['commentvalue'] == '0')
        {
            $data['comments'] = '';
        }
        
        if(isset($_POST['insertHotelNameManually']))
            $hotelId = 0;
        else
            $hotelId = $data['hotelId'];
        $req_quote = array(
            'var_city' => $data['locationName'],
            'var_checkin' => date('Y-m-d',strtotime($data['from'])),
            'var_checkout' => date('Y-m-d',strtotime($data['top'])),
            'fk_coupon' => $data['coupans'],
            'tourico_hotel_id' => $hotelId,
            'tourico_destination_id'  => $data['locationId'],
            'var_night' => $data['nights'],
            'var_room' => $data['roomNo'],
            'var_adult' => $data['adultsNo'],
            'var_child' => $data['childrenNo'],
            'var_hotelname' => $data['hotelName'],
            'var_rating' => $data['starRating'],
            'var_nameofreservation' => $data['nor_fname'].' '.$data['nor_lname'],
            'var_comments' => $data['comments'],
            'var_fkuser' => $fkuser,
            'var_points' =>'0',
            'dt_reuested_date' => date('Y-m-d H:i:s'),
            'dt_updated_date' => date('Y-m-d H:i:s'),
            'dt_created_date' => date('Y-m-d H:i:s'),
       );
        date_default_timezone_set('America/Los_Angeles');
        $this->db->insert('dz_quote', $req_quote);
        
        $data_array = array(
            'var_status' =>'D',
            'var_updated_date' =>date('Y-m-d H:i:s'),
        );
        $this->db->where('int_glcode',$req_quote['fk_coupon']);
        $this->db->update('dz_coupons',$data_array);
        return TRUE;
     }
 }
?>
