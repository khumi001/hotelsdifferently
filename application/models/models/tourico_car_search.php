<?php
class Tourico_Car_Search extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    function GetCarCompanyNameIdPair()
    {
        return array(
            "1"   =>    "Budget",
            "2"   =>	"Hertz",
            "3"   =>	"Thrifty",
            "4"   =>	"Enterprise",
            "5"   =>	"National",
            "7"   =>	"Alamo",
            "9"   =>	"Fox",
            "11"  =>    "Dollar",
            "13"  =>    "Sixt",
            "15"  =>	"Avis"

        );
    }
}
?>