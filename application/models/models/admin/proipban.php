<?php
 class proipban extends CI_Model{
     
     function getipban(){

        $this->db->select('dz_ban_ip_address.int_glcode,dz_ban_ip_address.var_createddate,dz_ban_ip_address.var_ipban,dz_ban_ip_address.var_reason,dz_user.var_username,
                           dz_user.var_email,dz_user.var_accountid,dz_ban_ip_address.var_banby');
        $this->db->from('dz_ban_ip_address');
        $this->db->join('dz_user','dz_ban_ip_address.fk_user =dz_user.int_glcode');
        $this->db->where('dz_ban_ip_address.chr_status','B');
        $result = $this->db->get()->result_array();
        return $result;
     }
     function status_change($data){
         date_default_timezone_set('America/Los_Angeles');

         $change = array(
             'chr_status' =>'A',
         );
         $this->db->where('int_glcode',$data['id']);
         $this->db->update('dz_ban_ip_address',$change);
         
         $query = $this->db->get_where('dz_ban_ip_address',array('int_glcode'=>$data['id']))->result_array();
         
         $data_array = array(
             'login_status' =>'Active',
             'int_logincnt' =>'0',
         );
         $this->db->where('int_glcode',$query[0]['fk_user']);
         $this->db->update('dz_user',$data_array);
         
         return TRUE;
     }
 }
     ?>