<?php

class Authorization_model extends CI_Model {

    function menuauthorize($data) 
	{
		
        $allmenu = array(
            'Quote Requests' => 'no',
            'Sent Quotes' => 'no',
            'Promo Codes' => 'no',
            'Confirmed Reservations'=> 'no',
			'Pending Cancellations' => 'no',
            'Pending reservations' => 'NO',
            'Cancel reservations' => 'no',
             'Processed Cancellations' => 'no',
            'Coupons' => 'no',
            'Affiliate Request' => 'no',
            'Affiliate Database' => 'no',
            'Affiliate Tracker' => 'no',
            'Affiliate Payment' => 'no',
            'Posted Payments' => 'no',
            'Chargebacks' => 'no',
            'Processed Chargebacks' => 'no',
            'Authorization' => 'no',
            'IP Address BAN' => 'no',
            'Processed IP BAN' => 'no',
            'Database Management' => 'no',
            'Login Log' => 'no',
            'Statistics' => 'no',
            'Profile' => 'no',
            'Admin Activity Log' => 'no',
            'Master account' => 'no',
        );

        foreach ($allmenu as $key => $value) {
            for ($i = 0; $i < count($data['authocheck']); $i++) {
                if ($data['authocheck'][$i] == $key) {
                    $allmenu[$key] = 'yes';
                }
            }
        }
        $authocheckarray = serialize($allmenu);
        $menuvalue = urlencode($authocheckarray);
        $dataarray = array(
            'var_authorization' => $menuvalue
        );
        $this->db->where('fk_user', $data['id']);
        $this->db->update('dz_giveauthorize', $dataarray);
        
            $data_array = array(
                  'chr_user_type' =>'A',  
                );
            $this->db->where('int_glcode',$data['id']);
            $this->db->update('dz_user',$data_array);
        return true;
    }
    
    function userdelete($data)
	{    
        $this->db->where('int_glcode',$data['id']);
        $this->db->delete('dz_user');   
        return TRUE;
    }

}

?>
