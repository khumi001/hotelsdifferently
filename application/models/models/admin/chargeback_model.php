<?php 
class Chargeback_model extends CI_Model
{
    function add_data($data){
        
        date_default_timezone_set('America/Los_Angeles');
        $user_id = $this->session->userdata['valid_adminuser']['id'];      
        $data_array = array(
             'fk_user' => $user_id,  
             'fk_affilate_chargeback'=>$data['affiliate'],
             'chr_status' => 'CB',
             'var_amount'=>$data['amount'],
             'var_comment'=>$data['comment'],
             'var_createddate' =>date("Y-m-d H:i:s"),
             'var_updateddate' =>date("Y-m-d H:i:s"),
            );
        $this->db->insert('dz_chargeback',$data_array);
            return TRUE;
    } 
    
}
?>