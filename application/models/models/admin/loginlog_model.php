<?php 
class Loginlog_model extends CI_Model
{
//    function add_loginlog($data,$usertype)
//    {   
//        $ipaddress =  $_SERVER['REMOTE_ADDR']; 
//        $data_array = array(
//            'var_email' => $data['username'],
//            'var_ipaddress' => $ipaddress,
//            'var_status' => 'Failed',
//            'chr_user_type' => $usertype,
//            'var_createddate' => date('Y-m-d H:i'),
//        );
//        $this->db->insert('dz_login_log',$data_array);   
//        $lastid = $this->db->insert_id();
//        return $lastid;        
//    }
    function add_loginlog($data)
    {   
        $this->db->select('chr_user_type');
        $this->db->where('var_email',$data['username']);
        $query = $this->db->get('dz_user')->result_array();
        $ipaddress =  $_SERVER['REMOTE_ADDR']; 
        $data_array = array(
            'var_email' => $data['username'],
            'var_ipaddress' => $ipaddress,
            'var_status' => 'Failed',
            'chr_user_type' => $query[0]['chr_user_type'],
            'var_createddate' =>date('Y-m-d H:i'),
        );
		$this->db->insert('dz_login_log',$data_array);   
        $lastid = $this->db->insert_id();
        
        return $lastid;        
    }
    function get_log()
    {
        $this->db->order_by('int_glcode', 'desc');
        $query = $this->db->get('dz_login_log');
        return $query->result_array();
    }
    function get_loginlogadmin()
    {              
        $this->db->order_by('int_glcode', 'desc');
        $this->db->where('chr_user_type','A');
        $query = $this->db->get('dz_login_log');
        return $query->result_array();
    }
    function get_loginlogmember()
    {
        $this->db->order_by('int_glcode', 'desc');
        $this->db->where('chr_user_type','U');
        $query = $this->db->get('dz_login_log');
        return $query->result_array();
    }   
    function get_loginlogaffiliates()
    {
        $this->db->order_by('int_glcode', 'desc');
        $this->db->where('chr_user_type','AF');
        $query = $this->db->get('dz_login_log');
        return $query->result_array();
    }
    function addlogincnt($cnt,$id)
    { 
        $cntvalue = $cnt + 1;
        $data_array = array(
            'int_logincnt' => $cntvalue,
        );
        $this->db->where('int_glcode',$id);
        $this->db->update('dz_user',$data_array);
        return true;        
    }
     
}
?>
