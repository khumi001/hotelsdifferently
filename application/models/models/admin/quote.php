<?php

class quote extends CI_Model {

    function sentquote_getdata($id) {
        date_default_timezone_set(AMERICA_LOS_ANGLES_TIME_ZONE);
        $this->db->select('int_fkquote');
        $this->db->from('dz_rplyquote');
        $this->db->where('int_glcode', $id);
        $query1 = $this->db->get()->result_array();

        $query = $this->db->select('dz_quote.*,dz_user.var_username,dz_user.var_accountid')
                        ->from('dz_quote')
                        ->join('dz_user', 'dz_user.int_glcode=dz_quote.var_fkuser')
                        ->where('dz_quote.int_glcode', $query1[0]['int_fkquote'])->get()->result_array();

        return $query;
    }

    function getquote() {
        date_default_timezone_set(AMERICA_LOS_ANGLES_TIME_ZONE);
        $this->db->select('
            dz_quote.int_glcode,
            dz_quote.fk_resubmit_id,
            dz_quote.var_points,
            dz_quote.dt_reuested_date,
            dz_user.var_username,
            dz_user.var_point,
            dz_user.fk_parent_account,
            dz_quote.var_city,
            dz_quote.var_checkin,
            dz_quote.var_checkout,
            dz_quote.var_room,
            dz_quote.var_hotelname,
            dz_quote.var_rating,
            dz_quote.var_fkuser');
        $this->db->from('dz_quote');
        $this->db->join('dz_user', 'dz_quote.var_fkuser = dz_user.int_glcode');
        $this->db->where('dz_quote.quote_status', 'NR');
        $query = $this->db->get()->result_array();
        return $query;
    }

    function addquote($data) {
        date_default_timezone_set(AMERICA_LOS_ANGLES_TIME_ZONE);
        $quote = array(
            'var_city' => $data['city'],
            'var_checkin' => date("Y-m-d", strtotime($data['indate'])),
            'var_checkout' => date("Y-m-d", strtotime($data['outdate'])),
            'var_night' => '02',
            'var_room' => $data['room'],
            'var_adult' => $data['adult'],
            'var_child' => $data['child'],
            'var_hotelname' => $data['hotel'],
            'var_rating' => $data['rating'],
            'var_name' => 'user',
            'var_comments' => $data['comment'],
            'var_cpolicy' => $data['policy'],
            'var_price' => $data['price'],
            'var_createddate' => date("Y-m-d H:i:s"),
        );

        $this->db->insert('dz_quote', $quote);
        return TRUE;
    }

    function getsentquote() {
        date_default_timezone_set(AMERICA_LOS_ANGLES_TIME_ZONE);

        $query = $this->db->order_by('var_createddate', 'desc')->get('dz_rplyquote')->result_array();
         
        for ($i = 0; $i < count($query); $i++) {
            $this->db->where('int_glcode', $query[$i]['int_fkquote']);
            $quote = $this->db->get('dz_quote')->result_array();

            $this->db->where('int_glcode', $quote[0]['var_fkuser']);
            $quoteuser = $this->db->get('dz_user')->result_array();

            $this->db->where('int_glcode', $query[$i]['fk_replayuser_id']);
            $quotesenduser = $this->db->get('dz_user')->result_array();

            $data[] = array(
                'replayid' => $query[$i]['int_glcode'],
                'dt_created_date' => $query[$i]['var_createddate'],
                'var_username' => $quoteuser[0]['var_username'],
                'var_email' => $quoteuser[0]['var_email'],
                'var_accountid' => $quoteuser[0]['var_accountid'],
                'var_uniq_quoteid' => $query[$i]['var_uniq_quoteid'],
                'sendby' => $quotesenduser[0]['var_username'],
            );
        }
//        print_r($data); exit();
        return $data;
    }

    function random_num($numAlpha = 5, $numNonAlpha = 4) {
        date_default_timezone_set(AMERICA_LOS_ANGLES_TIME_ZONE);
        $listAlpha = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $listNonAlpha = '0123456789';
        return str_shuffle(
                substr(str_shuffle($listAlpha), 0, $numAlpha) .
                substr(str_shuffle($listNonAlpha), 0, $numNonAlpha)
        );
    }

    function rplyquote_model($data) {
//        $this->db->where('int_glcode', $data['fkquote']);
//        $main_quote = $this->db->get('dz_quote')->result_array();
//        print_r($main_quote);
//        exit;

        date_default_timezone_set(AMERICA_LOS_ANGLES_TIME_ZONE);
        $user_id = $this->session->userdata['valid_adminuser']['id'];
//        echo $user_id; exit();
        $id = $data['fkquote'];
        $c = 1;

        $query = $this->db->select('dz_quote.*,dz_user.var_username,dz_user.var_fname,dz_user.var_accountid,dz_user.var_email')
                        ->from('dz_quote')
                        ->join('dz_user', 'dz_user.int_glcode=dz_quote.var_fkuser')
                        ->where('dz_quote.int_glcode', $id)->get()->result_array();

        $this->email->set_mailtype("html");
        $this->email->from(NOREPLY, 'Admin');
        $this->email->to($query[0]['var_email']);
        $this->email->subject('Your quote is here!');
        $mail_body.='<p style="color:#fff;margin-bottom:10px;">Dear ' . $query[0]['var_fname'] . ',</p>';
        $mail_body.='<p style="color:#fff;margin-bottom:10px;">Thank you for using <strong>Wholesale Hotels Group</strong>. We would like to let you know that your requested quotes are now ready for your review. </p>';

        $mail_body.='<p style="color:#fff;margin-bottom:10px;">If you are delighted with the potential savings we were able to offer, we encourage you to pay for the reservation at your earliest convenience and we thank you for choosing us. To pay for this reservation simply login to your <strong>Wholesale Hotels Group</strong> account.</br></p>';
        $mail_body.='<p style="color:#fff;margin-bottom:10px;font-weight:600;text-decoration:underline;">IMPORTANT: </br></p>';
        $mail_body.='<p style="color:#fff;margin-bottom:10px;">Please note the following to ensure a successful and smooth booking experience:</p>';
        $mail_body.='<p style="color:#fff;margin-bottom:10px;">Full payment must be made within 24 hours of the receipt of this email, if you fail to pay the quoted amount in full, you will have to submit a new request. </p>';

        $mail_body.='<p style="color:#fff;margin-bottom:10px;">You may also find our Frequently Asked Questions (FAQ) helpful. You may access the FAQ by signing into your account with Us and selecting RESERVATIONS and then selecting FAQ.</p>';
        $mail_body.='<p style="color:#fff;margin-bottom:10px;">Again, thank you for choosing us and please always give us a try before booking your next trip!<br></p>';
        //  $mail_body.='<p>Sincerely,</br>HotelsDifferentlysm</p>';
//            $this->email->message($mail_body);
//            $this->email->send();

        for ($i = 0; $i < count($data['pricequote']); $i++) {
            if ($i == 0) {
                $data['s_site'][$i] = str_replace(',', "", $data['s_site'][$i]);
                $data['pricequote'][$i] = str_replace(',', "", $data['pricequote'][$i]);
                $data['k_site'][$i] = str_replace(',', "", $data['k_site'][$i]);
                $data['s_site'][$i] = str_replace(',', "", $data['s_site'][$i]);
                $data['s_src'][$i] = str_replace(',', "", $data['s_src'][$i]);

                $random = $this->random_num();
                if ($data['s_site'][$i] == '') {
                    $data['s_site'][$i] = 0;
                }
                if($data['resubmitid'] == ''){
                    $quote[] = array(
                    'var_fkuser' => $query[0]['int_glcode'],
                    'fk_replayuser_id' => $user_id,
                    'var_prize' => $data['pricequote'][$i],
                    'int_fkquote' => $id,
                    'var_site1' => $data['w_site'][$i],
                    'int_site1_price' => $data['k_site'][$i],
                    'var_site2' => $data['f_site'][$i],
                    'int_site2_price' => $data['s_site'][$i],
                    'var_tripadvisor' => $data['tripadvisor'][$i],
                    'var_src' => $data['src'][$i],
                    'int_src_price' => $data['s_src'][$i],
                    'var_room_type' => $data['room_type'][$i],
                    'var_address' => $data['address'][$i],
                    'var_uniq_quoteid' => $random . $i,
                    'var_NOH' => $data['hotel'][$i],
                    'chr_status' => 'UP',
                    'var_star' => $data['rating'][$i],
                    'var_policy' => $data['policy'][$i],
                    'var_comment' => $data['comment'][$i],
                    'var_createddate' => date('Y-m-d H:i:s'),
                );
                $this->db->insert('dz_rplyquote', $quote[$i]);
                }else{
                    $quote[] = array(
                    'var_fkuser' => $query[0]['int_glcode'],
                    'fk_replayuser_id' => $user_id,
                    'var_prize' => $data['pricequote'][$i],
                    'int_fkquote' => $id,
                    'chr_quote_status' => 'A',
                    'var_site1' => $data['w_site'][$i],
                    'int_site1_price' => $data['k_site'][$i],
                    'var_site2' => $data['f_site'][$i],
                    'int_site2_price' => $data['s_site'][$i],
                    'var_tripadvisor' => $data['tripadvisor'][$i],
                    'var_src' => $data['src'][$i],
                    'int_src_price' => $data['s_src'][$i],
                    'var_room_type' => $data['room_type'][$i],
                    'var_address' => $data['address'][$i],    
                    'var_uniq_quoteid' => $random . $i,
                    'var_NOH' => $data['hotel'][$i],
                    'chr_status' => 'UP',
                    'var_star' => $data['rating'][$i],
                    'var_policy' => $data['policy'][$i],
                    'var_comment' => $data['comment'][$i],
                    'var_createddate' => date('Y-m-d H:i:s'),
                );
                    $this->db->where('int_glcode',$data['resubmitid']);
                    $this->db->update('dz_rplyquote',$quote[$i]);
                }
                

                if ($data['s_site'][$i] != "") {
                    if ($data['k_site'][$i] > $data['s_site'][$i]) {
                        $saving = $data['s_site'][$i] - $data['pricequote'][$i];
                    } else {
                        $saving = $data['k_site'][$i] - $data['pricequote'][$i];
                    }
                } else {
                    $saving = $data['k_site'][$i] - $data['pricequote'][$i];
                }
                $mail_body.='<p style="color:#fff;margin-bottom:10px;"><h3 style="color:#fff;margin-bottom:10px;">Quote #' . $c++ . ':</h3></p>';
                $mail_body.='<p style="color:#fff;margin-bottom:10px;"><span style="width:48%;float:left;"><b>Quote ID: </b>' . $random . $i . '</span><span style="width:48%;margin-left:10px;"><b>Name: </b>' . $query[0]['var_nameofreservation'] . ' </span></p>';
                $mail_body.='<p style="color:#fff;margin-bottom:10px;"><span style="float:left;width:100%;"><b>Hotel: </b>' . $data['hotel'][$i] . '</span><div style="clear:both;"></div></p>';
                $mail_body.='<p style="color:#fff;margin-bottom:10px;"><span style="float:left;width:33%;"><b>Star: </b>' . $data['rating'][$i] . '</span><span style="margin-left:10px;width:33%;float:left;"><b>Rooms: </b>' . $query[0]['var_room'] . '</span><span style="margin-left:10px;width:34%;"><b>TripAdvisor: </b>' . $data['tripadvisor'][$i] . '</span></p>';
//            $mail_body.='<p style="color:#fff;margin-bottom:10px;"><span style="width:100%;float:left;margin-bottom:10px;"><b>Location: </b>' . $query[0]['var_city'] . '</span></p>';
                $mail_body.= '<p style="color:#fff;margin-bottom:10px;"><span style="width:100%;float:left;margin-bottom:10px;"><b>Comment: </b>' . $data['comment'][$i] . '</span></p></p>';
                $mail_body.='<p style="color:#fff;margin-bottom:10px;"><span style="float:left;width:100%;color:#fff;margin-bottom:20px;"><b>Cancellation Policy: </b>' . $data['policy'][$i] . '</span>';
                $mail_body.='<p style="color:#fff;margin-bottom:10px;"><b>' . $data['w_site'][$i] . ' price: </b>' . "$" . (number_format($data['k_site'][$i], 2)) . '</p>';
                if ($data['f_site'][$i] != "" && $data['s_site'][$i] != "") {
                    $mail_body.='<p style="color:#fff;margin-bottom:10px;"><b> ' . $data['f_site'][$i] . ' price: </b>' . "$" . (number_format($data['s_site'][$i], 2)) . '</p>';
                }
                $mail_body.='<p style="color:#fff;margin-bottom:10px;"><strong>Wholesale Hotels Group price:</strong> ' . "$" . (number_format($data['pricequote'][$i], 2)) . '</p>';
                $mail_body.='<p style="color:#fff;margin-bottom:10px;"><span style="float:left;width:100%;color:#fff;margin-bottom:20px;"><b>SAVINGS TOTAL: ' . "$" . (number_format($saving, 2)) . ' <sup>*</sup></b></p>';
            } else {
                $this->db->where('int_glcode', $id);
                $main_quote = $this->db->get('dz_quote')->result_array();
                $new_quote = array(
                    'var_fkuser' => $main_quote[0]['var_fkuser'],
                    'var_city' => $main_quote[0]['var_city'],
                    'var_checkin' => date("Y-m-d", strtotime($main_quote[0]['var_checkin'])),
                    'var_checkout' =>date("Y-m-d", strtotime($main_quote[0]['var_checkout'])),
                    'var_night' => $main_quote[0]['var_night'],
                    'var_room' => $main_quote[0]['var_room'],
                    'var_adult' => $main_quote[0]['var_adult'],
                    'var_child' => $main_quote[0]['var_child'],
                    'var_hotelname' => $main_quote[0]['var_hotelname'],
                    'var_rating' => $main_quote[0]['var_rating'],
                    'var_nameofreservation' => $main_quote[0]['var_nameofreservation'],
                    'var_points' => $main_quote[0]['var_points'],
                    'quote_status' => 'R',
                    'dt_updated_date' => date("Y-m-d H:i:s"),
                    'dt_created_date' => date("Y-m-d H:i:s"),
                );
                $this->db->insert('dz_quote', $new_quote);
                $last_quote = $this->db->insert_id();
                if ($last_quote != '') {
                    $data['s_site'][$i] = str_replace(',', "", $data['s_site'][$i]);
                    $data['pricequote'][$i] = str_replace(',', "", $data['pricequote'][$i]);
                    $data['k_site'][$i] = str_replace(',', "", $data['k_site'][$i]);
                    $data['s_site'][$i] = str_replace(',', "", $data['s_site'][$i]);
                    $data['s_src'][$i] = str_replace(',', "", $data['s_src'][$i]);

                    $random = $this->random_num();
                    if ($data['s_site'][$i] == '') {
                        $data['s_site'][$i] = 0;
                    }
                    $quote[] = array(
                        'var_fkuser' => $query[0]['int_glcode'],
                        'fk_replayuser_id' => $user_id,
                        'var_prize' => $data['pricequote'][$i],
                        'int_fkquote' => $last_quote,
                        'var_site1' => $data['w_site'][$i],
                        'int_site1_price' => $data['k_site'][$i],
                        'var_site2' => $data['f_site'][$i],
                        'int_site2_price' => $data['s_site'][$i],
                        'var_tripadvisor' => $data['tripadvisor'][$i],
                        'var_src' => $data['src'][$i],
                        'int_src_price' => $data['s_src'][$i],
                        'var_room_type' => $data['room_type'][$i],
                        'var_address' => $data['address'][$i],
                        'var_uniq_quoteid' => $random . $i,
                        'var_NOH' => $data['hotel'][$i],
                        'chr_status' => 'UP',
                        'var_star' => $data['rating'][$i],
                        'var_policy' => $data['policy'][$i],
                        'var_comment' => $data['comment'][$i],
                        'var_createddate' => date('Y-m-d H:i:s'),
                    );
                    $this->db->insert('dz_rplyquote', $quote[$i]);

                    if ($data['s_site'][$i] != "") {
                        if ($data['k_site'][$i] > $data['s_site'][$i]) {
                            $saving = $data['s_site'][$i] - $data['pricequote'][$i];
                        } else {
                            $saving = $data['k_site'][$i] - $data['pricequote'][$i];
                        }
                    } else {
                        $saving = $data['k_site'][$i] - $data['pricequote'][$i];
                    }
                    $mail_body.='<p style="color:#fff;margin-bottom:10px;"><h3 style="color:#fff;margin-bottom:10px;">Quote #' . $c++ . ':</h3></p>';
                    $mail_body.='<p style="color:#fff;margin-bottom:10px;"><span style="width:48%;float:left;"><b>Quote ID: </b>' . $random . $i . '</span><span style="width:48%;margin-left:10px;"><b>Name: </b>' . $query[0]['var_nameofreservation'] . ' </span></p>';
                    $mail_body.='<p style="color:#fff;margin-bottom:10px;"><span style="float:left;width:100%;"><b>Hotel: </b>' . $data['hotel'][$i] . '</span><div style="clear:both;"></div></p>';
                    $mail_body.='<p style="color:#fff;margin-bottom:10px;"><span style="float:left;width:33%;"><b>Star: </b>' . $data['rating'][$i] . '</span><span style="margin-left:10px;width:33%;float:left;"><b>Rooms: </b>' . $query[0]['var_room'] . '</span><span style="margin-left:10px;width:34%;"><b>TripAdvisor: </b>' . $data['tripadvisor'][$i] . '</span></p>';
//            $mail_body.='<p style="color:#fff;margin-bottom:10px;"><span style="width:100%;float:left;margin-bottom:10px;"><b>Location: </b>' . $query[0]['var_city'] . '</span></p>';
                    $mail_body.= '<p style="color:#fff;margin-bottom:10px;"><span style="width:100%;float:left;margin-bottom:10px;"><b>Comment: </b>' . $data['comment'][$i] . '</span></p></p>';
                    $mail_body.='<p style="color:#fff;margin-bottom:10px;"><span style="float:left;width:100%;color:#fff;margin-bottom:20px;"><b>Cancellation Policy: </b>' . $data['policy'][$i] . '</span>';
                    $mail_body.='<p style="color:#fff;margin-bottom:10px;"><b>' . $data['w_site'][$i] . ' price: </b>' . "$" . (number_format($data['k_site'][$i], 2)) . '</p>';
                    if ($data['f_site'][$i] != "" && $data['s_site'][$i] != "") {
                        $mail_body.='<p style="color:#fff;margin-bottom:10px;"><b> ' . $data['f_site'][$i] . ' price: </b>' . "$" . (number_format($data['s_site'][$i], 2)) . '</p>';
                    }
                    $mail_body.='<p style="color:#fff;margin-bottom:10px;"><strong>Wholesale Hotels Group price:</strong>' . "$" . (number_format($data['pricequote'][$i], 2)) . '</p>';
                    $mail_body.='<p style="color:#fff;margin-bottom:10px;"><span style="float:left;width:100%;color:#fff;margin-bottom:20px;"><b>SAVINGS TOTAL:' . "$" . (number_format($saving, 2)) . ' <sup>*</sup></b></p>';
                }
            }
        }
        // echo $mail_body;exit
        $mail_body .='<p style="color:#fff;">Sincerely,<br/><a href="www.WHotelsGroup.com" style="color:#fff;font-weight:600;">www.WHotelsGroup.com</a></p>';
        $mail_body .= '<p style="color:#fff;margin-bottom:10px;font-size:9px;"><sup>*</sup> Cancellation policies between <b>Wholesale Hotels Group</b> quotes and competitor sites may vary.</p>';

        $data['mail_body'] = $mail_body;
        $data['bookbutton'] = "Yes";
        $message = $this->load->view('admin/account/genral_emailtemplate', $data, TRUE);
        $this->email->message($message);
        $this->email->send();


        $data_array = array(
            'quote_status' => 'R',
            'dt_updated_date' => date('Y-m-d H:i:s'),
        );
        $this->db->where('int_glcode', $id);
        $this->db->update('dz_quote', $data_array);

        return TRUE;
    }

    function gethotel() {
        $q = $this->db->get('dz_hotel')->result_array();
        return $q;
    }

}

?>