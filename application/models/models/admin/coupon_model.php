<?php

class Coupon_model extends CI_Model {

    function coupon_generate($data) {
               // print_r($data);exit;
        date_default_timezone_set(AMERICA_LOS_ANGLES_TIME_ZONE);
        if($data['expdate'] == ""){
            $expire = date('Y-m-d', strtotime('+1 years'));
            }else{
                $expire =$data['expdate'];
            }
        if ($data['status'] == 'all') {
            $this->db->where('chr_user_type', 'U');
            $query = $this->db->get('dz_user')->result_array();
            if ($data['coupanoff1'] == '') {
                $coupanvalue = $data['coupanoff2'];
                for ($i = 0; $i < count($query); $i++) {
                    $couponcode = $this->couponcode();
                    $data_array = array(
                        'fk_user' => $query[$i]['int_glcode'],
                        'var_couponcode' => $couponcode . $i,
                        'var_couponvalue' => $coupanvalue,
                        'var_coupan_type' => '2',
                        'var_created_date' => date('Y-m-d h:i'),
                        'var_updated_date'=>date('Y-m-d h:i:s'),
                        'var_couponexpi'=>$expire,
                    );                 
                    $this->db->insert('dz_coupons', $data_array);
                }
            } else {
                $coupanvalue = $data['coupanoff1'];
              
                for ($i = 0; $i < count($query); $i++) {
                    $couponcode = $this->couponcode();
                    
                    $data_array = array(
                        'fk_user' => $query[$i]['int_glcode'],
                        'var_couponcode' => $couponcode . $i,
                        'var_couponvalue' => $coupanvalue,
                        'var_coupan_type' => '1',
                        'var_created_date' => date('Y-m-d h:i'),
                        'var_couponexpi'=>$expire,
                    );
                                      
                    $this->db->insert('dz_coupons', $data_array);
                }
            }
        } 
        else {
            $arraydata = explode(',',$data['status']);
            if(($key = array_search('on', $arraydata)) !== false) {
                unset($arraydata[$key]);
            }
          //  print_r($arraydata);
           // exit();
            $this->db->where_in('int_glcode',$arraydata);
            $query = $this->db->get('dz_user')->result_array();
          //  echo $this->db->last_query();
          //  exit();
            if($data['expdatesing'] == ""){
                $expire1 = date('Y-m-d', strtotime('+1 years'));
            }else{
                $expire1 =$data['expdatesing'];
            }
         //   print_r($query);
         //   exit();
            if ($data['coupanoff3'] == '') {
                $coupanvalue = $data['coupanoff4'];
                for ($i = 0; $i < count($query); $i++) {
                    $couponcode = $this->couponcode();
                    $data_array = array(
                        'fk_user' => $query[$i]['int_glcode'],
                        'var_couponcode' => $couponcode . $i,
                        'var_couponvalue' => $coupanvalue,
                        'var_coupan_type' => '2',
                        'var_created_date' => date('Y-m-d h:i'),
                        'var_couponexpi'=>$expire1,
                    );                   
                    $this->db->insert('dz_coupons', $data_array);
                }
            } else {
                for ($i = 0; $i < count($query); $i++) {
                    $coupanvalue = $data['coupanoff3'];
                    $couponcode = $this->couponcode();
                    $data_array = array(
                        'fk_user' => $query[$i]['int_glcode'],
                        'var_couponcode' => $couponcode . $i,
                        'var_couponvalue' => $coupanvalue,
                        'var_coupan_type' => '1',
                        'var_created_date' => date('Y-m-d h:i'),
                        'var_couponexpi'=> $expire1,
                    );                   
                    $this->db->insert('dz_coupons', $data_array);
                  
                }
            }
        }
        
        return TRUE;
    }

    function status_update($data) {
        date_default_timezone_set(AMERICA_LOS_ANGLES_TIME_ZONE);
        $query = $this->db->get_where('dz_coupons', array('int_glcode' => $data['id']))->result_array();

        if ($query[0]['var_status'] == 'A') {
            $data_array = array(
                'var_status' => 'AD',
            );
            $this->db->where('int_glcode', $data['id']);
            $this->db->update('dz_coupons', $data_array);
        } else {
            $data_array = array(
                'var_status' => 'A',
            );
            $this->db->where('int_glcode', $data['id']);
            $this->db->update('dz_coupons', $data_array);
        }
        return True;
    }

    function couponcode($numno = 2, $numAlpha = 4, $numno = 3) {
        $listAlpha = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $listno = '0123456789';
        return str_shuffle(
                substr(str_shuffle($listno), 0, $numno) .
                substr(str_shuffle($listAlpha), 0, $numAlpha) .
                substr(str_shuffle($listno), 0, $numno)
        );
    }

    function status_singup($data) {
        
        date_default_timezone_set(AMERICA_LOS_ANGLES_TIME_ZONE);
//        $this->db->select('chr_status');
//        $this->db->where('int_glcode', 1);
//        $query = $this->db->get('dz_singup_coupons')->result_array();
//        if ($query[0]['chr_status'] == '1') {
//            $data_array = array(
//                'chr_status' => '0',
//                'dt_update_date' => date("Y-m-d H:i:s"),
//            );
//        } else {
            $data_array = array(
                'chr_status' => $data['id'],
                'dt_update_date' => date("Y-m-d H:i:s"),
            );
//        }
        $this->db->where('int_glcode', 1);
        $this->db->update('dz_singup_coupons', $data_array);
        return TRUE;
    }
    function generatedata($data)
    {
        date_default_timezone_set(AMERICA_LOS_ANGLES_TIME_ZONE);
        $data_array = array(
          'var_couponvalue' => $data['couponper'],  
          'var_couponexpi' => $data('Y-m-d')
        );
        $this->db->insert('dz_coupons', $data_array);
    }

}

?>
