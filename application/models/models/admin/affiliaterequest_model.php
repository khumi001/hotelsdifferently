<?php

class Affiliaterequest_model extends CI_Model {

    function statusapprove($id) {
        $reqstatus = array(
            'chr_status' => 'A',
            'login_status' => 'Active'
        );
        $this->db->where('int_glcode', $id);
        $this->db->update('dz_user', $reqstatus);
        
        $this->db->where('int_glcode', $id);
        $this->db->where("chr_user_type","AF");
        $query = $this->db->get('dz_user')->result_array();
        
        $reqstatus1 = array(
            'var_promocode' => $query[0]['var_accountid'],
            'int_amount' => '20'
        );
      //  $this->db->where('int_glcode', $id);
        $this->db->insert('dz_promocode', $reqstatus1);
        
        //$url = base_url() . 'front/home/member_signup/' . $query[0]['var_accountid'];
        $url = base_url() .'affiliate/banner_link';
        $this->email->set_mailtype("html");            
        $this->email->from(NOREPLY, 'Admin');
        $this->email->to($query[0]['var_email']);
        $this->email->subject("You're IN!");
        $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Dear '.$query[0]['var_fname'].',</p>';
        $mail_body .= '<p style="color:#fff;margin-bottom:0px;">Allow Us to thank you for enrolling as one of our Affiliates. Here at <strong>HotelsDifferently<sup>sm</sup></strong>, we are always delighted to have people like you on board. </p> <p>For future reference please check out our Affiliate FAQ from within your dashboard. You can also access vital information and keep track of important information such as:</p>';
        $mail_body .= '<p style="color:#fff;margin-bottom:10px;">- Signups<br/>- Commission <br/>- Pending transactions<br/>- Payouts<br/>- Referral / Affiliate link<br/>- Account information<br/>...and much more!</p>';
        $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Every new visitor who becomes a Member (remember there is no charge for someone to become a Member) you refer gets $20 OFF their first booking.</p>';
        $mail_body .= '<p style="color:#fff;margin-bottom:10px;"><h3>REMEMBER</h3></p>';
        $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Any new Member you refer will have your referral number on their future bookings, therefore if you have someone signup once, every time that Member uses his account to make a booking, you will receive commission for as long as they pay for bookings. That way we not only reward you for referring new clients but we will always reward you as long as there is a booking from the referred account which can provide you a nice paycheck for the lifetime of that specific account. The more signups you refer, the more money you can make!!!</p>';
        $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Your banner can be accessed from within your dashboard and your referral link can be found here:</p>';
        $mail_body .= '<a style="color:#fff;margin-bottom:10px;" href='.$url.'>' .$url. '</a>';
        $mail_body .= '<p style="color:#fff;margin-bottom:10px;" >Use this link to start earning money right away!</p>';
        $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Thank you for your enrollment and we very much look forward to working with you!</br></p>';
        $mail_body .='<p style="color:#fff;">Sincerely,<br/><strongstyle="color:#fff;">HotelsDifferently.com</strong><br/><span style="font-size:9px;">Take the hotel challenge!</spam></p>';
//        $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Cancellation policies between <b>HotelsDifferently<sup>sm</sup></b> quotes and competitor sites may vary.</p>';
        $data['mail_body'] = $mail_body;
        $message = $this->load->view('admin/account/genral_emailtemplate',$data,TRUE);
        $this->email->message($message);
        $this->email->send(); 
                           
        return TRUE;
    }

    function statusreject($id) {
//       
        $this->db->where('int_glcode', $id);
        $this->db->where("chr_user_type","AF");
        $query = $this->db->get('dz_user')->result_array();
    //    print_r($query);
        $this->email->set_mailtype("html");            
        $this->email->from(NOREPLY, 'Admin');
        $this->email->to($query[0]['var_email']);
        $this->email->subject('Your Request is Reject');
        $mail_body.='<p style="color:#fff;margin-bottom:10px;">Dear '.$query[0]['var_fname'].',</p>';
        $mail_body.='<p style="color:#fff;margin-bottom:10px;">Thank you very much for your submission to enroll as our Affiliate at HotelsDifferently.com, We regret to inform you that after careful consideration we had to turn down your application. We appreciate the time you took to try to enroll and we hope to be able to work with you sometime in the near future on another occasion</p>';
        $mail_body.='Thanks again and we wish you the best!</br>';
        $mail_body.='Sincerely,</br><strong>HotelsDifferently<sup>sm</sup></strong>customer service';
//         print_r($mail_body);exit;
        $this->email->message($mail_body);
        $this->email->send(); 
     //   exit();
        
        $this->db->where('int_glcode', $id);
        $this->db->delete('dz_user');
        
        return TRUE;
    }

    function affiliatereq_update($data) {
        $data_array = array(
            'var_fname' => $data['firstname'],
            'var_lname' => $data['lastname'],
            'var_email' => $data['email'],
            'var_company' => $data['companyname'],
            'var_title' => $data['title'],
            'var_country' => $data['country'],
            'var_state' => $data['state'],
            'var_zip' => $data['zip'],
            'var_address1' => $data['address'],
            'var_address2' => $data['address2'],
            'var_phone' => $data['phone'],
            'var_newsletter' => $data['newsletter'],
            'var_websiteurl' => $data['wesiteurl'],
            'var_sitedescription' => $data['sitedesc'],
            'var_visitors' => $data['visitors'],
            'var_payment' => $data['paymentmethod']
        );
//        print_r($data_array);
//        exit();
        $this->db->where('int_glcode', $data['req_id']);
        $this->db->update('dz_user', $data_array);
        
        $id = $data['req_id'];
        $this->db->where('fk_user',$id);
        
        $row = $this->db->get('dz_paymount_prefrence')->num_rows();
        if($row == '0')
        { 
            $data_array = array(
                'fk_user' => $id,
                'var_fullname' => $data['ppfullname'],
                'var_phonenumber' => $data['ppphone'],
                'var_city' => $data['ppcity'],
                'var_state' => $data['ppstate'],
                'var_country' => $data['ppcountry'],
                'var_paypalemail' => $data['paypalemail'],
                'var_paymentmethod' => $data['paymentmethod'],
                'var_money' => $data['ppamount'],          
                'dt_updateddate' => date('Y-m-d h:i:s'),
            );        
            $this->db->insert('dz_paymount_prefrence',$data_array);    
            
            $data_array1 = array(
                'var_payment' => $data['method_payment'],
            );        
            $this->db->where('int_glcode',$id);
            $this->db->update('dz_user',$data_array1);   
                
        }
        else
        {  
            $data_array = array(
                'fk_user' => $id,
                'var_fullname' => $data['ppfullname'],
                'var_phonenumber' => $data['ppphone'],
                'var_city' => $data['ppcity'],
                'var_state' => $data['ppstate'],
                'var_country' => $data['ppcountry'],
                'var_paypalemail' => $data['paypalemail'],
                'var_paymentmethod' => $data['paymentmethod'],
                'var_money' => $data['ppamount'],          
                'dt_updateddate' => date('Y-m-d h:i:s'),
            );       
            $this->db->where('fk_user',$id);
            $this->db->update('dz_paymount_prefrence',$data_array);   
            
             $data_array1 = array(
                'var_payment' => $data['method_payment'],
            );        
            $this->db->where('int_glcode',$id);
            $this->db->update('dz_user',$data_array1);   
          
        }   
        return TRUE;
    }
    
    function affiliatereq_add($data){
        $data_array = array(
            'var_fname' => $data['firstname'],
            'var_lname' => $data['lastname'],
            'var_email' => $data['email'],
            'var_company' => $data['companyname'],
            'var_title' => $data['title'],
            'chr_status' =>'A',
            'var_country' => $data['country'],
            'var_state' => $data['state'],
            'var_zip' => $data['zip'],
            'var_address' => $data['address'],
            'var_phone' => $data['phone'],
            'var_newsletter' => $data['newsletter'],
            'var_websiteurl' => $data['wesiteurl'],
            'var_sitedescription' => $data['sitedesc'],
            'var_visitors' => $data['visitors'],
            'var_payment' => $data['paymentmethod']
        );
        $this->db->insert('dz_user', $data_array);
        return TRUE;
    }

}

?>
