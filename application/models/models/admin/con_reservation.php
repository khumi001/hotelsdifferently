<?php

class Con_reservation extends CI_Model {

    function get_conres() {
        $this->db->select('dz_commision.var_paid_amount,
            dz_user.int_glcode,
            dz_user.var_email,
            dz_user.var_username,
            dz_user.var_accountid,
            dz_rplyquote.var_transactionid,
            dz_rplyquote.int_fkquote,
            dz_rplyquote.chr_status,
            dz_rplyquote.var_createddate,
            dz_rplyquote.int_fkquote,
            dz_commision.fk_quote');
        $this->db->from('dz_rplyquote');
        $this->db->where('dz_rplyquote.chr_status', 'P');
        $this->db->join('dz_quote', 'dz_rplyquote.int_fkquote = dz_quote.int_glcode');
        $this->db->join('dz_user', 'dz_quote.var_fkuser = dz_user.int_glcode');
        $this->db->join('dz_commision', 'dz_rplyquote.int_fkquote=dz_commision.fk_quote');
        $query = $this->db->get()->result_array();

        return $query;
    }
    
    function confirm_getdata($id){
//         $this->db->select('var_checkin,var_checkout,var_city,var_rating,var_comments,var_room,var_adult,var_child,var_hotelname');
//        $this->db->from('dz_quote');
//        $this->db->where('int_glcode', $id);
//        $query1 = $this->db->get()->result_array();
//
//        $query = $this->db->select('dz_rplyquote.var_uniq_quoteid,dz_rplyquote.var_policy,dz_rplyquote.var_star')
//                           ->from('dz_rplyquote')
//                           ->join('dz_rplyquote', 'dz_user.int_glcode=dz_rplyquote.var_fkuser')
//                           ->where('dz_rplyquote.var_fkuser', $query1[0]['int_fkquote'])->get()->result_array();
//       
//        return $query;
        
         $this->db->select('dz_quote.var_checkin,dz_quote.var_checkout,dz_quote.var_city,dz_quote.var_comments,
             dz_rplyquote.fk_coupon,dz_rplyquote.var_prize,
             dz_quote.var_room,dz_quote.var_adult,dz_quote.var_child,dz_quote.var_hotelname,
             dz_rplyquote.var_uniq_quoteid, dz_rplyquote.var_tripadvisor,dz_rplyquote.var_policy,dz_rplyquote.var_star');
        $this->db->from('dz_rplyquote');
        $this->db->where('dz_rplyquote.var_fkuser', $id);
        $this->db->join('dz_quote', 'dz_rplyquote.'.$id.' = dz_quote.int_glcode');

        $query = $this->db->get()->result_array();

        return $query;
    }
    
    function add_pending($data){ 
        $id = $data['editid'];
                $array = array(
                    'var_admin_soures' => $data['source'],
                    'var_admin_amount' => $data['amount'],
                    'var_soures_confirmation' => $data['source_confirmation']
                );
            $this->db->where('int_glcode', $id);
            $this->db->update('dz_rplyquote', $array);
        return TRUE;
    }
    
    function createcharjbackrevart($replayid)
    {
         $this->db->where('fk_replayquote', $replayid);
         $commsion = $this->db->get('dz_commision')->result_array();
        
         // find user
        $this->db->select('dz_user.*');
        $this->db->from('dz_rplyquote');
        $this->db->where('dz_rplyquote.int_glcode', $replayid);
        $this->db->join('dz_quote', 'dz_rplyquote.int_fkquote = dz_quote.int_glcode');
        $this->db->join('dz_user', 'dz_user.int_glcode = dz_quote.var_fkuser');
        $userdata = $this->db->get()->result_array();
        
         $this->db->where('fk_replayquote', $replayid);
         $commsion = $this->db->get('dz_commision')->result_array();
         if(!empty($commsion))
         {
             $data_array2 = array(
                    'fk_user' => $commsion[0]['fk_user'],
                    'fk_affilate_chargeback' => $commsion[0]['fk_user'],
                    'var_amount' => $commsion[0]['var_commision_amount'],
                    'var_comment' => 'Cancelled reservation!',
                    'chr_status' => 'CB',
                );
                $result = $this->db->delete('dz_chargeback', $data_array2);
                
            $get_perent1 = $this->db->select('fk_parent_account,var_email,var_point')->get_where('dz_user', array('int_glcode' => $this->userid))->row();
          //  print_r($get_perent1);
        //    exit();
            $uamount = $userdata[0]['var_point'] + $commsion[0]['var_paid_amount'];
            $pointupdate = array(
                'var_point' => $uamount,
            );
            $this->db->where('int_glcode', $userdata[0]['int_glcode']);
            $this->db->update('dz_user', $pointupdate);
         }
         return TRUE;
    }

}

?>