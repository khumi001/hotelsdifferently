<?php

class Signup_model extends CI_Model {

    function signup_mod($data) 
	{ 
        $userid = $data['parentid'];
        $code = $this->couponcode();
        date_default_timezone_set(AMERICA_LOS_ANGLES_TIME_ZONE);
        $data_array = array(
            'var_username' => $data['username'],
            'var_fname' => $data['firstname'],
            'var_lname' => $data['lastname'],
            'var_email' => $data['email'],
            'var_password' => base64_encode($data['password']),
            //'var_phone' => $data['phone1'] . '-' . $data['phone2'] . '-' . $data['phone3'],
            'var_accountid' => $code,
            'fk_parent_account' => $userid,
            'var_signupip' => $_SERVER['REMOTE_ADDR'],
            //'var_country' => $data['country'],
            'chr_user_type' => 'U',
            'chr_status' => 'P',
            'dt_created_date' => date('Y-m-d H:i:s'),
            'login_status' => 'Inactive',
            //'var_abuot_us' => $data['about_us']
        );

        $this->db->insert('dz_user', $data_array);
        $last_id = $this->db->insert_id();
         $reqstatus1 = array(
            'var_promocode' => $code,
            'int_amount' => '20'
        );
      //  $this->db->where('int_glcode', $id);
        $this->db->insert('dz_promocode', $reqstatus1);
        
        $value = $this->db->get_where('dz_promocode',array('var_promocode'=>$data['promocode']))->result_array();
		if(!empty($value))
		{
			$coupanvalue = $value[0]['int_amount']; 
			$update_prome = array(
				'var_promo_code' => $data['promocode'],
				 'var_abuot_us' => $data['promocode'],
			);
		}
		else
		{
			$update_prome = array(
				'var_abuot_us' => $data['about_us'],
			);
		}
		$this->db->where('int_glcode',$last_id);
		$this->db->update('dz_user',$update_prome);
        
            // they remove this funality same ip
        $this->db->where('var_signupip', $_SERVER['REMOTE_ADDR']);
        $norow = $this->db->get('dz_user')->num_rows();
        
//        if ($norow == 1) 
            
            if($data['promocode'] != "")
            {
                
            $query = $this->db->select('chr_status')->get('dz_singup_coupons')->result_array();
            $couponcode = $this->couponcode();
            if ($query[0]['chr_status'] == '1') {
                $futureDate = date('Y-m-d', strtotime('+1 year'));
                if ($userid == 0) {
                    $disscount = '25';
                } else {
                    $disscount = '20';
                }
                date_default_timezone_set(AMERICA_LOS_ANGLES_TIME_ZONE);
                $data_array1= array(
                    'fk_user' => $last_id,
                    'var_couponcode' => $couponcode,
                    'var_couponvalue' => $coupanvalue,
                    'var_couponexpi' => $futureDate,
                    'var_created_date' => date('Y-m-d h:i'),
                    'var_updated_date' => date('Y-m-d h:i:s'),
                    'var_coupan_type' => '2'
                );
                
                $this->db->insert('dz_coupons', $data_array1);
              //  return true;
            }
        }
        
        $url = base_url() . 'front/home/activation/' . $code;

        $this->load->library('email');        
        $this->email->set_mailtype("html");
        $this->email->from(NOREPLY, 'Member');
        $this->email->to($data['email']);
        $this->email->subject('Welcome to HotelsDifferently!');
        $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Thank you for using <strong style="color:#fff;"><a href="www.HotelsDifferently.com" style="color:#fff;">www.HotelsDifferently.com</a></strong>. We hope that you will find our website very useful and that it will help you achieve your dream vacation for less money. We currently offer deals for you at over <b>100,000 HOTELS</b> in <b>181 COUNTRIES!</b></p>';
        $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Please activate your account by clicking <a style="color: white" href=' . $url . '>HERE</a>.</p>';
        $mail_body .= '<p style="color: #fff;margin-bottom:10px;">Alternatively, you can also copy and paste the following into your browser: <a style="color: white" href=' . $url . '>' . $url . '</a></p>';
        $mail_body .= '<p style="color:#fff;margin-bottom:10px;">If you feel that you received this message by mistake, do nothing and your email address will not be added to our database!</p>';
        $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Thank you for choosing us and please always give us a try before booking your next trip!</p>';
        $mail_body .='<p style="color:#fff;">Sincerely,<br/><strong style="color:#fff;"><a href="www.HotelsDifferently.com" style="color:#fff;">www.HotelsDifferently.com</a></strong></p>';
        $data['mail_body'] = $mail_body;
        $message = $this->load->view('admin/account/genral_emailtemplate', $data, TRUE);
        
        $this->email->message($message);
        $sendit = $this->email->send();
       // sleep(5);
        if($sendit)
        {
            return true;
        }
        else
		{
            return FALSE;
        }

        
    }

    function newsletter($data) {

        $code = $this->input->post('email');
        date_default_timezone_set(AMERICA_LOS_ANGLES_TIME_ZONE);
        $data_array = array(
            'var_email' => $this->input->post('email'),
            'var_status' => 'Inactive',
            'created_date' => date('Y-m-d H:i:s'),
            'update_date' => date('Y-m-d H:i:s')
        );
        $this->db->insert('dz_newsletter', $data_array);

        $last_id = $this->db->insert_id();
        $url = base_url() . 'front/home/news_activation/' . $last_id;

        $this->email->set_mailtype("html");
        $this->email->from('admin@gmail.com', 'Member');
        $this->email->to($data['email']);
        $this->email->subject('Your Signup successful genrate link');
        $mail_body = '<p style="color:#fff;">Hello</p>';
        $mail_body .= '<p style="color:#fff;">Thank you for using <strong style="color:#fff;"><a href="www.HotelsDifferently.com" style="color:#fff;">www.HotelsDifferently.com</a></strong>. We hope that you will find our website very useful and that it will help you achieve your dream vacation for less money. We currently offer deals for you at over 10,000 HOTELS in 100 CITIES WORLDWIDE!</p>';
        $mail_body .= '<p style="color:#fff;">Please activate your account by clicking <a style="color: white" href=' . $url . '>HERE</a>.</p>';
        $mail_body .= '<p style="color: white">Alternatively, you can also copy and paste the following into your browser:' . $url . '</a></p>';
        $mail_body .= '<p style="color:#fff;">If you feel that you received this message by mistake, do nothing and your email address will not be added to our database!</p>';
        $mail_body .= '<p style="color:#fff;">Thank you for choosing us and please always give us a try before booking your next trip!</p>';
        $mail_body .='<p style="color:#fff;">Sincerely,</p>';
        $mail_body .='<p style="color:#fff;"><strong style="color:#fff;"><a href="www.HotelsDifferently.com" style="color:#fff;">www.HotelsDifferently.com</a></strong></p>';
      //  $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Cancellation policies between <b>HotelsDifferently<sup>sm</sup></b> quotes and competitor sites may vary.</p>';
        $data['mail_body'] = $mail_body;
        $message = $this->load->view('admin/account/genral_emailtemplate', $data, TRUE);
        $this->email->message($message);
        $this->email->send();

        return true;
    }

    function couponcode($numno = 4, $numno = 4) {
        $listno = '0123456789';
        return str_shuffle(
                substr(str_shuffle($listno), 0, $numno) .
                substr(str_shuffle($listno), 0, $numno)
        );
    }

}

?>
