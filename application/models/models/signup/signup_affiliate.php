<?php

class Signup_affiliate extends CI_Model {

    function signup_mod($data) {
        $code = $this->couponcode();
        if ($data['state'] == "") {
            $data['state'] = $data['state2'];
        }
        // print_r($data);
        //   exit();
        $data_array = array(
            'var_fname' => $data['firstname'],
            'var_lname' => $data['lastname'],
            'var_email' => $data['email'],
            'var_password' => base64_encode($data['password']),
            'var_company' => $data['company'],
            'var_title' => $data['title'],
            'var_signupip' => $_SERVER['REMOTE_ADDR'],
//            'var_country' => $data['country'],
//            'var_address' => $data['address'] . $data['address2'],
//            'var_city' => $data['city'],
            'var_country' => $data['country_full'],
//            'var_address' => $data['address'] . $data['address2'],
            'var_address1' => $data['address1'],
            'var_address2' => $data['address2'],
            'var_city' => $data['city'],
            'var_zip' => $data['zipcode'],
            'var_state' => $data['state'],
            'var_phone' => $data['phone1'] . '-' . $data['phone2'] . '-' . $data['phone3'],
//            'var_dob' => date('Y-m-d', strtotime($data['dob'])),
            'var_accountid' => $code,
            'var_newsletter' => $data['newsletter'],
            'var_websiteurl' => $data['website_url'],
            'var_sitedescription' => $data['site_desc'],
            'var_visitors' => $data['visitors'],
            'var_payment' => $data['payment_method'],
            'chr_user_type' => 'AF',
            'dt_created_date' => date('Y-m-d h:i'),
            'login_status' => 'inactive',
            'chr_status' => 'P',
        );
        $this->db->insert('dz_user', $data_array);
        $last_id = $this->db->insert_id();
        
        
        $data_array = array(
            'fk_user' => $last_id,
            'var_paymentmethod' => $data['payment_method'],
            'var_money' => '99.99',          
            'dt_updateddate' => date('Y-m-d H:i:s'),
        );        
        $this->db->insert('dz_paymount_prefrence',$data_array);  
            
        $confirm_account = base64_encode($data['email']);
        // echo $confirm_account;
//        $config = Array(
//            'protocol' => 'smtp',
//            'smtp_host' => 'hotelsdifferentlycom.ipage.com',
//            'smtp_port' => 587,
//            'smtp_user' => NOREPLY,
//            'smtp_pass' => 'Jelszo456',
//            'mailtype' => 'html',
//            'charset' => 'iso-8859-1'
//        );
//        $this->load->library('email');
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        
        $url = base_url() . 'front/confirm_account/user/' . $confirm_account;
        $this->load->library('email');
        $this->email->set_mailtype("html");
        $this->email->from(NOREPLY, 'Affiliate');
        $this->email->to($data['email']);
        $this->email->subject('Welcome to HotelsDifferently!');
        $mail_body = '<p style="color:#fff;margin-bottom:10px;">Dear ' . $data['firstname'] . ',</p>';
        $mail_body .='<p style="padding-top:10px;color:#fff;">Thank you for using <strong style="color:#fff;"><a href="www.HotelsDifferently.com" style="color:#fff;">www.HotelsDifferently.com</a></strong>. We THANK YOU for signing up and showing interest in becoming one of our valued Affiliates. We currently offer deals for you to offer 65,000 HOTELS in 176 COUNTRIES and always try to find ways to beat the biggest competitors on the market thus making it attractive for end users to book with us and making it attractive for our affiliates to earn money based on those bookings..</p>';
        $mail_body .='<p style="color:#fff;margin-bottom:10px;">If you are interested in how we work with our Affiliates and how we make it one of the MOST LUCRATIVE one to maintain a long-lasting relationship with people like you, please log-in to your account after successful activation and read the material for Affiliates within your dashboard.</p>';
        $mail_body .='<p style="color:#fff;margin-bottom:10px;">Please confirm your email address by clicking <a style="color: white" href="' . $url . '">HERE</a>.</p>';
        $mail_body .='<p style="color:#fff;margin-bottom:10px;">Alternatively, you can also copy and paste the following into your browser’s URL: <a style="color:#fff;" href="'.$url.'">' . $url . '</a></p>';
        $mail_body .='<p style="color:#fff;margin-bottom:10px;">If you feel that you received this message by mistake, do nothing and your email address will not be added to our database!</p>';
        $mail_body .='<p style="padding-bottom:10px;color:#fff;">Sincerely,<br/><strong style="color:#fff;"><a href="www.HotelsDifferently.com" style="color:#fff;">www.HotelsDifferently.com</a></strong> </p>';
      //  $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Cancellation policies between <b>HotelsDifferently<sup>sm</sup></b> quotes and competitor sites may vary.</p>';
        $data['mail_body'] = $mail_body;
        $message = $this->load->view('admin/account/genral_emailtemplate', $data, TRUE);
//        echo $message;exit; 
        $this->email->message($message);
        $this->email->send();
        return $last_id;
    }

    function couponcode($numno = 3, $numno = 3) {
        $listno = '0123456789';
        return str_shuffle(
                substr(str_shuffle($listno), 0, $numno) .
                substr(str_shuffle($listno), 0, $numno)
        );
    }

}

?>