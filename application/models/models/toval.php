<?php

class Toval extends CI_Model {
	
    // id to val
    function idtoval($id, $val, $name, $table) {
        if (isset($val)) {
            $row = $this->idtorow($id, $val, $table, $name);
            if (isset($row->$name)) {
                return $row->$name;
            }
        }
    }

    function idtorow($id, $val, $table, $name = '*') {

        $this->db->select($name);
        $this->db->from($table);
        $this->db->where($id, $val);
        $query = $this->db->get();
        //echo  $this->db->last_query();
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row;
        }
    }
//function idtorow1($val, $table, $name = '*') {
//
//        $this->db->select($name);
//        $this->db->from($table);
//        $this->db->where($val);
//        $query = $this->db->get();
//        //echo  $this->db->last_query();
//        if ($query->num_rows() > 0) {
//            $row1 = $query->row();
//            return $row1;
//        }
//    }
    function tdrow($val, $table, $name) {

        $this->db->select('*');
        $this->db->from($table);
        $this->db->where('status', $val);
        $d = date('Y-m-d');
        $this->db->where('create_date', $d);
        $query = $this->db->get();
        $tdrow = $query->num_rows();
        return $tdrow;
    }

    function trow($val, $table, $name) {

        $this->db->select('*');
        $this->db->from($table);
        $this->db->where('status', $val);
        $query = $this->db->get();
        $trow = $query->num_rows();
        return $trow;
    }

    function idtorowarr($id, $val, $table, $name = '*') {

        $this->db->select($name);
        $this->db->from($table);
        $this->db->where($id, $val);
        $query = $this->db->get();
        //echo  $this->db->last_query();
        if ($query->num_rows() > 0) {
            $row = $query->row_array();
            return $row;
        }
    }

    function idtorowarray($id, $val, $table, $name = '*', $type = "data") {

        $this->db->select($name);
        $this->db->from($table);

        $this->db->where($id, $val);
        $query = $this->db->get();
        //echo  $this->db->last_query();
        if ($type == "count") {
            return $query->num_rows();
        }
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
//                echo $row['title'];
//                echo $row['name'];
//                echo $row['body'];
                $result[] = $row;
            }
        }
        return $result;
    }

    function fetchArrayMC($condiion, $table, $name = '*', $type = "data") {

        $this->db->select($name);
        $this->db->from($table);
        if (!empty($condiion)) {
            foreach ($condiion as $key => $value) {
                $this->db->where($key, $value);
            }
        }

        $query = $this->db->get();
        //echo  $this->db->last_query();
        if ($type == "count") {
            return $query->num_rows();
        }
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
//                echo $row['title'];
//                echo $row['name'];
//                echo $row['body'];
                $result[] = $row;
            }
        }
        return $result;
    }

    function check_cookie_ajax($js, $js_plugin, $css, $css_plugin) {
      //  print_r($js);
        
        $js_ajax = get_cookie('js');
        $js_ajax = json_decode($js_ajax);
        $count = count($js);
        for ($i = 0; $i < $count; $i++) {
            if ($js_ajax == '') {
                $js_ajax = array();
            }
            if (in_array($js[$i], $js_ajax)) {
                unset($js[$i]);
            } else {
                if ($js[$i] != '') {
                    array_push($js_ajax, $js[$i]);
                }
            }
        }
      //  print_r($js_ajax);
       // exit();
        //for js plugin            
        $count = count($js_plugin);
        for ($i = 0; $i < $count; $i++) {
            if ($js_ajax == '') {
                $js_ajax = array();
            }
            if (in_array($js_plugin[$i], $js_ajax)) {
                unset($js_plugin[$i]);
            } else {
                if ($js_plugin[$i] != '') {
                    array_push($js_ajax, $js_plugin[$i]);
                }
            }
        }

        $count = count($css);
        for ($i = 0; $i < $count; $i++) {
            if ($js_ajax == '') {
                $js_ajax = array();
            }
            if (in_array($css[$i], $js_ajax)) {
                unset($css[$i]);
            } else {
                if ($css[$i] != '') {
                    array_push($js_ajax, $css[$i]);
                }
            }
        }
        $count = count($css_plugin);
        for ($i = 0; $i < $count; $i++) {
            if ($js_ajax == '') {
                $js_ajax = array();
            }
            if (in_array($css_plugin[$i], $js_ajax)) {
                unset($css_plugin[$i]);
            } else {
                if ($css_plugin[$i] != '') {
                    array_push($js_ajax, $css_plugin[$i]);
                }
            }
        }
        $cookie_js = array(
            'name' => 'js',
            'value' => json_encode($js_ajax),
            'expire' => time() + 86400,
            'path' => '/',
        );
        $this->input->set_cookie($cookie_js);
     //   print_r(array('js' => $js, 'js_plugin' => $js_plugin, 'css' => $css, 'css_plugin' => $css_plugin));
     //   exit();
        return array('js' => $js, 'js_plugin' => $js_plugin, 'css' => $css, 'css_plugin' => $css_plugin);
    }

    function check_cookie($js, $js_plugin, $css, $css_plugin) {
        delete_cookie('js');
        $js_ajax = array();
        foreach ($js as $row) {
            array_push($js_ajax, $row);
        }
        foreach ($js_plugin as $row) {
            array_push($js_ajax, $row);
        }
        foreach ($css as $row) {
            array_push($js_ajax, $row);
        }
        foreach ($css_plugin as $row) {
            array_push($js_ajax, $row);
        }
        $cookie_js = array(
            'name' => 'js',
            'value' => json_encode($js_ajax),
            'expire' => time() + 86400,
            'path' => '/',
        );
        $this->input->set_cookie($cookie_js);
        return array('js' => $js, 'js_plugin' => $js_plugin, 'css' => $css, 'css_plugin' => $css_plugin);
    }

    function upload_it($row_array_key, $file_name) {


        //load the helper
        $this->load->helper('form');

        //Configure
        //set the path where the files uploaded will be copied. NOTE if using linux, set the folder to permission 777
        $config['upload_path'] = 'upload/img/slider/';

        // set the filter image types
        $config['allowed_types'] = 'gif|jpg|png|pdf';
        $config['file_name'] = $file_name;
        //load the upload library
        $this->load->library('upload', $config);

        $this->upload->initialize($config);

        $this->upload->set_allowed_types('*');

        $data['upload_data'] = '';

        //if not successful, set the error message
        if (!$this->upload->do_upload($row_array_key)) {
            $data = array('msg' => $this->upload->display_errors());
            return FALSE;
        } else { //else, set the success message
            $data = array('msg' => "Upload success!");
            return TRUE;

            //$data['upload_data'] = $this->upload->data();
        }
    }

    function upload_it_review($row_array_key, $file_name) {


        //load the helper
        $this->load->helper('form');


        //Configure
        //set the path where the files uploaded will be copied. NOTE if using linux, set the folder to permission 777
        $config['upload_path'] = 'upload/img/review/';

        // set the filter image types
        $config['allowed_types'] = 'gif|jpg|png|pdf';
        $config['file_name'] = $file_name;
        //load the upload library
        $this->load->library('upload', $config);

        $this->upload->initialize($config);

        $this->upload->set_allowed_types('*');

        $data['upload_data'] = '';



        //if not successful, set the error message
        if (!$this->upload->do_upload($row_array_key)) {
            $data = array('msg' => $this->upload->display_errors());
            return FALSE;
        } else { //else, set the success message
            $data = array('msg' => "Upload success!");
            // $this->image_lib->clear();
            return TRUE;

            //$data['upload_data'] = $this->upload->data();
        }
    }
    
    function upload_it_rac($row_array_key, $file_name) {


        //load the helper
        $this->load->helper('form');


        //Configure
        //set the path where the files uploaded will be copied. NOTE if using linux, set the folder to permission 777
        $config['upload_path'] = 'upload/img/rac/';

        // set the filter image types
        $config['allowed_types'] = 'gif|jpg|png|pdf';
        $config['file_name'] = $file_name;
        //load the upload library
        $this->load->library('upload', $config);

        $this->upload->initialize($config);

        $this->upload->set_allowed_types('*');

        $data['upload_data'] = '';



        //if not successful, set the error message
        if (!$this->upload->do_upload($row_array_key)) {
            $data = array('msg' => $this->upload->display_errors());
            return FALSE;
        } else { //else, set the success message
            $data = array('msg' => "Upload success!");
            // $this->image_lib->clear();
            return TRUE;

            //$data['upload_data'] = $this->upload->data();
        }
    }
    
    function upload_it_logo($row_array_key, $file_name) {


        //load the helper
        $this->load->helper('form');


        //Configure
        //set the path where the files uploaded will be copied. NOTE if using linux, set the folder to permission 777
        $config['upload_path'] = 'upload/img/restaurant_logo/';

        // set the filter image types
        $config['allowed_types'] = 'gif|jpg|png|pdf';
        $config['file_name'] = $file_name;
        //load the upload library
        $this->load->library('upload', $config);

        $this->upload->initialize($config);

        $this->upload->set_allowed_types('*');

        $data['upload_data'] = '';



        //if not successful, set the error message
        if (!$this->upload->do_upload($row_array_key)) {
            $data = array('msg' => $this->upload->display_errors());
            return FALSE;
        } else { //else, set the success message
            $data = array('msg' => "Upload success!");
            // $this->image_lib->clear();
            return TRUE;

            //$data['upload_data'] = $this->upload->data();
        }
    }   
    function upload_it_img_gallery($row_array_key, $file_name) {


        //load the helper
        $this->load->helper('form');


        //Configure
        //set the path where the files uploaded will be copied. NOTE if using linux, set the folder to permission 777
        $config['upload_path'] = 'upload/img/restaurant_img_gallery/';

        // set the filter image types
        $config['allowed_types'] = 'gif|jpg|png';
        $config['file_name'] = $file_name;
        //load the upload library
        $this->load->library('upload', $config);

        $this->upload->initialize($config);

        $this->upload->set_allowed_types('*');

        $data['upload_data'] = '';



        //if not successful, set the error message
        if (!$this->upload->do_upload($row_array_key)) {
            $data = array('msg' => $this->upload->display_errors());
            return FALSE;
        } else { //else, set the success message
            $data = array('msg' => "Upload success!");
            // $this->image_lib->clear();
            return TRUE;

            //$data['upload_data'] = $this->upload->data();
        }
    } 
    function upload_it_pdf($row_array_key, $file_name) {


        //load the helper
        $this->load->helper('form');


        //Configure
        //set the path where the files uploaded will be copied. NOTE if using linux, set the folder to permission 777
        $config['upload_path'] = 'upload/img/restaurant_menu_pdf/';

        // set the filter image types
        $config['allowed_types'] = 'pdf';
        $config['file_name'] = $file_name;
        //load the upload library
        $this->load->library('upload', $config);

        $this->upload->initialize($config);

        $this->upload->set_allowed_types('*');

        $data['upload_data'] = '';



        //if not successful, set the error message
        if (!$this->upload->do_upload($row_array_key)) {
            $data = array('msg' => $this->upload->display_errors());
            return FALSE;
        } else { //else, set the success message
            $data = array('msg' => "Upload success!");
            // $this->image_lib->clear();
            return TRUE;

            //$data['upload_data'] = $this->upload->data();
        }
    }
    function upload_it_feature($row_array_key, $file_name) {


        //load the helper
        $this->load->helper('form');


        //Configure
        //set the path where the files uploaded will be copied. NOTE if using linux, set the folder to permission 777
        $config['upload_path'] = 'upload/img/feature/';

        // set the filter image types
        $config['allowed_types'] = 'gif|jpg|png|pdf';
        $config['file_name'] = $file_name;
        //load the upload library
        $this->load->library('upload', $config);

        $this->upload->initialize($config);

        $this->upload->set_allowed_types('*');

        $data['upload_data'] = '';



        //if not successful, set the error message
        if (!$this->upload->do_upload($row_array_key)) {
            $data = array('msg' => $this->upload->display_errors());
            return FALSE;
        } else { //else, set the success message
            $data = array('msg' => "Upload success!");
            // $this->image_lib->clear();
            return TRUE;

            //$data['upload_data'] = $this->upload->data();
        }
    }  
    function upload_it_event($row_array_key, $file_name) {

        //load the helper
        $this->load->helper('form');

        //Configure
        //set the path where the files uploaded will be copied. NOTE if using linux, set the folder to permission 777
        $config['upload_path'] = 'upload/img/event/';

        // set the filter image types
        $config['allowed_types'] = 'gif|jpg|png|pdf';
        $config['file_name'] = $file_name;
        //load the upload library
        $this->load->library('upload', $config);

        $this->upload->initialize($config);

        $this->upload->set_allowed_types('*');

        $data['upload_data'] = '';



        //if not successful, set the error message
        if (!$this->upload->do_upload($row_array_key)) {
            $data = array('msg' => $this->upload->display_errors());
            return FALSE;
        } else { //else, set the success message
            $data = array('msg' => "Upload success!");
            // $this->image_lib->clear();
            return TRUE;

            //$data['upload_data'] = $this->upload->data();
        }
    }
    
     function dropdown($table, $field_name) {

        $query =  $this->db->select($field_name)
                ->from($table)
                ->get()->result_array();

        return $query;
    }
    
    function ipban($banbyadmin = '')
    {
        if($banbyadmin != '')
        {
             $query =  $this->db->select('*')
                    ->from('dz_ban_ip_address')
                    ->where('var_banby','Admin')
                     ->where('fk_user',$banbyadmin)
                    ->where('chr_status','B')
                    ->get()->result_array();
            
        }
        else
        {
            $query =  $this->db->select('*')
                    ->from('dz_ban_ip_address')
                    ->where('var_ipban',$_SERVER['REMOTE_ADDR'])
                    ->where('chr_status','B')
                    ->get()->result_array();
        }
        return $query;
    }

}

?>