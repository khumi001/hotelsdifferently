<?php
    class Cruise_port_model extends CI_Model
    {
        function __construct()
        {
            parent::__construct();
        }
        
        function SynchronizeCruisePortWithApi()
        {
            $processCruiseAPIObj = new ProcessCruiseAPI();
            $fetchedPortList = $processCruiseAPIObj->ProcessPortList();
            
            $query = $this->db->from('dz_tourico_cruise_port')->get();
            $dbPortList = $query->result_array();
            foreach( $fetchedPortList as $aFetchedPort )
            {
                $found = false;
                foreach($dbPortList as $aDbPort)
                {
                    if($aDbPort['tourico_port_id']==$aFetchedPort['port_id'])
                    {
                        $found = true;
                        break;
                    }
                }
                
                if ($found)
                {
                    $data = array(
                                    "port_name"         =>  $aFetchedPort['port_name'],
                                    "port_image"        =>  $aFetchedPort['port_image'],
                                    "port_description"  =>  $aFetchedPort['port_description'],
                                    "port_longitude"    =>  $aFetchedPort['port_longitude'],
                                    "port_latitude"     =>  $aFetchedPort['port_latitude']
                                );
                    $where = array( "tourico_port_id"  =>  $aFetchedPort['port_id'] );
                    
                    $this->db->update('dz_tourico_cruise_port', $data, $where);
                }
                else
                {
                    $data = array(
                                    "tourico_port_id"  =>  $aFetchedPort['port_id'],
                                    "port_name"         =>  $aFetchedPort['port_name'],
                                    "port_image"        =>  $aFetchedPort['port_image'],
                                    "port_description"  =>  $aFetchedPort['port_description'],
                                    "port_longitude"    =>  $aFetchedPort['port_longitude'],
                                    "port_latitude"     =>  $aFetchedPort['port_latitude']
                                );
                    $this->db->insert('dz_tourico_cruise_port', $data); 
                }
            }
        }
    
    }
?>