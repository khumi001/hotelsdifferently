<?php
    class Tourico_hotel_price_multiplier extends CI_Model
    {
        function __construct()
        {
            parent::__construct();
        }
        function ProcessPriceMultiplier($hotelDetailList)
        {
            $hotelIdArr = array();
            foreach($hotelDetailList as $aHotelDetail )
                $hotelIdArr[] = $aHotelDetail['hotelId'];
            
            $query = $this->db->from('dz_tourico_hotel_price_multiplier')->where_in('tourico_hotel_id', $hotelIdArr)->get();
            $hotDealList = $query->result_array();
            
            $returningDetailList = array();
            foreach( $hotelDetailList as $aHotelDetail )
            {
                $price = null;
                foreach( $hotDealList as $aHotDeal )
                {
                    if( $aHotDeal['tourico_hotel_id']==$aHotelDetail['hotelId'] )
                    {
                        $price = $aHotDeal['multiplier_in_percentage'];
                        break;
                    }
                }
                $aHotelDetail['multiplier_in_percentage']  = $price;
                $returningDetailList[] = $aHotelDetail; 
            }
            return $returningDetailList;
        }
    
    
        function SaveMultiplier( $hotelId, $hotelName, $hotelImageUrl, $profitPercentage,$touricoDestinationName )
        {
            $query = $this->db->from('dz_tourico_hotel_price_multiplier')->where('tourico_hotel_id', $hotelId)->get();
            if ($query->num_rows() > 0)
            {
                $data = array(
                                "hotel_name"    =>  $hotelName,
                                "hotel_image_url"   =>  $hotelImageUrl,
                                "multiplier_in_percentage"  =>  $profitPercentage,
                                "tourico_destination_name"  =>  $touricoDestinationName
                            );
                $where = array( "tourico_hotel_id"  =>  $hotelId );
                
                $this->db->update('dz_tourico_hotel_price_multiplier', $data, $where);
            }
            else
            {
                $data = array(
                                "hotel_name"    =>  $hotelName,
                                "hotel_image_url"   =>  $hotelImageUrl,
                                "multiplier_in_percentage"  =>  $profitPercentage,
                                "tourico_hotel_id"  =>  $hotelId,
                                "tourico_destination_name"  =>  $touricoDestinationName
                            );
                $this->db->insert('dz_tourico_hotel_price_multiplier', $data); 
            }
        }
    }
?>