<?php
    class Cuise_price_multiplier_model extends CI_Model
    {
        function __construct()
        {
            parent::__construct();
        }
        
        function ProcessPriceMultiplier($cruiseDetailList)
        {
            if( count($cruiseDetailList)<=0 )
                return array();
            $cruiseIdArr = array();
            foreach($cruiseDetailList as $aCruiseDetail )
                $cruiseIdArr[] = $aCruiseDetail['cruise_itinerary_id'];
            
            $query = $this->db->from('dz_tourico_cruise_price_multiplier')->where_in('tourico_cruise_id', $cruiseIdArr)->get();
            $cruisePriceMultiplierList = $query->result_array();
            
            $returningDetailList = array();
            foreach( $cruiseDetailList as $aCruiseDetail )
            {
                $price = null;
                foreach( $cruisePriceMultiplierList as $aCruisePriceMultiplier )
                {
                    if( $aCruisePriceMultiplier['tourico_cruise_id']==$aCruiseDetail['cruise_itinerary_id'] )
                    {
                        $price = $aCruisePriceMultiplier['multiplier_in_percentage'];
                        break;
                    }
                }
                $aCruiseDetail['multiplier_in_percentage']  = $price;
                $returningDetailList[] = $aCruiseDetail; 
            }
            return $returningDetailList;
        }
        
        function SaveMultiplier( $cruiseId, $cruiseName, $shipName, $shipImageUrl, $profitPercentage, $touricoDestinationName )
        {
            $query = $this->db->from('dz_tourico_cruise_price_multiplier')->where('tourico_cruise_id', $cruiseId)->get();
            if ($query->num_rows() > 0)
            {
                $data = array(
                                "tourico_destination_name"    =>  $touricoDestinationName,
                                "cruise_name"   =>  $cruiseName,
                                "ship_name"  =>  $shipName,
                                "ship_image_url"  =>  $shipImageUrl,
                                "multiplier_in_percentage"  =>  $profitPercentage
                            );
                $where = array( "tourico_cruise_id"  =>  $cruiseId );
                
                $this->db->update('dz_tourico_cruise_price_multiplier', $data, $where);
            }
            else
            {
                $data = array(
                                "tourico_destination_name"    =>  $touricoDestinationName,
                                "cruise_name"   =>  $cruiseName,
                                "ship_name"  =>  $shipName,
                                "ship_image_url"  =>  $shipImageUrl,
                                "multiplier_in_percentage"  =>  $profitPercentage,
                                "tourico_cruise_id"  =>  $cruiseId
                            );
                $this->db->insert('dz_tourico_cruise_price_multiplier', $data); 
            }
        }
    }
?>