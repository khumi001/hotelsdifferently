<?php

class Affpayment_modal extends CI_Model {

    function payment_add($data) {
   //      print_r($data);

        $dataid = $data['payid'];
//       print_r($newid);exit;
        $id = $this->session->userdata['valid_affiliate']['id'];

        
//       print_r($data_array);exit;
        $query = $this->db->select('dz_user.var_fname,dz_user.var_lname,dz_user.var_accountid,dz_user.int_glcode,dz_user.var_email')
                        ->from('dz_user')
                      //  ->join('dz_user', 'dz_user.int_glcode = dz_affiliate_payments.fk_user')
                        ->where('int_glcode', $dataid)->get()->result_array();
//       $query[0]['var_email']
   //     print_r($query);exit;
        $amount1 = '$' . number_format((float) $data['amount'], 2, '.', ',');
        $this->email->set_mailtype("html");
        $this->email->from(NOREPLY, 'Admin');
        $this->email->to($query[0]['var_email']);
        $this->email->subject('Payment Detail');
        $mail_body.='<p style="color:#fff;margin-bottom:10px;">Dear ' . $query[0]['var_fname'] . ',</p>';
        $mail_body.='<p style="color:#fff;margin-bottom:10px;">We are delighted to let you know that your Affiliate payment has been issued today. Please see the details of your transaction below:</p>';
        $mail_body.= '<p style="color:#fff;margin-bottom:10px;"><b>Account ID: </b> '.$query[0]['var_accountid'].'</p>';
        $mail_body.= '<p style="color:#fff;margin-bottom:10px;"><b>Full name: </b>'.$query[0]['var_fname'].' '. $query[0]['var_fname'].'</p>';
        $mail_body.= '<p style="color:#fff;margin-bottom:10px;"><b>Payment method: </b>'.$data['optionsRadios'].'</p>';
        $mail_body.= '<p style="color:#fff;margin-bottom:10px;"><b>Payment amount: </b>'.$amount1.'</p>';
        $mail_body.= '<p style="color:#fff;margin-bottom:10px;"><b>Transaction ID / Confirmation number: </b>'.$data['ptransaction'].'</p>';
        $mail_body.= '<p style="color:#fff;margin-bottom:10px;">Please note that transaction costs charged by PayPal<sup>TM</sup> or WesternUnion<sup>TM</sup> will be deducted from your payment and that transaction times varies. For more information please refer to the Terms and Conditions of PayPal<sup>TM</sup> or WesternUnion<sup>TM</sup>.</p>';
        $mail_body.= '<p style="color:#fff;margin-bottom:10px;">Don&rsquo;t forget that we have annual incentives that we give out to our very best Affiliates (such as luxurious hotel stays and/or flights paid by <strong>HotelsDifferently<sup>sm</sup></strong>). Be our TOP AFFILIATE, MAKE MONEY and get rewarded with LUXURIOUS GIVEAWAYS!!!</p>

<p style="color:#fff;margin-bottom:10px;">Thank you for your continued hard work and the business you drive to our site!</p>';
        $mail_body.='<p style="color:#fff;margin-bottom:10px;">Sincerely,<br/><strong>HotelsDifferently<sup>sm</sup></strong> customer service<br/><span style="font-size:12px;">Take the hotel challenge!</span></p>';
       $mail_body .= '<p style="color:#fff;margin-bottom:10px;font-size:9px;">Cancellation policies between <b>HotelsDifferently<sup>sm</sup></b> quotes and competitor sites may vary.</p>';
        $data['mail_body'] = $mail_body;
        $message = $this->load->view('admin/account/genral_emailtemplate', $data, TRUE);
    //    echo $message;
   //     exit();
        $this->email->message($message);
        $this->email->send();
        
        $data_array = array(
            'fk_user' => $dataid,
            'var_status' => 'P',
            'var_amount' => $data['amount'],
            'fk_chagebackid' => $data['charjbackid'],
            'fk_commsionid' => $data['comssionid'],
            'var_paymethod' => $data['optionsRadios'],
            'var_transid' => $data['ptransaction'],
            'var_created_date' => date('Y-m-d'),
        );
        $this->db->insert('dz_affiliate_payments', $data_array);
        
        
        $reqstatus = array(
            'chr_status' => 'P'
        );
        $this->db->where_in('int_glcode', explode(',', $data['comssionid']));
        $this->db->update('dz_commision', $reqstatus);
        
        $reqstatus1 = array(
            'chr_status' => 'UCB'
        );
        $this->db->where_in('int_glcode', explode(',', $data['charjbackid']));
        $this->db->update('dz_chargeback', $reqstatus1);
        
//        print_r($this->db->last_query());exit;

        return TRUE;
    }

//     function statusunpaid($id) {
//        $reqstatus = array(
//            'var_status' => 'U'
//        );
//        $this->db->where('int_glcode', $id);
//        $this->db->update('dz_affiliate_payments', $reqstatus);
//        return TRUE;
//    }
}

?>
