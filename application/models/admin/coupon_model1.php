<?php
 class Coupon_model extends CI_Model{
     function coupon_generate($data){
       if($data['status'] == 'all'){
             $this->db->where('chr_user_type','U');
             $query = $this->db->get('dz_user')->result_array();
           if($data['coupanoff1'] == ''){
                    $coupanvalue = $data['coupanoff2'];
                     for($i=0; $i<count($query); $i++){
                   $couponcode = $this->couponcode();
                   $data_array = array(
                     'fk_user'=>$query[$i]['int_glcode'],
                     'var_couponcode'=> $couponcode.$i,
                     'var_couponvalue'=>$coupanvalue.$i,
                     'var_coupan_type'=>'2',
                     'var_created_date'=>date('Y-m-d h:i'),
                     //'var_updated_date'=>date('Y-m-d h:i:s'),
                 );
                    $this->db->insert('dz_coupons',$data_array);
                }
           }else{
               $coupanvalue = $data['coupanoff1'];
                for($i=0; $i<count($query); $i++){
              $couponcode = $this->couponcode();
              $data_array = array(
                'fk_user'=>$query[$i]['int_glcode'],
                'var_couponcode'=> $couponcode.$i,
                'var_couponvalue'=>$coupanvalue.$i,
                'var_coupan_type'=>'1',
                'var_created_date'=>date('Y-m-d h:i'),
                //'var_updated_date'=>date('Y-m-d h:i:s'),
            );
               $this->db->insert('dz_coupons',$data_array);
           }
           }
            
         }else{
             $this->db->where_in('int_glcode',$data['status']);
             $query = $this->db->get('dz_user')->result_array();
            for($i=0; $i<count($query); $i++){
                $couponcode = $this->couponcode();
                $data_array = array(
                    'fk_user'=>$query[$i]['int_glcode'],
                    'var_couponcode'=> $couponcode.$i,
                    'var_couponvalue'=>'25',
                    'var_created_date'=>date('Y-m-d h:i'),
                );
                $this->db->insert('dz_coupons',$data_array);
            }
         }
       //$couponcode = $this->couponcode();
//       $this->db->where("chr_user_type","M");
//       $query = $this->db->get('dz_user')->result_array();
//       for($i=0;$i<count($query);$i++){
//            $this->email->set_mailtype("html");            
//            $this->email->from(NOREPLY, 'Admin');
//            $this->email->to($query[$i]['var_email']);
//            $this->email->subject('Your coupon discount code');
//            $mail_body ='<p><h3>Helllo "'.$query[$i]['var_username'].'"</h3></p>';
//            $mail_body .='<p>Your coupon code :"'.$couponcode.'"</p>';
//            $mail_body .='<p>Please Save it.</p>';
//            $mail_body .='<p>Regard,</br>Admin</p>';
//            $this->email->message($mail_body);
//            $this->email->send(); 
//            $rslt[] = array(
//                'fk_user' => $query[$i]['int_glcode'],
//                'var_status'=>'Active',
//            );
//       }
//       $id = $this->session->userdata['valid_user']['id'];
//       if($data['coupon'] == '1'){
//       $coupon = array(
//           'fk_user' =>$id,
//           'var_couponcode'=>$couponcode,
//           'var_user'=>'$rslt',
//           'var_peroff'=>$data['couponper'],
//           'var_couponexpi'=>$data['expdate'],
//           'var_status'=>'Active',
//           'var_created_date'=>date('Y-m-d',strtotime($data['expdate']))
//       );
//       }else{
//       $coupon = array(
//           'fk_user' =>$id,
//           'var_couponcode'=>$couponcode,
//           'var_user'=>'$rslt',
//           'var_couponoff'=>$data['couponoff'],    
//           'var_couponexpi'=>$data['expdate'],
//           'var_status'=>'Active',
//           'var_created_date'=>date('Y-m-d',strtotime($data['expdate']))
//       );
//       }
//        $this->db->insert('dz_coupons',$coupon);
        return TRUE;
     }
     function status_update($data){
                  
         $query = $this->db->get_where('dz_coupons',array('int_glcode'=>$data['id']))->result_array();
        
         if($query[0]['var_status'] == 'A'){
             $data_array = array(
                 'var_status' =>'D',
             );
             $this->db->where('int_glcode',$data['id']);
             $this->db->update('dz_coupons',$data_array);
         }else{
             $data_array = array(
                 'var_status' =>'A',
             );
             $this->db->where('int_glcode',$data['id']);
             $this->db->update('dz_coupons',$data_array);
         }
         return True;
     }
     
     function couponcode($numno=2,$numAlpha = 4,$numno=3) {
        $listAlpha = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $listno = '0123456789';
        return str_shuffle(
                substr(str_shuffle($listno), 0,$numno).
                substr(str_shuffle($listAlpha), 0, $numAlpha).
                substr(str_shuffle($listno), 0,$numno)
        );
    }
     
    function status_singup(){
        $this->db->select('chr_status');
        $this->db->where('int_glcode',1);
        $query = $this->db->get('dz_singup_coupons')->result_array();
        if($query[0]['chr_status'] == '1'){
            $data_array = array(
                'chr_status' =>'0',
                'dt_update_date'=>date("Y-m-d H:i:s"),
            );
        }else{
            $data_array = array(
                'chr_status' =>'1',
                'dt_update_date'=>date("Y-m-d H:i:s"),
            );
        }
        $this->db->where('int_glcode',1);
        $this->db->update('dz_singup_coupons',$data_array);
        return TRUE;
    }
 }
?>
