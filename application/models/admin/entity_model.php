<?php

class entity_model extends CI_Model {
    /*
    *	Arguments
    *	1 User status Not Null
    */
    public function getEnrollments($status){
        if(empty($status))
            return [];
        $this->db->select('dz_user.*, entity_detail.*, users_memberships.endDate,users_memberships.isLifeTime, states.name as state_name, countries.name as country_name');
        $this->db->from('dz_user');
        $this->db->join('entity_detail', 'entity_detail.user_id= dz_user.int_glcode');
        $this->db->join('states', 'entity_detail.state = states.id', 'left');
        $this->db->join('countries', 'entity_detail.country = countries.id', 'left');
        $this->db->join('users_memberships', 'dz_user.int_glcode = users_memberships.userId', 'left');
        $this->db->where('dz_user.chr_user_type', 'EN');
        $this->db->where('dz_user.chr_status', $status);
        $data =  $this->db->get();
        //echo "<pre>";print_r($data->result());
        //echo $this->db->last_query();
        //exit;
        return $data;
    }
    public function getEnrollmentsWithId($id){
        if(empty($id))
            return [];
        $this->db->select('dz_user.*, entity_detail.*, states.name as state_name, countries.name as country_name');
        $this->db->from('dz_user');
        $this->db->join('entity_detail', 'entity_detail.user_id= dz_user.int_glcode');
        $this->db->join('countries', 'entity_detail.country = countries.id', 'left');
        $this->db->join('states', 'entity_detail.state = states.id', 'left');
        $this->db->where('dz_user.int_glcode', $id);
        $data =  $this->db->get();
        //echo $this->db->last_query();
        //exit;
        return $data;
    }

    public function getSubEntities($status){
        if(empty($status))
            return [];
        $this->db->select('dz_user.*,users_memberships.*');
        $this->db->from('dz_user');
        $this->db->join('users_memberships', 'users_memberships.userId= dz_user.int_glcode');
        $this->db->where('dz_user.chr_user_type', 'EN');
        $this->db->where('dz_user.entity_parent_account !=', '0');
        $this->db->where('dz_user.chr_status', $status);
        $data =  $this->db->get();
        return $data;
    }

    public function getEntityRevenue($from='',$to=''){
        $finalDataArray = array();
        $this->db->select('entity_detail.company_name,dz_user.int_glcode,dz_user.var_email,dz_user.var_fname,dz_user.var_lname,dz_user.var_phone');
        $this->db->from('dz_user');
        $this->db->join('entity_detail', 'entity_detail.user_id= dz_user.int_glcode');
        $this->db->where('dz_user.chr_user_type', 'EN');
        $this->db->where('dz_user.entity_parent_account', '0');
        $data =  $this->db->get();
        foreach ($data->result() as $k=>$v) {
            // get sub entities
            $this->db->select('SUM(booking_detail.return_amount)as totalRefund,SUM(booking_detail.publishPrice)as totalSpend,dz_user.int_glcode,dz_user.var_email,dz_user.var_fname,dz_user.var_lname,dz_user.var_phone');
            $this->db->from('dz_user');
            $this->db->join('booking_detail', 'booking_detail.uid= dz_user.int_glcode');
            if(!empty($from))
                $this->db->where('booking_detail.fromDate >= "'.$from.'"');

            if(!empty($to))
                $this->db->where('booking_detail.toDate <= "'.$to.'"');

            $this->db->where('dz_user.chr_user_type', 'EN');
            $this->db->where('dz_user.entity_parent_account', $v->int_glcode);
            $subData =  $this->db->get()->row();
            $finalData = array(
                'id'=>$v->int_glcode,
                'company'=>$v->company_name,
                'email'=>$v->var_email,
                'phone'=>$v->var_phone,
                'name'=>$v->var_fname.' '.$v->var_lname,
                'totalSpend'=>$subData->totalSpend,
                'refund'=>$subData->totalRefund,
                'revenue'=>$subData->totalSpend - $subData->totalRefund,
            );
            $finalDataArray[] = $finalData;
        }

        //echo '<pre>';print_r($this->db->last_query());exit;
        return $finalDataArray;
    }
}
?>