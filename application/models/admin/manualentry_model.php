<?php

class Manualentry_model extends CI_Model {

    function get_quetes($data) {

        $result = $this->db->select('
            dz_user.int_glcode,
            dz_user.var_username,
            dz_rplyquote.var_prize,
            dz_rplyquote.int_site2_price,
            dz_rplyquote.int_site1_price,
            dz_rplyquote.fk_coupon,
            dz_quote.int_glcode as quoteid
            ')
                        ->from('dz_rplyquote')
                        ->join('dz_quote', 'dz_quote.int_glcode=dz_rplyquote.int_fkquote')
                        ->join('dz_user', 'dz_user.int_glcode=dz_quote.var_fkuser')
                        ->where('dz_rplyquote.var_transactionid', $data['id'])->get()->result_array();

        return $result;
    }

    function insert_data($data) {
        $fkid = $data['fkid'];
        $this->db->where('dz_quote_id', $data['id']);
        $savingdatabase = $this->db->get('dz_savingdatabase')->result_array();

        if (!empty($savingdatabase)) {
            return FALSE;
        }

        $this->db->where('var_transactionid', $data['id']);
        $replaydata = $this->db->get('dz_rplyquote')->result_array();

        $fkquote = $replaydata[0]['int_fkquote'];
        $this->db->where('int_glcode', $fkquote);
        $datedata = $this->db->get('dz_quote')->result_array();

        $checkin1 = $datedata[0]['var_checkin'];
        $checkout1 = $datedata[0]['var_checkout'];

        $checkin = date('m/d', strtotime($checkin1));
        $checkout = date('m/d', strtotime($checkout1));

        $night1 = $datedata[0]['var_night'];

        $HotelCity = explode(',', $datedata[0]['var_city']);
        if (count($HotelCity) == 4) {
            $city = $HotelCity[1];
        } else if (count($HotelCity) == 3) {
            $city = $HotelCity[0];
        } else if (count($HotelCity) == 5) {
            $city = $HotelCity[2];
        }

        if ($data['saveprice'] < 0) {
            $htr = str_ireplace('-', '-$', $data['saveprice']);
        } else {
            $htr = '$' . $data['saveprice'];
        }

        $hotel = $replaydata[0]['var_NOH'];

        $site1 = $replaydata[0]['int_site1_price'];
        $site2 = $replaydata[0]['int_site2_price'];
        if ($site2 != 0) {
            if ($site1 < $site2) {
                $site_data = $replaydata[0]['var_site1'];
                $htr1 = (100 - (($data['quote'] * 100) / $replaydata[0]['int_site1_price']));
                $mailcounter = $site2 - $data['quote'];
            } else {
                $site_data = $replaydata[0]['var_site2'];
                $htr1 = (100 - (($data['quote'] * 100) / $replaydata[0]['int_site2_price']));
                $mailcounter = $site1 - $data['quote'];
            }
        } else {
            $site_data = $replaydata[0]['var_site1'];
            $htr1 = (100 - (($data['quote'] * 100) / $replaydata[0]['int_site1_price']));
            $mailcounter = $site1 - $data['quote'];
        }


        $night1 = $htr1 / $night1;

        if ($night1 < 0) {
            $night = str_ireplace('-', '-$', $night1);
        } else {
            $night = '$' . $night1;
        }

        $htr2 = round($htr1);
        $htr3 = number_format((float) $htr2, 2, '.', ',');
        $str = '<span>' . $data['username'] . '</span> just saved <span> ' . $htr2 . '% </span> at the <span>' . $hotel . '</span> for <span>' . $checkin . ' - ' . $checkout . '</span> based on <span>' . $site_data . '</span> pricing!*';

        $data_array = array(
            'var_username' => $data['username'],
            'int_quote' => $data['quote'],
            'int_highprice' => $data['highprice'],
            'int_saved' => $data['saveprice'],
            'fk_user' => $fkid,
            'dz_text' => $str,
            'dz_quote_id' => $data['id'],
            'var_city' => $city,
        );

        $this->db->insert('dz_savingdatabase', $data_array);

        // add home counter
        $this->db->where('int_glcode', 1);
        $maincounter = $this->db->get('dz_maincounter')->result_array();

        $amount = $maincounter[0]['int_amount'] + $mailcounter;

        $data_array4 = array(
            'int_amount' => $amount,
        );
        $this->db->where('int_glcode', 1);
        $this->db->update('dz_maincounter', $data_array4);

        $this->db->where("int_glcode", $fkid);
        $query = $this->db->get('dz_user')->result_array();

        return TRUE;
    }

    function addpromocode($post) 
	{
        $data_array = array(
            'var_promocode' => $post['promocode'],
            'int_amount' => $post['amount'],
            'minspend' => $post['minspend'],
        );
        $this->db->insert('dz_promocode', $data_array);
        //edit by tayyab for acativity log
        $insert_id = $this->db->insert_id();
        $CI = & get_instance();
        $CI->load->model('admin/admin_log_model');
        $log = array(
            'log_type' => 'new_promo_added',
            'log_type_fk_id' => $insert_id,
            'log_desc' => ''
        );
        $CI->admin_log_model->addlog($log);
        return TRUE;
    }

    function deletepromocode($data) {
        $promoCodeQuery = $this->db->query('SELECT var_promocode,int_amount FROM dz_promocode WHERE int_glcode = '.$data['id'].'');
        $promoResult    = $promoCodeQuery->row();
        $log_type       = 'Promo code deleted with code '.$promoResult->var_promocode.' and amount '.$promoResult->int_amount;
        $this->db->where('int_glcode', $data['id']);
        $this->db->delete('dz_promocode');
        //edit by tayyab for acativity log
        $CI = & get_instance();
        $CI->load->model('admin/admin_log_model');
        $log = array(
            'log_type' => 'promo_code_deleted',
            'log_type_fk_id' => '0',
            'log_desc'  => $log_type
        );
        $CI->admin_log_model->addlog($log);
        return TRUE;
    }

}

?>