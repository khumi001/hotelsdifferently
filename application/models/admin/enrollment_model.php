<?php

class enrollment_model extends CI_Model {
    /*
    *	Arguments
    *	1 User status Not Null
    */
    public function getEnrollments($status){
        if(empty($status))
            return [];
        $this->db->select('dz_user.*, travel_professional_detail.*, states.name as state_name, countries.name as country_name, company_types.name as company_type_name');
        $this->db->from('dz_user');
        $this->db->join('travel_professional_detail', 'travel_professional_detail.user_id= dz_user.int_glcode');
        $this->db->join('states', 'travel_professional_detail.state = states.id', 'left');
        $this->db->join('countries', 'travel_professional_detail.country = countries.id', 'left');
        $this->db->join('company_types', 'travel_professional_detail.company_type = company_types.id', 'left');
        $this->db->where('dz_user.chr_user_type', 'TF');
        $this->db->where('dz_user.chr_status', $status);
        $data =  $this->db->get();
        //echo $this->db->last_query();
        //exit;
        return $data;
    }
    public function getEnrollmentsWithId($id){
        if(empty($id))
            return [];
        $this->db->select('dz_user.*, travel_professional_detail.*, states.name as state_name, countries.name as country_name, company_types.name as company_type_name');
        $this->db->from('dz_user');
        $this->db->join('travel_professional_detail', 'travel_professional_detail.user_id= dz_user.int_glcode');
        $this->db->join('countries', 'travel_professional_detail.country = countries.id', 'left');
        $this->db->join('states', 'travel_professional_detail.state = states.id', 'left');
        $this->db->join('company_types', 'travel_professional_detail.company_type = company_types.id', 'left');
        $this->db->where('dz_user.int_glcode', $id);
        $data =  $this->db->get();
        //echo $this->db->last_query();
        //exit;
        return $data;
    }
    public function getSubscribedUsers(){
        $this->db->select('dz_user.var_accountid,dz_user.var_email,dz_user.int_glcode,dz_user.var_fname,dz_user.var_lname,dz_user.is_subscribed,dz_user.var_phone,users_memberships.*');
        $this->db->from('dz_user');
        $this->db->join('users_memberships', 'users_memberships.userId=dz_user.int_glcode AND (users_memberships.status != 1 OR users_memberships.status != 0)');
        $this->db->where('dz_user.is_subscribed', 2);
        $this->db->or_where('dz_user.is_subscribed', 4);
        $data =  $this->db->get()->result();
        //echo $this->db->last_query();exit;
        return $data;
    }

    public function getMembershipUsers($status){
        $this->db->select('dz_user.var_accountid,dz_user.fk_parent_account,dz_user.var_email,dz_user.int_glcode,dz_user.var_fname,dz_user.var_lname,dz_user.cardNumber,dz_user.is_subscribed,dz_user.manuallExtension,dz_user.var_phone,users_memberships.*');
        $this->db->from('dz_user');
        $this->db->join('users_memberships', 'users_memberships.userId=dz_user.int_glcode AND (users_memberships.status = 1 OR users_memberships.status = 0)');
        $this->db->where('(dz_user.is_subscribed = 1 OR dz_user.is_subscribed = 3) AND dz_user.chr_user_type = "U"');
        //$this->db->where();
        if($status == 1){
            $this->db->where('DATE(users_memberships.endDate) >= DATE(NOW())');
        }else if($status == 2){
            $this->db->where('DATE(users_memberships.endDate) <= DATE(NOW() - INTERVAL 1 DAY)');
        }
        $data =  $this->db->get()->result();
        //echo $this->db->last_query();
        //exit;
        return $data;
    }

    public function getMembershipUserById($id){
        $this->db->select('dz_user.*,users_memberships.*');
        $this->db->from('dz_user');
        $this->db->join('users_memberships', 'users_memberships.userId=dz_user.int_glcode AND (users_memberships.status = 1 OR users_memberships.status = 0)');
        $this->db->where('(dz_user.is_subscribed = 1 OR dz_user.is_subscribed = 3) AND dz_user.chr_user_type = "U"');
        $this->db->where('dz_user.int_glcode', $id);
        $data =  $this->db->get()->result();
        //echo $this->db->last_query();
        //exit;
        return $data;
    }

    public function getRevenue($from='',$to=''){
        $finalDataArray = array();
        $this->db->select('travel_professional_detail.*,dz_user.int_glcode,dz_user.var_email,dz_user.var_fname,dz_user.var_lname,dz_user.var_phone');
        $this->db->from('dz_user');
        $this->db->join('travel_professional_detail', 'travel_professional_detail.user_id = dz_user.int_glcode');
        $this->db->where('dz_user.chr_user_type', 'TF');
        $data =  $this->db->get();
        foreach ($data->result() as $k=>$v) {
            $this->db->select('SUM(booking_detail.return_amount)as totalRefund,SUM(booking_detail.publishPrice)as totalSpend,dz_user.int_glcode,dz_user.var_email,dz_user.var_fname,dz_user.var_lname,dz_user.var_phone');
            $this->db->from('dz_user');
            $this->db->join('booking_detail', 'booking_detail.uid= dz_user.int_glcode');
            if(!empty($from))
                $this->db->where('booking_detail.fromDate >= "'.$from.'"');

            if(!empty($to))
                $this->db->where('booking_detail.toDate <= "'.$to.'"');

            $this->db->where('dz_user.chr_user_type', 'TF');
            $subData =  $this->db->get()->row();
            $finalData = array(
                'id'=>$v->int_glcode,
                'company'=>$v->company_name,
                'email'=>$v->var_email,
                'phone'=>$v->var_phone,
                'name'=>$v->var_fname.' '.$v->var_lname,
                'totalSpend'=>$subData->totalSpend,
                'refund'=>$subData->totalRefund,
                'revenue'=>$subData->totalSpend - $subData->totalRefund,
            );
            $finalDataArray[] = $finalData;
        }

        //echo '<pre>';print_r($finalDataArray);exit;
        return $finalDataArray;
    }
}
?>