<?php

class admin_log_model extends CI_Model {

    function addlog($data) {
        date_default_timezone_set('America/Los_Angeles');
        $CI = & get_instance();
        $data = array(
            'user_id' => $_SESSION['valid_adminuser']['id'],
            'log_type' => $data['log_type'],
            'log_time' => date("Y-m-d H:i:s"),
            'log_type_fk_id' => $data['log_type_fk_id'],
            'log_desc' => $data['log_desc'],
        );
        $this->db->insert('dz_admin_log', $data);
        return true;
    }

    function getlog() {
        $log_result = $this->db->query('SELECT al.*,u.var_username , u.var_email '
                        . 'FROM dz_admin_log al,dz_user u '
                        . 'WHERE al.user_id = u.int_glcode order by al.id DESC')->result();
        
        if (count($log_result) > 0) {
            $i = 0;
            foreach ($log_result as $log_row) {
                switch ($log_row->log_type) {
                    case "new_promo_added":
                        $result_ar[$i]['type'] = 'New Promocode code is added by ' . $log_row->var_email;
                        $log_meta = $this->db->query('SELECT var_promocode,int_amount '
                                        . 'FROM dz_promocode '
                                        . 'WHERE int_glcode = ' . $log_row->log_type_fk_id . '')->row();
                        $result_ar[$i]['desc'] = 'A promocode is added by ' . $log_row->var_email . ' with code ' . $log_meta->var_promocode . ' and amount ' . $log_meta->int_amount;
                        $result_ar[$i]['time'] = $log_row->log_time;
                        break;
                    case "promo_code_deleted":
                        $result_ar[$i]['type'] = 'Promocode code is deleted by ' . $log_row->var_email;
                        $result_ar[$i]['desc'] = $log_row->log_desc;
                        $result_ar[$i]['time'] = $log_row->log_time;
                        break;
                    case "hotel_multiplier_updated":
                        $result_ar[$i]['type'] = 'A hotel Multiplier is updated by ' . $log_row->var_email;
                        $result_ar[$i]['desc'] = $log_row->log_desc;
                        $result_ar[$i]['time'] = $log_row->log_time;
                        break;
                    case "new_hote_multiplier_added":
                        $result_ar[$i]['type'] = 'New Multiplier is added by ' . $log_row->var_email;
                        $log_meta = $this->db->query('SELECT m.multiplier_in_percentage '
                                        . 'FROM dz_tourico_hotel_price_multiplier m '
                                        . 'WHERE  m.tourico_hotel_id = ' . $log_row->log_type_fk_id . '')->row();
                        $result_ar[$i]['desc'] = $log_row->log_desc. ' with price ' . $log_meta->multiplier_in_percentage;
                        $result_ar[$i]['time'] = $log_row->log_time;
                        break;
                    case "new_coupen_genrated":
                        $result_ar[$i]['type'] = 'A coupon is genrated by ' . $log_row->var_email;
                        $log_meta = $this->db->query('SELECT u.var_username,c.var_couponcode,c.var_couponvalue,c.var_coupan_type '
                                        . 'FROM dz_user u,dz_coupons c '
                                        . 'WHERE c.fk_user = u.int_glcode and c.int_glcode = ' . $log_row->log_type_fk_id . '')->row();
                        $amount = ($log_meta->var_coupan_type == 1) ? $log_meta->var_couponvalue . '%' : $log_meta->var_couponvalue;
                        $result_ar[$i]['desc'] = 'A coupen is genrated by ' . $log_row->var_username . ' for ' . $log_meta->var_username . ' with code:' . $log_meta->var_couponcode . ' and amount: ' . $amount;
                        $result_ar[$i]['time'] = $log_row->log_time;
                        break;
                    case "authorize_menu":
                        $result_ar[$i]['type'] = 'Menu authorized by ' . $log_row->var_email;
                        $log_meta = $this->db->query('SELECT u.var_email,au.var_authorization '
                                        . 'FROM dz_user u,dz_giveauthorize au '
                                        . 'WHERE au.fk_user = u.int_glcode and au.fk_user = ' . $log_row->log_type_fk_id . '')->row();
                        $amount = ($log_meta->var_coupan_type == 1) ? $log_meta->var_couponvalue . '%' : $log_meta->var_couponvalue;
                        $result_ar[$i]['desc'] = 'Menu is authorized by ' . $log_row->var_username . ' for ' . $log_meta->var_email;
                        $result_ar[$i]['time'] = $log_row->log_time;
                        break;
					case "decline_pen_reservation":
                        $result_ar[$i]['type'] = 'Reservation is decline by ' . $log_row->var_email;
                        $result_ar[$i]['desc'] = 'Reservation is decline by '.$log_row->var_email.' '.$log_row->log_desc;
                        $result_ar[$i]['time'] = $log_row->log_time;
                        break;
					case "change_con_num":
                        $result_ar[$i]['type'] = 'changed Tourico reservation number by ' . $log_row->var_email;
                        $result_ar[$i]['desc'] = $log_row->var_email.' '.$log_row->log_desc;
                        $result_ar[$i]['time'] = $log_row->log_time;
                        break;
					case "change_res_source":
                        $result_ar[$i]['type'] = 'changed reservation source by ' . $log_row->var_email;
                        $result_ar[$i]['desc'] = $log_row->var_email.' '.$log_row->log_desc;
                        $result_ar[$i]['time'] = $log_row->log_time;
                        break;
					case "approve_pen_reservation":
                        $result_ar[$i]['type'] = 'Reservation is approved by ' . $log_row->var_email;
                        $result_ar[$i]['desc'] = $log_row->log_desc;
                        $result_ar[$i]['time'] = $log_row->log_time;
                        break;
					case "activate_coupon":
                        $result_ar[$i]['type'] = $log_row->log_desc.' by ' . $log_row->var_email;
                        $result_ar[$i]['desc'] = $log_row->log_desc;
                        $result_ar[$i]['time'] = $log_row->log_time;
                        break;
					case "deactivate_coupon":
                        $result_ar[$i]['type'] = $log_row->log_desc.' by ' . $log_row->var_email;
                        $result_ar[$i]['desc'] = $log_row->log_desc;
                        $result_ar[$i]['time'] = $log_row->log_time;
                        break;
                    case "ip_banned":
                        $result_ar[$i]['type'] = 'A IP is banned by ' . $log_row->var_email;
                        $result_ar[$i]['desc'] = $log_row->log_desc;
                        $result_ar[$i]['time'] = $log_row->log_time;
                        break;
					case "lift_ip_ban":
                        $result_ar[$i]['type'] = 'IP Lift by ' . $log_row->var_email;
                        $result_ar[$i]['desc'] = $log_row->var_email.' '.$log_row->log_desc;
                        $result_ar[$i]['time'] = $log_row->log_time;
                        break;
                    case "user_deleted":
                        $result_ar[$i]['type'] = 'A user is deleted by ' . $log_row->var_email;
                        $result_ar[$i]['desc'] = $log_row->log_desc;
                        $result_ar[$i]['time'] = $log_row->log_time;
                        break;
					case "update_all_hotel_price":
                        $result_ar[$i]['type'] = 'ALL HOTELS price update by ' . $log_row->var_email;
                        $result_ar[$i]['desc'] = $log_row->var_email.$log_row->log_desc;
                        $result_ar[$i]['time'] = $log_row->log_time;
                        break;
					case "add_all_hotel_price":
                        $result_ar[$i]['type'] = 'ALL HOTELS price add by ' . $log_row->var_email;
                        $result_ar[$i]['desc'] = $log_row->var_email.$log_row->log_desc;
                        $result_ar[$i]['time'] = $log_row->log_time;
                        break;
                    default:;
                }
               $i++; 
            }
        }
        return $result_ar;
    }

}

?>