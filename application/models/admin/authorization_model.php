<?php

class Authorization_model extends CI_Model {

    function menuauthorize($data) {
        $allmenu = array(
            /*'Quote Requests' => 'no',
'Sent Quotes' => 'no',
'Hotel Price Updated' => 'no',
'Promo Codes' => 'no',*/
            'Pending Enrollment'=>'no',
            'Membership Database'=>'no',
            'Sub Entities'=>'no',
            'Entity Pending Enrollment'=>'no',
            'Entities Database'=>'no',
            'TP Pending Enrollments'=>'no',
            'Approval Enrollments'=>'no',
            'TP Database'=>'no',
            'Confirmed Reservations' => 'no',
            'Pending Reservations' => 'no',
            'Pending Cancellations' => 'no',
            /*'Cancel reservations' => 'no',*/
            'Processed Cancellations' => 'no',
            "Activities"=>'no',
            'Coupons' => 'no',
            /*'Affiliate Request' => 'no',
            'Affiliate Database' => 'no',
            'Affiliate Tracker' => 'no',
            'Affiliate Payment' => 'no',
            'Posted Payments' => 'no',
            'Chargebacks' => 'no',
            'Processed Chargebacks' => 'no',*/
            'Authorization' => 'no',
            'IP Address BAN' => 'no',
            'Processed IP BAN' => 'no',
            'Profile' => 'no',
            'Master Account' => 'no',
            'User Markup'=>'no',
            'Statistics' => 'no',
            'Revenue'=>'no',
            /*'Database Management' => 'no',*/
            'Login Log' => 'no',
            'Admin Activity Log' => 'no',
        );

        foreach ($allmenu as $key => $value) {
            for ($i = 0; $i < count($data['authocheck']); $i++) {
                if ($data['authocheck'][$i] == $key) {
                    $allmenu[$key] = 'yes';
                }
            }
        }
        $authocheckarray = serialize($allmenu);
        $menuvalue = urlencode($authocheckarray);
        $dataarray = array(
            'var_authorization' => $menuvalue
        );
        $this->db->where('fk_user', $data['id']);
        $this->db->update('dz_giveauthorize', $dataarray);

        /*$data_array = array(
            'chr_user_type' => 'A',
        );
        $this->db->where('int_glcode', $data['id']);
        $this->db->update('dz_user', $data_array);*/
        //edit by tayyab for acativity log
        $CI = & get_instance();
        $CI->load->model('admin/admin_log_model');
        $log = array(
            'log_type' => 'authorize_menu',
            'log_type_fk_id' => $data['id'], //user id
            'log_desc' => 'Menu is authorize.'
        );
        $CI->admin_log_model->addlog($log);
        return true;
    }

    function userdelete($data) {

        $userQuery = $this->db->query('SELECT var_email FROM dz_user WHERE int_glcode = '.$data['id'].'');
        $userResult = $userQuery->row();
        $log_type = 'A user deleted with email '.$userResult->var_email;
        $this->db->where('int_glcode', $data['id']);
        $this->db->delete('dz_user');
        //edit by tayyab for acativity log
        $CI = & get_instance();
        $CI->load->model('admin/admin_log_model');
        $log = array(
            'log_type' => 'user_deleted',
            'log_type_fk_id' => $data['id'],
            'log_desc' => $log_type
        );
        $CI->admin_log_model->addlog($log);
        return TRUE;
    }

}

?>
