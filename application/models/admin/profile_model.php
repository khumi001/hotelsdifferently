<?php
class Profile_model extends CI_Model
{
	function __construct() {
        parent::__construct();
		$ipban = $this->toval->ipban();
		if(count($ipban) > 0)
        {
			if($_SERVER['REQUEST_URI'] != '/ipban')
			{
				redirect('ipban', 'location');
			}
        }
	}
    function editprofile()
    {
       $data= $this->session->userdata['valid_adminuser'];
       $id= $data['id'];
     
       $allResult=array();
       $this->db->where('int_glcode',$id);
       $query= $this->db->get('dz_user');
       $result= $query->result_array();
       //print_r($result); exit;
         return $result;            
    }
    function edit($data)
    {  
        $id = $this->session->userdata['valid_adminuser']['id'];
//        echo $id; exit();
        $data_user = array(
            'var_fname'   =>$data['fname'],
            'var_lname'   =>$data['lname'],
            'var_username'=>$data['username'],
            'var_email'   =>$data['email'], 
            'var_company'   =>$data['company'], 
            'var_address1'   =>$data['add'], 
            'var_city'   =>$data['city'], 
            );
        $this->db->where('int_glcode',$id);
        $this->db->update('dz_user',$data_user);
        return TRUE;
    }
    
     function change_password($data)
    {
        $id= $this->session->userdata['valid_adminuser']['id']; 
//       echo "$id";exit;
        $this->db->where('int_glcode',$id);
        $this->db->where('var_password',base64_encode($data['old_psw']));
        $query = $this->db->get('dz_user');
        
        if ($query->num_rows() == 1) {
            $content = array(
                'var_password' => base64_encode($data['new_psw']),
             );
            $this->db->where('int_glcode',$id);
            $this->db->update('dz_user',$content);
            return TRUE;
        }else{
            return FALSE;
        }
    }
     function update_img($data,$sts)
     {               
        if($sts == 'consultant')
        {
         //   print_r($this->session->all_userdata());exit;
            $id= $this->session->userdata['valid_adminuser']['id'];           
        //    $img = 'public/uploads/profilepic/' . $data;
            $img = 'public/uploads/profilepic/'.$data;            
            $data_arry = array(
                'profile_pic' => $img,
            );
            $this->db->where('int_glcode', $id);
            $this->db->update('dz_user', $data_arry);
        }                
    }
    function get_profile()
    { 
            $id = $this->session->userdata['valid_adminuser']['id'];
            $this->db->where('int_glcode',$id);
            $sql = $this->db->get('dz_user')->result_array();
            
            return $sql;
    }
}
?>
