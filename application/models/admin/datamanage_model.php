<?php
class Datamanage_model extends CI_Model
{
    function countryadd($data){
        $data_array = array(
            'var_country' => $data['country'],
            'var_createddate' => date('Y-m-d'),
        );
        $this->db->insert('dz_country',$data_array);
        return TRUE;
    }
    function countrydelete($data){
        $this->db->where('int_glcode',$data['id']);
        $this->db->delete('dz_country');
        
        $this->db->where('fk_country',$data['id']);
        $this->db->delete('dz_state');
        
        $this->db->where('fk_country',$data['id']);
        $this->db->delete('dz_city');
        
        $this->db->where('fk_country',$data['id']);
        $this->db->delete('dz_hotel');
        
        return TRUE;
    }
    function countryupdate($data){
        $data_array = array(
            'var_country' => $data['country'],
            'var_updatedate' => date('Y-m-d'),
        );
        $this->db->where('int_glcode',$data['id']);
        $this->db->update('dz_country',$data_array);
        return TRUE;
    }
    function stateadd($data){
        $data_array = array(   
            'fk_country' => $data['fk_contry'],
            'var_state' => $data['state'],
            'var_createddate' => date('Y-m-d'),
        );
        $this->db->insert('dz_state',$data_array);
        return TRUE;
    }
    function statedelete($data){
        $this->db->where('int_glcode',$data['id']);
        $this->db->delete('dz_state');
        
        $this->db->where('fk_state',$data['id']);
        $this->db->delete('dz_city');
        
        $this->db->where('fk_state',$data['id']);
        $this->db->delete('dz_hotel');
        
        return TRUE;
    }
    function stateupdate($data){
        $data_array = array(
            'var_state' => $data['state'],
            'var_updateddate' => date('Y-m-d'),
        );
        $this->db->where('int_glcode',$data['id']);
        $this->db->update('dz_state',$data_array);
        return TRUE;
    }
    function cityadd($data){
        $data_array = array(
            'fk_country'=>$data['fk_contry'],
            'fk_state' => $data['fk_state'],
            'var_city' => $data['city'],
            'var_createddate' => date('Y-m-d'),
        );
        $this->db->insert('dz_city',$data_array);
        return TRUE;
    }
    function citydelete($data){
        $this->db->where('int_glcode',$data['id']);
        $this->db->delete('dz_city');
        
        $this->db->where('fk_city',$data['id']);
        $this->db->delete('dz_hotel');
        return TRUE;
    }
    function cityupdate($data){
        $data_array = array(
            'var_city' => $data['city'],
            'var_updateddate' => date('Y-m-d'),
        );
        $this->db->where('int_glcode',$data['id']);
        $this->db->update('dz_city',$data_array);
        return TRUE;
    }
    function hoteladd($data){
        $data_array = array(
            'fk_country' =>$data['fk_contry'],
            'fk_state' =>$data['fk_state'],
            'fk_city' =>$data['fk_city'],
            'var_hotelname' => $data['hotel'],
            'var_createddate' => date('Y-m-d'),
        );
        $this->db->insert('dz_hotel',$data_array);
        return TRUE;
    }
    function hoteldelete($data){
        $this->db->where('int_glcode',$data['id']);
        $this->db->delete('dz_hotel');
        return TRUE;
    }
    function hotelupdate($data){
        $data_array = array(
            'var_hotelname' => $data['hotel'],
            'var_updatedate' => date('Y-m-d'),
        );
        $this->db->where('int_glcode',$data['id']);
        $this->db->update('dz_hotel',$data_array);
        return TRUE;
    }
}
?>
