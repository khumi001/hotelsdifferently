<?php
 class proipban extends CI_Model{
     
     function getipban()
	 {
        $this->db->where('chr_status','B');
        $result = $this->db->get('dz_ban_ip_address')->result_array();
        return $result;
     }
     function status_change($data)
	 {
		$CI = & get_instance(); 
		date_default_timezone_set('America/Los_Angeles');
		$CI->load->model('admin/admin_log_model');
		$row = $this->db->where('int_glcode',$data['id'])->get('dz_ban_ip_address')->result();
		if(count($row) > 0)
		{
			$row = $row[0];
			if(empty($row->var_ipban))
			{
				$da = array(
					"login_status" => 'Active'
				);
				$this->db->where('int_glcode',$row->fk_user);
				$this->db->update('dz_user' , $da);
			}
			$this->db->where('int_glcode',$data['id']);
			$this->db->delete('dz_ban_ip_address');
			$log = array(
				'log_type' => 'lift_ip_ban',
				'log_type_fk_id' => $row->fk_user,
				'log_desc' => 'lifted ban for '.$row->var_ipban
			);
			$CI->admin_log_model->addlog($log);
			return TRUE; 
		}
		else
		{
			return FALSE;
		}
     }
 }
?>