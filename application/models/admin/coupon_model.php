<?php

class Coupon_model extends CI_Model {

    function coupon_generate($data)
    {
        $CI = & get_instance();
        $CI->load->model('admin/admin_log_model');

        date_default_timezone_set(AMERICA_LOS_ANGLES_TIME_ZONE);
        if($data['expdate'] == "")
        {
            $expire = date('Y-m-d', strtotime('+1 years'));
        }
        else
        {
            $expire =$data['expdate'];
        }
        $minamount = $data['minamount'];
        if ($data['status'] == 'all')
        {
            $this->db->where('chr_user_type', 'U');
            $query = $this->db->get('dz_user')->result_array();
            if ($data['coupanoff1'] == '')
            {
                $coupanvalue = $data['coupanoff2'];
                for ($i = 0; $i < count($query); $i++)
                {
                    $couponcode = $this->couponcode();
                    $data_array = array(
                        'fk_user' => $query[$i]['int_glcode'],
                        'var_couponcode' => $couponcode . $i,
                        'var_couponvalue' => $coupanvalue,
                        'min_amount' => $minamount,
                        'var_coupan_type' => '2',
                        'var_created_date' => date('Y-m-d h:i'),
                        'var_updated_date'=>date('Y-m-d h:i:s'),
                        'var_couponexpi'=>$expire,
                    );
                    $this->db->insert('dz_coupons', $data_array);
                    //edit by tayyab for acativity log
                    $insert_id = $this->db->insert_id();
                    if(empty($minamount))
                    {
                        $minamount = '0.00';
                    }
                    $log = array(
                        'log_type' => 'new_coupen_genrated',
                        'log_type_fk_id' => $insert_id,
                        'log_desc' => 'A new coupen is genrated'
                    );
                    $CI->admin_log_model->addlog($log);
                    $this->load->helper('tourico_api');
                    $user1 = getUserById($query[$i]['int_glcode']);
                    $this->load->library('email');
                    $this->email->set_mailtype("html");
                    $this->email->from(NOREPLY,  SITE_NAME);
                    $this->email->to($user1->var_email);
                    $this->email->subject('Coupon');
                    $mail_body = '<p style="color:#fff;margin-bottom:10px;">Dear '.$user1->var_fname . '</p>';
                    $mail_body .='<p style="color:#fff;margin-bottom:10px;">Thank you for using <b>'.SITE_NAME.'<sup>sm</sup></b>.</p>';
                    $mail_body .='<p style="color:#fff;margin-bottom:10px;">To thank you for your loyalty and continued support, we would like to extend our gratitude in the form of this coupon. This coupon will give you <b>$'.$coupanvalue.'</b> with a minimum spending of <b>$'.$minamount.'</b> and expires <b>'.date(DISPLAY_DATE_FORMAT, strtotime($expire)).'</b>.</p>';
                    $mail_body .='<p style="color:#fff;margin-bottom:10px;">Thank you for choosing us and please always give us a try before booking your next trip so that you can be sure that you made the best booking decision!</p>';
                    $mail_body .= '<p style="color:#fff;margin-bottom:10px; font-style:italic;">Sincerely,</br>';
                    $mail_body .= '<p style="color:#fff;margin-bottom:10px;"><b>'.SITE_NAME.'<sup>sm</sup></b><br>
						<small style="font-style: italic;">Making hotels happen for you!</small><b><sup>sm</sup></b>
						</p>';
                    $data['mail_body'] = $mail_body;
                    $message = $this->load->view('admin/account/genral_emailtemplate', $data, TRUE);
                    $this->email->message($message);
                    $this->email->send();
                }
            }
            else
            {
                $coupanvalue = $data['coupanoff1'];
                for ($i = 0; $i < count($query); $i++)
                {
                    $couponcode = $this->couponcode();
                    $data_array = array(
                        'fk_user' => $query[$i]['int_glcode'],
                        'var_couponcode' => $couponcode . $i,
                        'var_couponvalue' => $coupanvalue,
                        'min_amount' => $minamount,
                        'var_coupan_type' => '1',
                        'var_created_date' => date('Y-m-d h:i'),
                        'var_couponexpi'=>$expire,
                    );
                    $this->db->insert('dz_coupons', $data_array);
                    //edit by tayyab for acativity log
                    $insert_id = $this->db->insert_id();
                    if(empty($minamount))
                    {
                        $minamount = '0.00';
                    }
                    $log = array(
                        'log_type' => 'new_coupen_genrated',
                        'log_type_fk_id' => $insert_id,
                        'log_desc' => 'A new coupen is genrated'
                    );
                    $CI->admin_log_model->addlog($log);
                    $this->load->helper('tourico_api');
                    $user1 = getUserById($query[$i]['int_glcode']);
                    $this->load->library('email');
                    $this->email->set_mailtype("html");
                    $this->email->from(NOREPLY, SITE_NAME);
                    $this->email->to($user1->var_email);
                    $this->email->subject('Coupon');
                    $mail_body = '<p style="color:#fff;margin-bottom:10px;">Dear '.$user1->var_fname . '</p>';
                    $mail_body .='<p style="color:#fff;margin-bottom:10px;">Thank you for using <b>'.SITE_NAME.'<sup>sm</sup></b>.</p>';
                    $mail_body .='<p style="color:#fff;margin-bottom:10px;">To thank you for your loyalty and continued support, we would like to extend our gratitude in the form of this coupon. This coupon will give you <b>'.$coupanvalue.'%</b> with a minimum spending of <b>$'.$minamount.'</b> and expires <b>'.date(DISPLAY_DATE_FORMAT, strtotime($expire)).'</b>.</p>';
                    $mail_body .='<p style="color:#fff;margin-bottom:10px;">Thank you for choosing us and please always give us a try before booking your next trip so that you can be sure that you made the best booking decision!</p>';
                    $mail_body .= '<p style="color:#fff;margin-bottom:10px; font-style:italic;">Sincerely,</br>';
                    $mail_body .= '<p style="color:#fff;margin-bottom:10px;"><b>'.SITE_NAME.'<sup>sm</sup></b><br>
						<small style="font-style: italic;">Making hotels happen for you!</small><b><sup>sm</sup></b>
						</p>';
                    $data['mail_body'] = $mail_body;
                    $message = $this->load->view('admin/account/genral_emailtemplate', $data, TRUE);
                    $this->email->message($message);
                    $this->email->send();
                }
            }
        }
        else
        {
            $arraydata = $data['status'];
            if( !is_array($data['status']) ){
                $arraydata = explode(',',$data['status']);
            }

            //print_r($arraydata);die('here');

            if(($key = array_search('on', $arraydata)) !== false)
            {
                unset($arraydata[$key]);
            }
            $this->db->where_in('int_glcode',$arraydata);
            $query = $this->db->get('dz_user')->result_array();
            if($data['expdatesing'] == "")
            {
                $expire1 = date('Y-m-d', strtotime('+1 years'));
            }
            else
            {
                $expire1 =$data['expdatesing'];
            }
            if ($data['coupanoff3'] == '')
            {
                $coupanvalue = $data['coupanoff4'];
                for ($i = 0; $i < count($query); $i++)
                {
                    $couponcode = $this->couponcode();
                    $data_array = array(
                        'fk_user' => $query[$i]['int_glcode'],
                        'var_couponcode' => $couponcode . $i,
                        'var_couponvalue' => $coupanvalue,
                        'min_amount' => $minamount,
                        'var_coupan_type' => '2',
                        'var_created_date' => date('Y-m-d h:i'),
                        'var_couponexpi'=>$expire1,
                    );
                    $this->db->insert('dz_coupons', $data_array);
                    //edit by tayyab for acativity log
                    $insert_id = $this->db->insert_id();
                    if(empty($minamount))
                    {
                        $minamount = '0.00';
                    }
                    $log = array(
                        'log_type' => 'new_coupen_genrated',
                        'log_type_fk_id' => $insert_id,
                        'log_desc' => 'A new coupen is genrated'
                    );
                    $CI->admin_log_model->addlog($log);
                    $this->load->helper('tourico_api');
                    $user1 = getUserById($query[$i]['int_glcode']);
                    $this->load->library('email');
                    $this->email->set_mailtype("html");
                    $this->email->from(NOREPLY, SITE_NAME);
                    $this->email->to($user1->var_email);
                    $this->email->subject('Coupon');
                    $mail_body = '<p style="color:#fff;margin-bottom:10px;">Dear '.$user1->var_fname . '</p>';
                    $mail_body .='<p style="color:#fff;margin-bottom:10px;">Thank you for using <b>'.SITE_NAME.'<sup>sm</sup></b>.</p>';
                    $mail_body .='<p style="color:#fff;margin-bottom:10px;">To thank you for your loyalty and continued support, we would like to extend our gratitude in the form of this coupon. This coupon will give you <b>$'.$coupanvalue.'</b> with a minimum spending of <b>$'.$minamount.'</b> and expires <b>'.date(DISPLAY_DATE_FORMAT, strtotime($expire1)).'</b>.</p>';
                    $mail_body .='<p style="color:#fff;margin-bottom:10px;">Thank you for choosing us and please always give us a try before booking your next trip so that you can be sure that you made the best booking decision!</p>';
                    $mail_body .= '<p style="color:#fff;margin-bottom:10px; font-style:italic;">Sincerely,</br>';
                    $mail_body .= '<p style="color:#fff;margin-bottom:10px;"><b>'.SITE_NAME.'<sup>sm</sup></b><br>
						<small style="font-style: italic;">Making hotels happen for you!</small><b><sup>sm</sup></b>
						</p>';
                    $data['mail_body'] = $mail_body;
                    $message = $this->load->view('admin/account/genral_emailtemplate', $data, TRUE);
                    $this->email->message($message);
                    $this->email->send();
                }
            }
            else
            {
                for ($i = 0; $i < count($query); $i++)
                {
                    $coupanvalue = $data['coupanoff3'];
                    $couponcode = $this->couponcode();
                    $data_array = array(
                        'fk_user' => $query[$i]['int_glcode'],
                        'var_couponcode' => $couponcode . $i,
                        'var_couponvalue' => $coupanvalue,
                        'min_amount' => $minamount,
                        'var_coupan_type' => '1',
                        'var_created_date' => date('Y-m-d h:i'),
                        'var_couponexpi'=> $expire1,
                    );
                    $this->db->insert('dz_coupons', $data_array);

                    //edit by tayyab for acativity log
                    $insert_id = $this->db->insert_id();
                    $log = array(
                        'log_type' => 'new_coupen_genrated',
                        'log_type_fk_id' => $insert_id,
                        'log_desc' => 'A new coupen is genrated'
                    );
                    $CI->admin_log_model->addlog($log);
                    if(empty($minamount))
                    {
                        $minamount = '0.00';
                    }
                    $this->load->helper('tourico_api');
                    $user1 = getUserById($query[$i]['int_glcode']);
                    $this->load->library('email');
                    $this->email->set_mailtype("html");
                    $this->email->from(NOREPLY, SITE_NAME);
                    $this->email->to($user1->var_email);
                    $this->email->subject('Coupon');
                    $mail_body = '<p style="color:#fff;margin-bottom:10px;">Dear '.$user1->var_fname . '</p>';
                    $mail_body .='<p style="color:#fff;margin-bottom:10px;">Thank you for using <b>'.SITE_NAME.'<sup>sm</sup></b>.</p>';
                    $mail_body .='<p style="color:#fff;margin-bottom:10px;">To thank you for your loyalty and continued support, we would like to extend our gratitude in the form of this coupon. This coupon will give you <b>'.$coupanvalue.'%</b> with a minimum spending of <b>$'.$minamount.'</b> and expires <b>'.date(DISPLAY_DATE_FORMAT, strtotime($expire1)).'</b>.</p>';
                    $mail_body .='<p style="color:#fff;margin-bottom:10px;">Thank you for choosing us and please always give us a try before booking your next trip so that you can be sure that you made the best booking decision!</p>';
                    $mail_body .= '<p style="color:#fff;margin-bottom:10px; font-style:italic;">Sincerely,</br>';
                    $mail_body .= '<p style="color:#fff;margin-bottom:10px;"><b>'.SITE_NAME.'<sup>sm</sup></b><br>
						<small style="font-style: italic;">Making hotels happen for you!</small><b><sup>sm</sup></b>
						</p>';
                    $data['mail_body'] = $mail_body;
                    $message = $this->load->view('admin/account/genral_emailtemplate', $data, TRUE);
                    $this->email->message($message);
                    $this->email->send();
                }
            }
        }
        return TRUE;
    }


    function coupon_generate_user_end($data)
    {
        date_default_timezone_set(AMERICA_LOS_ANGLES_TIME_ZONE);
        $expire = date('Y-m-d', strtotime('+1 years'));
        $user_id = $this->session->userdata['valid_user']['id'];
        $coupanvalue = $data['coupanvalue'];
        $couponcode = $this->couponcode();
        $i = 0;
        $minamount = 0;
        $data_array = array(
            'fk_user' => $user_id,
            'var_couponcode' => $couponcode . $i,
            'var_couponvalue' => $coupanvalue,
            'min_amount' => $minamount,
            'var_coupan_type' => '2',
            'var_created_date' => date('Y-m-d h:i'),
            'var_couponexpi'=>$expire,
        );
        $this->db->insert('dz_coupons', $data_array);
        $this->load->helper('tourico_api');
        $user1 = getUserById($user_id);
        $this->load->library('email');
        $this->email->set_mailtype("html");
        $this->email->from(NOREPLY, SITE_NAME);
        $this->email->to($user1->var_email);
        $this->email->subject('Coupon');
        $mail_body = '<p style="color:#fff;margin-bottom:10px;">Dear '.$user1->var_fname . '</p>';
        $mail_body .='<p style="color:#fff;margin-bottom:10px;">Thank you for using <b>'.SITE_NAME.'<sup>sm</sup></b>.</p>';
        $mail_body .='<p style="color:#fff;margin-bottom:10px;">To thank you for your loyalty and continued support, we would like to extend our gratitude in the form of this coupon. This coupon will give you <b>$'.$coupanvalue.'</b> with a minimum spending of <b>$'.$minamount.'</b> and expires <b>'.date(DISPLAY_DATE_FORMAT, strtotime($expire)).'</b>.</p>';
        $mail_body .='<p style="color:#fff;margin-bottom:10px;">Thank you for choosing us and please always give us a try before booking your next trip so that you can be sure that you made the best booking decision!</p>';
        $mail_body .= '<p style="color:#fff;margin-bottom:10px; font-style:italic;">Sincerely,</br>';
        $mail_body .= '<p style="color:#fff;margin-bottom:10px;"><b>'.SITE_NAME.'<sup>sm</sup></b><br>
			<small style="font-style: italic;">Making hotels happen for you!</small><b><sup>sm</sup></b>
			</p>';
        $data['mail_body'] = $mail_body;
        $message = $this->load->view('admin/account/genral_emailtemplate', $data, TRUE);
        $this->email->message($message);
        $this->email->send();
        return TRUE;
    }

    function status_update($data)
    {
        date_default_timezone_set(AMERICA_LOS_ANGLES_TIME_ZONE);
        $query = $this->db->get_where('dz_coupons', array('int_glcode' => $data['id']))->result_array();
        $CI = & get_instance();
        $CI->load->model('admin/admin_log_model');
        if ($query[0]['var_status'] == 'A')
        {
            $data_array = array(
                'var_status' => 'AD',
            );
            $this->db->where('int_glcode', $data['id']);
            $this->db->update('dz_coupons', $data_array);
            $log = array(
                'log_type' => 'deactivate_coupon',
                'log_type_fk_id' => $data['id'],
                'log_desc' => $query[0]['var_couponcode'].' Coupon is deactivated'
            );
            $CI->admin_log_model->addlog($log);
        }
        else
        {
            $data_array = array(
                'var_status' => 'A',
            );
            $this->db->where('int_glcode', $data['id']);
            $this->db->update('dz_coupons', $data_array);
            $log = array(
                'log_type' => 'activate_coupon',
                'log_type_fk_id' => $data['id'],
                'log_desc' => $query[0]['var_couponcode'].' Coupon is activated'
            );
            $CI->admin_log_model->addlog($log);
        }
        return True;
    }

    function couponcode($numno = 2, $numAlpha = 4, $numno = 3) {
        $listAlpha = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $listno = '0123456789';
        return str_shuffle(
            substr(str_shuffle($listno), 0, $numno) .
            substr(str_shuffle($listAlpha), 0, $numAlpha) .
            substr(str_shuffle($listno), 0, $numno)
        );
    }

    function status_singup($data) {

        date_default_timezone_set(AMERICA_LOS_ANGLES_TIME_ZONE);
//        $this->db->select('chr_status');
//        $this->db->where('int_glcode', 1);
//        $query = $this->db->get('dz_singup_coupons')->result_array();
//        if ($query[0]['chr_status'] == '1') {
//            $data_array = array(
//                'chr_status' => '0',
//                'dt_update_date' => date("Y-m-d H:i:s"),
//            );
//        } else {
        $data_array = array(
            'chr_status' => $data['id'],
            'dt_update_date' => date("Y-m-d H:i:s"),
        );
//        }
        $this->db->where('int_glcode', 1);
        $this->db->update('dz_singup_coupons', $data_array);
        return TRUE;
    }
    function generatedata($data)
    {
        date_default_timezone_set(AMERICA_LOS_ANGLES_TIME_ZONE);
        $data_array = array(
            'var_couponvalue' => $data['couponper'],
            'var_couponexpi' => $data('Y-m-d')
        );
        $this->db->insert('dz_coupons', $data_array);
    }
}
?>
