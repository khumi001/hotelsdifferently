<?php

class ipban extends CI_Model {

    function addipban($data) {
        date_default_timezone_set('America/Los_Angeles');

        $this->db->select('int_glcode');
        $this->db->or_where('var_username', $data['ban_by_acc']);
        $this->db->or_where('var_email', $data['ban_by_acc']);
        $this->db->or_where('var_accountid', $data['ban_by_acc']);

        $res = $this->db->get('dz_user')->result_array();
        // print_r($res);
        $id = $res[0]['int_glcode'];
		$user1 = getUserById($this->session->userdata['valid_adminuser']['id']);
		$user2 = getUserById($id);

        $login_count = array(
            'login_status' => 'Inactive',
        );
        $this->db->where('int_glcode', $id);
        $this->db->update('dz_user', $login_count);
        
         if($data['ban_time'] == '1'){
                $today = date("Y-m-d H:i:s");
               $end = date('Y-m-d H:i:s', strtotime($today. ' + 1 day')); 
            }else if($data['ban_time'] == '7'){
               $today = date("Y-m-d H:i:s");
               $end = date('Y-m-d H:i:s', strtotime($today. ' + 7 day')); 
            }else if($data['ban_time'] == '30'){
                 $today = date("Y-m-d H:i:s");
               $end = date('Y-m-d H:i:s', strtotime($today. ' + 30 day'));
            }else{
                 $today = date("Y-m-d H:i:s");
                $end = date('Y-m-d H:i:s', strtotime($today. ' + 5 years'));
            }
        $ipban = array(
            'var_ipban' => $data['ban_by_ip'],
            'var_banaue' => $data['ban_by_acc'],
            'var_reason' => $data['reason'],
            'date_banstart_time'=>date("Y-m-d H:i:s"),
            'date_banend_time'=>$end,
            'var_createddate' => date('Y-m-d H:i:s'),
            'var_updateddate' => date('Y-m-d H:i:s'),
            'fk_user' => $id,
            'var_banby' => $user1->var_email,
            'chr_status' => 'B',
        );
        $this->db->insert('dz_ban_ip_address', $ipban);
        //edit by tayyab for acativity log
        $insert_id = $this->db->insert_id();
        $CI = & get_instance();
        $CI->load->model('admin/admin_log_model');
        $log = array(
			'log_type' => 'ip_banned',
			'log_type_fk_id' => $insert_id,
			'log_desc' => 'Account '.$user2->var_email.' BAN by '.$user1->var_email
		);
        $CI->admin_log_model->addlog($log);
        return TRUE;
    }

}

?>
