<?php

class Confirm extends CI_Controller {

    function __construct() {
        parent::__construct();
        date_default_timezone_set('America/Los_Angeles');
    }

    function email($confirm_email,$last_id) 
	{
        $confirm_id= base64_decode($last_id);
        $email = base64_decode($confirm_email);
		$data['page'] = "front/home/activation";
		
        date_default_timezone_set('America/Los_Angeles');
       
        $this->db->where('var_email',$email);
        $query = $this->db->get('dz_user')->result_array();
		if(count($query) == 0)
		{	
			$data['message'] =  "Sorry! It looks like that this account has been activated already. If you do not know your login information please try the “Forgot password” feature at the login window.";
		}
		else
		{
			$id = $query[0]['int_glcode'];
			$this->db->where('fk_user',$id);
			$this->db->where('int_glcode',$confirm_id);
			$query1 = $this->db->where('chr_status !=' , 'E')->get('dz_confirm_email')->result_array();
			if(count($query1) > 0)
			{
				if($query1['chr_status'] == 'U')
				{
					$data['message'] =  "Sorry! It looks like that this account has been activated already. If you do not know your login information please try the “Forgot password” feature at the login window.";
				}
				else
				{
					$result = $query1[0]['dt_created_date'];
					$TotalHours = 0;
					$today = date('Y-m-d H:i:s');
					$start = new DateTime($result);
					$End = new DateTime($today);
					$interval = $start->diff($End);
					$TotalHours = $interval->h;
					if ($TotalHours < 48) 
					{
						if (!empty($query)) 
						{
							$content = array(
								'var_email' => $query1[0]['var_email'],
							);
							$this->db->where('int_glcode', $id);
							$this->db->update('dz_user', $content);
							
							$dcont = array(
								"chr_status" => 'U'
							);
							$this->db->where('int_glcode', $query1[0]['int_glcode']);
							$this->db->update('dz_confirm_email', $dcont);
							$data['message'] =  "Thank you! Your account is now active and ready to use.";
						} 
						else 
						{
							redirect('');
						}
					} 
					else 
					{
						$content = array(
							'chr_status' => 'E',
						);
						$this->db->where('int_glcode',$confirm_id);
						$this->db->update('dz_confirm_email', $content);
						$data['message'] =  "Sorry This Link Expired Please try Again";
					}
				}
			}
			else
			{
				$data['message'] =  "Sorry! Wrong activation link. Please check if you typed the activation code properly.";
			}
		}
        
        $this->load->view(FRONT_LAYOUT, $data);
    }

}
?>
