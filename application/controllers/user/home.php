<?php

class Home extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
		redirect('front/home');
        $data['page'] = "user/home/home";
        $this->page_name = 'Home';
        $data['js'] = array(
            'login.js',
            'moment.min.js',
            'front/home.js',
            'front/jquery.bxslider.js',
            'front/jquery.slimscroll.min.js',
            'front/jquery.slimscroll.js',
            'affiliate/req_quote.js',
            'custom/components-pickers.js',
            'custom/components-dropdowns.js',
            'front/jquery.bxslider.js'
        );
        $data['js_plugin'] = array(
            'jquery.bxslider.min.js',
            'bootstrap-fileinput/bootstrap-fileinput.js',
            'checkincheckout/bootstrap-datepicker.js',
            'bootstrap-select/bootstrap-select.min.js',
            'select2/select2.min.js',
            'jquery.bxslider.min.js'
        );
        $data['css_plugin'] = array(
            'jquery.bxslider.css',
            'bootstrap-fileinput/bootstrap-fileinput.css',
            'bootstrap-datepicker/css/datepicker.css',
            'bootstrap-select/bootstrap-select.min.css',
            'select2/select2.css',
            'select2/select2-metronic.css',
            'jquery-ui/jquery-ui.css',
            'jquery.bxslider.css'
        );
        $data['mobailjs'] = array(
//            'public/jquerymobail/css/themes/default/jquery.mobile-1.4.5.min.css',
//            'public/jquerymobail/_assets/css/jqm-demos.css',
//            'public/jquerymobail/js/jquery.js',
//            'public/jquerymobail/_assets/js/index.js',
//            'public/jquerymobail/js/jquery.mobile-1.4.5.min.js',
        );
        $data['css'] = array(
        );
       
        if ($this->input->post()) {
            $res = $this->quote_model->add_req_quote($this->input->post());
            if ($res) {
                echo "success";
                exit;
            } else {
                echo "error";
                exit;
            }
        }
        $data['init'] = array(
            'Login.init()',
            'Home.init()',
            'Req_quote.init()',
            'Req_quote.comments_placeholder()',
            'ComponentsPickers.init_datepicker()',
        );
        $this->db->where('fk_user', $this->userid);
        $this->db->where('var_status', 'A');
        $data['coupons'] = $this->db->get('dz_coupons')->result_array();
        
//        $this->db->select('dz_city.int_glcode,dz_city.var_city,dz_state.var_state,dz_country.var_country');
//        $this->db->from('dz_city');
//        $this->db->join('dz_state', 'dz_city.fk_state = dz_state.int_glcode', 'left');
//        $this->db->join('dz_country', 'dz_city.fk_country = dz_country.int_glcode', 'left');
//        $data['city'] = $this->db->get()->result_array();
//        
//        $this->db->select('var_hotelname,dz_city.var_city,dz_state.var_state,dz_country.var_country');
//        $this->db->from('dz_hotel');
//        $this->db->join('dz_city', 'dz_hotel.fk_city = dz_city.int_glcode', 'left');
//        $this->db->join('dz_state', 'dz_hotel.fk_state = dz_state.int_glcode', 'left');
//        $this->db->join('dz_country', 'dz_hotel.fk_country = dz_country.int_glcode', 'left');
//        $data['hotels'] = $this->db->get()->result_array();
        
        $this->db->select('*');
        $this->db->where('int_glcode', '1');
        $this->db->from('dz_maincounter');
        $data['maincounter'] = $this->db->get()->result_array();
        
        $this->db->select('var_fname,var_lname');
        $this->db->where('int_glcode', $this->session->userdata['valid_user']['id']);
        $this->db->from('dz_user');
        $data['userdetail'] = $this->db->get()->result_array();

        $this->load->view(USER_LAYOUT, $data);
    }

    function get_hotel() {
        $post_array = $_POST['id'];
        $city = explode(',', $post_array);
        $city = $city['0'];
        $city_id = $this->db->select('int_glcode')->get_where('dz_city', array('var_city' => $city))->row('int_glcode');
        $query = $this->db->get_where('dz_hotel', array('fk_city' => $city_id))->result_array();
        echo json_encode($query);
        exit();
    }
    
    function get_city_detail(){
        
        if($this->input->post('value')){
            $postvalue = explode(" ",$this->input->post('value'));
            if(count($postvalue) == 1)
            {
                 $this->db->select('int_glcode,var_city,var_state,var_country');
                $this->db->or_like('var_city',$this->input->post('value'));
                $this->db->or_like('var_state',$this->input->post('value'));
                $this->db->or_like('var_country',$this->input->post('value'));
                $this->db->distinct('var_city');
    //            $this->db->limit(15);
                $ListofHotels['city'] = $this->db->get('dz_hotel_list')->result_array();


                $this->db->select('int_glcode,var_hotel_name,var_city,var_state,var_country');
                $this->db->or_like('var_hotel_name',$this->input->post('value'));
                $this->db->or_like('var_city',$this->input->post('value'));
                $this->db->or_like('var_state',$this->input->post('value'));
                $this->db->or_like('var_country',$this->input->post('value'));
    //            $this->db->limit(15);
                $ListofHotels['hotel'] = $this->db->get('dz_hotel_list')->result_array();
            }
           else
           {
               $ListofHotels['city'] = array();
               $ListofHotels['hotel'] = $this->searchSencondArgument($postvalue);
           }
//            print_r($ListofHotels['hotel']); exit();
//            if(count($postvalue) == 2){
//                $this->searchSencondArgument($ListofHotels,$postvalue);
//            }
            
            echo json_encode($ListofHotels);
        }
    }
    
    function searchSencondArgument($postvalue){
        if(count($postvalue == 2))
        {
            $finalarray = array();
            $get1 = array();
            $intglcode = array();
            for($i =0;$i< count($postvalue);$i++)
            {
                $this->db->select('int_glcode');
                $this->db->or_like('var_hotel_name',$postvalue[0]);
                   $this->db->or_like('var_city',$postvalue[0]);
                   $this->db->or_like('var_state',$postvalue[0]);
                   $this->db->or_like('var_country',$postvalue[0]);
//                   $this->db->where('int_glcode',$Hotels['hotel'][$j]['int_glcode']);  
                   $get[] = $this->db->get('dz_hotel_list')->result_array();
            }
          //  print_r($get);
            for($j = 0;$j< count($get[0]);$j++)
            {
                $intglcode[] = $get[0][$j]['int_glcode'];
            }
            $str = implode(',', $intglcode);
           
            $finaldataarray = $this->db->query('SELECT * FROM (`dz_hotel_list`) WHERE `int_glcode` IN ('.$str.') AND  (`var_hotel_name`  LIKE  "%'.$postvalue[1].'%" OR  `var_city`  LIKE  "%'.$postvalue[1].'%" OR  `var_state`  LIKE  "%'.$postvalue[1].'%" OR  `var_country`  LIKE "%'.$postvalue[1].'%")');
         
           $finaldata =  $finaldataarray->result_array();
           return $finaldata;
        }
        if(count($postvalue == 3))
        {
            $finalarray = array();
            $finaldata1 = array();
            $get1 = array();
            $intglcode = array();
            $intglcode1 = array();
            for($i =0;$i< count($postvalue);$i++)
            {
                $this->db->select('int_glcode');
                $this->db->or_like('var_hotel_name',$postvalue[0]);
                   $this->db->or_like('var_city',$postvalue[0]);
                   $this->db->or_like('var_state',$postvalue[0]);
                   $this->db->or_like('var_country',$postvalue[0]);
//                   $this->db->where('int_glcode',$Hotels['hotel'][$j]['int_glcode']);  
                   $get[] = $this->db->get('dz_hotel_list')->result_array();
            }
          //  print_r($get);
            for($j = 0;$j< count($get[0]);$j++)
            {
                $intglcode[] = $get[0][$j]['int_glcode'];
            }
            $str = implode(',', $intglcode);
           
            $finaldataarray = $this->db->query('SELECT `int_glcode` FROM (`dz_hotel_list`) WHERE `int_glcode` IN ('.$str.') AND  (`var_hotel_name`  LIKE  "%'.$postvalue[1].'%" OR  `var_city`  LIKE  "%'.$postvalue[1].'%" OR  `var_state`  LIKE  "%'.$postvalue[1].'%" OR  `var_country`  LIKE "%'.$postvalue[1].'%")');
            $finaldata =  $finaldataarray->result_array();
            
            for($k = 0;$k< count($finaldata);$k++)
            {
                $intglcode1[] = $finaldata[$k]['int_glcode'];
            }
            $str1 = implode(',', $intglcode1);
            
             $finaldataarray1 = $this->db->query('SELECT * FROM (`dz_hotel_list`) WHERE `int_glcode` IN ('.$str1.') AND  (`var_hotel_name`  LIKE  "%'.$postvalue[2].'%" OR  `var_city`  LIKE  "%'.$postvalue[2].'%" OR  `var_state`  LIKE  "%'.$postvalue[2].'%" OR  `var_country`  LIKE "%'.$postvalue[2].'%")');
            $finaldata1 =  $finaldataarray1->result_array();
            
           return $finaldata1;
        }
        

    }
        

    function contains($str, array $arr)
    {
        foreach($arr as $a) {
            if (stripos($str,$a) !== false) return true;
        }
        return false;
    }



    function logout() {
        $this->session->unset_userdata('valid_front_user');
        redirect('admin/login');
    }

    function noactivity() {
        $this->session->sess_destroy();
        redirect(base_url());
    }

    function testmail() {
        $this->load->config('mandrill');

        $this->load->library('mandrill');
        $mandrill_ready = NULL;

        try {

            $this->mandrill->init($this->CI->config->item('Rbx4qqVB5HM5aga70h6UhA'));
            $mandrill_ready = TRUE;
        } catch (Mandrill_Exception $e) {

            $mandrill_ready = FALSE;
        }

        if ($mandrill_ready) {
//            echo 'jj';
            //Send us some email!
            $email = array(
                'html' => '<p>This is my message<p>', //Consider using a view file
                'text' => 'This is my plaintext message',
                'subject' => 'This is my subject',
                'from_email' => 'testmpatel@gmail.com',
                'from_name' => 'Me-Oh-My',
                'to' => array(array('email' => 'kartikdesai123@gmail.com')) //Check documentation for more details on this one
                    //'to' => array(array('email' => 'joe@example.com' ),array('email' => 'joe2@example.com' )) //for multiple emails
            );

            $result = $this->mandrill->messages_send($email);
            echo $result;
        }
    }

}
?>