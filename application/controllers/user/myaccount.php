<?php

class Myaccount extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('Datatables');
        $this->load->helper('stripe_lib/init');
        date_default_timezone_set(AMERICA_LOS_ANGLES_TIME_ZONE);
        $this->userid =  $this->session->userdata['valid_user']['id'];
    }

    function timeTest(){
        echo date('l, F d, Y h:i:s A');exit;
        $row = $this->db->insert('dz_user', array('customerId'=>date('l, F d, Y h:i:s A')));exit;
    }

    function index() {

    }

    function info() {
        if (!is_user_login()) {
            redirect("/");
        }
        regularUserAndSubEntityCheck();
        $data['page'] = "user/myaccount/info";
        $this->page_name = 'My Info';
        $this->parent_menu = 'myaccount';
        $data['js'] = array(
            'custom/components-pickers.js',
            'user/my_account.js'
        );
        $data['js_plugin'] = array(
            'jquery-validation/dist/jquery.validate.min.js',
            'jquery-validation/dist/additional-methods.min.js',
            'bootstrap-fileinput/bootstrap-fileinput.js',
            'bootstrap-datepicker/js/bootstrap-datepicker.js',
            'bootstrap-select/bootstrap-select.min.js',
            'select2/select2.min.js',
            'bootstrap-maxlength/bootstrap-maxlength.min.js'
        );
        $data['css_plugin'] = array(
            'bootstrap-fileinput/bootstrap-fileinput.css',
            'bootstrap-datepicker/css/datepicker.css',
            'bootstrap-select/bootstrap-select.min.css',
            'select2/select2.css',
            'select2/select2-metronic.css',
        );
        if($this->session->userdata['valid_user']['var_usertype'] == 'U' || $this->session->userdata['valid_user']['var_usertype'] == 'EN')
            $data['edit_data'] = $this->db->select("int_glcode,var_fname,var_lname,var_accountid,entity_activate_time,var_phone,dt_created_date,endDate,isLifeTime,autoRenewal,subscription")->join('users_memberships', 'users_memberships.userId = dz_user.int_glcode')->where('int_glcode', $this->userid)->get('dz_user')->row_array();
        else
            $data['edit_data'] = $this->db->select("int_glcode,var_fname,var_lname,entity_activate_time,var_accountid,var_phone,dt_created_date")->where('int_glcode', $this->userid)->get('dz_user')->row_array();

        $data['init'] = array(
            'My_account.init()',
            'ComponentsPickers.init_datepicker()'
        );
        //  echo $this->session->userdata['valid_user']['id'];
        //  exit();


        if ($_POST)
        {
            if(isset($_POST['autoRenewal']))
                $auto = 1;
            else
                $auto = 0;

            $arr = array(
                //'var_fname' => $this->input->post('fname'),
                //'var_lname' => $this->input->post('lname'),
                'autoRenewal'=>$auto,
                'var_phone' => $this->input->post('phone1') . '-' . $this->input->post('phone2') . '-' . $this->input->post('phone3'),
                //'var_country' => $this->input->post('country'),
            );

            // check if there is change in selection then perform action for stripe
            if($data['edit_data']['autoRenewal'] != $auto){
                if($auto == 0) {
                    $res = $this->_removeSubscription($data['edit_data']['subscription']);
                }
                else {
                    $res = $this->reactivatingCanceledSubs($data['edit_data']['subscription']);
                }

                if(!$res['success']){
                    echo 'error';exit;
                }
            }

            $this->db->where('int_glcode', $this->userid);
            $query = $this->db->update('dz_user', $arr);

            if ($query) {
                echo "success";
                exit;
            } else {
                echo "error";
                exit;
            }
        }

        $this->load->view(USER_LAYOUT, $data);
    }

    function coupons() {
        if (!is_user_login()) {

            redirect("/");

        }
        regularUserAndSubEntityCheck();
        $data['page'] = "user/myaccount/coupons";
        $this->page_name = 'My Coupons';
        $this->parent_menu = 'myaccount';
        $data['js'] = array(
            'user/coupons.js',
            'custom/components-pickers.js',
        );
        $data['js_plugin'] = array(
            'bootstrap-datepicker/js/bootstrap-datepicker.js',
        );
        $data['css_plugin'] = array(
            'uniform/css/uniform.default.css',
            'bootstrap-datepicker/css/datepicker.css'
        );
        $data['css'] = array(
        );
        $data['init'] = array(
            'Coupons.init()',
            'ComponentsPickers.init_datepicker()'
        );
        $this->load->view(USER_LAYOUT, $data);
    }

    function refer_friend() {
        if (!is_user_login()) {

            redirect("/");

        }
        regularUserAndSubEntityCheck();
//         $config = Array(
//            'protocol' => 'smtp',
//            'smtp_host' => 'mail.hotelsdifferently.com',
//            'smtp_port' => 465,
//            'smtp_user' => NOREPLY,
//            'smtp_pass' => 'Jelszo456',
//            'mailtype' => 'html',
//            'charset' => 'iso-8859-1'
//        );
//        $this->load->library('email');
//        $this->email->initialize($config);
//        $this->email->set_newline("\r\n");
//
//
//            $this->email->set_mailtype("html");
//            $this->email->from(NOREPLY, 'Referfriend');
//            $this->email->reply_to($this->session->userdata['valid_user']['var_email'], 'Referfriend');
//            $this->email->to('kartikdesai123@gmail.com');
//            $this->email->subject('BIG SAVINGS AND $20 SIGNUP COUPON FOR YOU!');
//
//            $mail_body .= "<p style='color:#fff;margin-bottom:10px;'>Dear ,</p>";
//            $data['mail_body'] = $mail_body;
//            $message = $this->load->view('admin/account/genral_emailtemplate', $data, TRUE);
//            $this->email->message($message);
//            $mail1 = $this->email->send();
//             echo $this->email->print_debugger();
//            exit();
        $data['page'] = "user/myaccount/refer_friend";
        $this->page_name = 'REFER A FRIEND';
        $this->parent_menu = 'myaccount';
        $data['js'] = array(
            'user/refer_friend.js',
            'custom/components-pickers.js',
        );
        $data['contactimpoter'] = 'yes';
        $data['js_plugin'] = array(
            'bootstrap-datepicker/js/bootstrap-datepicker.js',
        );
        $data['css_plugin'] = array(
            'bootstrap-datepicker/css/datepicker.css'
        );
        $data['css'] = array(
        );
        $data['init'] = array(
            'Refer_friend.init()',
            'ComponentsPickers.init_datepicker()'
        );
        $data['refer_link'] = $this->db->select('var_accountid')->where('int_glcode', $this->userid)->get('dz_user')->row_array();
        $refer_link = $this->db->select('var_accountid')->where('int_glcode', $this->userid)->get('dz_user')->result_array();
        $url1 = base_url();
        //$data['title'] = "Try this to get the CHEAPEST HOTEL DEALS! Get $20 OFF for your booking as a bonus: ".base_url()."front/home/member_signup/" . $refer_link[0]['var_accountid'];
        $data['title'] = urlencode("Check out this site and get wholesale hotel rates. I included my coupon so you can get a membership for 50% off! :".base_url()."front/home/member_signup/" . $refer_link[0]['var_accountid']);
        //  $data['desc'] = "Get a $20 OFF coupon for your booking as a sign up bonus if you use my link: ".$url1."front/home/member_signup/".$refer_link[0]['var_accountid'];

        if ($this->input->post()) {

            //print_r($post); exit;
            $emails = $this->input->post('referemail');
            $receivername = explode('@', $emails);

            $id = $this->session->userdata['valid_user']['id'];
            $this->db->where('int_glcode', $id);
            $query = $this->db->get('dz_user');
            $userdetail = $query->result_array();

            $accountid = $refer_link[0]['var_accountid'];
            $url = base_url() . 'front/home/member_signup/' . $accountid;
//            $config = Array(
//            'protocol' => 'smtp',
//            'smtp_host' => 'hotelsdifferentlycom.ipage.com',
//            'smtp_port' => 587,
//            'smtp_user' => NOREPLY,
//            'smtp_pass' => 'Jelszo456',
//            'mailtype' => 'html',
//            'charset' => 'iso-8859-1'
//        );
            $this->load->library('email');
//        $this->email->initialize($config);
//        $this->email->set_newline("\r\n");


            $this->email->set_mailtype("html");
            $this->email->from(NOREPLY, 'Referfriend');
            $this->email->reply_to($this->session->userdata['valid_user']['var_email'], 'Referfriend');
            $this->email->to($emails);
            $this->email->subject('BIG SAVINGS AND $20 SIGNUP COUPON FOR YOU!');

            $mail_body = "<p style='color:#fff;margin-bottom:10px;'>Dear " . $receivername[0] . ",</p>";
            $mail_body .= "<p style='color:#fff;margin-bottom:10px;'>I just found this website where you can get the BEST HOTEL DEALS! You can get $20 OFF on your first booking as a signup bonus: <a style='color:#fff;' href=" . $url . ">" . $url . "</a></p>";
//            $mail_body = '<p style="color:#fff;margin-bottom:10px;">Hello Your referral link is:<a href=' . $url . '>' . $url . '</a></p>';
            $mail_body .='<p style="color:#fff;margin-bottom:10px;">' . $userdetail[0]['var_fname'] . '</p>';
            $data['mail_body'] = $mail_body;
            $message = $this->load->view('admin/account/genral_emailtemplate', $data, TRUE);
            $this->email->message($message);
            $mail1 = $this->email->send();

            if ($mail1) {
                echo "success";
                exit;
            } else {
                echo "error";
                exit;
            }
        }

        $this->load->view(USER_LAYOUT, $data);
    }

    function coupons_datatable($status) {

        // for update coupan status
        $content = array(
            'var_status' => 'E',
        );
        $this->db->where('var_status', 'A');
        $this->db->where('var_couponexpi <', date('Y-m-d'));
        $row = $this->db->update('dz_coupons', $content);
        // for update coupan status
        if ($status != '') {
            $where = $this->datatables->where('var_status', $status);
        } else {
            $where = '';
        }
        $this->datatables
            ->select('
                         dz_coupons.var_created_date,
                         dz_coupons.var_couponcode,
                         dz_coupons.var_couponvalue,
                         dz_coupons.var_couponexpi,
                         dz_coupons.var_status,
                         dz_coupons.var_coupan_type
                        ')
            ->from('dz_coupons')
            ->where_in('var_status', array('A', 'D', 'E', 'U'))
            ->where('dz_coupons.fk_user', $this->userid);
        $where;
        $res = $this->datatables->generate();
        $results = json_decode($res, true);
//         print_r($results);
        //  exit();
        $count = count($results['aaData']);
        for ($i = 0; $i < $count; $i++) {
            if ($results['aaData'][$i][4] == 'A') {
                $results['aaData'][$i][4] = "Active";
            } else if ($results['aaData'][$i][4] == 'E') {
                $results['aaData'][$i][4] = "Expired";
            } else if ($results['aaData'][$i][4] == 'U') {
                $results['aaData'][$i][4] = "Used";
            } else {
                $results['aaData'][$i][4] = "Deactive";
            }

            if ($results['aaData'][$i][5] == 1) {
                $results['aaData'][$i][2] = $results['aaData'][$i][2] . "%";
            } else {
                $results['aaData'][$i][2] = "$" . $results['aaData'][$i][2];
            }

            $results['aaData'][$i][0] = date(DISPLAY_DATE_FORMAT, strtotime($results['aaData'][$i][0]));
            $results['aaData'][$i][3] = date(DISPLAY_DATE_FORMAT, strtotime($results['aaData'][$i][3]));
        }
        echo json_encode($results);
    }

    function refer_friend_datatable() {
        if ($_POST['from'] && $_POST['to']) {
            if ($_POST['from'] == $_POST['to']) {
                $this->datatables->like('dz_quote.dt_created_date', date('Y-m-d', strtotime($_POST['from'])), 'both');
            } else {
                $this->datatables->where('dz_quote.dt_created_date >=', date('Y-m-d', strtotime($_POST['from'])));
                $this->datatables->where('dz_quote.dt_created_date <=', date('Y-m-d', strtotime($_POST['to'])));
            }
        }
        $this->datatables
            ->select('
                         dz_user.var_accountid,
                         dz_quote.dt_created_date,
                         dz_commision.var_paid_amount,
                         dz_commision.var_commision_amount,
                         dz_quote.var_comments,
                         dz_commision.var_releasedate,
                         dz_rplyquote.chr_status
                        ')
            ->from('dz_commision')
            ->join('dz_user', 'dz_commision.fk_user = dz_user.int_glcode')
            ->join('dz_quote', 'dz_commision.fk_quote = dz_quote.int_glcode')
            ->join('dz_rplyquote', 'dz_commision.fk_replayquote = dz_rplyquote.int_glcode')
            // ->where('dz_rplyquote.chr_status !=', 'C')
            ->where('dz_user.int_glcode', $this->session->userdata['valid_user']['id']);

        $res = $this->datatables->generate();

        $results = json_decode($res, true);
        $count = count($results['aaData']);
        for ($i = 0; $i < $count; $i++) {
            $accountid = $results['aaData'][$i][0];
            $bookingdate = $results['aaData'][$i][1];
            $amount = $results['aaData'][$i][2];
            $commission = $results['aaData'][$i][3];
            $note = $results['aaData'][$i][4];
            $relesedate = $results['aaData'][$i][5];
            //  $status = $results['aaData'][$i][6];
            if($results['aaData'][$i][6] == "C")
            {
                $note = "Cancelled Resarvation!<span class='linethrow' style='display:none;'>".$commission."</span><span class='linethrow1' style='display:none;'>".$amount."</span>";
            }
            $results['aaData'][$i][0] = $accountid;
            $results['aaData'][$i][1] = date("Y-m-d / H:i", strtotime($bookingdate));
            $results['aaData'][$i][2] = "$" . (number_format($amount, 2));
            $results['aaData'][$i][3] = "$" . (number_format($commission, 2));
            $results['aaData'][$i][4] = $note;
            $results['aaData'][$i][5] = date("Y-m-d", strtotime($relesedate));
        }
        echo json_encode($results);
    }

    function change_pass() {
        if (!is_user_login()) {

            redirect("/");

        }
        regularUserAndSubEntityCheck();
        $data['page'] = "user/myaccount/change_pass";
        $this->page_name = 'Change Password';
        $this->parent_menu = 'myaccount';
        $data['js'] = array(
            'user/my_account.js'
        );
        $data['js_plugin'] = array(
            'jquery-validation/dist/jquery.validate.min.js',
            'jquery-validation/dist/additional-methods.min.js',
            'bootstrap-wizard/jquery.bootstrap.wizard.min.js',
            'select2/select2.min.js',
            'bootstrap-datepicker/js/bootstrap-datepicker.js',
            'bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',
            'bootstrap-maxlength/bootstrap-maxlength.min.js'
        );
        $data['css_plugin'] = array(
            'select2/select2-metronic.css',
            'select2/select2.css',
        );
        $data['init'] = array(
            'My_account.init()',
        );
        if ($this->input->post()) {
            $oldpass = $this->input->post('old_pass');
            $id = $this->session->userdata['valid_user']['id'];
            $this->db->where('int_glcode', $id);
            $this->db->where('var_password', base64_encode($oldpass));
            $query = $this->db->get('dz_user');

            if ($query->num_rows() == 1)
            {
                $content = array(
                    'var_password' => base64_encode($this->input->post('new_pass')),
                );
                $this->db->where('int_glcode', $id);
                $row = $this->db->update('dz_user', $content);
                if ($row)
                {
                    $this->db->where('int_glcode', $id);
                    $query2 = $this->db->get('dz_user');
                    $name = $query2->result_array();
                    $this->load->library('email');
                    $this->email->set_mailtype("html");
                    $this->email->from(NOREPLY, 'Wholesale Hotels Group');
                    $this->email->to($name[0]['var_email']);
                    $this->email->subject('Change Password');

                    $mail_body = '<p style="color:#fff;margin-bottom:10px;">Dear  ' . $name[0]['var_fname'] . '</p>';
                    $mail_body .='<p style="color:#fff;margin-bottom:10px;">Thank you for using <b>Wholesale Hotels Group<sup>sm</sup></b>. We are glad to confirm that your password is now successfully changed! Your new password is <b>'.$this->input->post('new_pass').'</b></p>';
                    $mail_body .='<p style="color:#fff;margin-bottom:10px;">Again, thank you for choosing us and please always give us a try before booking your next trip!</p>';
                    $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Sincerely,</br>';
                    $mail_body .= '<p style="color:#fff;margin-bottom:10px;"><b>Wholesale Hotels Group<sup>sm</sup></b><br>
					<small style="font-style: italic;">Making hotels happen for you!</small><b><sup>sm</sup></b>
					</p>';
                    $data['mail_body'] = $mail_body;
                    $message = $this->load->view('admin/account/genral_emailtemplate', $data, TRUE);
                    $this->email->message($message);
                    $res = $this->email->send();
                    if ($res)
                    {
                        echo "success";
                    }
                    else
                    {
                        echo "error";
                    }
                }
                else
                {
                    echo "error";
                }
            }
            else
            {
                echo "error";
            }
            exit;
        }
        $this->load->view(USER_LAYOUT, $data);
    }

    function change_email() {
        if (!is_user_login()) {

            redirect("/");

        }
        regularUserAndSubEntityCheck();
        $data['page'] = "user/myaccount/change_email";
        $this->page_name = 'Change Email Address';
        $this->parent_menu = 'myaccount';
        $data['js'] = array(
            'user/my_account.js'
        );
        $data['js_plugin'] = array(
        );
        $data['css_plugin'] = array(
        );
        $data['css'] = array(
        );
        $data['init'] = array(
            'My_account.init()',
        );
        if ($this->input->post())
        {
            $curremail = $this->input->post('email');
            $id = $this->session->userdata['valid_user']['id'];

            $this->db->where('int_glcode', $id);
            $this->db->where('var_email', $curremail);
            $query = $this->db->get('dz_user')->num_rows();

            $this->db->where('var_email', $this->input->post('new_email'));
            $query1 = $this->db->get('dz_user')->num_rows();

            if ($query == 1 && $query1 == 0)
            {
                $confirm = array(
                    'fk_user' => $id,
                    'var_email' => $this->input->post('confirm_email'),
                    'chr_status' => 'A',
                    'dt_created_date' => date('Y-m-d H:i:s'),
                    'dt_updated_date' => date('Y-m-d H:i:s'),
                );
                $row = $this->db->insert('dz_confirm_email', $confirm);
                $last_id = $this->db->insert_id();

                $id = base64_encode($last_id);
                $confirm_email = base64_encode($this->input->post('email'));

                $link = '<a style="color:#fff;" href="'.base_url() . 'user/confirm/email/' . $confirm_email . '/' . $id.'">'.base_url() . 'user/confirm/email/' . $confirm_email . '/' . $id.'</a>';
                //$this->db->where('int_glcode', $id);
                $this->db->where('var_email', $curremail);
                $query2 = $this->db->get('dz_user');
                $name = $query2->result_array();
                $this->load->library('email');
                ////////////To new mail
                $this->email->set_mailtype("html");
                $this->email->from(NOREPLY, 'Admin');
                $this->email->to($this->input->post('confirm_email'));
                $this->email->subject('Change Email Confirmation');

                $mail_body = '<p style="color:#fff;margin-bottom:10px;">Dear '.$name[0]['var_fname'] . '</p>';
                $mail_body .='<p style="color:#fff;margin-bottom:10px;">Thank you for using <b>Wholesale Hotels Group<sup>sm</sup></b>. We are glad to confirm that your email address is now changed! For your security, we sent out a confirmation letter to both email addresses.</p>';
                $mail_body .='<p style="color:#fff;margin-bottom:10px;">You only have one last job to do! Please be kind and confirm your new email address by clicking <b><u><a style="color:#fff" href="'.base_url() . 'user/confirm/email/' . $confirm_email . '/' . $id.'">HERE!</a></u></b></p>';
                $mail_body .='<p style="color:#fff;margin-bottom:10px;">Alternatively, you can also activate your new email address by copying and pasting the following URL link: <b><u>"' . $link . '"</b></u></p>';
                $mail_body .='<p style="color:#fff;margin-bottom:10px;">Again, thank you for choosing us and please always give us a try before booking your next trip!</p>';
                $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Sincerely,</br>';
                $mail_body .= '<p style="color:#fff;margin-bottom:10px;"><b>Wholesale Hotels Group<sup>sm</sup></b><br>
					<small style="font-style: italic;">Making hotels happen for you!</small><b><sup>sm</sup></b>
					</p>';

                $data['mail_body'] = $mail_body;
                $message = $this->load->view('admin/account/genral_emailtemplate', $data, TRUE);
                $this->email->message($message);
                $this->email->send();
                /////////// To previous mail
                $this->email->set_mailtype("html");
                $this->email->from(NOREPLY, 'Admin');
                $this->email->to($curremail);
                $this->email->subject('Change Email Confirmation');

                $mail_body = '<p style="color:#fff;margin-bottom:10px;">Dear '.$name[0]['var_fname'] . '</p>';
                $mail_body .='<p style="color:#fff;margin-bottom:10px;">Thank you for using <b>Wholesale Hotels Group<sup>sm</sup></b>. We are glad to confirm that your email address is now changed! For your security, we sent out a confirmation letter to both email addresses.</p>';
                $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Sincerely,</br>';
                $mail_body .= '<p style="color:#fff;margin-bottom:10px;"><b>Wholesale Hotels Group<sup>sm</sup></b><br>
					<small style="font-style: italic;">Making hotels happen for you!</small><b><sup>sm</sup></b>
					</p>';
                $data['mail_body'] = $mail_body;
                $message = $this->load->view('admin/account/genral_emailtemplate', $data, TRUE);
                $this->email->message($message);
                $this->email->send();



                if ($row)
                {
                    echo "success";
                    return $confirm_email;
                }
                else
                {
                    echo "error";
                }
            }
            else
            {
                echo "exits";
            }
            exit();
        }
        $id = $this->session->userdata['valid_user']['id'];
        $this->db->where('int_glcode', $id);
        $data['user'] = $this->db->get('dz_user')->result();
        //var_email
        $this->load->view(USER_LAYOUT, $data);
    }

    function oauth()
    {
        $data['contactimpoter'] = 'yes';
        $this->load->view('user/myaccount/oauth', $data);
    }

    function sss()
    {
        $data['page'] = "user/myaccount/userpaysss";
        $this->page_name = 'Payment Page';
        $this->parent_menu = 'reservation';
        $data['js'] = array(
            'user/userpay.js',
            'custom/components-pickers.js',
        );
        $data['js_plugin'] = array(
            'bootstrap-datepicker/js/bootstrap-datepicker.js',
        );
        $data['css_plugin'] = array(
            'bootstrap-datepicker/css/datepicker.css'
        );
        $data['css'] = array(
        );
        $data['init'] = array(
            'Userpay.init()',
            'ComponentsPickers.init_datepicker()'
        );

        $this->db->select('dz_quote.*,
            dz_rplyquote.var_site1,
            dz_rplyquote.int_site1_price,
            dz_rplyquote.var_site2,
            dz_rplyquote.int_site2_price,
            dz_rplyquote.var_tripadvisor,
            dz_rplyquote.var_uniq_quoteid,
            dz_rplyquote.var_policy,
            dz_rplyquote.var_comment,
            dz_rplyquote.var_star,
            dz_rplyquote.var_NOH,
            dz_rplyquote.var_prize'
        );
        $this->db->from('dz_quote');
        $this->db->join('dz_rplyquote', 'dz_quote.int_glcode=dz_rplyquote.int_fkquote');
        $this->db->where_in('dz_rplyquote.int_glcode', $replayquoteid);
        $query = $this->db->get()->result_array();
        //print_r($query);
        // echo json_encode($query); exit();
        $data['datacontent'] = $query;
        $this->db->where('fk_user', $this->userid);
        $this->db->where('var_status', 'A');
        $data['coupons'] = $this->db->get('dz_coupons')->result_array();

        $data['replayquoteid'] = $replayquoteid;
        $data['refer_link'] = $this->db->select('var_accountid')->where('int_glcode', $this->userid)->get('dz_user')->row_array();
        $this->load->view(USER_LAYOUT, $data);
    }

    function enrolling($policyAccept = 0){
        /*if (!regular_login_check()) {
            redirect("/");
        }*/
        regularUserCheck();

        if($policyAccept /*|| $this->session->userdata("enrollingDone")*/){
            $this->session->set_userdata("enrollingDone",true);
            redirect("user/myaccount/membership");
        }

        // this page should only be accessible if user has not subscribed yet.
        $this->db->where(array('is_subscribed'=>0,'int_glcode'=>$this->session->userdata['valid_user']['id']));
        $user = $this->db->get('dz_user')->num_rows();
        if($user > 0) {
            $data['page'] = "user/myaccount/enrolling";
            $this->load->view(FRONT_LAYOUT, $data);
        }else{
            redirect("/");
        }
    }

    function expired(){
        regularUserCheck();
        $this->db->select('int_glcode,var_fname,var_lname,var_email,is_subscribed');
        $this->db->where(array('int_glcode'=>$this->session->userdata['valid_user']['id']));
        $user = $this->db->get('dz_user')->row_array();
        if($user['is_subscribed'] == 3) {
            $this->session->set_userdata("enrollingDone",true);
            $data['page'] = "user/myaccount/expired";
            $data['user'] = $user;
            $this->load->view(FRONT_LAYOUT, $data);
        }else{
            redirect("/");
        }
    }

    function entity_expired(){
        if (!regular_login_check()) {
            redirect("/");
        }

        // check is user has any membership plan?
        $this->db->select('int_glcode,var_fname,var_lname,var_email');
        $this->db->where(array('int_glcode'=>$this->session->userdata['valid_user']['id']));
        $user = $this->db->get('dz_user')->row_array();

        $data['page'] = "user/myaccount/entity_expired";
        $data['user'] = $user;
        $this->load->view(FRONT_LAYOUT, $data);
    }

    function unverified(){
        regularUserCheck();
        $this->db->where(array('is_subscribed'=>4,'int_glcode'=>$this->session->userdata['valid_user']['id']));
        $user = $this->db->get('dz_user')->num_rows();
        if($user == 0) {
            redirect("/");
        }
        $data['page'] = "user/myaccount/unverified";
        $this->load->view(FRONT_LAYOUT, $data);
    }


    function membership(){
        regularUserCheck();
        // allow only if user has come from enrolling page.
        if(!$this->session->userdata("enrollingDone")){
            redirect("/user/myaccount/enrolling");
        }
        $res = $this->db->where('int_glcode' , $this->session->userdata['valid_user']['id'])->get("dz_user")->row();
        // get amount
        if ($this->input->post('mPaymentPost')){
            // get user detail
            $this->db->where('type', $this->input->post('membership'));
            $detail = $this->db->get('membership_plans')->row_array();
            if(count($detail)>0) {
                $this->session->set_userdata("membershipDone",true);
                $this->session->set_userdata("membership", array('paymentMethod'=>$this->input->post('paymentMethod'),'stripe_plan_id'=>$detail['stripe_plan_id'],'days'=>$detail['days'],'amountPerMonth'=>$detail['amountPerMonth'],'amount' => $detail['totalAmount'],'actualPrice' => $detail['totalAmount'], 'plan' => $detail['title']));
                redirect("user/myaccount/payment");
            }else{
                $_SESSION['error'] = "Invalid membership type selected";
            }
        }
        $data['status'] = $res->is_subscribed;
        $data['cardNumber'] = $res->cardNumber;
        $data['page'] = "user/myaccount/membership";
        $this->load->view(FRONT_LAYOUT, $data);
    }

    function thankYou(){
        regularUserCheck();
        // allow only if user has come from payment page.
        if(!$this->session->userdata("paymentDone")){
            redirect("/user/myaccount/payment");
        }
        if($this->session->userdata['membership']['paymentMethod'] == 'oldCard' && $this->session->userdata['userDetail']['is_subscribed'] == 3 && $this->session->userdata['userDetail']['customerId'] != ''){
            $data['page'] = "user/myaccount/renew_thank_you";
        }else {
            $data['page'] = "user/myaccount/thank_you";
        }
        $this->load->view(FRONT_LAYOUT, $data);
    }

    function verification(){
        regularUserCheck();

        // this page should only be accessible if user subs status is verification.
        $this->db->where(array('is_subscribed'=>2,'int_glcode'=>$this->session->userdata['valid_user']['id']));
        $user = $this->db->get('dz_user')->num_rows();
        if($user == 0) {
            redirect("/");
        }

        // remove all checks so that user cannot access that pages after payment
        if($this->session->userdata('paymentDone')) {
            $this->session->unset_userdata('paymentDone');
            $this->session->unset_userdata('membershipDone');
            $this->session->unset_userdata('enrollingDone');
        }
        $data['js'] = array(
            'custom/jquery.maskMoney.js'
        );
        if ($this->input->post()){
            $this->db->select('int_glcode,var_fname,var_lname,var_email');
            $this->db->where(array('int_glcode'=>$this->session->userdata['valid_user']['id']));
            $user = $this->db->get('dz_user')->row_array();

            $amount = $this->input->post('amount');
            $this->db->select('id,days,paid,subscription');
            $this->db->where('refundedAmount', $amount)->where("status",-1)->where("refundedAmount !=",0)->where("userId",$this->session->userdata['valid_user']['id']);
            $detail = $this->db->get('users_memberships')->row_array();
            if(count($detail)>0){
                // success
                $endDate = date("Y-m-d H:i:s",strtotime("+".$detail['days']." days"));
                // unix timestamp for stripe
                $datetime = new DateTime($endDate);
                $endTrial = $datetime->getTimestamp();
                $sRes = $this->retrieveAndUpdateSubs($detail['subscription'],$endTrial);
                if($sRes['success']) {
                    $this->db->where(array('id' => $detail['id']));
                    $this->db->update('users_memberships', array(
                        'startDate' => date("Y-m-d H:i:s"),
                        'endDate' => $endDate,
                        'status' => '1'
                    ));

                    $this->db->where(array('int_glcode' => $this->session->userdata['valid_user']['id']));
                    $this->db->set('is_subscribed', '1', FALSE);
                    $this->db->set('membership_attempt', '1', FALSE);
                    $this->db->set('entity_activate_time', date('Y-m-d H:i:s'));
                    $this->db->update('dz_user');
                    redirect("/");
                }else{
                    $_SESSION['error'] = $sRes['details'];
                }
                // send email
                /*$this->load->library('email');
                $this->email->set_mailtype("html");
                $this->email->from(NOREPLY, 'Wholesale Hotels Group');
                $this->email->to($user['var_email']);
                $this->email->subject('Enrollment finalization');
                $data['amount'] = $detail['paid'];
                $data['user'] = $user;
                $message = $this->load->view('user/myaccount/verified_email', $data, TRUE);
                $this->email->message($message);
                $this->email->send();*/
            }else{
                // error
                $this->db->where(array('userId'=>$this->session->userdata['valid_user']['id'],"status"=>-1));
                $this->db->set('remainingAttempts', 'remainingAttempts-1', FALSE);
                $this->db->update('users_memberships');
                // get latest
                $this->db->where("status",-1)->where("userId",$this->session->userdata['valid_user']['id']);
                $detail = $this->db->get('users_memberships')->row_array();
                if($detail['remainingAttempts'] <= 0){
                    $this->db->where(array('id'=>$detail['id']));
                    $this->db->update('users_memberships',array(
                        'status'=> '2'
                    ));

                    $this->db->where(array('int_glcode'=>$this->session->userdata['valid_user']['id']));
                    $this->db->set('is_subscribed', '4', FALSE);
                    $this->db->update('dz_user');
                    // send email
                    /*$this->load->library('email');
                    $this->email->set_mailtype("html");
                    $this->email->from(NOREPLY, 'Wholesale Hotels Group');
                    $this->email->to($user['var_email']);
                    $this->email->subject('Unsuccessful verification');
                    $data['user'] = $user;
                    $message = $this->load->view('user/myaccount/unverified_email', $data, TRUE);
                    $this->email->message($message);
                    $this->email->send();*/
                    redirect("user/myaccount/unverified");
                }
                $_SESSION['error'] = "Your provided refund amount is invalid. You have ".$detail['remainingAttempts']." attempt(s) left.";
            }
        }
        $data['page'] = "user/myaccount/verification";
        $this->load->view(FRONT_LAYOUT, $data);
    }

    function payment(){
        regularUserCheck();
        // allow only if user has come from membership page.
        if(!$this->session->userdata("membershipDone")){
            redirect("/user/myaccount/membership");
        }
        //if(!isset($this->session->userdata['userDetail'])) {
        // get user detail
        $this->db->select('int_glcode,var_email,var_fname,var_lname,cardNumber,is_subscribed,customerId,membership_attempt,autoRenewal');
        $this->db->where('var_email', $this->session->userdata['valid_user']['var_email']);
        $detail = $this->db->get('dz_user')->row_array();
        $this->session->set_userdata("userDetail", $detail);
        //}

        if ($this->input->post()){
            echo 1;
            var_dump($this->input->post('three_d_secure'));exit;
        }
        if($this->input->get('source_token_3d')){
            $stripeSource = $this->input->get('source_token_3d');
            $last_four = $this->input->get('card');
            //$description = ' Account Email : '.(isset($this->session->userdata['valid_user']['var_email']) ? $this->session->userdata['valid_user']['var_email'] : '');
            $description = $this->session->userdata['userDetail']['var_fname']." ".$this->session->userdata['userDetail']['var_lname']." (".$this->session->userdata['userDetail']['var_email'].") buy membership plan (".$this->session->userdata['membership']['plan'].") for $".$this->session->userdata['membership']['amount'];
            //echo '<pre>';print_r($chargeData);echo '</pre>';exit;
            // Charge the user's card:
            $chargeData = array(
                "amount" => $this->session->userdata['membership']['amount']*100,
                "currency" => "usd",
                "description" => $description,
                "customer" => $this->session->userdata("customerSource"),
                'source'=>$stripeSource,
                "receipt_email" => $this->session->userdata['userDetail']['var_email']
            );

            $payResponse = $this->stripePayment($chargeData);
            if ($payResponse['success'] == 1) {

                // add subscription
                $subs = $this->_addSubscription(array(
                    "customer" => $this->session->userdata("customerSource"),
                    "items" => array(
                        array(
                            "plan" => $this->session->userdata['membership']['stripe_plan_id'],
                        ),
                    ),
                    "trial_period_days" => $this->session->userdata['membership']['days'],
                    "coupon" => $this->session->userdata['membership']['coupon']
                ));
                if ($subs['success'] == 1) {
                    // remove the old user from stripe if user has come after expired subscription
                    if($this->session->userdata['userDetail']['is_subscribed'] == 3 && $this->session->userdata['userDetail']['customerId'] != ''){
                        $this->removeCustomer($this->session->userdata['userDetail']['customerId']);
                        // remove database subscription
                        $this->db->where('userId', $this->session->userdata['userDetail']['int_glcode']);
                        $this->db->delete('users_memberships');

                        // if user auto renewal option is set to OFF then update the subsc
                        // and set it to auto renewal OFF at stripe
                        if($this->session->userdata['userDetail']['autoRenewal'] == 0) {
                            $res = $this->_removeSubscription($subs['subs']->id);
                        }
                    }

                    //error_reporting(E_ALL);
                    //ini_set('display_errors', 1);
                    $charge = $payResponse['charge_info']->id;
                    $stripePaymetarray = serialize($payResponse['charge_info']->__toArray(true));
                    $rec = array(
                        'userId' => $this->session->userdata['userDetail']['int_glcode'],
                        'startDate' => date("Y-m-d H:i:s"),
                        'endDate' => date("Y-m-d H:i:s", strtotime("+" . $this->session->userdata['membership']['days'] . " days")),
                        'plan' => $this->session->userdata['membership']['plan'],
                        'amountPerMonth' => $this->session->userdata['membership']['amountPerMonth'],
                        'stripeInfo' => $stripePaymetarray,
                        'paid' => $this->session->userdata['membership']['amount'],
                        'description' => $description,
                        'status' => -1, // not verified yet,
                        'days' => $this->session->userdata['membership']['days'],
                        'subscription' => $subs['subs']->id
                    );
                    $userUpdateData = array('cardNumber'=>$last_four,'customerId'=>$this->session->userdata("customerSource"),'is_subscribed' => 2);
                    if($this->session->userdata['membership']['paymentMethod'] == 'oldCard' && $this->session->userdata['userDetail']['is_subscribed'] == 3 && $this->session->userdata['userDetail']['customerId'] != ''){
                        // extension email
                        $this->load->library('email');
                        $this->email->set_mailtype("html");
                        $this->email->from(NOREPLY, 'Wholesale Hotels Group');
                        $this->email->to($this->session->userdata['userDetail']['var_email']);
                        $this->email->subject("Membership's Extension");
                        $data['data'] = $this->session->userdata['userDetail'];
                        $data['endDate'] = date(DISPLAY_DATE_FORMAT, strtotime("+" . $this->session->userdata['membership']['days'] . " days"));
                        $data['paid'] = $this->session->userdata['membership']['amount'];
                        $message = $this->load->view('user/myaccount/extension', $data, TRUE);
                        $this->email->message($message);
                        $this->email->send();
                        // user has used old card for renew so keep his account active.. no need for verification
                        $rec['status'] = 1;
                        $userUpdateData['is_subscribed'] = 1;
                        $userUpdateData['membership_attempt'] = 1;
                        $userUpdateData['entity_activate_time'] = date("Y-m-d H:i:s");
                    }else{
                        $this->load->library('email');
                        $this->email->set_mailtype("html");
                        $this->email->from(NOREPLY, 'Wholesale Hotels Group');
                        $this->email->to($this->session->userdata['userDetail']['var_email']);
                        $this->email->subject('Enrollment finalization');
                        $data['amount'] = $this->session->userdata['membership']['amount'];
                        $data['user'] = $this->session->userdata['userDetail'];
                        $message = $this->load->view('user/myaccount/verified_email', $data, TRUE);
                        $this->email->message($message);
                        $this->email->send();
                    }
//echo '<pre>';print_r($rec);
                    //                  echo '<pre>';print_r($rec);exit;
                    // pause for 20 seconds so that webhook can triggered
                    sleep(10);
                    $this->db->where('int_glcode', $this->session->userdata['userDetail']['int_glcode']);
                    $query = $this->db->update('dz_user', $userUpdateData);
                    $row = $this->db->insert('users_memberships', $rec);
//                    $this->session->unset_userdata('membership');
                    $this->session->set_userdata("paymentDone",true);
                    redirect("user/myaccount/thankYou");
                }else{
                    $_SESSION['error'] = $subs['details'];
                }
            }else{
                $_SESSION['error'] = $payResponse['details'];
            }
        }

        $data['countries'] = get_all_contries();
        $data['states'] = get_cities_id(192);
        $data['userDetail'] = $this->session->userdata['userDetail'];
        $data['page'] = "user/myaccount/payment";
        $this->load->view(FRONT_LAYOUT, $data);
    }

    /*function getDBEvents(){
        $this->db->select('*');
        $this->db->where('type', "invoice.payment_succeeded");
        $this->db->where('id', "27");
        $detail = $this->db->get('events')->result_array();
        foreach ($detail as $val) {
            $data = json_decode(unserialize($val['data']));
            var_dump(date('Y-m-d H:i:s',$data->data->object->lines->data[0]->period->end));
            var_dump($val['created']);
            echo '<pre>';print_r($data);

        }
        exit;

    }*/

    function webhook(){
        \Stripe\Stripe::setApiKey(STRIPE_SECRET_KEY);

        // Retrieve the request's body and parse it as JSON
        $input = @file_get_contents("php://input");
        $event_json = json_decode($input);
        $this->db->insert('events', array('type'=>$event_json->type,'data'=>serialize($input)));
        //echo '<pre>';print_r($event_json);exit;
        $customer_id = $event_json->data->object->customer;
        $period_end = $event_json->data->object->lines->data[0]->period->end;
        $amount_due = $event_json->data->object->amount_due;
        $this->db->select('int_glcode,customerId,var_email,var_fname,var_lname,membership_attempt');
        $this->db->where('customerId', $customer_id);
        $detail = $this->db->get('dz_user')->row_array();
        if(count($detail)>0) {
            // send this email when stripe attempt for renewal not the first time when user subscribed. first time
            // we send enrollment finalize email.
            // default value is 0, but when user verify account then we set it to 1.
            if($detail['membership_attempt'] > 0) {
                if ($event_json->type == "invoice.payment_succeeded") {
                    $amount_due = $amount_due / 100;
                    $this->db->where(array('userId'=>$detail['int_glcode']));
                    $this->db->update('users_memberships',array(
                        'endDate'=> date('Y-m-d H:i:s',$period_end),
                        'paid'=>$amount_due
                    ));
                    //$row = $this->db->insert('users_memberships', array('description'=>serialize(json_encode($event_json))));
                    // send email
                    $this->load->library('email');
                    $this->email->set_mailtype("html");
                    $this->email->from(NOREPLY, 'Wholesale Hotels Group');
                    $this->email->to($detail['var_email']);
                    $this->email->subject('Membership Extension');
                    $data['data'] = $detail;
                    $data['endDate'] = date(DISPLAY_DATE_FORMAT,$period_end);
                    $data['paid'] = $amount_due;
                    $message = $this->load->view('user/myaccount/extension', $data, TRUE);
                    $this->email->message($message);
                    $this->email->send();
                }
            }

            if ($event_json->type == "invoice.payment_failed" || $event_json->type == "customer.subscription.deleted") {
                // update status
                $this->db->where(array('userId'=>$detail['int_glcode']));
                $this->db->update('users_memberships',array(
                    'status'=> '0'
                ));

                $this->db->where(array('int_glcode'=>$detail['int_glcode']));
                $this->db->set('is_subscribed', '3', FALSE);
                $this->db->set('membership_attempt', '0', FALSE);
                $this->db->update('dz_user');

                if($event_json->type == "invoice.payment_failed"){
                    $this->db->select('userId,subscription');
                    $this->db->where('userId', $detail['int_glcode']);
                    $subs = $this->db->get('users_memberships')->row_array();
                    if(count($subs)>0) {
                        $this->_removeSubscriptionPermanently($subs['subscription']);
                    }
                }

                if($event_json->type == "customer.subscription.deleted") {
                    // we do not send email on payemnt fail. as on payment fail we manually remove subscription
                    // and on removal "customer.subscription.deleted" event will trigger, so in this way no double emails will be sent
                    // only one email will be sent
                    // send email
                    $this->load->library('email');
                    $this->email->set_mailtype("html");
                    $this->email->from(NOREPLY, 'Wholesale Hotels Group');
                    $this->email->to($detail['var_email']);
                    $this->email->subject('Account expired');
                    $data['data'] = $detail;
                    $message = $this->load->view('user/myaccount/expired_email', $data, TRUE);
                    $this->email->message($message);
                    $this->email->send();
                }
            }

            /*if ($event_json->type == "customer.subscription.deleted") {
                // send email
                $this->load->library('email');
                $this->email->set_mailtype("html");
                $this->email->from(NOREPLY, 'Wholesale Hotels Group');
                $this->email->to($detail['var_email']);
                $this->email->subject('Account expired');
                $this->email->message("customer.subscription.deleted");
                $this->email->send();
            }*/

        }
        http_response_code(200);
    }

    function stripecallback()
    {
        //echo '<pre>';print_r($_GET);exit;
        redirect('user/myaccount/payment?source_token_3d=' . $_GET['source']."&card=".$_GET['card']);

    }

    function createCustomer(){
        if($this->input->post('cardSource')) {
            $cRes = $this->_createCustomer(array(
                "email" => $this->input->post('email'),
                "source" => $this->input->post('cardSource'),
            ));
            if($cRes['success']){
                $this->session->set_userdata("customerSource",$cRes['customer']->id);
                echo json_encode(array('status'=>1,'id'=>$cRes['customer']->id,'amount'=>$this->session->userdata['membership']['amount']));
            }else{
                echo json_encode(array('status'=>0));
            }
        }else{
            echo json_encode(array('status'=>0));
        }
        exit;
    }

    function retrieveCoupon(){
        if($this->input->post('code')) {
            $cRes = $this->_retrieveCoupon($this->input->post('code'));
            if($cRes['success']){
                //echo '<pre>';print_r($cRes);exit;
                if(!empty($cRes['coupon']->amount_off)){
                    // subtract amount
                    $newAmount = number_format(($this->session->userdata['membership']['actualPrice'] - $cRes['coupon']->amount_off),2);
                }else{
                    $newAmount = number_format(($this->session->userdata['membership']['actualPrice'] - ($this->session->userdata['membership']['actualPrice'] / 100) *($cRes['coupon']->percent_off)),2);
                }
                $oldVal = $this->session->userdata['membership'];
                $oldVal['amount'] = $newAmount;
                $oldVal['coupon'] = $cRes['coupon']->id;
                $this->session->set_userdata("membership",$oldVal);
                echo $this->session->userdata['membership']['amount'];exit;
            }else{
                echo 'error';
            }
        }else{
            echo 'error';
        }
        exit;
    }


    private function _retrieveCoupon($code)
    {
        \Stripe\Stripe::setApiKey(STRIPE_SECRET_KEY);
        try {
            $coupon = \Stripe\Coupon::retrieve($code);
            return array('success' => true, 'coupon' => $coupon);
        } catch (Exception $e) {
            return array('success' => false, 'reason' => 'other_error', 'details' => 'connection to stripe is not working');
        }
    }

    private function _createCustomer($detail)
    {
        \Stripe\Stripe::setApiKey(STRIPE_SECRET_KEY);
        try {
            $customer = \Stripe\Customer::create($detail);
            return array('success' => true, 'customer' => $customer);
        }catch (\Stripe\Error\InvalidRequest $e) {
            return array('success' => false, 'reason' => 'invalid_parameter_supplied', 'details' => 'Your charge attempt was declined. Please check the information you entered and try again.');
        } catch (\Stripe\Error\Authentication $e) {
            return array('success' => false, 'reason' => 'secret_key_not_valid', 'details' => 'Your charge attempt was declined. Please check the information you entered and try again.');
        } catch (\Stripe\Error\ApiConnection $e) {
            return array('success' => false, 'reason' => 'connection_problem', 'details' => 'connection to stripe is not working');
        } catch (Exception $e) {
            return array('success' => false, 'reason' => 'other_error', 'details' => 'connection to stripe is not working');
        }
    }

    private function reactivatingCanceledSubs($subsKey){
        //error_reporting(E_ALL);
        //ini_set('display_errors', 1);
        \Stripe\Stripe::setApiKey(STRIPE_SECRET_KEY);
        try {
            $subs = \Stripe\Subscription::retrieve($subsKey);
            //echo '<pre>';print_r($subs);exit;
            $planId = $subs->plan->id;
            $subs->plan = $planId;
            $subs->save();
            return (array('success' => true,  'details' => 'Auto renewal has been deactivated successfully'));
        } catch (Exception $e) {
            //var_dump($e);exit;
            return (array('success' => false, 'reason' => 'other_error', 'details' => 'Something went wrong please try again'));
        }
    }

    private function _addSubscription($detail)
    {
        //error_reporting(E_ALL);
        //ini_set('display_errors', 1);
        \Stripe\Stripe::setApiKey(STRIPE_SECRET_KEY);
        try {
            $subs = \Stripe\Subscription::create($detail);
            return array('success' => true, 'subs' => $subs);
        }catch (\Stripe\Error\InvalidRequest $e) {
            return array('success' => false, 'reason' => 'invalid_parameter_supplied', 'details' => 'Your charge attempt was declined. Please check the information you entered and try again.');
        } catch (\Stripe\Error\Authentication $e) {
            return array('success' => false, 'reason' => 'secret_key_not_valid', 'details' => 'Your charge attempt was declined. Please check the information you entered and try again.');
        } catch (\Stripe\Error\ApiConnection $e) {
            return array('success' => false, 'reason' => 'connection_problem', 'details' => 'connection to stripe is not working');
        } catch (Exception $e) {
            return array('success' => false, 'reason' => 'other_error', 'details' => 'connection to stripe is not working');
        }
    }

    private function _removeSubscription($subsKey)
    {
        \Stripe\Stripe::setApiKey(STRIPE_SECRET_KEY);
        try {
            $subs = \Stripe\Subscription::retrieve($subsKey);
            $subs->cancel(array('at_period_end' => true));
            return (array('success' => true,  'details' => 'Auto renewal has been deactivated successfully'));
        } catch (Exception $e) {
            return (array('success' => false, 'reason' => 'other_error', 'details' => 'Something went wrong please try again'));
        }
    }

    private function _removeSubscriptionPermanently($subsKey)
    {
        \Stripe\Stripe::setApiKey(STRIPE_SECRET_KEY);
        try {
            $subs = \Stripe\Subscription::retrieve($subsKey);
            $subs->cancel();
            return (array('success' => true,  'details' => 'Removed permanently'));
        } catch (Exception $e) {
            return (array('success' => false, 'reason' => 'other_error', 'details' => 'Something went wrong please try again'));
        }
    }

    function retrieveAndUpdateSubs($code,$end_trial)
    {
        //echo $code."\n";echo $end_trial."\n";echo date("Y-m-d H:i",$end_trial);exit;
        \Stripe\Stripe::setApiKey(STRIPE_SECRET_KEY);
        try {
            $subs = \Stripe\Subscription::retrieve($code);
            $subs->trial_end = $end_trial;
            $subs->prorate = false;
            $subs->save();
            return (array('success' => true,  'details' => 'Subscription has been updated'));
        } catch (Exception $e) {
            return (array('success' => false, 'reason' => 'other_error', 'details' => 'Something went wrong please try again'));
        }
    }

    function getcreateCustomer()
    {
        \Stripe\Stripe::setApiKey(STRIPE_SECRET_KEY);
        $customer = \Stripe\Customer::all(array("limit" => 3));
        echo '<pre>';print_r($customer);exit;
    }

    function removeCustomer($id)
    {
        try{
            \Stripe\Stripe::setApiKey(STRIPE_SECRET_KEY);
            $cu = \Stripe\Customer::retrieve($id);
            $cu->delete();
            return (array('success' => true,  'details' => 'Deleted'));
        } catch (Exception $e) {
            return (array('success' => false, 'reason' => 'other_error', 'details' => 'Something went wrong please try again'));
        }
        //echo '<pre>';print_r($cu);exit;
    }

    function stripePayment($paymentInfo)
    {
        \Stripe\Stripe::setApiKey(STRIPE_SECRET_KEY);
        $planInfo = null;
        try {
            $charge = \Stripe\Charge::create($paymentInfo);
            return array('success' => true, 'charge_info' => $charge);
        } catch (\Stripe\Error\Card $e) {
            $body = $e->getJsonBody();
            $err = $body['error'];

            return array('success' => false, 'reason' => 'card_declined', 'details' => $err['message']);
        } catch (\Stripe\Error\InvalidRequest $e) {
            return array('success' => false, 'reason' => 'invalid_parameter_supplied', 'details' => 'Your charge attempt was declined. Please check the information you entered and try again.');
        } catch (\Stripe\Error\Authentication $e) {
            return array('success' => false, 'reason' => 'secret_key_not_valid', 'details' => 'Your charge attempt was declined. Please check the information you entered and try again.');
        } catch (\Stripe\Error\ApiConnection $e) {
            return array('success' => false, 'reason' => 'connection_problem', 'details' => 'connection to stripe is not working');
        } catch (Exception $e) {
            return array('success' => false, 'reason' => 'other_error', 'details' => 'connection to stripe is not working');
        }
    }

    function expirationAlert(){
        // 3 and 30 days alert
        $this->db->select('int_glcode,id,userId,chr_user_type,var_fname,var_lname,var_email,endDate,Cast(endDate AS DATE) - INTERVAL 3 DAY as currentDate');
        $this->db->join('dz_user', 'dz_user.int_glcode = users_memberships.userId AND dz_user.is_subscribed = 1');
        $this->db->where('status = 1 AND (CURDATE() = Cast(endDate AS DATE) - INTERVAL 1 DAY OR CURDATE() = Cast(endDate AS DATE) - INTERVAL 3 DAY)');
        $users = $this->db->get('users_memberships')->result_array();
        foreach ($users as $val) {
            // send email
            $this->load->library('email');
            $this->email->set_mailtype("html");
            $this->email->from(NOREPLY, 'Wholesale Hotels Group');
            $this->email->to($val['var_email']);
            $this->email->subject('Account expiration');
            $data['data'] = $val;
            $message = $this->load->view('user/myaccount/expiration_email', $data, TRUE);
            $this->email->message($message);
            $this->email->send();
        }

        //echo '<pre>';print_r($users);
        //$row = $this->db->insert('users_memberships', array('userId'=>374));
        exit;
    }

    function expiredAlert(){
        // only for entities. user expired in hanlded with stripe webhook
        $this->db->select('int_glcode,id,userId,chr_user_type,var_fname,var_lname,var_email,endDate');
        $this->db->join('dz_user', 'dz_user.int_glcode = users_memberships.userId AND dz_user.is_subscribed = 1 AND dz_user.chr_user_type != "U"');
        $this->db->where('status = 1 AND CURDATE() = Cast(endDate AS DATE)');
        $users = $this->db->get('users_memberships')->result_array();
        foreach ($users as $val) {
            // update status
            $this->db->where(array('id'=>$val['id']));
            $this->db->update('users_memberships',array(
                'status'=> '0'
            ));

            $this->db->where(array('int_glcode'=>$val['int_glcode']));
            $this->db->set('is_subscribed', '3', FALSE);
            $this->db->update('dz_user');
            // send email
            $this->load->library('email');
            $this->email->set_mailtype("html");
            $this->email->from(NOREPLY, 'Wholesale Hotels Group');
            $this->email->to($val['var_email']);
            $this->email->subject('Account expired');
            $data['data'] = $val;
            if($val['chr_user_type'] == 'U')
                $message = $this->load->view('user/myaccount/expired_email', $data, TRUE);
            else if($val['chr_user_type'] == 'EN')
                $message = $this->load->view('user/myaccount/entity_expired_email', $data, TRUE);
            $this->email->message($message);
            $this->email->send();
        }

        //echo '<pre>';print_r($users);
        //$row = $this->db->insert('users_memberships', array('userId'=>374));
        exit;
    }
}
?>