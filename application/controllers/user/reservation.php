<?php

class Reservation extends User_Controller {
    var $layout = USER_LAYOUT;
    function __construct()
    {
        parent::__construct();
        $this->load->library('datatables');
        date_default_timezone_set(AMERICA_LOS_ANGLES_TIME_ZONE);
        $this->load->model('front/paymentsuccess');
        $this->load->helper('tourico_api');
        // get layout
        if ($this->session->userdata['menu_layout']){
            $this->layout = $this->session->userdata['menu_layout'];
        }

    }

    function index()
    {

    }

    function request_submitted()
    {
        if (!is_user_login()) {

            redirect("/");

        }
        regularUser_SubEntity_TP_Check();
        $data['page'] = "user/reservation/request_submitted";
        $this->page_name = 'Pending Reservations';
        $this->parent_menu = 'reservation';
        $data['js'] = array(
            'user/req_submitted.js',
            'custom/components-pickers.js',
        );
        $data['js_plugin'] = array(
            'bootstrap-datepicker/js/bootstrap-datepicker.js',
        );
        $data['css_plugin'] = array(
            'bootstrap-datepicker/css/datepicker.css'
        );
        $data['css'] = array(
        );
        $data['init'] = array(
            "confirm_reservation_datatable('ALL')",
            'ComponentsPickers.init_datepicker()'
        );
        $user = $this->session->userdata['valid_user']['id'];
        $data['booking'] = $this->db->where('uid' , $user)->where('book_status' , 'Request')->or_where('onreq_status' , 1)/*->where('pending_status ' , 0 )*/->or_where('pending_status' , 1)->order_by("id", "desc")->get('book_hotal')->result();
        $this->load->view($this->layout, $data);
    }

    function quotes()
    {

        $data['page'] = "user/reservation/quotes";
        $this->page_name = 'Quotes';
        $this->parent_menu = 'reservation';
        $data['js'] = array(
            'user/quotes.js',
            'custom/components-pickers.js',
        );
        $data['js_plugin'] = array(
            'bootstrap-datepicker/js/bootstrap-datepicker.js',
        );
        $data['css_plugin'] = array(
            'bootstrap-datepicker/css/datepicker.css'
        );
        $data['css'] = array(
        );
        $data['init'] = array(
            'Quotes.init()',
            'ComponentsPickers.init_datepicker()'
        );
        $this->load->view($this->layout, $data);
    }

    function confirm_reservation()
    {
        if (!is_user_login()) {

            redirect("/");

        }
        regularUser_SubEntity_TP_Check();
        $data['page'] = "user/reservation/confirm_reservation";
        $this->page_name = 'Confirmed Reservations';
        $this->parent_menu = 'reservation';
        $data['js'] = array(
            'user/confirm_reservation.js',
            'custom/components-pickers.js',
        );
        $data['js_plugin'] = array(
            'bootstrap-datepicker/js/bootstrap-datepicker.js',
        );
        $data['css_plugin'] = array(
            'bootstrap-datepicker/css/datepicker.css'
        );
        $data['css'] = array(
        );
        $data['init'] = array(
            "confirm_reservation_datatable('All')",
            "ComponentsPickers.init_datepicker()"
        );
        $this->load->view($this->layout, $data);
    }

    function req_submitted_datatable($status)
    {
        $where = '';
        $user = $this->session->userdata['valid_user']['id'];
        //$this->datatables->where("uid` = '$user' and (book_status = 'Request' or onreq_status = '1') and (pending_status = '0' or pending_status = '1')")->order_by("id", "desc")->from('book_hotal');
        $this->datatables->where("uid` = '$user' and (book_status = 'Request' or onreq_status = '1') and (pending_status = '0' or pending_status = '1')")->order_by("id", "desc")->from('book_hotal');
        $res = $this->datatables->generate();
        $results = json_decode($res, true);
        $newarray = array();
        if(isset($_POST['from']) && !empty($_POST['from']) && isset($_POST['to']) && !empty($_POST['to']))
        {
            $_POST['from'] = date("Y-m-d", strtotime($_POST['from']));
            $_POST['to'] = date("Y-m-d", strtotime($_POST['to']));
            foreach($results['aaData'] as $sing)
            {
                $datetime = $sing[10];
                if(strtotime($datetime) >= strtotime($_POST['from']) && strtotime($datetime) <= strtotime($_POST['to']))
                {
                    $newarray[] = $sing;
                }
            }
            $results['aaData'] = $newarray;
        }
        $newarray = array();
        $count = count($results['aaData']);
        foreach($results['aaData'] as $sing)
        {
            $st = '';
            if($sing[14] == 0)
            {
                if($sing[13] == 'Confirm')
                {
                    $st = "Approved";
                }
                else
                {
                    $st = "Pending";
                }
            }
            else
            {
                $st = "Declined";
            }
            $indArr = [date(DISPLAY_DATE_FORMAT_FULL, strtotime($sing[10])) , $sing[1] , date(DISPLAY_DATE_FORMAT, strtotime($sing[7])) , date(DISPLAY_DATE_FORMAT, strtotime($sing[8])) , $sing[9] , $sing[6] , $sing[11] , $st];
            $newarray[] = $indArr;
        }

        $results['aaData'] = $newarray;
        if ($status != '' && ($status == "Approved" || $status == "Declined" || $status == "Pending"))
        {
            $array = array();
            if ($status == "Approved")
            {
                for ($i = 0; $i < $count; $i++)
                {
                    if(stripos($results['aaData'][$i][7], "Approved")  !== false)
                    {
                        $array[] = $results['aaData'][$i];
                    }
                }
            }
            else if ($status == "Declined")
            {
                for ($i = 0; $i < $count; $i++)
                {
                    if(stripos($results['aaData'][$i][7], "Declined")  !== false)
                    {
                        $array[] = $results['aaData'][$i];

                    }
                }
            }
            else
            {
                for ($i = 0; $i < $count; $i++)
                {
                    if(stripos($results['aaData'][$i][7], "Pending")  !== false)
                    {
                        $array[] = $results['aaData'][$i];

                    }
                }
            }
            $results['aaData'] = $array;
        }

        echo json_encode($results);
    }

    function quotes_datatable($status)
    {
        if ($status != '') {
            $where = $this->datatables->where('dz_rplyquote.chr_quote_status', $status);
        } else {
            $where = '';
        }
        if ($_POST['from'] && $_POST['to']) {
            if ($_POST['from'] == $_POST['to']) {
                $this->datatables->like('dz_quote.var_checkin', date('Y-m-d', strtotime($_POST['from'])), 'both');
            } else {
                $this->datatables->where('dz_quote.var_checkin >=', date('Y-m-d', strtotime($_POST['from'])));
                $this->datatables->where('dz_quote.var_checkin <=', date('Y-m-d', strtotime($_POST['to'])));
            }
        }
        $this->datatables
            ->select('
                         dz_rplyquote.int_glcode,
                         dz_rplyquote.var_uniq_quoteid,
                         dz_quote.var_nameofreservation,
                         dz_quote.var_city,
                         dz_rplyquote.var_NOH,
                         dz_quote.var_room,
                         dz_quote.var_checkin,
                         dz_quote.var_checkout,
                         dz_quote.var_night,
                         dz_quote.quote_status,
                         dz_rplyquote.var_createddate,
                         dz_rplyquote.chr_status,
                         dz_rplyquote.var_admin_soures,
                         dz_quote.var_adult,
                         dz_rplyquote.chr_quote_status
                        ')
            ->from('dz_quote')
            ->join('dz_rplyquote', 'dz_quote.int_glcode=dz_rplyquote.int_fkquote')
            ->where_in('dz_quote.quote_status', array('R', 'E', 'C'))
            ->where_in('dz_rplyquote.chr_status', array("P", "UP", "AC"))
            ->where('dz_rplyquote.var_admin_soures', NULL)
            ->where('dz_quote.var_fkuser', $this->userid);
        $where;
        $res = $this->datatables->generate();

        $results = json_decode($res, true);

        $count = count($results['aaData']);
        for ($i = 0; $i < $count; $i++) {
            date_default_timezone_set(AMERICA_LOS_ANGLES_TIME_ZONE);
            $TotalHours = 0;
            $result = $results['aaData'][$i][10];
            $today = date('Y-m-d H:i:s');
            $start = new DateTime($result);
            $End = new DateTime($today);
            $interval = $start->diff($End);
            //  print_r($interval);

            $day = $interval->days;
            $houres = $interval->h;
            $TotalHours = ($day * 24) + $houres;
//              echo  $TotalHours; exit();
            $id = $results['aaData'][$i][0];
            $quoteid = $results['aaData'][$i][1];
            $name = $results['aaData'][$i][2];
            $city = $results['aaData'][$i][3];
            $hotelname = $results['aaData'][$i][4];
            $room = $results['aaData'][$i][5];
            $banifite = $results['aaData'][$i][13];
            $checkin = $results['aaData'][$i][6];
            $checkout = $results['aaData'][$i][7];
            $night = $results['aaData'][$i][8];

            if ($TotalHours > 24) {

                $str = '<a id=' . $id . ' class="btn  default_btn listing_btn resubmit" data-toggle="modal" href="#resubmit_modal" style="padding:5px 20px; background: red;">Resubmit</a><input type="hidden" class="quotecolor">';
            } else {

                $str = '<a id=' . $id . ' class="btn green_btn listing_btn view_modal couponview" data-toggle="modal" href="#myModal_autocomplete">View</a>';
                if ($results['aaData'][$i][11] != 'P') {
                    $id2 = $quoteid;
                    $str.= '<a id=' . $id . ' class="btn  default_btn listing_btn pay_quote" href="' . base_url() . 'user/reservation/userpay/' . $id2 . '" style="padding:5px 20px;">Pay</a>';
                }
            }
            $cityLen = strlen($results['aaData'][$i][3]);
            if ($cityLen > 11) {
                $citySmall = substr($results['aaData'][$i][3], 0, 11);
                $city = '<div style="word-wrap:break-word; width:85px" class="hoverShow">' . $citySmall . '..<span class="hoverShowDiv" style="display:none;">' . $results['aaData'][$i][3] . '</span></div>';
//                    $city = '<a href= "" data-toggle="tooltip" data-placement="bottom" style="color: black;text-decoration: none;" title="'.$results['aaData'][$i][3].'"><div style="word-wrap:break-word; width:85px">' . $citySmall . '..</div></a>';
            } else {
                $city = '<div style="word-wrap:break-word; width:85px">' . $results['aaData'][$i][3] . '</div>';
            }
            $HotelLen = strlen($results['aaData'][$i][4]);
            if ($HotelLen > 11) {
                $HotelSmall = substr($results['aaData'][$i][4], 0, 11);
                $hotelname = '<div style="word-wrap:break-word; width:85px" class="hoverShow">' . $HotelSmall . '..<span class="hoverShowDiv" style="display:none;">' . $results['aaData'][$i][4] . '</span></div>';
//                    $city = '<a href= "" data-toggle="tooltip" data-placement="bottom" style="color: black;text-decoration: none;" title="'.$results['aaData'][$i][3].'"><div style="word-wrap:break-word; width:85px">' . $citySmall . '..</div></a>';
            } else {
                $hotelname = '<div style="word-wrap:break-word; width:85px">' . $results['aaData'][$i][4] . '</div>';
            }
            $results['aaData'][$i][0] = $quoteid;
            $results['aaData'][$i][1] = '<div style="word-wrap:break-word; width:85px">' . $name . '</div>';
            $results['aaData'][$i][2] = $city;
            $results['aaData'][$i][3] = $hotelname;
            $results['aaData'][$i][4] = $room;
            $results['aaData'][$i][5] = $banifite;
            $results['aaData'][$i][6] = $checkin;
            $results['aaData'][$i][7] = $checkout;
            $results['aaData'][$i][8] = $night;
            $results['aaData'][$i][9] = $str;

        }
        echo json_encode($results);
    }

    function quotesstatus() {

        $id = $this->input->post('id');
        $this->db->where('int_glcode', $id);
        $query = $this->db->get('dz_rplyquote')->result_array();

        if ($this->input->post('coupan')) {
            $data_array3 = array(
                'var_status' => 'D',
                //   'dt_updated_date'=>date("Y-m-d H:i:s"),
            );
            $this->db->where('int_glcode', $this->input->post('coupan'));
            $this->db->update('dz_coupons', $data_array3);
        }
        $data_array = array(
            'quote_status' => 'C',
            'dt_updated_date' => date("Y-m-d H:i:s"),
        );
        $this->db->where('int_glcode', $query[0]['int_fkquote']);
        $this->db->update('dz_quote', $data_array);

        $this->db->where('int_glcode', $query[0]['int_fkquote']);
        $quote = $this->db->get('dz_quote')->result_array();


        //payment relase date
        $paymentdate = date("Y-m-d", strtotime(date("Y-m-d", strtotime($quote[0]['var_checkout'])) . " +90 day"));

        $data_array1 = array(
            'chr_status' => 'P',
            'var_updateddate' => date("Y-m-d H:i:s"),
        );
        $this->db->where('int_glcode', $id);
        $this->db->update('dz_rplyquote', $data_array1);

        $query1 = $this->db->select('var_prize')->get_where('dz_rplyquote', array('int_glcode' => $id))->row();
        $actual_amount = $query1->var_prize;
        $paid = preg_replace('/\D/', '', $actual_amount) / 100;
        $off_percentage = ($paid * 25) / 100;
        $paid_amount = $paid - $off_percentage;

        $commision_percentage = ($paid * 5) / 100;
        //   $commision_amount = $paid - $commision_percentage;

        $get_perent = $this->db->select('fk_parent_account')->get_where('dz_user', array('int_glcode' => $this->userid))->row();

        if ($get_perent->fk_parent_account != "") {
            $data_array2 = array(
                'fk_user' => $get_perent->fk_parent_account,
                'fk_quote' => $query[0]['int_fkquote'],
                'chr_status' => 'U',
                'var_actual_amont' => $paid,
                'var_paid_amount' => $paid_amount,
                'var_commision_amount' => $commision_percentage,
                'var_releasedate' => $paymentdate,
                'dt_created_date' => date('Y-m-d H:i:s'),
                'dt_update_date' => date('Y-m-d H:i:s'),
            );
//            print_r($data_array);
//            exit();
            $result = $this->db->insert('dz_commision', $data_array2);
            if ($result) {
                echo "success";
                exit;
            } else {
                echo "error";
                exit;
            }
        } else {
            echo "success";
        }
    }

    function conf_reservation_datatable($status)
    {
        $search = $_POST['sSearch'];
        $where = '';
        $user = $this->session->userdata['valid_user']['id'];
        $this->datatables->from('booking_detail')->where('uid' , $user)->where('status' , 'Confirm')->order_by("id", "desc");
        $res = $this->datatables->generate();
        $results = json_decode($res, true);
        //echo '<pre>';print_r($results);exit;
        $newarray = array();
        if(isset($_POST['from']) && !empty($_POST['from']) && isset($_POST['to']) && !empty($_POST['to']))
        {
            $_POST['from'] = date("Y-m-d", strtotime($_POST['from']));
            $_POST['to'] = date("Y-m-d", strtotime($_POST['to']));
            foreach($results['aaData'] as $sing)
            {
                $bookid = $sing[4];
                if(strtotime(getBookingcheckIn($bookid)) >= strtotime($_POST['from']) && strtotime(getBookingcheckOut($bookid)) <= strtotime($_POST['to']))
                {
                    $newarray[] = $sing;
                }
            }
            $results['aaData'] = $newarray;
        }
        $count = count($results['aaData']);
        for ($i = 0; $i < $count; $i++)
        {
            $id = $results['aaData'][$i][0];
            $checkIn = date('Y-m-d',strtotime($results['aaData'][$i][6]));
            $checkOut = date('Y-m-d',strtotime($results['aaData'][$i][7]));
            $roomInf = unserialize($results['aaData'][$i][23]);
            //echo '<pre>';print_r($roomInf);exit;
            $bookid = $results['aaData'][$i][4];
            $bookingDate = getBookingDateTime($bookid);
            $paidAmount = getBookingperRoomPaid($bookid);//$results['aaData'][$i][11];
            $status1 = '';
            if($results['aaData'][$i][19] == 0)
            {
                if(getBookingcheckIn($bookid) >= date('Y-m-d'))
                {
                    if($results['aaData'][$i][24] == 0)
                    {
                        $status1 =  '<a onclick="getCancelationFee('.$id.')">CANCEL </a>';
                    }
                    else
                    {
                        $payment_info = unserialize(getPaymentInfo($bookid));
                        $reomprefst = explode("@",$payment_info['room_pref_string']);
                        $roomwisebrek = array();
                        if(count($payment_info['roomInfo']) > 0)
                        {
                            foreach($payment_info['roomInfo'] as $key => $singleUser)
                            {
                                $roombrde = explode('|',$singleUser['optradio']);
                                $allbreakdo = explode('-' , $roombrde[4]);
                                unset($allbreakdo[count($allbreakdo) - 1]);
                                $roomwisebrek[] = $allbreakdo;
                            }
                        }
                        $breakdown = "";
                        $check_in = getBookingcheckIn($bookid);
                        $getRoomConfNumbers = getRoomConfNumbers($bookid);
                        foreach($roomwisebrek as $key => $singcheckrbd)
                        {
                            $roomno = $key + 1;
                            $inner = "";
                            foreach($singcheckrbd as $key1=>$singinner)
                            {
                                $inner .= '
								<tr>
									<td colspan="5" style="font-size:10px;font-weight:normal;">'.date('l, F d,Y', strtotime($check_in. ' + '.$key1.' days')).'</td>
									<td style="font-size:10px; font-weight:bold;">$'.$singinner.'</td>
								</tr>
								';
                            }
                            $inner .= '<tr style="height:10px"></tr>';
                            /*$breakdown .= '
                                <tr>
                                    <td colspan="6" style="font-size:10px; font-weight:bold;">Room '.$roomno.'('.$getRoomConfNumbers[$key]->confirm_no.')</td>
                                </tr>

                                    '.$inner.'

                            ';*/
                        }
                        //$payment_info['total_room_acost']
                        /*if(!empty($breakdown))
                        {
                            $price = substr($payment_info['total_room_cost'],1,strlen($payment_info['total_room_cost']));
                            $taxes = substr($payment_info['total_tax_fees'],1,strlen($payment_info['total_tax_fees']));
                            $total_supp_amount = substr($payment_info['total_supp_amount'],1,strlen($payment_info['total_supp_amount']));
                            $coupon = getcomrescoupon($bookid);
                            $price = $price + $taxes + $total_supp_amount;
                            $price = '$'.$price;
                            if($coupon != 0)
                            {
                                $coupon = '<tr>
                                    <td colspan="5" style="font-size:10px;font-weight:bold;">Coupon</td>
                                    <td style="font-size:10px; font-weight:bold;">'.$coupon.'</td>
                                </tr>';
                            }
                            else
                            {
                                $coupon = "";
                            }
                            $breakdown = '<table>'.$breakdown.'
                                <tr>
                                    <td colspan="5" style="font-size:10px;font-weight:bold;">Room Cost</td>
                                    <td style="font-size:10px; font-weight:bold;">'.$payment_info['total_room_acost'].'</td>
                                </tr>
                                <tr>
                                    <td colspan="5" style="font-size:10px;font-weight:bold;">Supplement</td>
                                    <td style="font-size:10px; font-weight:bold;">'.$payment_info['total_supp_amount'].'</td>
                                </tr>
                                <tr>
                                    <td colspan="5" style="font-size:10px;font-weight:bold;">Taxes/Fees</td>
                                    <td style="font-size:10px; font-weight:bold;">'.$payment_info['total_tax_fees'].'</td>
                                </tr>
                                '.$coupon.'
                                <tr>
                                    <td colspan="5" style="font-size:10px;font-weight:bold;">TOTAL PAID</td>
                                    <td style="font-size:10px; font-weight:bold;">'.$price.'</td>
                                </tr>
                                <tr style="height:10px"></tr>
                            </table>';
                        }*/

                        $cpol = "";
                        if($results['aaData'][$i][27] == 'TO') {
                            $breakdown .= "Your hotel is: ".$roomInf['ProductInfo']['@attributes']['name']."<br>";
                            $breakdown .= "Your reservation is: ".$checkIn.'/'.$checkOut."<br>";
                            $cancel_pol = unserialize(getcancelpolicy($bookid));
                            foreach ($cancel_pol as $cp) {
                                $cpol .= $cp['message'] . " And check in date is <b>" . getBookingcheckIn($bookid) . "</b><br>";
                            }
                        }else{
                            $breakdown .= "Your hotel is: ".$roomInf['hotelName']."<br>";
                            $breakdown .= "Your reservation is: ".$checkIn.'/'.$checkOut."<br>";
                            $cpol .= $results['aaData'][$i][28];
                        }
                        $breakdown .= '
						<tr>
							<td colspan="6" style="font-size:10px;
							font-weight:normal;">Your cancellation policy is: '.$cpol.'</td>
						</tr>';
                        $breakdown .= 'You paid: $'.$paidAmount.'<br>';
                        $status1 =  '<a onclick="adminCancelationalert('.$id.')">CANCEL </a>
							<div id="admincantext'.$id.'" style="display:none;">
								'.$breakdown.'
							</div>
						';
                    }
                }
                else
                {
                    $status1 =  "<span style='color:red' class='OUT DATED'></span>";
                }
            }
            else
            {
                $status1 =  "<span style='color:red'>CANCELED</span>";
            }
            if($results['aaData'][$i][27] == 'TL') {
                $confNum = explode('-', $results['aaData'][$i][3]);
                $confNum = $confNum[0];
                $guestName = $roomInf['guestInfo']['firstname'].' '.$roomInf['guestInfo']['lastname'];
            }else{
                $confNum = $results['aaData'][$i][3];
                $infoAttr = $roomInf['ProductInfo']['Passenger']['@attributes'];
                $guestName = $infoAttr['firstName'].' '.$infoAttr['lastName'];
            }


            $results['aaData'][$i][0] = $bookingDate;
            $results['aaData'][$i][1] = $guestName;
            $results['aaData'][$i][2] = '<span id="confimationo'.$id.'">'.$results['aaData'][$i][3].'</span>';
            $results['aaData'][$i][3] = getBookingLocation($bookid);
            $results['aaData'][$i][4] = getBookinghotelName($bookid);
            $results['aaData'][$i][5] = $results['aaData'][$i][20];
            $results['aaData'][$i][6] = getBookingcheckIn($bookid);
            $results['aaData'][$i][7] = getBookingcheckOut($bookid);
            $results['aaData'][$i][8] = '$'.getBookingperRoomPaid($bookid).'<input type="hidden" value='.getBookingperRoomPaid($bookid).'" id="roomprice'.$id.'" />';
            if($this->session->userdata['valid_user']['var_usertype'] == 'TF'){
                $results['aaData'][$i][9] = '<a href="'.base_url().'front/quote_request/pdf/'.$bookid.'"><img src="https://iconbug.com/data/5b/507/52ff0e80b07d28b590bbc4b30befde52.png" style="width:30px" /></a> / <a href="'.base_url().'front/quote_request/voucher/'.$bookid.'"><img src="https://iconbug.com/data/5b/507/52ff0e80b07d28b590bbc4b30befde52.png" style="width:30px" /></a>';
            }else {
                $results['aaData'][$i][9] = '<a href="'.base_url().'front/quote_request/pdf/'.$bookid.'"><img src="https://iconbug.com/data/5b/507/52ff0e80b07d28b590bbc4b30befde52.png" style="width:30px" /></a>';
            }
            $results['aaData'][$i][10] = '<div class="bookCanId' . $confNum . '" >' . $status1 . '</div>';
        }
        if(!empty($search))
        {
            $array = array();
            for ($i = 0; $i < $count; $i++)
            {
                if(
                    stripos($results['aaData'][$i][0], $search) !== false  ||
                    stripos($results['aaData'][$i][1], $search) !== false ||
                    stripos($results['aaData'][$i][2], $search) !== false ||
                    stripos($results['aaData'][$i][3], $search) !== false ||
                    stripos($results['aaData'][$i][4], $search) !== false ||
                    stripos($results['aaData'][$i][5], $search) !== false ||
                    stripos($results['aaData'][$i][6], $search) !== false ||
                    stripos($results['aaData'][$i][7], $search) !== false ||
                    stripos($results['aaData'][$i][8], $search) !== false ||
                    stripos($results['aaData'][$i][9], $search) !== false ||
                    stripos($results['aaData'][$i][10], $search) !== false
                )
                {
                    $array[] = $results['aaData'][$i];
                }
            }
            $results['aaData'] = $array;
        }
        if ($status != '' && ($status == "OUTDATED" || $status == "ACTIVE" || $status == "CANCEL"))
        {
            $array = array();
            if ($status == "OUTDATED")
            {
                for ($i = 0; $i < $count; $i++)
                {
                    if(stripos($results['aaData'][$i][10], "OUT DATED")  !== false)
                    {
                        $array[] = $results['aaData'][$i];
                    }
                }
            }
            elseif ($status == "CANCEL")
            {
                for ($i = 0; $i < $count; $i++)
                {
                    if(stripos($results['aaData'][$i][10], "CANCELED")  !== false)
                    {
                        $array[] = $results['aaData'][$i];
                    }
                }
            }
            else
            {
                for ($i = 0; $i < $count; $i++)
                {
                    if(stripos($results['aaData'][$i][10], "CANCEL ")  !== false)
                    {
                        $array[] = $results['aaData'][$i];

                    }
                }
            }
            $results['aaData'] = $array;
        }
        echo json_encode($results);
    }

    function faqs() {
        if (!is_user_login()) {

            redirect("/");

        }
        regularUserAndSubEntityCheck();
        $data['page'] = "user/reservation/faqs";
        $this->page_name = 'Faq';
        $this->parent_menu = 'reservation';
        $data['js'] = array(
            'user/faq.js',
        );
        $data['js_plugin'] = array(
        );
        $data['css_plugin'] = array(
        );
        $data['css'] = array(
        );
        $data['init'] = array(
            'faq.init()',
        );



        $this->load->view($this->layout, $data);
    }

    function searchfaqs() {
        $value = $this->input->post('search');
        if ($value != '') {
            $this->db->like('var_quation', $value, 'both');
        }
        $query = $this->db->get('dz_faqs')->result_array();
        $data['faqdata'] = $query;
        $this->load->view('user/reservation/searchfaqs', $data);
    }

    function get_reply() {

        $this->db->select('dz_quote.*,
            dz_rplyquote.var_site1,
            dz_rplyquote.int_site1_price,
            dz_rplyquote.var_room_type,
            dz_rplyquote.var_address,
            dz_rplyquote.var_site2,
            dz_rplyquote.int_site2_price,
            dz_rplyquote.var_NOH,
            dz_rplyquote.var_tripadvisor,
            dz_rplyquote.var_uniq_quoteid,
            dz_rplyquote.var_policy,
            dz_rplyquote.var_comment,
            dz_rplyquote.var_star,
            dz_rplyquote.var_NOH,
            dz_rplyquote.var_prize,
            dz_rplyquote.var_src,
            dz_rplyquote.int_src_price');
        $this->db->from('dz_quote');
        $this->db->join('dz_rplyquote', 'dz_quote.int_glcode=dz_rplyquote.int_fkquote');
        $this->db->where_in('dz_rplyquote.int_glcode', $_POST['id']);
        $query = $this->db->get()->result_array();
        // echo $this->db->last_query();
        if ($query[0]['int_site1_price'] != "" && $query[0]['int_site2_price'] != 0) {
            if ($query[0]['int_site1_price'] > $query[0]['int_site2_price']) {
                $price = floatval($query[0]['int_site2_price']) - floatval($query[0]['var_prize']);
            } else {
                $price = floatval($query[0]['int_site1_price']) - floatval($query[0]['var_prize']);
            }
        } else {
            $price = floatval($query[0]['int_site1_price']) - floatval($query[0]['var_prize']);
        }

        $query1[] = array(
            'int_glcode' => $query[0]['int_glcode'],
            'var_fkuser' => $query[0]['var_fkuser'],
            'fk_coupon' => $query[0]['fk_coupon'],
            'var_city' => $query[0]['var_city'],
            'var_checkin' => $query[0]['var_checkin'],
            'var_checkout' => $query[0]['var_checkout'],
            'var_night' => $query[0]['var_night'],
            'var_room' => $query[0]['var_room'],
            'var_adult' => $query[0]['var_adult'],
            'var_child' => $query[0]['var_child'],
            'var_hotelname' => $query[0]['var_hotelname'],
            'var_rating' => $query[0]['var_rating'],
            'var_nameofreservation' => $query[0]['var_nameofreservation'],
            'var_comments' => $query[0]['var_comments'],
            'var_points' => $query[0]['var_points'],
            'quote_status' => $query[0]['quote_status'],
            'dt_reuested_date' => $query[0]['dt_reuested_date'],
            'dt_updated_date' => $query[0]['dt_updated_date'],
            'dt_created_date' => $query[0]['dt_created_date'],
            'var_site1' => $query[0]['var_site1'],
            'int_site1_price' => number_format((float) $query[0]['int_site1_price'], 2, '.', ','),
            'var_site2' => $query[0]['var_site2'],
            'int_site2_price' => number_format((float) $query[0]['int_site2_price'], 2, '.', ','),
            'var_src' => $query[0]['var_src'] . ' - $' . number_format((float) $query[0]['int_src_price'], 2, '.', ','),
            'var_NOH' => $query[0]['var_NOH'],
            'var_tripadvisor' => $query[0]['var_tripadvisor'],
            'var_uniq_quoteid' => $query[0]['var_uniq_quoteid'],
            'var_policy' => $query[0]['var_policy'],
            'var_room_type' => $query[0]['var_room_type'],
            'var_address' => $query[0]['var_address'],
            'var_comment' => $query[0]['var_comment'],
            'var_star' => $query[0]['var_star'],
            'var_prize' => number_format((float) $query[0]['var_prize'], 2, '.', ','),
            'savingamount' => number_format((float) $price, 2, '.', ','),
        );

        echo json_encode($query1);
        exit();
    }

    function get_confirmreply() {
        $this->db->select('dz_quote.*,
            dz_rplyquote.var_site1,
            dz_rplyquote.int_site1_price,
            dz_rplyquote.var_site2,
            dz_rplyquote.int_site2_price,
            dz_rplyquote.var_NOH,
            dz_rplyquote.var_tripadvisor,
            dz_rplyquote.var_uniq_quoteid,
            dz_rplyquote.var_transactionid,
            dz_rplyquote.var_policy,
            dz_rplyquote.var_comment,
            dz_rplyquote.var_star,
            dz_rplyquote.var_NOH,
            dz_rplyquote.var_src,
            dz_rplyquote.int_src_price,
            dz_rplyquote.var_prize,
            dz_rplyquote.chr_status,
            dz_rplyquote.date_usercanceltime');
        $this->db->from('dz_quote');
        $this->db->join('dz_rplyquote', 'dz_quote.int_glcode=dz_rplyquote.int_fkquote');
        $this->db->where_in('dz_rplyquote.int_glcode', $_POST['id']);
        $query = $this->db->get()->result_array();
        // echo $this->db->last_query();
        if ($query[0]['int_site1_price'] != "" && $query[0]['int_site2_price'] != 0) {
            if ($query[0]['int_site1_price'] > $query[0]['int_site2_price']) {
                $price = floatval($query[0]['int_site2_price']) - floatval($query[0]['var_prize']);
            } else {
                $price = floatval($query[0]['int_site1_price']) - floatval($query[0]['var_prize']);
            }
        } else {
            $price = floatval($query[0]['int_site1_price']) - floatval($query[0]['var_prize']);
        }
        $query1[] = array(
            'int_glcode' => $query[0]['int_glcode'],
            'var_fkuser' => $query[0]['var_fkuser'],
            'fk_coupon' => $query[0]['fk_coupon'],
            'var_city' => $query[0]['var_city'],
            'var_checkin' => $query[0]['var_checkin'],
            'var_checkout' => $query[0]['var_checkout'],
            'var_night' => $query[0]['var_night'],
            'var_room' => $query[0]['var_room'],
            'var_adult' => $query[0]['var_adult'],
            'var_child' => $query[0]['var_child'],
            'var_hotelname' => $query[0]['var_hotelname'],
            'var_rating' => $query[0]['var_rating'],
            'var_nameofreservation' => $query[0]['var_nameofreservation'],
            'var_comments' => $query[0]['var_comments'],
            'var_points' => $query[0]['var_points'],
            'quote_status' => $query[0]['quote_status'],
            'dt_reuested_date' => $query[0]['dt_reuested_date'],
            'dt_updated_date' => $query[0]['dt_updated_date'],
            'dt_created_date' => $query[0]['dt_created_date'],
            'var_site1' => $query[0]['var_site1'],
            'int_site1_price' => number_format((float) $query[0]['int_site1_price'], 2, '.', ','),
            'var_site2' => $query[0]['var_site2'],
            'int_site2_price' => number_format((float) $query[0]['int_site2_price'], 2, '.', ','),
            'var_NOH' => $query[0]['var_NOH'],
            'var_src' => $query[0]['var_src'] . ' - $' . number_format((float) $query[0]['int_src_price'], 2, '.', ','),
            'var_tripadvisor' => $query[0]['var_tripadvisor'],
            'var_uniq_quoteid' => $query[0]['var_uniq_quoteid'],
            'var_policy' => $query[0]['var_policy'],
            'var_comment' => $query[0]['var_comment'],
            'var_transactionid' => $query[0]['var_transactionid'],
            'chr_status' => $query[0]['chr_status'],
            'date_usercanceltime' => date('F d, Y - h:i A', strtotime($query[0]['date_usercanceltime'])),
            'var_star' => $query[0]['var_star'],
            'var_prize' => number_format((float) $query[0]['var_prize'], 2, '.', ','),
            'savingamount' => number_format((float) $price, 2, '.', ','),
        );

        echo json_encode($query1);
        exit();
    }

    function resubmit() {
        //print_r($_POST); exit();
        if ($this->input->post()) {
            date_default_timezone_set(AMERICA_LOS_ANGLES_TIME_ZONE);
            $this->db->select('var_createddate,int_glcode,int_fkquote');
            $this->db->where('int_glcode', $this->input->post('id'));
            $query = $this->db->get('dz_rplyquote')->result_array();

//            $this->db->where('int_glcode', $id);
//            $this->db->delete('dz_user');


            $data_array = array(
                'quote_status' => 'NR',
                'fk_resubmit_id' => $this->input->post('id'),
                'dt_created_date' => date('Y-m-d H:i:s'),
                'dt_reuested_date' => date('Y-m-d H:i:s')
            );

            $this->db->where('int_glcode', $query[0]['int_fkquote']);
            $result = $this->db->update('dz_quote', $data_array);

            $data_array1 = array(
                'var_createddate'=> date('Y-m-d H:i:s'),
            );
            $this->db->where('int_glcode', $this->input->post('id'));
            $this->db->update('dz_rplyquote', $data_array1);


            if ($result) {
                echo"success";
            } else {
                echo "error";
            }
        }
    }

    function userpay($replayquoteid1) {        //print_r($replayquoteid);EXIT;
        $this->db->select('int_glcode');
        $this->db->from('dz_rplyquote');
        $this->db->where('var_uniq_quoteid', $replayquoteid1);
        $replayid = $this->db->get()->result_array();

        $replayquoteid = $replayid[0]['int_glcode'];
        $data['page'] = "user/reservation/userpay";
//        $data['page'] = "user/reservation/confirmation_page";
        $this->page_name = 'Payment Page';
        $this->parent_menu = 'reservation';
        $data['js'] = array(
            'user/userpay.js',
            'custom/components-pickers.js',
        );
        $data['js_plugin'] = array(
            'bootstrap-datepicker/js/bootstrap-datepicker.js',
        );
        $data['css_plugin'] = array(
            'bootstrap-datepicker/css/datepicker.css'
        );
        $data['css'] = array(
        );
        $data['init'] = array(
            'Userpay.init()',
            'ComponentsPickers.init_datepicker()'
        );

        $this->db->select('dz_quote.*,
            dz_rplyquote.var_site1,
            dz_rplyquote.int_site1_price,
            dz_rplyquote.var_site2,
            dz_rplyquote.int_site2_price,
            dz_rplyquote.var_tripadvisor,
            dz_rplyquote.var_uniq_quoteid,
            dz_rplyquote.var_policy,
            dz_rplyquote.var_comment,
            dz_rplyquote.var_star,
            dz_rplyquote.var_NOH,
            dz_rplyquote.var_prize,
            dz_rplyquote.var_room_type,
            dz_rplyquote.var_address,'
        );
        $this->db->from('dz_quote');
        $this->db->join('dz_rplyquote', 'dz_quote.int_glcode=dz_rplyquote.int_fkquote');
        $this->db->where('dz_rplyquote.int_glcode', $replayquoteid);
        $query = $this->db->get()->result_array();

        // echo json_encode($query); exit();
        $data['datacontent'] = $query;
        $this->db->where('fk_user', $this->userid);
        $this->db->where('var_status', 'A');
        $data['coupons'] = $this->db->get('dz_coupons')->result_array();
        $data['bicoin_policy'] = $this->db->get('dz_bitcoin_payment')->result_array();
        $data['replayquoteid'] = $replayquoteid;
        $data['refer_link'] = $this->db->select('var_accountid')->where('int_glcode', $this->userid)->get('dz_user')->row_array();
        $this->load->view($this->layout, $data);
    }

    function getfinalamount($coupanid) {
        if ($this->input->post()) {
            $this->db->select('*');
            $this->db->where('int_glcode', $this->input->post('coupanid'));
            $coupan = $this->db->get('dz_coupons')->result_array();

            if ($coupan[0]['var_coupan_type'] == 1) {
                // echo $coupan[0]['var_couponvalue'] - $this->input->post('price');
                $discoutamount = floatval(($coupan[0]['var_couponvalue'] * $this->input->post('price')) / 100);
                // $discoutamount = number_format((float) $discoutamount, 2, '.', ',');
                // echo $discoutamount;
                //  exit();
                $acutelamount = $this->input->post('price') - $discoutamount;
                $acutelamount = number_format((float) $acutelamount, 2, '.', ',');
                echo '$' . $acutelamount;
            }
            if ($coupan[0]['var_coupan_type'] == 2) {
                $acutelamount = $this->input->post('price') - $coupan[0]['var_couponvalue'];
                $acutelamount = number_format((float) $acutelamount, 2, '.', ',');
                echo '$' . $acutelamount;
            }
        }
    }

    function getorignalamount() {
        $acutelamount = number_format((float) $this->input->post('price'), 2, '.', ',');
        echo '$' . $acutelamount;
    }

    function paypalsuccess($replyquotid, $coupanid = NULL) {

        //print_r($replyquotid);
//        print_r($this->input->post());EXIT;
        // comistion count start
        $id = $replyquotid;
        $this->db->where('int_glcode', $id);
        $query = $this->db->get('dz_rplyquote')->result_array();
        // print_r($query);
        // exit();
        //  echo $coupanid;
        //    exit();
        if ($coupanid != "") {
//            $data_array3 = array(
//                'var_status' => 'U',
//                    //   'dt_updated_date'=>date("Y-m-d H:i:s"),
//            );
//            $this->db->where('int_glcode', $coupanid);
//            $this->db->update('dz_coupons', $data_array3);
//
//            $data_array = array(
//                'quote_status' => 'C',
//                'dt_updated_date' => date("Y-m-d H:i:s"),
//                'fk_coupon' => $coupanid
//            );
//            $this->db->where('int_glcode', $query[0]['int_fkquote']);
//            $this->db->update('dz_quote', $data_array);
//
//            $data_array1 = array(
//                'fk_coupon' => $coupanid
//            );
//            $this->db->where('int_glcode', $id);
//            $this->db->update('dz_rplyquote', $data_array1);
            //     echo $this->db->last_query();
            //    exit();
        } else {
//            $data_array = array(
//                'quote_status' => 'C',
//                'dt_updated_date' => date("Y-m-d H:i:s"),
//                    //  'fk_coupon' => $coupanid
//            );
//            $this->db->where('int_glcode', $query[0]['int_fkquote']);
//            $this->db->update('dz_quote', $data_array);
        }

        $this->db->where('int_glcode', $query[0]['int_fkquote']);
        $quote = $this->db->get('dz_quote')->result_array();

        //payment relase date
        $paymentdate = date("Y-m-d", strtotime(date("Y-m-d", strtotime($quote[0]['var_checkout'])) . " +90 day"));

//        $data_array12 = array(
//            'chr_status' => 'P',
//            'var_updateddate' => date("Y-m-d H:i:s"),
//        );
//        $this->db->where('int_glcode', $id);
//        $this->db->update('dz_rplyquote', $data_array12);
        //  echo $this->db->last_query();
        //  exit();
        $query1 = $this->db->select('var_prize')->get_where('dz_rplyquote', array('int_glcode' => $id))->row();

        $actual_amount = $query1->var_prize;

        $paid1 = preg_replace('/\D/', '', $actual_amount) / 100;

        if ($coupanid != "") {

            $paid = $this->getfinalamount1($paid1, $coupanid);

        } else {

            $paid = $paid1;

        }

        // transaction id start
        if ($query[0]['var_transactionid'] == "" || $query[0]['var_transactionid'] == NULL) {
//            $this->load->model('admin/quote');
//            $random = $this->quote->random_num();
//            $data_array13 = array(
//                'var_transactionid' => $random,
//            );
//            $this->db->where('int_glcode', $id);
//            $this->db->update('dz_rplyquote', $data_array13);
            //   echo $this->db->last_query();

//            $query[0]['var_transactionid'] = $random;

            // cout point
//            $get_perent1 = $this->db->select('fk_parent_account,var_email,var_point')->get_where('dz_user', array('int_glcode' => $this->userid))->row();
//            $uamount = $get_perent1->var_point + $paid;
//            $pointupdate = array(
//                'var_point' => $uamount,
//            );
//            $this->db->where('int_glcode', $this->userid);
//            $this->db->update('dz_user', $pointupdate);
            // cout point
        }

        // transaction id end

        $commision_percentage = ($paid * 5) / 100;
        $commision_percentage = number_format((float) $commision_percentage, 2, '.', '');
        //   $commision_amount = $paid - $commision_percentage;
        $this->db->select('dz_quote.*,
                            dz_rplyquote.var_site1,
                            dz_rplyquote.int_site1_price,
                            dz_rplyquote.var_site2,
                            dz_rplyquote.int_site2_price,
                            dz_rplyquote.var_tripadvisor,
                            dz_rplyquote.var_uniq_quoteid,
                            dz_rplyquote.var_policy,
                            dz_rplyquote.var_comment,
                            dz_rplyquote.var_NOH,
                            dz_rplyquote.var_star,
                            dz_rplyquote.var_prize,
                            dz_rplyquote.var_transactionid');
        $this->db->from('dz_quote');
        $this->db->join('dz_rplyquote', 'dz_quote.int_glcode=dz_rplyquote.int_fkquote');

        $this->db->where_in('dz_rplyquote.int_glcode', $id);
        $datacontent = $this->db->get()->result_array();
//print_r($datacontent);exit;
        $this->db->where('int_glcode', $coupanid);
        $coupanidquery = $this->db->get('dz_coupons')->result_array();

        if ($coupanidquery[0]['var_coupan_type'] == 1) {

            $coupanstr = $coupanidquery[0]['var_couponvalue'] . '% OFF';
        }
        if ($coupanidquery[0]['var_coupan_type'] == 2) {

            $coupanstr = '$' . $coupanidquery[0]['var_couponvalue'] . 'OFF';
        }

        // automatic counter
//        $this->load->model('front/paymentsuccess');
//        $content = array('id' => $query[0]['var_transactionid']);

//        $inserdata = $this->paymentsuccess->get_quetes($content);
        //  print_r($inserdata);

//        $dataarray = array(
//            'id' => $query[0]['var_transactionid'],
//            'username' => $inserdata['username'],
//            'quote' => $inserdata['quote'],
//            'highprice' => $inserdata['price'],
//            'saveprice' => $inserdata['save'],
//            'fkid' => $inserdata['fkid'],
//        );
//        $this->manualentry_model->insert_data($dataarray);
        //  exit();
        //automatic counter
        $get_perent = $this->db->select('fk_parent_account,var_email,var_point,var_accountid')->get_where('dz_user', array('int_glcode' => $this->userid))->row();
//        $this->email->set_mailtype("html");
//        $this->email->from(NOREPLY, 'Admin');
//        $this->email->to($get_perent->var_email);
//        $this->email->subject('Thank you for payment');
//        $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Dear Member</p>';
//        $mail_body .= '<p style="color:#fff;">Your booking is successfully.Follwing is detail for it.</p>';
//        $mail_body .= '<p style="color:#fff; display: inline-block; width: 47%; margin:5px">Check-in: ' . $datacontent[0]['var_checkin'] . '</p>';
//        $mail_body .= '<p style="color:#fff; display: inline-block; width: 47%; margin:5px">Check-out: ' . $datacontent[0]['var_checkout'] . '</p>';
//
//        $mail_body .= '<p style="color:#fff; display: inline-block; width: 47%; margin:5px">QUOTE ID: ' . $datacontent[0]['var_uniq_quoteid'] . '</p>';
////        $mail_body .= '<p style="color:#fff;">Name of hotel: ' . $datacontent[0]['var_NOH'] . '</p>';
//        $mail_body .= '<p style="color:#fff; display: inline-block; width: 47%; margin:5px">Stars: ' . $datacontent[0]['var_star'] . '</p>';
//        $mail_body .= '<p style="color:#fff; display: inline-block; width: 47%; margin:5px">TripAdvisor: ' . $datacontent[0]['var_tripadvisor'] . '</p>';
//        $mail_body .= '<p style="color:#fff; display: inline-block; width: 47%; margin:5px">Rooms: ' . $datacontent[0]['var_room'] . '</p>';
//        $mail_body .= '<p style="color:#fff; display: inline-block; width: 47%; margin:5px">Adults: ' . $datacontent[0]['var_adult'] . '</p>';
//        $mail_body .= '<p style="color:#fff; display: inline-block; width: 47%; margin:5px">Children: ' . $datacontent[0]['var_child'] . '</p>';
//        $mail_body .= '<p style="color:#fff; display: inline-block; width: 47%; margin:5px">Cancellation policy: ' . $datacontent[0]['var_policy'] . '</p>';
//        $mail_body .= '<p style="color:#fff; display: inline-block; width: 47%; margin:5px">Current Price: ' .'$'.$datacontent[0]['var_prize'] . '</p>';
//        $mail_body .= '<p style="color:#fff; display: inline-block; width: 47%; margin:5px">Coupan: ' . $coupanstr . '</p>';
//        $mail_body .= '<p style="color:#fff; display: inline-block; width: 47%; margin:5px">Final amount due: ' .'$'. $paid . '</p>';
//        $mail_body .= '<p style="color:#fff; ">City: ' . $datacontent[0]['var_city'] . '</p>';
//        $mail_body .= '<p style="color:#fff;">Comments: ' . $datacontent[0]['var_comment'] . '</p>';
//        $mail_body .= '<p style="color:#fff;">Sincerely,<br/><a href="'.SITE_NAME.'" style=color:#fff>'.SITE_NAME.'</a></p>';
//        $mail_body .= '<p style="color:#fff;margin-bottom:10px;font-size:9px;">Cancellation policies between <b>'.SITE_NAME.'<sup>sm</sup></b> quotes and competitor sites may vary.</p>';
//
//        $data['mail_body'] = $mail_body;
//        $message = $this->load->view('admin/account/genral_emailtemplate', $data, TRUE);
////        $message = $this->load->view('user/reservation/confirmation_page', $data, TRUE);
//        $this->email->message($message);
//        $sent = $this->email->send();

//      sleep(5);
//        $get_perent = $this->db->select('fk_parent_account,var_email,var_point,var_accountid')->get_where('dz_user', array('int_glcode' => $this->userid))->row();


        if ($get_perent->fk_parent_account != "") {

            $this->db->where('fk_user', $get_perent->fk_parent_account);
            $this->db->where('fk_replayquote', $query[0]['int_glcode']);
            $commsion = $this->db->get('dz_commision')->result_array();
            // echo $this->db->last_query();
            // print_r($commsion);
            //  echo 'ji';
            //  exit();
//            if (empty($commsion)) {
//                $data_array2 = array(
//                    'fk_user' => $get_perent->fk_parent_account,
//                    'fk_quote' => $query[0]['int_fkquote'],
//                    'fk_replayquote' => $query[0]['int_glcode'],
//                    'chr_status' => 'U',
//                    'var_actual_amont' => $paid1,
//                    'var_paid_amount' => $paid,
//                    'var_commision_amount' => $commision_percentage,
//                    'var_releasedate' => $paymentdate,
//                    'dt_created_date' => date('Y-m-d H:i:s'),
//                    'dt_update_date' => date('Y-m-d H:i:s'),
//                );
//                $result = $this->db->insert('dz_commision', $data_array2);
//            }



            if ($result) {
                $data['page'] = "user/reservation/paypalsuccess";
            } else {
                //  $data['page'] = "user/reservation/paypalerror";
                $data['page'] = "user/reservation/paypalsuccess";
            }
        } else {
            $data['page'] = "user/reservation/paypalsuccess";
        }
        // comistion count end

        $this->page_name = 'PAYMENT CONFIRMATION';
        $this->parent_menu = 'reservation';
        $data['js'] = array(
            'user/userpay.js',
            'custom/components-pickers.js',
        );
        $data['js_plugin'] = array(
            'bootstrap-datepicker/js/bootstrap-datepicker.js',
        );
        $data['css_plugin'] = array(
            'bootstrap-datepicker/css/datepicker.css'
        );
        $data['css'] = array(
        );
        $data['init'] = array(
            'Userpay.init()',
            'ComponentsPickers.init_datepicker()'
        );
        $data['transactionid'] = $query[0]['var_transactionid'];

        //------------- Confirmation Page Start (27-05-2015)----------------//
//        $this->db->select('dz_quote.*,
//            dz_rplyquote.var_site1,
//            dz_rplyquote.int_site1_price,
//            dz_rplyquote.var_site2,
//            dz_rplyquote.int_site2_price,
//            dz_rplyquote.var_tripadvisor,
//            dz_rplyquote.var_uniq_quoteid,
//            dz_rplyquote.var_policy,
//            dz_rplyquote.var_comment,
//            dz_rplyquote.var_star,
//            dz_rplyquote.var_NOH,
//            dz_rplyquote.var_prize'
//         );
//        $this->db->from('dz_quote');
//        $this->db->join('dz_rplyquote', 'dz_quote.int_glcode=dz_rplyquote.int_fkquote');
//        $this->db->where_in('dz_rplyquote.int_glcode', $replyquotid);
//        $query = $this->db->get()->result_array();
//        //print_r($query);
//        // echo json_encode($query); exit();
//        $data['datacontent'] = $query;
//        $this->db->where('fk_user', $this->userid);
//        $this->db->where('var_status', 'A');
//        $data['coupons'] = $this->db->get('dz_coupons')->result_array();
//
//        $data['replayquoteid'] = $replayquoteid;
//
//        $data['page'] = "user/reservation/confirmation_page";
//        $this->page_name = 'Payment Page';
//        $this->parent_menu = 'reservation';
//        $data['js'] = array(
//            'user/userpay.js',
//            'custom/components-pickers.js',
//        );
//        $data['js_plugin'] = array(
//            'bootstrap-datepicker/js/bootstrap-datepicker.js',
//        );
//        $data['css_plugin'] = array(
//            'bootstrap-datepicker/css/datepicker.css'
//        );
//        $data['css'] = array(
//        );
//        $data['init'] = array(
//            'Userpay.init()',
//            'ComponentsPickers.init_datepicker()'
//        );
        //------------- Confirmation Page End----------------//

        $this->db->select('dz_quote.*,
            dz_rplyquote.var_site1,
            dz_rplyquote.int_site1_price,
            dz_rplyquote.var_site2,
            dz_rplyquote.int_site2_price,
            dz_rplyquote.var_tripadvisor,
            dz_rplyquote.var_uniq_quoteid,
            dz_rplyquote.var_policy,
            dz_rplyquote.var_NOH,
            dz_rplyquote.var_room_type,
            dz_rplyquote.var_address,
            dz_rplyquote.var_comment,
            dz_rplyquote.var_star,
            dz_rplyquote.var_prize');
        $this->db->from('dz_quote');
        $this->db->join('dz_rplyquote', 'dz_quote.int_glcode=dz_rplyquote.int_fkquote');
        $this->db->where_in('dz_rplyquote.int_glcode', $id);
        $query = $this->db->get()->result_array();
        //print_r($query);
        // echo json_encode($query); exit();
        $data['datacontent'] = $query;
        $this->db->where('int_glcode', $coupanid);
        $data['coupons'] = $this->db->get('dz_coupons')->result_array();

        $data['paidamount'] = $paid;
        $data['replayquoteid'] = $replayquoteid;

        $data['refer_link'] = $this->db->select('var_accountid')->where('int_glcode', $this->userid)->get('dz_user')->row_array();
        $refer_link = $this->db->select('var_accountid')->where('int_glcode', $this->userid)->get('dz_user')->result_array();

        $url1 = base_url();
        if ($query[0]['int_site1_price'] != "" && $query[0]['int_site2_price'] != 0) {
            if ($query[0]['int_site1_price'] > $query[0]['int_site2_price']) {
                $price = floatval($query[0]['int_site2_price']) - floatval($query[0]['var_prize']);
            } else {
                $price = floatval($query[0]['int_site1_price']) - floatval($query[0]['var_prize']);
            }
        } else {
            $price = floatval($query[0]['int_site1_price']) - floatval($query[0]['var_prize']);
        }
        $data['facebookamount'] = $price;
        $data['title'] = "I just saved $" . $price . " using ".SITE_NAME.". Sign up and get $20 OFF! LINK: " . $url1 . "front/home/member_signup/" . $get_perent->var_accountid;
        $data['desc'] = "";


        $this->load->view($this->layout, $data);
//        exit();
    }

    function successpage($replyquotid, $coupanid = NULL)
    {
        $id = $replyquotid;
        $data['page'] = "user/reservation/paypalsuccess";


        $this->db->select('dz_quote.*,
            dz_rplyquote.var_site1,
            dz_rplyquote.int_site1_price,
            dz_rplyquote.var_site2,
            dz_rplyquote.int_site2_price,
            dz_rplyquote.var_tripadvisor,
            dz_rplyquote.var_uniq_quoteid,
            dz_rplyquote.var_policy,
            dz_rplyquote.var_NOH,
            dz_rplyquote.var_comment,
            dz_rplyquote.var_star,
            dz_rplyquote.var_prize');
        $this->db->from('dz_quote');
        $this->db->join('dz_rplyquote', 'dz_quote.int_glcode=dz_rplyquote.int_fkquote');
        $this->db->where_in('dz_rplyquote.int_glcode', $id);
        $query = $this->db->get()->result_array();

        $query1 = $this->db->select('var_prize')->get_where('dz_rplyquote', array('int_glcode' => $id))->row();
        $actual_amount = $query1->var_prize;
        $paid1 = preg_replace('/\D/', '', $actual_amount) / 100;

        if ($coupanid != "") {
            $paid = $this->getfinalamount1($paid1, $coupanid);
        } else {
            $paid = $paid1;
        }
        $this->db->where('int_glcode', $coupanid);
        $data['coupons'] = $this->db->get('dz_coupons')->result_array();
        //print_r($query);
        // echo json_encode($query); exit();
        $data['datacontent'] = $query;
        $this->page_name = 'PAYMENT CONFIRMATION';
        $this->parent_menu = 'reservation';
        $data['js'] = array(
            'user/userpay.js',
            'custom/components-pickers.js',
        );
        $data['js_plugin'] = array(
            'bootstrap-datepicker/js/bootstrap-datepicker.js',
        );
        $data['css_plugin'] = array(
            'bootstrap-datepicker/css/datepicker.css'
        );
        $data['css'] = array(
        );
        $data['init'] = array(
            'Userpay.init()',
            'ComponentsPickers.init_datepicker()'
        );
        $data['transactionid'] = $query[0]['var_transactionid'];
        $data['paidamount'] = $paid;
        $url1 = base_url();
        if ($query[0]['int_site1_price'] != "" && $query[0]['int_site2_price'] != 0) {
            if ($query[0]['int_site1_price'] > $query[0]['int_site2_price']) {
                $price = floatval($query[0]['int_site2_price']) - floatval($query[0]['var_prize']);
            } else {
                $price = floatval($query[0]['int_site1_price']) - floatval($query[0]['var_prize']);
            }
        } else {
            $price = floatval($query[0]['int_site1_price']) - floatval($query[0]['var_prize']);
        }
        $get_perent = $this->db->select('fk_parent_account,var_email,var_point,var_accountid')->get_where('dz_user', array('int_glcode' => $this->userid))->row();

        $data['facebookamount'] = $price;
        $data['title'] = "I just saved $" . $price . " using ".SITE_NAME.". Sign up and get $20 OFF! LINK: " . $url1 . "front/home/member_signup/" . $get_perent->var_accountid;
        $data['desc'] = "";

        $this->load->view($this->layout, $data);
    }
    function bitcoinsuccess($replyquotid, $coupanid = NULL) {
        //print_r($replyquotid);
//        print_r($this->input->post());EXIT;
        // comistion count start
        $id = $replyquotid;
        $this->db->where('int_glcode', $id);
        $query = $this->db->get('dz_rplyquote')->result_array();
        // print_r($query);
        // exit();
        if ($coupanid != "") {
            $data_array3 = array(
                'var_status' => 'U',
                //   'dt_updated_date'=>date("Y-m-d H:i:s"),
            );
            $this->db->where('int_glcode', $coupanid);
            $this->db->update('dz_coupons', $data_array3);

            $data_array = array(
                'quote_status' => 'C',
                'dt_updated_date' => date("Y-m-d H:i:s"),
                'fk_coupon' => $coupanid
            );
            $this->db->where('int_glcode', $query[0]['int_fkquote']);
            $this->db->update('dz_quote', $data_array);

            $data_array1 = array(
                'fk_coupon' => $coupanid
            );
            $this->db->where('int_glcode', $id);
            $this->db->update('dz_rplyquote', $data_array1);
        } else {
            $data_array = array(
                'quote_status' => 'C',
                'dt_updated_date' => date("Y-m-d H:i:s"),
                //  'fk_coupon' => $coupanid
            );
            $this->db->where('int_glcode', $query[0]['int_fkquote']);
            $this->db->update('dz_quote', $data_array);
        }

        $this->db->where('int_glcode', $query[0]['int_fkquote']);
        $quote = $this->db->get('dz_quote')->result_array();

        //payment relase date
        $paymentdate = date("Y-m-d", strtotime(date("Y-m-d", strtotime($quote[0]['var_checkout'])) . " +90 day"));

        $data_array1 = array(
            'chr_status' => 'P',
            'var_updateddate' => date("Y-m-d H:i:s"),
        );
        $this->db->where('int_glcode', $id);
        $this->db->update('dz_rplyquote', $data_array1);

        $query1 = $this->db->select('var_prize')->get_where('dz_rplyquote', array('int_glcode' => $id))->row();
        $actual_amount = $query1->var_prize;
        $paid1 = preg_replace('/\D/', '', $actual_amount) / 100;

        if ($coupanid != "") {
            $paid = $this->getfinalamount1($paid1, $coupanid);
        } else {
            $paid = $paid1;
        }

        // transaction id start
        if ($query[0]['var_transactionid'] == "" || $query[0]['var_transactionid'] == NULL) {
            $this->load->model('admin/quote');
            $random = $this->quote->random_num();
            $data_array1 = array(
                'var_transactionid' => $random,
            );
            $this->db->where('int_glcode', $id);
            $this->db->update('dz_rplyquote', $data_array1);
            $query[0]['var_transactionid'] = $random;

            // cout point
            $get_perent1 = $this->db->select('fk_parent_account,var_email,var_point')->get_where('dz_user', array('int_glcode' => $this->userid))->row();
            $uamount = $get_perent1->var_point + $paid;
            $pointupdate = array(
                'var_point' => $uamount,
            );
            $this->db->where('int_glcode', $this->userid);
            $this->db->update('dz_user', $pointupdate);
            // cout point
        }

        // transaction id end

        $commision_percentage = ($paid * 5) / 100;
        $commision_percentage = number_format((float) $commision_percentage, 2, '.', '');
        //   $commision_amount = $paid - $commision_percentage;
        $this->db->select('dz_quote.*,
                            dz_rplyquote.var_site1,
                            dz_rplyquote.int_site1_price,
                            dz_rplyquote.var_site2,
                            dz_rplyquote.int_site2_price,
                            dz_rplyquote.var_tripadvisor,
                            dz_rplyquote.var_uniq_quoteid,
                            dz_rplyquote.var_policy,
                            dz_rplyquote.var_comment,
                            dz_rplyquote.var_NOH,
                            dz_rplyquote.var_star,
                            dz_rplyquote.var_prize,
                            dz_rplyquote.var_transactionid');
        $this->db->from('dz_quote');
        $this->db->join('dz_rplyquote', 'dz_quote.int_glcode=dz_rplyquote.int_fkquote');

        $this->db->where_in('dz_rplyquote.int_glcode', $id);
        $datacontent = $this->db->get()->result_array();
//print_r($datacontent);exit;
        $this->db->where('int_glcode', $coupanid);
        $coupanidquery = $this->db->get('dz_coupons')->result_array();

        if ($coupanidquery[0]['var_coupan_type'] == 1) {
            $coupanstr = $coupanidquery[0]['var_couponvalue'] . '% OFF';
        }
        if ($coupanidquery[0]['var_coupan_type'] == 2) {
            $coupanstr = '$' . $coupanidquery[0]['var_couponvalue'] . 'OFF';
        }

        // automatic counter
        $this->load->model('front/paymentsuccess');
        $content = array('id' => $query[0]['var_transactionid']);

        $inserdata = $this->paymentsuccess->get_quetes($content);
//        print_r($inserdata);
//         exit();
        $dataarray = array(
            'id' => $query[0]['var_transactionid'],
            'username' => $inserdata['username'],
            'quote' => $inserdata['quote'],
            'highprice' => $inserdata['price'],
            'saveprice' => $inserdata['save'],
            'fkid' => $inserdata['fkid'],
        );
        $this->manualentry_model->insert_data($dataarray);

        //automatic counter

        $this->email->set_mailtype("html");
        $this->email->from($this->input->post('email_add'), 'User');
        $this->email->to($get_perent->var_email);
        $this->email->subject('User Contact us');
        $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Dear Member</p>';
        $mail_body .= '<p style="color:#fff;">Your booking is successfully.Follwing is detail for it.</p>';
        $mail_body .= '<p style="color:#fff;">Check-in: ' . $datacontent[0]['var_checkin'] . '</p>';
        $mail_body .= '<p style="color:#fff;">Check-out: ' . $datacontent[0]['var_checkout'] . '</p>';
        $mail_body .= '<p style="color:#fff;">City: ' . $datacontent[0]['var_city'] . '</p>';
        $mail_body .= '<p style="color:#fff;">QUOTE ID: ' . $datacontent[0]['var_uniq_quoteid'] . '</p>';
        $mail_body .= '<p style="color:#fff;">Name of hotel: ' . $datacontent[0]['var_NOH'] . '</p>';
        $mail_body .= '<p style="color:#fff;">Stars: ' . $datacontent[0]['var_star'] . '</p>';
        $mail_body .= '<p style="color:#fff;">TripAdvisor: ' . $datacontent[0]['var_tripadvisor'] . '</p>';
        $mail_body .= '<p style="color:#fff;">Rooms: ' . $datacontent[0]['var_room'] . '</p>';
        $mail_body .= '<p style="color:#fff;">Adults: ' . $datacontent[0]['var_adult'] . '</p>';
        $mail_body .= '<p style="color:#fff;">Children: ' . $datacontent[0]['var_child'] . '</p>';
        $mail_body .= '<p style="color:#fff;">Cancellation policy: ' . $datacontent[0]['var_policy'] . '</p>';
        $mail_body .= '<p style="color:#fff;">Comments: ' . $datacontent[0]['var_comment'] . '</p>';
        $mail_body .= '<p style="color:#fff;">Current Price: ' . $datacontent[0]['var_prize'] . '</p>';
        $mail_body .= '<p style="color:#fff;">Coupan: ' . $coupanstr . '</p>';
        $mail_body .= '<p style="color:#fff;">Final amount due: ' . $paid . '</p>';
        $mail_body .= '<p style="color:#fff;">Sincerely,</br>User</p>';
        $mail_body .= '<p style="color:#fff;margin-bottom:10px;font-size:9px;">Cancellation policies between <b>'.SITE_NAME.'<sup>sm</sup></b> quotes and competitor sites may vary.</p>';

        $data['mail_body'] = $mail_body;
        $message = $this->load->view('admin/account/genral_emailtemplate', $data, TRUE);
//        $message = $this->load->view('user/reservation/confirmation_page', $data, TRUE);
        $this->email->message($message);
        $sent = $this->email->send();

        $get_perent = $this->db->select('fk_parent_account,var_email,var_point,var_accountid')->get_where('dz_user', array('int_glcode' => $this->userid))->row();


        if ($get_perent->fk_parent_account != "") {

            $this->db->where('fk_user', $get_perent->fk_parent_account);
            $this->db->where('fk_replayquote', $query[0]['int_glcode']);
            $commsion = $this->db->get('dz_commision')->result_array();
            // echo $this->db->last_query();
            // print_r($commsion);
            //  echo 'ji';
            //  exit();
            if (empty($commsion)) {
                $data_array2 = array(
                    'fk_user' => $get_perent->fk_parent_account,
                    'fk_quote' => $query[0]['int_fkquote'],
                    'fk_replayquote' => $query[0]['int_glcode'],
                    'chr_status' => 'U',
                    'var_actual_amont' => $paid1,
                    'var_paid_amount' => $paid,
                    'var_commision_amount' => $commision_percentage,
                    'var_releasedate' => $paymentdate,
                    'dt_created_date' => date('Y-m-d H:i:s'),
                    'dt_update_date' => date('Y-m-d H:i:s'),
                );
                $result = $this->db->insert('dz_commision', $data_array2);
            }



            if ($result) {
                $data['page'] = "user/reservation/bitcoinsuccess";
            } else {
                //  $data['page'] = "user/reservation/paypalerror";
                $data['page'] = "user/reservation/bitcoinsuccess";
            }
        } else {
            $data['page'] = "user/reservation/bitcoinsuccess";
        }
        // comistion count end

        $this->page_name = 'PAYMENT CONFIRMATION';
        $this->parent_menu = 'reservation';
        $data['js'] = array(
            'user/userpay.js',
            'custom/components-pickers.js',
        );
        $data['js_plugin'] = array(
            'bootstrap-datepicker/js/bootstrap-datepicker.js',
        );
        $data['css_plugin'] = array(
            'bootstrap-datepicker/css/datepicker.css'
        );
        $data['css'] = array(
        );
        $data['init'] = array(
            'Userpay.init()',
            'ComponentsPickers.init_datepicker()'
        );
        $data['transactionid'] = $query[0]['var_transactionid'];

        //------------- Confirmation Page Start (27-05-2015)----------------//
//        $this->db->select('dz_quote.*,
//            dz_rplyquote.var_site1,
//            dz_rplyquote.int_site1_price,
//            dz_rplyquote.var_site2,
//            dz_rplyquote.int_site2_price,
//            dz_rplyquote.var_tripadvisor,
//            dz_rplyquote.var_uniq_quoteid,
//            dz_rplyquote.var_policy,
//            dz_rplyquote.var_comment,
//            dz_rplyquote.var_star,
//            dz_rplyquote.var_NOH,
//            dz_rplyquote.var_prize'
//         );
//        $this->db->from('dz_quote');
//        $this->db->join('dz_rplyquote', 'dz_quote.int_glcode=dz_rplyquote.int_fkquote');
//        $this->db->where_in('dz_rplyquote.int_glcode', $replyquotid);
//        $query = $this->db->get()->result_array();
//        //print_r($query);
//        // echo json_encode($query); exit();
//        $data['datacontent'] = $query;
//        $this->db->where('fk_user', $this->userid);
//        $this->db->where('var_status', 'A');
//        $data['coupons'] = $this->db->get('dz_coupons')->result_array();
//
//        $data['replayquoteid'] = $replayquoteid;
//
//        $data['page'] = "user/reservation/confirmation_page";
//        $this->page_name = 'Payment Page';
//        $this->parent_menu = 'reservation';
//        $data['js'] = array(
//            'user/userpay.js',
//            'custom/components-pickers.js',
//        );
//        $data['js_plugin'] = array(
//            'bootstrap-datepicker/js/bootstrap-datepicker.js',
//        );
//        $data['css_plugin'] = array(
//            'bootstrap-datepicker/css/datepicker.css'
//        );
//        $data['css'] = array(
//        );
//        $data['init'] = array(
//            'Userpay.init()',
//            'ComponentsPickers.init_datepicker()'
//        );
        //------------- Confirmation Page End----------------//

        $this->db->select('dz_quote.*,
            dz_rplyquote.var_site1,
            dz_rplyquote.int_site1_price,
            dz_rplyquote.var_site2,
            dz_rplyquote.int_site2_price,
            dz_rplyquote.var_tripadvisor,
            dz_rplyquote.var_uniq_quoteid,
            dz_rplyquote.var_policy,
            dz_rplyquote.var_NOH,
            dz_rplyquote.var_comment,
            dz_rplyquote.var_star,
            dz_rplyquote.var_prize');
        $this->db->from('dz_quote');
        $this->db->join('dz_rplyquote', 'dz_quote.int_glcode=dz_rplyquote.int_fkquote');
        $this->db->where_in('dz_rplyquote.int_glcode', $id);
        $query = $this->db->get()->result_array();
        //print_r($query);
        // echo json_encode($query); exit();
        $data['datacontent'] = $query;
        $this->db->where('int_glcode', $coupanid);
        $data['coupons'] = $this->db->get('dz_coupons')->result_array();

        $data['paidamount'] = $paid;
        $data['replayquoteid'] = $replayquoteid;

        $data['refer_link'] = $this->db->select('var_accountid')->where('int_glcode', $this->userid)->get('dz_user')->row_array();
        $refer_link = $this->db->select('var_accountid')->where('int_glcode', $this->userid)->get('dz_user')->result_array();
        $url1 = base_url();
        if ($query[0]['int_site1_price'] != "" && $query[0]['int_site2_price'] != 0) {
            if ($query[0]['int_site1_price'] > $query[0]['int_site2_price']) {
                $price = floatval($query[0]['int_site2_price']) - floatval($query[0]['var_prize']);
            } else {
                $price = floatval($query[0]['int_site1_price']) - floatval($query[0]['var_prize']);
            }
        } else {
            $price = floatval($query[0]['int_site1_price']) - floatval($query[0]['var_prize']);
        }
        $data['facebookamount'] = $price;
        $data['title'] = "I just saved $" . $price . " using ".SITE_NAME.". Sign up and get $20 OFF! LINK: " . $url1 . "front/home/member_signup/" . $get_perent->var_accountid;
        $data['desc'] = "";

        $this->load->view($this->layout, $data);
    }

    function getfinalamount1($paid1, $coupanid) {
        //  if($this->input->post()){
        $this->db->select('*');
        $this->db->where('int_glcode', $coupanid);
        $coupan = $this->db->get('dz_coupons')->result_array();

        if ($coupan[0]['var_coupan_type'] == 1) {
            // echo $coupan[0]['var_couponvalue'] - $this->input->post('price');
            $discoutamount = floatval(($coupan[0]['var_couponvalue'] * $paid1) / 100);
            $discoutamount = number_format((float) $discoutamount, 2, '.', '');
            $acutelamount = $paid1 - $discoutamount;
            $acutelamount = number_format((float) $acutelamount, 2, '.', '');
            return $acutelamount;
        }
        if ($coupan[0]['var_coupan_type'] == 2) {
            $acutelamount = $paid1 - $coupan[0]['var_couponvalue'];
            $acutelamount = number_format((float) $acutelamount, 2, '.', '');
            return $acutelamount;
        }
        //  }
    }

    function cansel_reservation() {
        if ($this->input->post()) {
            $id = $this->input->post('id');
            date_default_timezone_set(AMERICA_LOS_ANGLES_TIME_ZONE);
            $array = array('chr_status' => 'C', 'date_usercanceltime' => date('Y-m-d H:i:s'));
            $this->db->where('int_glcode', $id);
            $update = $this->db->update('dz_rplyquote', $array);

            $this->paymentsuccess->createcharjback($id);

            $this->db->where('int_glcode', $id);
            $result = $this->db->get('dz_rplyquote')->result_array();

            $this->email->set_mailtype("html");
            $this->email->from(NOREPLY, NOREPLY);
            $this->email->to($this->session->userdata['var_email']);
            $this->email->subject('Cancel Reservation');
            $mail_body = '<p style="color:#fff;margin-bottom:10px;">Hello User</p>';
            $mail_body .='<p style="color:#fff;margin-bottom:10px;">Thank you for using <strong>'.SITE_NAME.'<sup>sm</sup></strong>, Take the hotel challenge!. We are now confirming your cancellation request for confirmation number:' . $result[0]['var_uniq_quoteid'] . '. Your reservation is fully refundable and your refund will be processed within 5-7 business days. Please note that every credit card company processes refunds differently; if you have any questions regarding to their practice, please be kind and contact the issuer of the credit card you used for your reservation.
Again, thank you for choosing Us and please always give Us a try before booking your next trip!</p>';
            $mail_body .='<p style="color:#fff;margin-bottom:10px;">Sincerely,<br></p>';
            $mail_body .='<p style="color:#fff;margin-bottom:10px;">'.SITE_NAME.'<sup>sm</sup><br></p>';
            $mail_body .='<p style="color:#fff;margin-bottom:10px;">Take the hotel challenge!</p>';
//            $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Cancellation policies between <b>'.SITE_NAME.'<sup>sm</sup></b> quotes and competitor sites may vary.</p>';
            $data['mail_body'] = $mail_body;
            $message = $this->load->view('admin/account/genral_emailtemplate', $data, TRUE);
            $this->email->message($message);
            $this->email->send();
            if ($update) {
                echo 'success';
                exit;
            } else {
                echo 'error';
                exit;
            }
        }
    }

    function getcancletionpolicy() {
        $value = $this->input->post('id');
        $this->db->select('dz_quote.*,
            dz_rplyquote.var_site1,
            dz_rplyquote.int_site1_price,
            dz_rplyquote.var_site2,
            dz_rplyquote.int_site2_price,
            dz_rplyquote.var_NOH,
            dz_rplyquote.var_tripadvisor,
            dz_rplyquote.var_uniq_quoteid,
            dz_rplyquote.var_transactionid,
            dz_rplyquote.var_policy,
            dz_rplyquote.var_comment,
            dz_rplyquote.var_star,
            dz_rplyquote.var_NOH,
            dz_rplyquote.var_src,
            dz_rplyquote.int_src_price,
            dz_rplyquote.var_prize');
        $this->db->from('dz_quote');
        $this->db->join('dz_rplyquote', 'dz_quote.int_glcode=dz_rplyquote.int_fkquote');
        $this->db->where('dz_rplyquote.int_glcode', $value);
        $query = $this->db->get()->result_array();
//        print_r($query);exit;
        echo json_encode($query);
        exit();
    }

    function payment_way() {        //print_r($this->input->post());exit;
        if ($this->input->post('filter')) {
            if ($this->input->post('filter') == 'paypal') {
                $this->paymentsuccess->paypal($this->input->post());
            } elseif ($this->input->post('filter') == 'credit_debit') {

            } elseif ($this->input->post('filter') == 'cash') {

            } elseif ($this->input->post('filter') == 'bitcon') {
                //redirect().$this->input->post('payment_bitcoin');
            }
        }
    }

    function userpay1($replayquoteid) {        //print_r($replayquoteid);EXIT;
        $data['page'] = "user/reservation/userpay1";
//        $data['page'] = "user/reservation/confirmation_page";
        $this->page_name = 'Payment Page';
        $this->parent_menu = 'reservation';
        $data['js'] = array(
            'user/userpay.js',
            'custom/components-pickers.js',
        );
        $data['js_plugin'] = array(
            'bootstrap-datepicker/js/bootstrap-datepicker.js',
        );
        $data['css_plugin'] = array(
            'bootstrap-datepicker/css/datepicker.css'
        );
        $data['css'] = array(
        );
        $data['init'] = array(
            'Userpay.init()',
            'ComponentsPickers.init_datepicker()'
        );

        $this->db->select('dz_quote.*,
            dz_rplyquote.var_site1,
            dz_rplyquote.int_site1_price,
            dz_rplyquote.var_site2,
            dz_rplyquote.int_site2_price,
            dz_rplyquote.var_tripadvisor,
            dz_rplyquote.var_uniq_quoteid,
            dz_rplyquote.var_policy,
            dz_rplyquote.var_comment,
            dz_rplyquote.var_star,
            dz_rplyquote.var_NOH,
            dz_rplyquote.var_prize'
        );
        $this->db->from('dz_quote');
        $this->db->join('dz_rplyquote', 'dz_quote.int_glcode=dz_rplyquote.int_fkquote');
        $this->db->where_in('dz_rplyquote.int_glcode', $replayquoteid);
        $query = $this->db->get()->result_array();
        //print_r($query);
        // echo json_encode($query); exit();
        $data['datacontent'] = $query;
        $this->db->where('fk_user', $this->userid);
        $this->db->where('var_status', 'A');
        $data['coupons'] = $this->db->get('dz_coupons')->result_array();

        $data['replayquoteid'] = $replayquoteid;
        $data['refer_link'] = $this->db->select('var_accountid')->where('int_glcode', $this->userid)->get('dz_user')->row_array();
        $this->load->view($this->layout, $data);
    }

}
?>