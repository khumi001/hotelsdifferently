<?php

class Testpaypal extends CI_Controller {

    function __construct() {

        parent::__construct();
    }

    function index()
    {
                 //print_r($replayquoteid);EXIT;
        $data['page'] = "user/myaccount/userpaysss";
//        $data['page'] = "user/reservation/confirmation_page";
        $this->page_name = 'Payment Page';
        $this->parent_menu = 'reservation';
        $data['js'] = array(
            'user/userpay.js',
            'custom/components-pickers.js',
        );
        $data['js_plugin'] = array(
            'bootstrap-datepicker/js/bootstrap-datepicker.js',
        );
        $data['css_plugin'] = array(
            'bootstrap-datepicker/css/datepicker.css'
        );
        $data['css'] = array(
        );
        $data['init'] = array(
            'Userpay.init()',
            'ComponentsPickers.init_datepicker()'
        );

        $this->db->select('dz_quote.*,
            dz_rplyquote.var_site1,
            dz_rplyquote.int_site1_price,
            dz_rplyquote.var_site2,
            dz_rplyquote.int_site2_price,
            dz_rplyquote.var_tripadvisor,
            dz_rplyquote.var_uniq_quoteid,
            dz_rplyquote.var_policy,
            dz_rplyquote.var_comment,
            dz_rplyquote.var_star,
            dz_rplyquote.var_NOH,
            dz_rplyquote.var_prize'
         );
        $this->db->from('dz_quote');
        $this->db->join('dz_rplyquote', 'dz_quote.int_glcode=dz_rplyquote.int_fkquote');
        $this->db->where_in('dz_rplyquote.int_glcode', $replayquoteid);
        $query = $this->db->get()->result_array();
        //print_r($query);
        // echo json_encode($query); exit();
        $data['datacontent'] = $query;
        $this->db->where('fk_user', $this->userid);
        $this->db->where('var_status', 'A');
        $data['coupons'] = $this->db->get('dz_coupons')->result_array();

        $data['replayquoteid'] = $replayquoteid;
        $data['refer_link'] = $this->db->select('var_accountid')->where('int_glcode', $this->userid)->get('dz_user')->row_array();
        $this->load->view(USER_LAYOUT, $data);
    }

   
}

?>