<?php

class Myaccount extends User_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('Datatables');
        date_default_timezone_set(AMERICA_LOS_ANGLES_TIME_ZONE);
    }

    function index() {
        
    }

    function info() {

        $data['page'] = "user/myaccount/info";
        $this->page_name = 'My Info';
        $this->parent_menu = 'myaccount';
        $data['js'] = array(
            'custom/components-pickers.js',
            'user/my_account.js'
        );
        $data['js_plugin'] = array(
            'jquery-validation/dist/jquery.validate.min.js',
            'jquery-validation/dist/additional-methods.min.js',
            'bootstrap-fileinput/bootstrap-fileinput.js',
            'bootstrap-datepicker/js/bootstrap-datepicker.js',
            'bootstrap-select/bootstrap-select.min.js',
            'select2/select2.min.js',
            'bootstrap-maxlength/bootstrap-maxlength.min.js'
        );
        $data['css_plugin'] = array(
            'bootstrap-fileinput/bootstrap-fileinput.css',
            'bootstrap-datepicker/css/datepicker.css',
            'bootstrap-select/bootstrap-select.min.css',
            'select2/select2.css',
            'select2/select2-metronic.css',
        );
        $data['init'] = array(
            'My_account.init()',
            'ComponentsPickers.init_datepicker()'
        );
        $data['edit_data'] = $this->db->where('int_glcode', $this->userid)->get('dz_user')->row_array();
        if ($this->input->post()) {

            $arr = array(
                'var_fname' => $this->input->post('fname'),
                'var_lname' => $this->input->post('lname'),
                'var_phone' => $this->input->post('phone1') . '-' . $this->input->post('phone2') . '-' . $this->input->post('phone3'),
                'var_country' => $this->input->post('country'),
//                'var_newsletter' => $this->input->post('newsletter'),
            );

            $this->db->where('int_glcode', $this->userid);
            $query = $this->db->update('dz_user', $arr);
            if ($query) {
                echo "success";
                exit;
            } else {
                echo "error";
                exit;
            }
        }

        $this->load->view(USER_LAYOUT, $data);
    }

    function coupons() {

        $data['page'] = "user/myaccount/coupons";
        $this->page_name = 'My Coupons';
        $this->parent_menu = 'myaccount';
        $data['js'] = array(
            'user/coupons.js',
            'custom/components-pickers.js',
        );
        $data['js_plugin'] = array(
            'bootstrap-datepicker/js/bootstrap-datepicker.js',
        );
        $data['css_plugin'] = array(
            'uniform/css/uniform.default.css',
            'bootstrap-datepicker/css/datepicker.css'
        );
        $data['css'] = array(
        );
        $data['init'] = array(
            'Coupons.init()',
            'ComponentsPickers.init_datepicker()'
        );
        $this->load->view(USER_LAYOUT, $data);
    }

    function refer_friend() {
        
       
        $data['page'] = "user/myaccount/refer_friend";
        $this->page_name = 'REFER A FRIEND';
        $this->parent_menu = 'myaccount';
        $data['js'] = array(
            
            'user/refer_friend.js',
            'custom/components-pickers.js',
        );
        $data['contactimpoter'] = 'yes';
        $data['js_plugin'] = array(
            'bootstrap-datepicker/js/bootstrap-datepicker.js',
        );
        $data['css_plugin'] = array(
            'bootstrap-datepicker/css/datepicker.css'
        );
        $data['css'] = array(
        );
        $data['init'] = array(
            'Refer_friend.init()',
            'ComponentsPickers.init_datepicker()'
        );
        $data['refer_link'] = $this->db->select('var_accountid')->where('int_glcode', $this->userid)->get('dz_user')->row_array();
        $refer_link = $this->db->select('var_accountid')->where('int_glcode', $this->userid)->get('dz_user')->result_array();
        $url1 = base_url();
        $data['title'] = "Try this to get the CHEAPEST HOTEL DEALS! Get $20 OFF for your booking as a bonus: http://demo.webeet.net/dealz/front/home/member_signup/" . $refer_link[0]['var_accountid'];
        //  $data['desc'] = "Get a $20 OFF coupon for your booking as a sign up bonus if you use my link: ".$url1."front/home/member_signup/".$refer_link[0]['var_accountid'];
        
        if ($this->input->post()) {
           
            //print_r($post); exit;
            $emails = $this->input->post('referemail');
            $receivername = explode('@',$emails);
//            $emails = explode(',',$this->input->post('referemail'));
            
//            print_r($emails);
//            exit;
            
//            for($i=0;$i<count($emails);$i++){
//                $to[] =  $emails[$i];
//            }
//            $email1 = implode(',',$to);;
//            print_r($emails);
//            exit;
            $id = $this->session->userdata['valid_user']['id'];
            $this->db->where('int_glcode', $id);
            $query = $this->db->get('dz_user');
            $userdetail = $query->result_array();
            
            $accountid = $refer_link[0]['var_accountid'];
            $url = base_url() . 'front/home/member_signup/' . $accountid;
//            $email = $this->input->post('referemail');
            
            $this->email->set_mailtype("html");
            $this->email->from(NOREPLY, 'Referfriend');
            $this->email->reply_to($this->session->userdata['valid_user']['var_email'], 'Referfriend');
            $this->email->to($emails);
            $this->email->subject('BIG SAVINGS AND $20 SIGNUP COUPON FOR YOU!');
            
            $mail_body .= "<p style='color:#fff;margin-bottom:10px;'>Dear ".$receivername[0].",</p>";
            $mail_body .= "<p style='color:#fff;margin-bottom:10px;'>I just found this website where you can get the BEST HOTEL DEALS! You can get $20 OFF on your first booking as a signup bonus: <a style='color:#fff;' href=". $url . ">" . $url . "</a></p>";
//            $mail_body = '<p style="color:#fff;margin-bottom:10px;">Hello Your referral link is:<a href=' . $url . '>' . $url . '</a></p>';
            $mail_body .='<p style="color:#fff;margin-bottom:10px;">'.$userdetail[0]['var_fname'].'</p>';
            $data['mail_body'] = $mail_body;
            $message = $this->load->view('admin/account/genral_emailtemplate', $data, TRUE);
            $this->email->message($message);
            $mail1 = $this->email->send();
           
            if ($mail1) {
                echo "success";
                exit;
            } else {
                echo "error";
                exit;
            }
        }

        $this->load->view(USER_LAYOUT, $data);
    }

    function coupons_datatable($status) {

        // for update coupan status
        $content = array(
            'var_status' => 'E',
        );
        $this->db->where('var_status', 'A');
        $this->db->where('var_couponexpi <', date('Y-m-d'));
        $row = $this->db->update('dz_coupons', $content);
        // for update coupan status
        if ($status != '') {
            $where = $this->datatables->where('var_status', $status);
        } else {
            $where = '';
        }
        $this->datatables
                ->select('
                         dz_coupons.var_created_date,
                         dz_coupons.var_couponcode,
                         dz_coupons.var_couponvalue,
                         dz_coupons.var_couponexpi,
                         dz_coupons.var_status,
                         dz_coupons.var_coupan_type
                        ')
                ->from('dz_coupons')
                ->where_in('var_status', array('A', 'D', 'E', 'U'))
                ->where('dz_coupons.fk_user', $this->userid);
        $where;
        $res = $this->datatables->generate();
        $results = json_decode($res, true);
        // print_r($results);
        //  exit();
        $count = count($results['aaData']);
        for ($i = 0; $i < $count; $i++) {
            if ($results['aaData'][$i][4] == 'A') {
                $results['aaData'][$i][4] = "Active";
            } else if ($results['aaData'][$i][4] == 'E') {
                $results['aaData'][$i][4] = "Expired";
            } else if ($results['aaData'][$i][4] == 'U') {
                $results['aaData'][$i][4] = "Used";
            } else {
                $results['aaData'][$i][4] = "Deactive";
            }

            if ($results['aaData'][$i][5] == 1) {
                $results['aaData'][$i][2] = $results['aaData'][$i][2] . "%";
            } else {
                $results['aaData'][$i][2] = "$" . $results['aaData'][$i][2];
            }
        }
        echo json_encode($results);
    }

    function refer_friend_datatable() {
        if ($_POST['from'] && $_POST['to']) {
            if ($_POST['from'] == $_POST['to']) {
                $this->datatables->like('dz_quote.dt_created_date', date('Y-m-d', strtotime($_POST['from'])), 'both');
            } else {
                $this->datatables->where('dz_quote.dt_created_date >=', date('Y-m-d', strtotime($_POST['from'])));
                $this->datatables->where('dz_quote.dt_created_date <=', date('Y-m-d', strtotime($_POST['to'])));
            }
        }
        $this->datatables
                ->select('
                         dz_user.var_accountid,
                         dz_quote.dt_created_date,                        
                         dz_commision.var_paid_amount,                        
                         dz_commision.var_commision_amount,                        
                         dz_quote.var_comments,                        
                         dz_commision.var_releasedate,
                         dz_rplyquote.chr_status
                        ')
                ->from('dz_commision')
                ->join('dz_user', 'dz_commision.fk_user = dz_user.int_glcode')
                ->join('dz_quote', 'dz_commision.fk_quote = dz_quote.int_glcode')
                ->join('dz_rplyquote', 'dz_commision.fk_replayquote = dz_rplyquote.int_glcode')
               // ->where('dz_rplyquote.chr_status !=', 'C')
                ->where('dz_user.int_glcode', $this->session->userdata['valid_user']['id']);

        $res = $this->datatables->generate();

        $results = json_decode($res, true);
        $count = count($results['aaData']);
        for ($i = 0; $i < $count; $i++) {
            $accountid = $results['aaData'][$i][0];
            $bookingdate = $results['aaData'][$i][1];
            $amount = $results['aaData'][$i][2];
            $commission = $results['aaData'][$i][3];
            $note = $results['aaData'][$i][4];
            $relesedate = $results['aaData'][$i][5];
          //  $status = $results['aaData'][$i][6];
            if($results['aaData'][$i][6] == "C")
            {
                $note = "Cancelled Resarvation!<span class='linethrow' style='display:none;'>".$commission."</span><span class='linethrow1' style='display:none;'>".$amount."</span>";
            }
            $results['aaData'][$i][0] = $accountid;
            $results['aaData'][$i][1] = date("Y-m-d / H:i", strtotime($bookingdate));
            $results['aaData'][$i][2] = "$" . (number_format($amount, 2));
            $results['aaData'][$i][3] = "$" . (number_format($commission, 2));
            $results['aaData'][$i][4] = $note;
            $results['aaData'][$i][5] = date("Y-m-d", strtotime($relesedate));
        }
        echo json_encode($results);
    }

    function change_pass() {

        $data['page'] = "user/myaccount/change_pass";
        $this->page_name = 'Change Password';
        $this->parent_menu = 'myaccount';
        $data['js'] = array(
            'user/my_account.js'
        );
//        $data['js'] = array(
//            'custom/form-wizard.js',
//            'custom/components-pickers.js',
//        );
        $data['js_plugin'] = array(
            'jquery-validation/dist/jquery.validate.min.js',
            'jquery-validation/dist/additional-methods.min.js',
            'bootstrap-wizard/jquery.bootstrap.wizard.min.js',
            'select2/select2.min.js',
            'bootstrap-datepicker/js/bootstrap-datepicker.js',
            'bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',
            'bootstrap-maxlength/bootstrap-maxlength.min.js'
        );
        $data['css_plugin'] = array(
            'select2/select2-metronic.css',
            'select2/select2.css',
        );
        $data['init'] = array(
            'My_account.init()',
        );
        if ($this->input->post()) {
            $oldpass = $this->input->post('old_pass');
            // exit();
            $id = $this->session->userdata['valid_user']['id'];
            $this->db->where('int_glcode', $id);
            $this->db->where('var_password', base64_encode($oldpass));
            $query = $this->db->get('dz_user');

            if ($query->num_rows() == 1) {
                $content = array(
                    'var_password' => base64_encode($this->input->post('new_pass')),
                );
                $this->db->where('int_glcode', $id);
                $row = $this->db->update('dz_user', $content);
            }

            if ($row) {
                echo "success";
            } else {
                echo "error";
            }
            exit;
        }
        $this->load->view(USER_LAYOUT, $data);
    }

    function change_email() {

        $data['page'] = "user/myaccount/change_email";
        $this->page_name = 'Change Email Address';
        $this->parent_menu = 'myaccount';
        $data['js'] = array(
            'user/my_account.js'
        );
        $data['js_plugin'] = array(
        );
        $data['css_plugin'] = array(
        );
        $data['css'] = array(
        );
        $data['init'] = array(
            'My_account.init()',
        );
        if ($this->input->post()) {
            $curremail = $this->input->post('email');
            $id = $this->session->userdata['valid_user']['id'];

            $this->db->where('int_glcode', $id);
            $this->db->where('var_email', $curremail);
            $query = $this->db->get('dz_user')->num_rows();

            $this->db->where('var_email', $this->input->post('new_email'));
            $query1 = $this->db->get('dz_user')->num_rows();

            if ($query == 1 && $query1 == 0) {

                $confirm = array(
                    'fk_user' => $id,
                    'var_email' => $this->input->post('confirm_email'),
                    'chr_status' => 'A',
                    'dt_created_date' => date('Y-m-d H:i:s'),
                    'dt_updated_date' => date('Y-m-d H:i:s'),
                );
                $row = $this->db->insert('dz_confirm_email', $confirm);
                $last_id = $this->db->insert_id();

                $id = base64_encode($last_id);
                $confirm_email = base64_encode($this->input->post('email'));
                $link = base_url() . 'user/confirm/email/' . $confirm_email . '/' . $id;
                $this->db->where('int_glcode', $id);
                $this->db->where('var_email', $curremail);
                $query2 = $this->db->get('dz_user');
                $name = $query2->result_array();
                $this->load->library('email');
                $this->email->set_mailtype("html");
                $this->email->from('abhishekdesai91@gmail.com', 'Admin');
                $this->email->to($this->input->post('confirm_email'));
                $this->email->subject('Confirmation Link');

                $mail_body = '<p style="color:#fff;margin-bottom:10px;">Helllo ' . $name[0]['var_fname'] . '</p>';
                $mail_body .='<p style="color:#fff;margin-bottom:10px;">Here is Your Confirmation Link.</p>';
                $mail_body .='<p style="color:#fff;margin-bottom:10px;">Confirmation Link :"' . $link . '"</p>';
                $mail_body .='<p style="color:#fff;margin-bottom:10px;">Please Click on Confirmation link to activate your new email address .</p>';
                $mail_body .='<p style="color:#fff;margin-bottom:10px;">Regard,</br>Admin</p>';

                $data['mail_body'] = $mail_body;
                $message = $this->load->view('admin/account/genral_emailtemplate', $data, TRUE);
                $this->email->message($message);
                $this->email->send();
                if ($row) {
                    echo "success";
                    return $confirm_email;
                } else {
                    echo "error";
                }
            } else {
                echo "exits";
            }
            exit();
        }
        $this->load->view(USER_LAYOUT, $data);
    }
    
    function oauth()
    {
     
        $data['contactimpoter'] = 'yes';
        
        $this->load->view('user/myaccount/oauth', $data);   
    }
    
}

?>