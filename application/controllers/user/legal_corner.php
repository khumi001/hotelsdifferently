<?php

class Legal_corner extends CI_Controller {

    function __construct() {

        parent::__construct();
    }

    function index() {
        
    }
	
	function travelinsurance()
	{
		$data['var_meta_title'] = 'Wholesale Hotels Group – Great travel deals for Corporate Travel and Individuals';
        $data['var_meta_description'] = 'Get WHOLESALE HOTEL inventories and deals worldwide for Corporate Travel, Non-Profit travel, Government travel, Travel Agents and Individual users.';
        $data['var_meta_keyword'] = 'Wholesale Hotels Group, WHotelsGroup.com, wholesale hotel deals, corporate travel, government travel, non-profit travel, travel agency travel, travel agency hotels, wholesale hotels, Online Travel Agency, best OTAs';
		$data['page'] = "user/legal_corner/travelinsurance";
        $this->page_name = 'Travel Insurance';
        //$this->parent_menu = 'legalcorner';
        $data['js'] = array(
        );
        $data['js_plugin'] = array(
        );
        $data['css_plugin'] = array(
        );
        $data['css'] = array(
        );
        $data['init'] = array(
        );
        $this->load->view(FRONT_LAYOUT, $data);
	}
	
	function event_tickets()
	{
        $data['var_meta_title'] = 'Wholesale Hotels Group – Great travel deals for Corporate Travel and Individuals';
        $data['var_meta_description'] = 'Get WHOLESALE HOTEL inventories and deals worldwide for Corporate Travel, Non-Profit travel, Government travel, Travel Agents and Individual users.';
        $data['var_meta_keyword'] = 'Wholesale Hotels Group, WHotelsGroup.com, wholesale hotel deals, corporate travel, government travel, non-profit travel, travel agency travel, travel agency hotels, wholesale hotels, Online Travel Agency, best OTAs';
		$data['page'] = "user/legal_corner/event_tickets";
        $this->page_name = 'EVENT TICKETS';
        //$this->parent_menu = 'legalcorner';
        $data['js'] = array(
        );
        $data['js_plugin'] = array(
        );
        $data['css_plugin'] = array(
        );
        $data['css'] = array(
        );
        $data['init'] = array(
        );
        $this->load->view(FRONT_LAYOUT, $data);
	}
	
	function coupons()
	{
        $data['var_meta_title'] = 'Wholesale Hotels Group – Great travel deals for Corporate Travel and Individuals';
        $data['var_meta_description'] = 'Get WHOLESALE HOTEL inventories and deals worldwide for Corporate Travel, Non-Profit travel, Government travel, Travel Agents and Individual users.';
        $data['var_meta_keyword'] = 'Wholesale Hotels Group, WHotelsGroup.com, wholesale hotel deals, corporate travel, government travel, non-profit travel, travel agency travel, travel agency hotels, wholesale hotels, Online Travel Agency, best OTAs';
		$data['page'] = "user/legal_corner/coupons";
        $this->page_name = 'Coupons';
        //$this->parent_menu = 'legalcorner';
        $data['js'] = array(
        );
        $data['js_plugin'] = array(
        );
        $data['css_plugin'] = array(
        );
        $data['css'] = array(
        );
        $data['init'] = array(
        );
        $this->load->view(FRONT_LAYOUT, $data);
	}
	
	/*function ourfuture()
	{
		$data['var_meta_title'] = 'Cheap Airline Tickets, Hotels & Travel Deals | HotelsDifferently.com';
        $data['var_meta_description'] = 'Learn a few exciting updates about our site to get amazing travel deals online. Cheapest airline tickets and hotel accommodations at lowest price!';
        $data['var_meta_keyword'] = 'ourfuture';
		$data['page'] = "user/legal_corner/ourfuture";
        $this->page_name = 'Our Future';
        //$this->parent_menu = 'legalcorner';
        $data['js'] = array(
        );
        $data['js_plugin'] = array(
        );
        $data['css_plugin'] = array(
        );
        $data['css'] = array(
        );
        $data['init'] = array(
        );
        $this->load->view(FRONT_LAYOUT, $data);
	}*/

    function terms_condition() {

        $data['page'] = "user/legal_corner/terms_condition";
        $this->page_name = 'Terms and Conditions';
        $this->parent_menu = 'legalcorner';
        $data['js'] = array(
        );
        $data['js_plugin'] = array(
        );
        $data['css_plugin'] = array(
        );
        $data['css'] = array(
        );
        $data['init'] = array(
        );
        $this->load->view(FRONT_LAYOUT, $data);
    }

    function faqs() 
	{
	
		$query = $this->db->get('dz_faqs')->result_array();
        	$data['faqdata'] = $query;

        $data['var_meta_title'] = 'Wholesale Hotels Group – Great travel deals for Corporate Travel and Individuals';
        $data['var_meta_description'] = 'Get WHOLESALE HOTEL inventories and deals worldwide for Corporate Travel, Non-Profit travel, Government travel, Travel Agents and Individual users.';
        $data['var_meta_keyword'] = 'Wholesale Hotels Group, WHotelsGroup.com, wholesale hotel deals, corporate travel, government travel, non-profit travel, travel agency travel, travel agency hotels, wholesale hotels, Online Travel Agency, best OTAs';
        $data['page'] = "user/reservation/faqs";
        $this->page_name = 'Faq';
        $this->parent_menu = 'reservation';
        $data['js'] = array(
            'user/faq.js',
        );
        $data['js_plugin'] = array(
        );
        $data['css_plugin'] = array(
        );
        $data['css'] = array(
        );
        $data['init'] = array(
            'faq.init()',
        );
        $this->load->view(FRONT_LAYOUT, $data);
    }

    function searchfaqs() {
        $value = $this->input->post('search');
        if ($value != '') 
		{
            $this->db->like('var_quation', $value, 'both');
        }
        $query = $this->db->get('dz_faqs')->result_array();
        $data['faqdata'] = $query;
        $this->load->view('user/reservation/searchfaqs', $data);
    }

    function privacy_policy() {

        $data['page'] = "user/legal_corner/privacy_policy";
        $this->page_name = 'Privacy Policy';
        $this->parent_menu = 'legalcorner';
        $data['js'] = array(
        );
        $data['js_plugin'] = array(
        );
        $data['css_plugin'] = array(
        );
        $data['css'] = array(
        );
        $data['init'] = array(
        );
        $this->load->view(FRONT_LAYOUT, $data);
    }

    function property_policy() {

        $data['page'] = "user/legal_corner/property_policy";
        $this->page_name = 'Intellectual Property Policy';
        $this->parent_menu = 'legalcorner';
        $data['js'] = array(
        );
        $data['js_plugin'] = array(
        );
        $data['css_plugin'] = array(
        );
        $data['css'] = array(
        );
        $data['init'] = array(
        );
        $this->load->view(FRONT_LAYOUT, $data);
    }

    function dmca_Policy() {

        $data['page'] = "user/legal_corner/dmca_policy";
        $this->page_name = 'DMCA Policy';
        $this->parent_menu = 'dmca_policy';
        $data['js'] = array(
        );
        $data['js_plugin'] = array(
        );
        $data['css_plugin'] = array(
        );
        $data['css'] = array(
        );
        $data['init'] = array(
        );
        $this->load->view(FRONT_LAYOUT, $data);
    }
    
    function security_statement(){
        $data['page'] = "user/legal_corner/security_statement";
        $this->page_name = 'We Care for Your Security';
//        $this->parent_menu = 'dmca_policy';
        $data['js'] = array(
        );
        $data['js_plugin'] = array(
        );
        $data['css_plugin'] = array(
        );
        $data['css'] = array(
        );
        $data['init'] = array(
        );
        $this->load->view(FRONT_LAYOUT, $data);
    }
    
    function bitcoin_policy(){
        $data['page'] = "user/legal_corner/bitcoin_policy";
        $this->page_name = 'Bitcoin Policy';
//        $this->parent_menu = 'dmca_policy';
        $data['js'] = array(
        );
        $data['js_plugin'] = array(
        );
        $data['css_plugin'] = array(
        );
        $data['css'] = array(
        );
        $data['init'] = array(
        );
        $this->load->view(FRONT_LAYOUT, $data);
    }
    
    function member() {
        $data['page'] = "user/legal_corner/member";
        $this->page_name = 'Members';
        $this->parent_menu = 'dmca_policy';
        $data['js'] = array(
        );
        $data['js_plugin'] = array(
        );
        $data['css_plugin'] = array(
        );
        $data['css'] = array(
        );
        $data['init'] = array(
        );
        $this->load->view(FRONT_LAYOUT, $data);
    }

    function aboutus() 
	{
//        print_r($this->session->userdata);
//        exit();
        $data['var_meta_title'] = 'Wholesale Hotels Group – Great travel deals for Corporate Travel and Individuals';
        $data['var_meta_description'] = 'Get WHOLESALE HOTEL inventories and deals worldwide for Corporate Travel, Non-Profit travel, Government travel, Travel Agents and Individual users.';
        $data['var_meta_keyword'] = 'Wholesale Hotels Group, WHotelsGroup.com, wholesale hotel deals, corporate travel, government travel, non-profit travel, travel agency travel, travel agency hotels, wholesale hotels, Online Travel Agency, best OTAs';
        $data['page'] = "user/legal_corner/aboutus";
        $this->page_name = 'About Us';
        $this->parent_menu = 'dmca_policy';
        $data['js'] = array(
        );
        $data['js_plugin'] = array(
        );
        $data['css_plugin'] = array(
        );
        $data['css'] = array(
        );
        $data['init'] = array(
        );
        $this->load->view(FRONT_LAYOUT, $data);
    }

    function affiliateprogram() {
        $data['page'] = "user/legal_corner/affiliateprogram";
        $this->page_name = 'AFFILIATE PROGRAM';
        $this->parent_menu = 'dmca_policy';
        $data['js'] = array(
        );
        $data['js_plugin'] = array(
        );
        $data['css_plugin'] = array(
        );
        $data['css'] = array(
        );
        $data['init'] = array(
        );
        $this->load->view(FRONT_LAYOUT, $data);
    }

    function contactus() 
	{
        $data['var_meta_title'] = 'Wholesale Hotels Group – Great travel deals for Corporate Travel and Individuals';
        $data['var_meta_description'] = 'Get WHOLESALE HOTEL inventories and deals worldwide for Corporate Travel, Non-Profit travel, Government travel, Travel Agents and Individual users.';
        $data['var_meta_keyword'] = 'Wholesale Hotels Group, WHotelsGroup.com, wholesale hotel deals, corporate travel, government travel, non-profit travel, travel agency travel, travel agency hotels, wholesale hotels, Online Travel Agency, best OTAs';
        $data['page'] = "user/legal_corner/contactus";
        $this->page_name = 'Contact Us';
        $this->parent_menu = 'dmca_policy';
        $data['js'] = array(
            'front/frontcontactus.js'
        );
        $data['js_plugin'] = array(
        );
        $data['css_plugin'] = array(
        );
        $data['css'] = array(
        );
        $data['init'] = array(
            'Contact_us.init()'
        );
        if ($this->input->post()) {
			if(isset($_POST['g-recaptcha-response']))
			{
				$captcha=$this->input->post('g-recaptcha-response');
				if(!$captcha){
				  echo 'Please Fill Captcha!';
				  exit;
				}
				$response=json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6Ld1XicTAAAAAOqDFJSIjxAbqF1h0EmaGipcN_ys&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']), true);
				if($response['success'] == false)
				{
				  echo 'Invalid Captcha';
				  exit;
				}
				else
				{
					$this->load->library('email');
					$this->email->set_mailtype("html");
					$this->email->from('info@hotelsdifferently.com', 'User');
					$this->email->to('techsupport@hotelsdifferently.com');
					$this->email->reply_to($this->input->post('email_add'));
					$this->email->subject('User Contact us');
				   // $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Dear Member</p><br/>';
				 //   $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Topic: ' . $this->input->post('topic') . '</p><br/>';
				//    $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Response: ' . $this->input->post('response') . '</p><br/>';
					$mail_body .= '<p style="color:#fff;margin-bottom:10px;">' . $this->input->post('message') . '</p><br/>';
			   //     $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Sincerely,</br>User</p>';
					$data['mail_body'] = $mail_body;
				   // $message = $this->load->view('admin/account/genral_emailtemplate', $data, TRUE);
					$this->email->message($mail_body);
				  
					$sent = $this->email->send();
					if ($sent) {
						echo "success";
						exit;
					} else {
						echo "error";
						exit;
					}
				}
			}
		}
        $this->load->view(FRONT_LAYOUT, $data);
    }
}

?>