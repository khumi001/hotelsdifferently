<?php

class Contactus extends User_Controller {
    var $layout = USER_LAYOUT;
    function __construct() {
        parent::__construct();
        date_default_timezone_set(AMERICA_LOS_ANGLES_TIME_ZONE);
        // get layout
        if ($this->session->userdata['menu_layout']){
            $this->layout = $this->session->userdata['menu_layout'];
        }
    }

    function index(){
        $data['page'] = "user/contactus/contact_us";
        $this->page_name = 'Contact Us';

        $data['js'] = array(
            'user/contact_us.js'
        );

        $data['js_plugin'] = array(
            'bootstrap-fileinput/bootstrap-fileinput.js',
            'bootstrap-datepicker/js/bootstrap-datepicker.js',
            'bootstrap-select/bootstrap-select.min.js',
            'select2/select2.min.js',
        );

        $data['css_plugin'] = array(
            'bootstrap-fileinput/bootstrap-fileinput.css',
            'bootstrap-datepicker/css/datepicker.css',
            'bootstrap-select/bootstrap-select.min.css',
            'select2/select2.css',
            'select2/select2-metronic.css',
        );

        $data['css'] = array();

        $data['init'] = array(
            'Contact_us.init()'
        );

        $data['acc_info'] = $this->db->where('int_glcode', $this->userid)->get('dz_user')->row_array();

        if ($this->input->post()){

            if(isset($_POST['g-recaptcha-response'])){

                $captcha=$this->input->post('g-recaptcha-response');
                if(!$captcha){
                    echo 'Please Fill Captcha!';
                    exit;
                }

                /*$response=json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6Ld1XicTAAAAAOqDFJSIjxAbqF1h0EmaGipcN_ys&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']), true);
                if($response['success'] == false){
                  echo 'Invalid Captcha';
                  exit;

                }else{*/

                if($this->input->post('topic') == "Technical / Website issues"){
                    $toemail = "techsupport@".EMAIL_DOMAIN;
                }elseif ($this->input->post('topic') == "Billing inquiry") {
                    $toemail = "billing@".EMAIL_DOMAIN;
                }elseif ($this->input->post('topic') == "Booking changes") {
                    $toemail = "bookings@".EMAIL_DOMAIN;
                }elseif ($this->input->post('topic') == "Problem with booking") {
                    $toemail = "bookingproblems@".EMAIL_DOMAIN;
                }elseif ($this->input->post('topic') == "Reservations") {
                    $toemail = "bookings@".EMAIL_DOMAIN;
                }
                /*elseif ($this->input->post('topic') == "Email confirmation issue") {

                    $toemail = "feedback@".EMAIL_DOMAIN;

                }*/
                elseif ($this->input->post('topic') == "Complaint / Compliment") {
                    $toemail = "feedback@".EMAIL_DOMAIN;
                }elseif ($this->input->post('topic') == "Other") {
                    $toemail = "feedback@".EMAIL_DOMAIN;
                }


                if($this->input->post('topic') ==''){
                    $subject = 'User Contact us';
                }else{
                    $subject = $this->input->post('topic');
                }

                $this->email->set_mailtype("html");
                //$this->email->from('info@'.EMAIL_DOMAIN, 'User');
                $this->email->from('info@'.EMAIL_DOMAIN, 'User');

                $this->email->to($toemail);
                $this->email->reply_to($this->input->post('email_add'));
                $this->email->subject($subject);
                // $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Dear Member</p><br/>';
                //   $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Topic: ' . $this->input->post('topic') . '</p><br/>';
                //$mail_body .= '<p style="color:#fff;margin-bottom:10px;">Response: ' . $this->input->post('response') . '</p><br/>';
                $mail_body = '<span style="color:#1EA5F2 !important;margin-bottom:10px;">' . $this->input->post('message') . '</span><br/>';
                //$mail_body .= '<p style="color:#fff;margin-bottom:10px;">Sincerely,</br>User</p>';
                $data['mail_body'] = $mail_body;
                // $message = $this->load->view('admin/account/genral_emailtemplate', $data, TRUE);
                $this->email->message($mail_body);
                $sent = $this->email->send();

                if ($sent) {
                    echo "success";
                    exit;

                } else {

                    echo "error";
                    exit;
                }

            }
            //}

        }

        $this->load->view($this->layout, $data);
    }
    function contactimpoter()
    {
        $toemail = $this->input->post('textemail');
        $subject = $this->input->post('subject');
        $message = $this->input->post('message');
        //   echo $toemail;
        //  exit();
        $tomailarray = explode(',', $toemail);
        $tomailarraycount = count($tomailarray) - 1;
        //   print_r($tomailarray);
        // echo $toemail;

        for($i=0;$i < $tomailarraycount;$i++)
        {
            //  echo $tomailarray[$i].'-';
            $email = array();
            $email = explode('@', $tomailarray[$i]);
            $this->load->library('email');
            $this->email->set_mailtype("html");
            $this->email->from($this->session->userdata['valid_user']['var_email'], $this->session->userdata['valid_user']['username']);
//            $this->email->from(NOREPLY, $this->session->userdata['valid_user']['username']);
            $this->email->to($tomailarray[$i]);
            $this->email->reply_to($this->session->userdata['valid_user']['var_email'], $this->session->userdata['valid_user']['username']);
            $this->email->subject($subject);
            //$message = "";
            $mail_body = "";
            $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Dear '.ucfirst($email[0]).'</p><br/>';
            $mail_body .= '<p style="color:#fff;margin-bottom:10px;">' . $message . '</p><br/>';
            $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Sincerely,<br/>'.ucfirst($this->session->userdata['valid_user']['username']).'</p>';
            $data['mail_body'] = $mail_body;
            $message1 = $this->load->view('admin/account/genral_emailtemplate', $data, TRUE);
            $this->email->message($message1);
            $sent = $this->email->send();
            //  echo $this->email->print_debugger();
            // exit();
        }

        if ($sent) {
            echo "success";
            exit;
        } else {
            echo "error";
            exit;
        }
    }

}

?>