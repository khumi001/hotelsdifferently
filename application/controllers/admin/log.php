<?php

class Log extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin/loginlog_model');
    }

    function index()
    {
        if(!$this->session->userdata('valid_adminuser'))
        {
            redirect('admin/login');
        }
        if ($this->blobmenuarray1['Login Log'] != 'yes')
        {
            exit();
        }
        $data['page'] = "admin/log/loginlist";
        $data['breadcrumb_list'] = array(
            array('Login log', ''),
        );
        $data['log'] = 'class="start active"';
        $data['var_meta_title'] = 'Login log';
        $data['var_meta_description'] = 'Login log';
        $data['var_meta_keyword'] = 'Login log';
        $data['js'] = array(
            'admin/log.js',
            //  'custom/table-managed.js',
            'custom/components-pickers.js',
        );
        $data['js_plugin'] = array(
            'select2/select2.min.js',
            'data-tables/jquery.dataTables.js',
            'data-tables/DT_bootstrap.js',
        );
        $data['css_plugin'] = array(
            'select2/select2.css',
            'select2/select2-metronic.css',
            'data-tables/DT_bootstrap.css'
        );
        $data['css'] = array(
        );
        $data['init'] = array(
            'Log.init()',
//            'TableManaged.init()',

        );
        if ($this->input->is_ajax_request())
        {
            $result = $this->toval->check_cookie_ajax($data['js'], $data['js_plugin'], $data['css'], $data['css_plugin']);
            $data['js'] = $result['js'];
            $data['js_plugin'] = $result['js_plugin'];
        }
        else
        {
            $result = $this->toval->check_cookie($data['js'], $data['js_plugin'], $data['css'], $data['css_plugin']);
            $data['js'] = $result['js'];
            $data['js_plugin'] = $result['js_plugin'];
        }
        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function update_loginlog()
    {
        if(!$this->session->userdata('valid_adminuser'))
        {
            redirect('admin/login');
        }
        $this->loginlog_model->updateloginlog();
    }

    function log_datatable()
    {
        if(!$this->session->userdata('valid_adminuser'))
        {
            redirect('admin/login');
        }
        $this->load->library('Datatables');
        $this->datatables->select('
            dz_login_log.var_createddate,
            dz_login_log.var_email,
            dz_login_log.var_ipaddress,
            dz_login_log.var_status,
        
            ');
        $this->datatables->from('dz_login_log');
        $results = $this->datatables->generate();
        $results = json_decode($results, true);
        for ($i = 0; $i < count($results['aaData']); $i++) {
            $status = "";
            if ($results['aaData'][$i][3] == "Failed") {
                $status = 'Failed';
            } else {
                $status = '<span style="color:green;">OK</span>';
            }
            $results['aaData'][$i][0] = date(DISPLAY_DATE_FORMAT_FULL, strtotime($results['aaData'][$i][0]));
            $results['aaData'][$i][1] = $results['aaData'][$i][1];
            $results['aaData'][$i][2] = $results['aaData'][$i][2];
            $results['aaData'][$i][3] = $status;
        }
        echo json_encode($results);
    }
    function affilatel_datatable()
    {
        if(!$this->session->userdata('valid_adminuser'))
        {
            redirect('admin/login');
        }
        $this->load->library('Datatables');

        $this->datatables->select('
            dz_login_log.var_createddate,
            dz_login_log.var_email,
            dz_login_log.var_ipaddress,
            dz_login_log.var_status');
        $this->datatables->from('dz_login_log');
        $this->datatables->where('dz_login_log.chr_user_type', 'AF');
        $results = $this->datatables->generate();
        $results = json_decode($results, true);
        for ($i = 0; $i < count($results['aaData']); $i++) {
            $status = "";
            if ($results['aaData'][$i][3] == "Failed") {
                $status = 'Failed';
            } else {
                $status = '<span style="color:green;">OK</span>';
            }
            $results['aaData'][$i][0] = date(DISPLAY_DATE_FORMAT_FULL, strtotime($results['aaData'][$i][0]));
            $results['aaData'][$i][1] = $results['aaData'][$i][1];
            $results['aaData'][$i][2] = $results['aaData'][$i][2];
            $results['aaData'][$i][3] = $status;
        }
        echo json_encode($results);
    }
    function admin_datatable()
    {
        if(!$this->session->userdata('valid_adminuser'))
        {
            redirect('admin/login');
        }
        $this->load->library('Datatables');
        $this->datatables->select('
            dz_login_log.var_createddate,
            dz_login_log.var_email,
            dz_login_log.var_ipaddress,
            dz_login_log.var_status,
        
            ');
        $this->datatables->from('dz_login_log');
        $this->datatables->where('dz_login_log.chr_user_type', 'A');
        $results = $this->datatables->generate();
        $results = json_decode($results, true);
        for ($i = 0; $i < count($results['aaData']); $i++)
        {
            $status = "";
            if ($results['aaData'][$i][3] == "Failed")
            {
                $status = 'Failed';
            }
            else
            {
                $status = '<span style="color:green;">OK</span>';
            }
            $results['aaData'][$i][0] = date(DISPLAY_DATE_FORMAT_FULL, strtotime($results['aaData'][$i][0]));
            $results['aaData'][$i][1] = $results['aaData'][$i][1];
            $results['aaData'][$i][2] = $results['aaData'][$i][2];
            $results['aaData'][$i][3] = $status;
        }
        echo json_encode($results);
    }
    function member_datatable()
    {
        if(!$this->session->userdata('valid_adminuser'))
        {
            redirect('admin/login');
        }
        $this->load->library('Datatables');
        $this->datatables->select('
            dz_login_log.var_createddate,
            dz_login_log.var_email,
            dz_login_log.var_ipaddress,
            dz_login_log.var_status,
        
            ');
        $this->datatables->from('dz_login_log');
        $this->datatables->where('dz_login_log.chr_user_type', 'U');
        $results = $this->datatables->generate();
        $results = json_decode($results, true);
        for ($i = 0; $i < count($results['aaData']); $i++) {
            $status = "";
            if ($results['aaData'][$i][3] == "Failed") {
                $status = 'Failed';
            } else {
                $status = '<span style="color:green;">OK</span>';
            }
            $results['aaData'][$i][0] = date(DISPLAY_DATE_FORMAT_FULL, strtotime($results['aaData'][$i][0]));
            $results['aaData'][$i][1] = $results['aaData'][$i][1];
            $results['aaData'][$i][2] = $results['aaData'][$i][2];
            $results['aaData'][$i][3] = $status;
        }
        echo json_encode($results);
    }
}

?>
