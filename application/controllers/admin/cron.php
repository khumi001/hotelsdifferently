<?php

class Cron extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function update() {
        // expire replay Quoit
        date_default_timezone_set('America/Los_Angeles');
        $this->db->select('var_createddate,int_glcode,int_fkquote');
        $query = $this->db->get('dz_rplyquote')->result_array();

        for ($i = 0; $i < count($query); $i++) {
            $result = $query[$i]['var_createddate'];

            $TotalHours = 0;
            $today = date('Y-m-d H:i:s');
            $start = new DateTime($result);
            $End = new DateTime($today);
            $interval = $start->diff($End);
            $day = $interval->days;
            $houres = $interval->h;
            $TotalHours = ($day * 24) + $houres;
            if ($TotalHours > 24) {

                $dataarray1 = array(
                    'quote_status' => "E",
                );
                $this->db->where('int_glcode', $query[$i]['int_fkquote']);
                $this->db->update('dz_quote', $dataarray1);

                $dataarray = array(
                    'chr_quote_status' => "E",
                );
                $this->db->where('int_glcode', $query[$i]['int_glcode']);
                $this->db->update('dz_rplyquote', $dataarray);
            }
        }
        
    }

    function generatepdf() {
        $convert_url = base_url() . 'admin/cron/pdfhtml';
//        $url = 'http://freehtmltopdf.com';
//        $data = array('convert' => $convert_url,
//            'html' => '',
//            'baseurl' => 'http://demo.webeet.net/safety/');
//
//        // use key 'http' even if you send the request to https://...
//        $options = array(
//            'http' => array(
//                'header' => "Content-type: application/x-www-form-urlencoded\r\n",
//                'method' => 'POST',
//                'content' => http_build_query($data),
//            ),
//        );
//        $context = stream_context_create($options);
//        $result = file_get_contents($url, false, $context);
//
//        // set the pdf data as download content:
//        header('Content-type: application/pdf');
//        header('Content-Disposition: attachment; filename="Report('.$month.').pdf"');
//        var_dump($result);
        $pdf = file_get_contents('http://api.simplehtmltopdf.com/?link=' . urlencode($convert_url) . '&orientation=Portrait&mtop=0&mright=0&mleft=0&mbot=0');

        // set HTTP response headers
        header("Content-Type: application/pdf");
        header("Cache-Control: no-cache");
        header("Accept-Ranges: none");
        header('Content-Disposition: attachment; filename="Report.pdf"');

        // send the pdf that you create
        echo $pdf;
    }

    function pdfhtml() {
        $data['breadcrumb'] = "Affiliate Database";
        $data['breadcrumb_sub'] = "Affiliate Database Sub";
        $data['breadcrumb_list'] = array(
            array('Affiliate Database', ''),
        );
        $data['affdatabase'] = 'class="start active"';
        $data['var_meta_title'] = 'Affiliate Database ';
        $data['var_meta_description'] = 'Affiliate Database ';
        $data['var_meta_keyword'] = 'Affiliate Database ';

        $data['js'] = array(
            'custom/table-managed.js',
            'admin/affidata.js',
                //  'admin/affiliaterequest.js',
        );
        $data['css_plugin'] = array(
            'select2/select2.css',
            'select2/select2-metronic.css',
            'data-tables/DT_bootstrap.css'
        );
        $data['js_plugin'] = array(
            'select2/select2.min.js',
            'data-tables/jquery.dataTables.js',
            'data-tables/DT_bootstrap.js',
        );
        $data['css'] = array(
        );

        $data['init'] = array(
            'Affiliatedata.init()',
            'TableManaged.init()',
        );

        $this->db->where('chr_user_type', 'AF');
        $this->db->where('chr_status', 'A');
        $user = $this->db->get('dz_user')->result_array(); // print_r($user);exit;

        for ($i = 0; $i < count($user); $i++) {
            $this->db->where('fk_user', $user[$i]['int_glcode']);
            $payment = $this->db->get('dz_paymount_prefrence')->result_array(); // print_r($payment);exit;
            $info[] = array(
                'id' => $user[$i]['int_glcode'],
                'fname' => $user[$i]['var_fname'],
                'lname' => $user[$i]['var_lname'],
                'email' => $user[$i]['var_email'],
                'company' => $user[$i]['var_company'],
                'title' => $user[$i]['var_title'],
                'address1' => $user[$i]['var_address1'],
                'address2' => $user[$i]['var_address2'],
                'city' => $user[$i]['var_city'],
                'country' => $user[$i]['var_country'],
                'phone' => $user[$i]['var_phone'],
                'payment' => $user[$i]['var_payment'],
                'newsletter' => $user[$i]['var_newsletter'],
                'websiteurl' => $user[$i]['var_websiteurl'],
                'sitedescription' => $user[$i]['var_sitedescription'],
                'visitors' => $user[$i]['var_visitors'],
                'paypalemail' => $payment[0]['var_paypalemail'],
                'money' => $payment[0]['var_money'],
                'fullname' => $payment[0]['var_fullname'],
                'phonenumber' => $payment[0]['var_phonenumber'],
                'city' => $payment[0]['var_city'],
                'state' => $payment[0]['var_state'],
                'countrypayment' => $payment[0]['var_country'],
            ); // print_r($info); exit;       
        }
        //  print_r($info);
        //   exit();
        $data['affilateinfo'] = $info;

        $this->load->view('admin/affidatabase/pdfhtml', $data);
    }

    function updatecoupan() {
        $content = array(
            'var_status' => 'E',
        );
        $this->db->where('var_status', 'A');
        $this->db->where('var_couponexpi <', date('Y-m-d'));
        $row = $this->db->update('dz_coupons', $content);
    }

}

?>
