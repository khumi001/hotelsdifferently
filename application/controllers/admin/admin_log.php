<?php

class Admin_log extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin/admin_log_model');
    }

    function index() 
	{
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
        if ($this->blobmenuarray1['Admin Activity Log'] != 'yes') {
            exit();
        }
        $data['page'] = "admin/admin_log/admin_log_list";
        $data['breadcrumb_list'] = array(
            array('Admin Activity Log', ''),
        );
        $data['admin_act_log'] = 'class="start active"';
        $data['var_meta_title'] = 'Admin Activity Log';
        $data['var_meta_description'] = 'Admin Activity Log';
        $data['var_meta_keyword'] = 'Admin Activity Log';
        $data['js'] = array(
            'admin/admin_log.js',
            'custom/components-pickers.js',
        );
        $data['js_plugin'] = array(
            'data-tables/jquery.dataTables.js',
            'select2/select2.min.js',
            'data-tables/DT_bootstrap.js',
            'bootstrap-datepicker/js/bootstrap-datepicker.js',
        );
        $data['css_plugin'] = array(
            'select2/select2.css',
            'select2/select2-metronic.css',
            'data-tables/DT_bootstrap.css',
            'bootstrap-datepicker/css/datepicker.css',
        );
        $data['css'] = array(
        );
        $data['init'] = array(
            'Adminlog.init()',
            'ComponentsPickers.init()',
        );
        $data['log_result']  = $this->admin_log_model->getlog();
        
        $this->load->view(ADMIN_LAYOUT, $data);
    }
}

?>
