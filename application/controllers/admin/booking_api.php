<?php

class Booking_api extends Front_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('tourico_api');
        $this->load->model('tourico_api/destination_model');
        $this->load->model('tourico_api/tourico_hotel_price_multiplier');
        $this->load->model('tourico_api/cuise_price_multiplier_model');
        $this->load->model('tourico_api/car_price_multiplier_model');
        $this->load->helper('date');
    }

    /*function ShowHotelSearchForm()
	{
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
        $data['page'] = "admin/tourico_api/hotel_search_form";
        $data['breadcrumb'] = "Hotel API";
        $data['breadcrumb_sub'] = "Hotel Price Update";
		$data['hotelPriceUpdate'] = 'class="start active"';


        $data['breadcrumb_list'] = array(
            array('Search Hotel Form', ''),
        );
        $data['var_meta_title'] = 'Hotel Price Update';
        $data['var_meta_description'] = 'Hotel Price Update';
        $data['var_meta_keyword'] = 'Hotel Price Update';

        $data['js'] = array(
			'custom/jquery.maskMoney.js',
            'admin/tourico_api/hotel_search_form.js',
            'custom/table-managed.js',
        );

        $data['css_plugin'] = array(
            'select2/select2.css',
            'select2/select2-metronic.css',
            'data-tables/DT_bootstrap.css'
        );

        $data['js_plugin'] = array
            (
            //'select2/select2.min.js',
            'data-tables/jquery.dataTables.js',
            'data-tables/DT_bootstrap.js'
        );

        $data['css'] = array(
        );
		
		if($_POST)
		{
			$CI = & get_instance();
			$CI->load->model('admin/admin_log_model');
			$priceper = $this->input->post('priceper');
			$res = $this->db->where('utype' , 1)->get('dz_tourico_hotel_price_multiplier')->result();
			if(count($res) > 0)
			{
				$data1 = array(
					'multiplier_in_percentage' => $priceper,
					'update_time' => date('Y-m-d H:i:s')
				);
				$this->db->where('utype' , 1)->update('dz_tourico_hotel_price_multiplier' , $data1);
				$log = array(
					'log_type' => 'update_all_hotel_price',
					'log_type_fk_id' => 0,
					'log_desc' => ' changed ALL HOTELS from '.$res[0]->multiplier_in_percentage.' to '.$priceper.'%'
				);
				$CI->admin_log_model->addlog($log);
			}
			else
			{
				$data1 = array(
					'multiplier_in_percentage' => $priceper,
					'utype' => 1,
					'update_time' => date('Y-m-d H:i:s')
				);
				$this->db->insert('dz_tourico_hotel_price_multiplier' , $data1);
				$log = array(
					'log_type' => 'add_all_hotel_price',
					'log_type_fk_id' => 0,
					'log_desc' => ' add ALL HOTELS '.$priceper.'%'
				);
				$CI->admin_log_model->addlog($log);
			}
		}
		$res = $this->db->where('utype' , 1)->get('dz_tourico_hotel_price_multiplier')->result();
		if(count($res) > 0)
		{
			$pprice = $res[0]->multiplier_in_percentage;
		}
		else
		{
			$pprice = 0.00;
		}
		$data['pprice'] = $pprice;
        $this->load->view(ADMIN_LAYOUT, $data);
    }*/

    function ShowHotelSearchResult() 
	{
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
        $destinationId = $this->input->get('locationId', TRUE);
        $hotelName = $this->input->get('hotelName', TRUE);

        $datetime = new DateTime();
        $datetime->modify('+10 day');
        $checkInDate = $datetime->format('Y-m-d');

        $datetime = new DateTime();
        $datetime->modify('+11 day');
        $checkOutDate = $datetime->format('Y-m-d');

        $destinationInfo = $this->destination_model->GetDestinationDetailById($destinationId);

        $destinationCode = $destinationInfo['destination_code'];
        $hotelCityName = '';
        $hotelLocationName = $destinationInfo['location_name'];
        $hotelSearchAPIResponseProcessObj = new HotelSearchAPIResponseProcess();
        $adultNumber = 2;
        $childNumber = 0;
        $starLevel = "0";
        $brandName = "";





        $roomInfoStr = <<<EOM
<hot1:RoomInfo>
                  <hot1:AdultNum>2</hot1:AdultNum>
                  <hot1:ChildNum>0</hot1:ChildNum>
               </hot1:RoomInfo>
EOM;

        $hotelList = $hotelSearchAPIResponseProcessObj->ProcessSearchResult($roomInfoStr, $destinationCode, $hotelCityName, $checkInDate, $checkOutDate, $adultNumber, $hotelLocationName, $hotelName, $childNumber, $childAges, $starLevel);

        $hotelIdArr = array();
        $hotelIdNameArr = array();
        //hotelId
        foreach ($hotelList as $aHotel) {
            $hotelIdArr[] = $aHotel['hotelId'];
            $hotelIdNameArr[] = $aHotel['hotelId'].'|'.preg_replace('/[^A-Za-z0-9\-]/', '', $aHotel['hotelName']);
        }
        $data = array();
        $data['hotel_list'] = $this->tourico_hotel_price_multiplier->ProcessPriceMultiplier($hotelList);
        $data['hotel_id_list'] = $hotelIdNameArr;
        $data['location_name'] = $this->input->get('locationName', TRUE);

        $data['page'] = "admin/tourico_api/hotel_search_result";
        $data['breadcrumb'] = "Hotel Price Update";
        $data['breadcrumb_sub'] = "Set Hotel As Hot Deal";

        $data['breadcrumb_list'] = array(
            array('Hotel Price Update', 'Hotel Price Update'),
        );
        $data['var_meta_title'] = 'Hotel Price Update';
        $data['var_meta_description'] = 'Hotel Price Update';
        $data['var_meta_keyword'] = 'Hotel Price Update';

        $data['js'] = array(
            'admin/tourico_api/hotel_search_result.js',
            'custom/table-managed.js',
        );

        $data['css_plugin'] = array(
            'select2/select2.css',
            'select2/select2-metronic.css',
            'data-tables/DT_bootstrap.css'
        );

        $data['js_plugin'] = array
            (
            'select2/select2.min.js',
            'data-tables/jquery.dataTables.js',
            'data-tables/DT_bootstrap.js',
            'ui-block/jquery.blockUI.js',

        );

        $data['css'] = array(
        );


        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function UpdateHotelPriceAjax() 
	{
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
        $hotel_core_apiObj = new HotelSearchAPICore();
        $profitPercentage = $this->input->post('profitPercentage', TRUE);
        $profitPercentage = $profitPercentage / 100;
        $hotelId = $this->input->post('hotelId', TRUE);
        //edit by tayyab
        $response = $hotel_core_apiObj->GetHotelDetailByHotelId(array($hotelId));
        $simpleXMLELementObj = simplexml_load_string($response);
        $hotelDetail = $simpleXMLELementObj->children('http://schemas.xmlsoap.org/soap/envelope/')->Body->children('http://tourico.com/webservices/hotelv3')->GetHotelDetailsV3Response->GetHotelDetailsV3Result->children()->TWS_HotelDetailsV3->Hotel;
        $hotel_name = !empty($hotelDetail->attributes()->name) ? $hotelDetail->attributes()->name->__toString() : '';
        $this->tourico_hotel_price_multiplier->SaveMultiplier( $hotelId,$profitPercentage,$hotel_name );
        $data = array(
            "multiplier_in_percentage" => $profitPercentage,
            "tourico_hotel_id" => $hotelId,
            'hotel_name' => $hotel_name
        );
        echo json_encode($data);
    }

    function UpdateAllHotelPriceAjax() 
	{
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
        $hotelIdList = $this->input->post('hotelIdList', TRUE);
        $allHotelMultiplier = $this->input->post('allHotelMultiplier', TRUE);
        $allHotelMultiplier = $allHotelMultiplier / 100;
        $hotelIdArr = json_decode($hotelIdList, true);
        $hotelIdArr2 = array();
        foreach ($hotelIdArr as $aHotelId) {
            $hotelIdName = explode('|',$aHotelId);
            $aHotelIdd    = $hotelIdName[0];
            $hotel_name  = $hotelIdName[1];
            $hotelIdArr2[] = $hotelIdName[0];
            $this->tourico_hotel_price_multiplier->SaveMultiplier($aHotelIdd, $allHotelMultiplier,$hotel_name);
        }
        $data = array(
            "all_hotel_multiplier" => $allHotelMultiplier,
            'hotel_id_list' => $hotelIdArr2
        );
        echo json_encode($data);
    }

    function ViewHotelMultiplierList() 
	{
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
        $hotelList = $this->db->get('dz_tourico_hotel_price_multiplier');

        $data = array();
        $data['hotel_list'] = $hotelList->result_array();

        $data['page'] = "admin/tourico_api/show_hotel_list_with_price_multiplier";
        $data['breadcrumb'] = "Hotel API";
        $data['breadcrumb_sub'] = "Set Hotel As Hot Deal";

        $data['breadcrumb_list'] = array(
            array('Search Hotel Form', ''),
        );
        $data['var_meta_title'] = 'Hotel Search Form';
        $data['var_meta_description'] = 'Hotel Search Form';
        $data['var_meta_keyword'] = 'Hotel Search Form';

        $data['js'] = array(
            'admin/tourico_api/existent_percentile_list.js',
            'custom/table-managed.js',
        );

        $data['css_plugin'] = array(
            'select2/select2.css',
            'select2/select2-metronic.css',
            'data-tables/DT_bootstrap.css'
        );

        $data['js_plugin'] = array
            (
            'select2/select2.min.js',
            'data-tables/jquery.dataTables.js',
            'data-tables/DT_bootstrap.js',
        );

        $data['css'] = array(
        );

        $this->load->view(ADMIN_LAYOUT, $data);
    }

    /////////////////////////for cruise///////////////////////////
    function ShowCruiseSearchFrom() 
	{
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
        $query = $this->db->from('dz_tourico_cruise_destination')->get();
        $data['cruise_destination_list'] = $query->result_array();

        $data['page'] = "admin/tourico_api/cruise_search_form";
        $data['breadcrumb'] = "Hotel API";
        $data['breadcrumb_sub'] = "Set Hotel As Hot Deal";

        $data['breadcrumb_list'] = array(
            array('Search Hotel Form', ''),
        );
        $data['var_meta_title'] = 'Hotel Search Form';
        $data['var_meta_description'] = 'Hotel Search Form';
        $data['var_meta_keyword'] = 'Hotel Search Form';

        $data['js'] = array(
            'admin/tourico_api/hotel_search_form.js',
            'custom/table-managed.js',
        );

        $data['css_plugin'] = array(
            'select2/select2.css',
            'select2/select2-metronic.css',
            'data-tables/DT_bootstrap.css'
        );

        $data['js_plugin'] = array
            (
            //'select2/select2.min.js',
            'data-tables/jquery.dataTables.js',
            'data-tables/DT_bootstrap.js',
        );

        $data['css'] = array(
        );


        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function ShowCruiseSearchResult() 
	{
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
        $cruiseDestination = $this->input->get('cruiseDestination', TRUE);
        $query = $this->db->from('dz_tourico_cruise_destination')->where('tourico_destination_id', $cruiseDestination)->get();
        $destinationInfo = $query->row_array();

        $processCruiseAPIObj = new ProcessCruiseAPI();
        $departmingYearMonth = date('Y-m', strtotime('+1 month'));
        $cruiseLengthRange = "0-999";

        $data = array();
        $searchResultList = $processCruiseAPIObj->SearchCruise($cruiseDestination, $departmingYearMonth, $cruiseLengthRange);

        $data['cruise_list'] = $this->cuise_price_multiplier_model->ProcessPriceMultiplier($searchResultList);

        $data['location_name'] = $destinationInfo['destination_name'];
        $data['page'] = "admin/tourico_api/cruise_search_result";
        $data['breadcrumb'] = "Hotel API";
        $data['breadcrumb_sub'] = "Set Hotel As Hot Deal";

        $data['breadcrumb_list'] = array(
            array('Search Hotel Form', ''),
        );
        $data['var_meta_title'] = 'Hotel Search Form';
        $data['var_meta_description'] = 'Hotel Search Form';
        $data['var_meta_keyword'] = 'Hotel Search Form';

        $data['js'] = array(
            'admin/tourico_api/cruise_search_result.js',
            'custom/table-managed.js',
        );

        $data['css_plugin'] = array(
            'select2/select2.css',
            'select2/select2-metronic.css',
            'data-tables/DT_bootstrap.css'
        );

        $data['js_plugin'] = array
            (
            'select2/select2.min.js',
            'data-tables/jquery.dataTables.js',
            'data-tables/DT_bootstrap.js',
        );

        $data['css'] = array(
        );


        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function UpdateCruisePriceAjax() 
	{
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
        $profitPercentage = $this->input->post('profitPercentage', TRUE);
        $cruiseName = $this->input->post('cruiseName', TRUE);
        $shipName = $this->input->post('shipName', TRUE);
        $cruiseId = $this->input->post('cruiseId', TRUE);
        $shipImageUrl = $this->input->post('shipImageUrl', TRUE);
        $touricoDestinationName = $this->input->post('touricoDestinationName', TRUE);

        $this->cuise_price_multiplier_model->SaveMultiplier($cruiseId, $cruiseName, $shipName, $shipImageUrl, $profitPercentage, $touricoDestinationName);
        $data = array(
            "tourico_destination_name" => $touricoDestinationName,
            "cruise_name" => $cruiseName,
            "ship_name" => $shipName,
            "ship_image_url" => $shipImageUrl,
            "multiplier_in_percentage" => $profitPercentage,
            "tourico_cruise_id" => $cruiseId
        );
        echo json_encode($data);
    }

    function ViewCruiseProfitPercentilelList() 
	{
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
        $cruiseListQuery = $this->db->get('dz_tourico_cruise_price_multiplier');

        $data = array();
        $data['cruise_list'] = $cruiseListQuery->result_array();

        $data['page'] = "admin/tourico_api/show_cruise_list_with_price_multiplier";
        $data['breadcrumb'] = "Hotel API";
        $data['breadcrumb_sub'] = "Set Hotel As Hot Deal";

        $data['breadcrumb_list'] = array(
            array('Search Hotel Form', ''),
        );
        $data['var_meta_title'] = 'Hotel Search Form';
        $data['var_meta_description'] = 'Hotel Search Form';
        $data['var_meta_keyword'] = 'Hotel Search Form';

        $data['js'] = array(
            'admin/tourico_api/cruise_existent_percentile_list.js',
            'custom/table-managed.js',
        );

        $data['css_plugin'] = array(
            'select2/select2.css',
            'select2/select2-metronic.css',
            'data-tables/DT_bootstrap.css'
        );

        $data['js_plugin'] = array
            (
            'select2/select2.min.js',
            'data-tables/jquery.dataTables.js',
            'data-tables/DT_bootstrap.js',
        );

        $data['css'] = array(
        );

        $this->load->view(ADMIN_LAYOUT, $data);
    }

    ////////////////////for car//////////////////////////////////////////
    function ShowCarLocationForm() 
	{
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
        /* $data['cruise_list'] = $this->car_price_multiplier_model->GetCarDestinationListWithPriceMutltiplier(); */

        $data['page'] = "admin/tourico_api/set_multiplier_car_city";
        $data['breadcrumb'] = "Hotel API";
        $data['breadcrumb_sub'] = "Set Hotel As Hot Deal";

        $data['breadcrumb_list'] = array(
            array('Search Hotel Form', ''),
        );
        $data['var_meta_title'] = 'Hotel Search Form';
        $data['var_meta_description'] = 'Hotel Search Form';
        $data['var_meta_keyword'] = 'Hotel Search Form';

        $data['js'] = array(
            'admin/tourico_api/car_location_set_price.js',
            'custom/table-managed.js',
        );

        $data['css_plugin'] = array(
            'select2/select2.css',
            'select2/select2-metronic.css',
            'data-tables/DT_bootstrap.css'
        );

        $data['js_plugin'] = array
            (
            //'select2/select2.min.js',
            'data-tables/jquery.dataTables.js',
            'data-tables/DT_bootstrap.js',
        );

        $data['css'] = array(
        );


        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function AddCarLocationPrice() 
	{
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
        $carDestinationId = $this->input->get('locationId', TRUE);
        $carLocationPercentile = $this->input->get('carLocationPercentile', TRUE);
        $this->car_price_multiplier_model->SaveMultiplier($carDestinationId, $carLocationPercentile);
        redirect(site_url('admin/Booking_api/ShowAllCarLocationPrice'));
    }

    function AddUpdateLocationPriceAjax() 
	{
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
        $carDestinationId = $this->input->post('carLocationId', TRUE);
        $carLocationPercentile = $this->input->post('profitPercentage', TRUE);
        $this->car_price_multiplier_model->SaveMultiplier($carDestinationId, $carLocationPercentile);
        $data = array(
            "multiplier_in_percentage" => $carLocationPercentile,
            "tourico_destination_id" => $carDestinationId
        );
        echo json_encode($data);
    }

    function ShowAllCarLocationPrice() 
	{
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
        $data = array();
        $data['car_destination_list'] = $this->car_price_multiplier_model->GetCarDestinationListWithPriceMutltiplier();
        /* echo "<pre>";
          print_r($data['car_destination_list']);
          echo "</pre>";
          exit(0); */
        $data['page'] = "admin/tourico_api/show_car_location_list_with_price_multiplier";
        $data['breadcrumb'] = "Hotel API";
        $data['breadcrumb_sub'] = "Set Hotel As Hot Deal";

        $data['breadcrumb_list'] = array(
            array('Search Hotel Form', ''),
        );
        $data['var_meta_title'] = 'Hotel Search Form';
        $data['var_meta_description'] = 'Hotel Search Form';
        $data['var_meta_keyword'] = 'Hotel Search Form';

        $data['js'] = array(
            'admin/tourico_api/car_location_price_list.js',
            'custom/table-managed.js',
        );

        $data['css_plugin'] = array(
            'select2/select2.css',
            'select2/select2-metronic.css',
            'data-tables/DT_bootstrap.css'
        );

        $data['js_plugin'] = array
            (
            'select2/select2.min.js',
            'data-tables/jquery.dataTables.js',
            'data-tables/DT_bootstrap.js',
        );

        $data['css'] = array(
        );
        $this->load->view(ADMIN_LAYOUT, $data);
    }

}

?>