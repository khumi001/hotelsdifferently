<?php
class Profile extends Admin_Controller{
    
    function __construct() {
        parent::__construct();
        $this->load->model('admin/profile_model');
    }
    
    function index()
	{
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
       if($this->blobmenuarray1['Profile'] != 'yes')
        {
            exit();
        }
        $data['page'] = "admin/profile/profile_edit";
        $data['breadcrumb'] = "Profile";
        $data['breadcrumb_sub'] = "profile Sub";
        $data['breadcrumb_list']= array(
            array('Profile','')
        );
        $data['profile'] = 'class="start active"';
	$data['var_meta_title'] = 'Dealz Profile';
	$data['var_meta_description'] = 'Dealz Profile';
	$data['var_meta_keyword'] = 'Dealz Profile';
        $data['js'] = array(
            'admin/profile.js',
            'custom/components-pickers.js'
        );
        $data['js_plugin'] = array(
            'bootstrap-fileinput/bootstrap-fileinput.js',
            'ckeditor/ckeditor.js',
        );
        $data['css'] = array(       
        );
        $data['css_plugin'] = array(
             'bootstrap-fileinput/bootstrap-fileinput.css',
             'bootstrap-datepicker/css/datepicker.css',
        );        
        $data['init'] = array(
            'Profile.init()'   
        );        
         if($this->input->is_ajax_request()){                        
            $result = $this->toval->check_cookie_ajax($data['js'],$data['js_plugin'],$data['css'],$data['css_plugin']);
            $data['js'] = $result['js'];
            $data['js_plugin'] = $result['js_plugin'];

        }else{                        
            $result = $this->toval->check_cookie($data['js'],$data['js_plugin'],$data['css'],$data['css_plugin']);            
            $data['js'] = $result['js'];
            $data['js_plugin'] = $result['js_plugin'];
        } 
        $data['result']= $this->profile_model->editprofile();
       
        $this->load->view(ADMIN_LAYOUT, $data);        
    }
    function edit_profile()
    {
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
        if ($this->input->is_ajax_request()) 
	{            
            $result = $this->profile_model->edit($this->input->post());
            if($result)
            {
                echo "success";
            }else{
                echo "error";
            }
        }
    }
    function change_password()
    {
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
        if ($this->input->is_ajax_request()) {
            $row = $this->profile_model->change_password($this->input->post());            
            if ($row) {
                echo "success";
            } else {
                echo "error";
            }
        }
    }

   function profile_img()
   {
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
        
        $name = $_FILES['prof_img']['name'];
//        echo $name; exit();
         if($name !='')
         {
            $type = explode(".", $name);    
            $file_name = $this->session->userdata['valid_adminuser']['id'].".".$type['1'];
            $config['upload_path'] = 'public/uploads/profilepic';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['file_name'] = $file_name;
            $config['overwrite'] = TRUE;
            $this->load->library('upload', $config);
            
            if (!$this->upload->do_upload('prof_img'))
            {
                echo $this->upload->display_errors();
            }
            else
            {
                 $this->upload->data();
                 $this->profile_model->update_img($file_name,'consultant');
                 redirect('admin/profile');
            }
        }else{
             redirect('admin/profile');
        }
    }
}
?>
