<?php

class Processipban extends Admin_Controller {

    function __construct() {
        parent::__construct();
         $this->load->model('admin/proipban');
		 $this->load->helper('tourico_api');
    }

    function index() 
	{
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
        if($this->blobmenuarray1['Processed IP BAN'] != 'yes')
        {
            exit();
        }
        $data['page'] = "admin/proipban/list";
        $data['breadcrumb'] = "Processed IP Address Ban";
        $data['breadcrumb_sub'] = "Processed IP Address Ban sub";
        $data['breadcrumb_list'] = array(
            array('Processed IP Address Ban', ''),
        );
        $data['pipaddressban'] = 'class="start active"';
        $data['var_meta_title'] = 'Processed IP Address Ban';
        $data['var_meta_description'] = 'Processed IP Address Ban';
        $data['var_meta_keyword'] = 'Processed IP Address Ban';
        $data['js'] = array(
            'custom/table-managed.js',
            'admin/banipaddress.js',
        );
        $data['css_plugin'] = array(
            'select2/select2.css',
            'select2/select2-metronic.css',
            'data-tables/DT_bootstrap.css'
        );
        $data['js_plugin'] = array(
            'select2/select2.min.js',
            'data-tables/jquery.dataTables.js',
            'data-tables/DT_bootstrap.js',
        );
        $data['css'] = array(
        );

        $data['init'] = array(
            'TableManaged.init()',
            'banipaddres.init()',
        );
         if ($this->input->is_ajax_request()) {
            $result = $this->toval->check_cookie_ajax($data['js'], $data['js_plugin'], $data['css'], $data['css_plugin']);
            $data['js'] = $result['js'];
            $data['js_plugin'] = $result['js_plugin'];
        } else {
            $result = $this->toval->check_cookie($data['js'], $data['js_plugin'], $data['css'], $data['css_plugin']);
            $data['js'] = $result['js'];
            $data['js_plugin'] = $result['js_plugin'];
        }
        $data['ipban'] = $this->proipban->getipban();
        
        $this->load->view(ADMIN_LAYOUT, $data);
    }
    function change_status()
	{
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
        if($this->input->post())
		{
            $res =$this->proipban->status_change($this->input->post());
        }
        if($res)
		{
            echo "success";
        }
		else
		{
            echo "error";
        }
    }

}

?>
