<?php

class Pen_res extends Admin_Controller {

    function __construct() 
	{
        parent::__construct();
        $this->load->model('admin/con_reservation');
		$this->load->helper('stripe_lib/init');
		$this->load->helper('tourico_api');
    }
	
	function stripeRefund($refundinfo) 
	{
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
        \Stripe\Stripe::setApiKey('sk_test_pXZu0HXdj2nffgTwk8xbG8Tr');
        $planInfo = null;
        try 
		{
			$re = \Stripe\Refund::create(array(
			  "charge" => $refundinfo
			));
            return array('success' => true, 'refund_info' => $re);
        } 
		catch (\Stripe\Error\Card $e) 
		{
            $body = $e->getJsonBody();
            $err = $body['error'];
            return array('success' => false, 'reason' => 'card_declined', 'details' => $err['message']);
        } 
		catch (\Stripe\Error\InvalidRequest $e) 
		{
            return array('success' => false, 'reason' => 'invalid_parameter_supplied', 'details' => 'Invalid parameter supplied to stripe');
        } 
		catch (\Stripe\Error\Authentication $e) 
		{
            return array('success' => false, 'reason' => 'secret_key_not_valid', 'details' => 'Invalid parameter supplied to stripe');
        } 
		catch (\Stripe\Error\ApiConnection $e) 
		{
            return array('success' => false, 'reason' => 'connection_problem', 'details' => 'connection to stripe is not working');
        } 
		catch (Exception $e) 
		{
            return array('success' => false, 'reason' => 'other_error', 'details' => 'connection to stripe is not working');
        }
    }

    function index($action = '' , $id = false) 
	{
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
		if ($this->blobmenuarray1['Pending reservations'] != 'yes') {
            exit();
        }
		if(!empty($action))
		{
			if($action == 'approve')
			{
				$res = $this->db->where('id' , $id)->get('book_hotal')->result();
				if(count($res) > 0)
				{
					if($res[0]->pending_status == 0)
					{
						$dat = array(
							"book_status" => "Confirm"
						);
						$this->db->where('id' , $id)->update('book_hotal' , $dat);
						$dat = array(
							"status" => "Confirm"
						);
						$this->db->where('book_id' , $id)->update('booking_detail' , $dat);
						
						$user1 = getUserById($res[0]->uid);
						$this->load->library('email');
						$this->email->set_mailtype("html");
						$this->email->from(NOREPLY, 'HotelsDifferently ');
						$this->email->to($user1->var_email);
						$this->email->subject('Reservation Confirmation');
						$this->load->helper('pdf');
						tcpdf($id , true);
						$this->email->attach('application/helpers/tcpdf/examples/files/Invoice.pdf');
                        if($user1->chr_user_type == "TF") {
                            travelProVoucher($id, $user1, true);
                            $this->email->attach('application/helpers/tcpdf/examples/files/voucher.pdf');
                        }
						//unlink('application/helpers/tcpdf/examples/files/Invoice.pdf');
						/*$mail_body = '<p style="color:#fff;margin-bottom:10px;">Dear '.$user1->var_fname . '</p>';
						$mail_body .='<p style="color:#fff;margin-bottom:10px;">Thank you for using <b>HotelsDifferently<sup>sm</sup></b>.</p>';
						$mail_body .='<p style="color:#fff;margin-bottom:10px;">Your reservation for '.$res[0]->hotelName.' for  for check-in '.date('Y-m-d',strtotime($res[0]->checkIn)).' and check-out '.date('Y-m-d',strtotime($res[0]->checkOut)).' in '.$res[0]->Location.' is now confirmed successfully and your confirmation number is <b>'.$res[0]->codeID.'</b></p>';
						$mail_body .='<p style="color:#fff;margin-bottom:10px;">Again, thank you for choosing us and please always give us a try before booking your next trip!</p>';
						$mail_body .= '<p style="color:#fff;margin-bottom:10px;">Sincerely,</br>';
						$mail_body .= '<p style="color:#fff;margin-bottom:10px;"><b>HotelsDifferently<sup>sm</sup></b><br>
							<small style="font-style: italic;">Making hotels happen for you!</small><b><sup>sm</sup></b>
							</p>';*/

                        $mail_body = '<p style="color:#fff;margin-bottom:10px;">Dear ' . $user1->var_fname . '</p>';
                        $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Thank you for using <b>HotelsDifferently<sup>sm</sup></b>.</p>';
                        $mail_body .='<p style="color:#fff;margin-bottom:10px;">Your reservation for '.$res[0]->hotelName.' for  for check-in '.date('Y-m-d',strtotime($res[0]->checkIn)).' and check-out '.date('Y-m-d',strtotime($res[0]->checkOut)).' in '.$res[0]->Location.' is now confirmed successfully and your confirmation number is <b>'.$res[0]->codeID.'</b></p>';
                        $mail_body .= 'Your payment of '.$res[0]->paid.' for your membership was successfully charged and your charges will appear as “<b>HOTELSDIFF</b>” or “<b>HOTELSDFRNTL8882872307</b>” and a receipt has been sent to you as well.';
                        $mail_body .= '<p style="color:#fff !important;margin-bottom:10px;">Again, thank you for choosing us and please always give us a try before booking your next trip!</p>';
                        $mail_body .= '<p style="margin-bottom:10px;color:#fff !important;">Sincerely,</br>';
                        $mail_body .= '<p style="color:#fff;margin-bottom:10px;"><b>HotelsDifferently<sup>sm</sup></b><br>
                <small style="font-style: italic;">Making hotels happen for you!</small><b><sup>sm</sup></b>
                </p>';
                        $mail_body .= '<p style="color:#fff;margin-bottom:10px;">PS: This inbox is NOT monitored, please do NOT send us any emails here.,</br>';
                        $mail_body .= '
                <p style="border:1px solid #000;"></p>
                <p><b style="font-size:14px; display:block; text-align:center; color:#fff;">NEED A TICKET FOR AN EVENT OR CONCERT?</b></p>
                <p style="color:#fff;">We are proud to be affiliated with TicketLiquidator which is one of the biggest names in the event ticket industry. On many occasions, you can obtain tickets for events that are even sold out. Type in a Venue, an Event or a Performer and see how much you can save!</p>

                <p><a href="https://www.ticketliquidator.com/search?q=search-by-artist-or-event&ref=cj&utm_source=cj&utm_medium=aff&utm_campaign=8137399&xtor=AL-168-[cj]-[8137399]" target="_blank"> <img style="display:block; width:100%; height:auto; " src="' . base_url() . 'public/assets/img/find-tickets.jpg" alt=""></a></p>

                <p style="border:1px solid #000;"></p>
                <p><b style="font-size:14px; display:block; text-align:center; color:#fff;">NEED INSURANCE?</b></p>
                <p style="color:#fff;">We (<strong>HotelsDifferently</strong><sup>sm</sup>) do NOT offer travel insurance, however you can purchase travel insurance from a third party insurance company. Please note that we do not directly sell any types of insurance; we are solely an Affiliate to a third party insurance company.</p>
            ';
                        $mail_body .= '<a style="display:inline-block; verticle-align:top; max-width:150px;" href="http://www.dpbolvw.net/click-8137399-10892804-1466615700000" target="_top">
                                <img style="display:block; width:100%; height:auto;" src="http://www.ftjcfx.com/image-8137399-10892804-1466615700000" width="125" height="125" alt="Allianz Travel Insurance" border="0"/>
                            </a>
                            <a style="display:inline-block; verticle-align:top; max-width:150px;" href="http://www.jdoqocy.com/click-8137399-11779657-1466617745000" target="_top">
                                <img style="display:block; width:100%; max-width:150px; height:auto;" src="http://www.lduhtrp.net/image-8137399-11779657-1466617745000" width="300" height="100%" alt="Allianz Travel Insurance" border="0"/>
                            </a>
                            <a style="display:inline-block; verticle-align:top; max-width:150px;" href="http://www.tkqlhce.com/click-8137399-11176385-1466616611000" target="_top">
                                <img style="display:block; width:100%; max-width:150px; height:auto;" src="http://www.tqlkg.com/image-8137399-11176385-1466616611000" width="300" height="100%" alt="" border="0"/>
                            </a>';

                        $mail_body .= '
                <p style="border:1px solid #000;"></p>
                <h3 style="font-size:17px; margin:0 0 10px; font-weight:bold; text-align:center; color:#fff;">NEED A RIDE?</h3>
                <h3 style="font-size:14px; margin:15px 0 10px; font-weight:bold; text-align:center;color:#e00303;"><a href="https://www.uber.com/invite/8z1j5" target="_blank"><img src="' . base_url() . 'public/assets/img/uber-logo.png" alt="uber"></a></h3>
                            <h2 style="font-size:13px; margin:0 0 10px; color:#fff;">GET <span style="color:#fff; font-weight:bold;">$15 OFF OR MORE</span>depending on your location by signing up through the banner above! (or enter Promo Code <span style="color:#fff; font-weight:bold;">8z1j5</span> in the app)</h2>



                            <h3 style="font-size:14px; margin:0 0 10px; font-weight:bold; text-align:center;color:#e00303;"><a href="https://www.lyft.com/invite/SZILARD527631" target="_blank"><img src="' . base_url() . 'public/assets/img/lyft-logo.png" alt="LYFT"></a></h3>
                            <h2 style="font-size:13px; margin:0 0 10px; color:#fff;">GET <span style="color:#fff; font-weight:bold;">$50 OFF</span> towards your first rides by signing up through the banner above!</h2>

            ';
						$data['mail_body'] = $mail_body;
						$message = $this->load->view('admin/account/genral_emailtemplate', $data, TRUE);
						$this->email->message($message);
						$this->email->send();
						
						$CI = & get_instance();
						$CI->load->model('admin/admin_log_model');
						$log = array(
							'log_type' => 'approve_pen_reservation',
							'log_type_fk_id' => $id,
							'log_desc' => 'onRequest reservation approve confirmation code '.$res[0]->codeID
						);
						$CI->admin_log_model->addlog($log);
						$this->session->set_flashdata('success', "Reservation Status changed Successfully!");
						redirect('admin/pen_res');
					}
				}
				else
				{
					$data['error'] = "No such Id exist!";
				}
			}
			else if($action == 'decline')
			{
				$res = $this->db->where('id' , $id)->get('book_hotal')->result();
				if(count($res) > 0)
				{
					if($res[0]->pending_status == 0)
					{
						$stripRes = $this->stripeRefund($res[0]->stripe_charge);
						if($stripRes['success'] && isset($stripRes['refund_info']))
						{
							$dat = array(
								"pending_status" => 1,
								'refund_id' => $stripRes['refund_info']->id,
								'stripe_refund_resp' => serialize($stripRes['refund_info']->__toArray(true))
								
							);
							$this->db->where('id' , $id)->update('book_hotal' , $dat);
							$dat1 = array(
								'cancel_status' => 1,
								'admin_changed' => 0,
								'return_amount' => getBookingperRoomPaid($id),
								'stripe_no' => $stripRes['refund_info']->id,
							);
							$this->db->where('book_id' , $id)->update('booking_detail' , $dat1);
							
							$user1 = getUserById($res[0]->uid);
							$this->load->library('email');
							$this->email->set_mailtype("html");
							$this->email->from(NOREPLY, 'HotelsDifferently');
							$this->email->to($user1->var_email);
							$this->email->subject('Booking Declined');
							$mail_body = '<p style="color:#fff;margin-bottom:10px;">Dear '.$user1->var_fname . '</p>';
							$mail_body .='<p style="color:#fff;margin-bottom:10px;">Thank you for using <b>HotelsDifferently<sup>sm</sup></b>. At this time, we are unable to confirm your reservation request you submitted for '.$res[0]->hotelName.' for the dates of  '.date('Y-m-d',strtotime($res[0]->checkIn)).' till '.date('Y-m-d',strtotime($res[0]->checkOut)).'. We are truly sorry for not being able to secure this reservation for you and rest assured that your full refund was processed automatically by our system.</p>';
							$mail_body .='<p style="color:#fff;margin-bottom:10px;">Again, thank you for choosing us and please always give us a try before booking your next trip!</p>';
							$mail_body .= '<p style="color:#fff;margin-bottom:10px;">Sincerely,</br>';
							$mail_body .= '<p style="color:#fff;margin-bottom:10px;"><b>HotelsDifferently<sup>sm</sup></b><br>
								<small style="font-style: italic;">Making hotels happen for you!</small><b><sup>sm</sup></b>
								</p>';							
							$data['mail_body'] = $mail_body;
							$message = $this->load->view('admin/account/genral_emailtemplate', $data, TRUE);
							$this->email->message($message);
							$this->email->send();
							
							
							$CI = & get_instance();
							$CI->load->model('admin/admin_log_model');
							$log = array(
								'log_type' => 'decline_pen_reservation',
								'log_type_fk_id' => $id,
								'log_desc' => ' onRequest reservation decline confirmation code '.$res[0]->codeID
							);
							$CI->admin_log_model->addlog($log);
							$this->session->set_flashdata('success', "Reservation Status changed Successfully and Refund!");
							redirect('admin/pen_res');
						}
						else
						{
							$data['error'] = $stripRes['reason'];
						}
					}
				}
				else
				{
					$data['error'] = "No such Id exist!";
				}
			}
		}
        $data['page'] = "admin/pending_reservations/conformation_list";
        $data['breadcrumb'] = "Pending reservations";
        $data['breadcrumb_sub'] = "Pending reservations Sub";
        $data['breadcrumb_list'] = array(
            array('Pending reservations', ''),
        );
        $data['pen_res'] = 'class="start active"';
        $data['var_meta_title'] = 'Pending reservations';
        $data['var_meta_description'] = 'Pending reservations';
        $data['var_meta_keyword'] = 'Pending reservations';
        $data['js'] = array(
            'custom/jquery.maskMoney.js',
            'custom/table-managed.js',
            'admin/pending_reser.js',
            'custom/components-pickers.js',
        );
        $data['css_plugin'] = array(
            'select2/select2.css',
            'select2/select2-metronic.css',
            'data-tables/DT_bootstrap.css',
            'bootstrap-datepicker/css/datepicker.css',
        );
        $data['js_plugin'] = array(
            'select2/select2.min.js',
            'data-tables/jquery.dataTables.js',
            'data-tables/DT_bootstrap.js',
            'bootstrap-datepicker/js/bootstrap-datepicker.js',
        );
        $data['css'] = array(
        );
        $data['init'] = array(
            'TableManaged.init()',
            'Quote.init()',
            'ComponentsPickers.init()'
        );

//        $data['conform_res'] = $this->con_reservation->get_conres();        
//        if ($this->input->is_ajax_request()) {
//            $result = $this->toval->check_cookie_ajax($data['js'], $data['js_plugin'], $data['css'], $data['css_plugin']);
//            $data['js'] = $result['js'];
//            $data['js_plugin'] = $result['js_plugin'];
//        } else {
//            $result = $this->toval->check_cookie($data['js'], $data['js_plugin'], $data['css'], $data['css_plugin']);
//            $data['js'] = $result['js'];
//            $data['js_plugin'] = $result['js_plugin'];
//        }       
		$data['confReservations'] = $this->db->order_by("id", "desc")->where('book_status' , 'Request')->where('pending_status' , 0)/*->or_where('pending_status' , 1)*/->get('book_hotal')->result();
        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function datatable($fist = "", $secound = "") 
	{
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
        $this->load->library('Datatables');
        if ($fist != 0) {
            $from = $this->datatables->where('dz_quote.var_checkin', $fist);
        }
        if ($secound != 0) {
            $to = $this->datatables->where('dz_quote.var_checkout', $secound);
        }
        $this->datatables->select('
            dz_rplyquote.var_createddate,
            dz_user.var_username,
            dz_user.var_email,
            dz_user.var_accountid,
            dz_rplyquote.var_transactionid,
            dz_rplyquote.var_prize,
            dz_rplyquote.int_glcode,
            dz_rplyquote.int_fkquote,
            dz_rplyquote.chr_status,
            dz_rplyquote.fk_coupon,
            dz_rplyquote.var_src,
            dz_rplyquote.int_src_price,
            dz_user.var_fname,
            dz_user.var_lname
           
            ');
        $this->datatables->from('dz_rplyquote');
        $this->datatables->where('dz_rplyquote.chr_status', 'P');
        $this->datatables->where('dz_rplyquote.var_admin_soures', NULL);
        $this->datatables->where('dz_rplyquote.var_admin_amount', NULL);
        $this->datatables->where('dz_rplyquote.var_soures_confirmation', NULL);
        $this->datatables->join('dz_quote', 'dz_rplyquote.int_fkquote = dz_quote.int_glcode');
        $this->datatables->join('dz_user', 'dz_quote.var_fkuser = dz_user.int_glcode');
//        $this->datatables->order_by('var_createddate', 'DESC');
        //$this->datatables->join('dz_commision', 'dz_rplyquote.int_fkquote=dz_commision.fk_quote');
        $results = $this->datatables->generate();
        $results = json_decode($results, true);
//          print_r($results['aaData']);
//           exit();
        for ($i = 0; $i < count($results['aaData']); $i++) {
            $price = "";
            if ($results['aaData'][$i][9] != "" && $results['aaData'][$i][9] != 0) {
                $price = $this->getfinalamount1($results['aaData'][$i][5], $results['aaData'][$i][9]);
            } else {
                $price = $results['aaData'][$i][5];
            }
            $email = $results['aaData'][$i][1];
            $quotid = $results['aaData'][$i][6];
            //  $data['finalamount'] = $this->getfinalamount1($data['confirm'][0]['var_prize'],$data['confirm'][0]['fk_coupon']);
            $results['aaData'][$i][0] = $results['aaData'][$i][0];
            $results['aaData'][$i][1] = $results['aaData'][$i][1];
            $results['aaData'][$i][2] = $results['aaData'][$i][2];
            $results['aaData'][$i][3] = $results['aaData'][$i][3];
            $results['aaData'][$i][4] = $results['aaData'][$i][4];
            $results['aaData'][$i][5] = $price;
            $results['aaData'][$i][6] = '<a href="#myModal_autocomplete" data-toggle="modal" data-id="' . $quotid . '" id="conform_list" class="sentquote label label-sm label-success">View</a> 
                <a href="#myModal_paid" data-toggle="modal" src="'.$results['aaData'][$i][10].'" srcint="'.$results['aaData'][$i][11].'" data-id="' . $quotid . '" id="paid_list" class="paid_list label label-sm label-success">Paid</a>
                    <a href="javascript:;" data-toggle="modal" name="'.$results['aaData'][$i][12]." ".$results['aaData'][$i][13].'" email="'.$results['aaData'][$i][2].'" data-id="' . $quotid . '" id="cancel_list" class="cancle_list label label-sm label-success">Cancel</a>';
//            $results['aaData'][$i][6] = '<a href="#myModal_paid" data-toggle="modal" data-id="' . $quotid . '" id="paid_list" class="sentquote label label-sm label-success">Paid</a>';
        }
//        print_r($results);exit;
        echo json_encode($results);
    }

    function getconfirm_res() 
	{
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
        $id = $this->input->post('id');
        $userid = $this->input->post('userid');
        
        $this->db->select('
             dz_quote.var_checkin,
             dz_quote.var_checkout,
             dz_quote.var_city,
             dz_quote.var_rating,
             dz_quote.var_comments,
             dz_quote.var_room,
             dz_quote.var_adult,
             dz_quote.var_child,
             dz_quote.var_hotelname,
             dz_rplyquote.fk_coupon,
             dz_rplyquote.var_prize,
             dz_rplyquote.var_NOH,
             dz_rplyquote.var_prize,
             dz_rplyquote.var_uniq_quoteid,
             dz_rplyquote.var_policy,
             dz_rplyquote.var_star,
             dz_rplyquote.var_transactionid,
             dz_rplyquote.var_tripadvisor,
             dz_rplyquote.var_src,
             dz_rplyquote.int_src_price,
             dz_rplyquote.var_admin_soures,
             dz_rplyquote.var_admin_amount,
             dz_rplyquote.var_soures_confirmation,
             dz_rplyquote.var_comment,
             dz_rplyquote.var_cancalationtype,
             dz_rplyquote.var_refundamount,
             dz_rplyquote.var_updateddate
             ');
        $this->db->from('dz_rplyquote');
        $this->db->where('dz_rplyquote.int_glcode', $id);
        $this->db->join('dz_quote', 'dz_rplyquote.int_fkquote = dz_quote.int_glcode');
        
      //  $this->db->join('dz_user', 'dz_rplyquote.prosseduser_id = dz_user.int_glcode');
//      $this->db->join('dz_commision', 'dz_rplyquote.int_fkquote = dz_commision.fk_quote');
        $data['confirm'] = $this->db->get()->result_array();
        
        
        $this->db->select('*');
        $this->db->where('int_glcode',$userid);
        $this->db->from('dz_user');
        $data['userdetail'] = $this->db->get()->result_array();
        
        if ($data['confirm'][0]['fk_coupon'] != "" && $data['confirm'][0]['fk_coupon'] != 0) {
            $data['finalamount'] = $this->getfinalamount1($data['confirm'][0]['var_prize'], $data['confirm'][0]['fk_coupon']);
            $this->db->select('*');
            $this->db->where('int_glcode', $data['confirm'][0]['fk_coupon']);
            $coupan = $this->db->get('dz_coupons')->result_array();

            if ($coupan[0]['var_coupan_type'] == 1) {
                // echo $coupan[0]['var_couponvalue'] - $this->input->post('price');
                $coupanstr = $coupan[0]['var_couponvalue'] . "% OFF";
            }
            if ($coupan[0]['var_coupan_type'] == 2) {
                $coupanstr = "$" . $coupan[0]['var_couponvalue'] . ' OFF';
            }
            $data['coupanstr'] = $coupanstr;
        } else {
            $data['finalamount'] = $data['confirm'][0]['var_prize'];
            $data['coupanstr'] = 'No coupons';
        }
//        print_r($data['confirm']);exit;
//        $data['confirm'] = $this->con_reservation->confirm_getdata($id); 
      //  print_r($data);exit;
        $this->load->view('admin/pending_reservations/popupquoteslist', $data);
    }

    function getfinalamount1($paid1, $coupanid) 
	{
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
        //  if($this->input->post()){
        $this->db->select('*');
        $this->db->where('int_glcode', $coupanid);
        $coupan = $this->db->get('dz_coupons')->result_array();

        if ($coupan[0]['var_coupan_type'] == 1) {
            // echo $coupan[0]['var_couponvalue'] - $this->input->post('price');
            $discoutamount = floatval(($coupan[0]['var_couponvalue'] * $paid1) / 100);
            $discoutamount = number_format((float) $discoutamount, 2, '.', '');
            $acutelamount = $paid1 - $discoutamount;
            $acutelamount = number_format((float) $acutelamount, 2, '.', '');
            return $acutelamount;
        }
        if ($coupan[0]['var_coupan_type'] == 2) {
            $acutelamount = $paid1 - $coupan[0]['var_couponvalue'];
            $acutelamount = number_format((float) $acutelamount, 2, '.', '');
            return $acutelamount;
        }
        //  }
    }

    function add_pending() 
	{
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
        if ($this->input->post()) {
//            $return_data = $this->con_reservation->add_pending($this->input->post());
//            if ($return_data) {
//                $return = array('status' => 'success');
//            } else {
//                $return = array('status' => 'error');
//            }
//            echo json_encode($return);
            
            $id = $this->input->post('editid');
                $array = array(
                    'var_admin_soures' => $this->input->post('source'),
                    'var_admin_amount' => $this->input->post('amount'),
                    'var_soures_confirmation' => $this->input->post('source_confirmation')
                );
            $this->db->where('int_glcode', $id);
            $edit = $this->db->update('dz_rplyquote', $array);

//            $id = $this->input->post('editid');
            $this->db->select('
             dz_user.*,
             dz_quote.var_fkuser,
             dz_quote.var_checkin,
             dz_quote.var_checkout,
             dz_quote.var_city,
             dz_quote.var_rating,
             dz_quote.var_comments,
             dz_quote.var_room,
             dz_quote.var_adult,
             dz_quote.var_child,
             dz_quote.var_hotelname,
             dz_rplyquote.fk_coupon,
             dz_rplyquote.var_prize,
             dz_rplyquote.var_NOH,
             dz_rplyquote.var_prize,
             dz_rplyquote.var_uniq_quoteid,
             dz_rplyquote.var_policy,
             dz_rplyquote.var_star,
             dz_rplyquote.var_transactionid,
             dz_rplyquote.var_tripadvisor,
             dz_rplyquote.var_src,
             dz_rplyquote.int_src_price,
             dz_rplyquote.var_admin_soures,
             dz_rplyquote.var_admin_amount,
             dz_rplyquote.var_soures_confirmation,
             dz_rplyquote.var_transactionid,
             dz_rplyquote.var_comment,
             ');
            $this->db->from('dz_rplyquote');
            $this->db->where('dz_rplyquote.int_glcode', $id);
            $this->db->join('dz_quote', 'dz_rplyquote.int_fkquote = dz_quote.int_glcode');
            $this->db->join('dz_user', 'dz_user.int_glcode = dz_quote.var_fkuser');
//      $this->db->join('dz_commision', 'dz_rplyquote.int_fkquote = dz_commision.fk_quote');
            $data['confirm'] = $this->db->get()->result_array();
            
        if ($data['confirm'][0]['fk_coupon'] != "" && $data['confirm'][0]['fk_coupon'] != 0)
        {
                $data['finalamount'] = $this->getfinalamount1($data['confirm'][0]['var_prize'], $data['confirm'][0]['fk_coupon']);
                $this->db->select('*');
                $this->db->where('int_glcode', $data['confirm'][0]['fk_coupon']);
                $coupan = $this->db->get('dz_coupons')->result_array();

                if ($coupan[0]['var_coupan_type'] == 1) {
                    // echo $coupan[0]['var_couponvalue'] - $this->input->post('price');
                    $coupanstr = $coupan[0]['var_couponvalue'] . "% OFF";
                }
                if ($coupan[0]['var_coupan_type'] == 2) {
                    $coupanstr = "$" . $coupan[0]['var_couponvalue'] . ' OFF';
                }
                $data['coupanstr'] = $coupanstr;
        } else {
            $data['finalamount'] = $data['confirm'][0]['var_prize'];
            $data['coupanstr'] = 'No coupons';
        }
        $confirmresarvationdetail = $this->load->view('admin/account/confirmresarvationdetail', $data, TRUE);
            $this->db->select('*');
            $this->db->where('int_glcode', $data['confirm'][0]['var_fkuser']);
            $user = $this->db->get('dz_user')->result_array();
            
            $temp = time();
            $mail_body = '';
            $mail_body.='<p style="color:#fff;margin-bottom:10px;">Dear ' . $data['confirm'][0]['var_fname'].' '. $data['confirm'][0]['var_lname'] . ',</p>';    
            $mail_body.='<p style="color:#fff;margin-bottom:10px;"><b>CONGRATULATIONS! Your reservation is booked!</b></p>';
            $mail_body.='<p style="color:#fff;margin-bottom:10px;"><span style="color: #fff;">Thank you for booking with <b>HotelsDifferently!<sup>sm</sup></b> We are delighted that you decided to choose us for your next getaway.</span></p>';
            $mail_body.= '<p style="color:#fff;margin-bottom:10px;"><span style="color: #fff;">Your charge will appear on your statement as: <b>HOTELSDIFFERENTLY</b></span></p>';
            $mail_body.= '<p style="color:#fff;margin-bottom:10px;"><span style="color: #fff;">Your reservation confirmation number is:  <b>'.$data['confirm'][0]['var_transactionid'] .'</b></span></p>';
            $mail_body.= '<p style="color:#fff;margin-bottom:10px;"><span style="color: #fff;">Once again, here are the details of your reservation: </span></p>';
            $mail_body.= $confirmresarvationdetail; 
            
            $mail_body.= '<div style="clear: both"></div><p style="color:#fff;">Sincerely,<br/><b>HotelsDifferently<sup>sm</sup></b>  <br/><span style="font-size:9px;">YOUR VACATION BEGINS HERE!</spam></p>';
            $mail_body .= '<p style="color:#fff;margin-bottom:10px;font-size:9px;">Cancellation policies between <b>HotelsDifferently<sup>sm</sup></b> quotes and competitor sites may vary.</p>';
         
            $data['mail_body'] = $mail_body;
            $html = $this->load->view('admin/account/genral_emailtemplate', $data, TRUE);
        //    echo $html;
       //    exit();
            $this->email->set_mailtype("html");
             $this->email->from(NOREPLY, 'HotelsDifferently');
             //$this->email->from('testmpatel@gmail.com');
            
            $this->email->to($user[0]['var_email']);
            $this->email->subject("Your reservation is confirmed!");
            $this->email->message($html);
            $emailsend = $this->email->send();
//            if ($this->email->send()) {
//                echo $this->email->print_debugger();
//            } else {
//                echo $this->email->print_debugger();
//            }
//            echo $this->db->last_query();exit;
            if ($emailsend) {
                echo 'success';
                exit;
            } else {
                echo 'error';
                exit;
            }
        }
    }
    
    function cancelquote()
	{
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
        $replyid = $this->input->post('replayid');
        $name = $this->input->post('name');
        $email = $this->input->post('email');
        
        $updatevalue = array(
            'var_createddate' => $paymentdate = date("Y-m-d H:i:s", strtotime("-3 day")),
            'chr_status' => "AC",
            'var_cancalationtype' => "Admin Cancelation"
        );
         $this->db->where('int_glcode', $replyid);
        $result = $this->db->update('dz_rplyquote', $updatevalue);
            $mail_body = '';
            $mail_body.='<p style="color:#fff;margin-bottom:10px;">Dear ' . $name. ',</p>';    
            $mail_body.='<p style="color:#fff;margin-bottom:10px;">Thank you for using <b>HotelsDifferently<sup>sm</sup></b>. We would like to let you know that your reservation cannot be secured for the rate you booked at this time. Hotel rates and room availability can fluctuate from minute to minute based on many factors, including how many rooms a hotel has available at a precise point in time. We have automatically refunded your payment and the confirmation email from PayPal<sup>TM</sup> will arrive soon acknowledging the processed refund. Should you be interested in booking this specific room, please feel free to RESUBMIT your quote request. You can resubmit your quote request with only a click of a button from RESERVATIONS – QUOTES.</p>';
            $mail_body.='<p style="color:#fff;margin-bottom:10px;">Again, thank you for choosing us and please always give us a try before booking your next trip and we DO apologize for the inconveniences this may have caused you!</p>';
            $mail_body.= '<p style="color:#fff;">Sincerely,<br/><b>HotelsDifferently<sup>sm</sup></b>  <br/><span style="font-size:9px;">YOUR VACATION BEGINS HERE!</spam></p>';
            $mail_body .= '<p style="color:#fff;margin-bottom:10px;font-size:9px;">Cancellation policies between <b>HotelsDifferently<sup>sm</sup></b> quotes and competitor sites may vary.</p>';
         
            $data['mail_body'] = $mail_body;
            $html = $this->load->view('admin/account/genral_emailtemplate', $data, TRUE);
        //    echo $html;
       //    exit();
            $this->email->set_mailtype("html");
             $this->email->from(NOREPLY, 'HotelsDifferently ');
             //$this->email->from('testmpatel@gmail.com');
            
            $this->email->to($email);
            $this->email->subject("Your reservation is canceled!");
            $this->email->message($html);
            $emailsend = $this->email->send();
            
        if($emailsend)
        {
            echo 'success';
        }
        else{
            echo 'error';
        }
    }
}
?>