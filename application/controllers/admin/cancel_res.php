<?php

class Cancel_res extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin/con_reservation');
    }

    function index() 
	{
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
//        if($this->blobmenuarray1['Conform Reservation'] != 'yes')
//        {
//            exit();
//        }
        $data['page'] = "admin/cancel_reservations/cancel_list";
        $data['breadcrumb'] = "Cancel reservations";
        $data['breadcrumb_sub'] = "Cancel reservations Sub";
        $data['breadcrumb_list'] = array(
            array('Cancel reservations', ''),
        );
        $data['can_res'] = 'class="start active"';
        $data['var_meta_title'] = 'Cancel reservations';
        $data['var_meta_description'] = 'Cancel reservations';
        $data['var_meta_keyword'] = 'Cancel reservations';
        $data['js'] = array(
            'custom/jquery.maskMoney.js',
            'custom/table-managed.js',
            'admin/cancel_reser.js',
            'custom/components-pickers.js',
        );
        $data['css_plugin'] = array(
            'select2/select2.css',
            'select2/select2-metronic.css',
            'data-tables/DT_bootstrap.css',
            'bootstrap-datepicker/css/datepicker.css',
        );
        $data['js_plugin'] = array(
            'select2/select2.min.js',
            'data-tables/jquery.dataTables.js',
            'data-tables/DT_bootstrap.js',
            'bootstrap-datepicker/js/bootstrap-datepicker.js',
        );
        $data['css'] = array(
        );
        $data['init'] = array(
            'TableManaged.init()',
            'Cancel_reser.init()',
            'ComponentsPickers.init()'
        );

//        $data['conform_res'] = $this->con_reservation->get_conres();        
//        if ($this->input->is_ajax_request()) {
//            $result = $this->toval->check_cookie_ajax($data['js'], $data['js_plugin'], $data['css'], $data['css_plugin']);
//            $data['js'] = $result['js'];
//            $data['js_plugin'] = $result['js_plugin'];
//        } else {
//            $result = $this->toval->check_cookie($data['js'], $data['js_plugin'], $data['css'], $data['css_plugin']);
//            $data['js'] = $result['js'];
//            $data['js_plugin'] = $result['js_plugin'];
//        }       

        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function datatable($fist = "", $secound = "") 
	{
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
        
        $this->load->library('Datatables');
        if ($fist != 0) {
            $from = $this->datatables->where('dz_quote.var_checkin', $fist);
        }
        if ($secound != 0) {
            $to = $this->datatables->where('dz_quote.var_checkout', $secound);
        }
        $this->datatables->select('
            dz_rplyquote.var_updateddate,
            dz_user.var_username,
            dz_user.var_email,
            dz_user.var_accountid,
            dz_rplyquote.var_transactionid,
            dz_rplyquote.var_prize,
            dz_rplyquote.int_glcode,
            dz_rplyquote.int_fkquote,
            dz_rplyquote.chr_status,
            dz_rplyquote.fk_coupon,
          
           
            ');
        $this->datatables->from('dz_rplyquote');
        $this->datatables->where('dz_rplyquote.chr_status', 'C');
        $this->datatables->where('dz_rplyquote.var_cancalationtype', NULL);
//        $this->datatables->where('dz_rplyquote.var_admin_amount', NULL);
//        $this->datatables->where('dz_rplyquote.var_soures_confirmation', NULL);
        $this->datatables->join('dz_quote', 'dz_rplyquote.int_fkquote = dz_quote.int_glcode');
        $this->datatables->join('dz_user', 'dz_quote.var_fkuser = dz_user.int_glcode');
//        $this->datatables->order_by('var_createddate', 'DESC');
        //$this->datatables->join('dz_commision', 'dz_rplyquote.int_fkquote=dz_commision.fk_quote');
        $results = $this->datatables->generate();
        $results = json_decode($results, true);
//          print_r($results['aaData']);
//           exit();
        for ($i = 0; $i < count($results['aaData']); $i++) {
            $price = "";
            if ($results['aaData'][$i][9] != "" && $results['aaData'][$i][9] != 0) {
                $price = $this->getfinalamount1($results['aaData'][$i][5], $results['aaData'][$i][9]);
            } else {
                $price = $results['aaData'][$i][5];
            }
            $email = $results['aaData'][$i][1];
            $quotid = $results['aaData'][$i][6];
            //  $data['finalamount'] = $this->getfinalamount1($data['confirm'][0]['var_prize'],$data['confirm'][0]['fk_coupon']);
            $results['aaData'][$i][0] = $results['aaData'][$i][0];
            $results['aaData'][$i][1] = $results['aaData'][$i][1];
            $results['aaData'][$i][2] = $results['aaData'][$i][2];
            $results['aaData'][$i][3] = $results['aaData'][$i][3];
            $results['aaData'][$i][4] = $results['aaData'][$i][4];
            $results['aaData'][$i][5] = $price;
            $results['aaData'][$i][6] = '<a href="#myModal_autocomplete" data-toggle="modal" data-id="' . $quotid . '" id="conform_list" class="sentquote label label-sm label-success">View</a>
                <a style="margin-left:10px;" href="#myModal_paid" data-toggle="modal" data-id="' . $quotid . '" id="paid_list" class="okbutton label label-sm label-success">OK</a>
                    <a style="margin-left:10px;" href="javascript:;" data-id="' . $quotid . '" id="revart" class="revart label label-sm label-success">Revert</a>';
//            $results['aaData'][$i][6] = '<a href="#myModal_paid" data-toggle="modal" data-id="' . $quotid . '" id="paid_list" class="sentquote label label-sm label-success">Paid</a>';
        }
//        print_r($results);exit;
        echo json_encode($results);
    }
    function getconfirm_res() 
	{
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
        $id = $this->input->post('id');
        $this->db->select('
             dz_quote.var_checkin,
             dz_quote.var_checkout,
             dz_quote.var_city,
             dz_quote.var_rating,
             dz_quote.var_comments,
             dz_quote.var_room,
             dz_quote.var_adult,
             dz_quote.var_child,
             dz_quote.var_hotelname,
             dz_rplyquote.fk_coupon,
             dz_rplyquote.var_prize,
             dz_rplyquote.var_NOH,
             dz_rplyquote.var_prize,
             dz_rplyquote.var_uniq_quoteid,
             dz_rplyquote.var_policy,
             dz_rplyquote.var_star,
             dz_rplyquote.var_transactionid,
             dz_rplyquote.var_tripadvisor,
             dz_rplyquote.var_src,
             dz_rplyquote.int_src_price,
             dz_rplyquote.var_admin_soures,
             dz_rplyquote.var_admin_amount,
             dz_rplyquote.var_soures_confirmation,
             dz_rplyquote.var_comment,
             ');
        $this->db->from('dz_rplyquote');
        $this->db->where('dz_rplyquote.int_glcode', $id);
        $this->db->join('dz_quote', 'dz_rplyquote.int_fkquote = dz_quote.int_glcode');
//      $this->db->join('dz_commision', 'dz_rplyquote.int_fkquote = dz_commision.fk_quote');
        $data['confirm'] = $this->db->get()->result_array();
        if ($data['confirm'][0]['fk_coupon'] != "" && $data['confirm'][0]['fk_coupon'] != 0) {
            $data['finalamount'] = $this->getfinalamount1($data['confirm'][0]['var_prize'], $data['confirm'][0]['fk_coupon']);
            $this->db->select('*');
            $this->db->where('int_glcode', $data['confirm'][0]['fk_coupon']);
            $coupan = $this->db->get('dz_coupons')->result_array();

            if ($coupan[0]['var_coupan_type'] == 1) {
                // echo $coupan[0]['var_couponvalue'] - $this->input->post('price');
                $coupanstr = $coupan[0]['var_couponvalue'] . "% OFF";
            }
            if ($coupan[0]['var_coupan_type'] == 2) {
                $coupanstr = "$" . $coupan[0]['var_couponvalue'] . ' OFF';
            }
            $data['coupanstr'] = $coupanstr;
        } else {
            $data['finalamount'] = $data['confirm'][0]['var_prize'];
            $data['coupanstr'] = 'No coupons';
        }
//        print_r($data['confirm']);exit;
//        $data['confirm'] = $this->con_reservation->confirm_getdata($id); 
      //  print_r($data);exit;
        $this->load->view('admin/pending_reservations/popupquoteslist', $data);
    }
    function getfinalamount1($paid1, $coupanid) 
	{
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
        //  if($this->input->post()){
        $this->db->select('*');
        $this->db->where('int_glcode', $coupanid);
        $coupan = $this->db->get('dz_coupons')->result_array();

        if ($coupan[0]['var_coupan_type'] == 1) {
            // echo $coupan[0]['var_couponvalue'] - $this->input->post('price');
            $discoutamount = floatval(($coupan[0]['var_couponvalue'] * $paid1) / 100);
            $discoutamount = number_format((float) $discoutamount, 2, '.', '');
            $acutelamount = $paid1 - $discoutamount;
            $acutelamount = number_format((float) $acutelamount, 2, '.', '');
            return $acutelamount;
        }
        if ($coupan[0]['var_coupan_type'] == 2) {
            $acutelamount = $paid1 - $coupan[0]['var_couponvalue'];
            $acutelamount = number_format((float) $acutelamount, 2, '.', '');
            return $acutelamount;
        }
        //  }
    }
    
    function revart()
    {
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
         $id = $this->input->post('id');
         $array = array('chr_status' => 'P');
         $this->db->where('int_glcode', $id);
         $update = $this->db->update('dz_rplyquote', $array);
            
         $updatedata =  $this->con_reservation->createcharjbackrevart($id);
         
         if($updatedata)
         {
             echo 'success';
             exit();
         }
    }
}

?>