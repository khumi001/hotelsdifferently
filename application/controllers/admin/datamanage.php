<?php

class Datamanage extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin/datamanage_model');
        $this->load->library('datatables');
    }

    function index() 
	{
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
        if($this->blobmenuarray1['Database Management'] != 'yes')
        {
            exit();
        }
        $data['page'] = "admin/datamanage/hotellist";
        $data['breadcrumb'] = "Hotel List";
        $data['breadcrumb_sub'] = "";
        $data['breadcrumb_list'] = array(
            array('Hotel List', '')
        );
        $data['datamanage'] = 'class="start active"';
        $data['var_meta_title'] = 'Hotel List';
        $data['var_meta_description'] = 'Hotel List';
        $data['var_meta_keyword'] = 'Hotel List';
        $data['js'] = array(
            'admin/datamanage.js',
            'custom/table-managed.js',
        );
        $data['js_plugin'] = array(
            'data-tables/jquery.dataTables.js',
            'data-tables/DT_bootstrap.js',
            'bootstrap-select/bootstrap-select.min.js',
            'select2/select2.min.js',
        );
        $data['css'] = array(
        );
        $data['css_plugin'] = array(
            'data-tables/DT_bootstrap.css',
            'bootstrap-select/bootstrap-select.min.css',
            'select2/select2.css',
            'select2/select2-metronic.css',
        );
        $data['init'] = array(
            'TableManaged.init()',
            'Datamanage.init()',
//            'Datamanage.int_delete()',
//            'Datamanage.int_get()',
//            'Datamanage.int_edit()',
        );
        if ($this->input->is_ajax_request()) {
            $result = $this->toval->check_cookie_ajax($data['js'], $data['js_plugin'], $data['css'], $data['css_plugin']);
            $data['js'] = $result['js'];
            $data['js_plugin'] = $result['js_plugin'];
        } else {
            $result = $this->toval->check_cookie($data['js'], $data['js_plugin'], $data['css'], $data['css_plugin']);
            $data['js'] = $result['js'];
            $data['js_plugin'] = $result['js_plugin'];
        }
//        $data['countrylist'] = $this->db->order_by('var_country', 'asc')->get('dz_country')->result_array();
        //$data['statelist'] = $this->db->get('dz_state')->result_array();
        //$data['citylist'] = $this->db->get('dz_city')->result_array();
        //$data['hotellist'] = $this->db->get('dz_hotel')->result_array();
        $this->load->view(ADMIN_LAYOUT, $data);
    }
    
     function hotellist_datatable() 
	 {
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
         
        $this->datatables
                ->select('
                           dz_hotel_list.var_hotel_name,
                           dz_hotel_list.var_city,
                           dz_hotel_list.var_state,
                           dz_hotel_list.var_country,
                           dz_hotel_list.var_address,
                           dz_hotel_list.var_category')
                ->from('dz_hotel_list');
                
        echo $this->datatables->generate();
//        $results = json_decode($results,true);
//        echo json_encode($results);
    }
    
    
    function addcountry() 
	{
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}

        if ($this->input->post('country') != "") {
            $result = $this->datamanage_model->countryadd($this->input->post());
            if ($result) {
                echo "success";
                exit;
            } else {
                echo "error";
                exit;
            }
        }
    }

    function deletecountry() 
	{
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
        if ($this->input->post()) {
            $result = $this->datamanage_model->countrydelete($this->input->post());
            if ($result) {
                echo "success";
                exit;
            } else {
                echo "error";
                exit;
            }
        }
    }

    function updatecountry() 
	{
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
        if ($this->input->post()) {
            $result = $this->datamanage_model->countryupdate($this->input->post());
            if ($result) {
                echo "success";
                exit;
            } else {
                echo "error";
                exit;
            }
        }
    }

    function addstate() 
	{
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
        if ($this->input->post('state') != "") {
            $result = $this->datamanage_model->stateadd($this->input->post());
            if ($result) {
                echo "success";
                exit;
            } else {
                echo "error";
                exit;
            }
        }
    }

    function deletestate() 
	{
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
        if ($this->input->post()) {
            $result = $this->datamanage_model->statedelete($this->input->post());
            if ($result) {
                echo "success";
                exit;
            } else {
                echo "error";
                exit;
            }
        }
    }

    function updatestate() 
	{
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
        if ($this->input->post()) {
            $result = $this->datamanage_model->stateupdate($this->input->post());
            if ($result) {
                echo "success";
                exit;
            } else {
                echo "error";
                exit;
            }
        }
    }

    function addcity() 
	{
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
        if ($this->input->post('city') != "") {
            $result = $this->datamanage_model->cityadd($this->input->post());
            if ($result) {
                echo "success";
                exit;
            } else {
                echo "error";
                exit;
            }
        }
    }

    function deletecity() 
	{
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
        if ($this->input->post()) {
            $result = $this->datamanage_model->citydelete($this->input->post());
            if ($result) {
                echo "success";
                exit;
            } else {
                echo "error";
                exit;
            }
        }
    }

    function updatecity() 
	{
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
        if ($this->input->post()) {
            $result = $this->datamanage_model->cityupdate($this->input->post());
            if ($result) {
                echo "success";
                exit;
            } else {
                echo "error";
                exit;
            }
        }
    }

    function addhotel() 
	{
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
        if ($this->input->post('hotel') != "") {
            $result = $this->datamanage_model->hoteladd($this->input->post());
            if ($result) {
                echo "success";
                exit;
            } else {
                echo "error";
                exit;
            }
        }
    }

    function deletehotel() 
	{
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
        if ($this->input->post()) {
            $result = $this->datamanage_model->hoteldelete($this->input->post());
            if ($result) {
                echo "success";
                exit;
            } else {
                echo "error";
                exit;
            }
        }
    }

    function updatehotel() 
	{
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
        if ($this->input->post()) {
            $result = $this->datamanage_model->hotelupdate($this->input->post());
            if ($result) {
                echo "success";
                exit;
            } else {
                echo "error";
                exit;
            }
        }
    }

    function get_state() 
	{
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
        $result = $this->db->order_by('var_state', 'asc')->get_where('dz_state', array('fk_country' => $_POST['countryid']))->result_array();
        if (count($result) == 0) {
            echo "Null";
            exit();
        } else {
            $html = '<div class="list_box radio-list"> <ul>';
            for ($i = 0; $i < count($result); $i++) {
                $html .= '<li class="state_sel" data-id='.$result[$i]['int_glcode'].'>'.$result[$i]['var_state'].'</li>';
            }
            $html .= "</ul></div>";
            echo $html;
            exit();
        }
    }

    function get_city() 
	{
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
        $result = $this->db->order_by('var_city', 'asc')->get_where('dz_city', array('fk_country' => $_POST['countryid'], 'fk_state' => $_POST['stateid']))->result_array();
        $html = '<div class="list_box radio-list"> <ul>';
        for ($i = 0; $i < count($result); $i++) {
            $html .= "<li class='city_sel' data-id=" . $result[$i]['int_glcode'] . ">" . $result[$i]['var_city'] . "</li>";
        }
        $html .= "</ul></div>";
        echo $html;
        exit();
    }

    function get_hotel() 
	{
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
//        print_
        $result = $this->db->order_by('var_hotelname','asc')->get_where('dz_hotel', array('fk_country' => $_POST['countryid'], 'fk_state' => $_POST['stateid'], 'fk_city' => $_POST['cityid']))->result_array();
        $html = '<div class="list_box radio-list"> <ul>';
        for ($i = 0; $i < count($result); $i++) {
            $html .= "<li class='hotel_sel' data-id=" . $result[$i]['int_glcode'] . ">" . $result[$i]['var_hotelname'] . "</li>";
        }
        $html .= "</ul></div>";
        echo $html;
        exit();
    }

}

?>