<?php

class Manualentry extends Admin_Controller {

    function __construct() {

        parent::__construct();
        $this->load->model('admin/manualentry_model');
    }

    function index() 
	{
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
        if($this->blobmenuarray1['Promo Codes'] != 'yes')
        {
            exit();
        }
        $data['page'] = "admin/manual/list";
        $data['breadcrumb'] = "Manual Entry";
        $data['breadcrumb_sub'] = "Manual Entry";
        $data['breadcrumb_list'] = array(
            array('Promo Codes', ''),
        );
        $data['manual'] = 'class="start active"';
        $data['var_meta_title'] = 'Manual';
        $data['var_meta_description'] = 'Manual';
        $data['var_meta_keyword'] = 'Manual';
        $data['js'] = array(
            'custom/table-managed.js',
			'custom/jquery.maskMoney.js',
            'admin/saving.js',
        );
        $data['js_plugin'] = array(
            'select2/select2.min.js',
        );
        $data['css_plugin'] = array(
            'select2/select2.css',
            'select2/select2-metronic.css',
        );
        $data['css'] = array(
        );
        $data['init'] = array(
          'Saving.init()',
        );
        if ($this->input->is_ajax_request()) {
            $result = $this->toval->check_cookie_ajax($data['js'], $data['js_plugin'], $data['css'], $data['css_plugin']);
            $data['js'] = $result['js'];
            $data['js_plugin'] = $result['js_plugin'];
        } else {
            $result = $this->toval->check_cookie($data['js'], $data['js_plugin'], $data['css'], $data['css_plugin']);
            $data['js'] = $result['js'];
            $data['js_plugin'] = $result['js_plugin'];
        }
        $result =  $this->db->select('*')
             ->from('dz_promocode')
             ->get()->result_array();
//print_r($result);

         $data['promocode'] = $result;
        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function searchQuets() 
	{
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
         if($this->input->post()){
            $result = $this->manualentry_model->get_quetes($this->input->post());
            
            foreach ($result as $row) {
                if($row['int_site1_price'] < $row['int_site2_price']){
                    $price = $row['int_site1_price'];
                } else{
                    $price = $row['int_site2_price'];
                }
            }
            
           if($result[0]['fk_coupon'] != "")
           {
                $this->db->where('int_glcode',$result[0]['fk_coupon']);
                $coupan = $this->db->get('dz_coupons')->result_array(); 
                
                if($coupan[0]['var_coupan_type'] == 1)
                {
                    $acutelamount = floatval(($coupan[0]['var_couponvalue'] * $result[0]['var_prize'])/100);
                }
                if($coupan[0]['var_coupan_type'] == 2){
                    $acutelamount = $coupan[0]['var_couponvalue'];
                }
                // $save =  $price - $row['var_prize'] + $acutelamount ;
                 $save =  $price - $row['var_prize'];
           }
           else{
               $save =  $price - $row['var_prize'] ;
           }
           
        
           $res = array(
               'fkid'=>$result[0]['int_glcode'],
               'username'=>$result[0]['var_username'],
               'quote'=>$row['var_prize'],
               'price'=>$price,
               'save'=>$save);
           echo json_encode($res);exit(); 
        }   
    }
    
    function insertQuets() 
	{
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
        if ($this->input->post()) {
            $result = $this->manualentry_model->insert_data($this->input->post());
            if ($result) {
                echo "success";
            } else {
                echo "error";
            }
        }
    }
    
    function addpromocode()
    {
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
        if ($this->input->post()) {
            $result = $this->manualentry_model->addpromocode($this->input->post());
            if ($result) {
                echo "success";
            } else {
                echo "error";
            }
        }
    }
    
    function delete_promo()
	{
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
        if($this->input->post('id')){
            $result = $this->manualentry_model->deletepromocode($this->input->post());
            if($result){
                echo "success";
            }else{
                echo "error";
            }
        }
    }
    
    
}

?>