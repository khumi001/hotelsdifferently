<?php
class Coupons extends Admin_Controller{
    function __construct() {
        parent::__construct();
        $this->load->model('admin/coupon_model');
    }
    function index()
	{
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
        $data['page'] = "admin/coupons/couponslist";
        $data['breadcrumb'] = "Coupons";
        $data['breadcrumb_sub'] = "Coupons Sub";
        $data['breadcrumb_list']= array(
            array('Coupons',''),            
        );
        $data['coupons'] = 'class="start active"';
	$data['var_meta_title'] = 'Coupons';
	$data['var_meta_description'] = 'Coupons';
	$data['var_meta_keyword'] = 'Coupons';
        $data['js'] = array(
            'custom/table-managed.js',
            'custom/components-pickers.js',
            'admin/coupon.js',
        );
        $data['css_plugin'] = array(
            'select2/select2.css',     
            'select2/select2-metronic.css',
            'data-tables/DT_bootstrap.css',
            'bootstrap-datepicker/css/datepicker.css',    
            'bootstrap-switch/css/bootstrap-switch.min.css',
        );
        $data['js_plugin'] = array(
          'select2/select2.min.js',
          'data-tables/jquery.dataTables.js',
          'data-tables/DT_bootstrap.js', 
          'bootstrap-datepicker/js/bootstrap-datepicker.js', 
          'bootstrap-switch/js/bootstrap-switch.min.js',  
        );
        $data['css'] = array(
        );        
        $data['init'] = array(
            'TableManaged.init()',
            'ComponentsPickers.init()', 
            'Coupon.init()',
        );
        
        $data['coupon_status'] = $this->db->select('chr_status')->get('dz_singup_coupons')->row_array();
        
        $this->db->select('dz_user.var_accountid,dz_user.var_username,dz_user.var_email,dz_coupons.int_glcode,dz_coupons.var_couponcode,dz_coupons.var_couponvalue,dz_coupons.var_status,dz_user.int_glcode as userid');
        $this->db->from('dz_user');
        $this->db->join('dz_coupons','dz_coupons.fk_user = dz_user.int_glcode');        
        $data['userlist'] = $this->db->get()->result_array();
         if ($this->input->is_ajax_request()) {
            $result = $this->toval->check_cookie_ajax($data['js'], $data['js_plugin'], $data['css'], $data['css_plugin']);
            $data['js'] = $result['js'];
            $data['js_plugin'] = $result['js_plugin'];
        } else {
            $result = $this->toval->check_cookie($data['js'], $data['js_plugin'], $data['css'], $data['css_plugin']);
            $data['js'] = $result['js'];
            $data['js_plugin'] = $result['js_plugin'];
        }
    $this->load->view(ADMIN_LAYOUT, $data);    
    }
    function coupon_generate()
	{
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
       if($this->input->post('status')!= ""){
           print_r($_POST); exit();
           $result = $this->coupon_model->coupon_generate($this->input->post());
        if($result){
            echo 'success';
        }else{
            echo 'error';
        }
       }
    }
    
    function update_status()
	{
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
        if($this->input->post()){
            $res = $this->coupon_model->status_update($this->input->post());
        }
    }
    
    function singup_status()
	{
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
        if($this->input->post()){
            $result = $this->coupon_model->status_singup();
            if($result){
                echo "success"; 
            }else{
                echo "error";
            }
        }
    } 
}
?>
