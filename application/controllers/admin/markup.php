<?php
class Markup extends Admin_Controller {



    function __construct()
    {
        parent::__construct();
    }

    function index()
    {

        $data['page'] = "admin/profile/usermarkup";

        $data['breadcrumb'] = "New";

        $data['breadcrumb_sub'] = "A";

        $data['breadcrumb_list'] = array(

            array('User Markup', ''),

        );
        $data['js'] = array(
            'custom/jquery.maskMoney.js',
        );

        $data['pen_res'] = 'class="start active"';

        $data['var_meta_title'] = 'User Markup';

        $data['var_meta_description'] = 'User Markup';

        $data['var_meta_keyword'] = 'User Markup';

        if($this->input->post()){
            $tf_en = $this->input->post('TF_EN');
            $user = $this->input->post('U');
            $this->db->query("UPDATE markups SET percentage = '$tf_en' WHERE userType = 'TF' OR userType = 'EN'");
            $this->db->query("UPDATE markups SET percentage = '$user' WHERE userType = 'U'");
        }

        $records = $this->db->select('*')->get('markups')->result_array();
        $values = array();
        foreach ($records as $val) {
            if($val['userType'] == 'TF' || $val['userType'] == 'EN'){
                $values['TF_EN'] = $val['percentage'];
            }else{
                $values['U'] = $val['percentage'];
            }
        }
        $data['records'] = $values;

        $this->load->view(ADMIN_LAYOUT, $data);

    }

}

?>