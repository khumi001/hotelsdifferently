<?php
class Signup extends Admin_Controller
{
    function __construct() {
        parent::__construct();         
        $this->load->model('signup/signup_model');
    }
    function index(){
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
        $data['page'] = "admin/signup";
        $data['breadcrumb'] = "Signup";
        $data['breadcrumb_sub'] = "";
        $data['breadcrumb_list']= array(
            array('Signup','')           
        );
        $data['signup'] = 'class="start active"';
	$data['var_meta_title'] = 'Signup';
	$data['var_meta_description'] = 'Signup';
	$data['var_meta_keyword'] = 'Signup';
        $data['js'] = array(      
            'core/app.js',
            'custom/form-wizard.js',
        );
        $data['js_plugin'] = array(  
//            'jquery-validation/dist/jquery.validate.min.js',
//            'jquery-validation/dist/additional-methods.min.js',
            'bootstrap-wizard/jquery.bootstrap.wizard.min.js',
            'select2/select2.min.js',
        );
        $data['css'] = array(            
        );
        $data['css_plugin'] = array( 
            'select2/select2.css',
            'select2/select2-metronic.css',
        );                
        $data['init'] = array(  
            ' FormWizard.init()',
        );      
         if ($this->input->is_ajax_request()) {
            $result = $this->toval->check_cookie_ajax($data['js'], $data['js_plugin'], $data['css'], $data['css_plugin']);
            $data['js'] = $result['js'];
            $data['js_plugin'] = $result['js_plugin'];
        } else {
            $result = $this->toval->check_cookie($data['js'], $data['js_plugin'], $data['css'], $data['css_plugin']);
            $data['js'] = $result['js'];
            $data['js_plugin'] = $result['js_plugin'];
        }
        $this->load->view(ADMIN_LAYOUT, $data);   
    }
    function signup_con()
    {
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
        $result = $this->signup_model->signup_mod($this->input->post());
        if($result)
        {
            echo "success";exit;
        }
        else
        {
             echo "error";exit;
        }
    }
}
?>