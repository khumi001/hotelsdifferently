<?php

class Regular_users extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin/enrollment_model');
    }

    function index() {
        $data['page'] = "admin/regular_users/pending_enrollments";
        $data['breadcrumb_list'] = array(
            array('Regular Users - Pending Enrollments', ''),
        );
        $data['us_listing'] = 'class="start active"';
        $data['var_meta_title'] = 'Regular Users - Pending Enrollments';
        $data['var_meta_description'] = 'Regular Users - Pending Enrollments';
        $data['var_meta_keyword'] = 'Regular Users - Pending Enrollments';
        $data['Heading'] = 'Pending Enrollments';
        $data['js'] = array(
            'admin/enrollment.js',
            'custom/jquery.maskMoney.js',
        );
        $data['js_plugin'] = array(
            'data-tables/jquery.dataTables.js',
        );
        $data['action'] = 'true';
        $data['users'] = $this->enrollment_model->getSubscribedUsers(); //Argument is status
        //echo '<pre>';print_r($data['users']);exit;
        $this->load->view(ADMIN_LAYOUT, $data);
    }

    public function getUserInfo($uid){
        if(!empty($uid)){
            $userInfo = $this->enrollment_model->getMembershipUserById($uid);
            $data['data'] = $userInfo[0];
            $this->load->view('admin/regular_users/info', $data);
        }else{
            exit('Access Denied.');
        }
    }

    public function edit($uid){
        if(!empty($uid)){
            $userInfo = $this->enrollment_model->getMembershipUserById($uid);
            $data['data'] = $userInfo[0];

            $this->load->view('admin/regular_users/edit', $data);
        }else{
            exit('Access Denied.');
        }
    }

    function update(){
        $post = $this->input->post();

        $data 			= [];
        $data['cardNumber'] 		= $post['cardNumber'];
        $data['var_fname'] 			= $post['var_fname'];
        $data['var_lname'] = $post['var_lname'];
        $data['var_phone'] 		= $post['var_phone'];

        $this->db->where('int_glcode', $post['uid']);
        $this->db->update('dz_user', $data);

        redirect(base_url()."admin/regular_users/memberships");
        exit;
    }

    function memberships() {
        $data['page'] = "admin/regular_users/memberships";
        $data['breadcrumb_list'] = array(
            array('Regular Users - Membership Database', ''),
        );
        $data['us_membership'] = 'class="start active"';
        $data['var_meta_title'] = 'Regular Users - Membership Database ';
        $data['var_meta_description'] = 'Regular Users - Membership Database ';
        $data['var_meta_keyword'] = 'Regular Users - Membership Database ';
        $data['Heading'] = ' Membership Database';
        $data['js'] = array(
            //'admin/enrollment.js',
            'custom/table-managed.js',
        );
        $data['js_plugin'] = array(
            'data-tables/jquery.dataTables.js',
        );
        $data['init'] = array(
            'TableManaged.init()',
        );
        $data['action'] = 'true';
        $data['users'] = $this->enrollment_model->getMembershipUsers(); //Argument is status
        //echo '<pre>';print_r($data['users']);exit;
        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function getFilterResult(){
        if($this->input->post('status')) {
            $status = $this->input->post('status');
            $data['users'] = $this->enrollment_model->getMembershipUsers($status);
            $this->load->view("admin/regular_users/filter_result", $data);
        }else{
            echo 'Invalid request';
        }
    }

    function AddRefundAmount(){
        if($this->input->post('membershipId')){
            $this->db->where('id', $this->input->post('membershipId'));
            $this->db->update('users_memberships', ['refundedAmount'=>$this->input->post('amount')]);
            echo true;
        }else{
            echo false;
        }
        exit;
    }

    function addCardNumber(){
        if($this->input->post('userId')){
            $this->db->where('int_glcode', $this->input->post('userId'));
            $this->db->update('dz_user', ['cardNumber'=>$this->input->post('card')]);
            echo true;
        }else{
            echo false;
        }
        exit;
    }

    public function delete(){
        $post = $this->input->post();
        if(isset($post['userId']) && $post['userId'] != ''){
            $this->db->where('int_glcode', trim($post['userId']));
            $this->db->delete('dz_user');
            $this->db->where('userId', trim($post['userId']));
            $this->db->delete('users_memberships');
            echo 'success';
            exit;
        }else{
            echo 'error';
            exit;
        }
    }

    function markedExtension($id){
        if($id > 0 || !empty($id)) {
            $this->db->where('int_glcode', $id);
            $this->db->update('dz_user', ['manuallExtension' => 1]);
        }
        redirect(base_url("admin/regular_users/memberships"));
    }
}
?>
