<?php

class Pending_cancellation extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin/con_reservation');
        $this->load->helper('stripe_lib/init');
        $this->load->helper('tourico_api');
    }

    function stripeRefund($refundinfo)
    {
        if(!$this->session->userdata('valid_adminuser'))
        {
            redirect('admin/login');
        }
        \Stripe\Stripe::setApiKey('sk_test_pXZu0HXdj2nffgTwk8xbG8Tr');
        $planInfo = null;
        try
        {
            $re = \Stripe\Refund::create(array(
                "charge" => $refundinfo
            ));
            return array('success' => true, 'refund_info' => $re);
        }
        catch (\Stripe\Error\Card $e)
        {
            $body = $e->getJsonBody();
            $err = $body['error'];
            return array('success' => false, 'reason' => 'card_declined', 'details' => $err['message']);
        }
        catch (\Stripe\Error\InvalidRequest $e)
        {
            return array('success' => false, 'reason' => 'invalid_parameter_supplied', 'details' => 'Invalid parameter supplied to stripe');
        }
        catch (\Stripe\Error\Authentication $e)
        {
            return array('success' => false, 'reason' => 'secret_key_not_valid', 'details' => 'Invalid parameter supplied to stripe');
        }
        catch (\Stripe\Error\ApiConnection $e)
        {
            return array('success' => false, 'reason' => 'connection_problem', 'details' => 'connection to stripe is not working');
        }
        catch (Exception $e)
        {
            return array('success' => false, 'reason' => 'other_error', 'details' => 'connection to stripe is not working');
        }
    }

    function index($action = '' , $id = false)
    {
        if(!$this->session->userdata('valid_adminuser'))
        {
            redirect('admin/login');
        }
        if ($this->blobmenuarray1['Pending Cancellations'] != 'yes') {
            exit();
        }
        if($_POST)
        {
            $this->load->helper(array('form', 'url'));
            $this->load->library('form_validation');
            $this->form_validation->set_rules('rid', 'Reservation ID', 'required');
            $this->form_validation->set_rules('confirm_number', 'Refund #', 'required');
            $this->form_validation->set_rules('ret_amount', 'Refunded', 'required');
            if ($this->form_validation->run() == FALSE)
            {
                $data['error'] = validation_errors();
            }
            else
            {
                $da = array(
                    "cancel_status"=>2,
                    "admin_changed" => 0,
                    "return_amount" => $_POST['ret_amount'],
                    "stripe_no" => $_POST['confirm_number'],
                );
                if($this->db->where('id' , $_POST['rid'])->update('booking_detail' , $da))
                {
                    $data['success'] = "Reservation cancel Successfully!";
                }
                else
                {
                    $data['error'] = 'Server error!';
                }
            }
        }
        $data['page'] = "admin/pendingcancel/pendcancel_list";
        $data['breadcrumb'] = "Pending Cancellations";
        $data['breadcrumb_sub'] = "Pending Cancellations Sub";
        $data['breadcrumb_list'] = array(
            array('Pending Cancellations', ''),
        );
        $data['pen_cancel'] = 'class="start active"';
        $data['var_meta_title'] = 'Pending Cancellations';
        $data['var_meta_description'] = 'Pending Cancellations';
        $data['var_meta_keyword'] = 'Pending Cancellations';
        $data['js'] = array(
            'custom/jquery.maskMoney.js',
            'custom/table-managed.js',
            'admin/pending_cancel.js',
            'custom/components-pickers.js',
        );
        $data['css_plugin'] = array(
            'select2/select2.css',
            'select2/select2-metronic.css',
            'data-tables/DT_bootstrap.css',
            'bootstrap-datepicker/css/datepicker.css',
        );
        $data['js_plugin'] = array(
            'select2/select2.min.js',
            'data-tables/jquery.dataTables.js',
            'data-tables/DT_bootstrap.js',
            'bootstrap-datepicker/js/bootstrap-datepicker.js',
        );
        $data['css'] = array(
        );
        $data['init'] = array(
            'TableManaged.init()',
            'Quote.init()',
            'ComponentsPickers.init()'
        );
        $data['confReservations'] = $this->db->order_by("id", "asc")->where('cancel_status' , 1)->/*where('admin_changed' , 1)->*/get('booking_detail')->result();
        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function datatable($fist = "", $secound = "")
    {
        if(!$this->session->userdata('valid_adminuser'))
        {
            redirect('admin/login');
        }

        $this->load->library('Datatables');
        if ($fist != 0) {
            $from = $this->datatables->where('dz_quote.var_checkin', $fist);
        }
        if ($secound != 0) {
            $to = $this->datatables->where('dz_quote.var_checkout', $secound);
        }
        $this->datatables->select('
            dz_rplyquote.var_createddate,
            dz_user.var_username,
            dz_user.var_email,
            dz_user.var_accountid,
            dz_rplyquote.var_transactionid,
            dz_rplyquote.var_prize,
            dz_rplyquote.int_glcode,
            dz_rplyquote.int_fkquote,
            dz_rplyquote.chr_status,
            dz_rplyquote.fk_coupon,
            dz_rplyquote.var_src,
            dz_rplyquote.int_src_price,
            dz_user.var_fname,
            dz_user.var_lname
           
            ');
        $this->datatables->from('dz_rplyquote');
        $this->datatables->where('dz_rplyquote.chr_status', 'P');
        $this->datatables->where('dz_rplyquote.var_admin_soures', NULL);
        $this->datatables->where('dz_rplyquote.var_admin_amount', NULL);
        $this->datatables->where('dz_rplyquote.var_soures_confirmation', NULL);
        $this->datatables->join('dz_quote', 'dz_rplyquote.int_fkquote = dz_quote.int_glcode');
        $this->datatables->join('dz_user', 'dz_quote.var_fkuser = dz_user.int_glcode');
//        $this->datatables->order_by('var_createddate', 'DESC');
        //$this->datatables->join('dz_commision', 'dz_rplyquote.int_fkquote=dz_commision.fk_quote');
        $results = $this->datatables->generate();
        $results = json_decode($results, true);
//          print_r($results['aaData']);
//           exit();
        for ($i = 0; $i < count($results['aaData']); $i++) {
            $price = "";
            if ($results['aaData'][$i][9] != "" && $results['aaData'][$i][9] != 0) {
                $price = $this->getfinalamount1($results['aaData'][$i][5], $results['aaData'][$i][9]);
            } else {
                $price = $results['aaData'][$i][5];
            }
            $email = $results['aaData'][$i][1];
            $quotid = $results['aaData'][$i][6];
            //  $data['finalamount'] = $this->getfinalamount1($data['confirm'][0]['var_prize'],$data['confirm'][0]['fk_coupon']);
            $results['aaData'][$i][0] = $results['aaData'][$i][0];
            $results['aaData'][$i][1] = $results['aaData'][$i][1];
            $results['aaData'][$i][2] = $results['aaData'][$i][2];
            $results['aaData'][$i][3] = $results['aaData'][$i][3];
            $results['aaData'][$i][4] = $results['aaData'][$i][4];
            $results['aaData'][$i][5] = $price;
            $results['aaData'][$i][6] = '<a href="#myModal_autocomplete" data-toggle="modal" data-id="' . $quotid . '" id="conform_list" class="sentquote label label-sm label-success">View</a> 
                <a href="#myModal_paid" data-toggle="modal" src="'.$results['aaData'][$i][10].'" srcint="'.$results['aaData'][$i][11].'" data-id="' . $quotid . '" id="paid_list" class="paid_list label label-sm label-success">Paid</a>
                    <a href="javascript:;" data-toggle="modal" name="'.$results['aaData'][$i][12]." ".$results['aaData'][$i][13].'" email="'.$results['aaData'][$i][2].'" data-id="' . $quotid . '" id="cancel_list" class="cancle_list label label-sm label-success">Cancel</a>';
//            $results['aaData'][$i][6] = '<a href="#myModal_paid" data-toggle="modal" data-id="' . $quotid . '" id="paid_list" class="sentquote label label-sm label-success">Paid</a>';
        }
//        print_r($results);exit;
        echo json_encode($results);
    }

    function getconfirm_res()
    {
        if(!$this->session->userdata('valid_adminuser'))
        {
            redirect('admin/login');
        }
        $id = $this->input->post('id');
        $userid = $this->input->post('userid');

        $this->db->select('
             dz_quote.var_checkin,
             dz_quote.var_checkout,
             dz_quote.var_city,
             dz_quote.var_rating,
             dz_quote.var_comments,
             dz_quote.var_room,
             dz_quote.var_adult,
             dz_quote.var_child,
             dz_quote.var_hotelname,
             dz_rplyquote.fk_coupon,
             dz_rplyquote.var_prize,
             dz_rplyquote.var_NOH,
             dz_rplyquote.var_prize,
             dz_rplyquote.var_uniq_quoteid,
             dz_rplyquote.var_policy,
             dz_rplyquote.var_star,
             dz_rplyquote.var_transactionid,
             dz_rplyquote.var_tripadvisor,
             dz_rplyquote.var_src,
             dz_rplyquote.int_src_price,
             dz_rplyquote.var_admin_soures,
             dz_rplyquote.var_admin_amount,
             dz_rplyquote.var_soures_confirmation,
             dz_rplyquote.var_comment,
             dz_rplyquote.var_cancalationtype,
             dz_rplyquote.var_refundamount,
             dz_rplyquote.var_updateddate
             ');
        $this->db->from('dz_rplyquote');
        $this->db->where('dz_rplyquote.int_glcode', $id);
        $this->db->join('dz_quote', 'dz_rplyquote.int_fkquote = dz_quote.int_glcode');

        //  $this->db->join('dz_user', 'dz_rplyquote.prosseduser_id = dz_user.int_glcode');
//      $this->db->join('dz_commision', 'dz_rplyquote.int_fkquote = dz_commision.fk_quote');
        $data['confirm'] = $this->db->get()->result_array();


        $this->db->select('*');
        $this->db->where('int_glcode',$userid);
        $this->db->from('dz_user');
        $data['userdetail'] = $this->db->get()->result_array();

        if ($data['confirm'][0]['fk_coupon'] != "" && $data['confirm'][0]['fk_coupon'] != 0) {
            $data['finalamount'] = $this->getfinalamount1($data['confirm'][0]['var_prize'], $data['confirm'][0]['fk_coupon']);
            $this->db->select('*');
            $this->db->where('int_glcode', $data['confirm'][0]['fk_coupon']);
            $coupan = $this->db->get('dz_coupons')->result_array();

            if ($coupan[0]['var_coupan_type'] == 1) {
                // echo $coupan[0]['var_couponvalue'] - $this->input->post('price');
                $coupanstr = $coupan[0]['var_couponvalue'] . "% OFF";
            }
            if ($coupan[0]['var_coupan_type'] == 2) {
                $coupanstr = "$" . $coupan[0]['var_couponvalue'] . ' OFF';
            }
            $data['coupanstr'] = $coupanstr;
        } else {
            $data['finalamount'] = $data['confirm'][0]['var_prize'];
            $data['coupanstr'] = 'No coupons';
        }
//        print_r($data['confirm']);exit;
//        $data['confirm'] = $this->con_reservation->confirm_getdata($id); 
        //  print_r($data);exit;
        $this->load->view('admin/pending_reservations/popupquoteslist', $data);
    }

    function getfinalamount1($paid1, $coupanid)
    {
        if(!$this->session->userdata('valid_adminuser'))
        {
            redirect('admin/login');
        }
        //  if($this->input->post()){
        $this->db->select('*');
        $this->db->where('int_glcode', $coupanid);
        $coupan = $this->db->get('dz_coupons')->result_array();

        if ($coupan[0]['var_coupan_type'] == 1) {
            // echo $coupan[0]['var_couponvalue'] - $this->input->post('price');
            $discoutamount = floatval(($coupan[0]['var_couponvalue'] * $paid1) / 100);
            $discoutamount = number_format((float) $discoutamount, 2, '.', '');
            $acutelamount = $paid1 - $discoutamount;
            $acutelamount = number_format((float) $acutelamount, 2, '.', '');
            return $acutelamount;
        }
        if ($coupan[0]['var_coupan_type'] == 2) {
            $acutelamount = $paid1 - $coupan[0]['var_couponvalue'];
            $acutelamount = number_format((float) $acutelamount, 2, '.', '');
            return $acutelamount;
        }
        //  }
    }

    function add_pending()
    {
        if(!$this->session->userdata('valid_adminuser'))
        {
            redirect('admin/login');
        }
        if ($this->input->post()) {
//            $return_data = $this->con_reservation->add_pending($this->input->post());
//            if ($return_data) {
//                $return = array('status' => 'success');
//            } else {
//                $return = array('status' => 'error');
//            }
//            echo json_encode($return);

            $id = $this->input->post('editid');
            $array = array(
                'var_admin_soures' => $this->input->post('source'),
                'var_admin_amount' => $this->input->post('amount'),
                'var_soures_confirmation' => $this->input->post('source_confirmation')
            );
            $this->db->where('int_glcode', $id);
            $edit = $this->db->update('dz_rplyquote', $array);

//            $id = $this->input->post('editid');
            $this->db->select('
             dz_user.*,
             dz_quote.var_fkuser,
             dz_quote.var_checkin,
             dz_quote.var_checkout,
             dz_quote.var_city,
             dz_quote.var_rating,
             dz_quote.var_comments,
             dz_quote.var_room,
             dz_quote.var_adult,
             dz_quote.var_child,
             dz_quote.var_hotelname,
             dz_rplyquote.fk_coupon,
             dz_rplyquote.var_prize,
             dz_rplyquote.var_NOH,
             dz_rplyquote.var_prize,
             dz_rplyquote.var_uniq_quoteid,
             dz_rplyquote.var_policy,
             dz_rplyquote.var_star,
             dz_rplyquote.var_transactionid,
             dz_rplyquote.var_tripadvisor,
             dz_rplyquote.var_src,
             dz_rplyquote.int_src_price,
             dz_rplyquote.var_admin_soures,
             dz_rplyquote.var_admin_amount,
             dz_rplyquote.var_soures_confirmation,
             dz_rplyquote.var_transactionid,
             dz_rplyquote.var_comment,
             ');
            $this->db->from('dz_rplyquote');
            $this->db->where('dz_rplyquote.int_glcode', $id);
            $this->db->join('dz_quote', 'dz_rplyquote.int_fkquote = dz_quote.int_glcode');
            $this->db->join('dz_user', 'dz_user.int_glcode = dz_quote.var_fkuser');
//      $this->db->join('dz_commision', 'dz_rplyquote.int_fkquote = dz_commision.fk_quote');
            $data['confirm'] = $this->db->get()->result_array();

            if ($data['confirm'][0]['fk_coupon'] != "" && $data['confirm'][0]['fk_coupon'] != 0)
            {
                $data['finalamount'] = $this->getfinalamount1($data['confirm'][0]['var_prize'], $data['confirm'][0]['fk_coupon']);
                $this->db->select('*');
                $this->db->where('int_glcode', $data['confirm'][0]['fk_coupon']);
                $coupan = $this->db->get('dz_coupons')->result_array();

                if ($coupan[0]['var_coupan_type'] == 1) {
                    // echo $coupan[0]['var_couponvalue'] - $this->input->post('price');
                    $coupanstr = $coupan[0]['var_couponvalue'] . "% OFF";
                }
                if ($coupan[0]['var_coupan_type'] == 2) {
                    $coupanstr = "$" . $coupan[0]['var_couponvalue'] . ' OFF';
                }
                $data['coupanstr'] = $coupanstr;
            } else {
                $data['finalamount'] = $data['confirm'][0]['var_prize'];
                $data['coupanstr'] = 'No coupons';
            }
            $confirmresarvationdetail = $this->load->view('admin/account/confirmresarvationdetail', $data, TRUE);
            $this->db->select('*');
            $this->db->where('int_glcode', $data['confirm'][0]['var_fkuser']);
            $user = $this->db->get('dz_user')->result_array();

            $temp = time();
            $mail_body.='<p style="color:#fff;margin-bottom:10px;">Dear ' . $data['confirm'][0]['var_fname'].' '. $data['confirm'][0]['var_lname'] . ',</p>';
            $mail_body.='<p style="color:#fff;margin-bottom:10px;"><b>CONGRATULATIONS! Your reservation is booked!</b></p>';
            $mail_body.='<p style="color:#fff;margin-bottom:10px;"><span style="color: #fff;">Thank you for booking with <b>Wholesale Hotels Group!<sup>sm</sup></b> We are delighted that you decided to choose us for your next getaway.</span></p>';
            $mail_body.= '<p style="color:#fff;margin-bottom:10px;"><span style="color: #fff;">Your charge will appear on your statement as: <b>Wholesale Hotels Group</b></span></p>';
            $mail_body.= '<p style="color:#fff;margin-bottom:10px;"><span style="color: #fff;">Your reservation confirmation number is:  <b>'.$data['confirm'][0]['var_transactionid'] .'</b></span></p>';
            $mail_body.= '<p style="color:#fff;margin-bottom:10px;"><span style="color: #fff;">Once again, here are the details of your reservation: </span></p>';
            $mail_body.= $confirmresarvationdetail;

            $mail_body.= '<div style="clear: both"></div><p style="color:#fff;">Sincerely,<br/><b>Wholesale Hotels Group<sup>sm</sup></b>  <br/><span style="font-size:9px;">YOUR VACATION BEGINS HERE!</spam></p>';
            $mail_body .= '<p style="color:#fff;margin-bottom:10px;font-size:9px;">Cancellation policies between <b>Wholesale Hotels Group<sup>sm</sup></b> quotes and competitor sites may vary.</p>';

            $data['mail_body'] = $mail_body;
            $html = $this->load->view('admin/account/genral_emailtemplate', $data, TRUE);
            //    echo $html;
            //    exit();
            $this->email->set_mailtype("html");
            $this->email->from(NOREPLY, 'Admin');
            //$this->email->from('testmpatel@gmail.com');

            $this->email->to($user[0]['var_email']);
            $this->email->subject("Your reservation is confirmed!");
            $this->email->message($html);
            $emailsend = $this->email->send();
//            if ($this->email->send()) {
//                echo $this->email->print_debugger();
//            } else {
//                echo $this->email->print_debugger();
//            }
//            echo $this->db->last_query();exit;
            if ($emailsend) {
                echo 'success';
                exit;
            } else {
                echo 'error';
                exit;
            }
        }
    }

    function cancelquote()
    {
        if(!$this->session->userdata('valid_adminuser'))
        {
            redirect('admin/login');
        }
        $replyid = $this->input->post('replayid');
        $name = $this->input->post('name');
        $email = $this->input->post('email');

        $updatevalue = array(
            'var_createddate' => $paymentdate = date("Y-m-d H:i:s", strtotime("-3 day")),
            'chr_status' => "AC",
            'var_cancalationtype' => "Admin Cancelation"
        );
        $this->db->where('int_glcode', $replyid);
        $result = $this->db->update('dz_rplyquote', $updatevalue);
        $mail_body.='<p style="color:#fff;margin-bottom:10px;">Dear ' . $name. ',</p>';
        $mail_body.='<p style="color:#fff;margin-bottom:10px;">Thank you for using <b>Wholesale Hotels Group<sup>sm</sup></b>. We would like to let you know that your reservation cannot be secured for the rate you booked at this time. Hotel rates and room availability can fluctuate from minute to minute based on many factors, including how many rooms a hotel has available at a precise point in time. We have automatically refunded your payment and the confirmation email from PayPal<sup>TM</sup> will arrive soon acknowledging the processed refund. Should you be interested in booking this specific room, please feel free to RESUBMIT your quote request. You can resubmit your quote request with only a click of a button from RESERVATIONS – QUOTES.</p>';
        $mail_body.='<p style="color:#fff;margin-bottom:10px;">Again, thank you for choosing us and please always give us a try before booking your next trip and we DO apologize for the inconveniences this may have caused you!</p>';
        $mail_body.= '<p style="color:#fff;">Sincerely,<br/><b>Wholesale Hotels Group<sup>sm</sup></b>  <br/><span style="font-size:9px;">YOUR VACATION BEGINS HERE!</spam></p>';
        $mail_body .= '<p style="color:#fff;margin-bottom:10px;font-size:9px;">Cancellation policies between <b>Wholesale Hotels Group<sup>sm</sup></b> quotes and competitor sites may vary.</p>';

        $data['mail_body'] = $mail_body;
        $html = $this->load->view('admin/account/genral_emailtemplate', $data, TRUE);
        //    echo $html;
        //    exit();
        $this->email->set_mailtype("html");
        $this->email->from(NOREPLY, 'Admin');
        //$this->email->from('testmpatel@gmail.com');

        $this->email->to($email);
        $this->email->subject("Your reservation is canceled!");
        $this->email->message($html);
        $emailsend = $this->email->send();

        if($emailsend)
        {
            echo 'success';
        }
        else{
            echo 'error';
        }
    }

}

?>