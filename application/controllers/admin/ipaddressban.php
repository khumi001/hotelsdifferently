<?php

class Ipaddressban extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin/ipban');
    }

    function index() 
	{
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
        if ($this->blobmenuarray1['IP Address BAN'] != 'yes') {
            exit();
        }
        $data['page'] = "admin/ipaddressban/list";
        $data['breadcrumb'] = "IP Address Ban";
        $data['breadcrumb_sub'] = "IP Address Ban sub";
        $data['breadcrumb_list'] = array(
            array('IP Address Ban', ''),
        );
        $data['ipaddressban'] = 'class="start active"';
        $data['var_meta_title'] = 'IP Address Ban';
        $data['var_meta_description'] = 'IP Address Ban';
        $data['var_meta_keyword'] = 'IP Address Ban';
        $data['js'] = array(
            'admin/ipbanadrress.js',
            'custom/table-managed.js',
        );
        $data['css_plugin'] = array(
            'select2/select2.css',
            'select2/select2-metronic.css',
            'data-tables/DT_bootstrap.css'
        );
        $data['js_plugin'] = array(
            'select2/select2.min.js',
            'data-tables/jquery.dataTables.js',
            'data-tables/DT_bootstrap.js',
        );
        $data['css'] = array(
        );

        $data['init'] = array(
            'TableManaged.init()',
            'IPban.init()',
        );
        if ($this->input->is_ajax_request()) {
            $result = $this->toval->check_cookie_ajax($data['js'], $data['js_plugin'], $data['css'], $data['css_plugin']);
            $data['js'] = $result['js'];
            $data['js_plugin'] = $result['js_plugin'];
        } else {
            $result = $this->toval->check_cookie($data['js'], $data['js_plugin'], $data['css'], $data['css_plugin']);
            $data['js'] = $result['js'];
            $data['js_plugin'] = $result['js_plugin'];
        }
        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function add() 
	{
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
        if ($this->input->post()) 
		{
			$this->load->helper('tourico_api');
			$user1 = getUserById($this->session->userdata['valid_adminuser']['id']);
			date_default_timezone_set('America/Los_Angeles');
            if ($this->input->post('ban_time') == '1') 
			{
                $today = date("Y-m-d H:i:s");
                $end = date('Y-m-d H:i:s', strtotime($today . ' + 1 day'));
            } 
			else if ($this->input->post('ban_time') == '7') 
			{
                $today = date("Y-m-d H:i:s");
                $end = date('Y-m-d H:i:s', strtotime($today . ' + 7 day'));
            } 
			else if ($this->input->post('ban_time') == '30') 
			{
                $today = date("Y-m-d H:i:s");
                $end = date('Y-m-d H:i:s', strtotime($today . ' + 30 day'));
            } 
			else 
			{
                $today = date("Y-m-d H:i:s");
                $end = date('Y-m-d H:i:s', strtotime($today . ' + 5 years'));
            }

            if ($this->input->post('optionsRadios') == 'option1') 
			{
                $data_array1 = array(
                    'fk_user' => '0',
                    'var_ipban' => $this->input->post('ban_by_ip'),
                    'var_banaue' => "not",
                    'date_banstart_time' => date("Y-m-d H:i:s"),
                    'date_banend_time' => $end,
                    'var_reason' => $this->input->post('reason'),
                    'chr_status' => 'B',
                    'var_banby' => $user1->var_email,
                    'var_createddate' => date("Y-m-d H:i:s"),
                    'var_updateddate' => date("Y-m-d H:i:s"),
                );
                $this->db->insert('dz_ban_ip_address', $data_array1);
                //edit by tayyab for acativity log
                $insert_id = $this->db->insert_id();
                $CI = & get_instance();
                $CI->load->model('admin/admin_log_model');
                $log = array(
                    'log_type' => 'ip_banned',
                    'log_type_fk_id' => $insert_id,
                    'log_desc' => 'IP '.$this->input->post('ban_by_ip').' BAN by '.$user1->var_email
                );
				$CI->admin_log_model->addlog($log);
                echo "success";
                exit;
            } 
			else 
			{
                $this->db->select('var_username,var_email,var_accountid,IP_ban');
                $this->db->or_where('var_username', $this->input->post('ban_by_acc'));
                $this->db->or_where('var_email', $this->input->post('ban_by_acc'));
                $this->db->or_where('var_accountid', $this->input->post('ban_by_acc'));
                //  $this->db->or_where('IP_ban', $this->input->post('ban_by_ip'));
                $query = $this->db->get('dz_user')->num_rows();
                if ($query == 1) 
				{
                    $request = $this->ipban->addipban($this->input->post());
                    if ($request == TRUE) 
					{
                        echo "success";
                        exit;
                    } 
					else 
					{
                        echo "error";
                        exit;
                    }
                } 
				else 
				{
                    echo "usernotexits";
                }
            }
        }
    }

}

?>
