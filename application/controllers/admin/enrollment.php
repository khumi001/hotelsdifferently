<?php

class Enrollment extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin/enrollment_model');
    }

    function pending() {
        $data['page'] = "admin/enrollment/pending";
        $data['breadcrumb_list'] = array(
            array('TP Enrollment - Pending', ''),
        );
        $data['tp_enrollment_pending'] = 'class="start active"';
        $data['var_meta_title'] = 'TP Enrollment - Pending';
        $data['var_meta_description'] = 'TP Enrollment - Pending';
        $data['var_meta_keyword'] = 'TP Enrollment - Pending';
        $data['Heading'] = 'Travel Professionals';
        $data['js'] = array(
            'admin/enrollment.js',
        );
        $data['js_plugin'] = array(
            'data-tables/jquery.dataTables.js',
        );

        $data['action'] = 'true';
        $data['users'] = $this->enrollment_model->getEnrollments('AP'); //Argument is status
        //print_r($data['users']);exit;
        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function index() {
        $data['page'] = "admin/enrollment/listing";
        $data['breadcrumb_list'] = array(
            array('TP Enrollment - Approval', ''),
        );
        $data['tp_enrollment_approval'] = 'class="start active"';
        $data['var_meta_title'] = 'TP Enrollment - Approval';
        $data['var_meta_description'] = 'TP Enrollment - Approval';
        $data['var_meta_keyword'] = 'TP Enrollment - Approval';
        $data['Heading'] = 'Travel Professionals';
        $data['js'] = array(
            'admin/enrollment.js',
        );
        $data['js_plugin'] = array(
            'data-tables/jquery.dataTables.js',
        );

        $data['action'] = 'true';
        $data['users'] = $this->enrollment_model->getEnrollments('P'); //Argument is status
        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function getAjaxRevenue(){
        if($this->input->post('from') != '' || $this->input->post('to') != '') {
            $data['users'] = $this->enrollment_model->getRevenue($this->input->post('from'),$this->input->post('to')); //Argument is status
            $this->load->view("admin/enrollment/ajax_revenue", $data);
        }
    }

    function revenue() {
        $data['page'] = "admin/enrollment/revenue";
        $data['breadcrumb_list'] = array(
            array('TP Revenue', ''),
        );
        $data['tp_entity'] = 'class="start active"';
        $data['var_meta_title'] = 'TP Revenue';
        $data['var_meta_description'] = 'TP Revenue';
        $data['var_meta_keyword'] = 'TP Revenue';
        $data['Heading'] = 'TP Revenue';
        $data['js'] = array(
            'custom/components-pickers.js',
            'admin/enrollment.js',
        );
        $data['js_plugin'] = array(
            'data-tables/jquery.dataTables.js',
            'select2/select2.min.js',
            'bootstrap-datepicker/js/bootstrap-datepicker.js',
        );
        $data['css_plugin'] = array(
            'select2/select2.css',
            'select2/select2-metronic.css',
            'data-tables/DT_bootstrap.css',
            'bootstrap-datepicker/css/datepicker.css',
            'bootstrap-switch/css/bootstrap-switch.min.css',
        );
        $data['css'] = array(
        );
        $data['init'] = array(
            'ComponentsPickers.init()',
        );

        $data['tp_revenue'] = 'true';
        $data['action'] = 'true';
//        $data['users'] = $this->entity_model->getEntityRevenue(); //Argument is status

        $data['users'] = $this->enrollment_model->getRevenue(); //Argument is status
        $this->load->view(ADMIN_LAYOUT, $data);
    }


    function travel_professionals() {

        $data['page'] = "admin/enrollment/listing";
        $data['breadcrumb_list'] = array(
            array('Travel Professionals', ''),
        );
        $data['Travel_Professionals'] = 'class="start active"';
        $data['var_meta_title'] = 'Travel Professionals';
        $data['var_meta_description'] = 'Travel Professionals';
        $data['var_meta_keyword'] = 'Travel Professionals';
        $data['Heading'] = 'Travel Professionals';
        $data['js'] = array(
            'admin/enrollment.js',
        );
        $data['js_plugin'] = array(
            'data-tables/jquery.dataTables.js',
        );
        $data['can_delete'] = "true";
        $data['action'] = 'false';
        $data['users'] = $this->enrollment_model->getEnrollments('A'); //Argument is status
        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function update(){
        $post = $this->input->post();

        if(isset($post['uid']) && isset($post['status'])){

            $userData = $this->db->select('*')->get_where('travel_professional_detail',['user_id'=>trim($post['uid'])])->result_array();

            if($post['status'] == 'P' || $post['status'] == 'R'){

                if($post['status'] == 'R'){

                    $this->db->where('int_glcode', trim($post['uid']));
                    $this->db->delete('dz_user');
                    $this->db->where('user_id', trim($post['uid']));
                    $this->db->delete('travel_professional_detail');

                    /*$this->db->where('int_glcode', trim($post['uid']));
                    $this->db->where('chr_user_type', 'TF');
                    $this->db->update('dz_user', ['chr_status'=>'R']);*/

                    $this->sendRejectEmail($userData[0]);
                    die('success');

                }else if($post['status'] == 'P'){

                    $this->db->where('int_glcode', trim($post['uid']));
                    $this->db->where('chr_user_type', 'TF');
                    $this->db->update('dz_user', ['chr_status'=>'P']);
                    die('success');
                }
            }

        }
    }

    function update_approval(){
        $post = $this->input->post();

        if(isset($post['uid']) && isset($post['status'])){

            $userData = $this->db->select('*')->get_where('travel_professional_detail',['user_id'=>trim($post['uid'])])->result_array();

            if($post['status'] == 'A' || $post['status'] == 'R'){

                if($post['status'] == 'R'){

                    $this->db->where('int_glcode', trim($post['uid']));
                    $this->db->delete('dz_user');
                    $this->db->where('user_id', trim($post['uid']));
                    $this->db->delete('travel_professional_detail');

                    /*$this->db->where('int_glcode', trim($post['uid']));
                    $this->db->where('chr_user_type', 'TF');
                    $this->db->update('dz_user', ['chr_status'=>'R']);*/

                    $this->sendRejectApprovalEmail($userData[0]);
                    exit('success');

                }else if($post['status'] == 'A'){

                    $this->db->where('int_glcode', trim($post['uid']));
                    $this->db->where('chr_user_type', 'TF');
                    $this->db->update('dz_user', ['chr_status'=>'A'/*,'activation_code'=>$post['code']*/]);

                    $this->sendApprovedEmail($userData[0]);
                    exit('success');
                }
            }

        }
    }

    private function sendApprovedEmail($data){

        $from_email = NOREPLY;
        $to_email = $data['email'];
        //Load email library
        $this->load->library('email');
        $this->email->set_mailtype("html");
        $this->email->from($from_email, 'hotelsDifferently');
        $this->email->to($to_email);
        $this->email->subject('Enrollment update');

        $message = $this->load->view('admin/enrollment/approve_email', ['data'=>$data], TRUE);

        $this->email->message($message);

        $this->email->send();
    }

    private function sendRejectEmail($data){

        $from_email = NOREPLY;
        $to_email = $data['email'];
        //Load email library
        $this->load->library('email');
        $this->email->set_mailtype("html");
        $this->email->from($from_email, 'hotelsDifferently');
        $this->email->to($to_email);
        $this->email->subject('Enrollment update');

        $message = $this->load->view('admin/enrollment/reject_email', ['data'=>$data], TRUE);

        $this->email->message($message);
        $this->email->send();

    }

    private function sendRejectApprovalEmail($data){
        $from_email = NOREPLY;
        $to_email = $data['email'];
        //Load email library
        $this->load->library('email');
        $this->email->set_mailtype("html");
        $this->email->from($from_email, 'hotelsDifferently');
        $this->email->to($to_email);
        $this->email->subject('Enrollment update');

        $message = $this->load->view('admin/enrollment/reject_approval_email', ['data'=>$data], TRUE);

        $this->email->message($message);
        $this->email->send();

    }

    public function info($uid){
        if(!empty($uid)){
            $userInfo = $this->enrollment_model->getEnrollmentsWithId($uid)->result();
            $data['data'] = $userInfo[0];
            $this->load->view('admin/enrollment/info', $data);
        }else{
            exit('Access Denied.');
        }
    }

    public function edit($uid){
        if(!empty($uid)){
            $userInfo = $this->enrollment_model->getEnrollmentsWithId($uid)->result();
            $data['data'] = $userInfo[0];

            $data['countries'] 		= $this->db->order_by("name", "asc")->get_where('countries')->result_array();
            $data['hear_abouts'] = ['Social Media'=>'Social Media','Another Travel Agent'=>'Another Travel Agent','Media (TV/Radio/Magazine)'=>'Media (TV/Radio/Magazine)','Email'=>'Email','Other'=>'Other'];
            $data['company_types'] 	= $this->db->get_where('company_types')->result_array();
            $this->load->view('admin/enrollment/edit', $data);
        }else{
            exit('Access Denied.');
        }
    }

    function update_info(){
        $post = $this->input->post();

        $data 			= [];
        $data['company_name'] 		= $post['company_name'];
        $data['website'] 			= $post['website'];
        $data['phone_country_code'] = $post['phone_country_code'];
        $data['phone_number'] 		= $post['phone_number'];
        $data['phone_ex'] 			= $post['phone_ex'];
        $data['country'] 			= $post['country'];
        $data['state'] 				= $post['state'];
        $data['city'] 				= $post['city'];
        $data['street'] 			= $post['street'];
        $data['hear_about'] 		= $post['hear_about'];
        $data['contact_f_name'] 	= $post['var_fname'];
        $data['contact_l_name'] 	= $post['var_lname'];
        $data['email'] 				= $post['email'];
        $data['iata'] 				= $post['iata'];
        $data['clia'] 				= $post['clia'];
        $data['arc'] 				= $post['arc'];
        $data['contact_country_code']= $post['contact_country'];
        $data['contact_phone'] 		= $post['contact_phone'];

        $data_user	= [];
        $data_user['var_email'] 	= trim($post['email']);
        $data_user['var_fname'] 	= $post['var_fname'];
        $data_user['var_lname'] 	= $post['var_lname'];
        $data_user['var_phone'] 	= $post['phone_country_code'].$post['contact_phone'];
        $data_user['cardNumber'] 	= $post['cardNumber'];

        $this->db->where('user_id', $post['uid']);
        $this->db->update('travel_professional_detail', $data);

        $this->db->where('int_glcode', $post['uid']);
        $this->db->update('dz_user', $data_user);
        redirect(base_url()."admin/enrollment/travel_professionals");
        exit;
    }


    public function deleteuser(){
        $post = $this->input->post();
        if(isset($post['uid']) && $post['uid'] != ''){
            $this->db->where('int_glcode', trim($post['uid']));
            $this->db->delete('dz_user');
            $this->db->where('user_id', trim($post['uid']));
            $this->db->delete('travel_professional_detail');
            echo 'success';
            exit;
        }
    }
}
?>
