<?php
class Process_active extends CI_Controller {
    function __construct() {
        parent::__construct();
    }

    function ipban() {
        date_default_timezone_set('America/Los_Angeles');
        $today = date("Y-m-d H:i:s");
        $this->db->select('date_banend_time,int_glcode');
        $this->db->from('dz_ban_ip_address');
        $query = $this->db->where("date_banend_time <", $today)->get()->result_array();
         for($i=0; $i<count($query); $i++){
                $data_array = array(
                     'chr_status'=>'A',
                      );
            $this->db->where('int_glcode',$query[$i]['int_glcode']);
            $this->db->update('dz_ban_ip_address',$data_array);
           
         }   
        
    }
}
?>
