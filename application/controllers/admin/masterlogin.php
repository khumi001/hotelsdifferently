<?php
class Masterlogin extends Admin_Controller {

    function __construct() {
        parent::__construct();
//        $this->load->model('admin/loginlog_model');
    }

    function loginmaster()
    {
        if(!$this->session->userdata('valid_adminuser'))
        {
            redirect('admin/login');
        }
        if($this->blobmenuarray1['Master Account'] != 'yes')
        {
            exit();
        }
        $data['page'] = "admin/account/masterlogin";
        $data['var_meta_title'] = 'Dealz Login';
        $data['var_meta_description'] = 'Dealz Login';
        $data['var_meta_keyword'] = 'Dealz Login';
        $data['breadcrumb'] = "LoginMaster";
        $data['breadcrumb_sub'] = "LoginMaster Sub";
        $data['breadcrumb_list']= array(
            array('LoginMaster','')
        );
        $data['LoginMaster'] = 'class="start active"';

        $data['js'] = array(
            'admin/masterlogin.js',
        );
        $data['js_plugin'] = array(

        );
        $data['css'] = array(
        );
        $data['init'] = array(
            'Login.init()',
        );
        if ($this->input->post()) {
            $this->email = $this->input->post('username');
            $this->password = $this->input->post('password');
            if ($this->email && $this->password) {
                $logid = $this->loginlog_model->add_loginlog($this->input->post());
                $row = $this->login_check();
                if($row == '1'){
                    echo "baned"; exit();
                }
                if ($row) {
                    $id = $row->int_glcode;
                    $this->db->select('int_logincnt');
                    $this->db->where('int_glcode', $id);
                    $data = $this->db->get('dz_user')->result_array();
                    $cntvalue = $data[0]['int_logincnt'];
                    if ($cntvalue >= 5) {
                        echo "login error";
                        exit;
                    }
                    if ($row->chr_user_type == "A") {
                        $this->db->where('fk_user', $row->int_glcode);
                        $query = $this->db->get('dz_giveauthorize')->result_array();
                        $arr = array(
                            'valid_adminuser' => array('var_email' => $row->var_email, 'id' => $row->int_glcode, 'var_usertype' => $row->chr_user_type, 'int_logincnt' => $row->int_logincnt, 'menu_details' => $query[0]['var_authorization']));
                        $this->session->set_userdata($arr);
                        echo "a";
                        $data_array = array(
                            'int_logincnt' => '0',
                        );
                        $this->db->where('int_glcode', '1');
                        $this->db->update('dz_user', $data_array);
                        $data_array = array(
                            'var_status' => 'OK',
                        );
                        $this->db->where('int_glcode', $logid);
                        $this->db->update('dz_login_log', $data_array);
                    }
                    else {
                        echo "error";
                        exit();
                    }
                }
            }
        } else {
            $this->load->view(ADMIN_LAYOUT, $data);
        }
    }

    function index()
    {
        if(!$this->session->userdata('valid_adminuser'))
        {
            redirect('admin/login');
        }
        $this->loginmaster();
    }
    function checkuser()
    {
        if(!$this->session->userdata('valid_adminuser'))
        {
            redirect('admin/login');
        }
        $user = $this->input->post('user');
        $pass = $this->input->post('pass');


        $this->db->where('chr_user_type','A');
        $admin = $this->db->get('dz_user');
        $admindata = $admin->result_array();
        // print_r($admindata);
        //  exit();
        $this->db->where('var_accountid',$user);
        $query = $this->db->get('dz_user');
        $data = $query->result_array();
        $num = $query->num_rows();
        if(empty($data))
        {
            // print_r($data);
            $this->db->where('var_email',$user);
            $query = $this->db->get('dz_user');
            $data1 = $query->result_array();
            $num = $query->num_rows();
            // print_r($data1);
            // exit();
            if(empty($data1))
            {
                $this->db->where('var_username',$user);
                $query = $this->db->get('dz_user');
                $data2 = $query->result_array();
                $num = $query->num_rows();
            }
        }
        //$num = $query->num_rows();
        //   echo $admindata[0]['password'];
        if(($num == 1) && ($admindata[0]['var_password'] == base64_encode($pass)))
        {
            $arry = array('sucess',base64_encode($user));
            echo json_encode($arry);
            exit;
        }else{
            $arry = array('error',base64_encode($user));
            echo json_encode($arry);
            exit;
        }

    }
}
?>
