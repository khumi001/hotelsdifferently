<?php

class Processedcharge extends Admin_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        if($this->blobmenuarray1['Processed Chargebacks'] != 'yes')
        {
            exit();
        }
        $data['page'] = "admin/procharge/prolist";
        $data['breadcrumb'] = "Processed Chargeback";
        $data['breadcrumb_sub'] = "Processed Chargeback Sub";
        $data['breadcrumb_list'] = array(
            array('Processed Chargeback', ''),
        );
        $data['procharge'] = 'class="start active"';
        $data['var_meta_title'] = 'Processed Chargeback';
        $data['var_meta_description'] = 'Processed Chargeback';
        $data['var_meta_keyword'] = 'Processed Chargeback';
        $data['js'] = array(
            'custom/table-managed.js',
        );
        $data['css_plugin'] = array(
            'select2/select2.css',
            'select2/select2-metronic.css',
            'data-tables/DT_bootstrap.css'
        );
        $data['js_plugin'] = array(
            'select2/select2.min.js',
            'data-tables/jquery.dataTables.js',
            'data-tables/DT_bootstrap.js',
        );
        $data['css'] = array(
        );

        $data['init'] = array(
            'TableManaged.init()',
        );
         if ($this->input->is_ajax_request()) {
            $result = $this->toval->check_cookie_ajax($data['js'], $data['js_plugin'], $data['css'], $data['css_plugin']);
            $data['js'] = $result['js'];
            $data['js_plugin'] = $result['js_plugin'];
        } else {
            $result = $this->toval->check_cookie($data['js'], $data['js_plugin'], $data['css'], $data['css_plugin']);
            $data['js'] = $result['js'];
            $data['js_plugin'] = $result['js_plugin'];
        }
        $this->db->select('dz_user.int_glcode,dz_user.var_accountid,dz_user.var_username,dz_user.var_email,dz_user.var_fname,dz_user.var_lname,dz_user.var_payment,dz_user.var_country,dz_user.chr_user_type,dz_chargeback.var_amount,dz_chargeback.chr_status,dz_chargeback.var_comment');
        $this->db->from('dz_user');
        $this->db->join('dz_chargeback','dz_chargeback.fk_affilate_chargeback = dz_user.int_glcode');
        $this->db->where('dz_chargeback.chr_status','CB');  
        $data['charge'] = $this->db->get()->result_array(); 
        
        $this->load->view(ADMIN_LAYOUT, $data);
    }

}

?>
