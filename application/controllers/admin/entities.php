<?php

class Entities extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin/entity_model');
    }

    function index() {
        $data['page'] = "admin/entity/listing";
        $data['breadcrumb_list'] = array(
            array('Entities', ''),
        );
        $data['tp_entity'] = 'class="start active"';
        $data['var_meta_title'] = 'Entities';
        $data['var_meta_description'] = 'Entities';
        $data['var_meta_keyword'] = 'Entities';
        $data['Heading'] = 'Entities';
        $data['js'] = array(
            'admin/entity.js',
        );
        $data['js_plugin'] = array(
            'data-tables/jquery.dataTables.js',
        );
        $data['edit_detail'] = 'true';
        $data['action'] = 'true';
        $data['users'] = $this->entity_model->getEnrollments('A'); //Argument is status
        //print_r($data['users']);exit;
        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function pending() {
        $data['page'] = "admin/entity/pending";
        $data['breadcrumb_list'] = array(
            array('Entities – Pending', ''),
        );
        $data['tp_entity_pending'] = 'class="start active"';
        $data['var_meta_title'] = 'Entities – Pending';
        $data['var_meta_description'] = 'Entities – Pending';
        $data['var_meta_keyword'] = 'Entities – Pending';
        $data['Heading'] = 'Entities';
        $data['js'] = array(
            'admin/entity.js',
        );
        $data['js_plugin'] = array(
            'data-tables/jquery.dataTables.js',
        );
        $data['edit_detail'] = 'false';
        $data['action'] = 'true';
        $data['users'] = $this->entity_model->getEnrollments('AP'); //Argument is status
        //print_r($data['users']);exit;
        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function sub_entities() {
        $data['page'] = "admin/entity/sub_entities";
        $data['breadcrumb_list'] = array(
            array('Sub Entities', ''),
        );
        $data['tp_entity'] = 'class="start active"';
        $data['var_meta_title'] = 'Sub Entities';
        $data['var_meta_description'] = 'Sub Entities';
        $data['var_meta_keyword'] = 'Sub Entities';
        $data['Heading'] = 'Sub Entities';
        $data['js'] = array(
            'admin/entity.js',
        );
        $data['js_plugin'] = array(
            'data-tables/jquery.dataTables.js',
        );
        $data['edit_detail'] = 'true';
        $data['action'] = 'true';
        $data['users'] = $this->entity_model->getSubEntities('A'); //Argument is status
        //print_r($data['users']);exit;
        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function getAjaxRevenue(){
        if($this->input->post('from') != '' || $this->input->post('to') != '') {
            $data['users'] = $this->entity_model->getEntityRevenue($this->input->post('from'),$this->input->post('to')); //Argument is status
            $this->load->view("admin/entity/ajax_revenue", $data);
        }
    }

    function revenue() {
        $data['page'] = "admin/entity/revenue";
        $data['breadcrumb_list'] = array(
            array('Entity Revenue', ''),
        );
        $data['tp_entity'] = 'class="start active"';
        $data['var_meta_title'] = 'Entity Revenue';
        $data['var_meta_description'] = 'Entity Revenue';
        $data['var_meta_keyword'] = 'Entity Revenue';
        $data['Heading'] = 'Entity Revenue';
        $data['js'] = array(
            'admin/entity.js',
            'custom/components-pickers.js',
        );
        $data['js_plugin'] = array(
            'data-tables/jquery.dataTables.js',
            'select2/select2.min.js',
            'bootstrap-datepicker/js/bootstrap-datepicker.js',
        );
        $data['css_plugin'] = array(
            'select2/select2.css',
            'select2/select2-metronic.css',
            'data-tables/DT_bootstrap.css',
            'bootstrap-datepicker/css/datepicker.css',
            'bootstrap-switch/css/bootstrap-switch.min.css',
        );
        $data['css'] = array(
        );
        $data['init'] = array(
            'ComponentsPickers.init()',
        );

        $data['edit_detail'] = 'true';
        $data['action'] = 'true';
//        $data['users'] = $this->entity_model->getEntityRevenue(); //Argument is status

        $data['users'] = $this->entity_model->getEntityRevenue(); //Argument is status
        $this->load->view(ADMIN_LAYOUT, $data);
    }

    public function sub_delete(){
        $post = $this->input->post();
        if(isset($post['userId']) && $post['userId'] != ''){
            $this->db->where('int_glcode', trim($post['userId']));
            $this->db->delete('dz_user');

            $this->db->where('userId', $post['userId']);
            $this->db->delete('users_memberships');
            echo 'success';
            exit;
        }else{
            echo 'error';
            exit;
        }
    }

    function extendMembershipCall(){
        $post = $this->input->post();
        if(isset($post['userId']) && $post['userId'] != ''){
            $membership = $this->db->select('id,endDate')->get_where('users_memberships',['userId'=>trim($post['userId'])])->row_array();
            if(count($membership)>0){
                //$extended = date("Y-m-d H:i:s", strtotime($membership['endDate'].", +365 days"));
                /*$this->db->where('id', trim($membership['id']));
                $this->db->update('users_membershipss', array('endDate'=>"DATE_ADD((endDate) , INTERVAL 365 DAY)"));*/
                $id = $membership['id'];
                //update membership if expired
                $this->db->where('int_glcode',$post['userId']);
                $this->db->update('dz_user', array('is_subscribed'=>'1'));

                $this->db->query("UPDATE`users_memberships` SET status = 1,days = days+365,`endDate` = DATE_ADD((endDate) , INTERVAL 365 DAY) WHERE `id` =  '$id'");
                $sub = $this->db->select('int_glcode,entity_parent_account')->get_where('dz_user',['entity_parent_account'=>trim($post['userId'])])->result_array();
                foreach ($sub as $s){
                    $id = $s['int_glcode'];
                    $this->db->query("UPDATE`users_memberships` SET status = 1,days = days+365,`endDate` = DATE_ADD((endDate) , INTERVAL 365 DAY) WHERE `userId` =  '$id'");
                    $this->db->where('int_glcode',$id);
                    $this->db->update('dz_user', array('is_subscribed'=>'1'));
                }
                echo 'success';
            }else{
                echo "No subscription found";
            }
        }else{
            echo 'error';
        }
        exit;
    }

    function update(){
        $post = $this->input->post();
        if(isset($post['uid']) && isset($post['status'])){

            $userDataMain = $this->db->select('*')->get_where('dz_user',['int_glcode'=>trim($post['uid'])])->result_array();
            $userData = $this->db->select('*')->get_where('entity_detail',['user_id'=>trim($post['uid'])])->result_array();
            $userData[0]['user_info'] = $userDataMain[0];
            if($post['status'] == 'A' || $post['status'] == 'R'){

                if($post['status'] == 'R'){

                    $this->db->where('int_glcode', trim($post['uid']));
                    $this->db->delete('dz_user');
                    $this->db->where('user_id', trim($post['uid']));
                    $this->db->delete('entity_detail');
                    $this->db->where('userId', $post['uid']);
                    $this->db->delete('users_memberships');
                    $subUsers = $this->db->select('*')->get_where('dz_user',['entity_parent_account'=>trim($post['uid'])])->result_array();
                    foreach ($subUsers as $sub) {
                        $this->db->where('userId', trim($sub['int_glcode']));
                        $this->db->delete('users_memberships');
                    }
                    $this->db->where('entity_parent_account', trim($post['uid']));
                    $this->db->delete('dz_user');

                    if(isset($post['send_email']) && $post['send_email'] == 'false'){
                        //Do Nothing
                    }else{
                        $this->sendRejectEmail($userData[0]);
                    }
                    die('success');

                }else if($post['status'] == 'A'){
                    $this->db->where('int_glcode', trim($post['uid']));
                    $this->db->update('dz_user', ['entity_activate_time'=>date("Y-m-d H:i:s"),'is_subscribed'=>1,'chr_status'=>'A','entity_sec_code'=>rand(1000000,9999999)]);
                    // subscription
                    $rec = array(
                        'userId' => trim($post['uid']),
                        'startDate' => date("Y-m-d H:i:s"),
                        'endDate' => date("Y-m-d H:i:s", strtotime("+90 days")),
                        'plan' => "",
                        'amountPerMonth' => 0,
                        'stripeInfo' => "",
                        'paid' => 0,
                        'description' => 'Entity membership',
                        'status' => 1,
                        'days' => 90
                    );
                    $row = $this->db->insert('users_memberships', $rec);
                    $userDataMain = $this->db->select('*')->get_where('dz_user',['int_glcode'=>trim($post['uid'])])->result_array();
                    $userData[0]['user_info'] = $userDataMain[0];
                    $this->sendApprovedEmail($userData[0]);
                    die('success');
                }
            }

        }
    }

    private function sendApprovedEmail($data){

        $from_email = NOREPLY;
        $to_email = $data['email'];
        //Load email library
        $this->load->library('email');
        $this->email->set_mailtype("html");
        $this->email->from($from_email, 'hotelsDifferently');
        $this->email->to($to_email);
        $this->email->subject('Enrollment update');

        $message = $this->load->view('admin/entity/approve_email', ['data'=>$data], TRUE);

        $this->email->message($message);

        $this->email->send();
    }

    private function sendRejectEmail($data){

        $from_email = NOREPLY;
        $to_email = $data['email'];
        //Load email library
        $this->load->library('email');
        $this->email->set_mailtype("html");
        $this->email->from($from_email, 'hotelsDifferently');
        $this->email->to($to_email);
        $this->email->subject('Enrollment update');

        $message = $this->load->view('admin/entity/reject_email', ['data'=>$data], TRUE);

        $this->email->message($message);
        $this->email->send();

    }
    public function info($uid){
        if(!empty($uid)){
            $userInfo = $this->entity_model->getEnrollmentsWithId($uid)->result();
            $data['data'] = $userInfo[0];
            //echo '<pre>';print_r($data);exit;
            $this->load->view('admin/entity/info', $data);
        }else{
            exit('Access Denied.');
        }
    }
    public function infoEditTrue($uid){
        if(!empty($uid)){
            $userInfo = $this->entity_model->getEnrollmentsWithId($uid)->result();
            $data['data'] = $userInfo[0];
            //echo '<pre>';print_r($data);exit;
            $this->load->view('admin/entity/info', $data);
        }else{
            exit('Access Denied.');
        }
    }
    public function edit($uid){
        if(!empty($uid)){
            $userInfo = $this->entity_model->getEnrollmentsWithId($uid)->result();
            $data['data'] = $userInfo[0];

            $data['countries'] 		= $this->db->order_by("name", "asc")->get_where('countries')->result_array();
            $data['hear_abouts'] = ['Social Media'=>'Social Media','Another Travel Agent'=>'Another Travel Agent','Media (TV/Radio/Magazine)'=>'Media (TV/Radio/Magazine)','Email'=>'Email','Other'=>'Other'];

            $data['how_many_people_arr'] = $this->db->order_by("id", "asc")->get_where('company_people_range')->result_array();
            $this->load->view('admin/entity/edit', $data);
        }else{
            exit('Access Denied.');
        }
    }

    function update_entity(){
        $post = $this->input->post();

        $data 			= [];
        $data['company_name'] 		= $post['company_name'];
        $data['website'] 			= $post['website'];
        $data['phone_country_code'] = $post['phone_country_code'];
        $data['phone_number'] 		= $post['phone_number'];
        $data['phone_ex'] 			= $post['phone_ex'];
        $data['country'] 			= $post['country'];
        $data['state'] 				= $post['state'];
        $data['city'] 				= $post['city'];
        $data['street'] 			= $post['street'];
        $data['company_peoples'] 	= $post['how_many_people'];
        $data['hear_about'] 		= $post['hear_about'];
        $data['contact_full_name'] 	= $post['contact_full_name'];
        $data['email'] 				= $post['email'];
        $data['contact_country_code']= $post['contact_country'];
        $data['contact_phone'] 		= $post['contact_phone'];

        $domain = trim($post['email']);
        $domain = explode('@',$domain);
        $domain = trim($domain[1]);
        $data['email_domain'] 		= $domain;

        $data_user	= [];
        $data_user['var_email'] 	= trim($post['email']);
        $data_user['var_fname'] 	= $post['contact_full_name'];
        $data_user['var_phone'] 	= $post['phone_country_code'].$post['contact_phone'];

        $this->db->where('user_id', $post['uid']);
        $this->db->update('entity_detail', $data);

        $this->db->where('int_glcode', $post['uid']);
        $this->db->update('dz_user', $data_user);
        redirect(base_url()."admin/entities");
        exit;
    }

}
?>