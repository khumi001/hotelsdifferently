<?php

class Authorization extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin/authorization_model');
    }

    function index()
    {
        if(!$this->session->userdata('valid_adminuser'))
        {
            redirect('admin/login');
        }
        if ($this->blobmenuarray1['Authorization'] != 'yes') {
            exit();
        }
        $data['page'] = "admin/authorization/list";
        $data['breadcrumb'] = "Authorization";
        $data['breadcrumb_sub'] = "Authorization sub";
        $data['breadcrumb_list'] = array(
            array('Authorization', ''),
        );
        $data['authorization'] = 'class="start active"';
        $data['var_meta_title'] = 'Authorization';
        $data['var_meta_description'] = 'Authorization';
        $data['var_meta_keyword'] = 'Authorization';
        $data['js'] = array(
            //   'custom/table-managed.js',
            'admin/authorization.js',
        );
        $data['css_plugin'] = array(
            'select2/select2.css',
            'select2/select2-metronic.css',
            'data-tables/DT_bootstrap.css'
        );
        $data['js_plugin'] = array(
            'select2/select2.min.js',
            'data-tables/jquery.dataTables.js',
            'data-tables/DT_bootstrap.js',
        );
        $data['css'] = array(
        );

        $data['init'] = array(
            //  'TableManaged.init()',
            'Authorization.init()',
        );
        $this->db->where_in('chr_user_type', array('AF', 'U', 'A'));
        $data['userlist'] = $this->db->get("dz_user")->result_array();

        if ($this->input->is_ajax_request()) {
            $result = $this->toval->check_cookie_ajax($data['js'], $data['js_plugin'], $data['css'], $data['css_plugin']);
            $data['js'] = $result['js'];
            $data['js_plugin'] = $result['js_plugin'];
        } else {
            $result = $this->toval->check_cookie($data['js'], $data['js_plugin'], $data['css'], $data['css_plugin']);
            $data['js'] = $result['js'];
            $data['js_plugin'] = $result['js_plugin'];
        }

        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function getauthorizemenu()
    {
        if(!$this->session->userdata('valid_adminuser'))
        {
            redirect('admin/login');
        }
        $this->db->where('fk_user', $this->input->post('id'));
        $query = $this->db->get('dz_giveauthorize')->result_array();
        $blobmenustatus = $query[0]['var_authorization'];
        $blobstatusarray = unserialize(urldecode($blobmenustatus));
        $data['menu_detail'] = $blobstatusarray;
        $this->load->view('admin/authorization/checkbox', $data);
    }

    function checkmenu()
    {
        if(!$this->session->userdata('valid_adminuser'))
        {
            redirect('admin/login');
        }
        if ($this->input->post())
        {
            $this->db->where('fk_user', $this->input->post('id'));
            $row = $this->db->get('dz_giveauthorize')->num_rows();
            if ($row == 0)
            {
                $allmenu = array(
                    /*'Quote Requests' => 'no',
'Sent Quotes' => 'no',
'Hotel Price Updated' => 'no',
'Promo Codes' => 'no',*/
                    'Pending Enrollment'=>'no',
                    'Membership Database'=>'no',
                    'Sub Entities'=>'no',
                    'Entity Pending Enrollment'=>'no',
                    'Entities Database'=>'no',
                    'TP Pending Enrollments'=>'no',
                    'Approval Enrollments'=>'no',
                    'TP Database'=>'no',
                    'Confirmed Reservations' => 'no',
                    'Pending Reservations' => 'no',
                    'Pending Cancellations' => 'no',
                    /*'Cancel reservations' => 'no',*/
                    'Processed Cancellations' => 'no',
                    "Activities"=>'no',
                    'Coupons' => 'no',
                    /*'Affiliate Request' => 'no',
                    'Affiliate Database' => 'no',
                    'Affiliate Tracker' => 'no',
                    'Affiliate Payment' => 'no',
                    'Posted Payments' => 'no',
                    'Chargebacks' => 'no',
                    'Processed Chargebacks' => 'no',*/
                    'Authorization' => 'no',
                    'IP Address BAN' => 'no',
                    'Processed IP BAN' => 'no',
                    'Profile' => 'no',
                    'Master Account' => 'no',
                    'User Markup'=>'no',
                    'Statistics' => 'no',
                    'Revenue'=>'no',
                    /*'Database Management' => 'no',*/
                    'Login Log' => 'no',
                    'Admin Activity Log' => 'no',
                );

                foreach ($allmenu as $key => $value)
                {
                    for ($i = 0; $i < count($_POST['authocheck']); $i++)
                    {
                        if ($_POST['authocheck'][$i] == $key) {
                            $allmenu[$key] = 'yes';
                        }
                    }
                }
                $authocheckarray = serialize($allmenu);
                $menuvalue = urlencode($authocheckarray);
                $dataarray = array(
                    'fk_user' => $_POST['id'],
                    'var_authorization' => $menuvalue
                );
                $this->db->insert('dz_giveauthorize', $dataarray);
                //edit by tayyab for acativity log
                $CI = & get_instance();
                $CI->load->model('admin/admin_log_model');
                $log = array(
                    'log_type' => 'authorize_menu',
                    'log_type_fk_id' => $_POST['id'],//user id
                    'log_desc' => 'Menu is authorize.'
                );
                $CI->admin_log_model->addlog($log);

                $data_array = array(
                    'chr_user_type' => 'A',
                );
                $this->db->where('int_glcode', $_POST['id']);
                $this->db->update('dz_user', $data_array);
                echo 'success';
                exit;
            }
            else
            {
                $result = $this->authorization_model->menuauthorize($this->input->post());
                if ($result)
                {
                    echo 'success';
                }
                else
                {
                    echo 'error';
                }
            }
        }
    }

    function deleteuser()
    {
        if(!$this->session->userdata('valid_adminuser'))
        {
            redirect('admin/login');
        }
        if ($this->input->post()) {
            $res = $this->authorization_model->userdelete($this->input->post());
            if ($res) {
                echo 'success';
            } else {
                echo 'error';
            }
        }
    }
}
?>
