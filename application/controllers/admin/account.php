<?php

class Account extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin/loginlog_model');
        $this->load->model('toval');
    }

    function skip_btn_form()
    {
        if($this->input->post())
        {
            $code = $this->input->post('skip_code');
            if($code == SKIP_CODE)
            {
                echo 1;
            }
            else
            {
                echo 0;
            }
            exit;
        }
        echo 0;exit;
    }

    function verify_sms()
    {
        if ($this->input->post())
        {
            $token = $this->input->post('sms_code');
            $skip = $this->input->post('skip');
            $api_server = "https://api.authy.com";
            $api_key = "MKCCSG8rF0qpDxoZHJkz6fFuUy2pDPo5";
            $authy_id = 0;//"7894312"; // My Authy User ID
            $url = $api_server . "/protected/json/verify/" . $token . "/" . $authy_id . "?api_key=" . $api_key;
            // Initialize session and set URL.
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            // Set so curl_exec returns the result instead of outputting it.
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            // Get the response and close the channel.
            $response = curl_exec($ch);
            curl_close($ch);
            $response = json_decode($response, true);
            $this->session->unset_userdata('skipLogin');
            if($skip)
            {
                $this->session->set_userdata("skipLogin" , true);
            }
            if ($response['success'] || $this->config->item('test_authy') || $skip)
            {
                $this->email = $this->input->post('username');
                $this->password = $this->input->post('password');
                if ($this->email && $this->password)
                {
                    $row = $this->login_check();
                    $usertype = $row->chr_user_type;
                    $logid = $this->loginlog_model->add_loginlog($this->input->post());
                    if ($row == '1')
                    {
                        echo "baned";
                        exit();
                    }

                    if ($row)
                    {
                        $id = $row->int_glcode;
                        $this->db->select('int_logincnt');
                        $this->db->where('int_glcode', $id);
                        $data = $this->db->get('dz_user')->result_array();
                        $cntvalue = $data[0]['int_logincnt'];
                        if ($cntvalue >= 5)
                        {
                            echo "login error";
                            exit;
                        }
                        if ($row->chr_user_type == "A" || $row->chr_user_type == "U")
                        {
                            $this->db->where('fk_user', $row->int_glcode);
                            $query = $this->db->get('dz_giveauthorize')->result_array();
                            if(count($query) == 0)
                            {
                                die("Access Denied!");
                            }
                            $blobmenu = $query[0]['var_authorization'];
                            $blobmenuarray = unserialize(urldecode($blobmenu));

                            if(count($blobmenuarray) == 0)
                            {
                                die("Access Denied!");
                            }
                            $arr = array(
                                'var_email' => $row->var_email,
                                'id' => $row->int_glcode,
                                'var_usertype' => $row->chr_user_type,
                                'int_logincnt' => $row->int_logincnt,
                                'menu_details' => $query[0]['var_authorization']
                            );
                            $_SESSION['valid_adminuser'] = $arr;
                            $this->session->set_userdata('valid_adminuser' , $arr);
                            $data_array = array(
                                'int_logincnt' => '0',
                            );
                            $this->db->where('int_glcode', '1');
                            $this->db->update('dz_user', $data_array);
                            $data_array = array(
                                'var_status' => 'OK',
                            );
                            $this->db->where('int_glcode', $logid);
                            $this->db->update('dz_login_log', $data_array);
                            if ($blobmenuarray['Pending Reservations'] == 'yes' || $blobmenuarray['Pending Reservations'] == '')
                            {
                                redirect('admin/pen_res', 'location');
                            }
                            elseif ($blobmenuarray['Coupons'] == 'yes' || $blobmenuarray['Coupons'] == '')
                            {
                                redirect('admin/coupons', 'location');
                            }
                            elseif ($blobmenuarray['Authorization'] == 'yes' || $blobmenuarray['Authorization'] == '')
                            {
                                redirect('admin/authorization', 'location');
                            }
                            elseif ($blobmenuarray['IP Address BAN'] == 'yes' || $blobmenuarray['IP Address BAN'] == '')
                            {
                                redirect('admin/ipaddressban', 'location');
                            }
                            elseif ($blobmenuarray['Processed IP BAN'] == 'yes' || $blobmenuarray['Processed IP BAN'] == '')
                            {
                                redirect('admin/processipban', 'location');
                            }
                            elseif ($blobmenuarray['Login Log'] == 'yes' || $blobmenuarray['Login Log'] == '')
                            {
                                redirect('admin/log', 'location');
                            }
                            elseif ($blobmenuarray['Statistics'] == 'yes' || $blobmenuarray['Statistics'] == '')
                            {
                                redirect('admin/statistics', 'location');
                            }
                            elseif ($blobmenuarray['Profile'] == 'yes' || $blobmenuarray['Profile'] == '')
                            {
                                redirect('admin/profile', 'location');
                            }
                            elseif ($blobmenuarray['Master Account'] == 'yes' || $blobmenuarray['Master Account'] == '')
                            {
                                redirect('admin/masterlogin', 'location');
                            }
                            else
                            {
                                redirect('/', 'location');
                            }
                        }
                        else
                        {
                            echo "error";
                            exit();
                        }
                    }
                }
            }
            else
            {
                redirect('admin/login', 'location');
            }
        }
        else
        {
            redirect('admin/login', 'location');
        }
    }

    function login()
    {
        if($this->session->userdata('valid_adminuser'))
        {
            redirect('admin/profile');
        }
        $data['page'] = "admin/account/login";
        $data['var_meta_title'] = 'Dealz Login';
        $data['var_meta_description'] = 'Dealz Login';
        $data['var_meta_keyword'] = 'Dealz Login';
        $data['js'] = array(
        );
        $data['js_plugin'] = array(
        );
        $data['css'] = array(
        );
        $data['init'] = array(
        );
        if ($this->input->post())
        {
            $this->email = $this->input->post('username');
            $this->password = $this->input->post('password');
            if ($this->email && $this->password)
            {
                $row = $this->login_check();
                $usertype = $row->chr_user_type;
                //Delete due to double entry
                //$logid = $this->loginlog_model->add_loginlog($this->input->post(), $usertype);
                if ($row == '1')
                {
                    echo "baned";
                    exit();
                }
                if ($row)
                {
                    $api_server = "https://api.authy.com";
                    //$api_key = "poe1jyGssCXGtm1ZjN85n483ISz4Av1Q"; Raheel account
                    $api_key = "MKCCSG8rF0qpDxoZHJkz6fFuUy2pDPo5";
                    //$authy_id = "24411048"; // My Authy User ID Raheel account
                    $authy_id = 0;//"7894312"; // My Authy User ID
                    $url = $api_server . "/protected/json/sms/" . $authy_id . "?api_key=" . $api_key;
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    $response = curl_exec($ch);
                    curl_close($ch);
                    $response = json_decode($response, true);
                    //var_dump($response);exit;
                    if ($response['success'] || $this->config->item('test_authy'))
                    {
                        $this->session->set_userdata('code_sent', 'yes');
                        $this->session->set_userdata('session_username', $this->input->post('username'));
                        $this->session->set_userdata('session_password', $this->input->post('password'));
                        echo "admin/login";
                        exit();
                    }
                    else
                    {
                        echo "authy_error";
                        exit();
                    }
                }
                else
                {
                    echo "notexist";
                    exit();
                }
            }
        }
        else
        {
            $this->load->view(ADMIN_LAYOUT_LOGIN, $data);
        }

        //$data['content'] = $this->models_models->get_models_list();
    }

    function index() {

        $this->login();
    }

    function login_check()
    {
        $row = $this->toval->idtorow('var_email', $this->email, 'dz_user');
        if (!empty($row))
        {
            if ($row->var_password == base64_encode($this->password))
            {
                $id = $row->int_glcode;
                $this->db->where('int_glcode', $id);
                $query = $this->db->get('dz_user');
                $data = $query->result_array();
                $cnt = $this->loginlog_model->addlogincnt1($_SERVER['REMOTE_ADDR']);
                $count = 5 - $cnt;

                if ($count <= 0)
                {
                    $enddate = date('Y-m-d H:i:s', strtotime($row->dt_updated_date . '+6 houres'));
                    if (date('Y-m-d H:i:s') > $enddate)
                    {
                        $lastlogin = array(
                            'int_logincnt' => $count
                        );
                        $this->db->where('int_glcode', $row->int_glcode);
                        $this->db->update('dz_user', $lastlogin);
                    }
                    echo "last_error";
                    exit();
                    return FALSE;
                }
                else
                {
                    $this->db->where('ip' , $_SERVER['REMOTE_ADDR'])->delete('ip_wrong_request');
                    $this->db->where('var_ipban' , $_SERVER['REMOTE_ADDR'])->delete('dz_ban_ip_address');
                    $lastlogin = array(
                        'int_logincnt' => 0
                    );
                    $this->db->where('int_glcode', $row->int_glcode);
                    $rslt = $this->db->update('dz_user', $lastlogin);
                    if ($rslt == 1)
                    {
                        return $row;
                    }
                    else
                    {
                        return 1;
                    }
                }
            }
            else
            {
                $id = $row->int_glcode;
                $this->db->where('int_glcode', $id);
                $query = $this->db->get('dz_user');
                $data = $query->result_array();
                $cnt = $this->loginlog_model->addlogincnt1($_SERVER['REMOTE_ADDR']);
                $count = 5 - $cnt;
                if ($count <= 0)
                {
                    $startdate = date('Y-m-d H:i:s');
                    $enddate = date('Y-m-d H:i:s', strtotime('+6 hours'));
                    $ip_ban = array(
                        'fk_user' => $id,
                        'var_ipban' => $_SERVER['REMOTE_ADDR'],
                        'var_banaue' => $data[0]['var_username'],
                        'var_reason' => 'Failed login attempts',
                        'chr_status' => 'B',
                        'var_banby' => 'SYSTEM',
                        'date_banstart_time' => $startdate,
                        'date_banend_time' => $enddate,
                        'var_createddate' => date('Y-m-d H:i:s'),
                        'var_updateddate' => date('Y-m-d H:i:s'),
                    );
                    $this->db->insert('dz_ban_ip_address', $ip_ban);
                    $this->db->where('ip' , $_SERVER['REMOTE_ADDR'])->delete('ip_wrong_request');
                    echo "last_error";
                    exit();
                }
                else
                {
                    $logid = $this->loginlog_model->add_loginlog($this->input->post());
                    echo $count;
                    exit();
                }
                return false;
            }
        }
        else
        {
            $logid = $this->loginlog_model->add_loginlog($this->input->post());
            return false;
        }
    }

    /*function login_check()
    {
        $row = $this->toval->idtorow('var_email', $this->email, 'dz_user');
        if (!empty($row))
        {
            if ($row->var_password == base64_encode($this->password))
            {
                $id = $row->int_glcode;
                $this->db->where('int_glcode', $id);
                $query = $this->db->get('dz_user');
                $data = $query->result_array();
                $cnt = $data[0]['int_logincnt'];

                if ($cnt < 5)
                {
                    $this->db->select('dz_user.int_glcode,dz_user.var_email,dz_ban_ip_address.chr_status');
                    $this->db->from('dz_user');
                    $this->db->join('dz_ban_ip_address', 'dz_user.int_glcode = dz_ban_ip_address.fk_user');
                    $this->db->where('dz_ban_ip_address.chr_status', 'B');
                    $this->db->where('dz_user.int_glcode', $row->int_glcode);
                    $rslt = $this->db->get()->num_rows();
                    $lastlogin = array(
                        'int_logincnt' => 0
                    );
                    $this->db->where('int_glcode', $row->int_glcode);
                    $this->db->update('dz_user', $lastlogin);
                    if ($rslt == 1)
                    {
                        return $rslt;
                    }
                    else
                    {
                        return $row;
                    }
                }
                else
                {
                    $enddate = date('Y-m-d H:i:s', strtotime($row->dt_updated_date . '+6 houres'));
                    if (date('Y-m-d H:i:s') > $enddate)
                    {
                        $lastlogin = array(
                            'int_logincnt' => $cnt
                        );
                        $this->db->where('int_glcode', $row->int_glcode);
                        $this->db->update('dz_user', $lastlogin);
                    }
                    echo "last_error";
                    return FALSE;
                }
            }
            else
            {
                $id = $row->int_glcode;
                $this->db->where('int_glcode', $id);
                $query = $this->db->get('dz_user');
                $data = $query->result_array();
                $cnt = $data[0]['int_logincnt'];
                if ($cnt < 4)
                {
                    $cnt1 = 4 - $cnt;
                    $logincnt = $this->loginlog_model->addlogincnt($cnt, $id);
                    echo $cnt1;
                    exit();
                }
                else
                {
                    echo "last_error";
                }
                $logincnt = $this->loginlog_model->addlogincnt($cnt, $id);
                return false;
            }
        }
        else
        {
            return false;
        }
    }*/

    function sign_up() {

        if ($this->input->post("h_sign_up") == "T") {

            $this->load->library('form_validation');

            $email_check = $this->form_validation->set_rules('var_email', 'Email', 'is_unique[ii_user.var_email]');

            if ($email_check->run() == TRUE) {

                $this->account_model->sign_up($this->input->post());

                echo "success";

                exit;
            } else {

                echo "failure";

                exit;
            }
        }
    }

    function logout()
    {
        $this->session->unset_userdata('valid_adminuser');
        redirect('admin/login');
    }

    function forgot_password()
    {

        if ($this->input->post("f_pass") == "T") {

            if ($this->input->post("email") != "") {

                $return = $this->account_model->forgot_password($this->input->post());

                if ($return == TRUE) {

                    echo "success";
                } else {

                    echo "fail";
                }
            } else {

                echo "enteremail";
            }
        }
    }

    function ipban($banbyadmin = NULL) {
        if ($banbyadmin != '') {

            $row = $this->toval->idtorow('var_email', $banbyadmin, 'dz_user');

            $banbyadmin = $row->int_glcode;

            $ipban = $this->toval->ipban($banbyadmin);

            $data['userid'] = $banbyadmin;
        }
        else
        {
            $ipban = $this->toval->ipban();
        }

        //  $ipban = $this->toval->ipban();

        if ($ipban[0]['var_reason'] == "Failed login attempts")
        {
            $data['message'] = 'We take the security of our Member and Affiliate accounts very seriously, therefore your IP address has been banned due to too many failed login attempts. We apologize for the inconvenience and please try again later. Remember, next time use the "Forgot password?" page to retrieve your login credentials.';
        }
        else
        {

            $data['message'] = $ipban[0]['var_reason'];
        }


        $timeFirst  = strtotime(date('Y-m-d H:i:s'));
        $timeSecond = strtotime($ipban[0]['date_banend_time']);
        $differenceInSeconds = $timeSecond - $timeFirst;
        if($differenceInSeconds <= 0)
        {
            $da = array(
                "chr_status" =>  "A"
            );
            $this->db->where('int_glcode' , $ipban[0]['int_glcode'])->update('dz_ban_ip_address' , $da);
            $this->db->where('ip' , $_SERVER['REMOTE_ADDR'])->delete('ip_wrong_request');
            redirect("/");
        }

        $data['year'] = date('Y', strtotime($ipban[0]['var_updateddate']));

        $data['month'] = date('m', strtotime($ipban[0]['var_updateddate']));

        $data['date'] = date('d', strtotime($ipban[0]['var_updateddate']));

        $data['houres'] = date('H', strtotime($ipban[0]['var_updateddate']));

        $data['min'] = date('i', strtotime($ipban[0]['var_updateddate']));

        $data['sec'] = date('s', strtotime($ipban[0]['var_updateddate']));
        $data['diffrenceinsec'] = $differenceInSeconds;

        $this->load->view('admin/account/ipban', $data);
    }

    function timecall($userid = NULL) {

        if ($this->input->post('datime')) {

            $ipban = $this->toval->ipban($userid);

            //  echo date('Y-m-d H:i:s');

            $datetime1 = new DateTime(date('Y-m-d H:i:s'));

            $datetime2 = new DateTime($ipban[0]['date_banend_time']);

            //  echo $ipban[0]['date_banend_time'];

            $interval = $datetime1->diff($datetime2);

            //  print_r($interval);

            $time = array(
                'days' => $interval->days,
                'houres' => $interval->h,
                'minutes' => $interval->i,
                'seconds' => $interval->s,
            );

            echo json_encode($time);
        }
    }
}
?>