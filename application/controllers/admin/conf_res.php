<?php

class Conf_res extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin/con_reservation');
        $this->load->helper('tourico_api');
    }

    function changeTransectionNum($id)
    {
        if(!$this->session->userdata('valid_adminuser'))
        {
            redirect('admin/login');
        }
        if(isset($_POST['nconf']))
        {
            $row = $this->db->where('id' , $id)->get('booking_detail')->result();
            if(count($row) > 0)
            {
                $data = array(
                    "admin_changed" => 1,
                    "reservationId" => $this->input->post('nconf')
                );
                $this->db->where('id' , $id)->update('booking_detail' , $data);
                $CI = & get_instance();
                $CI->load->model('admin/admin_log_model');
                $log = array(
                    'log_type' => 'change_con_num',
                    'log_type_fk_id' => $id,
                    'log_desc' => 'changed Tourico reservation number '.$row[0]->reservationId.' To '.$this->input->post('nconf')
                );
                return  $CI->admin_log_model->addlog($log);
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    function get_sources()
    {
        if(!$this->session->userdata('valid_adminuser'))
        {
            redirect('admin/login');
        }
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
        $chingParameter = $this->input->get('term', TRUE);
        $this->db->like('name', $chingParameter);
        $query = $this->db->get('dz_booking_source');
        $sourceList = $query->result_array();
        $response = array();
        foreach ($sourceList as $aLocation) {
            $response[] = array(
                'id' => $aLocation['id'],
                'label' => $aLocation['name'],
                'phone' => $aLocation['phone'],
                'value' => $aLocation['name'],
                'shortName'=>$aLocation['shortName'],
                'text_order' => strpos(strtolower($aLocation['name']), strtolower($chingParameter))
            );
        }
        $response = $this->sortByNestedArray($response);
        echo json_encode($response);
    }

    function sortByNestedArray($arr)
    {
        if(!$this->session->userdata('valid_adminuser'))
        {
            redirect('admin/login');
        }
        for($i = 0 ; $i < count($arr) ; $i++)
        {
            for ($j=0; $j<count($arr)-1-$i; $j++)
            {
                if ($arr[$j+1]['text_order'] < $arr[$j]['text_order'])
                {
                    $this->swap($arr, $j, $j+1);
                }
            }
        }
        return $arr;
    }

    function changesource_res()
    {
        if(!$this->session->userdata('valid_adminuser'))
        {
            redirect('admin/login');
        }
        $id = $this->input->post('id');
        //$shortName = $this->input->post('shortName');
        $source_id = $this->input->post('source_id');
        $data = array(
            "sourceID" => $source_id,
            //'hotelType'=>$shortName
        );
        if(getSourceOfReservation($id) != $source_id)
        {
            $CI = & get_instance();
            $CI->load->model('admin/admin_log_model');
            $log = array(
                'log_type' => 'change_res_source',
                'log_type_fk_id' => $id,
                'log_desc' => 'changed reservation source '.getSourcename(getSourceOfReservation($id)).' To '.getSourcename($source_id)
            );
            $CI->admin_log_model->addlog($log);
        }
        echo $this->db->where('id' , $id)->update("booking_detail" , $data);
    }

    function index()
    {
        if(!$this->session->userdata('valid_adminuser'))
        {
            redirect('admin/login');
        }
        $data['page'] = "admin/con_res/conformation_list";
        $data['breadcrumb'] = "Confirm Reservation";
        $data['breadcrumb_sub'] = "Confirm Reservation Sub";
        $data['breadcrumb_list'] = array(
            array('Confirm Reservation', ''),
        );
        $data['conf_res'] = 'class="start active"';
        $data['var_meta_title'] = 'Confirm Reservation';
        $data['var_meta_description'] = 'Confirm Reservation';
        $data['var_meta_keyword'] = 'Confirm Reservation';
        $data['js'] = array(
            'custom/jquery.maskMoney.js',
            'custom/table-managed.js',
            'admin/confirm_reservation.js',
            'custom/components-pickers.js',
            'custom/components-dropdowns.js',
        );
        $data['css_plugin'] = array(
            'select2/select2.css',
            'select2/select2-metronic.css',
            'data-tables/DT_bootstrap.css',
            'bootstrap-datepicker/css/datepicker.css',
        );
        $data['js_plugin'] = array(
            'select2/select2.min.js',
            'data-tables/jquery.dataTables.js',
            'data-tables/DT_bootstrap.js',
            'bootstrap-datepicker/js/bootstrap-datepicker.js',
        );
        $data['css'] = array(
        );
        $data['init'] = array(
            'TableManaged.init()',
            'Quote.init()',
            'ComponentsPickers.init()'
        );
        $data['confReservations'] = $this->db->order_by("id", "desc")->where('status' , 'Confirm')->where('cancel_status' , 0)->get('booking_detail')->result();
        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function getFilterResult(){
        if($this->input->post('status')) {
            $status = $this->input->post('status');
            if($status == 1){
                // active
                $data['confReservations'] = $this->db->order_by("id", "desc")->where('DATE(toDate) >= DATE(NOW() - INTERVAL 1 DAY)')->where('status', 'Confirm')->where('cancel_status', 0)->get('booking_detail')->result();
            }else if($status == 2){
                // expired
                $data['confReservations'] = $this->db->order_by("id", "desc")->where('DATE(toDate) <= DATE(NOW() - INTERVAL 2 DAY)')->where('status', 'Confirm')->where('cancel_status', 0)->get('booking_detail')->result();
                //echo '<pre>';print_r($data['confReservations']);exit;
            }else{
                // all
                $data['confReservations'] = $this->db->order_by("id", "desc")->where('status', 'Confirm')->where('cancel_status', 0)->get('booking_detail')->result();
            }
            $this->load->view("admin/con_res/filter_result", $data);
        }else{
            echo 'Invalid request';
        }
    }

    function datatable($fist = "", $secound = "")
    {
        if(!$this->session->userdata('valid_adminuser'))
        {
            redirect('admin/login');
        }
        $this->load->library('Datatables');
        if ($fist != 0) {
            $from = $this->datatables->where('dz_quote.var_checkin', $fist);
        }
        if ($secound != 0) {
            $to = $this->datatables->where('dz_quote.var_checkout', $secound);
        }
        $this->datatables->select('
            dz_rplyquote.var_createddate,
            dz_user.var_username,
            dz_user.var_email,
            dz_user.var_accountid,
            dz_rplyquote.var_transactionid,
            dz_rplyquote.var_prize,
            dz_user.int_glcode,
            dz_rplyquote.int_fkquote,
            dz_rplyquote.chr_status,
            dz_rplyquote.fk_coupon,
           
            ');
        $this->datatables->from('dz_rplyquote');
        $this->datatables->where('dz_rplyquote.chr_status', 'P');
        $this->datatables->where('dz_rplyquote.var_admin_soures !=', 'NULL');
        $this->datatables->where('dz_rplyquote.var_admin_amount !=', 'NULL');
        $this->datatables->where('dz_rplyquote.var_soures_confirmation !=', 'NULL');
        $this->datatables->join('dz_quote', 'dz_rplyquote.int_fkquote = dz_quote.int_glcode');
        $this->datatables->join('dz_user', 'dz_quote.var_fkuser = dz_user.int_glcode');
//        $this->datatables->order_by('var_createddate', 'DESC');
        //$this->datatables->join('dz_commision', 'dz_rplyquote.int_fkquote=dz_commision.fk_quote');
        $results = $this->datatables->generate();
//        echo $this->db->last_query();exit;
        $results = json_decode($results, true);
//          print_r($results['aaData']);
//           exit();
        for ($i = 0; $i < count($results['aaData']); $i++) {
            $price = "";
            if ($results['aaData'][$i][9] != "" && $results['aaData'][$i][9] != 0) {
                $price = $this->getfinalamount1($results['aaData'][$i][5], $results['aaData'][$i][9]);
            } else {
                $price = $results['aaData'][$i][5];
            }
            $email = $results['aaData'][$i][1];
            $quotid = $results['aaData'][$i][7];
            //  $data['finalamount'] = $this->getfinalamount1($data['confirm'][0]['var_prize'],$data['confirm'][0]['fk_coupon']);
            $results['aaData'][$i][0] = $results['aaData'][$i][0];
            $results['aaData'][$i][1] = $results['aaData'][$i][1];
            $results['aaData'][$i][2] = $results['aaData'][$i][2];
            $results['aaData'][$i][3] = $results['aaData'][$i][3];
            $results['aaData'][$i][4] = $results['aaData'][$i][4];
            $results['aaData'][$i][5] = $price;
            $results['aaData'][$i][6] = '<a href="#myModal_autocomplete" data-toggle="modal" data-id="' . $quotid . '" id="conform_list" class="sentquote label label-sm label-success">View</a>';
        }
//        print_r($results);exit;
        echo json_encode($results);
    }

    function getconfirm_res()
    {
        if(!$this->session->userdata('valid_adminuser'))
        {
            redirect('admin/login');
        }
        $id = $this->input->post('id');
        $this->db->select('
             dz_quote.var_checkin,
             dz_quote.var_checkout,
             dz_quote.var_city,
             dz_quote.var_rating,
             dz_quote.var_comments,
             dz_quote.var_room,
             dz_quote.var_adult,
             dz_quote.var_child,
             dz_quote.var_hotelname,
             dz_rplyquote.fk_coupon,
             dz_rplyquote.var_prize,
             dz_rplyquote.var_NOH,
             dz_rplyquote.var_prize,
             dz_rplyquote.var_uniq_quoteid,
             dz_rplyquote.var_policy,
             dz_rplyquote.var_star,
             dz_rplyquote.var_transactionid,
             dz_rplyquote.var_tripadvisor,
             dz_rplyquote.var_src,
             dz_rplyquote.int_src_price,
             dz_rplyquote.var_admin_soures,
             dz_rplyquote.var_admin_amount,
             dz_rplyquote.var_soures_confirmation,
             dz_rplyquote.var_comment,
             ');
        $this->db->from('dz_rplyquote');
        $this->db->where('dz_rplyquote.int_fkquote', $id);
        $this->db->join('dz_quote', 'dz_rplyquote.int_fkquote = dz_quote.int_glcode');
//      $this->db->join('dz_commision', 'dz_rplyquote.int_fkquote = dz_commision.fk_quote');
        $data['confirm'] = $this->db->get()->result_array();
        if ($data['confirm'][0]['fk_coupon'] != "" && $data['confirm'][0]['fk_coupon'] != 0) {
            $data['finalamount'] = $this->getfinalamount1($data['confirm'][0]['var_prize'], $data['confirm'][0]['fk_coupon']);
            $this->db->select('*');
            $this->db->where('int_glcode', $data['confirm'][0]['fk_coupon']);
            $coupan = $this->db->get('dz_coupons')->result_array();

            if ($coupan[0]['var_coupan_type'] == 1) {
                // echo $coupan[0]['var_couponvalue'] - $this->input->post('price');
                $coupanstr = $coupan[0]['var_couponvalue'] . "% OFF";
            }
            if ($coupan[0]['var_coupan_type'] == 2) {
                $coupanstr = "$" . $coupan[0]['var_couponvalue'] . ' OFF';
            }
            $data['coupanstr'] = $coupanstr;
        } else {
            $data['finalamount'] = $data['confirm'][0]['var_prize'];
            $data['coupanstr'] = 'No coupons';
        }
//        print_r($data['confirm']);
//        $data['confirm'] = $this->con_reservation->confirm_getdata($id); 
//        print_r($data);exit;
        $this->load->view('admin/con_res/popupquoteslist', $data);
    }

    function getfinalamount1($paid1, $coupanid)
    {
        if(!$this->session->userdata('valid_adminuser'))
        {
            redirect('admin/login');
        }
        //  if($this->input->post()){
        $this->db->select('*');
        $this->db->where('int_glcode', $coupanid);
        $coupan = $this->db->get('dz_coupons')->result_array();

        if ($coupan[0]['var_coupan_type'] == 1) {
            // echo $coupan[0]['var_couponvalue'] - $this->input->post('price');
            $discoutamount = floatval(($coupan[0]['var_couponvalue'] * $paid1) / 100);
            $discoutamount = number_format((float) $discoutamount, 2, '.', '');
            $acutelamount = $paid1 - $discoutamount;
            $acutelamount = number_format((float) $acutelamount, 2, '.', '');
            return $acutelamount;
        }
        if ($coupan[0]['var_coupan_type'] == 2) {
            $acutelamount = $paid1 - $coupan[0]['var_couponvalue'];
            $acutelamount = number_format((float) $acutelamount, 2, '.', '');
            return $acutelamount;
        }
        //  }
    }

}
?>

