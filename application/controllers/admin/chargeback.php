<?php

class Chargeback extends Admin_Controller {

    function __construct() {
        
        parent::__construct();
        $this->load->model('admin/chargeback_model');
        
    }

    function index() 
	{
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
        if ($this->blobmenuarray1['Chargebacks'] != 'yes') {
            exit();
        }
        $data['page'] = "admin/chargeback/chargelist";
        $data['breadcrumb'] = "Chargeback";
        $data['breadcrumb_sub'] = "Chargeback Sub";
        $data['breadcrumb_list'] = array(
            array('Chargeback', ''),
        );
        $data['chargebacks'] = 'class="start active"';
        $data['var_meta_title'] = 'Chargeback Home';
        $data['var_meta_description'] = 'Chargeback Home';
        $data['var_meta_keyword'] = 'Chargeback Home';
        $data['js'] = array(
            'custom/jquery.maskMoney.js',
            'admin/chargeback.js',
            'custom/table-managed.js',
        );
        $data['css_plugin'] = array(
            'select2/select2.css',
            'select2/select2-metronic.css',
            'data-tables/DT_bootstrap.css'
        );
        $data['js_plugin'] = array(
            'select2/select2.min.js',
            'data-tables/jquery.dataTables.js',
            'data-tables/DT_bootstrap.js',
        );
        $data['css'] = array(
        );

        $data['init'] = array(
            'TableManaged.init()',
            'Chargeback.init()',
        );
        if ($this->input->is_ajax_request()) {
            $result = $this->toval->check_cookie_ajax($data['js'], $data['js_plugin'], $data['css'], $data['css_plugin']);
            $data['js'] = $result['js'];
            $data['js_plugin'] = $result['js_plugin'];
        } else {
            $result = $this->toval->check_cookie($data['js'], $data['js_plugin'], $data['css'], $data['css_plugin']);
            $data['js'] = $result['js'];
            $data['js_plugin'] = $result['js_plugin'];
        }
//        $this->db->select('dz_user.int_glcode,dz_user.var_accountid,dz_user.var_username,dz_user.var_email,dz_user.var_fname,dz_user.var_lname,dz_user.var_payment,dz_user.var_country,dz_chargeback.int_glcode as id');
//        $this->db->from('dz_user');
//        $this->db->join('dz_chargeback','dz_chargeback.fk_user = dz_user.int_glcode');
//        $this->db->where('dz_chargeback.chr_status','NCB');  
//        $data['charge'] = $this->db->get()->result_array();

        $data['charge'] = $this->db->get_where('dz_user', array('chr_user_type' => 'AF', 'chr_status' => 'A'))->result_array();

        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function add_data() 
	{
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
        if ($this->input->post()) {
            $result = $this->chargeback_model->add_data($this->input->post());
            if ($result) {
                echo 'sucess';exit;
            } else {
                echo 'error';exit;
            }
        }
    }

    function get_affilate() 
	{
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
        $query = $this->db->get_where('dz_user', array('int_glcode' => $_POST['affiliate']))->result_array();
        echo json_encode($query);
        exit();
    }

    function get_amount() 
	{
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
         echo "success";
            exit;
        $result = $this->db->get_where('dz_commision', array('fk_user' => $_POST['id'],'chr_status' => 'U'))->result_array();
        $sum = 0;
        for ($k = 0; $k < count($result); $k++) {
            $sum = $sum + $result[$k]['var_commision_amount'];
        }
        if ($sum >= $_POST['amount']) {
            echo "success";
            exit;
        } else {
            echo "error";
        }
    }
    
    function canclerefund()
    {
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
        $replyquotes = $this->input->post('replyquotes');
        $cancle = $this->input->post('canceltype');
        if($cancle == 'coupanamount')
        {
            $price = $this->input->post('amount1');
        }
        else {
            $price = $this->input->post('amount2');
        }
        date_default_timezone_set('America/Los_Angeles');
        $updated = array(
            'var_cancalationtype' => $cancle,
            'var_refundamount' => $price,
            'prosseduser_id' => $this->session->userdata['valid_adminuser']['id'],
            'var_updateddate' => date('Y-m-d H:i:s')
        );
        $this->db->where('int_glcode',$replyquotes);
        $this->db->update('dz_rplyquote',$updated);
        
          $this->db->select('dz_quote.*
              ,dz_user.*,
              dz_quote.var_city as quotecity,
            dz_rplyquote.var_site1,
            dz_rplyquote.int_site1_price,
            dz_rplyquote.var_site2,
            dz_rplyquote.int_site2_price,
            dz_rplyquote.var_NOH,
            dz_rplyquote.var_tripadvisor,
            dz_rplyquote.var_uniq_quoteid,
            dz_rplyquote.var_policy,
            dz_rplyquote.var_comment,
            dz_rplyquote.var_star,
            dz_rplyquote.var_NOH,
            dz_rplyquote.var_prize,
            dz_rplyquote.var_src,
            dz_rplyquote.int_src_price');
        $this->db->from('dz_quote');
        $this->db->join('dz_rplyquote', 'dz_quote.int_glcode = dz_rplyquote.int_fkquote');
        $this->db->join('dz_user', 'dz_user.int_glcode = dz_quote.var_fkuser');
        $this->db->where_in('dz_rplyquote.int_glcode', $replyquotes);
        $data['confirm'] = $this->db->get()->result_array();
//        print_r($data['confirm']);
//        exit();
        $resarvationdetail = $this->load->view('admin/account/resarvationdetail', $data, TRUE);
       // echo $cancle; exit;
      //  print_r($query);
        
        $this->email->set_mailtype("html");
        $this->email->from(NOREPLY, 'Admin');
        $this->email->to($data['confirm'][0]['var_email']);
        $this->email->subject('Cancellation confirmation');
        $mail_body = "";
        if($this->input->post('canceltype') == 'coupanamount')
        {
            $mail_body.='<p style="color:#fff;margin-bottom:10px;">Dear ' . $data['confirm'][0]['var_fname'].' '. $data['confirm'][0]['var_lname'].',</p>';    
            $mail_body.='<p style="color:#fff;margin-bottom:10px;">We are sorry to hear that you decided to cancel your reservation:</p>';
            $mail_body.= $resarvationdetail; 
            $mail_body.= '<p style="color:#fff;margin-bottom:10px;">Your reservation was non-refundable, therefore you are NOT eligible for a refund as per the cancellation policy you were quoted when you accepted and paid for it….<b> BUT GOOD NEWS!</b></p>';
            $mail_body.= '<p style="color:#fff;margin-bottom:10px;">We are issuing you a courtesy credit of $'. $price .' in form of a coupon. You can login to your account and make a new reservation using that coupon and there is NO MINIMUM amount you need to comply with in order to use it. Just get a quote, make a reservation and enjoy your savings!</p>';
            $mail_body.= '<p style="color:#fff;margin-bottom:10px;"><b>Your cancellation is NOW CONFIRMED!</b></p>';
            $mail_body.= '<p style="color:#fff;margin-bottom:10px;">We strive to provide you an excellent customer service and should you have any questions please do not hesitate to contact us. Hope to see you back on our site soon!</p>';
            $mail_body.= '<p style="color:#fff;margin-bottom:10px;">Sincerely,<br/><b>HotelsDifferently<sup>sm</sup></b> <br/><span style="font-size:9px;">YOUR VACATION BEGINS HERE!</spam></p>';
         //   $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Cancellation policies between <b>HotelsDifferently<sup>sm</sup></b> quotes and competitor sites may vary.</p>';
            $data['mail_body'] = $mail_body;
            $message = $this->load->view('admin/account/genral_emailtemplate', $data, TRUE);
           // echo $message;
            $this->email->message($message);
            $eamilsend = $this->email->send();
            
        }
        
        elseif($this->input->post('canceltype') == 'refund') {
            
            $mail_body.='<p style="color:#fff;margin-bottom:10px;">Dear ' . $data['confirm'][0]['var_fname'].' '. $data['confirm'][0]['var_lname'] . ',</p>';    
            $mail_body.='<p style="color:#fff;margin-bottom:10px;">We are sorry to hear that you decided to cancel your reservation:</p>';
            $mail_body.= $resarvationdetail; 
            $mail_body.= '<p style="color:#fff;margin-bottom:10px;">Your reservation was fully refundable; therefore we will issue you a <b>full refund </b>in the amount of $'. $price .' to your original form of payment. You can expect it to show up on your account within the next<b> 7-10 business days.</b></p>';
//            $mail_body.= '<p style="color:#fff;margin-bottom:10px;">We are issuing you a courtesy credit of $'. $price .' in form of a coupon. You can login to your account and make a new reservation using that coupon and there is NO MINIMUM amount you need to comply with in order to use it. Just get a quote, make a reservation and enjoy your savings!</p>';
            $mail_body.= '<p style="color:#fff;margin-bottom:10px;"><b>Your cancellation is NOW CONFIRMED!</b></p>';
            $mail_body.= '<p style="color:#fff;margin-bottom:10px;">We strive to provide you an excellent customer service and should you have any questions please do not hesitate to contact us. Hope to see you back on our site soon!</p>';
            $mail_body.= '<p style="color:#fff;">Sincerely,<br/><b>HotelsDifferently<sup>sm</sup></b>  <br/><span style="font-size:9px;">YOUR VACATION BEGINS HERE!</spam></p>';
            $data['mail_body'] = $mail_body;
            $message = $this->load->view('admin/account/genral_emailtemplate', $data, TRUE);
          //  echo $message;
            $this->email->message($message);
            $eamilsend = $this->email->send();
            
        }
//        exit();
        else if($this->input->post('canceltype') == 'norefund'){
            
            $mail_body.='<p style="color:#fff;margin-bottom:10px;">Dear ' . $data['confirm'][0]['var_fname'].' '. $data['confirm'][0]['var_lname'] . ',</p>';    
            $mail_body.='<p style="color:#fff;margin-bottom:10px;">We are sorry to hear that you decided to cancel your reservation:</p>';
            $mail_body.= $resarvationdetail; 
            $mail_body.= '<p style="color:#fff;margin-bottom:10px;">Your reservation was non-refundable, therefore you are NOT eligible for a refund as per the cancellation policy you were quoted when you accepted and paid for it.</p>';
            $mail_body.= '<p style="color:#fff;margin-bottom:10px;"><b>Your cancellation is NOW CONFIRMED!</b></p>';
            $mail_body.= '<p style="color:#fff;margin-bottom:10px;">We strive to provide you an excellent customer service and should you have any questions please do not hesitate to contact us.</p>';
            $mail_body.= '<p style="color:#fff;">Sincerely,<br/><b>HotelsDifferently<sup>sm</sup></b>  <br/><span style="font-size:9px;">YOUR VACATION BEGINS HERE!</spam></p>';
         
            $data['mail_body'] = $mail_body;
            $message = $this->load->view('admin/account/genral_emailtemplate', $data, TRUE);
           
            $this->email->message($message);
           $eamilsend = $this->email->send();
        }
//         echo $message;
//         exit();
        if($eamilsend)
        {
            echo 'success';
            exit();
        }    
        else
        {
            echo 'error';
            exit();
        }
    }

}

?>