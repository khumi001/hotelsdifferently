<?php

class Statistics extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('Datatables');
    }

    function index() 
	{
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
        if ($this->blobmenuarray1['Statistics'] != 'yes') {
            exit();
        }
        $data['page'] = "admin/statistics/statilist";
        $data['breadcrumb_list'] = array(
            array('Statistics', ''),
        );
        $data['statistics'] = 'class="start active"';
        $data['var_meta_title'] = 'Statistics';
        $data['var_meta_description'] = 'Statistics';
        $data['var_meta_keyword'] = 'Statistics';
        $data['js'] = array(
            'admin/static.js',
        //    'custom/table-managed.js',
            'custom/components-pickers.js',
        );
        $data['js_plugin'] = array(
            'data-tables/jquery.dataTables.js',
            'select2/select2.min.js',
            'data-tables/DT_bootstrap.js',
            'bootstrap-datepicker/js/bootstrap-datepicker.js',
        );
        $data['css_plugin'] = array(
            'select2/select2.css',
            'select2/select2-metronic.css',
            'data-tables/DT_bootstrap.css',
            'bootstrap-datepicker/css/datepicker.css',
        );
        $data['css'] = array(
        );
        $data['init'] = array(
            'Static.init()',
       //     'Charts.initPieCharts()',
            'ComponentsPickers.init()',
         //   'TableManaged.init()',
        );
        $this->db->select('var_abuot_us');
        $this->db->group_by('var_abuot_us');
        $about = $this->db->get('dz_user')->result_array();
        for($i=0;$i<count($about);$i++)
        {
            $this->db->where('var_abuot_us',$about[$i]['var_abuot_us']);
            $about[$i]['count'] = $this->db->get('dz_user')->num_rows();
        }
        $data['about'] = $about;
        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function datatable($fist = "", $secound = "") 
	{
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
//        if ($fist != 0) {
//            $from = $this->datatables->where('dz_quote.var_checkin', $fist);
//        }
//        if ($secound != 0) {
//            $to = $this->datatables->where('dz_quote.var_checkout', $secound);
//        }

        $this->datatables->select('dz_user.int_glcode,dz_user.var_accountid,dz_user.var_email,dz_user.var_fname,dz_user.var_lname,dz_commision.fk_user');
        $this->datatables->from('dz_user');
        $this->datatables->where('dz_user.chr_user_type', 'AF');
        $this->datatables->where('dz_user.int_glcode !=', 0);
        $this->datatables->group_by('dz_user.int_glcode');
        $this->datatables->join('dz_commision', 'dz_commision.fk_user = dz_user.int_glcode');
//        if ($fist != 0 || $secound != 0) {
//            $this->datatables->join('dz_quote', 'dz_quote.var_fkuser = dz_user.int_glcode');
//        }
        
        $results = $this->datatables->generate();
        $results = json_decode($results, true);
        
        $sum = 0;
        $sum1 = 0;

        for ($i = 0; $i < count($results['aaData']); $i++) { 
            $this->db->where('fk_parent_account', $results['aaData'][$i][0]);
            $count = $this->db->get('dz_user')->num_rows();

            $this->db->select('dz_commision.var_paid_amount');
            $this->db->select_sum('dz_commision.var_paid_amount');
            $this->db->where('dz_commision.fk_user', $results['aaData'][$i][0]);
            if ($fist != 0) {
                $this->db->where('dz_commision.dt_created_date >=', $fist);
               // $this->db->join('dz_quote', 'dz_commision.fk_quote = dz_quote.int_glcode');
             }
            if ($secound != 0) {
                $this->db->where('dz_commision.dt_created_date <=', $secound);
              //  $this->db->join('dz_quote', 'dz_quote.var_fkuser = dz_user.int_glcode');
            }
            if ($fist != 0 || $secound != 0) {
             //   $this->db->join('dz_quote', 'dz_commision.fk_quote = dz_quote.int_glcode');
            }
            $paidamount = $this->db->get('dz_commision')->result_array();
            
            $this->db->select('dz_commision.var_commision_amount');
            $this->db->select_sum('dz_commision.var_commision_amount');
            $this->db->where('dz_commision.chr_status', 'P');
            $this->db->where('dz_commision.fk_user', $results['aaData'][$i][0]);
            if ($fist != 0) {
                $this->db->where('dz_quote.var_checkin', $fist);
              //  $this->db->join('dz_quote', 'dz_commision.fk_quote = dz_quote.int_glcode');
             }
            if ($secound != 0) {
                $this->db->where('dz_quote.var_checkout', $secound);
             //   $this->db->join('dz_quote', 'dz_quote.var_fkuser = dz_user.int_glcode');
            }
            if ($fist != 0 || $secound != 0) {
                $this->db->join('dz_quote', 'dz_commision.fk_quote = dz_quote.int_glcode');
            }
            $comission = $this->db->get('dz_commision')->result_array();
     
                $accountid = $results['aaData'][$i][1];
                $email = $results['aaData'][$i][2];
                $results['aaData'][$i][0] = $accountid;
                $results['aaData'][$i][1] = $email;
                $results['aaData'][$i][2] = $count;
                $results['aaData'][$i][3] = '$' . number_format((float) $paidamount[0]['var_paid_amount']?$paidamount[0]['var_paid_amount']:0, 2, '.', ',');
                $results['aaData'][$i][4] = '$' . number_format((float) $comission[0]['var_commision_amount']?$comission[0]['var_commision_amount']:0, 2, '.', ',');
        }
        echo json_encode($results);
    }
    
    function chartdata()
    {
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
        $this->db->select('*');
        $this->db->where('dz_user.chr_user_type', 'AF');
        $this->db->where('dz_user.chr_status','A');
        $results = $this->db->get('dz_user')->result_array();
       $data = array();
        for($i=0;$i < count($results);$i++)
        {
             $coun1 = $this->db->get_where('dz_commision',array('fk_user'=>$results[$i]['int_glcode']))->result_array();
         
             $sum = 0;
             if(!empty($coun1))
             {
                for($j=0; $j<count($coun1); $j++){
                    $sum  = $sum + $coun1[$j]['var_paid_amount'];
                }
             }
            
            $data[] = array(
                'country' => $results[$i]['var_accountid'].','.$results[$i]['var_email'],
                'litres' => $sum,
            );
        }
        echo json_encode($data);
        exit();
    }

}

?>
