<?php

class Postpayment extends Admin_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        if($this->blobmenuarray1['Posted Payments'] != 'yes')
        {
            exit();
        }
        $data['page'] = "admin/postpayment/postlist";
        $data['breadcrumb'] = "Affiliate Posted Payment";
        $data['breadcrumb_sub'] = "Affiliate Posted Payment Sub";
        $data['breadcrumb_list'] = array(
            array('Affiliate Posted Payment', ''),
        );
        $data['postpayment'] = 'class="start active"';
        $data['var_meta_title'] = 'Affiliate Posted Payment';
        $data['var_meta_description'] = 'Affiliate Posted Payment';
        $data['var_meta_keyword'] = 'Affiliate Posted Payment';
        $data['js'] = array(
            'custom/table-managed.js',
        );
        $data['css_plugin'] = array(
            'select2/select2.css',
            'select2/select2-metronic.css',
            'data-tables/DT_bootstrap.css'
        );
        $data['js_plugin'] = array(
            'select2/select2.min.js',
            'data-tables/jquery.dataTables.js',
            'data-tables/DT_bootstrap.js',
        );
        $data['css'] = array(
        );

        $data['init'] = array(
            'TableManaged.init()',
        );
         if ($this->input->is_ajax_request()) {
            $result = $this->toval->check_cookie_ajax($data['js'], $data['js_plugin'], $data['css'], $data['css_plugin']);
            $data['js'] = $result['js'];
            $data['js_plugin'] = $result['js_plugin'];
        } else {
            $result = $this->toval->check_cookie($data['js'], $data['js_plugin'], $data['css'], $data['css_plugin']);
            $data['js'] = $result['js'];
            $data['js_plugin'] = $result['js_plugin'];
        }
        $this->db->select('dz_user.int_glcode,dz_user.var_username,dz_user.var_email,dz_affiliate_payments.var_transid,dz_affiliate_payments.var_amount,dz_affiliate_payments.var_created_date,dz_user.var_accountid');
        $this->db->from('dz_user');
        $this->db->join('dz_affiliate_payments','dz_user.int_glcode = dz_affiliate_payments.fk_user');
        $this->db->where('dz_user.chr_user_type','AF');
        $this->db->where('dz_user.chr_status','A');
        $this->db->where('dz_affiliate_payments.var_status','P');
        $data['post_payment'] = $this->db->get()->result_array();
//        print_r($data['post_payment']);exit;
        $this->load->view(ADMIN_LAYOUT, $data);
    }

}

?>
