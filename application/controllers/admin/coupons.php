<?php
class Coupons extends Admin_Controller{
    function __construct() {
        parent::__construct();
        $this->load->model('admin/coupon_model');
    }
    function index()
	{
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
        if($this->blobmenuarray1['Coupons'] != 'yes')
        {
            exit();
        }
        $data['page'] = "admin/coupons/couponslist";
        $data['breadcrumb'] = "Coupons";
        $data['breadcrumb_sub'] = "Coupons Sub";
        $data['breadcrumb_list']= array(
            array('Coupons',''),            
        );
        $data['coupons'] = 'class="start active"';
		$data['var_meta_title'] = 'Coupons';
		$data['var_meta_description'] = 'Coupons';
		$data['var_meta_keyword'] = 'Coupons';
        $data['js'] = array(
            'custom/table-managed.js',
            'custom/components-pickers.js',
            'admin/coupon.js',
            'custom/jquery.maskMoney.js',
        );
        $data['js_plugin'] = array(
			'select2/select2.min.js',
			'data-tables/jquery.dataTables.js',
			'data-tables/DT_bootstrap.js', 
			'bootstrap-datepicker/js/bootstrap-datepicker.js', 
			'bootstrap-switch/js/bootstrap-switch.min.js',  
        );
        $data['css_plugin'] = array(
            'select2/select2.css',     
            'select2/select2-metronic.css',
            'data-tables/DT_bootstrap.css',
            'bootstrap-datepicker/css/datepicker.css',    
            'bootstrap-switch/css/bootstrap-switch.min.css',
        );
        
        $data['css'] = array(
        );        
        $data['init'] = array(
            'TableManaged.init()',
            'ComponentsPickers.init()', 
            'Coupon.init()',
        );
        
        $data['coupon_status'] = $this->db->select('chr_status')->get('dz_singup_coupons')->row_array();
        $this->db->where('dz_user.chr_user_type','U');
        $this->db->where('dz_user.is_subscribed',1);
        $this->db->from('dz_user');
        $userdata = $this->db->get()->result_array();
        for($i=0;$i < count($userdata);$i++)
		{
            $this->db->where('fk_user',$userdata[$i]['int_glcode']);
            $this->db->from('dz_coupons');
            $coupandata = $this->db->get()->result_array();
            if(count($coupandata) > 0)
            {
                for($j = 0;$j < count($coupandata);$j++)
                {
                    $coupanarray[]= array(
                        'var_accountid'=>$userdata[$i]['var_accountid'],
                        'var_username'=>$userdata[$i]['var_username'],
                        'var_email'=>$userdata[$i]['var_email'],
                        'int_glcode'=>$coupandata[$j]['int_glcode'],
                        'var_coupan_type'=>$coupandata[$j]['var_coupan_type'],
                        'var_couponcode'=>$coupandata[$j]['var_couponcode'],
                        'min_amount'=>'$'.$coupandata[$j]['min_amount'],
                        'var_couponvalue'=>$coupandata[$j]['var_couponvalue'],
                        'var_status'=>$coupandata[$j]['var_status'],
                        'userid'=>$userdata[$i]['int_glcode'],
                    );
                }
            }
            else
			{
                $coupanarray[]= array(
					'var_accountid'=>$userdata[$i]['var_accountid'],
					'var_username'=>$userdata[$i]['var_username'],
					'var_email'=>$userdata[$i]['var_email'],
					'int_glcode'=>'0',
					'var_coupan_type'=>'0',
					'var_couponcode'=>'-',
					'var_couponvalue'=>'-',
					'min_amount'=>'-',
					'var_status'=>'-',
					'userid'=>$userdata[$i]['int_glcode'],
				);
            }    
        }
		$data['userlist'] = $coupanarray;
		if ($this->input->is_ajax_request()) 
		{
            $result = $this->toval->check_cookie_ajax($data['js'], $data['js_plugin'], $data['css'], $data['css_plugin']);
            $data['js'] = $data['js'];
            $data['js_plugin'] = $data['js_plugin'];
        } 
		else 
		{
            $result = $this->toval->check_cookie($data['js'], $data['js_plugin'], $data['css'], $data['css_plugin']);
            $data['js'] = $result['js'];
            $data['js_plugin'] = $result['js_plugin'];
        }
		$this->load->view(ADMIN_LAYOUT, $data);    
    }
    function coupon_generate()
	{
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
       if($this->input->post('status')!= "")
	   {
			$result = $this->coupon_model->coupon_generate($this->input->post());
			if($result)
			{
				echo 'success';
			}
			else
			{
				echo 'error';
			}
       }
    }
    
    function update_status()
	{
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
        if($this->input->post()){
            $res = $this->coupon_model->status_update($this->input->post());
        }
    }
    
    function singup_status()
	{
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
        if($this->input->post()){
            $result = $this->coupon_model->status_singup($this->input->post());
            if($result){
                echo "success"; 
            }else{
                echo "error";
            }
        }
    } 
    function genaratedata1()
    {
		if(!$this->session->userdata('valid_adminuser'))
		{
			redirect('admin/login');
		}
        if($this->input->post())
        {
            $result = $this->coupon_model->genaratedata();
            if($result){
                 echo "success"; 
            }else{
                echo "error";
            }
            
        }
    }
}
?>
