<?php
class Sentquotes extends Admin_Controller{
     function __construct() {
        parent::__construct();
        $this->load->model('admin/quote');
        $this->load->library('datatables');
    }
    function index(){
        if($this->blobmenuarray1['Sent Quotes'] != 'yes')
        {
            exit();
        }
        $data['page'] = "admin/sentquotes/quoteslist";
        $data['breadcrumb'] = "Sent Quotes";
        $data['breadcrumb_sub'] = "Sent Quotes Sub";
        $data['breadcrumb_list']= array(
            array('Sent Quotes',''),            
        );
        $data['sentquotes'] = 'class="start active"';
	$data['var_meta_title'] = 'Sent Quotes';
	$data['var_meta_description'] = 'Sent Quotes';
	$data['var_meta_keyword'] = 'Sent Quotes';
        $data['js'] = array(
            'admin/sendquote.js',
           // 'custom/table-managed.js',
        );
        $data['css_plugin'] = array(
            'data-tables/DT_bootstrap.css',
        );
        $data['js_plugin'] = array(
          'data-tables/jquery.dataTables.js',
          'data-tables/DT_bootstrap.js',
        );
        $data['css'] = array(
        );        
        $data['init'] = array(
           'Sendquote.init_table()',
//           'TableManaged.init()',
        );           
        
    $this->load->view(ADMIN_LAYOUT, $data);    
    }
    
    function sendquote_datatable(){
        $this->datatables
                ->select
                (
                'dz_rplyquote.int_glcode,
                 dz_rplyquote.var_uniq_quoteid,
                 dz_rplyquote.var_createddate,
                 dz_quote.var_fkuser,
                 dz_rplyquote.fk_replayuser_id,
                 dz_quote.var_room,
                 dz_quote.int_glcode as quoteid'
                )
                ->from('dz_rplyquote')
                ->join('dz_quote','dz_rplyquote.int_fkquote = dz_quote.int_glcode');
//                ->order_by('dz_rplyquote.var_createddate', 'desc')
        
            $results = $this->datatables->generate();
            $results = json_decode($results,true);
            for($i=0; $i< count($results['aaData']); $i++){
                
                $replayid = $results['aaData'][$i][0];
                $uniqueid = $results['aaData'][$i][1];
                $datetime = $results['aaData'][$i][2];
                $quoteuserid = $results['aaData'][$i][3];
                $Replayuserid = $results['aaData'][$i][4];
                
                $option = "<a href='#myModal_autocomplete' data-toggle='modal' data-id='".$replayid."' id='sentquote' class='sentquote label label-sm label-success'>View</a>";
                 
                $this->db->where('int_glcode', $quoteuserid);
                $quoteuser = $this->db->get('dz_user')->result_array();
                
                $this->db->where('int_glcode',$Replayuserid);
                $quotesenduser = $this->db->get('dz_user')->result_array();
                
                $results['aaData'][$i][0] = $datetime;
                $results['aaData'][$i][1] = $quoteuser[0]['var_username'];
                $results['aaData'][$i][2] = $quoteuser[0]['var_email'];
                $results['aaData'][$i][3] = $quoteuser[0]['var_accountid'];
                $results['aaData'][$i][4] = $uniqueid;
                $results['aaData'][$i][5] = $quotesenduser[0]['var_username'];
                $results['aaData'][$i][6] = $option;
            }
//            print_r($results); exit();
            echo json_encode($results);
    }
    
    function getrequestquote() { 
        $id = $this->input->post('id');
        
        $data['quote'] = $this->quote->sentquote_getdata($id);
        $this->db->where('int_glcode',$id);
        $data['viewquotereply'] = $this->db->get('dz_rplyquote')->result_array();
        $this->load->view('admin/sentquotes/popupquoteslist', $data);  
    }
}
?>

