<?php

class Page extends CI_Controller {

    //put your code here
    function __construct() {

        parent::__construct();
        $this->load->model('pages');
        $this->load->helper('captcha');
    }
	
    function index($ajax) {
        $data['page'] = 'front/home';
        //$this->load->view('front/home');
        $this->load->view(LAYOUT, $data);
    }

    function home($ajax) {
        $data['page'] = 'front/home';
        $data['ajax'] = $ajax;
         $data['detail'] = $this->pages->get_aboutus_detail();
        $this->load->view(LAYOUT, $data);
    }

    function portfolio($ajax) {
        $data['page'] = 'front/portfolio';
        $data['portfolio'] = $this->pages->get_portfolio();
        $data['ajax'] = $ajax;
        $this->load->view(LAYOUT, $data);
    }

    function clients($ajax) {
        $data['page'] = 'front/clients';
        $data['ajax'] = $ajax;
        $data['cdetail'] = $this->pages->get_client_detail();
        $this->load->view(LAYOUT, $data);
    }

    function service($ajax) {
        $data['page'] = 'front/service';
        $data['service_details'] = $this->pages->get_services();
        $data['ajax'] = $ajax;
        $this->load->view(LAYOUT, $data);
    }

    function webdesign($ajax) {
        $data['page'] = 'front/webdesign';
        $data['webdesign_details'] = $this->pages->get_webdesign_detail();
        $data['ajax'] = $ajax;
        $this->load->view(LAYOUT, $data);
    }

    function api_development($ajax) {
        $data['page'] = 'front/api_development';
        $data['api_details'] = $this->pages->get_api_detail();
        $data['ajax'] = $ajax;
        $this->load->view(LAYOUT, $data);
    }

    function cms_development($ajax) {
        $data['page'] = 'front/cms_development';
        $data['cms_detail'] = $this->pages->get_cms_detail();
        $data['ajax'] = $ajax;
        $this->load->view(LAYOUT, $data);
    }

    function social_media_integration($ajax) {
        $data['page'] = 'front/social_media_integration';
        $data['social_detail'] = $this->pages->get_social_detail();
        $data['ajax'] = $ajax;
        $this->load->view(LAYOUT, $data);
    }

    function mobile_development($ajax) {
        $data['page'] = 'front/mobile_development';
        $data['mobile_details'] = $this->pages->get_mobile_detail();
        $data['ajax'] = $ajax;
        $this->load->view(LAYOUT, $data);
    }

    function ecommerce_development($ajax) {
        $data['page'] = 'front/ecommerce_development';
        $data['ecommerce_details'] = $this->pages->get_ecommerce_detail();
        $data['ajax'] = $ajax;
        $this->load->view(LAYOUT, $data);
    }

    function contact($ajax) {
        $data['page'] = 'front/contact';
        $data['contactdetail'] = $this->pages->get_contact_detail();
        $data['ajax'] = $ajax;
        $data['js'] = array(
            'front/js/contact.js',
        );
        $data['init'] = array(
            'Contact.init()',
        );

        $vals = array(
            'img_path' => './public/captcha/',
            'img_url' => base_url() . '/public/captcha/'
        );

        $cap = create_captcha($vals);

        $data['aa'] = array(
            'captcha_time' => $cap['time'],
            'ip_address' => $this->input->ip_address(),
            'word' => $cap['word']
        );
        //print_r($data['aa']); exit;
        $query = $this->db->insert_string('captcha', $data['aa']);
        $this->db->query($query);

        $data['capimg'] = $cap;

        if ($this->input->post('inq_name')) {
            //print_r($this->input->post('codetext')); exit;
            if (($this->input->post('txtword')) == ($this->input->post('codetext'))) {
                $this->pages->add($this->input->post());
            } else {
                //echo $this->input->post('txtword');
                //echo $this->input->post('codetext'); exit;
                echo "error";
                exit;
            }
        }
        //echo $this->input->post('codetext'); exit;
        $this->load->view(LAYOUT, $data);
    }

    function about($ajax) {
        $data['page'] = 'front/about';
        $data['ajax'] = $ajax;
        $data['detail'] = $this->pages->get_aboutus_detail();
        $this->load->view(LAYOUT, $data);
    }

    function portfoliodetail($ajax) {
        $data['ajax'] = $ajax;
        $data['detail'] = $this->pages->get_porfolio_detail();
    }

    function getportfoliyo($data) {
        $data_json = file_get_contents('php://input');
        $data_get = json_decode($data_json, true);
        $return = $this->pages->ajaxget_portfolio($data_get);
        echo $return;
    }

    function getloadmoreportfoliyo($data) {
        $data_json = file_get_contents('php://input');
        $data_get = json_decode($data_json, true);
        $return = $this->pages->getloadmore_portfolio($data_get);
        echo $return;
    }

    function getloadmoreclient($data) {
        $data_json = file_get_contents('php://input');
        $data_get = json_decode($data_json, true);
        $return = $this->pages->getloadmore_client($data_get);
        echo $return;
    }
      function get_capcha($ajax)
    {
        $vals = array(
                'img_path' => './public/captcha/',
                'img_url' => base_url().'/public/captcha/'
            );

            $cap = create_captcha($vals);
            $data['aa']= array(
                'captcha_time' => $cap['time'],
                'ip_address' => $this->input->ip_address(),
                'word' => $cap['word']
            );
           
           $query = $this->db->insert_string('captcha', $data['aa']);
           $this->db->query($query);
           echo $cap['image']; exit;
           
    }
}

?>
