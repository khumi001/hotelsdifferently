<?php

class Travel_professional extends Front_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('session');
    }

    function signup(){

				$data['company_types'] 	= $this->db->get_where('company_types')->result_array();
				$data['countries'] 		= $this->db->order_by("name", "asc")->get_where('countries')->result_array();
				$data['hear_abouts']		= ['Social Media'=>'Social Media','Another Travel Agent'=>'Another Travel Agent','Media (TV/Radio/Magazine)'=>'Media (TV/Radio/Magazine)','Email'=>'Email','Other'=>'Other'];
		
        $data['page'] = "front/travel_professional/signup";

        $this->load->view(FRONT_LAYOUT, $data);
    }
	function signup_post(){
		$requiredArr = ['company_name','phone_country','phone_number','city','hear_about','contact_f_name','contact_l_name','email','password','c_pasword'];
		$post = $this->input->post();
		foreach($post as $postKey=>$postValue){
			if(in_array($postKey,$requiredArr)){
				if(trim($postValue) == ''){
					exit('Please fill the required fields.');
				}
			}
		}
		if (!filter_var($this->input->post('email'), FILTER_VALIDATE_EMAIL)) {
            exit('Your email is invalid.');
        }
		$data 			= [];
		$data['company_name'] 	= $post['company_name'];
		$data['company_type'] 	= $post['company_type'];
		$data['website'] 		= $post['website'];
		$data['phone_country_code'] 	= $post['phone_country'];
		$data['phone_number'] 	= $post['phone_number']; 
		$data['phone_ex'] 		= $post['phone_ex'];
		$data['country'] 		= $post['country']; 
		$data['state'] 			= $post['state']; 
		$data['city'] 			= $post['city'];
		$data['street'] 		= $post['street']; 
		$data['company_owner'] 	= $post['company_owner']; 
		$data['iata'] 			= $post['iata'];
		$data['clia'] 			= $post['clia'];
		$data['arc'] 			= $post['arc'];
		$data['hear_about'] 	= $post['hear_about'];
		$data['other_hear'] 	= $post['other_hear'];
		$data['contact_f_name'] = $post['contact_f_name'];
		$data['contact_l_name'] = $post['contact_l_name'];
		$data['email'] 			= trim($post['email']);
		$data['password'] 		= base64_encode($post['password']);
		$data['contact_country_code']= $post['contact_country'];
		$data['contact_phone'] 	= $post['contact_phone'];
		
		$data_user	= [];
		$data_user['var_email'] 	= trim($post['email']);
		$data_user['var_password'] 	= base64_encode($post['password']);
		$data_user['var_fname'] 	= $post['contact_f_name'];
		$data_user['var_lname'] 	= $post['contact_l_name'];
		$data_user['var_phone'] 	= $post['contact_country'].$post['contact_phone'];
		$data_user['var_country'] 	= 'null';
		$data_user['chr_user_type'] = 'TF';
		$data_user['dt_created_date'] = date('Y-m-d H:i:s');
		$data_user['var_signupip'] 	= $this->getIP();
		$data_user['var_accountid'] = $this->generateAccountId();
		$data_user['profile_pic'] = 'null';
		$data_user['chr_status'] = 'AP';
		
		$alreadyExist = $this->db->select('int_glcode')->get_where('dz_user',['var_email'=>trim($post['email'])])->result_array();	
		if(empty($alreadyExist)){
			
			$this->db->insert('dz_user', $data_user);
			$lastInsertedId = $this->db->insert_id();
			$data['user_id'] = $lastInsertedId;
			$this->db->insert('travel_professional_detail', $data);
			$this->session->set_userdata('signup_data',$data);
			$this->send_mail(trim($post['email']),'Travel Professional signup',$data);
			echo 'success';
			exit;
		}else{
			echo 'Sorry! You are already signup with this email.';
			exit;
		}	
	}
	function checkemail(){
        if (!filter_var($this->input->post('email'), FILTER_VALIDATE_EMAIL)) {
            echo "error";
            exit();
        }
        $this->db->where('var_email', $this->input->post('email'));
        $query = $this->db->get('dz_user')->num_rows();
        if ($query == 0) {
            echo "success";
            exit;
        } else {
            echo "error";
            exit;
        }
    }
	private function getIP() {
		$tmp = getenv("HTTP_CLIENT_IP");
		if ( $tmp && !strcasecmp( $tmp, "unknown"))
			return $tmp;
	
		$tmp = getenv("HTTP_X_FORWARDED_FOR");
		if( $tmp && !strcasecmp( $tmp, "unknown"))
			return $tmp;
		$tmp = getenv("REMOTE_ADDR");
		if($tmp && !strcasecmp($tmp, "unknown"))
			return $tmp;
	
		return("unknown");
	}

	private function send_mail($to, $subject, $data) {
			$from_email = NOREPLY; 
			$to_email = $to; 

			//Load email library 
			$this->load->library('email'); 
			$this->email->set_mailtype("html");
			$this->email->from($from_email, 'Wholesale Hotels Group'); 
			$this->email->to($to_email);
			$this->email->subject($subject); 

			$message = $this->load->view('front/travel_professional/signup_email', ['data'=>$data], TRUE);

			$this->email->message($message);
			$this->email->send();
   }

	function thanks(){

			$data['data'] = $this->session->userdata('signup_data');
			//$this->send_mail('adnankhanbs@gmail.com', 'testing', $data['data'] );
			$data['page'] = "front/travel_professional/thankyou";
	    $this->load->view(FRONT_LAYOUT, $data);
	}

	function states($country_id){
		$states	= $this->db->get_where('states',['country_id'=>$country_id])->result_array();	
		echo json_encode($states);exit;
	}
	function congratulation(){
		$allSession  = $this->session->all_userdata();
		
		if(isset($allSession['travel_professional_detail']) && (int)$allSession['travel_professional_detail']['welcome_show'] == 0){
			$data['page'] = "front/travel_professional/congrats";
	    	$this->load->view(FRONT_LAYOUT, $data);

			$allSession['travel_professional_detail']['welcome_show'] = 1;
			$this->session->set_userdata($allSession);
			
			$this->db->where('id', $allSession['travel_professional_detail']['id']);
            $this->db->update('travel_professional_detail', ['welcome_show'=>'1']);
			
		}else{
			redirect("/");
		}
	}
	function account_info(){
        TP_Check();
		$this->page_name = 'account_info';
		$this->parent_menu = 'myaccount';
		$allSession  = $this->session->all_userdata();
//echo '<pre>';print_r($allSession);exit;
		
		$this->db->select('dz_user.*, travel_professional_detail.*, countries.name as country_name, company_types.name as company_type_name');
		$this->db->from('dz_user');
		$this->db->join('travel_professional_detail', 'travel_professional_detail.user_id= dz_user.int_glcode');
		$this->db->join('countries', 'travel_professional_detail.country = countries.id', 'left');
		$this->db->join('company_types', 'travel_professional_detail.company_type = company_types.id', 'left');
		$this->db->where('dz_user.int_glcode', $allSession['travel_professional_detail']['user_id']);
		$user_info =  $this->db->get();
        $data['company_types'] 	= $this->db->get_where('company_types')->result_array();
		$data['user_info'] = (array)$user_info->result()[0];
		$data['page_name'] = 'account_info';
		$data['page'] = "front/travel_professional/account_info";
	    $this->load->view(FRONT_LAYOUT, $data);
	}
	function profile_update_post(){
		$allSession  = $this->session->all_userdata();
		
		$requiredArr = ['phone_country','phone_number','city','contact_f_name','contact_l_name'];
		$post = $this->input->post();
		foreach($post as $postKey=>$postValue){
			if(in_array($postKey,$requiredArr)){
				if(trim($postValue) == ''){
					exit('Please fill the required fields.');
				}
			}
		}
		
		$data 			= [];
		$data['website'] 		= $post['website'];
		$data['phone_country_code'] 	= $post['phone_country'];
		$data['phone_number'] 	= $post['phone_number']; 
		$data['phone_ex'] 		= $post['phone_ex'];
		$data['state'] 			= $post['state']; 
		$data['city'] 			= $post['city'];
		$data['street'] 		= $post['street']; 
		$data['company_owner'] 	= $post['company_owner']; 
		$data['iata'] 			= $post['iata'];
		$data['clia'] 			= $post['clia'];
		$data['arc'] 			= $post['arc'];
		$data['contact_f_name'] = $post['contact_f_name'];
		$data['contact_l_name'] = $post['contact_l_name'];
		$data['contact_country_code']= $post['contact_country'];
		$data['contact_phone'] 	= $post['contact_phone'];
		
		$this->db->where('user_id', $allSession['travel_professional_detail']['user_id']);
		$this->db->update('travel_professional_detail', $data);
		
		$data_user = [];
		$data_user['var_fname']	= $post['contact_f_name'];
		$data_user['var_lname']	= $post['contact_l_name'];
		$data_user['var_phone']	= $post['contact_country'].$post['contact_phone'];
		
		$this->db->where('int_glcode', $allSession['travel_professional_detail']['user_id']);
		$this->db->update('dz_user', $data_user);
		
		/*****Update Session Start****************/
		$travel_professional_detail = $this->db->get_where('travel_professional_detail',['user_id'=>trim($allSession['travel_professional_detail']['user_id'])])->result_array();
		$arr['travel_professional_detail'] = $travel_professional_detail[0];
		$this->session->set_userdata($arr);
		/*****Update Session Start****************/
		echo 'success';
		exit;
	}
	
	function password_update(){
        TP_Check();
		$this->page_name = 'Change Password';
		$this->parent_menu = 'myaccount';
		$data['js'] = array(
			'front/travel_professional/change_password.js'
		);
		$data['js_plugin'] = array(
			'jquery-validation/dist/jquery.validate.min.js',
			'jquery-validation/dist/additional-methods.min.js',
			'bootstrap-wizard/jquery.bootstrap.wizard.min.js',
			'select2/select2.min.js',
			'bootstrap-datepicker/js/bootstrap-datepicker.js',
			'bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',
			'bootstrap-maxlength/bootstrap-maxlength.min.js'
		);
		$data['css_plugin'] = array(
			'select2/select2-metronic.css',
			'select2/select2.css',
		);
		$data['init'] = array('Change_password.init()');
		$data['page'] = "front/travel_professional/password_update";


		if ($this->input->post()) {

			$oldpass = $this->input->post('old_pass');
			$id = $this->session->userdata['valid_user']['id'];
			$this->db->where('int_glcode', $id);
			$this->db->where('var_password', base64_encode($oldpass));
			$query = $this->db->get('dz_user');

			if ($query->num_rows() == 1) {
							
				$content = array(
				  'var_password' => base64_encode($this->input->post('new_pass')),
				);
				$this->db->where('int_glcode', $id);
				$row = $this->db->update('dz_user', $content);

				if ($row){
					$this->db->where('int_glcode', $id);
					$query2 = $this->db->get('dz_user');
					$name = $query2->result_array();
					$this->load->library('email');
					$this->email->set_mailtype("html");
					$this->email->from(NOREPLY, SITE_NAME);
					$this->email->to($name[0]['var_email']);
					$this->email->subject('Change Password');
				
					$mail_body = '<p style="color:#fff;margin-bottom:10px;">Dear  ' . $name[0]['var_fname'] . '</p>';
					$mail_body .='<p style="color:#fff;margin-bottom:10px;">Thank you for using <b>'.SITE_NAME.'<sup>sm</sup></b>. We are glad to confirm that your password is now successfully changed! Your new password is <b>'.$this->input->post('new_pass').'</b></p>';
					$mail_body .='<p style="color:#fff;margin-bottom:10px;">Again, thank you for choosing us and please always give us a try before booking your next trip!</p>';
					$mail_body .= '<p style="color:#fff;margin-bottom:10px;">Sincerely,</br>';
					$mail_body .= '<p style="color:#fff;margin-bottom:10px;"><b>'.SITE_NAME.'<sup>sm</sup></b><br>
					<small style="font-style: italic;"> Where better deals are made for YOU!</small><b><sup>sm</sup></b>
					</p>';
					$data['mail_body'] = $mail_body;
					$message = $this->load->view('admin/account/genral_emailtemplate', $data, TRUE);
					$this->email->message($message);
					$res = $this->email->send();
					
					if ($res){
						echo "success";
					}else{
						echo "email error";
					}
				}else{
					echo "error";
				}
			}else{
				echo "error";
			}
			exit;
		}
		$this->load->view(FRONT_LAYOUT, $data);
	}
	
	function agent_markup(){
        TP_Check();
		$this->page_name = 'agent markup';
		$this->parent_menu = 'myaccount';
		
		$data['page'] = "front/travel_professional/agent_markup";
		$data['js'] = array(
			'custom/jquery.maskMoney.js'
		);

		//http://localhost/hotelsdifferently/public/assets/javascripts/custom/jquery.maskMoney.js

		$this->load->view(FRONT_LAYOUT, $data);
	}

	function agent_markup_post(){
		$allSession  = $this->session->all_userdata();
		$post = $this->input->post();
		
		$data = array('markup_type'=>$post['markup_type']);
		
		if(trim($post['markup_type']) == '$'){
			$data['markup_value'] = trim($post['dollar_value']) != ''?$post['dollar_value']:'0';
		}
		if(trim($post['markup_type']) == '%'){
			$data['markup_value'] = trim($post['percentage_value']) != ''?$post['percentage_value']:'0';
		}
		$this->db->where('user_id', $allSession['travel_professional_detail']['user_id']);
		$this->db->update('travel_professional_detail', $data);
		/*****Update Session Start****************/
		$travel_professional_detail = $this->db->get_where('travel_professional_detail',['user_id'=>trim($allSession['travel_professional_detail']['user_id'])])->result_array();
		$arr['travel_professional_detail'] = $travel_professional_detail[0];
		$this->session->set_userdata($arr);
		/*****Update Session Start****************/
		exit('success');
	}
	function incentives(){
        TP_Check();
        $data['page'] = "front/travel_professional/incentives";
        $this->load->view(FRONT_LAYOUT, $data);
    }
	function generateAccountId($numno1 = 3, $numno2 = 3) {
        $listno = '0123456';
        return str_shuffle(
                substr(str_shuffle($listno), 0, $numno1) .
                substr(str_shuffle($listno), 0, $numno2)
        );
    }
	function faqs(){
        TP_Check();
		$this->page_name = 'Faq';
		$this->parent_menu = '';
        $data['page'] = "front/travel_professional/travel_pro_faq";
        $this->load->view(FRONT_LAYOUT, $data);
    }
}
?>