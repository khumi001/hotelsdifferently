<?php

class activities extends Front_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('tourico_api');
        $this->load->model('tourico_api/destination_model');
        $this->load->model('tourico_api/tourico_hotel_price_multiplier');
        $this->load->library('session');
        $this->load->model('front/quote_model');
        $this->load->model('tourico_activity');
        $this->load->helper('stripe_lib/init');
    }
	
	function listing() {

    	ini_set('memory_limit','256M');
    	
        $_SESSION['activity_search_url'] = base_url().ltrim($_SERVER['REQUEST_URI'], '/');

        $activityLocationName = $this->input->get("activityLocationName", TRUE);
        $activity_date_from = $this->input->get("activity_date_from", TRUE);
        $activity_date_to = $this->input->get("activity_date_to", TRUE);
        $activityLocationId = $this->input->get("activityLocationId", TRUE);
        $activity_name = $this->input->get("activity_name", TRUE);

        $data = array(
        	'activityLocationName'=>$activityLocationName,
        	'activityLocationId'=>$activityLocationId,
        	'activity_date_from'=>$activity_date_from,
        	'activity_date_to'=>$activity_date_to,
        	'activity_name'=>$activity_name
        );

        //$destinationInfo = $this->destination_model->GetDestinationCodeByDestinationName($destinationId);
        //$destinationCode = $destinationInfo['tourico_destination_id'];

        $fromDateObj = DateTime::createFromFormat('m/d/Y', $activity_date_from);
        $fromDate = $fromDateObj->format('Y-m-d');

        $toDateObj = DateTime::createFromFormat('m/d/Y', $activity_date_to);
        $toDate = $toDateObj->format('Y-m-d');

        $activityResponseObj = new ActivityAPIResponseProcess();

        $allActivities = $activityResponseObj->ProcessGetActivityDetailsByDestinationIdResponse($activityLocationId, $fromDate, $toDate, $activity_name);

        $data['activities'] = $allActivities;

        $data['cart_list'] = isset($_SESSION['cart']) ? $_SESSION['cart'] : array();

        $data['page'] = "front/activities/listing";
        $this->page_name = 'Home';
        $data['js'] = array(
            'login.js',
            'moment.min.js',
            'front/home.js',
            'affiliate/req_quote.js',
            'custom/components-pickers.js',
            'custom/components-dropdowns.js',
            'front/jquery.bxslider.js'
        );
        $data['js_plugin'] = array(
            'bootstrap-fileinput/bootstrap-fileinput.js',
            'checkincheckout/bootstrap-datepicker.js',
            'bootstrap-select/bootstrap-select.min.js',
            'select2/select2.min.js',
            'jquery.bxslider.min.js',
            'bsStarRating/star-rating.min.js'
        );
        $data['css_plugin'] = array(
            'bootstrap-fileinput/bootstrap-fileinput.css',
            'bootstrap-datepicker/css/datepicker.css',
            'bootstrap-select/bootstrap-select.min.css',
            'select2/select2.css',
            'select2/select2-metronic.css',
            'jquery-ui/jquery-ui.css',
            'jquery.bxslider.css',
            'bsStarRating/star-rating.min.css'
        );
        $data['css'] = array(
        );


        $this->load->view('front/search_layout', $data);
    }

    function view() {
        $activity_id = $this->input->post("activity_id", TRUE);
        $activity_details_obj = new ActivityAPIResponseProcess();

        $activities_by_activity_id = $activity_details_obj->ProcessGetActivityDetailsResponse($activity_id);

        echo json_encode(array('activities_by_id' => $activities_by_activity_id));
    }
	
    function SynchronizeWithDestinationApi() {
        $this->destination_model->SynchronizeWithApi();
    }

    function GetDestinationListBasedOnSuggestion() 
	{
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
        $chingParameter = $this->input->get('term', TRUE);
        $locationList = $this->destination_model->GetSuggestionListByText($chingParameter);
        $locationListFromTL = $this->destination_model->GetSuggestionListByTextFromTL($chingParameter);

        $response = array();
        $dataBase = array();
        // add all tourico data
        foreach ($locationList as $aLocation) {
            $response[] = array(
                'id' => $aLocation['tourico_destination_id'],
                'label' => $aLocation['destination_name'],
                'value' => $aLocation['destination_name'],
                'text_order' => strpos(strtolower($aLocation['destination_name']), strtolower($chingParameter))
            );
        }
        //echo '<pre>';print_r($locationListFromTL);exit;
        // logic for TL
        foreach ($locationListFromTL as $tLocation) {
            if (empty($tLocation['stateName']))
                $TLCityState = $tLocation['city_name'];
            else
                $TLCityState = $tLocation['city_name'] . ', ' . $tLocation['stateName'];

            if (empty($tLocation['touricoCountryName']))
                $TLCityState = $TLCityState . ', ' . $tLocation['country_name'];
            else
                $TLCityState = $TLCityState.', '.$tLocation['touricoCountryName'];

            if(count($locationList)>0) {
                foreach ($locationList as $aLocation) {
                    // if travleanda city and state are present in tourico search then use tourico data as it is beign handled
                    // in hotels search, otherwise add up the travelanda data in search results too.
                    // check is TL location mateched with tourico location and that string should not already exist in tourico locations array
                    //echo $aLocation['destination_name'] . ' - '.$TLCityState."\n";
                    if (strpos($aLocation['destination_name'], $TLCityState) == false /*&& !in_array($aLocation['destination_name'], $touricoDataBase)*/) {
                        //$touricoDataBase[] = $aLocation['destination_name'];
                        $response[] = array(
                            'id' => $tLocation['id'],
                            'label' => $TLCityState,
                            'value' => $TLCityState,
                            'text_order' => strpos(strtolower($TLCityState), strtolower($chingParameter))
                        );
                    }
                }
            }else{
                $response[] = array(
                    'id' => $tLocation['id'],
                    'label' => $TLCityState,
                    'value' => $TLCityState,
                    'text_order' => strpos(strtolower($TLCityState), strtolower($chingParameter))
                );
            }
        }
        //echo '<pre>';print_r($response);
        //$response = array_unique($response, SORT_REGULAR);
        //$response = array_values($response);
        //$response = array_map("unserialize", array_unique(array_map("serialize", $response)));
        //echo '<pre>';print_r($response);exit;
        $fData = array();
        foreach ($response as $val) {
            if(!in_array($val['value'],$dataBase)){
                $dataBase[] = $val['value'];
                $fData[] = $val;
            }
        }

        $fData = $this->sortByNestedArray($fData);
        echo json_encode($fData);
		exit;
    }
	
	function sortByNestedArray($arr)
	{
		for($i = 0 ; $i < count($arr) ; $i++)
		{
			for ($j=0; $j<count($arr)-1-$i; $j++) 
			{
				if ($arr[$j+1]['text_order'] < $arr[$j]['text_order']) 
				{
					$this->swap($arr, $j, $j+1);
				}
			}
		}
		return $arr;
	}
	
	function swap(&$arr, $a, $b) {
		$tmp = $arr[$a];
		$arr[$a] = $arr[$b];
		$arr[$b] = $tmp;
	}

    function add_item_to_cart() {

        //error_reporting(E_ALL);
        //ini_set('display_errors', 1);

        $optionId = $this->input->post("optionId", TRUE);
        $activityId = $this->input->post("activityId", TRUE);
        $activityName = $this->input->post("activityName", TRUE);
        $thumbUrl = $this->input->post("thumbUrl", TRUE);
        $optionTitle = $this->input->post("optionTitle", TRUE);

        $activity_date = $this->input->post("activity_date", TRUE);

        $no_of_adults = $this->input->post("no_of_adults", TRUE);
        $no_of_childs = $this->input->post("no_of_childs", TRUE);
        $no_of_units = $this->input->post("no_of_units", TRUE);

        $adult_price = $this->input->post("adult_price", TRUE);
        $child_price = $this->input->post("child_price", TRUE);
        $unit_price = $this->input->post("unit_price", TRUE);

        $currency = $this->input->post("currency", TRUE);

        $location = $this->input->post("location", TRUE);

        $activity_details_obj = new ActivityAPIResponseProcess();
        $activities_by_activity_id = $activity_details_obj->ProcessGetActivityDetailsResponse($activityId);

        //echo '<pre>';print_r($activities_by_activity_id);exit;
        $total_price = (float)( ($no_of_adults * $adult_price) + ($no_of_childs * $child_price) + ($no_of_units * $unit_price) );

        $cartItem = array(
            'activityDetails'=>$activities_by_activity_id,
            'location'=>$location,
            'optionId'=>$optionId,
            'activityId'=>$activityId,
            'activityName'=>$activityName,
            'thumbUrl'=>$thumbUrl,
            'optionTitle'=>$optionTitle,
            'activity_date'=>$activity_date,
            'no_of_adults'=>$no_of_adults,
            'no_of_childs'=>$no_of_childs,
            'no_of_units'=>$no_of_units,
            'adult_price'=>$adult_price,
            'unit_price'=>$unit_price,
            'child_price'=>$child_price,
            'currency'=>$currency,
            'total_price'=>$total_price
        );

        $activityReservationAPIObj = new ActivityReservationAPI();
        $preBook = $activityReservationAPIObj->ActivityPreBook($cartItem);
        //echo '<pre>';print_r($preBook);exit;

        if( !empty($preBook) ){
        
            $cartItem['cancellation_policy'] = $preBook['CancellationPolicy'];
            $cartItem['cancellation_policy'] = $this->cancellation_policy_text($preBook['CancellationPolicy']);

            $cartItem['original_prices'] = $preBook['OriginalPrices'];//prices without delta price
            $cartItem['passengers'] = $preBook['Passengers'];
            $cartItem['activity_additions'] = isset($preBook['ActivityAdditions']) ? $preBook['ActivityAdditions'] : array();
            $cartItem['passenger_additions'] = isset($preBook['PassengerAdditions']) ? $preBook['PassengerAdditions'] : array();

            $cartItem['total_price'] = $preBook['ActivityInfo']['total_price'];//updating price in case there is change in price during process
            $cartItem['currency'] = $preBook['ActivityInfo']['currency'];
            
            if( !empty($preBook['ActivityInfo']['adult_price']) ){
                $adult_price = $preBook['ActivityInfo']['adult_price'];//updating price in case there is change in price during process
                $cartItem['adult_price'] = $adult_price;    
            }
            
            if( !empty($preBook['ActivityInfo']['child_price']) ){
                $child_price = $preBook['ActivityInfo']['child_price'];//updating price in case there is change in price during process
                $cartItem['child_price'] = $child_price;    
            }

            if( isset($_SESSION['cart'][$optionId][$activity_date]) ){
                $_SESSION['cart'][$optionId][$activity_date]['no_of_adults'] = (int)$_SESSION['cart'][$optionId][$activity_date]['no_of_adults'] + (int)$no_of_adults;
                $_SESSION['cart'][$optionId][$activity_date]['no_of_childs'] = (int)$_SESSION['cart'][$optionId][$activity_date]['no_of_childs'] + (int)$no_of_childs;
                $_SESSION['cart'][$optionId][$activity_date]['no_of_units'] = (int)$_SESSION['cart'][$optionId][$activity_date]['no_of_units'] + (int)$no_of_units;
                $_SESSION['cart'][$optionId][$activity_date]['total_price']  =  (int)$_SESSION['cart'][$optionId][$activity_date]['total_price'] + (float)(($no_of_adults * $adult_price) + ($no_of_childs * $child_price));
            }else{
                $_SESSION['cart'][$optionId][$activity_date] = $cartItem;
            }    
        }else{
            echo '0';exit;
        }

        //echo '<pre>';print_r($_SESSION['cart']);

        $data = array('cart_list'=>$_SESSION['cart']);
        $this->load->view('front/carts/add_item_to_cart', $data);

    }
    
    function remove_item_from_cart(){
        $optionId = $this->input->post("optionId", TRUE);
        $activity_date = $this->input->post("activity_date", TRUE);

        unset($_SESSION['cart'][$optionId][$activity_date]);

        if( empty($_SESSION['cart'][$optionId]) ){
            unset($_SESSION['cart'][$optionId]);
        }
        
        die('1');
    }

    function checkout(){

        //error_reporting(E_ALL);
        //ini_set('display_errors', 1);

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            $travInfo = $this->input->post('TravelerInfo');
            $activityAdditions = $this->input->post('ActivityAdditions');
            $passengerAdditions = $this->input->post('PassengerAdditions');

            //echo '<pre>';print_r($activityAdditions);exit;

            foreach($travInfo as $optionId=>$info){
                foreach($info as $activity_date=>$details){
                   $_SESSION['cart'][$optionId][$activity_date]['traveler_info'] = $details;
                }
            }


            if( !empty($activityAdditions) ){
                foreach($activityAdditions as $optionId=>$info){
                    foreach($info as $activity_date=>$details){
                        foreach($details as $additionTypeID=>$value){
                            $_SESSION['cart'][$optionId][$activity_date]['activity_additions'][ $value['addtype'] ][$additionTypeID]['value'] = $value['value'];
                        }
                    }
                }
            }

            if( !empty($passengerAdditions) ){
                foreach($passengerAdditions as $optionId=>$info){
                    foreach($info as $activity_date=>$details){
                        foreach($details as $additionTypeID=>$value){
                            $_SESSION['cart'][$optionId][$activity_date]['passenger_additions'][ $value['addtype'] ][$additionTypeID]['value'] = $value['value'];
                        }
                    }
                }
            }

            //echo '<pre>';print_r($_SESSION);exit;
            //$_SESSION['cart_billing'] = $this->input->post('cart_billing');

            redirect('front/carts/billing');

        }

        $countries = $this->db->order_by('name' , 'asc')->get('countries')->result();

        
        $res = $this->db->order_by('name' , 'asc')->where('country_id' , '192')->get('states')->result();
        $states = '';
        foreach($res as $re){
            $states  .= '<option value="'.$re->name.'">'.$re->name.'</option>';
        }

        $cart_list = isset($_SESSION['cart']) ? $_SESSION['cart'] : null;

        if( empty($cart_list) ){
            redirect('/');
        }

        $userInfo = getUserById($this->session->userdata['valid_user']['id']);

        $cart_billing = isset($_SESSION['cart_billing']) ? $_SESSION['cart_billing'] : null;
        $data = array(
            'userInfo'=>$userInfo,
            'cart_list'=>$_SESSION['cart'],
            'cart_billing'=>$cart_billing,
            'countries'=>$countries,
            'states' => $states
        );

        $data['page'] = "front/carts/checkout";

        //$this->load->view(FRONT_LAYOUT, $data);
        $this->load->view('front/search_layout', $data);
    }

    function billing(){

        //error_reporting(E_ALL);
        //ini_set('display_errors', 1);

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            $_SESSION['cart_billing'] = $this->input->post('cart_billing');

            redirect('front/carts/confirm');

        }

        $countries = $this->db->order_by('name' , 'asc')->get('countries')->result();
        
        $res = $this->db->order_by('name' , 'asc')->where('country_id' , '192')->get('states')->result();
        $states = '';
        foreach($res as $re){
            $states  .= '<option value="'.$re->name.'">'.$re->name.'</option>';
        }

        $cart_list = isset($_SESSION['cart']) ? $_SESSION['cart'] : null;

        if( empty($cart_list) ){
            redirect('/');
        }

        $userInfo = getUserById($this->session->userdata['valid_user']['id']);

        $cart_billing = isset($_SESSION['cart_billing']) ? $_SESSION['cart_billing'] : null;

        $data = array(
            'userInfo'=>$userInfo,
            'cart_list'=>$_SESSION['cart'],
            'cart_billing'=>$cart_billing,
            'countries'=>$countries,
            'states' => $states
        );

        $data['page'] = "front/carts/billing";

        //$this->load->view(FRONT_LAYOUT, $data);
        $this->load->view('front/search_layout', $data);
    }

    function confirm(){

        //error_reporting(E_ALL);
        //ini_set('display_errors', 1);

        $cart_list = isset($_SESSION['cart']) ? $_SESSION['cart'] : null;
        $cart_billing = isset($_SESSION['cart_billing']) ? $_SESSION['cart_billing'] : null;

        if( empty($cart_list) ){
            redirect('/');
        }
        //echo '<pre>';print_r($cart_billing);exit;

        if ($_SERVER['REQUEST_METHOD'] === 'POST') { 

            $user_id = $this->session->userdata['valid_user']['id'];

            $submitted = $this->input->post("submitted", TRUE);
            if($submitted){

                $activityReservationAPIObj = new ActivityReservationAPI();
                //Prebook and check availability and change in activity
                $cart_error = false;
                $total_price = 0;
                foreach($cart_list as $optionId=>$cart){
                    foreach($cart as $activity_date=>$cartItem){
                        //echo '<pre>';print_r($cartItem['activityName']);exit;
                        $preBook = $activityReservationAPIObj->ActivityPreBook($cartItem);
                        //echo '<pre>';print_r($preBook);exit;

                        if(empty($preBook['ActivityInfo'])){
                            $cartItem['error'] = true;
                            $cartItem['error_message'] = 'Unfortunately, our supplier does not have availability for '.$cartItem['activityName'].'. Please reduce the amount of people on the booking or remove the activity entirely in order to proceed.';
                            $cart_error = true;
                            $_SESSION['cart'][$optionId][$activity_date] = $cartItem;
                            continue;
                        }

                        $cartItem['cancellation_policy'] = $preBook['CancellationPolicy'];
                        $cartItem['cancellation_policy'] = $this->cancellation_policy_text($preBook['CancellationPolicy']);

                        $cartItem['original_prices'] = $preBook['OriginalPrices'];//prices without delta price
                        $cartItem['passengers'] = $preBook['Passengers'];
                        //$cartItem['activity_additions'] = $preBook['ActivityAdditions'];

                        if($preBook['ActivityInfo']['total_price'] != $cartItem['total_price']){
                            $cartItem['error'] = true;
                            $cartItem['error_message'] = 'Price of this item has been changed please check updated price and than confirm.';
                            $cart_error = true;
                        }
                        $cartItem['total_price'] = $preBook['ActivityInfo']['total_price'];//updating price in case there is change in price during process

                        $cartItem['currency'] = $preBook['ActivityInfo']['currency'];
                        
                        $adult_price = isset($preBook['ActivityInfo']['adult_price']) ? $preBook['ActivityInfo']['adult_price'] : 0;//updating price in case there is change in price during process
                        $unit_price = isset($preBook['ActivityInfo']['unit_price']) ? $preBook['ActivityInfo']['unit_price'] : 0;
                        $child_price = isset($preBook['ActivityInfo']['child_price']) ? $preBook['ActivityInfo']['child_price'] : 0;

                        if($adult_price != $cartItem['adult_price'] || $child_price != $cartItem['child_price'] || $unit_price != $cartItem['unit_price']){
                            $cartItem['error'] = true;
                            $cartItem['error_message'] = 'Price of this item has been changed please check updated price and than confirm.';
                            $cart_error = true;
                        }
                        $cartItem['adult_price'] = $adult_price;
                        $cartItem['child_price'] = $child_price;
                        $cartItem['unit_price'] = $unit_price;

                        $_SESSION['cart'][$optionId][$activity_date] = $cartItem;
                        
                        $total_price = $total_price + doubleval($cartItem['total_price']);

                    }
                }

                if($cart_error){
                    redirect('front/carts/confirm');        
                }

                //echo '<pre>';print_r($_SESSION);exit;

                //Process payment here
                //$price = (int)$total_price;
                $price = $total_price*100;
                $paymentInfo = array(
                    'amount' => $price,
                    'currency' => 'usd',
                    'card' => array(
                        "number" => $cart_billing['cardNumber'],
                        "exp_month" => $cart_billing['month'],
                        "exp_year" => $cart_billing['year'],
                        "cvc" => $cart_billing['security_code'],
                    ),
                    'shipping_methods' => null,
                    'selected_shipping_method' => null,
                    'shipping' => array(
                        'name' =>$cart_billing['firstname'].' '.$cart_billing['lastname'],
                        'address' => array(
                            "line1" => $cart_billing['street'],
                            "line2" => '',
                            'city' => $cart_billing['city'],
                            'postal_code' => $cart_billing['zip'],
                            'state' => $cart_billing['state'],
                            'country' => $cart_billing['country']
                        )
                    )
                );
                
                $payResponse = $this->stripePayment($paymentInfo); 
                
                //echo '<pre>';print_r($payResponse);exit;

                if($payResponse['success'] == 1){
                    $stripe_id = $payResponse['charge_info']->id;
                    $stripePaymetarray = serialize($payResponse['charge_info']->__toArray(true));

                    //adding record into activity_booking table
                    $activityBooking = array(
                        "user_id" => $user_id,
                        "currency" => $cartItem['currency'],
                        "total_amount"=> $total_price,
                        "created" => date('Y-m-d h:i:s'),
                        "stripe_id" => $stripe_id,
                        "stripe_payment_response" => $stripePaymetarray,
                        "lastfour" => substr($cart_billing['cardNumber'], -4),
                        "card_info" => serialize($cart_billing),
                        "payment_info" => serialize($paymentInfo)
                    );

                    $this->db->insert('activity_bookings' , $activityBooking);
                    $activity_booking_id = $this->db->insert_id();
                
                    $vouchers = '';
                    $activities_added = array();
                    foreach($cart_list as $optionId=>$cart){
                        foreach($cart as $activity_date=>$cartItem){

                            $bookreps = $activityReservationAPIObj->bookActivity($cartItem);

                            if( !$bookreps ){
                                //echo '<pre>';print_r($cartItem);exit;
                                $data['error'] = 'Error in booking.';//$bookreps['sFault']['faultstring'];
                                $_SESSION['error'] = 'Error in booking.';//$bookreps['sFault']['faultstring'];
                                redirect('front/carts/confirm');
                            }else{
                                $res = $this->quote_model->addActivityBookingDetailsTouricoAPI($activity_booking_id, $cartItem, $bookreps);
                                $activities_added[] = $res;
                                $vouchers .='<p style="color:#fff;margin-bottom:10px;">Your reservation for '.$cartItem['optionTitle'].' on  '.date('Y-m-d',strtotime($cartItem['activity_date'])).' in '.$cartItem['location'].'  is now confirmed successfully and your confirmation number is <b>'.$res['voucher_code'].'</b></p>';
                            }
                        }
                    }

                    $this->load->helper('pdf');
                    $userInfo = getUserById($user_id);
                    $this->load->library('email');
                    $this->email->set_mailtype("html");
                    $this->email->from(NOREPLY, 'Wholesale Hotels Group');
                    $this->email->to($userInfo->var_email);
                    //$this->email->to('adnankhanbs@gmail.com');
                    
                    foreach($activities_added as $pdfindex=>$act){
                        tcActivitypdf($activity_booking_id, $pdfindex, $act['id'] , true);
                        $this->email->attach('application/helpers/tcpdf/examples/files/Invoice'.$pdfindex.'.pdf');
                    }

                    $this->email->subject('Activity Booking Confirmation');
                    $mail_body = '<p style="color:#fff;margin-bottom:10px;">Dear '.$userInfo->var_fname . ',</p>';
                    $mail_body .='<p style="color:#fff;margin-bottom:10px;">Thank you for using <b>Wholesale Hotels Group<sup>sm</sup></b>.</p>';
                    
                    $mail_body .= $vouchers;
                    
                    $mail_body .='
                    <p style="color:#fff;margin-bottom:10px;">Your payment of <strong>$'.$total_price.'</strong> was successfully charged and your charges will appear as <strong>WholsaleHotLs8882872307</strong> .</p>

                    <p style="color:#fff;margin-bottom:10px;"><strong>Life without limits!</strong> Interested in our <strong>Lifetime Plan?</strong> Reach out to our customer service to inquire about pricing! </p>
                    <p style="color:#fff;margin-bottom:10px;">Thank you for choosing <strong>Wholesale Hotels Group</strong> where we are <i>Where better deals are made for YOU!</i></p>
                    <p style="color:#fff;margin-bottom:10px;"><strong>PS: This inbox is NOT monitored, please do NOT send us any emails here.</strong></p>';


                    $mail_body .='
                        <p style="border:1px solid #000;"></p>
                        <p><b style="font-size:14px; display:block; text-align:center; color:#fff;">NEED A TICKET FOR AN EVENT OR CONCERT?</b></p>
                        <p style="color:#fff;">We are proud to be affiliated with TicketLiquidator which is one of the biggest names in the event ticket industry. On many occasions, you can obtain tickets for events that are even sold out. Type in a Venue, an Event or a Performer and see how much you can save!</p>
                        
                        <p><a href="https://www.ticketliquidator.com/search?q=search-by-artist-or-event&ref=cj&utm_source=cj&utm_medium=aff&utm_campaign=8137399&xtor=AL-168-[cj]-[8137399]" target="_blank"> <img style="display:block; width:100%; height:auto; " src="'.base_url().'public/assets/img/find-tickets.jpg" alt=""></a></p>

                        <p style="border:1px solid #000;"></p>
                        <p><b style="font-size:14px; display:block; text-align:center; color:#fff;">NEED INSURANCE?</b></p>
                        <p style="color:#fff;">We (<strong>Wholesale Hotels Group</strong><sup>sm</sup>) do NOT offer travel insurance, however you can purchase travel insurance from a third party insurance company. Please note that we do not directly sell any types of insurance; we are solely an Affiliate to a third party insurance company.</p>
                    ';
                    $mail_body .='<a style="display:inline-block; verticle-align:top; max-width:150px;" href="http://www.dpbolvw.net/click-8137399-10892804-1466615700000" target="_top">
                                        <img style="display:block; width:100%; height:auto;" src="http://www.ftjcfx.com/image-8137399-10892804-1466615700000" width="125" height="125" alt="Allianz Travel Insurance" border="0"/>
                                    </a>
                                    <a style="display:inline-block; verticle-align:top; max-width:150px;" href="http://www.jdoqocy.com/click-8137399-11779657-1466617745000" target="_top">
                                        <img style="display:block; width:100%; max-width:150px; height:auto;" src="http://www.yceml.net/0585/11779657-1499411265028" width="300" height="100%" alt="Allianz Travel Insurance" border="0"/>
                                    </a>
                                    <a style="display:inline-block; verticle-align:top; max-width:150px;" href="http://www.tkqlhce.com/click-8137399-11176385-1466616611000" target="_top">
                                        <img style="display:block; width:100%; max-width:150px; height:auto;" src="http://www.yceml.net/0449/11176385-1499411104094" width="300" height="100%" alt="" border="0"/>
                                    </a>';
                    
                    $mail_body .='
                        <p style="border:1px solid #000;"></p>
                        <h3 style="font-size:17px; margin:0 0 10px; font-weight:bold; text-align:center; color:#fff;">NEED A RIDE?</h3>
                        <h3 style="font-size:14px; margin:15px 0 10px; font-weight:bold; text-align:center;color:#e00303;"><a href="https://www.uber.com/invite/8z1j5" target="_blank"><img src="'.base_url().'public/assets/img/uber-logo.png" alt="uber"></a></h3>
                                    <h2 style="font-size:13px; margin:0 0 10px; color:#fff;">GET <span style="color:#fff; font-weight:bold;">$15 OFF OR MORE</span>depending on your location by signing up through the banner above! (or enter Promo Code <span style="color:#fff; font-weight:bold;">8z1j5</span> in the app)</h2>
                                    
                                    
                                    
                                    <h3 style="font-size:14px; margin:0 0 10px; font-weight:bold; text-align:center;color:#e00303;"><a href="https://www.lyft.com/invite/SZILARD527631" target="_blank"><img src="'.base_url().'public/assets/img/lyft-logo.png" alt="LYFT"></a></h3>
                                    <h2 style="font-size:13px; margin:0 0 10px; color:#fff;">GET <span style="color:#fff; font-weight:bold;">$50 OFF</span> towards your first rides by signing up through the banner above!</h2>
                                    
                    ';
                    
                    
                    $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Sincerely,</br>';
                    $mail_body .= '<p style="color:#fff;margin-bottom:10px;"><b>Wholesale Hotels Group<sup>sm</sup></b><br>
                        <small style="font-style: italic;">Where better deals are made for YOU!</small><b><sup>sm</sup></b>
                        </p>';

                    
                    $data['mail_body'] = $mail_body;
                    $message = $this->load->view('admin/account/genral_emailtemplate', $data, TRUE);

                    try{
                        $this->email->message($message);
                    }catch(Exception $exc){
                        die('email sending issue.');   
                    }
                    $this->email->send();

                    foreach($activities_added as $pdfindex=>$act){
                        unlink('application/helpers/tcpdf/examples/files/Invoice'.$pdfindex.'.pdf');
                    }

                    $data['success'] = "Good job! Your reservation has been successfully made.";

                    //reseting cart session at this point
                    unset($_SESSION['cart']);
                    unset($_SESSION['cart_billing']);
                    redirect('front/carts/thankyou/'.$activity_booking_id);

                }else{   
                    $data['error'] = $payResponse['details'];
                }

            }else{
                redirect('front/carts/confirm');
            }
        }

        $data['cart_list'] = $cart_list;
        $data['cart_billing'] = $cart_billing;
            
        //echo '<pre>';print_r($data);exit;

        $data['page'] = "front/carts/confirm";
        //$this->load->view(FRONT_LAYOUT, $data);
        $this->load->view('front/search_layout', $data);
    }


    function thankyou($activity_booking_id){
        
        $res = $this->db->where('activity_booking_id' , $activity_booking_id)->get('activity_bookings_items')->result();
        $activity_booking = $this->db->where('id' , $activity_booking_id)->get('activity_bookings')->result();

        $data = array(
            'activity_booking_id'=>$activity_booking_id,
            'activity_bookings_items'=>$res,
            'activity_booking'=>$activity_booking[0],
            'success'=>1
        );

        $data['page'] = "front/carts/thankyou";
        //$this->load->view(FRONT_LAYOUT, $data);   
        $this->load->view('front/search_layout', $data);
    }

    function stripePayment($paymentInfo){

        \Stripe\Stripe::setApiKey('sk_test_pXZu0HXdj2nffgTwk8xbG8Tr');
        $planInfo = null;
        try 
        {
            $charge = \Stripe\Charge::create($paymentInfo);
            return array('success' => true, 'charge_info' => $charge);
        } 
        catch (\Stripe\Error\Card $e) 
        {
            $body = $e->getJsonBody();
            $err = $body['error'];
            return array('success' => false, 'reason' => 'card_declined', 'details' => $err['message']);
        } 
        catch (\Stripe\Error\InvalidRequest $e) 
        {
            return array('success' => false, 'reason' => 'invalid_parameter_supplied', 'details' => 'Invalid parameter supplied to stripe');
        } 
        catch (\Stripe\Error\Authentication $e) 
        {
            return array('success' => false, 'reason' => 'secret_key_not_valid', 'details' => 'Invalid parameter supplied to stripe');
        } 
        catch (\Stripe\Error\ApiConnection $e) 
        {
            return array('success' => false, 'reason' => 'connection_problem', 'details' => 'connection to stripe is not working');
        } 
        catch (Exception $e) 
        {
            return array('success' => false, 'reason' => 'other_error', 'details' => 'connection to stripe is not working');
        }
    }

    function pdf($activity_booking_item_id = false , $saveServer = false){

        if(!isLogin()){
            redirect("/");
        }
        
        if($activity_booking_item_id){

            $loginID = $this->session->userdata['valid_user']['id'];
            $res = $this->db->where('id' , $activity_booking_item_id)->get('activity_bookings_items')->result();

            if(count($res) > 0 ){

                if($res[0]->user_id == $loginID || isAdminLogin() ){
                    $this->load->helper('pdf_helper');
                    //$this->load->helper('pdf');
                    $data = array();
                    tcActivitypdf($res[0]->activity_booking_id, 0, $activity_booking_item_id, $saveServer);    
                }else{
                    redirect('/');
                }
                
            }else{
                redirect('/');
            }

        }else{
            redirect('/');
        }
        
    }

    //using in admin
    function voucher($activity_booking_item_id = false , $saveServer = false){

        if(!isLogin()){
            redirect("/");
        }
        
        if($activity_booking_item_id){

            $res = $this->db->where('id' , $activity_booking_item_id)->get('activity_bookings_items')->result();

            if(count($res) > 0 ){

                if( isAdminLogin() ){
                    $this->load->helper('pdf_helper');
                    //$this->load->helper('pdf');
                    $data = array();
                    tcActivityVoucher($res[0]->activity_booking_id, 0, $activity_booking_item_id, $saveServer);    
                }else{
                    redirect('/');
                }
                
            }else{
                redirect('/');
            }

        }else{
            redirect('/');
        }
        
    }

    function cancelActivityBookingAjax($id){

        $this->db->where('id' , $id);
        $res = $this->db->get('activity_bookings_items')->result();

        //echo '<pre>';print_r($res);exit;

        if(count($res) > 0){

            $booking_response = unserialize($res[0]->booking_response);
            //$reserID = $res[0]->reservation_id;
            //echo '<pre>';print_r($booking_response);exit;
            $reserID = $booking_response['reservationId'];

            $activitiesAPICoreObj = new ActivitiesAPICore();

            //$cancel = array('CancelReservationResponse'=>array('CancelReservationResult'=>true));//for testing
            $cancel = $activitiesAPICoreObj->CancellActivityBooking($reserID);
            //echo '<pre>';print_r($cancel);

            if(isset($cancel['CancelReservationResponse'])){

                if($cancel['CancelReservationResponse']['CancelReservationResult'] == true){

                
                    $this->db->where('id' , $id);
                    $dat = array(
                        //"return_amount" => $retAmount,
                        "cancel_status" => 1,
                        "admin_changed" => 0
                    );

                    if($this->db->update('activity_bookings_items' , $dat)){

                        $activity_info = unserialize($res[0]->activity_info);
                        $cancellation_policy = $this->cancellation_policy_text($activity_info['cancellation_policy']);
                        //echo '<pre>';print_r($cancellation_policy);exit;
                        $cancellation_policy_text = '';
                        foreach($cancellation_policy as $cp){
                            $cancellation_policy_text = $cp['message'].' <br />';
                        }
                        
                        $user = getUserById($res[0]->user_id);
                        $this->load->library('email');
                        $this->email->set_mailtype("html");
                        $this->email->from(NOREPLY, 'Admin');
                        $this->email->to($user->var_email);
                        $this->email->subject('Activity Reservation Cancelation');

                        $mail_body = '<p style="color:#fff;margin-bottom:10px;">Dear '.$user->var_fname . ',</p>';
                        $mail_body .='<p style="color:#fff;margin-bottom:10px;">Thank you for using <b>Wholesale Hotels Group<sup>sm</sup></b>. We are now confirming your cancellation request for <b>'.$res[0]->activity_title.'</b> with confirmation number: <b>'.$res[0]->code.'</b>.</p>';
                        $mail_body .='<p style="color:#fff;margin-bottom:10px;">Your original reservation\'s time and location: <b>'.$res[0]->activity_date.'</b> in <b>'.$res[0]->location.'</b>.</p>';
                        $mail_body .='<p style="color:#fff;margin-bottom:10px;">Your cancellation policy is:<br /> <b>'.$cancellation_policy_text.'</b></p>';
                        $mail_body .='<p style="color:#fff;margin-bottom:10px;">Your refund (if eligible) will be processed within 5-7 business days. Please note that every credit card company processes refunds differently; if you have any questions regarding to their practice, please be kind and contact the issuer of the credit card you used for your reservation.</p>';

                        $mail_body .='<p style="color:#fff;margin-bottom:10px;">Again, Thank you for choosing <b>Wholesale Hotels Group</b> where we are Where better deals are made for YOU!</p>';
                        $mail_body .='<p style="color:#fff;margin-bottom:10px;">PS: This inbox is NOT monitored, please do NOT send us any emails here.</p>';
                        $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Sincerely,</br>';
                        $mail_body .= '<p style="color:#fff;margin-bottom:10px;"><b>Wholesale Hotels Group<sup>sm</sup></b><br>
                            <small style="font-style: italic;">Where better deals are made for YOU!</small><b><sup>sm</sup></b>
                            </p>';

                        $data['mail_body'] = $mail_body;
                        $message = $this->load->view('admin/account/genral_emailtemplate', $data, TRUE);
                        $this->email->message($message);
                        $this->email->send();
                        $data = array(
                            "status" => 'success',
                            "msg" => "Thank you for your cancelation request! Your reservation cancelation confirmation will be sent to your email within a few minutes. Thank you for your patience!",
                            "id" => $id
                        );

                    }else{

                        $data = array(
                            "status" => 'error',
                            "error" => 'server issue'
                        );
                    }


                }else{
                    $data = array(
                        "status" => 'error',
                        "error" => 'Error in Cancellation!'
                    );
                }

            }else{
                $data = array(
                    "status" => 'error',
                    "error" => 'Error in Cancellation!'
                );
            }
        }else{
            $data = array(
                "status" => 'error',
                "error" => 'Such booking not exist!'
            );
        }
        
        echo json_encode($data);

    }

    function cancellation_policy_text($cancellationPolicy) {


        $finalCancellationPolicy = array();
        foreach ($cancellationPolicy as $index=>$cancellation_policy){

            $message = "";

            if ($cancellation_policy['Deadline']['offsetUnit'] == "Hours"){
                
                if ($cancellation_policy['Deadline']['unitsFromCheckIn'] == "0"){
                    $message .= "The cancellation penalty for No Show is ";
                }else{
                    $message .= "The cancellation rule is <b>" . $cancellation_policy['Deadline']['unitsFromCheckIn'] . "  " . 
                    strtolower($cancellation_policy['Deadline']['offsetUnit']) . 
                    "</b> prior to the reservation date (noon local activity’s time zone)  and cancellation penalty will be ";
                }

                if ( $cancellation_policy['Penalty']['value'] == 100){
                    $message .= " <b>full amount</b>.";
                }else if( $cancellation_policy['Penalty']['value'] == 0 ){
                    $message .= " <b>full amount</b>.";
                }else{
                    $btype = $cancellation_policy['Penalty']['basisType'];
                    $message .=  $cancellation_policy['Penalty']['value'] . $btype;
                }

            }

            $finalCancellationPolicy[$index] = $cancellation_policy;
            $finalCancellationPolicy[$index]['message'] = str_replace('Percent', '%', $message);
        }
        return $finalCancellationPolicy;
    }
}

?>