<?php

class Carts extends Front_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('session');
    }

    function add_item_to_cart() {

        $optionId = $this->input->post("optionId", TRUE);
        $activityId = $this->input->post("activityId", TRUE);
        $activityName = $this->input->post("activityName", TRUE);
        $thumbUrl = $this->input->post("thumbUrl", TRUE);
        $optionTitle = $this->input->post("optionTitle", TRUE);

        $activity_date = $this->input->post("activity_date", TRUE);
        $no_of_adults = $this->input->post("no_of_adults", TRUE);
        $no_of_childs = $this->input->post("no_of_childs", TRUE);
        $adult_price = $this->input->post("adult_price", TRUE);
        $child_price = $this->input->post("child_price", TRUE);
        $currency = $this->input->post("currency", TRUE);

        $cartItem = array(
			'optionId'=>$optionId,
			'activityId'=>$activityId,
			'activityName'=>$activityName,
            'thumbUrl'=>$thumbUrl,
			'optionTitle'=>$optionTitle,
			'activity_date'=>$activity_date,
			'no_of_adults'=>$no_of_adults,
			'no_of_childs'=>$no_of_childs,
			'adult_price'=>$adult_price,
			'child_price'=>$child_price,
			'currency'=>$currency,
			'total_price'=>(float)(($no_of_adults * $adult_price) + ($no_of_childs * $child_price))
        );


        if( isset($_SESSION['cart'][$optionId][$activity_date]) ){
        	$_SESSION['cart'][$optionId][$activity_date]['no_of_adults'] = (int)$_SESSION['cart'][$optionId][$activity_date]['no_of_adults'] + (int)$no_of_adults;
        	$_SESSION['cart'][$optionId][$activity_date]['no_of_childs'] = (int)$_SESSION['cart'][$optionId][$activity_date]['no_of_childs'] + (int)$no_of_childs;
        	$_SESSION['cart'][$optionId][$activity_date]['total_price'] = $_SESSION['cart'][$optionId][$activity_date]['total'] + (float)(($no_of_adults * $adult_price) + ($no_of_childs * $child_price));
        }else{
        	$_SESSION['cart'][$optionId][$activity_date] = $cartItem;
        }

        //echo '<pre>';print_r($_SESSION['cart']);

        $data = array('cart_list'=>$_SESSION['cart']);

        $this->load->view('front/carts/add_item_to_cart', $data);

    }
	
    function remove_item_from_cart(){
        $optionId = $this->input->post("optionId", TRUE);
        $activity_date = $this->input->post("activity_date", TRUE);

        unset($_SESSION['cart'][$optionId][$activity_date]);
        die('1');
    }

    function checkout(){

        $data['page'] = "front/carts/checkout";

        $this->load->view(FRONT_LAYOUT, $data);
    }
}

?>