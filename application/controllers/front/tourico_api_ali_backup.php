<?php

class Tourico_api extends Front_Controller
{
    var $days = 0;

    function __construct()
    {
        parent::__construct();
        $this->load->helper('tourico_api');
        //Flights
        $this->load->helper('skyper_api');
        $this->load->model('mdl_app', 'app');
        $this->load->helper('travellanda');
        $this->load->model('travellanda_api/travellanda_hotel_model', 'travellanda');
        $this->load->model('tourico_api/destination_model');
        $this->load->model('tourico_api/tourico_hotel_price_multiplier');
        $this->load->model('tourico_car_search');
        $this->load->model('tourico_car_search');
        $this->load->library('session');
        $this->load->model('front/quote_model');
        $this->load->model('tourico_activity');
        $this->load->model('tourico_cruise');
        $this->load->helper('stripe_lib/init');
    }

    //Flights
    function ViewSearchFlights()
    {
        //ini_set('display_errors', 1);
        //error_reporting(E_ALL);
        $searchData = array(
            "optradio" => $this->input->get('optradio', TRUE),
            "Flight_from" => $this->input->get('Flight_from', TRUE),
            "Flight_fromID" => $this->input->get('Flight_fromID', TRUE),
            "Flight_to" => $this->input->get('Flight_to', TRUE),
            "Flight_toID" => $this->input->get('Flight_toID', TRUE),
            "departure_date" => $this->input->get('departure_date', TRUE),
            "return_date" => $this->input->get('return_date', TRUE),
            "price_from" => 0,
            "price_to" => 9999,
            "pnum" => 1
        );
        if (isset($_GET['maxstopovers']) && $_GET['maxstopovers'] != "") {
            $searchData['maxstopovers'] = $this->input->get('maxstopovers', TRUE);
        }

        if (!empty($_GET['pnum'])) {
            $searchData['pnum'] = $this->input->get('pnum', TRUE);
        }

        if (isset($_GET['price_from']) && isset($_GET['price_to'])) {
            $searchData['price_from'] = $this->input->get('price_from', TRUE);
            $searchData['price_to'] = $this->input->get('price_to', TRUE);
        }


        if (!empty($_GET['dtimefrom'])) {
            $searchData['dtimefrom'] = $this->input->get('dtimefrom', TRUE);
        }

        if (!empty($_GET['dtimeto'])) {
            $searchData['dtimeto'] = $this->input->get('dtimeto', TRUE);
        }

        if (!empty($_GET['atimefrom'])) {
            $searchData['atimefrom'] = $this->input->get('atimefrom', TRUE);
        }

        if (!empty($_GET['atimeto'])) {
            $searchData['atimeto'] = $this->input->get('atimeto', TRUE);
        }

        //Return flights depatrue time filter
        if (!empty($_GET['returndtimefrom'])) {
            $searchData['returndtimefrom'] = $this->input->get('returndtimefrom', TRUE);
        }

        if (!empty($_GET['returndtimeto'])) {
            $searchData['returndtimeto'] = $this->input->get('returndtimeto', TRUE);
        }

        //Return flights arrival time filter
        if (!empty($_GET['returnatimefrom'])) {
            $searchData['returnatimefrom'] = $this->input->get('returnatimefrom', TRUE);
        }

        if (!empty($_GET['returnatimeto'])) {
            $searchData['returnatimeto'] = $this->input->get('returnatimeto', TRUE);
        }

        if (!empty($_GET['onlyWorkingDays'])) {
            $searchData['onlyWorkingDays'] = $this->input->get('onlyWorkingDays', TRUE);
        }
        if (!empty($_GET['onlyWeekends'])) {
            $searchData['onlyWeekends'] = $this->input->get('onlyWeekends', TRUE);
        }

        $skyperapi = new FlightSearchAPIResponseProcess();
        $airlines = $skyperapi->getLocalAirLineName();

        //echo '<pre>';print_r($data);exit;

        $searchData['sort'] = !empty($_GET['sort']) ? $this->input->get('sort', TRUE) : 'price';
        $searchData['asc'] = isset($_GET['asc']) ? $this->input->get('asc', TRUE) : 1;

        unset($searchData['pagination_limit']);
        $flights = $skyperapi->searchFlights($searchData);
        //echo '<pre>';print_r($flights );exit;

        $data['flights'] = $flights['flights'];
        $data['total_flights'] = $flights['total_flights'];
        $data['pagination_limit'] = $flights['pagination_limit'];
        $data['page_no'] = $flights['page_no'];
        $data['airlines'] = $airlines;
        $data['checkReturn'] = $flights['checkReturn'];
        $data['page'] = "front/search_result/search_flight_result";
        $data['searchData'] = $searchData;
        $this->page_name = 'Home';

        //getting Cheapest Flight info
        $searchData['sort'] = 'price';
        $searchData['asc'] = 1;
        $searchData['pagination_limit'] = 1;
        $data['cheapestFlight'] = $skyperapi->searchFlights($searchData);

        //getting Recommended Flight info
        $searchData['sort'] = 'quality';
        $searchData['asc'] = 0;
        $data['recommendedFlight'] = $skyperapi->searchFlights($searchData);

        //getting shortest Flight info
        $searchData['sort'] = 'duration';
        $searchData['asc'] = 1;
        $data['shortestFlight'] = $skyperapi->searchFlights($searchData);

        $data['js'] = array(
            'login.js',
            'moment.min.js',
            'front/home.js',
            'affiliate/req_quote.js',
            'custom/components-pickers.js',
            'custom/components-dropdowns.js'
        );
        $data['js_plugin'] = array(
            'bootstrap-fileinput/bootstrap-fileinput.js',
            'checkincheckout/bootstrap-datepicker.js',
            'bootstrap-select/bootstrap-select.min.js',
            'select2/select2.min.js',
            //'jquery.bxslider.min.js',
            'bsStarRating/star-rating.min.js',
            //'scrollbar/jquery.mCustomScrollbar.concat.min.js'
            //'bsSlider/bootstrap-slider.min.js'
        );
        $data['css_plugin'] = array(
            'bootstrap-fileinput/bootstrap-fileinput.css',
            'bootstrap-datepicker/css/datepicker.css',
            'bootstrap-select/bootstrap-select.min.css',
            'select2/select2.css',
            'select2/select2-metronic.css',
            'jquery-ui/jquery-ui.css',
            'jquery.bxslider.css',
            'bsStarRating/star-rating.min.css',
            'bsSlider/bootstrap-slider.min.css',
            'scrollbar/jquery.mCustomScrollbar.min.css'
        );
        $data['css'] = array();
        //echo '<pre>';print_r($data);exit;
        $this->load->view('front/search_layout', $data);
    }

    function get_flights_data()
    {
        error_reporting(0);
        //error_reporting(E_ALL);


        $searchData = array(
            "optradio" => $this->input->get('optradio', TRUE),
            "Flight_from" => $this->input->get('Flight_from', TRUE),
            "Flight_fromID" => $this->input->get('Flight_fromID', TRUE),
            "Flight_to" => $this->input->get('Flight_to', TRUE),
            "Flight_toID" => $this->input->get('Flight_toID', TRUE),
            "departure_date" => $this->input->get('departure_date', TRUE),
            "return_date" => $this->input->get('return_date', TRUE),
        );

        if (isset($_GET['maxstopovers']) && $_GET['maxstopovers'] != "") {
            $searchData['maxstopovers'] = $this->input->get('maxstopovers', TRUE);
        }

        if (!empty($_GET['pnum'])) {
            $searchData['pnum'] = $this->input->get('pnum', TRUE);
        }

        $sort_by = '';
        if (!empty($_GET['sort'])) {
            $searchData['sort'] = $this->input->get('sort', TRUE);
            $sort_by = $searchData['sort'];
        }

        if (isset($_GET['asc'])) {
            $searchData['asc'] = $this->input->get('asc', TRUE);
            $sort_by .= '_' . $searchData['asc'];
        }

        if (isset($_GET['price_from']) && isset($_GET['price_to'])) {
            $searchData['price_from'] = $this->input->get('price_from', TRUE);
            $searchData['price_to'] = $this->input->get('price_to', TRUE);
        }

        if (!empty($_GET['dtimefrom'])) {
            $searchData['dtimefrom'] = $this->input->get('dtimefrom', TRUE);
        }

        if (!empty($_GET['dtimeto'])) {
            $searchData['dtimeto'] = $this->input->get('dtimeto', TRUE);
        }

        if (!empty($_GET['atimefrom'])) {
            $searchData['atimefrom'] = $this->input->get('atimefrom', TRUE);
        }

        if (!empty($_GET['atimeto'])) {
            $searchData['atimeto'] = $this->input->get('atimeto', TRUE);
        }

        //Return flights depatrue time filter
        if (!empty($_GET['returndtimefrom'])) {
            $searchData['returndtimefrom'] = $this->input->get('returndtimefrom', TRUE);
        }

        if (!empty($_GET['returndtimeto'])) {
            $searchData['returndtimeto'] = $this->input->get('returndtimeto', TRUE);
        }

        //Return flights arrival time filter
        if (!empty($_GET['returnatimefrom'])) {
            $searchData['returnatimefrom'] = $this->input->get('returnatimefrom', TRUE);
        }

        if (!empty($_GET['returnatimeto'])) {
            $searchData['returnatimeto'] = $this->input->get('returnatimeto', TRUE);
        }

        if (!empty($_GET['onlyWorkingDays'])) {
            $searchData['onlyWorkingDays'] = $this->input->get('onlyWorkingDays', TRUE);
        }
        if (!empty($_GET['onlyWeekends'])) {
            $searchData['onlyWeekends'] = $this->input->get('onlyWeekends', TRUE);
        }

        if (isset($_GET['page_no']) && $_GET['page_no'] != "") {
            $searchData['page_no'] = $this->input->get('page_no', TRUE);
        }
        //echo '<pre>';print_r($searchData);exit;
        unset($searchData['pagination_limit']);

        $skyperapi = new FlightSearchAPIResponseProcess();
        $flights = $skyperapi->searchFlights($searchData);


        if (isset($searchData['page_no']) && ($searchData['page_no'] <= 1)) {

            //getting Cheapest Flight info
            $searchData['sort'] = 'price';
            $searchData['asc'] = 1;
            $searchData['pagination_limit'] = 1;
            $data['cheapestFlight'] = $skyperapi->searchFlights($searchData);

            //getting Recommended Flight info
            $searchData['sort'] = 'quality';
            $searchData['asc'] = 0;
            $data['recommendedFlight'] = $skyperapi->searchFlights($searchData);

            //getting shortest Flight info
            $searchData['sort'] = 'duration';
            $searchData['asc'] = 1;
            $data['shortestFlight'] = $skyperapi->searchFlights($searchData);
        }

        $searchData['sort_by'] = $sort_by;

        $data['flights'] = $flights['flights'];
        $data['total_flights'] = $flights['total_flights'];
        $data['pagination_limit'] = $flights['pagination_limit'];
        $data['page_no'] = $flights['page_no'];
        $data['airlines'] = $skyperapi->getLocalAirLineName();
        $data['checkReturn'] = $flights['checkReturn'];
        $data['page'] = "front/search_result/get_flights_data";
        $data['searchData'] = $searchData;
        $this->page_name = 'Home';

        //echo '<pre>';print_r($data);exit;
        $this->load->view('front/search_result/get_flights_data', $data);
    }

    function swap(&$arr, $a, $b)
    {
        $tmp = $arr[$a];
        $arr[$a] = $arr[$b];
        $arr[$b] = $tmp;
    }

    function GetCruiseDestinationListBasedOnSuggestion()
    {
        $chingParameter = $this->input->get('term', TRUE);
        $locationList = $this->destination_model->GetCruiseSuggestionListByText($chingParameter);
        $response = array();
        foreach ($locationList as $aLocation) {
            $response[] = array(
                'id' => $aLocation['tourico_destination_id'],
                'label' => $aLocation['destination_name'],
                'value' => $aLocation['destination_name']
            );
        }
        echo json_encode($response);
    }

    function GetHotelList()

    {

        $locationName = $this->input->get('locationName', TRUE);

        $locationId = $this->input->get('locationId', TRUE);

        $check_in = $this->input->get('check_in', TRUE);

        $check_out = $this->input->get('check_out', TRUE);

        $roomNo = $this->input->get('roomNo', TRUE);

        $numberOfAdults = $this->input->get('numberOfAdults', TRUE);

        $numberOfChild = $this->input->get('numberOfChild', TRUE);

        $childAge = $this->input->get('childAge', TRUE);

        $data_for_one_hotel_request = array(

            'check_in_date' => $check_in,

            'check_out_date' => $check_out,

            'adult_number' => $numberOfAdults,

            'child_number' => $numberOfChild,

            'number_of_rooms' => $roomNo,

            'child_age' => $childAge

        );

        //echo '<pre>';print_r($data_for_one_hotel_request);

        $this->session->set_userdata(array('input_data_for_one_hotel' => $data_for_one_hotel_request));

        $room_info_str = "";

        $childAgeStr = "";

        $roomInfoStr = "";

        $roomNo_count = $roomNo;

        $child_age_index = 0;

        for ($i = 0; $i < $roomNo_count; $i++) {

            for ($j = $child_age_index; $j < (intval($numberOfChild[$i]) + $child_age_index); $j++) {


                $childAgeStr .= <<<EOM

                                        <hot1:ChildAge age="{$childAge[$j]}"/>

EOM;

            }


            $child_age_index = $j;


            $roomInfoStr .= <<<EOM

<hot1:RoomInfo>

                  <hot1:AdultNum>{$numberOfAdults[$i]}</hot1:AdultNum>

                  <hot1:ChildNum>{$numberOfChild[$i]}</hot1:ChildNum>

                  <hot1:ChildAges>

				    {$childAgeStr}

                  </hot1:ChildAges>

               </hot1:RoomInfo>

EOM;


            $childAgeStr = "";

        }


        $numberOfAdults_count = count($numberOfAdults);

        $num_adults = 0;

        for ($i = 0; $i < $numberOfAdults_count; $i++) {

            $num_adults += intval($numberOfAdults[$i]);

        }


        $numberOfChild = $this->input->get('numberOfChild', TRUE);

        $numberOfChild_count = count($numberOfChild);

        $num_Child = 0;

        for ($i = 0; $i < $numberOfChild_count; $i++) {

            $num_Child += intval($numberOfChild[$i]);

        }


        $childAge = $this->input->get('childAge', TRUE);

        $childAge_count = count($childAge);

        for ($i = 0; $i < $childAge_count; $i++) {

            $childAge_arr[] = intval($childAge[$i]);

        }

        //echo '<pre>';print_r($roomInfoStr);

        //echo '<pre>';print_r($num_adults);

        //echo '<pre>';print_r($num_Child);

        //echo '<pre>';print_r($childAge);exit;

        $destinationInfo = $this->destination_model->GetDestinationCodeByDestinationName($locationName);

        //echo '<pre>';print_r($destinationInfo);exit;

        $destinationCode = $destinationInfo['destination_code'];


        $locationName = $destinationInfo['location_name'];


        $hotelCityName = "";


        $checkInDate = $check_in;

        $checkInDateObj = DateTime::createFromFormat('m/d/Y', $checkInDate);

        $checkInDate = $checkInDateObj->format('Y-m-d');


        $checkOutDate = $check_out;

        $checkOutDateObj = DateTime::createFromFormat('m/d/Y', $checkOutDate);

        $checkOutDate = $checkOutDateObj->format('Y-m-d');


        $hotelName = $this->input->post('hotelName', TRUE);

        //echo '<pre>';print_r($hotelName);

        $hotelRating = $this->input->post('hotelRating', TRUE);

        //

        $starLevel = intval($hotelRating);

        // Report all errors

        error_reporting(E_ALL);

        //for price and availabiltiy

        $_SESSION['roomInfoStr'] = $roomInfoStr;

        $brandName = "";


        ini_set('memory_limit', '1000M');


        $hotelSearchAPIResponseProcessObj = new HotelSearchAPIResponseProcess();

        $hotel_list = $hotelSearchAPIResponseProcessObj->ProcessSearchResult($roomInfoStr, $destinationCode, $hotelCityName, $checkInDate, $checkOutDate, $num_adults, $locationName, $hotelName, $num_Child, $childAge_arr, $starLevel, $brandName, $roomNo);
        //pre($hotel_list);
        $hotel_list = $this->tourico_hotel_price_multiplier->ProcessPriceOfHotel($hotel_list);

        return $hotel_list;

    }

    function cancelBooking($reserID)
    {
        $reservationApiProcessObj = new ResevationAPICore();
        $cancellation_policies_list = $reservationApiProcessObj->GetCancellationPolicyAfterReseration($reserID);
        $cancel = $reservationApiProcessObj->CancellBooking($reserID);
        if (isset($cancel['CancelReservationResponse'])) {
            if ($cancel['CancelReservationResponse']['CancelReservationResult'] == true) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private function TL_CancelBooking($ref){
        $travellensda = new TravellandaAPICore;
        $data = $travellensda->CancelTLBooking($ref);
        if (isset($data['Body']['Error'])) {
            $returnStatus = false;
        }else{
            $returnStatus = true;
        }

        return $returnStatus;
    }

    private function cancellationPolicyParser($res){
        $roomInf = unserialize($res[0]->roominfo);
        $hotelName = $roomInf['ProductInfo']['@attributes']['name'];
        $hotel_id = $roomInf['ProductInfo']['@attributes']['hotelId'];
        $room_type_id = $roomInf['ProductInfo']['RoomExtraInfo']['@attributes']['hotelRoomTypeId'];
        $check_in = date('Y-m-d', strtotime($res[0]->fromDate));
        $check_out = date('Y-m-d', strtotime($res[0]->toDate));
        if($res[0]->hotelType == 'TL'){
            $policyString = $res[0]->policyData;
        }else {
            $reservationApiProcessObj = new ReservationAPIProcess();
            $cancellation_policies_list = $reservationApiProcessObj->GetCancellationPoliciesBeforeReservation($hotel_id, $room_type_id, $check_in, $check_out);
            $policy_messages = '';
            for ($policy_index = 0; $policy_index < count($cancellation_policies_list); $policy_index++) {
                $policy_messages .= $cancellation_policies_list[$policy_index]['message'];
                $policy_messages .= "</br>";
            }
            $policyString = $policy_messages;
        }

        return $policyString;
    }

    function canceladminBookingAjax($id)
    {
        $this->db->where('id', $id);
        $res = $this->db->get('booking_detail')->result();
        if (count($res) > 0) {
            if (isset($res[0]) && $res[0]->admin_changed != 0) {
                $roomInf = unserialize($res[0]->roominfo);
                $dat = array(
                    "return_amount" => getBookingperRoomPaid($res[0]->book_id),
                    "cancel_status" => 1
                );
                if ($this->db->where('id', $id)->update('booking_detail', $dat)) {
                    $check_in = date('Y-m-d', strtotime($res[0]->fromDate));
                    $check_out = date('Y-m-d', strtotime($res[0]->toDate));
                    $hotelName = $roomInf['ProductInfo']['@attributes']['name'];

                    $user = getUserById($res[0]->uid);
                    $this->load->library('email');
                    $this->email->set_mailtype("html");
                    $this->email->from(NOREPLY, 'Admin');
                    $this->email->to($user->var_email);
                    //$this->email->bcc(STRIPE_ADMIN_EMAIL);
                    $this->email->subject('Reservation Cancelation');

                    $mail_body = '<p style="color:#fff;margin-bottom:10px;">Dear ' . $user->var_fname . '</p>';
                    $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Thank you for using <b>HotelsDifferently<sup>sm</sup></b>. We are now confirming your cancellation request for confirmation number: <b>' . $res[0]->confirm_no . '</b>.
                                The refund (if eligible) for your reservation for <b>' . $hotelName . '</b> for check-in <b>' . $check_in . '</b> and check-out <b>' . $check_out . '</b> with the following cancellation policy:' . $this->cancellationPolicyParser($res) . '
                            </p>';
                    $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Again, thank you for choosing Us and please always give Us a try before booking your next trip!</p>';
                    $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Sincerely,</br>';
                    $mail_body .= '<p style="color:#fff;margin-bottom:10px;"><b>HotelsDifferently<sup>sm</sup></b><br>
                        <small style="font-style: italic;">Making hotels happen for you!</small><b><sup>sm</sup></b>
                        </p>';

                    $data['mail_body'] = $mail_body;
                    $message = $this->load->view('admin/account/genral_emailtemplate', $data, TRUE);
                    $this->email->message($message);
                    $this->email->send();

                    $data = array(
                        "status" => 'success',
                        "msg" => "Booking cancel successfully!",
                        "id" => $id
                    );
                } else {
                    $data = array(
                        "status" => 'error',
                        "error" => 'server issue'
                    );
                }
            } else {
                $data = array(
                    "status" => 'error',
                    "error" => 'Such booking not exist!'
                );
            }
            echo json_encode($data);
        }
    }

    function cancelBookingAjax($id)
    {
        $this->db->where('id', $id);
        $res = $this->db->get('booking_detail')->result();
        if (count($res) > 0) {
            if (isset($res[0])) {
                $roomInf = unserialize($res[0]->roominfo);
                $cancellfee = $this->GetCancellationPolicyAfterReserationPage($id, true);
                if ($cancellfee['status'] == 'success') {
                    if($res[0]->hotelType == "TL"){
                        $result = $this->TL_CancelBooking($res[0]->reservationId);
                        $retAmount = $res[0]->publishPrice;
                    }else {
                        $fee = $cancellfee['fee'];
                        $retAmount = $fee;//getBookingperRoomPaid($res[0]->book_id);
                        /*$retAmount = getBookingperRoomPaid($res[0]->book_id);
                        $retAmount = $retAmount - $fee;*/
                        $reid = $res[0]->reservationId;
                        $result = $this->cancelBooking($reid);
                    }
                    if ($result) {
                        $this->db->where('id', $id);
                        $dat = array(
                            "return_amount" => $retAmount,
                            "cancel_status" => 1,
                            "admin_changed" => 1
                        );
                        if ($this->db->update('booking_detail', $dat)) {

                            $couponValue = getCouponperRoomPaid($res[0]->book_id);
                            if ($couponValue > 0) {
                                $this->load->model('admin/coupon_model');
                                $result = $this->coupon_model->coupon_generate_user_end(array("coupanvalue" => $couponValue));
                            }
                            $check_in = date('Y-m-d', strtotime($res[0]->fromDate));
                            $check_out = date('Y-m-d', strtotime($res[0]->toDate));
                            $hotelName = $roomInf['ProductInfo']['@attributes']['name'];
                            $user = getUserById($res[0]->uid);
                            $this->load->library('email');
                            $this->email->set_mailtype("html");
                            $this->email->from(NOREPLY, 'Admin');
                            $this->email->to($user->var_email);
                            //$this->email->bcc(STRIPE_ADMIN_EMAIL);
                            $this->email->subject('Reservation Cancelation');

                            $mail_body = '<p style="color:#fff;margin-bottom:10px;">Dear ' . $user->var_fname . '</p>';
                            $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Thank you for using <b>HotelsDifferently<sup>sm</sup></b>. We are now confirming your cancellation request for confirmation number: <b>' . $res[0]->confirm_no . '</b>.
                                The refund (if eligible) for your reservation for <b>' . $hotelName . '</b> for check-in <b>' . $check_in . '</b> and check-out <b>' . $check_out . '</b> with the following cancellation policy:' . $this->cancellationPolicyParser($res) . '
                            </p>';
                            $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Again, thank you for choosing Us and please always give Us a try before booking your next trip!</p>';
                            $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Sincerely,</br>';
                            $mail_body .= '<p style="color:#fff;margin-bottom:10px;"><b>HotelsDifferently<sup>sm</sup></b><br>
                                <small style="font-style: italic;">Making hotels happen for you!</small><b><sup>sm</sup></b>
                                </p>';

                            $data['mail_body'] = $mail_body;
                            $message = $this->load->view('admin/account/genral_emailtemplate', $data, TRUE);
                            $this->email->message($message);
                            $this->email->send();

                            $data = array(
                                "status" => 'success',
                                "msg" => "Booking cancel successfully!",
                                "id" => $id
                            );
                        } else {
                            $data = array(
                                "status" => 'error',
                                "error" => 'server issue'
                            );
                        }
                    } else {
                        $data = array(
                            "status" => 'error',
                            "error" => 'Such Booking already cancel, If not please contact to support!'
                        );
                    }
                } else {
                    $data = array(
                        "status" => 'error',
                        "error" => 'Error in Cancellation!'
                    );
                }
            } else {
                $data = array(
                    "status" => 'error',
                    "error" => 'Such booking not exist!'
                );
            }
            echo json_encode($data);
        }
    }

    function GetCancellationPolicyAfterReserationPage($id, $retarray = false)
    {
        $this->db->where('id', $id);
        $res = $this->db->get('booking_detail')->result();
        if (count($res) > 0 && isset($res[0])) {
            $roomInf = unserialize($res[0]->roominfo);
            // cancel for TL
            if($res[0]->hotelType == "TL"){
                //error_reporting(E_ALL);
                //ini_set('display_errors', 1);
                //$policy = $this->TLPolicy($roomInf['roomTypeId'],$res[0]->publishPrice);
                //echo '<pre>';print_r($res);echo '<pre>';print_r($roomInf);exit;
                $policy_messages = "Your hotel is: " . $roomInf['hotelName'] . "</br></br> Your reservation is: " . $res[0]->fromDate . "/" . $res[0]->toDate . "</br></br>";
                $policy_messages .= "Your cancellation policy is: </br>".$res[0]->policyData;
                $policy_messages .= "</br>You paid: $" . getBookingperRoomPaid($res[0]->book_id)/*$res[0]->publishPrice*/ . "</br></br> Are you sure you want to cancel your reservation? Once you cancel it, we will not be able to reinstate it!";
                $data = array(
                    "status" => 'success',
                    'message' => $policy_messages
                );
            }else {
                // cancel for TO
                $reservationApiProcessObj = new ResevationAPICore();
                $cancellation_policies_list = $reservationApiProcessObj->GetCancellationPolicyAfterReseration($res[0]->reservationId);

                $default_timezone = date_default_timezone_get();
                date_default_timezone_set('America/New_York');
                $dt = date('Y-m-d');
                date_default_timezone_set($default_timezone);

                $fee = $reservationApiProcessObj->GetCancellationFees($res[0]->reservationId, $dt);
                $response = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $fee);
                $xml = new SimpleXMLElement($response);
                $body = $xml->xpath('//soapBody')[0];
                $array = json_decode(json_encode((array)$body), TRUE);

                if (isset($array['GetCancellationFeeResponse'])) {
                    if ($retarray) {
                        $toricFee = $array['GetCancellationFeeResponse']['GetCancellationFeeResult']['CancellationFeeValue'];
                        if ($toricFee == $res[0]->price) {
                            $retAmount = $res[0]->price;
                            $couponValue = getCouponperRoomPaid($res[0]->book_id);
                            $retAmount = $retAmount - $toricFee;
                            if ($retAmount < 0) {
                                $retAmount = 0;
                            }
                            $data = array(
                                "status" => 'success',
                                "fee" => $retAmount,
                                "currency" => $array['GetCancellationFeeResponse']['GetCancellationFeeResult']['Currency'],
                                "coupon" => $couponValue
                            );
                        } else if ($toricFee == 0) {
                            $retAmount = getBookingperRoomPaid($res[0]->book_id);
                            $percentage = getExtraPerperRoom($res[0]->book_id);
                            $getFeePerc = $toricFee * ($percentage / 100);
                            $toricFee = $toricFee + $getFeePerc;
                            $couponValue = getCouponperRoomPaid($res[0]->book_id);
                            $retAmount = $retAmount - $toricFee;
                            if ($retAmount < 0) {
                                $retAmount = 0;
                            }
                            $data = array(
                                "status" => 'success',
                                "fee" => $retAmount,
                                "currency" => $array['GetCancellationFeeResponse']['GetCancellationFeeResult']['Currency'],
                                "coupon" => $couponValue
                            );
                        } else {
                            //$retAmount = $res[0]->price;//getBookingperRoomPaid($res[0]->book_id);
                            $retAmount = getBookingperRoomPaid($res[0]->book_id);
                            $percentage = getExtraPerperRoom($res[0]->book_id);
                            $getFeePerc = $toricFee * ($percentage / 100);
                            $toricFee = $toricFee + $getFeePerc;
                            $couponValue = getCouponperRoomPaid($res[0]->book_id);
                            $retAmount = $retAmount - $toricFee;
                            if ($retAmount < 0) {
                                $retAmount = 0;
                            }
                            $data = array(
                                "status" => 'success',
                                "fee" => $retAmount,
                                "currency" => $array['GetCancellationFeeResponse']['GetCancellationFeeResult']['Currency'],
                                "coupon" => $couponValue
                            );
                        }
                    } else {
                        /****
                         * scenario has been changed...
                         * we will only show the cancellation policy with other booking detail... not the refunding amount
                         * that is being retrieved from provider - K
                         */

                        // call for getting cancellation policy
                        //error_reporting(E_ALL);
                        //ini_set('display_errors', 1);
                        //$hotel_id = $roomInf['ProductInfo']['@attributes']['hotelId'];
                        //$room_type_id = $roomInf['ProductInfo']['RoomExtraInfo']['@attributes']['hotelRoomTypeId'];
                        $check_in = date('Y-m-d', strtotime($res[0]->fromDate));
                        $check_out = date('Y-m-d', strtotime($res[0]->toDate));
                        //$reservationApiProcessObj = new ReservationAPIProcess();
                        //$cancellation_policies_list = $reservationApiProcessObj->GetCancellationPoliciesBeforeReservation($hotel_id, $room_type_id, $check_in, $check_out);
                        $policy_messages = "Your hotel is: " . $roomInf['ProductInfo']['@attributes']['name'] . "</br></br> Your reservation is: " . $check_in . "/" . $check_out . "</br></br> Your cancellation policy is: ";
                        $policyString = $this->cancellationPolicyParser($res);
                        $policy_messages .= $policyString;
                        $policy_messages .= "</br>You paid: $" . getBookingperRoomPaid($res[0]->book_id) . "</br></br> Are you sure you want to cancel your reservation? Once you cancel it, we will not be able to reinstate it!";
                        $data = array(
                            "status" => 'success',
                            'message' => $policy_messages
                        );
                    }

                } else {
                    if (isset($array['soapFault']['faultstring'])) {
                        $data = array(
                            "status" => 'error',
                            "error" => $array['soapFault']['faultstring']
                        );
                    } else {
                        $data = array(
                            "status" => 'error',
                            "error" => 'Tourico api error!'
                        );
                    }
                }
            }
        } else {
            $data = array(
                "status" => 'error',
                "error" => 'Such booking not exist!'
            );
        }
        if ($retarray) {
            return $data;
        } else {
            echo json_encode($data);exit;
        }
    }

    function GetCancellationPoliciesBeforeReservationAjax()
    {
        $hotel_id = $this->input->post('hotel_id', TRUE);
        $room_type_id = $this->input->post('room_type_id', TRUE);
        $check_in = $this->input->post('check_in', TRUE);
        $check_out = $this->input->post('check_out', TRUE);

        $dateObj = DateTime::createFromFormat('m/d/Y', $check_in);
        $check_in = $dateObj->format('Y-m-d');

        $dateObj = DateTime::createFromFormat('m/d/Y', $check_out);
        $check_out = $dateObj->format('Y-m-d');

        $reservationApiProcessObj = new ReservationAPIProcess();
        $cancellation_policies_list = $reservationApiProcessObj->GetCancellationPoliciesBeforeReservation($hotel_id, $room_type_id, $check_in, $check_out);
        $retaray = array("cancellation_policies_list" => $cancellation_policies_list);
        echo json_encode($retaray);
    }

    function ViewAjaxSearchResult()
    {
        $hotel_list = $this->GetHotelList();

        if (0 == count($hotel_list)) {
            echo json_encode(array("hotel_list" => $hotel_list, "hotelFound" => false));
        } else {
            echo json_encode(array("hotel_list" => $hotel_list, "hotelFound" => true));
        }

        exit();
    }

    function ViewSearchResult()

    {
    
        if (!is_user_login()) {

            redirect("/");

        }
        set_time_limit(500);


        //error_reporting(E_ALL);

        //ini_set('display_errors', 1);

        ini_set('memory_limit', '256M');


        $destination_name = $this->input->get('locationName', TRUE);

        $destinationInfo = $this->destination_model->GetDestinationCodeByDestinationName($destination_name);

        $destination_id = $destinationInfo['tourico_destination_id'];


        $destination_list = $this->destination_model->GetDestinationListByParentId($destination_id);

        $location_names = array();

        foreach ($destination_list as $a_destination) {

            $location_names[] = $a_destination['location_name'];

        }

        // get and save days globally - K
        $this->days = $this->getDayBetweenDates();
        $hotel_list = $this->GetHotelList();
        
        // get TL hotels - K
        $TAHotels = $this->getTravelAndaHotelsList();
        //echo '<pre>';print_r($TAHotels);exit;
        /*****
         * merging hotels based on name and address
         * if similar found then add rooms in Tourico hotel
         * and unset the TL hotel - K
         */
        $mergedList = $this->mergeHotels($hotel_list, $TAHotels);
        //echo '<pre>';print_r($mergedList);exit;
        $data = array();
        $data['hotel_list'] = $mergedList;
        $data['check_in'] = $this->input->get('check_in', TRUE);
        $data['check_out'] = $this->input->get('check_out', TRUE);
        $data['locationName'] = $this->input->get('locationName', TRUE);

        $data['locationId'] = $this->input->get('locationId', TRUE);

        $data['roomNo'] = $this->input->get('roomNo', TRUE);

        $data['numberOfAdults'] = $this->input->get('numberOfAdults', TRUE);

        $data['numberOfChild'] = $this->input->get('numberOfChild', TRUE);

        $data['childAge'] = $this->input->get('childAge', TRUE);

        $data['hotelName'] = $this->input->get('hotelName', TRUE);

        $data['hotelRating'] = $this->input->get('hotelRating', TRUE);


        if (isset($_GET['hotelName']) && !empty($_GET['hotelName'])) {

            $hotelName = $this->input->get('hotelName', TRUE);

            if (!empty($hotelName)) {

                $newarry = array();

                foreach ($mergedList as $hotlist) {

                    if (strpos(strtolower($hotlist['hotelName']), strtolower($hotelName)) !== false) {

                        $newarry[] = $hotlist;

                    }

                }

                $mergedList = $newarry;

            }

        }


        if (isset($_GET['hotelRating']) && $_GET['hotelRating'] > 0) {

            $hotelRating = $this->input->get('hotelRating', TRUE);

            if ($hotelRating != 0) {

                $newarry = array();

                foreach ($mergedList as $hotlist) {

                    if ($hotlist['star_level'] >= $hotelRating) {

                        $newarry[] = $hotlist;

                    }

                }

                $mergedList = $newarry;

            }

        }

        // echo count($mergedList);echo '<pre>';print_r($mergedList);exit;
        $data['hotel_list'] = $mergedList;

        $data['totalDays'] = $this->days;

        $data['location_names'] = $location_names;

        $data['page'] = "front/search_result/search_result";

        $data['pagination_per_page'] = 10;

        $data['switch_tab_hotel_map_url'] = base_url('view_search_result_map') . '?' . http_build_query($_GET);

        $data['switch_tab_hotel_url'] = base_url('view_search_result') . '?' . http_build_query($_GET);

        $this->page_name = 'Home';


        //add temp data

        $tempData = array();

        $uniqueSession = uniqid();
        $_SESSION['uniqueSession'] = $uniqueSession;

        $date_time = date('Y-m-d H:i:s');

        foreach ($mergedList as $singhotel) {
            /*if($singhotel['hotelName'] == 'The Hills Hotel An Ascend Collection Hotel - Demo (merged) ') {
                  echo '<pre>';print_r($singhotel);exit;
            }*/
            $singhotel['aminities'] = serialize($singhotel['aminities']);

            $singhotel['location'] = serialize($singhotel['location']);

            $singhotel['room_type_list'] = serialize($singhotel['room_type_list']);

            $singhotel['session_id'] = $uniqueSession;

            $singhotel['date_time'] = $date_time;

            $tempData[] = $singhotel;

        }
        //echo '<pre>';print_r($tempData);exit;
        if (count($tempData) > 0) {
            $this->app->insertBatch('temp_hotels_data', $tempData);
        }

        // echo '<pre>';print_r($tempData[0]);exit;

        /*$tHtempData = array();
        foreach ($TAHotels as $tHotel) {
            $tHotel['hotelName'] = $tHotel['hotelName'].' (TA)';
            $tHotel['aminities'] = serialize($tHotel['aminities']);

            $tHotel['location'] = serialize($tHotel['location']);

            $tHotel['room_type_list'] = serialize($tHotel['room_type_list']);

            $tHotel['session_id'] = $uniqueSession;

            $tHotel['date_time'] = $date_time;

            $tHtempData[] = $tHotel;

        }

        //echo '<pre>';print_r($tempData);exit;

        if (count($tHtempData) > 0) {
            $this->app->insertBatch('temp_hotels_data', $tHtempData);
        }
        echo '<pre>';print_r($tHtempData[0]);exit;*/
        $data['uniqueSession'] = $uniqueSession;


        $data['js'] = array(

            'login.js',

            'moment.min.js',

            'front/home.js',

            'custom/sResultC.js',

            'affiliate/req_quote.js',

            'custom/components-pickers.js',

            'custom/components-dropdowns.js'

        );

        $data['js_plugin'] = array(

            'bootstrap-fileinput/bootstrap-fileinput.js',

            'checkincheckout/bootstrap-datepicker.js',

            'bootstrap-select/bootstrap-select.min.js',

            'select2/select2.min.js',

            //'jquery.bxslider.min.js',

            'bsStarRating/star-rating.min.js',

            //'scrollbar/jquery.mCustomScrollbar.concat.min.js'

            //'bsSlider/bootstrap-slider.min.js'

        );

        $data['css_plugin'] = array(

            'bootstrap-fileinput/bootstrap-fileinput.css',

            'bootstrap-datepicker/css/datepicker.css',

            'bootstrap-select/bootstrap-select.min.css',

            'select2/select2.css',

            'select2/select2-metronic.css',

            'jquery-ui/jquery-ui.css',

            'jquery.bxslider.css',

            'bsStarRating/star-rating.min.css',

            'bsSlider/bootstrap-slider.min.css',

            'scrollbar/jquery.mCustomScrollbar.min.css'

        );

        $data['roomChange'] = array(
            'show_children_ages_drop_down.js',
            'changing_rooms.js',
        );

        $data['css'] = array();

        $this->load->view('front/search_layout', $data);

    }

    function searchFilterAndMoreResult()

    {

        if (isset($_POST)) {

            $uniqueSession = $this->input->post('uniqueSession');

            $currentPage = $this->input->post('currentPage');

            $min_price = $this->input->post('min_price');

            $max_price = $this->input->post('max_price');

            $roomNo = $this->input->post('roomNo');

            $stars = $this->input->post('stars');

            $brand = $this->input->post('brand');

            $locations = $this->input->post('locations');

            $hotel_name = $this->input->post('hotel_name');

            $filerarray = array(

                "min_price" => $min_price,

                "max_price" => $max_price,

                "stars" => $stars,

                "brand" => $brand,

                "locations" => $locations,

            );

            if (!empty($hotel_name)) {

                $filerarray['hotel_name'] = $hotel_name;

            }

            $info = getSortDataByUniqueSessionIDInfo($uniqueSession, $filerarray);

            $hotel_list = getSortDataByUniqueSessionID($uniqueSession, $currentPage + 1, $filerarray);

            $data['hotel_list'] = $hotel_list;

            $data['uniqueSession'] = $uniqueSession;
            $data['locationId'] = $this->input->post('locationId');
            $data['locationName'] = $this->input->post('locationName');
            $data['numberOfAdults'] = $this->input->post('numberOfAdults');
            $data['roomNo'] = $this->input->post('roomNo');
            $data['numberOfChild'] = $this->input->post('numberOfChild');
            $data['check_in'] = $this->input->post('from');
            $data['check_out'] = $this->input->post('to');
            $data = $this->load->view('front/search_result/ajaxResultHotels', $data, TRUE);

            $retData = array(

                "status" => 1,

                "html" => $data,

                "nextPage" => $currentPage + 1,

                "totalRec" => $info['total_record']

            );

            echo json_encode($retData);

        }

    }

    private function policyParser($data,$minR,$return=false){
        //echo '<pre>';print_r($data);
        if (!isset($data['Body']['Error'])) {
            $CancellationDeadline = $data['Body']['CancellationDeadline'];
            $text = "<b>Cancellation deadline: </b>" . $CancellationDeadline . "<br/>";
            $policy = '';
            if (isset($data['Body']['Policies']['Policy']['From'])) {
                if ($data['Body']['Policies']['Policy']['Type'] == 'Amount') {
                    $policy .= "From " . $data['Body']['Policies']['Policy']['From'] . " the amount of $" . $data['Body']['Policies']['Policy']['Value'] . " will be charged.<br/>";
                } else if ($data['Body']['Policies']['Policy']['Type'] == 'Percentage') {
                    $policy .= "From " . $data['Body']['Policies']['Policy']['From'] . " the amount of " . $data['Body']['Policies']['Policy']['Value'] . "% of full stay will be charged.<br/>";
                } else if ($data['Body']['Policies']['Policy']['Type'] == 'Nights') {
                    $policy .= "From " . $data['Body']['Policies']['Policy']['From'] . " the amount of $" . ($data['Body']['Policies']['Policy']['Value'] * $minR) . " will be charged.<br/>";
                }
            } else {
                foreach ($data['Body']['Policies']['Policy'] as $k => $v) {
                    //rom 2013-12-14 the amount of $343.97 will be charged.
                    //From 2013-12-18 the amount of 100% of full stay will be charged. (Khurram: You can also use $ value instead of "100% of full stay")
                    if ($v['Type'] == 'Amount') {
                        $policy .= "From " . $v['From'] . " the amount of $" . $v['Value'] . " will be charged.<br/>";
                    } else if ($v['Type'] == 'Percentage') {
                        $policy .= "From " . $v['From'] . " the amount of " . $v['Value'] . "% of full stay will be charged.<br/>";
                    } else if ($v['Type'] == 'Nights') {
                        $policy .= "From " . $v['From'] . " the amount of $" . ($v['Value'] * $minR) . " will be charged.<br/>";
                    }
                }
            }
            $text = $text . $policy;

            if (isset($data['Body']['Alerts']['Alert'])) {
                $alert = "<b>Note:</b> <br/>";
                if (is_array($data['Body']['Alerts']['Alert'])) {
                    foreach ($data['Body']['Alerts']['Alert'] as $val) {
                        $alert .= $val . " <br/>";
                    }
                    $text = $text . $alert;
                } else {
                    $text = $text . $data['Body']['Alerts']['Alert'];
                }
            }

            if (isset($data['Body']['Restrictions']['Restriction'])) {
                $alert = "<b>Note:</b> <br/>";
                if (is_array($data['Body']['Restrictions']['Restriction'])) {
                    foreach ($data['Body']['Restrictions']['Restriction'] as $val) {
                        $alert .= $val . " <br/>";
                    }
                    $text = $text . $alert;
                } else {
                    $text = $text . $data['Body']['Restrictions']['Restriction'];
                }
            }

            if($return)
                return $text;
            else
                echo $text;
        } else {
            //if ($data['Body']['Error']['ErrorId'] == 117) {
            if($return)
                return "Search has been expired";
            else
                echo "Search has been expired";
            //}
            ///echo '<pre>';print_r($data);exit;
        }
    }

    function TL_HotelPolicyAJAX()
    {
        $optionId = $this->input->post('room_type_id', TRUE);
        $minR = $this->input->post('minR', TRUE);
        $travellensda = new TravellandaAPICore;
        $data = $travellensda->HotelPolicies($optionId);
        $this->policyParser($data,$minR);exit;
        //echo $minR;echo '<pre>';print_r($data);exit;
    }

    function TLPolicy($optionId,$minR)
    {
        $travellensda = new TravellandaAPICore;
        $data = $travellensda->HotelPolicies($optionId);
        $this->policyParser($data,$minR,true);
    }

    function sortByNestedArrayHotelRoom($arr)
    {
        for ($i = 0; $i < count($arr); $i++) {
            for ($j = 0; $j < count($arr) - 1 - $i; $j++) {
                if (isset($arr[$j + 1]['occupancy_list']['Room 1']['available_list'][0]['per_night_price']) && isset($arr[$j]['occupancy_list']['Room 1']['available_list'][0]['per_night_price'])) {
                    if ($arr[$j + 1]['occupancy_list']['Room 1']['available_list'][0]['per_night_price'] < $arr[$j]['occupancy_list']['Room 1']['available_list'][0]['per_night_price']) {
                        $this->swap($arr, $j, $j + 1);
                    }
                }
            }
        }
        return $arr;
    }

    function ViewSearchResultInMap()

    {

        //$this->session->set_userdata($data);

        $destination_name = "Las Vegas, Nevada, USA";

        $destinationCode = "LAS";

        $hotelCityName = "";

        $checkInDate = "2016-01-15";

        $checkOutDate = "2016-01-17";

        $adultNumber = 1;

        $hotelLocationName = "";

        $hotelName = "";

        $childNumber = 3;

        $starLevel = 0;

        $brandName = "";

        $childAges = array(10, 5, 10);

        // get and save days globally - K
        $this->days = $this->getDayBetweenDates();

        $hotel_list = $this->GetHotelList();
        //echo '<pre>';print_r($hotel_list);exit;
        // get TL hotels - K

        $TAHotels = $this->getTravelAndaHotelsList();
        /*****
         * merging hotels based on name and address
         * if similar found then add rooms in Tourico hotel
         * and unset the TL hotel - K
         */
        $mergedList = $this->mergeHotels($hotel_list, $TAHotels);
        //echo '<pre>';print_r($mergedList);exit;

        //$hotelSearchAPIResponseProcessObj = new HotelSearchAPIResponseProcess();

        //$hotel_list = $hotelSearchAPIResponseProcessObj->ProcessSearchResult($destinationCode,$hotelCityName,$checkInDate, $checkOutDate, $adultNumber, $hotelLocationName, $hotelName,$childNumber,$childAges,$starLevel,$brandName );


        $destinationInfo = $this->destination_model->GetDestinationCodeByDestinationName($destination_name);

        $destination_id = $destinationInfo['tourico_destination_id'];


        $destination_list = $this->destination_model->GetDestinationListByParentId($destination_id);


        foreach ($destination_list as $a_destination) {

            $location_names[] = $a_destination['location_name'];

        }


        $data = array();

        $data['check_in'] = $this->input->get('check_in', TRUE);
        $data['check_out'] = $this->input->get('check_out', TRUE);
        $data['locationName'] = $this->input->get('locationName', TRUE);
        $data['locationId'] = $this->input->get('locationId', TRUE);
        $data['numberOfAdults'] = $this->input->get('numberOfAdults', TRUE);
        $data['numberOfChild'] = $this->input->get('numberOfChild', TRUE);

        $data['hotel_list'] = $mergedList;

        $data['location_names'] = $location_names;

        $data['page'] = "front/search_result/search_result_map";

        $data['pagination_per_page'] = 10;

        $data['switch_tab_hotel_map_url'] = base_url('view_search_result_map') . '?' . http_build_query($_GET);

        $data['switch_tab_hotel_url'] = base_url('view_search_result') . '?' . http_build_query($_GET);


        //$data['switch_tab'] =

        $this->page_name = 'Home';

        $data['js'] = array(

            'login.js',

            'moment.min.js',

            'front/home.js',

            'affiliate/req_quote.js',

            'custom/components-pickers.js',

            'custom/components-dropdowns.js',

            //'front/jquery.bxslider.js',

            //'custom/sResultC.js'

        );

        $data['js_plugin'] = array(

            'bootstrap-fileinput/bootstrap-fileinput.js',

            'checkincheckout/bootstrap-datepicker.js',

            'bootstrap-select/bootstrap-select.min.js',

            'select2/select2.min.js',

            //'jquery.bxslider.min.js',

            'bsStarRating/star-rating.min.js',

            //'bsSlider/bootstrap-slider.min.js'

        );

        $data['css_plugin'] = array(

            'bootstrap-fileinput/bootstrap-fileinput.css',

            'bootstrap-datepicker/css/datepicker.css',

            'bootstrap-select/bootstrap-select.min.css',

            'select2/select2.css',

            'select2/select2-metronic.css',

            'jquery-ui/jquery-ui.css',

            'jquery.bxslider.css',

            'bsStarRating/star-rating.min.css',

            'bsSlider/bootstrap-slider.min.css'

        );

        $data['css'] = array();


        $this->load->view('front/search_layout', $data);

    }

    function ViewHotelSearchResultById()

    {

        $hotel_id = $this->input->post('hotel_id', TRUE);
        $hotel_type = $this->input->post('hotel_type', TRUE);

        if ($hotel_type == 'TL') {
            $ids = array($this->input->post('hotel_id', TRUE));
            $travellensda = new TravellandaAPICore;
            $data = $travellensda->GetHotelDetails($ids);
            $result = array();
            if (isset($data['Body']['Hotels']['Hotel']['HotelId'])) {
                $hotel = $data['Body']['Hotels']['Hotel'];
                $result['hotelId'] = $hotel['HotelId'];
                $result['hotelName'] = $hotel['HotelName'];
                $result['distanceFromAirport'] = "";
                $result['distanceFromAirportUnit'] = "";
                // facilities as aminities
                $facilities = array();
                if (isset($hotel['Facilities']['Facility'])) {
                    if (is_array($hotel['Facilities']['Facility'])) {
                        foreach ($hotel['Facilities']['Facility'] as $fc) {
                            $facilities[] = $fc['FacilityName'];
                        }
                    } else {
                        $facilities[0] = $hotel['Facilities']['Facility'];
                    }
                }

                $result['aminities'] = $facilities;
                $result['longitude'] = $hotel['Longitude'];
                $result['latitude'] = $hotel['Latitude'];
                $result['facilities_desc'] = "";
                $result['room_desc'] = $hotel['Description'];
                $result['sports_entertainment_desc'] = "";
                $result['meals_desc'] = "";
                $result['payment_desc'] = "";
                $result['location_desc'] = $hotel['Location'];
                $result['room_type_facilities_desc'] = "";
                $result['star_level'] = $hotel['StarRating'];
                $result['hotel_name'] = $hotel['HotelName'];
                $result['city'] = "";
                $result['address'] = $hotel['Address'];
                $images = array();
                if (isset($hotel['Images']['Image'])) {
                    if (is_array($hotel['Images']['Image'])) {
                        foreach ($hotel['Images']['Image'] as $fc) {
                            $images[] = $fc;
                        }
                    } else {
                        $images[0] = $hotel['Images']['Image'];
                    }
                }
                $result['images'] = $images;
                $result['hotel_phone'] = $hotel['PhoneNumber'];
            } else {
                $result['hotelId'] = "";
                $result['hotelName'] = "";
                $result['distanceFromAirport'] = "";
                $result['distanceFromAirportUnit'] = "";
                $result['aminities'] = array();
                $result['longitude'] = "";
                $result['latitude'] = "";
                $result['facilities_desc'] = "";
                $result['room_desc'] = "";
                $result['sports_entertainment_desc'] = "";
                $result['meals_desc'] = "";
                $result['payment_desc'] = "";
                $result['location_desc'] = "";
                $result['room_type_facilities_desc'] = "";
                $result['star_level'] = 0;
                $result['hotel_name'] = "";
                $result['city'] = "";
                $result['address'] = "";
                $result['images'] = array();
                $result['hotel_phone'] = "";
            }

            $hotel_by_id = json_encode(array('hotel_desc_by_id' => $result));


            echo $hotel_by_id;
        } else {

            $hotelSearchAPIResponseProcessObj = new HotelSearchAPIResponseProcess();

            $hotel_list = $hotelSearchAPIResponseProcessObj->GetOneHotelDetailsByHotelIdForHotelDetail($hotel_id);
            $hotel_list = $this->tourico_hotel_price_multiplier->ProcessPriceOfHotelDetail($hotel_list);

            $hotel_by_id = json_encode(array('hotel_desc_by_id' => $hotel_list));


            echo $hotel_by_id;
        }

    }

    function ShowQuotePageTestAnik()
    {

        $data['page'] = "front/search_result/quote_request";
        $this->page_name = 'Quote request';
        $data['js'] = array(
            'moment.min.js',
            'affiliate/req_quote.js',
            'custom/components-pickers.js',
            'custom/wizard.js'
        );

        $data['js_plugin'] = array(
            'bootstrap-fileinput/bootstrap-fileinput.js',
            'bootstrap-datepicker/js/bootstrap-datepicker.js',
        );

        $data['css_plugin'] = array(
            'bootstrap-fileinput/bootstrap-fileinput.css',
            'bootstrap-datepicker/css/datepicker.css'
        );

        $data['css'] = array();


        $data['init'] = array(
            'Req_quote.init()',
            'ComponentsPickers.init_datepicker()'
        );

        $this->load->view(FRONT_LAYOUT, $data);
    }

    function ShowBookPageCar()
    {

        $data['page'] = "front/search_result/book_car";
        $this->page_name = 'Quote request';
        $data['js'] = array(
            'moment.min.js',
            'affiliate/req_quote.js',
            'custom/components-pickers.js',
            'custom/wizard.js'
        );

        $data['js_plugin'] = array(
            'bootstrap-fileinput/bootstrap-fileinput.js',
            'bootstrap-datepicker/js/bootstrap-datepicker.js',
        );

        $data['css_plugin'] = array(
            'bootstrap-fileinput/bootstrap-fileinput.css',
            'bootstrap-datepicker/css/datepicker.css'
        );

        $data['css'] = array();


        $data['init'] = array(
            'Req_quote.init()',
            'ComponentsPickers.init_datepicker()'
        );

        $this->load->view(FRONT_LAYOUT, $data);
    }

    function ShowBookPageCruise()
    {

        $data['page'] = "front/search_result/book_cruise";
        $this->page_name = 'Quote request';
        $data['js'] = array(
            'moment.min.js',
            'affiliate/req_quote.js',
            'custom/components-pickers.js',
            'custom/wizard.js'
        );

        $data['js_plugin'] = array(
            'bootstrap-fileinput/bootstrap-fileinput.js',
            'bootstrap-datepicker/js/bootstrap-datepicker.js',
        );

        $data['css_plugin'] = array(
            'bootstrap-fileinput/bootstrap-fileinput.css',
            'bootstrap-datepicker/css/datepicker.css'
        );

        $data['css'] = array();


        $data['init'] = array(
            'Req_quote.init()',
            'ComponentsPickers.init_datepicker()'
        );

        $this->load->view(FRONT_LAYOUT, $data);
    }

    function get_cities($id)
    {
        $this->db->where('country_id', $id);
        $res = $this->db->get('states')->result();
        foreach ($res as $re) {
            echo '<option value="' . $re->name . '">' . $re->name . '</option>';
        }
    }

    function get_cities_id($id)
    {
        $this->db->order_by('name', 'asc')->where('country_id', $id);
        $res = $this->db->get('states')->result();

        $r1 = '';
        foreach ($res as $re) {
            $r1 .= '<option value="' . $re->name . '">' . $re->name . '</option>';
        }
        return $r1;
    }

    function get_all_contries()
    {
        return $this->db->order_by('name', 'asc')->get('countries')->result();
    }

    function ShowQuotePage()

    {


        if (!isUserLogin()) {

            redirect("/");

        }

        if ($this->input->get('stp', TRUE))

            $this->session->set_userdata(array('stp' => $this->input->get('stp', TRUE)));

        if (!empty($this->input->post())) {

            if ($this->input->get('stp', TRUE) == 'f') {

                if (isset($_SESSION['coupons'])) {

                    unset($_SESSION['coupons']);

                    unset($_SESSION['usedCoupon']);

                }

                $this->session->unset_userdata('compl');

                $book_fStepData = array(

                    'hotelId' => $this->input->post('hotelId'),

                    'hotelType' => $this->input->post('hotelType'),

                    'room_type_category' => $this->input->post('room_type_category'),

                    'locationName' => $this->input->post('locationName'),

                    'from' => $this->input->post('from'),

                    'top' => $this->input->post('top'),

                    'locationId' => $this->input->post('locationId'),

                    'adultsNo' => $this->input->post('adultsNo'),

                    'uniqueSession' => $this->input->post('uniqueSession'),

                    'childrenNo' => $this->input->post('childrenNo'),

                    'hotelName' => $this->input->post('hotelName'),

                    'starRating' => $this->input->post('starRating'),

                    'roomTypeId' => $this->input->post('roomTypeId'),

                    'roomId' => $this->input->post('roomId'),

                    'avalibilty' => $this->input->post('avalibilty'),

                    'supp' => $this->input->post('sup'),

                    'minPrice' => $this->input->post('minPrice'),

                    'all_info' => $_POST

                );
//echo '<pre>';print_r($book_fStepData);exit;
                $_SESSION['input_data_book_step1'] = $book_fStepData;

            } elseif ($this->input->get('stp', TRUE) === 's') {

                if (isset($_POST['coupons'])) {

                    $_SESSION['coupons'] = $_POST['coupons'];

                }

                $_SESSION['input_data_book_step2'] = $this->input->post('data');

            } elseif ($this->input->get('stp', TRUE) === 'th') {
                $_SESSION['input_data_book_card'] = $this->input->post('data');
                //$_SESSION['stripeToken'] = $this->input->post('stripeToken');
                $_SESSION['stripeSource'] = $this->input->post('source');
                $_SESSION['stripe3DSecure'] = $this->input->post('three_d_secure');

            } elseif ($this->input->get('stp', TRUE) === 'cnfrm') {

                if ($this->session->userdata('compl')) {

                    redirect(base_url(), 'refresh');

                    exit();

                }

                $terms = $this->input->post('terms');

                if (!empty($terms) && $terms == 1) {


                } else {

                    redirect(base_url(), 'refresh');

                    exit();

                }

            }

        }

        $user = $this->session->userdata['valid_user']['id'];

        $userChoice = $this->session->all_userdata();
        //echo '<pre>';print_r($userChoice);exit;
        if ($userChoice['stp'] == 'f') {

            $get_data_book_steps = $_SESSION['input_data_book_step1']; //$userChoice['input_data_book_step1'];

            $hotelId = $get_data_book_steps['hotelId']; //$this->input->get('hotelId', TRUE);

            $roomId = $get_data_book_steps['roomId']; //$this->input->get('roomId', TRUE);

            $roomTypeId = $get_data_book_steps['roomTypeId']; //$this->input->get('roomTypeId', TRUE);

            $data_for_one_hotel_xml = $userChoice['input_data_for_one_hotel'];

            $uniqueSession = $get_data_book_steps['uniqueSession'];

            $data_roomInfoStr = $_SESSION['roomInfoStr'];
            $hotelDetails = array();
            //echo '<pre>';print_r($get_data_book_steps);exit;
            if ($get_data_book_steps['hotelType'] == 'TO') {
                $hotelSearchAPIResponseProcessObj = new HotelSearchAPIResponseProcess();
                $hotelDetails = $hotelSearchAPIResponseProcessObj->GetOneHotelDetailsByHotelId($hotelId, $data_for_one_hotel_xml, $roomTypeId);
                $hotelDetails = $this->tourico_hotel_price_multiplier->ProcessPriceOfHotelDetail($hotelDetails);
            } else if ($get_data_book_steps['hotelType'] == 'TL') {
                /*$travellensda = new TravellandaAPICore;
                $hotelDetails = $travellensda->GetHotelDetails(array($hotelId));*/
                $hotel = $this->db->where('session_id', $uniqueSession)->where('hotelId', $hotelId)->get('temp_hotels_data')->result_array();
//                var_dump($uniqueSession);var_dump($hotelId);exit;

                $hotelDetails = convetsearlizeInarrayHotels($hotel);
                $hotelDetails = getRoomDetailByRoomTypeId($hotelDetails, $roomTypeId);
                //$roomId = 1;

            }
            // check is search expired or not
            // $this->checkTLRoomAvailability($hotelDetails[0]['room_type_list']);

            // echo '<pre>';print_r($hotelDetails);exit;
            $data = array();

            $data['number_of_rooms'] = $data_for_one_hotel_xml['number_of_rooms'];

            $data['room_type_category'] = $_SESSION['input_data_book_step1']['room_type_category'];

            $data['room_id'] = $roomId;

            $data['room_type_id'] = $roomTypeId;

            $data['hotel_id'] = $hotelId;

            $data['hotel_list'] = $hotelDetails;

            if (isset($hotelDetails[0]['voucher_remarks'][0])) {

                $_SESSION['input_data_book_step1']['all_info']['voucher_remarks'] = $hotelDetails[0]['voucher_remarks'][0];

            }

            $data['hotel_name'] = $hotelDetails['hotelName'];

            $data['check_in'] = $data_for_one_hotel_xml['check_in_date'];

            $data['check_out'] = $data_for_one_hotel_xml['check_out_date'];

            $data['adult_array'] = $data_for_one_hotel_xml['adult_number'];

            $data['child_array'] = $data_for_one_hotel_xml['child_number'];

            $data['room_no'] = $userChoice['room_no'];

            $data['destination_id'] = $userChoice['destination_id'];

            $data['destination_name'] = $userChoice['destination_name'];

            $data['star_level'] = $userChoice['star_level'];

            $data['supp'] = $get_data_book_steps['supp'];

            $res = $this->db->where('int_glcode', $user)->get('dz_user')->result();

            $data['user_profile'] = $res[0];

            $data['page_step'] = 1;

        } elseif ($userChoice['stp'] == 's') {

            $get_data_book_steps = $_SESSION['input_data_book_step1']; //$userChoice['input_data_book_step1'];

            $get_data_book_step2 = $_SESSION['input_data_book_step2']; //$userChoice['input_data_book_step2'];

            //Booking page step 2

            $hotelId = $get_data_book_steps['hotelId']; //$this->input->get('hotelId', TRUE);

            $roomId = $get_data_book_steps['roomId']; //$this->input->get('roomId', TRUE);

            $roomTypeId = $get_data_book_steps['roomTypeId']; //$this->input->get('roomTypeId', TRUE);

            $data_for_one_hotel_xml = $userChoice['input_data_for_one_hotel'];

            $data_roomInfoStr = $_SESSION['roomInfoStr'];

            /*$hotelSearchAPIResponseProcessObj = new HotelSearchAPIResponseProcess();
            $hotelDetails = $hotelSearchAPIResponseProcessObj->GetOneHotelDetailsByHotelId($hotelId, $data_for_one_hotel_xml);
            $hotelDetails = $this->tourico_hotel_price_multiplier->ProcessPriceOfHotel($hotelDetails);*/
            $uniqueSession = $get_data_book_steps['uniqueSession'];
            if ($get_data_book_steps['hotelType'] == 'TO') {
                $hotelSearchAPIResponseProcessObj = new HotelSearchAPIResponseProcess();
                $hotelDetails = $hotelSearchAPIResponseProcessObj->GetOneHotelDetailsByHotelId($hotelId, $data_for_one_hotel_xml, $roomTypeId);
                $hotelDetails = $this->tourico_hotel_price_multiplier->ProcessPriceOfHotelDetail($hotelDetails);
            } else if ($get_data_book_steps['hotelType'] == 'TL') {
                /*$travellensda = new TravellandaAPICore;
                $hotelDetails = $travellensda->GetHotelDetails(array($hotelId));*/
                $hotel = $this->db->where('session_id', $uniqueSession)->where('hotelId', $hotelId)->get('temp_hotels_data')->result_array();
                $hotelDetails = convetsearlizeInarrayHotels($hotel);
                $hotelDetails = getRoomDetailByRoomTypeId($hotelDetails, $roomTypeId);
                //$roomId = 1;
                //echo '<pre>';print_r($hotelDetails);exit;

            }
            //echo '<pre>';print_r($hotelDetails);exit;

            $data = array();

            //user choosen data

            $data['total_room_cost'] = $get_data_book_step2['total_room_cost'];

            $data['total_supp_amount'] = $get_data_book_step2['total_supp_amount'];

            $data['total_tax_fees'] = $get_data_book_step2['total_tax_fees'];

            //7-12-2016

            $data['included_supp_fees'] = $get_data_book_step2['included_supp_fees'];

            $data['total_price_html'] = $get_data_book_step2['total_price_html'];

            $data['number_of_rooms'] = $data_for_one_hotel_xml['number_of_rooms'];

            $data['room_type_category'] = $_SESSION['input_data_book_step1']['room_type_category'];

            $data['room_id'] = $roomId;

            $data['room_type_id'] = $roomTypeId;

            $data['hotel_id'] = $hotelId;

            $data['hotel_list'] = $hotelDetails;

            $data['hotel_name'] = $hotelDetails['hotelName'];

            $data['check_in'] = $data_for_one_hotel_xml['check_in_date'];

            $data['check_out'] = $data_for_one_hotel_xml['check_out_date'];

            $data['adult_array'] = $data_for_one_hotel_xml['adult_number'];

            $data['child_array'] = $data_for_one_hotel_xml['child_number'];

            $data['room_no'] = $userChoice['room_no'];

            $data['destination_id'] = $userChoice['destination_id'];

            $data['destination_name'] = $userChoice['destination_name'];

            $data['star_level'] = $userChoice['star_level'];

            $data['supp'] = $get_data_book_steps['supp'];

            $data['page_step'] = 2;

            $data['countries'] = $this->get_all_contries();

            $data['states'] = $this->get_cities_id(192);


        } elseif ($userChoice['stp'] == 'th') {

            $get_data_book_steps = $_SESSION['input_data_book_step1']; //$userChoice['input_data_book_step1'];

            $get_data_book_step2 = $_SESSION['input_data_book_step2']; //$userChoice['input_data_book_step2'];

            $allbreakdown = array();

            foreach ($get_data_book_step2['roomInfo'] as $breakd) {

                $brarr = explode('|', $breakd['optradio']);

                $breakdown = explode('-', $brarr[4]);

                unset($breakdown[count($breakdown) - 1]);

                $allbreakdown[] = array(

                    "breakdown" => $breakdown,

                    "tax" => $brarr[5]

                );


                //$breakdown;

            }


            //Booking page step 2

            $hotelId = $get_data_book_steps['hotelId']; //$this->input->get('hotelId', TRUE);

            $roomId = $get_data_book_steps['roomId']; //$this->input->get('roomId', TRUE);

            $roomTypeId = $get_data_book_steps['roomTypeId']; //$this->input->get('roomTypeId', TRUE);

            $data_for_one_hotel_xml = $userChoice['input_data_for_one_hotel'];

            $data_roomInfoStr = $_SESSION['roomInfoStr'];

            /*$hotelSearchAPIResponseProcessObj = new HotelSearchAPIResponseProcess();

            $hotelDetails = $hotelSearchAPIResponseProcessObj->GetOneHotelDetailsByHotelId($hotelId, $data_for_one_hotel_xml);

            $hotelDetails = $this->tourico_hotel_price_multiplier->ProcessPriceOfHotel($hotelDetails);*/

            $uniqueSession = $get_data_book_steps['uniqueSession'];
            if ($get_data_book_steps['hotelType'] == 'TO') {
                $hotelSearchAPIResponseProcessObj = new HotelSearchAPIResponseProcess();
                $hotelDetails = $hotelSearchAPIResponseProcessObj->GetOneHotelDetailsByHotelId($hotelId, $data_for_one_hotel_xml, $roomTypeId);
                $hotelDetails = $this->tourico_hotel_price_multiplier->ProcessPriceOfHotelDetail($hotelDetails);
            } else if ($get_data_book_steps['hotelType'] == 'TL') {
                /*$travellensda = new TravellandaAPICore;
                $hotelDetails = $travellensda->GetHotelDetails(array($hotelId));*/
                $hotel = $this->db->where('session_id', $uniqueSession)->where('hotelId', $hotelId)->get('temp_hotels_data')->result_array();
                $hotelDetails = convetsearlizeInarrayHotels($hotel);
                $hotelDetails = getRoomDetailByRoomTypeId($hotelDetails, $roomTypeId);
                //$roomId = 1;
                //echo '<pre>';print_r($hotelDetails);exit;

            }

            $data = array();

            $dateObj = DateTime::createFromFormat('m/d/Y', $data_for_one_hotel_xml['check_in_date']);

            $check_in = $dateObj->format('Y-m-d');

            $dateObj = DateTime::createFromFormat('m/d/Y', $data_for_one_hotel_xml['check_out_date']);

            $check_out = $dateObj->format('Y-m-d');

            $reservationApiProcessObj = new ReservationAPIProcess();

            $cancellation_policies_list = $reservationApiProcessObj->GetCancellationPoliciesBeforeReservation($hotelId, $roomTypeId, $check_in, $check_out);

            //user choosen data by pass

            $data['breakdown'] = $allbreakdown;

            $data['step1_data'] = $get_data_book_steps;

            $data['step2_data'] = $get_data_book_step2;

            $data['card_data'] = $_SESSION['input_data_book_card']; //$userChoice['input_data_book_card'];

            $data['total_room_cost'] = $get_data_book_step2['total_room_cost'];

            $data['total_supp_amount'] = $get_data_book_step2['total_supp_amount'];

            $data['total_tax_fees'] = $get_data_book_step2['total_tax_fees'];

            //7-12-2016

            $data['included_supp_fees'] = $get_data_book_step2['included_supp_fees'];

            $data['total_price_html'] = $get_data_book_step2['total_price_html'];

            $data['number_of_rooms'] = $data_for_one_hotel_xml['number_of_rooms'];

            $data['room_type_category'] = $_SESSION['input_data_book_step1']['room_type_category'];

            $data['room_id'] = $roomId;

            $data['room_type_id'] = $roomTypeId;

            $data['hotel_id'] = $hotelId;

            $data['hotel_list'] = $hotelDetails;

            $data['hotel_name'] = $hotelDetails['hotelName'];

            $data['check_in'] = $data_for_one_hotel_xml['check_in_date'];

            $data['check_out'] = $data_for_one_hotel_xml['check_out_date'];

            $data['adult_array'] = $data_for_one_hotel_xml['adult_number'];

            $data['child_array'] = $data_for_one_hotel_xml['child_number'];

            $data['room_no'] = $userChoice['room_no'];

            $data['destination_id'] = $userChoice['destination_id'];

            $data['destination_name'] = $userChoice['destination_name'];

            $data['star_level'] = $userChoice['star_level'];

            $data['supp'] = $get_data_book_steps['supp'];

            $data['cancellation_policies_list'] = $cancellation_policies_list;

            $data['page_step'] = 3;

            $_SESSION['cancellation_policies_list'] = $cancellation_policies_list;

            $_SESSION['supp'] = $data['supp'];

        } elseif ($userChoice['stp'] == 'cnfrm') {

            if (empty($_SESSION['input_data_book_step1']) || empty($_SESSION['input_data_book_step2'])) {
                redirect("/");
            }

            $_SESSION['stripe3DSource'] = $this->input->get('source_token_3d');
            $data['countries'] = $this->get_all_contries();
            $data['states'] = $this->get_cities_id(192);
            $get_data_book_steps = $_SESSION['input_data_book_step1']; //$userChoice['input_data_book_step1'];
            $get_data_book_step2 = $_SESSION['input_data_book_step2']; //$userChoice['input_data_book_step2'];
            //echo '<pre>';print_r($get_data_book_step2);exit;
            //Booking page step 2
            $hotelId = $get_data_book_steps['hotelId']; //$this->input->get('hotelId', TRUE);
            $roomId = $get_data_book_steps['roomId']; //$this->input->get('roomId', TRUE);
            $roomTypeId = $get_data_book_steps['roomTypeId']; //$this->input->get('roomTypeId', TRUE);
            $data_for_one_hotel_xml = $userChoice['input_data_for_one_hotel'];
            $data_roomInfoStr = $_SESSION['roomInfoStr'];
            /*$hotelSearchAPIResponseProcessObj = new HotelSearchAPIResponseProcess();

            $hotelDetails = $hotelSearchAPIResponseProcessObj->GetOneHotelDetailsByHotelId($hotelId, $data_for_one_hotel_xml);

            $hotelDetails = $this->tourico_hotel_price_multiplier->ProcessPriceOfHotel($hotelDetails);*/
            $uniqueSession = $get_data_book_steps['uniqueSession'];
            $hotelDetails = array();
            if ($get_data_book_steps['hotelType'] == 'TO') {
                $hotelSearchAPIResponseProcessObj = new HotelSearchAPIResponseProcess();
                $hotelDetails = $hotelSearchAPIResponseProcessObj->GetOneHotelDetailsByHotelId($hotelId, $data_for_one_hotel_xml, $roomTypeId);
                $hotelDetails = $this->tourico_hotel_price_multiplier->ProcessPriceOfHotelDetail($hotelDetails);
            } else if ($get_data_book_steps['hotelType'] == 'TL') {
                /*$travellensda = new TravellandaAPICore;
                $hotelDetails = $travellensda->GetHotelDetails(array($hotelId));*/
                $hotel = $this->db->where('session_id', $uniqueSession)->where('hotelId', $hotelId)->get('temp_hotels_data')->result_array();
                $hotelDetails = convetsearlizeInarrayHotels($hotel);
                $hotelDetails = getRoomDetailByRoomTypeId($hotelDetails, $roomTypeId);
                //$roomId = 1;
                // echo '<pre>';print_r($data_for_one_hotel_xml);exit;

            }

            //user choosen data

            $data['total_room_cost'] = $get_data_book_step2['total_room_cost'];

            $data['total_supp_amount'] = $get_data_book_step2['total_supp_amount'];

            $data['total_tax_fees'] = $get_data_book_step2['total_tax_fees'];

            //7-12-2016

            $data['included_supp_fees'] = $get_data_book_step2['included_supp_fees'];

            $data['total_price_html'] = $get_data_book_step2['total_price_html'];

            $data['number_of_rooms'] = $data_for_one_hotel_xml['number_of_rooms'];

            $data['room_id'] = $roomId;

            $data['room_type_id'] = $roomTypeId;

            $data['hotel_id'] = $hotelId;

            $data['hotel_list'] = $hotelDetails;

            //echo '<pre>';print_r($get_data_book_step2);exit;

            $data['hotel_name'] = $hotelDetails['hotelName'];

            $data['check_in'] = $data_for_one_hotel_xml['check_in_date'];

            $data['check_out'] = $data_for_one_hotel_xml['check_out_date'];

            $data['adult_array'] = $data_for_one_hotel_xml['adult_number'];

            $data['child_array'] = $data_for_one_hotel_xml['child_number'];

            $data['room_no'] = $userChoice['room_no'];

            $data['destination_id'] = $userChoice['destination_id'];

            $data['destination_name'] = $userChoice['destination_name'];

            $data['star_level'] = $userChoice['star_level'];

            $data['supp'] = $get_data_book_steps['supp'];

            $data['page_step'] = 4;

            //Booking page step 2

            $hotelId = $get_data_book_steps['hotelId']; //$this->input->get('hotelId', TRUE);

            $roomId = $get_data_book_steps['roomId']; //$this->input->get('roomId', TRUE);

            $roomTypeId = $get_data_book_steps['roomTypeId']; //$this->input->get('roomTypeId', TRUE);

            $data_roomInfoStr = $_SESSION['roomInfoStr'];


            // booking for TL-K
            if ($get_data_book_steps['hotelType'] == 'TL') {
                // check is room still available
                $this->checkTLRoomAvailability($hotelDetails[0]['room_type_list']);
                $roomNo_count = $data_for_one_hotel_xml['number_of_rooms'];
                $z = 1;
                $total_price = 0;
                $total_actual_price = 0;
                for ($i = 0; $i < $roomNo_count; $i++) {
                    $prices = explode('|', $get_data_book_step2['roomInfo'][$z]['optradio']);
                    $total_price = $total_price + $prices[0];
                    $total_actual_price = $total_actual_price + $prices[1];
                    $z++;
                }
                // get card info and charge user
                $get_data_card = $_SESSION['input_data_book_card'];
                if (isset($_SESSION['coupons'])) {
                    $chres = $this->db->where('int_glcode', $_SESSION['coupons'])->get('dz_coupons')->result();
                    if (count($chres) > 0) {
                        $coup = $chres[0];
                        $fkuser = $this->session->userdata['valid_user']['id'];
                        if ($coup->fk_user == $fkuser && $coup->var_status == 'A') {
                            $htmlAmount = $data['total_price_html'];
                            $htmlAmount = substr($htmlAmount, 1, strlen($htmlAmount));
                            $total_price = $htmlAmount;
                        }
                    }

                }
                $price = $total_price * 100;
                //$token = $_SESSION['stripeToken'];
                $stripeSource = $_SESSION['stripe3DSource'];
                //$description = ' Account Email : '.(isset($this->session->userdata['valid_user']['var_email']) ? $this->session->userdata['valid_user']['var_email'] : '');
                $description = $hotelDetails['hotelName'] . ' for ' . $data_for_one_hotel_xml['check_in_date'] . ' / ' . $data_for_one_hotel_xml['check_out_date'];
                // Charge the user's card:
                $chargeData = array(
                    "amount" => $price,
                    "currency" => "usd",
                    "description" => $description,
                    "source" => $stripeSource,
                    "receipt_email" => (isset($this->session->userdata['valid_user']['var_email']) ? $this->session->userdata['valid_user']['var_email'] : '')
                );
                //echo '<pre>';print_r($chargeData);echo '</pre>';
                $payResponse = $this->stripePayment($chargeData);
                if ($payResponse['success'] == 1) {
                    $charge = $payResponse['charge_info']->id;
                    $stripePaymetarray = serialize($payResponse['charge_info']->__toArray(true));
                    $resData = $this->setUpTLBooking($hotelDetails[0]['room_type_list'], $data_for_one_hotel_xml, $get_data_book_step2);
                    //echo '<pre>';print_r($resData);echo '</pre>';exit;
                    if (isset($resData['Body']['Error'])) {
                        $data['error'] = $resData['Body']['ErrorText'];
                        $data['card_data'] = $get_data_card;
                        $data['page_step'] = 1;
                        $_SESSION['error'] = $resData['Body']['ErrorText'];
                        redirect('request_quote?stp=f');
                    } else {
                        $hDetail = $hotelDetails;
                        $hDetail[0]['check_in_date'] = $data_for_one_hotel_xml['check_in_date'];
                        $hDetail[0]['check_out_date'] = $data_for_one_hotel_xml['check_out_date'];
                        $total_adult = 0;
                        //echo '<pre>';print_r($data_for_one_hotel_xml);echo '</pre>';exit;
                        for ($i = 0; $i < count($data_for_one_hotel_xml['adult_number']); $i++) {
                            $total_adult = $total_adult + $data_for_one_hotel_xml['adult_number'][$i];
                        }
                        $total_child = 0;
                        for ($i = 0; $i < count($data_for_one_hotel_xml['child_number']); $i++) {
                            $total_child = $total_child + $data_for_one_hotel_xml['child_number'][$i];
                        }
                        $hDetail[0]['total_child'] = $total_child;
                        $hDetail[0]['total_adult'] = $total_adult;
                        $res = $this->quote_model->AddQuoteFromTLAPI($data_for_one_hotel_xml, $get_data_book_step2,$hDetail[0], $resData, $charge, $stripePaymetarray);
                        $this->session->set_userdata('compl', true);
                        $data['success'] = "Good job! Your reservation has been successfully made and we forwarded it to the hotel.";
                        $data['res_id'] = $res['id'];
                        $data['usedCoupon'] = $res['usedCoupon'];
                    }
                } else {
                    $data['error'] = $payResponse['details'];
                    $data['card_data'] = $get_data_card;
                    $data['page_step'] = 2;
                }

            } else {
                //availabilty and prices check

                $hotelReservationAPIProcessObj = new ReservationAPIProcess();

                $getPriceAvail = $hotelReservationAPIProcessObj->GetPriceAndAvail($hotelId, $data_roomInfoStr, $data_for_one_hotel_xml['check_in_date'], $data_for_one_hotel_xml['check_out_date'], $userChoice['star_level'], 0, $roomId, $roomTypeId);

                if ($getPriceAvail['error'] == 0) {

                    $room_info_str = "";

                    $childAgeStr = "";

                    $roomInfoStr = "";

                    $roomNo_count = $data_for_one_hotel_xml['number_of_rooms'];

                    $child_age_index = 0;

                    $z = 1;

                    $total_price = 0;

                    $total_actual_price = 0;

                    $addSupp = array();

                    for ($i = 0; $i < $roomNo_count; $i++) {

                        $prices = explode('|', $get_data_book_step2['roomInfo'][$z]['optradio']);

                        $total_price = $total_price + $prices[0];

                        $total_actual_price = $total_actual_price + $prices[1];

                        $supnum = $prices[2];

                        for ($j = $child_age_index; $j < (intval($data_for_one_hotel_xml['child_number'][$i]) + $child_age_index); $j++) {


                            $childAgeStr .= <<<EOM

                                        <hot1:ChildAge age="{$data_for_one_hotel_xml['child_age'][$j]}"/>

EOM;

                        }

                        $child_age_index = $j;

                        $supp = $_SESSION['Supplemtnts'];

                        $supp = $supp[$supnum];

                        $suppStr = '';

                        if (!empty($supp)) {

                            foreach ($supp as $key => $su1) {

                                if ($su1['suppIsMandatory'] == 'false') {

                                    unset($supp[$key]);

                                }

                            }

                            if (count($supp) > 0) {

                                $suppStr .= <<<EOM

                        <hot1:SelectedSupplements>

EOM;

                                foreach ($supp as $su) {

                                    $suppArr = $su;

                                    $suppId = $suppArr['suppId'];

                                    if ($suppArr['suppChargeType'] == 'Addition') {

                                        if (!in_array($suppId, $addSupp)) {

                                            $addSupp[] = $suppId;

                                            $total_actual_price = $total_actual_price + $suppArr['price'];

                                        }

                                    }

                                    $supTotalPrice = $suppArr['price'];

                                    $supptType = $suppArr['supptType'];

                                    $SuppAgeGroup = $suppArr['SuppAgeGroup'];

                                    $supaggr = '';

                                    if (count($SuppAgeGroup) > 0) {

                                        $supaggr .= <<<EOM

                            <hot1:SupAgeGroup>

EOM;

                                        foreach ($SuppAgeGroup as $ageGrop) {

                                            $suppFrom = $ageGrop['@attributes']['suppFrom'];

                                            $suppTo = $ageGrop['@attributes']['suppTo'];

                                            $suppQuantity = $ageGrop['@attributes']['suppQuantity'];

                                            if ($suppFrom == 1 && $suppTo == 7) {

                                                $suppQuantity = $data_for_one_hotel_xml['child_number'][$i];

                                            } else {

                                                $suppQuantity = $data_for_one_hotel_xml['adult_number'][$i];

                                            }

                                            if ($suppQuantity == 0)

                                                continue;

                                            $suppPrice = $ageGrop['@attributes']['suppPrice'];

                                            $supaggr .= <<<EOM

                            <hot1:SuppAges suppFrom="{$suppFrom}" suppTo="{$suppTo}" suppQuantity="{$suppQuantity}" suppPrice="{$suppPrice}"/>

EOM;


                                        }

                                        $supaggr .= <<<EOM

                            </hot1:SupAgeGroup>

EOM;

                                    }


                                    $suppStr .= <<<EOM

                            <hot1:SupplementInfo suppId="{$suppId}" supTotalPrice="{$supTotalPrice}" suppType="{$supptType}">{$supaggr}</hot1:SupplementInfo>

EOM;

                                }

                                $suppStr .= <<<EOM

                        </hot1:SelectedSupplements>

EOM;


                            }


                        }

                        $boardbasstr = "";

                        if (isset($get_data_book_step2['roomInfo'][$z]['boardbase'])) {

                            $ordnum = $prices[3];

                            $boardba = explode('|', $get_data_book_step2['roomInfo'][$z]['boardbase'][$ordnum]);

                            $total_actual_price = $total_actual_price + $boardba[1];

                            $bbid = $boardba[0];

                            $bbprice = $boardba[1];

                            $boardbasstr .= <<<EOM

                            <hot1:SelectedBoardBase>

								<hot1:Id>{$bbid}</hot1:Id>

								<hot1:Price>{$bbprice}</hot1:Price>

							</hot1:SelectedBoardBase>

EOM;

                        }

                        $roomInfoStr .= <<<EOM

               <hot1:RoomReserveInfo>

                  <hot1:RoomId>{$get_data_book_steps['roomId']}</hot1:RoomId>

                  <hot1:ContactPassenger>

                     <hot1:FirstName>{$get_data_book_step2['roomInfo'][$z]['firstname']}</hot1:FirstName>

                     <hot1:MiddleName></hot1:MiddleName>

                     <hot1:LastName>{$get_data_book_step2['roomInfo'][$z]['lastname']}</hot1:LastName>

                     <hot1:HomePhone>{$get_data_book_step2['roomInfo'][$z]['phone']}</hot1:HomePhone>

                     <hot1:MobilePhone>{$get_data_book_step2['roomInfo'][$z]['phone']}</hot1:MobilePhone>

                  </hot1:ContactPassenger>

				  {$boardbasstr}

                  {$suppStr}

                  <hot1:Note>{$get_data_book_step2['roomInfo'][$z]['splReq']}</hot1:Note>

                  <hot1:AdultNum>{$data_for_one_hotel_xml['adult_number'][$i]}</hot1:AdultNum>

                  <hot1:ChildNum>{$data_for_one_hotel_xml['child_number'][$i]}</hot1:ChildNum>

                  <hot1:ChildAges>

				 {$childAgeStr}

                  </hot1:ChildAges>

               </hot1:RoomReserveInfo>

EOM;


                        $childAgeStr = "";

                        $z++;

                    }

                    $get_data_card = $_SESSION['input_data_book_card'];

                    if (isset($_SESSION['coupons'])) {

                        $chres = $this->db->where('int_glcode', $_SESSION['coupons'])->get('dz_coupons')->result();

                        if (count($chres) > 0) {

                            $coup = $chres[0];

                            $fkuser = $this->session->userdata['valid_user']['id'];

                            if ($coup->fk_user == $fkuser && $coup->var_status == 'A') {

                                $htmlAmount = $data['total_price_html'];

                                $htmlAmount = substr($htmlAmount, 1, strlen($htmlAmount));

                                $total_price = $htmlAmount;

                            }

                        }

                    }

                    $price = $total_price * 100;

                    //$token = $_SESSION['stripeToken'];
                    $stripeSource = $_SESSION['stripe3DSource'];

                    //$description = ' Account Email : '.(isset($this->session->userdata['valid_user']['var_email']) ? $this->session->userdata['valid_user']['var_email'] : '');
                    $description = $hotelDetails['hotelName'] . ' for ' . $data_for_one_hotel_xml['check_in_date'] . ' / ' . $data_for_one_hotel_xml['check_out_date'];

                    // Charge the user's card:
                    $chargeData = array(
                        "amount" => $price,
                        "currency" => "usd",
                        "description" => $description,
                        "source" => $stripeSource,
                        "receipt_email" => (isset($this->session->userdata['valid_user']['var_email']) ? $this->session->userdata['valid_user']['var_email'] : '')
                    );

                    //echo '<pre>';print_r($chargeData);echo '</pre>';

                    $payResponse = $this->stripePayment($chargeData);

                    if ($payResponse['success'] == 1) {

                        $charge = $payResponse['charge_info']->id;

                        $stripePaymetarray = serialize($payResponse['charge_info']->__toArray(true));

                        $hotelBookAPIResponseProcessObj = new ReservationAPIProcess();

                        //$bookreps = $hotelBookAPIResponseProcessObj->HotelBookProcess($get_data_book_steps['hotelId'],$get_data_book_steps['roomTypeId'],$data_for_one_hotel_xml['check_in_date'], $data_for_one_hotel_xml['check_out_date'],$roomInfoStr, $get_data_card['phone'],$total_actual_price,$get_data_book_step2['roomInfo'][1]['email']);

                        $bookreps = $hotelBookAPIResponseProcessObj->HotelBookProcess($get_data_book_steps['hotelId'], $get_data_book_steps['roomTypeId'], $data_for_one_hotel_xml['check_in_date'], $data_for_one_hotel_xml['check_out_date'], $roomInfoStr, $get_data_card['phone'], $total_actual_price, "customer@customer.com");
                        //echo '<pre>';print_r($bookreps);echo '</pre>';exit;
                        if (isset($bookreps['sFault'])) {

                            $data['error'] = $bookreps['sFault']['faultstring'];

                            $data['card_data'] = $get_data_card;

                            $data['page_step'] = 1;

                            $_SESSION['error'] = $bookreps['sFault']['faultstring'];

                            redirect('request_quote?stp=f');

                        } else {

                            $this->session->set_userdata('compl', true);

                            $res = $this->quote_model->AddQuoteFromTouricoAPI($bookreps, $charge, $stripePaymetarray);

                            $data['success'] = "Good job! Your reservation has been successfully made and we forwarded it to the hotel.";

                            $data['res_id'] = $res['id'];

                            $data['usedCoupon'] = $res['usedCoupon'];

                        }


                    } else {

                        $data['error'] = $payResponse['details'];

                        $data['card_data'] = $get_data_card;

                        $data['page_step'] = 2;

                    }

                } else {

                    $data['error'] = "<b>Your selected room type is no longer available. Please go back and select a different room. Thank you!</b>";

                    $data['page_step'] = 1;

                    $_SESSION['error'] = $data['error'];

                    redirect('request_quote?stp=f');

                }
            }

        } else {

            redirect(base_url(), 'refresh');

            exit();

        }

        $data['page'] = "front/search_result/quote_request";

        $this->page_name = 'Quote request';

        $data['js'] = array(

            'moment.min.js',

            'affiliate/req_quote.js',

            'custom/components-pickers.js',

        );

        $data['js_plugin'] = array(

            'jquery-1.10.2.min.js',

            'bootstrap-fileinput/bootstrap-fileinput.js',

            'bootstrap-datepicker/js/bootstrap-datepicker.js',

            'bsStarRating/star-rating.min.js'

        );

        $data['css_plugin'] = array(

            'bootstrap-fileinput/bootstrap-fileinput.css',

            'bootstrap-datepicker/css/datepicker.css',

            'bsStarRating/star-rating.min.css'

        );

        $data['css'] = array();

        $data['init'] = array(

            'Req_quote.init()',

            'ComponentsPickers.init_datepicker()'

        );

        if (isset($_SESSION['coupons'])) {

            $data['fcoupon'] = $_SESSION['coupons'];

        } else {

            $data['fcoupon'] = '';

        }

        $user = $this->session->userdata['valid_user']['id'];

        $this->db->where('var_status', 'A');

        $this->db->where('var_couponexpi >', date('Y-m-d'));

        $res = $this->db->where('fk_user', $user)->get('dz_coupons')->result_array();

        $data['input_data_book_step1'] = $_SESSION['input_data_book_step1'];
        $data['input_data_book_step2'] = $_SESSION['input_data_book_step2'];
        $data['input_data_book_card'] = $_SESSION['input_data_book_card'];

        $data['coupons'] = $res;

        if ($data['page_step'] == 4) {
            unset($_SESSION['input_data_book_step1']);
            unset($_SESSION['input_data_book_step2']);
            unset($_SESSION['input_data_book_card']);
            unset($_SESSION['stripe3DSource']);
            unset($_SESSION['stripe3DSecure']);
            unset($_SESSION['stripeSource']);

            redirect('/front/tourico_api/thankyou/' . $data['res_id']);
        }

        $this->load->view(FRONT_LAYOUT, $data);

    }

    function ViewCruiseDetails()
    {
        $data = array();
        $car_search_api_obj = new CarSearchAPIResponseProcess();
        $data['car_list'] = $car_search_api_obj->ParseCarSearchByCityXmlToArray("5751", "2015-12-12", "2015-12-13", "11", "11", 1, 13, 2);

        //$data['car_list'] = $car_lists;

        $data['page'] = "front/search_result/cruise_details";
        $this->page_name = 'Home';
        $data['js'] = array(
            'login.js',
            'moment.min.js',
            'front/home.js',
            'affiliate/req_quote.js',
            'custom/components-pickers.js',
            'custom/components-dropdowns.js',
            'front/jquery.bxslider.js',
            'custom/sResultC.js'
        );
        $data['js_plugin'] = array(
            'bootstrap-fileinput/bootstrap-fileinput.js',
            'checkincheckout/bootstrap-datepicker.js',
            'bootstrap-select/bootstrap-select.min.js',
            'select2/select2.min.js',
            'jquery.bxslider.min.js',
            'bsStarRating/star-rating.min.js'
        );
        $data['css_plugin'] = array(
            'bootstrap-fileinput/bootstrap-fileinput.css',
            'bootstrap-datepicker/css/datepicker.css',
            'bootstrap-select/bootstrap-select.min.css',
            'select2/select2.css',
            'select2/select2-metronic.css',
            'jquery-ui/jquery-ui.css',
            'jquery.bxslider.css',
            'bsStarRating/star-rating.min.css'
        );
        $data['css'] = array();


        $this->load->view(FRONT_LAYOUT, $data);
    }

    function ViewCruiseSearchResults()
    {

        $cruise_obj = new Destination_model();
        $cruise_list = $cruise_obj->GetCruiseSuggestionLists();
        $cruise_list_count = count($cruise_list);

        for ($i = 0; $i < $cruise_list_count; $i++) {
            if (0 == $cruise_list[$i]['parent_id']) {
                $cruise_list_parent[] = $cruise_list[$i];
            } else {
                $cruise_list_child[] = $cruise_list[$i];
            }
        }

        $cruiseDestinationId = $this->input->post('cruiseDestinationId', TRUE); // cruise detiantion id
        $cruiseDestinationName = $this->input->post('Destination', TRUE); // cruise destination name
        $cruiseDepartureYearMonth = $this->input->post('cruise_departure_month', TRUE); // cruise departure month
        $cruiseLengthRange = $this->input->post('cruise_length', TRUE); // cruise length
        $cruiseLine = $this->input->post('cruise_line', TRUE); // cruise line

        $fromDateObj = DateTime::createFromFormat('m/Y', $cruiseDepartureYearMonth);
        $cruiseDepartureYearMonth = $fromDateObj->format('Y-m');

        $cruise_lines_obj = new Tourico_Cruise();

        $cruise_obj = new ProcessCruiseAPI();
        $cruise_lists = $cruise_obj->SearchCruise($cruiseDestinationId, $cruiseDepartureYearMonth, $cruiseLengthRange);

        //var_dump($cruise_lists);
        $ship_info = $cruise_obj->GetShipDetail($cruise_lists[0]['ship_id']);

        $tourico_cruise_obj = new Tourico_Cruise();
        $cruise_lines = $tourico_cruise_obj->GetCruiseLines();

        $cruise_ports = $tourico_cruise_obj->GetCruisePorts();

        $cruise_list_count = count($cruise_lists);
        for ($cruise_list_index = 0; $cruise_list_index < $cruise_list_count; $cruise_list_index++) {
            $a_cruise = $cruise_lists[$cruise_list_index];

            $itenary_segment_list = $a_cruise['itinerary_segment_list'];
            $itinery_list_count = count($itenary_segment_list);
            for ($i = 0; $i < $itinery_list_count; $i++) {
                $a_itinery_list = $itenary_segment_list[$i];
                $port_id = $a_itinery_list['port_id'];

                $port_count = count($cruise_ports);
                for ($port_index = 0; $port_index < $port_count; $port_index++) {
                    $a_cruise_port = $cruise_ports[$port_index];

                    if ($port_id == $a_cruise_port['tourico_port_id']) {
                        $cruise_lists[$cruise_list_index]['itinerary_segment_list'][$i]['longitude'] = $a_cruise_port['port_longitude'];
                        $cruise_lists[$cruise_list_index]['itinerary_segment_list'][$i]['latitude'] = $a_cruise_port['port_latitude'];
                        $cruise_lists[$cruise_list_index]['itinerary_segment_list'][$i]['port_image'] = $a_cruise_port['port_image'];
                        $cruise_lists[$cruise_list_index]['itinerary_segment_list'][$i]['port_description'] = $a_cruise_port['port_description'];
                    }
                }
            }
        }

        $data = array();
        $data['cruise_lines'] = $cruise_lines;
        $data['cruise_lists'] = $cruise_lists;
        $data['cruise_list_parent'] = $cruise_list_parent;
        $data['cruise_list_child'] = $cruise_list_child;
        $data['cruise_departure_month_options'] = $this->GetCruiseDepartureMonthOptions();
        $data['cruise_length'] = $cruise_lines_obj->GetCruiseLength();


        $data['page'] = "front/search_result/cruise_search_results";
        $this->page_name = 'Home';
        $data['js'] = array(
            'login.js',
            'moment.min.js',
            'front/home.js',
            'affiliate/req_quote.js',
            'custom/components-pickers.js',
            'custom/components-dropdowns.js',
            'front/jquery.bxslider.js',
            'custom/sResultC.js'
        );
        $data['js_plugin'] = array(
            'bootstrap-fileinput/bootstrap-fileinput.js',
            'checkincheckout/bootstrap-datepicker.js',
            'bootstrap-select/bootstrap-select.min.js',
            'select2/select2.min.js',
            'jquery.bxslider.min.js',
            'bsStarRating/star-rating.min.js'
        );
        $data['css_plugin'] = array(
            'bootstrap-fileinput/bootstrap-fileinput.css',
            'bootstrap-datepicker/css/datepicker.css',
            'bootstrap-select/bootstrap-select.min.css',
            'select2/select2.css',
            'select2/select2-metronic.css',
            'jquery-ui/jquery-ui.css',
            'jquery.bxslider.css',
            'bsStarRating/star-rating.min.css'
        );
        $data['css'] = array();


        $this->load->view(FRONT_LAYOUT, $data);
    }

    function GetCarDetailsAjax()
    {
        $pickup_location_name = $this->input->post('pickupLocationName');
        $drop_off_location_name = $this->input->post('dropOfLocationName');
        $airport_city = $this->input->post('airportOrCity');
        $pickup_date = $this->input->post('pick_up_date');
        $pickup_hour = $this->input->post('pick_up_time');
        $drop_off_date = $this->input->post('drop_off_date');
        $drop_off_hour = $this->input->post('drop_off_time');

        //unnecessary parameters
        $transmission_type = $this->input->post('transmission_type');
        $vehicle_type = $this->input->post('vehicle_type');
        $rental_company = $this->input->post('rental_company');

        $result_arr = $this->destination_model->GetDestinationCodeByDestinationName($pickup_location_name);

        $pickup_station_id = array('pickup_station_id' => $result_arr['tourico_destination_id']);

        $this->session->set_userdata($pickup_station_id);
        $this->session->set_userdata($pickup_location_name);
        $this->session->set_userdata($drop_off_location_name);

        $result_arr = $this->destination_model->GetDestinationCodeByDestinationName($pickup_location_name);
        $drop_off_destination_id = $this->destination_model->GetDestinationCodeByDestinationName($pickup_location_name);

        $station_id = $pickup_station_id;

        if ("Airport" == $airport_city) {
            $car_search_api_obj = new CarSearchAPIResponseProcess();

            $dateObj = DateTime::createFromFormat('m/d/Y', $pickup_date);
            $pickup_date = $dateObj->format('Y-m-d');

            $dateObj = DateTime::createFromFormat('m/d/Y', $drop_off_date);
            $drop_off_date = $dateObj->format('Y-m-d');

            $pick_up_drop_off_times = array(
                'pickup_date' => $pickup_date,
                'pickup_hour' => $pickup_hour,
                'drop_off_date' => $drop_off_date,
                'drop_off_hour' => $drop_off_hour
            );

            $this->session->set_userdata($pick_up_drop_off_times);
            $this->session->set_userdata(
                array(
                    'pickup_location' => $pickup_location_name,
                    'drop_off_location' => $drop_off_location_name
                )
            );

            $vehicle_type = intval($vehicle_type);
            $rental_company = 0;
            $total_pax = 10;

            $data['car_list'] = $car_search_api_obj->ParseCarSearchByAirportXmlToArray(intval($station_id), $pickup_date, $drop_off_date, $pickup_hour, $drop_off_hour, $vehicle_type, $rental_company, $total_pax, $drive_country_code = "", $driver_age = 22, $drop_off = "");
        } else if ("City" == $airport_city) {
            $car_search_api_obj = new CarSearchAPIResponseProcess();

            $dateObj = DateTime::createFromFormat('m/d/Y', $pickup_date);
            $pickup_date = $dateObj->format('Y-m-d');

            $dateObj = DateTime::createFromFormat('m/d/Y', $drop_off_date);
            $drop_off_date = $dateObj->format('Y-m-d');

            $pick_up_drop_off_times = array(
                'pickup_date' => $pickup_date,
                'pickup_hour' => $pickup_hour,
                'drop_off_date' => $drop_off_date,
                'drop_off_hour' => $drop_off_hour
            );

            $this->session->set_userdata($pick_up_drop_off_times);
            $this->session->set_userdata(
                array(
                    'pickup_location' => $pickup_location_name,
                    'drop_off_location' => $drop_off_location_name
                )
            );

            $vehicle_type = intval($vehicle_type);
            $rental_company = 0;
            $total_pax = 10;

            //$pickup_station_id = 5751;
            $data['car_list'] = $car_search_api_obj->ParseCarSearchByCityXmlToArray(intval($station_id['pickup_station_id']), $pickup_date, $drop_off_date, $pickup_hour, $drop_off_hour, $vehicle_type, $rental_company, $total_pax, $drive_country_code = "", $driver_age = 22, $drop_off = "");
        }


        foreach ($data['car_list'] as &$a_car) {
            $a_car['company_logo_url'] = $this->GetCompanyLogoByCompanyName($a_car['carCompanyName']);
        }

        $data['car_companies'] = $this->tourico_car_search->GetCarCompanyNameIdPair();

        echo json_encode(array("card_details" => $data));
    }

    function ViewCarSearchResults()
    {
        $pickup_location_name = $this->input->post('pickupLocationName');
        $drop_off_location_name = $this->input->post('dropOfLocationName');
        $airport_city = $this->input->post('airportOrCity');
        $transmission_type = $this->input->post('transmission_type');
        $vehicle_type = $this->input->post('vehicle_type');
        $rental_company = $this->input->post('rental_company');
        $pickup_date = $this->input->post('pickup_date');
        $pickup_hour = $this->input->post('pickup_time');
        $drop_off_date = $this->input->post('drop_off_date');
        $drop_off_hour = $this->input->post('drop_off_time');

        $result_arr = $this->destination_model->GetDestinationCodeByDestinationName($pickup_location_name);

        $pickup_station_id = array('pickup_station_id' => $result_arr['tourico_destination_id']);

        $this->session->set_userdata($pickup_station_id);
        $this->session->set_userdata($pickup_location_name);
        $this->session->set_userdata($drop_off_location_name);

        $result_arr = $this->destination_model->GetDestinationCodeByDestinationName($pickup_location_name);
        $drop_off_destination_id = $this->destination_model->GetDestinationCodeByDestinationName($pickup_location_name);

        $station_id = $pickup_station_id;

        if ("City" == $airport_city) {
            $car_search_api_obj = new CarSearchAPIResponseProcess();

            $dateObj = DateTime::createFromFormat('m/d/Y', $pickup_date);
            $pickup_date = $dateObj->format('Y-m-d');

            $dateObj = DateTime::createFromFormat('m/d/Y', $drop_off_date);
            $drop_off_date = $dateObj->format('Y-m-d');

            $pick_up_drop_off_times = array(
                'pickup_date' => $pickup_date,
                'pickup_hour' => $pickup_hour,
                'drop_off_date' => $drop_off_date,
                'drop_off_hour' => $drop_off_hour
            );

            $this->session->set_userdata($pick_up_drop_off_times);
            $this->session->set_userdata(
                array(
                    'pickup_location' => $pickup_location_name,
                    'drop_off_location' => $drop_off_location_name
                )
            );

            $vehicle_type = intval($vehicle_type);
            $rental_company = 0;
            $total_pax = 10;

            $data['car_list'] = $car_search_api_obj->ParseCarSearchByAirportXmlToArray(intval($station_id['pickup_station_id']), $pickup_date, $drop_off_date, $pickup_hour, $drop_off_hour, $vehicle_type, $rental_company, $total_pax, $drive_country_code = "", $driver_age = 22, $drop_off = "");
        } else if ("City" == $airport_city) {
            $car_search_api_obj = new CarSearchAPIResponseProcess();

            $dateObj = DateTime::createFromFormat('m/d/Y', $pickup_date);
            $pickup_date = $dateObj->format('Y-m-d');

            $dateObj = DateTime::createFromFormat('m/d/Y', $drop_off_date);
            $drop_off_date = $dateObj->format('Y-m-d');

            $pick_up_drop_off_times = array(
                'pickup_date' => $pickup_date,
                'pickup_hour' => $pickup_hour,
                'drop_off_date' => $drop_off_date,
                'drop_off_hour' => $drop_off_hour
            );

            $this->session->set_userdata($pick_up_drop_off_times);
            $this->session->set_userdata(
                array(
                    'pickup_location' => $pickup_location_name,
                    'drop_off_location' => $drop_off_location_name
                )
            );

            $vehicle_type = intval($vehicle_type);
            $rental_company = 0;
            $total_pax = 10;

            //$pickup_station_id = 5751;
            $data['car_list'] = $car_search_api_obj->ParseCarSearchByCityXmlToArray(intval($station_id['pickup_station_id']), $pickup_date, $drop_off_date, $pickup_hour, $drop_off_hour, $vehicle_type, $rental_company, $total_pax, $drive_country_code = "", $driver_age = 22, $drop_off = "");
        }


        foreach ($data['car_list'] as &$a_car) {
            $a_car['company_logo_url'] = $this->GetCompanyLogoByCompanyName($a_car['carCompanyName']);
        }

        $data['car_companies'] = $this->tourico_car_search->GetCarCompanyNameIdPair();

        $data['page'] = "front/search_result/car_search_result";
        $this->page_name = 'Home';
        $data['js'] = array(
            'login.js',
            'moment.min.js',
            'front/home.js',
            'affiliate/req_quote.js',
            'custom/components-pickers.js',
            'custom/components-dropdowns.js',
            'custom/car_search_filter.js',
            'front/jquery.bxslider.js',
            'custom/sResultC.js'
        );
        $data['js_plugin'] = array(
            'bootstrap-fileinput/bootstrap-fileinput.js',
            'checkincheckout/bootstrap-datepicker.js',
            'bootstrap-select/bootstrap-select.min.js',
            'select2/select2.min.js',
            'jquery.bxslider.min.js',
            'bsStarRating/star-rating.min.js'
        );
        $data['css_plugin'] = array(
            'bootstrap-fileinput/bootstrap-fileinput.css',
            'bootstrap-datepicker/css/datepicker.css',
            'bootstrap-select/bootstrap-select.min.css',
            'select2/select2.css',
            'select2/select2-metronic.css',
            'jquery-ui/jquery-ui.css',
            'jquery.bxslider.css',
            'bsStarRating/star-rating.min.css'
        );
        $data['css'] = array();


        $this->load->view(FRONT_LAYOUT, $data);
    }

    function ViewCarDetails($product_id, $car_company_id, $car_class, $pickup_station_id)
    {
        $product_id = base64_decode($product_id);
        $pickup_location = $this->input->post("pickupLocationName");

        $data = array();
        $car_search_api_obj = new CarSearchAPIResponseProcess();
        $data['rules_regulations'] = $car_search_api_obj->ParseRulesAndRegulationsXmlToArray($car_company_id);
        $data['pickup_station_id'] = $this->session->userdata('pickup_station_id');
        $data['pickup_location_name'] = $this->session->userdata('pickup_location_name');
        $data['drop_off_location_name'] = $this->session->userdata('drop_off_location_name');

        $data['pickup_date'] = $this->session->userdata('pickup_date');
        $data['pickup_hour'] = $this->session->userdata('pickup_hour');
        $data['drop_off_date'] = $this->session->userdata('drop_off_date');
        $data['drop_off_hour'] = $this->session->userdata('drop_off_hour');
        $data['pickup_location'] = $this->session->userdata('pickup_location');
        $data['drop_off_location'] = $this->session->userdata('drop_off_location');

        $data['car_class'] = $car_class;

        $data['select_station'] = $car_search_api_obj->ParseSelectStationsXmlToArray($product_id, $pickup_station_id, $pickup_station_id);

        if ($data['select_station'] != "")
            $data['company_logo'] = $this->GetCompanyLogoByCompanyName($data['select_station'][0]['carCompanyName']);

        $data['page'] = "front/search_result/car_details";
        $this->page_name = 'Home';
        $data['js'] = array(
            'login.js',
            'moment.min.js',
            'front/home.js',
            'affiliate/req_quote.js',
            'custom/components-pickers.js',
            'custom/components-dropdowns.js',
            'front/jquery.bxslider.js',
            'custom/sResultC.js'
        );
        $data['js_plugin'] = array(
            'bootstrap-fileinput/bootstrap-fileinput.js',
            'checkincheckout/bootstrap-datepicker.js',
            'bootstrap-select/bootstrap-select.min.js',
            'select2/select2.min.js',
            'jquery.bxslider.min.js',
            'bsStarRating/star-rating.min.js'
        );
        $data['css_plugin'] = array(
            'bootstrap-fileinput/bootstrap-fileinput.css',
            'bootstrap-datepicker/css/datepicker.css',
            'bootstrap-select/bootstrap-select.min.css',
            'select2/select2.css',
            'select2/select2-metronic.css',
            'jquery-ui/jquery-ui.css',
            'jquery.bxslider.css',
            'bsStarRating/star-rating.min.css'
        );
        $data['css'] = array();


        $this->load->view(FRONT_LAYOUT, $data);
    }

    function GetCompanyLogoByCompanyName($company_name)
    {
        if ("Sixt" == $company_name) {
            $url = base_url("public/assets/img/company_logo/logo-Sixt.gif");
        } else if ("Hertz" == $company_name) {
            $url = base_url("public/assets/img/company_logo/logo-Hertz.gif");
        } else if ("Budget" == $company_name) {
            $url = base_url("public/assets/img/company_logo/logo-Budget.gif");
        } else if ("Thrifty" == $company_name) {
            $url = base_url("public/assets/img/company_logo/logo-Thrifty.gif");
        } else if ("Enterprise" == $company_name) {
            $url = base_url("public/assets/img/company_logo/logo-Enterprise.gif");
        } else if ("National" == $company_name) {
            $url = base_url("public/assets/img/company_logo/logo-National.gif");
        } else if ("Fox" == $company_name) {
            $url = base_url("public/assets/img/company_logo/logo-Fox.gif");
        } else if ("Avis" == $company_name) {
            $url = base_url("public/assets/img/company_logo/logo-Avis.gif");
        } else {
            $url = "";
        }

        return $url;
    }

    function GetCruiseDepartureMonthOptions()
    {
        $cruise_options = '<option value="01/0001">All Dates</option>';

        for ($i = 1; $i <= 12; $i++) {
            $curDate = "+" . $i . " month";
            $month_slash_year = date('m/Y', strtotime($curDate));
            $month_space_year = date('M', strtotime($curDate)) . "  " . date('Y', strtotime($curDate));

            $cruise_options .= '<option value=' . $month_slash_year . '>' . $month_space_year . '</option>';
        }

        return $cruise_options;
    }

    function GetCruiseDetails()
    {
        $ship_id = $this->input->post("ship_id");

        $tourico_cruise_obj = new ProcessCruiseAPI();
        $ship_details = $tourico_cruise_obj->GetShipDetail($ship_id);

        echo json_encode(array("ship_detail" => $ship_details));
    }

    function stripePayment($paymentInfo)
    {
        \Stripe\Stripe::setApiKey('sk_test_pXZu0HXdj2nffgTwk8xbG8Tr');
        $planInfo = null;
        try {
            $charge = \Stripe\Charge::create($paymentInfo);
            return array('success' => true, 'charge_info' => $charge);
        } catch (\Stripe\Error\Card $e) {
            $body = $e->getJsonBody();
            $err = $body['error'];
            return array('success' => false, 'reason' => 'card_declined', 'details' => $err['message']);
        } catch (\Stripe\Error\InvalidRequest $e) {
            return array('success' => false, 'reason' => 'invalid_parameter_supplied', 'details' => 'Your charge attempt was declined. Please check the information you entered and try again.');
        } catch (\Stripe\Error\Authentication $e) {
            return array('success' => false, 'reason' => 'secret_key_not_valid', 'details' => 'Your charge attempt was declined. Please check the information you entered and try again.');
        } catch (\Stripe\Error\ApiConnection $e) {
            return array('success' => false, 'reason' => 'connection_problem', 'details' => 'connection to stripe is not working');
        } catch (Exception $e) {
            return array('success' => false, 'reason' => 'other_error', 'details' => 'connection to stripe is not working');
        }
    }

    function load_iframe()
    {

        if (isset($_GET['url'])) {
            $url = $_GET['url'];
            $ch = curl_init();
            $timeout = 5;
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
            $data = curl_exec($ch);
            curl_close($ch);
            echo $data;
        }
        exit;
    }


    /*********************
     * private functions for TL - K
     */

    // getting total number of day between checkin and checkout
    private function getDayBetweenDates()
    {
        $date1 = new DateTime($this->input->get('check_in', TRUE));
        $date2 = new DateTime($this->input->get('check_out', TRUE));
        $interval = $date1->diff($date2);
        return $interval->d;
    }

    private function getTravelAndaHotelsList()
    {
        $data = array();

        $locationName = $this->input->get('locationName', TRUE);

        $dest = explode(',', $locationName);

        // first is city //

        $cityNameForTA = $dest[0];

        $countryNameForTA = end($dest);

        $check_in = $this->input->get('check_in', TRUE);

        $check_out = $this->input->get('check_out', TRUE);

        $dateObj = DateTime::createFromFormat('m/d/Y', $check_in);

        $check_in = $dateObj->format('Y-m-d');

        $dateObj = DateTime::createFromFormat('m/d/Y', $check_out);

        $check_out = $dateObj->format('Y-m-d');

        $roomNo = $this->input->get('roomNo', TRUE);

        $numberOfAdults = $this->input->get('numberOfAdults', TRUE);

        $numberOfChild = $this->input->get('numberOfChild', TRUE);

        $childAge = $this->input->get('childAge', TRUE);

        $data_for_one_hotel_request = array(

            'check_in_date' => $check_in,

            'check_out_date' => $check_out,

            'adult_number' => $numberOfAdults,

            'child_number' => $numberOfChild,

            'number_of_rooms' => $roomNo,

            'child_age' => $childAge

        );

        $adultChildData = array();
        $child_age_index = 0;
        for ($i = 0; $i < $roomNo; $i++) {
            $room = intval($numberOfAdults[$i]);
            //if($room >= 1 && $room <= 4) {
            $adultChildData[$i]['NumAdults'] = $room;
            //}
            if (isset($numberOfChild[$i]) && $numberOfChild[$i] > 0) {
                $ages = array();
                for ($j = $child_age_index; $j < (intval($numberOfChild[$i]) + $child_age_index); $j++) {
                    $ages[] = $childAge[$j];
                }
                $adultChildData[$i]['Children'] = $ages;

                $child_age_index = $j;
            }

        }

//        echo '<pre>';print_r($data_for_one_hotel_request);
        //      echo '<pre>';print_r($adultChildData);
        $countries = $this->app->getData(array(
            'table' => 'dz_travellenda_counties',
            'custom' => "touricoCountryName LIKE '%" . trim($countryNameForTA) . "%' OR country_code LIKE '%" . trim($countryNameForTA) . "%' OR country_name LIKE '%" . trim($countryNameForTA) . "%'",
            'fields' => 'country_code'
        ));
        $cities = array();
        foreach ($countries as $val) {
            $city = $this->app->getData(array(
                'table' => 'dz_travellenda_cities',
                'custom' => "country_code = '" . trim($val['country_code']) . "' AND city_name LIKE '%" . trim($cityNameForTA) . "%'",
                'fields' => 'city_id'
            ));
            if (count($city) > 0) {
                foreach ($city as $c) {
                    $cities[] = $c['city_id'];
                }
            }
        }
        //echo '<pre>';print_r($cities);exit;
        if (count($cities) > 0) {
            $travellensda = new TravellandaAPICore;
            //GetHotelDetails
            //print_r($reslt->HotelSearch("101848", "2016-11-24" , "2016-11-25" , 1 , array(array("NumAdults"=>1,"Children"=>array(2)))));
            $data = $travellensda->HotelSearch($cities[0], $check_in, $check_out, $roomNo, $adultChildData);
            // parse data
            $data = $this->parseTAHotelsData($data);

        }
        return $data;

    }

    // parsing
    private function parseTAHotelsData($data)
    {
        //echo '<pre>';print_r($data);exit;
        $hotelsIds = array();
        $hotelsData = array();
        $hotelBasicDetail = array();
        if (!isset($result['Error'])) {
            $result = $data['Body'];

            if (isset($result['Hotels']['Hotel']['HotelId'])) {
                $minPrice = 25000;
                //single
                // basic detail
                $hotelBasicDetail[0]['hotelId'] = $result['Hotels']['Hotel']['HotelId'];
                $hotelBasicDetail[0]['hotelType'] = "TL";
                $hotelBasicDetail[0]['voucher_remarks'] = '';
                $hotelBasicDetail[0]['hotelThumbUrl'] = '';
                $hotelBasicDetail[0]['distanceFromAirport'] = '';
                $hotelBasicDetail[0]['distanceFromAirportUnit'] = '';
                $hotelBasicDetail[0]['longitude'] = '';
                $hotelBasicDetail[0]['latitude'] = '';
                $hotelBasicDetail[0]['facilities'] = '';
                $hotelBasicDetail[0]['rooms'] = '';
                $hotelBasicDetail[0]['sports_entertainment'] = '';
                $hotelBasicDetail[0]['meals'] = '';
                $hotelBasicDetail[0]['payment'] = '';
                $hotelBasicDetail[0]['a_hotel_destination_id'] = '';
                $hotelBasicDetail[0]['hotel_location_country'] = '';
                $hotelBasicDetail[0]['hotel_location_state'] = '';
                $hotelBasicDetail[0]['hotel_location_city'] = '';
                $hotelBasicDetail[0]['hotel_location_zip'] = '';
                //$hotelBasicDetail[0]['multuiplier_percentage'] = '';


                $hotelBasicDetail[0]['hotelName'] = $result['Hotels']['Hotel']['HotelName'];
                $hotelBasicDetail[0]['star_level'] = $result['Hotels']['Hotel']['StarRating'];
                $hotelsIds[] = $result['Hotels']['Hotel']['HotelId'];
                // options detail
                $optionsList = array();
                if (isset($result['Hotels']['Hotel']['Options']['Option']) && count($result['Hotels']['Hotel']['Options']['Option']) > 0) {
                    if (isset($result['Hotels']['Hotel']['Options']['Option']['OptionId'])) {
                        // single record
                        // option is taken as room type
                        $optionsList[0]['roomTypeCategory'] = $this->getFirstRoomName($result['Hotels']['Hotel']['Options']['Option']['Rooms']);
                        $optionsList[0]['name'] = $this->getFirstRoomName($result['Hotels']['Hotel']['Options']['Option']['Rooms']);
                        $optionsList[0]['RoomTypeId'] = $result['Hotels']['Hotel']['Options']['Option']['OptionId'];
                        $optionsList[0]['HotelType'] = 'TL';
                        $optionsList[0]['RoomId'] = $result['Hotels']['Hotel']['Options']['Option']['OptionId'];
                        $optionsList[0]['isAvailable'] = ($result['Hotels']['Hotel']['Options']['Option']['OnRequest'] == 0) ? 'true' : 'false';
                        $optionsList[0]['TL_TotalPrice'] = ($result['Hotels']['Hotel']['Options']['Option']['TotalPrice']);
                        // rooms detail
                        $occupancy_list = $this->getRoomsDetail($result['Hotels']['Hotel']['Options']['Option']);
                        $optionsList[0]['occupancy_list'] = $occupancy_list['roomListSet'];
                        //$minPrice = $occupancy_list['minPrice'];
                        if ($occupancy_list['minPrice'] < $minPrice) {
                            $minPrice = $occupancy_list['minPrice'];
                        }
                        $optionsList[0]['occupancy_list_2'] = array();
                        $optionsList[0]['discount'] = $this->getDiscount($result['Hotels']['Hotel']['Options']['Option']);/*array(
                            "discType" => "TL_DISCOUNT",
                            "desc" => ''
                        );*/
                        $optionsList[0]['supp'] = array();
                    } else {
                        // multi records
                        foreach ($result['Hotels']['Hotel']['Options']['Option'] as $op => $options) {
                            // echo '<pre>';print_r($options);exit;
                            // option is taken as room type
                            $optionsList[$op]['roomTypeCategory'] = $this->getFirstRoomName($options['Rooms']);
                            $optionsList[$op]['name'] = $this->getFirstRoomName($options['Rooms']);
                            $optionsList[$op]['RoomTypeId'] = $options['OptionId'];
                            $optionsList[$op]['HotelType'] = 'TL';
                            $optionsList[$op]['RoomId'] = $options['OptionId'];
                            $optionsList[$op]['isAvailable'] = ($options['OnRequest'] == 0) ? 'true' : 'false';
                            $optionsList[$op]['TL_TotalPrice'] = ($options['TotalPrice']);
                            // rooms detail
                            $occupancy_list = $this->getRoomsDetail($options);
                            $optionsList[$op]['occupancy_list'] = $occupancy_list['roomListSet'];
                            //$minPrice = $occupancy_list['minPrice'];
                            if ($occupancy_list['minPrice'] < $minPrice) {
                                $minPrice = $occupancy_list['minPrice'];
                            }
                            $optionsList[$op]['occupancy_list_2'] = array();
                            $optionsList[$op]['discount'] = $this->getDiscount($options);
                            $optionsList[$op]['supp'] = array();
                        }
                    }
                }
                $hotelBasicDetail[0]['room_type_list'] = $optionsList;

                $hotelBasicDetail[0]['hotel_location_name'] = '';
                $hotelBasicDetail[0]['min_price'] = $minPrice;
                $hotelBasicDetail[0]['only_min_price1'] = $minPrice;
                $hotelBasicDetail[0]['only_min_price'] = $minPrice;
                $hotelBasicDetail[0]['only_min_price_tax'] = 0;
                $hotelBasicDetail[0]['only_min_price_inc_tax'] = $minPrice;
                $hotelBasicDetail[0]['distanceFromAirport'] = '';
                $hotelBasicDetail[0]['hotel_brand_name'] = '';
                $hotelBasicDetail[0]['aminities'] = array();
                $hotelBasicDetail[0]['location'] = array();
            } else {
                // multi
                if (isset($result['Hotels']['Hotel'])) {
                    foreach ($result['Hotels']['Hotel'] as $h => $hotels) {
                        /*if($hotels['HotelName'] == 'Comfort Inn Las Vegas'){
                            echo '<pre>';print_r($hotels);exit;
                        }*/
                        $minPrice = 25000;
                        // basic detail
                        $hotelBasicDetail[$h]['hotelId'] = $hotels['HotelId'];
                        $hotelBasicDetail[$h]['hotelType'] = "TL";
                        $hotelBasicDetail[$h]['hotelName'] = $hotels['HotelName'];
                        $hotelBasicDetail[$h]['star_level'] = $hotels['StarRating'];
                        $hotelBasicDetail[$h]['voucher_remarks'] = '';
                        $hotelBasicDetail[$h]['hotelThumbUrl'] = '';
                        $hotelBasicDetail[$h]['distanceFromAirport'] = '';
                        $hotelBasicDetail[$h]['distanceFromAirportUnit'] = '';
                        $hotelBasicDetail[$h]['longitude'] = '';
                        $hotelBasicDetail[$h]['latitude'] = '';
                        $hotelBasicDetail[$h]['facilities'] = '';
                        $hotelBasicDetail[$h]['rooms'] = '';
                        $hotelBasicDetail[$h]['sports_entertainment'] = '';
                        $hotelBasicDetail[$h]['meals'] = '';
                        $hotelBasicDetail[$h]['payment'] = '';
                        $hotelBasicDetail[$h]['a_hotel_destination_id'] = '';
                        $hotelBasicDetail[$h]['hotel_location_country'] = '';
                        $hotelBasicDetail[$h]['hotel_location_state'] = '';
                        $hotelBasicDetail[$h]['hotel_location_city'] = '';
                        $hotelBasicDetail[$h]['hotel_location_zip'] = '';
                        //$hotelBasicDetail[$h]['multuiplier_percentage'] = '';

                        $hotelsIds[] = $hotels['HotelId'];
                        // options detail
                        $optionsList = array();
                        if (isset($hotels['Options']['Option']) && count($hotels['Options']['Option']) > 0) {
                            if (isset($hotels['Options']['Option']['OptionId'])) {
                                // single record
                                // option is taken as room type
                                $optionsList[0]['roomTypeCategory'] = $this->getFirstRoomName($hotels['Options']['Option']['Rooms']);
                                $optionsList[0]['name'] = $this->getFirstRoomName($hotels['Options']['Option']['Rooms']);
                                $optionsList[0]['RoomTypeId'] = $hotels['Options']['Option']['OptionId'];
                                $optionsList[0]['HotelType'] = 'TL';
                                $optionsList[0]['RoomId'] = $hotels['Options']['Option']['OptionId'];
                                $optionsList[0]['isAvailable'] = ($hotels['Options']['Option']['OnRequest'] == 0) ? 'true' : 'false';
                                $optionsList[0]['TL_TotalPrice'] = ($hotels['Options']['Option']['TotalPrice']);
                                // rooms detail
                                $occupancy_list = $this->getRoomsDetail($hotels['Options']['Option']);
                                $optionsList[0]['occupancy_list'] = $occupancy_list['roomListSet'];
                                //$minPrice = $occupancy_list['minPrice'];
                                if ($occupancy_list['minPrice'] < $minPrice) {
                                    $minPrice = $occupancy_list['minPrice'];
                                }
                                $optionsList[0]['occupancy_list_2'] = array();
                                $optionsList[0]['discount'] = $this->getDiscount($hotels['Options']['Option']);
                                $optionsList[0]['supp'] = array();
                            } else {
                                // multi records
                                foreach ($hotels['Options']['Option'] as $op => $options) {
                                    // option is taken as room type
                                    $optionsList[$op]['roomTypeCategory'] = $this->getFirstRoomName($options['Rooms']);
                                    $optionsList[$op]['name'] = $this->getFirstRoomName($options['Rooms']);
                                    $optionsList[$op]['RoomTypeId'] = $options['OptionId'];
                                    $optionsList[$op]['HotelType'] = 'TL';
                                    $optionsList[$op]['RoomId'] = $options['OptionId'];
                                    $optionsList[$op]['isAvailable'] = ($options['OnRequest'] == 0) ? 'true' : 'false';
                                    $optionsList[$op]['TL_TotalPrice'] = ($options['TotalPrice']);
                                    // rooms detail
                                    $occupancy_list = $this->getRoomsDetail($options);
                                    $optionsList[$op]['occupancy_list'] = $occupancy_list['roomListSet'];
                                    //$minPrice = $occupancy_list['minPrice'];
                                    if ($occupancy_list['minPrice'] < $minPrice) {
                                        $minPrice = $occupancy_list['minPrice'];
                                    }
                                    $optionsList[$op]['occupancy_list_2'] = array();
                                    $optionsList[$op]['discount'] = $this->getDiscount($options);
                                    $optionsList[$op]['supp'] = array();
                                }
                            }
                        }
                        $hotelBasicDetail[$h]['room_type_list'] = $optionsList;

                        $hotelBasicDetail[$h]['hotel_location_name'] = '';
                        $hotelBasicDetail[$h]['only_min_price'] = $minPrice;
                        $hotelBasicDetail[$h]['min_price'] = $minPrice;
                        $hotelBasicDetail[$h]['only_min_price1'] = $minPrice;
                        $hotelBasicDetail[$h]['only_min_price_tax'] = 0;
                        $hotelBasicDetail[$h]['only_min_price_inc_tax'] = $minPrice;
                        $hotelBasicDetail[$h]['distanceFromAirport'] = '';
                        $hotelBasicDetail[$h]['hotel_brand_name'] = '';
                        $hotelBasicDetail[$h]['aminities'] = array();
                        $hotelBasicDetail[$h]['location'] = array();

                        /*foreach ($hotels['Options']['Option'] as $options) {

                        }*/
                    }
                }
            }
            /*$hotelBasicDetail[7]['hotelId'] = 1234;
            $hotelBasicDetail[7]['hotelName'] = "Circus Circus Hotel, Casino & Theme Park - Demo";
            $hotelBasicDetail[7]['star_level'] = 3;
            $hotelBasicDetail[7]['hotel_location_name'] = '';
            $hotelBasicDetail[7]['only_min_price'] = '';
            $hotelBasicDetail[7]['distanceFromAirport'] = '';
            $hotelBasicDetail[7]['hotel_brand_name'] = '';
            $hotelBasicDetail[7]['aminities'] = array();
            $hotelBasicDetail[7]['location'] = array();
            $hotelBasicDetail[7]['room_type_list'] = array();*/
        }
        //echo "<pre>";print_r($hotelsIds);exit;

        // get hotels detail
        $travellensda = new TravellandaAPICore;
        /**************
         * only 250 hotel ids are allowed in single request
         * so making chunks of 245 ids
         */
        $hDetailParsed = array();
        if (count($hotelsIds) > 245) {
            $chunks = array_chunk($hotelsIds, 245);
            foreach ($chunks as $ids) {

                $hDetailList = $travellensda->GetHotelDetails($ids);
                $hDetailParsedList = $this->parseHotelDetail($hDetailList);
                // echo "<pre>";print_r(($hDetailList));
                $hDetailParsed = array_merge($hDetailParsed, $hDetailParsedList);
                //echo "<pre>";print_r(($hDetail));
            }
        } else {
            $hDetail = $travellensda->GetHotelDetails($hotelsIds);
            $hDetailParsed = $this->parseHotelDetail($hDetail);
        }

        //echo "<pre>";print_r(($hotelsIds));echo "<pre>";print_r(count($hDetail));exit;
        // parse detail data

        // add detail in respective hotel
        $hotelsWithDetail = $this->addDetail($hotelBasicDetail, $hDetailParsed);

        return $hotelsWithDetail;
    }

    private function parseHotelDetail($data)
    {
        // echo '<pre>';print_r($data);exit;
        $result = array();
        if (isset($data['Body']['Hotels']['Hotel']['HotelId'])) {
            $result[0]['HotelId'] = $data['Body']['Hotels']['Hotel']['HotelId'];
            $result[0]['countryCode'] = '';
            $result[0]['stateCode'] = '';
            $result[0]['city'] = '';
            $result[0]['searchingState'] = '';
            $result[0]['searchingCity'] = '';
            $result[0]['latitude'] = $data['Body']['Hotels']['Hotel']['Latitude'];
            $result[0]['location'] = $data['Body']['Hotels']['Hotel']['Location'];
            $result[0]['longitude'] = $data['Body']['Hotels']['Hotel']['Longitude'];
            $result[0]['address'] = $data['Body']['Hotels']['Hotel']['Address'];
            if (isset($val['Images']['Image'])) {
                $result[0]['hotelThumbUrl'] = is_array($data['Body']['Hotels']['Hotel']['Images']['Image']) ? $data['Body']['Hotels']['Hotel']['Images']['Image'][0] : $data['Body']['Hotels']['Hotel']['Images']['Image'];
            } else {
                $result[0]['hotelThumbUrl'] = '';
            }

        } else {
            if (isset($data['Body']['Hotels']['Hotel'][0])) {
                foreach ($data['Body']['Hotels']['Hotel'] as $k => $val) {
                    $result[$k]['HotelId'] = $val['HotelId'];
                    $result[$k]['countryCode'] = '';
                    $result[$k]['stateCode'] = '';
                    $result[$k]['city'] = '';
                    $result[$k]['searchingState'] = '';
                    $result[$k]['searchingCity'] = '';
                    $result[$k]['latitude'] = $val['Latitude'];
                    $result[$k]['location'] = $val['Location'];
                    $result[$k]['longitude'] = $val['Longitude'];
                    $result[$k]['address'] = $val['Address'];
                    if (isset($val['Images']['Image'])) {
                        $result[$k]['hotelThumbUrl'] = is_array($val['Images']['Image']) ? $val['Images']['Image'][0] : $val['Images']['Image'];
                    } else {
                        $result[$k]['hotelThumbUrl'] = '';
                    }

                }
            }
        }

        return $result;
    }

    private function addDetail($hotelBasicDetail, $hDetailParsed)
    {
        foreach ($hotelBasicDetail as $k => $hb) {
            foreach ($hDetailParsed as $j => $parsed) {
                if ($hb['hotelId'] == $parsed['HotelId']) {
                    $hotelBasicDetail[$k]['longitude'] = $parsed['longitude'];
                    $hotelBasicDetail[$k]['latitude'] = $parsed['latitude'];
                    $hotelBasicDetail[$k]['hotelThumbUrl'] = $parsed['hotelThumbUrl'];
                    $hotelBasicDetail[$k]['location'] = $parsed;
                    continue;
                }
            }
        }

        return $hotelBasicDetail;

    }

    private function getRoomsDetail($data)
    {
        $roomListSet = array();
        $roomSet = $data['Rooms'];
        $price = 25000;
        if (isset($roomSet['Room']['RoomId'])) {
            $sRoom = $roomSet['Room'];
            // single record
            $roomList['Room 1']['room_sequnce'] = 1;
            $roomList['Room 1']['TL_RoomId'] = $sRoom['RoomId'];
            $roomList['Room 1']['TL_RoomName'] = $sRoom['RoomName'];
            $roomList['Room 1']['TL_optionId'] = $data['OptionId'];
            // daily room price
            if (isset($sRoom['DailyPrices'])) {
                if (is_array($sRoom['DailyPrices']['DailyPrice'])) {
                    $dailyPrice = round(array_sum($sRoom['DailyPrices']['DailyPrice']) / count($sRoom['DailyPrices']['DailyPrice']), 2);
                } else {
                    $dailyPrice = $sRoom['DailyPrices']['DailyPrice'];
                }
            } else {
                $dailyPrice = round($sRoom['RoomPrice'] / $this->days, 2);
            }

            // available_list
            $available_list[0]['per_night_price'] = $dailyPrice;
            $available_list[0]['per_night_tax'] = 0;
            $available_list[0]['max_guest'] = $sRoom['NumAdults'];
            $available_list[0]['max_child'] = $sRoom['NumChildren'];
            $available_list[0]['total_price'] = $sRoom['RoomPrice'];
            $available_list[0]['total_tax'] = 0;
            $available_list[0]['bedding'] = $sRoom['NumAdults'] + $sRoom['NumChildren'] . ',0';
            $available_list[0]['occpNo'] = 0;
            $available_list[0]['roomDet'][0] = array(
                'roomSeqNo' => 1,
                'numOfAdults' => $sRoom['NumAdults'],
                'numOfChild' => $sRoom['NumChildren'],
                //'childages'=>array(1,1)
            );
            if (isset($sRoom['DailyPrices'])) {
                if (is_array($sRoom['DailyPrices']['DailyPrice'])) {
                    $perDayPrices = $sRoom['DailyPrices']['DailyPrice'];
                } else {
                    $perDayPrices = array_fill(0, 1, $sRoom['DailyPrices']['DailyPrice']);
                }
            } else {
                $perDayPrices = array_fill(0, $this->days, $dailyPrice);
            }

            $available_list[0]['per_day_price'] = $perDayPrices;//isset($sRoom['DailyPrices']) ? $sRoom['DailyPrices']['DailyPrice']: array_fill(0, $this->days, $dailyPrice);
            if ($data['OnRequest'] == '1') {
                $perDay = 'false';
            } else {
                $perDay = 'true';
            }
            $available_list[0]['per_day_availability'] = array_fill(0, $this->days, $perDay);

            if ($data['BoardType'] != 'Room Only')
                $available_list[0]['BoardBases'][0] = array('bbId' => '', 'bbName' => $data['BoardType'], 'bbPrice' => 0, 'bbPublishPrice' => 0);
            else
                $available_list[0]['BoardBases'] = array();

            $available_list[0]['actual_total_price'] = $sRoom['RoomPrice'];
            $available_list[0]['per_night_price_inc_tax'] = $dailyPrice;
            $available_list[0]['total_price_inc_tax'] = $sRoom['RoomPrice'];
            $available_list[0]['per_day_price_inc_tax'] = array_fill(0, $this->days, $dailyPrice);
            $available_list[0]['total_tax'] = 0;
            // add list
            $roomList['Room 1']['available_list'] = $available_list;
            $roomListSet = $roomList;
            $dataSet = array('roomListSet' => $roomListSet, 'minPrice' => $dailyPrice);
        } else {
            // multi records
            $roomList = array();
            foreach ($roomSet['Room'] as $rK => $sRoom) {
                $roomNumber = 'Room ' . ($rK + 1);
                $roomList[$roomNumber]['room_sequnce'] = $rK + 1;
                $roomList[$roomNumber]['TL_RoomId'] = $sRoom['RoomId'];
                $roomList[$roomNumber]['TL_RoomName'] = $sRoom['RoomName'];
                $roomList[$roomNumber]['TL_optionId'] = $data['OptionId'];
                // daily room price
                if (isset($sRoom['DailyPrices'])) {
                    if (is_array($sRoom['DailyPrices']['DailyPrice'])) {
                        $dailyPrice = round(array_sum($sRoom['DailyPrices']['DailyPrice']) / count($sRoom['DailyPrices']['DailyPrice']), 2);
                    } else {
                        $dailyPrice = $sRoom['DailyPrices']['DailyPrice'];
                    }
                } else {
                    $dailyPrice = round($sRoom['RoomPrice'] / $this->days, 2);
                }
                // available_list
                $available_list[0]['per_night_price'] = $dailyPrice;
                $available_list[0]['per_night_tax'] = 0;
                $available_list[0]['max_guest'] = $sRoom['NumAdults'];
                $available_list[0]['max_child'] = $sRoom['NumChildren'];
                $available_list[0]['total_price'] = $sRoom['RoomPrice'];
                $available_list[0]['total_tax'] = 0;
                $available_list[0]['bedding'] = $sRoom['NumAdults'] + $sRoom['NumChildren'] . ',0';
                $available_list[0]['occpNo'] = $rK;
                $available_list[0]['roomDet'][0] = array(
                    'roomSeqNo' => $rK + 1,
                    'numOfAdults' => $sRoom['NumAdults'],
                    'numOfChild' => $sRoom['NumChildren'],
                    //'childages'=>array(1,1)
                );
                if (isset($sRoom['DailyPrices'])) {
                    if (is_array($sRoom['DailyPrices']['DailyPrice'])) {
                        $perDayPrices = $sRoom['DailyPrices']['DailyPrice'];
                    } else {
                        $perDayPrices = array_fill(0, 1, $sRoom['DailyPrices']['DailyPrice']);
                    }
                } else {
                    $perDayPrices = array_fill(0, $this->days, $dailyPrice);
                }
                $available_list[0]['per_day_price'] = $perDayPrices;//isset($sRoom['DailyPrices']) ? $sRoom['DailyPrices']: array_fill(0, $this->days, $dailyPrice);
                if ($data['OnRequest'] == '1') {
                    $perDay = 'false';
                } else {
                    $perDay = 'true';
                }
                $available_list[0]['per_day_availability'] = array_fill(0, $this->days, $perDay);
                //$available_list[0]['per_day_availability'] = array(($data['OnRequest'] == 0) ? true : false);

                if ($data['BoardType'] != 'Room Only')
                    $available_list[0]['BoardBases'][0] = array('bbId' => '', 'bbName' => $data['BoardType'], 'bbPrice' => 0, 'bbPublishPrice' => 0);
                else
                    $available_list[0]['BoardBases'] = array();

                $available_list[0]['per_night_price_inc_tax'] = $dailyPrice;
                $available_list[0]['actual_total_price'] = $sRoom['RoomPrice'];
                $available_list[0]['total_price_inc_tax'] = $sRoom['RoomPrice'];
                $available_list[0]['per_day_price_inc_tax'] = array_fill(0, $this->days, $dailyPrice);
                $available_list[0]['total_tax'] = 0;
                // get lowest price
                if ($dailyPrice < $price) {
                    $price = $dailyPrice;
                }
                // add list
                $roomList[$roomNumber]['available_list'] = $available_list;

            }
            $roomListSet = $roomList;
            $dataSet = array('roomListSet' => $roomListSet, 'minPrice' => $price);
        }

        /*if($data['OptionId']=='40118283485') {
            echo '<pre>';print_r($dataSet);exit;
        }*/
        return $dataSet;
    }

    private function getDiscount($optionSet)
    {
        /*******
         *
         *
         *
         *
         */
        $rName = array();
        if (isset($optionSet['DiscountApplied'])) {
            $rName['discType'] = "TL_DISCOUNT";
            $rName['amount'] = $optionSet['DiscountApplied'];
        }
        return $rName;
    }

    private function getFirstRoomName($roomSet)
    {
        /*******
         * taking room name as room type
         *
         * under each options of hotel, each option has same room name that's why taking first
         * if there is one room otherwise
         * overwriting the hotel name(as it is same)
         *
         * [Rooms] => Array
         * (
         * [Room] => Array
         * (
         * [0] => Array
         * (
         * [RoomId] => 2156552886-1-0
         * [RoomName] => Standard
         * [NumAdults] => 2
         * [NumChildren] => 1
         * [RoomPrice] => 72.18
         * [DailyPrices] => Array
         * (
         * [DailyPrice] => 72.18
         * )
         *
         * )
         *
         * [1] => Array
         * (
         * [RoomId] => 2156552886-2-0
         * [RoomName] => Standard
         * [NumAdults] => 2
         * [NumChildren] => 1
         * [RoomPrice] => 82.41
         * [DailyPrices] => Array
         * (
         * [DailyPrice] => 82.41
         * )
         *
         * )
         *
         * )
         *
         * )
         */
        $rName = '';
        if (isset($roomSet['Room']['RoomId'])) {
            $sRoom = $roomSet['Room'];
            // single record
            $rName = $sRoom['RoomName'];
        } else {
            // multi records
            foreach ($roomSet['Room'] as $rK => $sRoom) {
                $rName = $sRoom['RoomName'];
            }
        }

        return $rName;
    }

    private function mergeHotels($touricoHotels, $TLHotels)
    {
        $data = array();
        foreach ($touricoHotels as $k => $htl) {
            // round to near 0.5 interval
            $touricoHotels[$k]['star_level'] = round($touricoHotels[$k]['star_level'] * 2) / 2;
            foreach ($TLHotels as $tk => $tHotel) {
                // round to near 0.5 interval
                $TLHotels[$tk]['star_level'] = round($TLHotels[$tk]['star_level'] * 2) / 2;
                $isNameMatched = $this->stringMatch($htl['hotelName'], $tHotel['hotelName'], 55);
                if (isset($tHotel['location']['address'])) {
                    $isAddressMatched = $this->stringMatch($htl['location']['address'], $tHotel['location']['address'], 90);
                    //var_dump( $isAddressMatched);exit;
                    if ($isNameMatched && $isAddressMatched) {
                        $mergedHotels = array_merge($tHotel['room_type_list'], $htl['room_type_list']);
                        //echo '<pre>';print_r($htl);
                        //
                        //if($htl['hotelName'] == 'The Hills Hotel An Ascend Collection Hotel - Demo'){
                        //  echo '<pre>';print_r($mergedHotels);
                        $minP = 50000;

                        foreach ($mergedHotels as $mk => $mh) {
                            $sumOfPrices = 3000;
                            $ocupList = $mh['occupancy_list'];
                            foreach ($ocupList as $ocL) {
                                $available_list = $ocL['available_list'];
                                foreach ($available_list as $avL) {
                                    if ($avL['per_night_price'] < $minP) {
                                        $minP = $avL['per_night_price'];
                                    }

                                    if ($avL['per_night_price'] < $sumOfPrices) {
                                        $sumOfPrices = $avL['per_night_price'];
                                    }

                                    //$avL['per_night_price'] = (int)$avL['per_night_price'];
                                }

                            }

                            $mergedHotels[$mk]['sumOfPrices'] = $sumOfPrices;
                        }

                        // sort array by price ascending
                        usort($mergedHotels, function ($a, $b) {
                            return (int)($a['sumOfPrices']) - (int)($b['sumOfPrices']);
                        });

                        //echo '<pre>';print_r($hotel_list[$k]);
                        $touricoHotels[$k]['room_type_list'] = $mergedHotels;
                        $touricoHotels[$k]['hotelName'] = $touricoHotels[$k]['hotelName'] /*. " (merged) "*/;
                        $touricoHotels[$k]['min_price'] = $minP;
                        $touricoHotels[$k]['only_min_price'] = $minP;
                        $touricoHotels[$k]['only_min_price1'] = $minP;

                        //echo '<pre>';print_r($hotel_list[$k]);
                        //echo $minP;exit;
                        //}


                        unset($TLHotels[$tk]);
                        continue;
                    }
                }
            }

        }

        return array_merge($touricoHotels, $TLHotels);
    }

    private function stringMatch($str1, $str2, $matchUpto)
    {
        if (strlen($str1) > strlen($str2)) {
            $matchTo = explode(" ", $str2);
            $matchWith = explode(" ", $str1);
        } else {
            $matchTo = explode(" ", $str1);
            $matchWith = explode(" ", $str2);
        }

        $matchCount = 0;
        $matchedData = array();
        foreach ($matchTo as $v) {
            foreach ($matchWith as $v1) {
                //ctype_alpha
                if (strtolower($v) == strtolower($v1)) {
                    $matchCount++;
                    $matchedData[] = $v;
                }
            }
        }

        // echo '<pre>';print_r($matchCount).' - ';
        //echo '<pre>';print_r($matchedData).' - ';
        //echo '<pre>';print_r($matchWith).' - ';
        //echo '<pre>';print_r($matchTo).' - ';
        $per = ($matchCount / count($matchTo) * 100);
        //echo $per;exit;
        if ($per >= $matchUpto) {
            return true;
        } else {
            return false;
        }
        //echo $matchCount.' - ';
        //exit;
    }

    private function setUpTLBooking($detail, $queryDetail, $membersDetail)
    {
        $travellensda = new TravellandaAPICore;
        $optionId = $detail[0]['RoomId'];
        $RoomsData = array();
        foreach ($queryDetail['adult_number'] as $keyS => $numberOfAdults) {
            $adultDetail = array();
            for ($i = 0; $i < $numberOfAdults; $i++) {
                $adultDetail[] = array(
                    "Title" => 'Mr.',
                    'FirstName' => $membersDetail['roomInfo'][1]['firstname'],
                    'LastName' => $membersDetail['roomInfo'][1]['lastname']
                );
            }

            $childData = array();
            for ($i = 0; $i < $queryDetail['child_number'][$keyS]; $i++) {
                $childData[] = array(
                    'FirstName' => $membersDetail['roomInfo'][1]['firstname'],
                    'LastName' => $membersDetail['roomInfo'][1]['lastname']
                );
            }

            // add adult info in room
            $RoomsData[] = array(
                'RoomId' => $detail[0]['occupancy_list']['Room ' . ($keyS + 1)]['TL_RoomId'],
                'PaxNames' => array(
                    'AdultName' => $adultDetail,
                    "ChildName" => $childData
                )
            );
        }
        $data = $travellensda->HotelBooking($optionId, $RoomsData);
        return $data;
    }

    private function checkTLRoomAvailability($detail)
    {
        $travellensda = new TravellandaAPICore;
        $optionId = $detail[0]['RoomId'];
        $pData = $travellensda->HotelPolicies($optionId);
        if (!isset($pData['Body']['Error'])) {
            $minPrice = $_SESSION['input_data_book_step1']['minPrice'];
            $policy = $this->policyParser($pData,$minPrice,true);
            $_SESSION['policyData'] = $policy;
            return true;
        } else {
            $data['error'] = "<b>Your selected room type is no longer available. Please go back and select a different room. Thank you!</b>";

            $data['page_step'] = 1;

            $_SESSION['error'] = $data['error'];

            redirect('request_quote?stp=f');
        }
    }


    function stripecallback()
    {
        //echo '<pre>';print_r($_GET);exit;
        redirect('request_quote?stp=cnfrm&source_token_3d=' . $_GET['source']);

    }

    function thankyou($booking_id)
    {

        $res = $this->db->where('id', $booking_id)->get('book_hotal')->result();
        $hotel_info = unserialize($res[0]->hotel_info);
        //echo '<pre>';print_r($hotel_info);exit;
        $data = array(
            'booking_id' => $booking_id,
            'hotel' => $res[0],
            'hotel_info' => $hotel_info,
            'success' => 1
        );

        $data['page'] = "front/search_result/thankyou";
        $this->load->view('front/search_layout', $data);
    }

}

?>