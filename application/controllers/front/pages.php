<?php

class Pages extends Front_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('session');
    }

    function travel_professional_signup(){
		$data['compnay_types'] 	= $this->db->get_where('compnay_types')->result_array();
		$data['countries'] 		= $this->db->get_where('countries')->result_array();
		$data['hear_abouts']		= ['Social Media'=>'Social Media','Another Travel Agent'=>'Another Travel Agent','Media (TV/Radio/Magazine)'=>'Media (TV/Radio/Magazine)','Email'=>'Email','Other'=>'Other'];
		
        $data['page'] = "front/pages/travel_professional_signup";

        $this->load->view(FRONT_LAYOUT, $data);
    }
	function travel_professional_signup_post(){
		$requiredArr = ['company_name','phone_country','phone_number','city','hear_about','contact_f_name','contact_l_name','email','password','c_pasword'];
		$post = $this->input->post();
		foreach($post as $postKey=>$postValue){
			if(in_array($postKey,$requiredArr)){
				if(trim($postValue) == ''){
					exit('Please fill the required fields.');
				}
			}
		}
		if (!filter_var($this->input->post('email'), FILTER_VALIDATE_EMAIL)) {
            exit('Your email is invalid.');
        }
		$data 			= [];
		$data['company_name'] 	= $post['company_name'];
		$data['compnay_types'] 	= $post['compnay_types'];
		$data['website'] 		= $post['website'];
		$data['phone_country_code'] 	= $post['phone_country'];
		$data['phone_number'] 	= $post['phone_number']; 
		$data['phone_ex'] 		= $post['phone_ex'];
		$data['country'] 		= $post['country']; 
		$data['city'] 			= $post['city'];
		$data['street'] 		= $post['street']; 
		$data['company_owner'] 	= $post['company_owner']; 
		$data['iata'] 			= $post['iata'];
		$data['clia'] 			= $post['clia'];
		$data['arc'] 			= $post['arc'];
		$data['hear_about'] 	= $post['hear_about'];
		$data['other_hear'] 	= $post['other_hear'];
		$data['contact_f_name'] = $post['contact_f_name'];
		$data['contact_l_name'] = $post['contact_l_name'];
		$data['email'] 			= trim($post['email']);
		$data['password'] 		= base64_encode($post['password']);
		$data['contact_country_code']= $post['contact_country'];
		$data['contact_phone'] 	= $post['contact_phone'];
		
		$data_user	= [];
		$data_user['var_email'] 	= trim($post['email']);
		$data_user['var_password'] 	= base64_encode($post['password']);
		$data_user['var_fname'] 	= $post['contact_f_name'];
		$data_user['var_lname'] 	= $post['contact_l_name'];
		$data_user['var_phone'] 	= $post['contact_country'].$post['contact_phone'];
		$data_user['var_country'] 	= 'null';
		$data_user['chr_user_type'] = 'TF';
		$data_user['dt_created_date'] = date('Y-m-d H:i:s');
		$data_user['var_signupip'] 	= $this->getIP();
		$data_user['var_accountid'] = 'null';
		$data_user['profile_pic'] = 'null';
		$data_user['chr_status'] = 'AP';
		
		
		
		$alreadyExist = $this->db->select('int_glcode')->get_where('dz_user',['var_email'=>trim($post['email'])])->result_array();	
		if(empty($alreadyExist)){
			
			$this->db->insert('dz_user', $data_user);
			$lastInsertedId = $this->db->insert_id();
			$data['user_id'] = $lastInsertedId;
			$this->db->insert('travel_professional_detail', $data);
			$this->session->set_userdata('signup_data',$data);
			$this->send_mail(trim($post['email']),'User Register Email',$data);
			echo 'success';
			exit;
		}else{
			echo 'Sorry! You are already signup with this email.';
			exit;
		}	
	}
	function checkemail(){
        if (!filter_var($this->input->post('email'), FILTER_VALIDATE_EMAIL)) {
            echo "error";
            exit();
        }
        $this->db->where('var_email', $this->input->post('email'));
        $query = $this->db->get('dz_user')->num_rows();
        if ($query == 0) {
            echo "success";
            exit;
        } else {
            echo "error";
            exit;
        }
    }
	private function getIP() {
		$tmp = getenv("HTTP_CLIENT_IP");
		if ( $tmp && !strcasecmp( $tmp, "unknown"))
			return $tmp;
	
		$tmp = getenv("HTTP_X_FORWARDED_FOR");
		if( $tmp && !strcasecmp( $tmp, "unknown"))
			return $tmp;
		$tmp = getenv("REMOTE_ADDR");
		if($tmp && !strcasecmp($tmp, "unknown"))
			return $tmp;
	
		return("unknown");
	}

	private function send_mail($to,$subject,$data) {
         $from_email = NOREPLY; 
         $to_email = $to; 
   
         //Load email library 
         $this->load->library('email'); 
   		 $this->email->set_mailtype("html");
         $this->email->from($from_email, 'hotelsDifferently'); 
         $this->email->to($to_email);
         $this->email->subject($subject); 
		 $this->email->message('front/pages/signup_thanks',['data'=>$data],'true');
   		 $this->email->send();
      }
	function thanks(){

		$data['data'] = $this->session->userdata('signup_data');
		$data['page'] = "front/pages/signup_thanks";
	    $this->load->view(FRONT_LAYOUT, $data);
	}
	function states($country_id){
		$states	= $this->db->get_where('states',['country_id'=>$country_id])->result_array();	
		echo json_encode($states);exit;
	}
		     
    function member_new_signup(){

        $data['page'] = "front/pages/member_new_signup";

        $this->load->view(FRONT_LAYOUT, $data);
    }
    function travel_pro_faq(){

        $data['page'] = "front/pages/travel_pro_faq";

        $this->load->view(FRONT_LAYOUT, $data);
    }
    function pricing(){

        $data['page'] = "front/pages/pricing";

        $this->load->view(FRONT_LAYOUT, $data);
    }
    function enity_ac_info(){

        $data['page'] = "front/pages/enity_ac_info";

        $this->load->view(FRONT_LAYOUT, $data);
    }
	function furture(){

        $data['page'] = "front/pages/furture";

        $this->load->view(FRONT_LAYOUT, $data);
    }
    function templete(){

        $data['page'] = "front/pages/templete";

        $this->load->view(FRONT_LAYOUT, $data);
    }
    function my_info(){

        $data['page'] = "front/pages/my_info";

        $this->load->view(FRONT_LAYOUT, $data);
    }
    function Entity_faq(){

        $data['page'] = "front/pages/Entity_faq";

        $this->load->view(FRONT_LAYOUT, $data);
    }
     function enrolling (){

        $data['page'] = "front/pages/enrolling";

        $this->load->view(FRONT_LAYOUT, $data);
    }
    function checkout (){

        $data['page'] = "front/pages/checkout";

        $this->load->view(FRONT_LAYOUT, $data);
    }
    function androidcheckout (){

        $data['page'] = "front/pages/androidcheckout";

        $this->load->view(FRONT_LAYOUT, $data);
    }
    function entitysignuprequest (){

        $data['page'] = "front/pages/entitysignuprequest";

        $this->load->view(FRONT_LAYOUT, $data);
    }
    function cmpanytemplete (){

        $data['page'] = "front/pages/cmpanytemplete";

        $this->load->view(FRONT_LAYOUT, $data);
    }
    function entityenrollment (){

        $data['page'] = "front/pages/entityenrollment";

        $this->load->view(FRONT_LAYOUT, $data);
    }
    function enrollmentfinalization (){

        $data['page'] = "front/pages/enrollmentfinalization";

        $this->load->view(FRONT_LAYOUT, $data);
    }
    function entityenrollmentsnd (){

        $data['page'] = "front/pages/entityenrollmentsnd";

        $this->load->view(FRONT_LAYOUT, $data);
    }
     function landing (){

        $data['page'] = "front/pages/landing";

        $this->load->view(FRONT_LAYOUT, $data);
    }
    function refer (){

        $data['page'] = "front/pages/refer";

        $this->load->view(FRONT_LAYOUT, $data);
    }
    function paymenttemplete (){

        $data['page'] = "front/pages/paymenttemplete";

        $this->load->view(FRONT_LAYOUT, $data);
    }
    function payment (){

        $data['page'] = "front/pages/payment";

        $this->load->view(FRONT_LAYOUT, $data);
    }
    function billinginfo (){

        $data['page'] = "front/pages/billinginfo";

        $this->load->view(FRONT_LAYOUT, $data);
    }
     function acexpiration (){

        $data['page'] = "front/pages/acexpiration";

        $this->load->view(FRONT_LAYOUT, $data);
    }
    function enrollnotification (){

        $data['page'] = "front/pages/enrollnotification";

        $this->load->view(FRONT_LAYOUT, $data);
    }
    function membershipextension (){

        $data['page'] = "front/pages/membershipextension";

        $this->load->view(FRONT_LAYOUT, $data);
    }
    function expiration (){

        $data['page'] = "front/pages/expiration";

        $this->load->view(FRONT_LAYOUT, $data);
    }
    function verification (){

        $data['page'] = "front/pages/verification";

        $this->load->view(FRONT_LAYOUT, $data);
    }
    function notverified (){

        $data['page'] = "front/pages/notverified";

        $this->load->view(FRONT_LAYOUT, $data);
    }
    function expired (){

        $data['page'] = "front/pages/expired";

        $this->load->view(FRONT_LAYOUT, $data);
    }
    function expiredentity (){

        $data['page'] = "front/pages/expiredentity";

        $this->load->view(FRONT_LAYOUT, $data);
    }
    function membership (){

        $data['page'] = "front/pages/membership";

        $this->load->view(FRONT_LAYOUT, $data);
    }
}

?>