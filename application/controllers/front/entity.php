<?php

class Entity extends Front_Controller {



    function __construct() {

        parent::__construct();

        $this->load->library('session');

    }



    function signup(){

        $data['countries'] 		= $this->db->order_by("name", "asc")->get_where('countries')->result_array();

        $data['hear_abouts'] = ['Social Media'=>'Social Media','Another Travel Agent'=>'Another Travel Agent','Media (TV/Radio/Magazine)'=>'Media (TV/Radio/Magazine)','Email'=>'Email','Other'=>'Other'];



        $data['how_many_people_arr'] = $this->db->order_by("id", "asc")->get_where('company_people_range')->result_array();

        $data['page'] = "front/entity/signup";



        $this->load->view(FRONT_LAYOUT, $data);

    }

    function signup_post(){

        $requiredArr = ['company_name','website','phone_country','phone_number','city','street','hear_about','contact_full_name','email','c_pasword','password','contact_country','contact_phone','email_domain'];



        $post = $this->input->post();

        foreach($post as $postKey=>$postValue){

            if(in_array($postKey,$requiredArr)){

                if(trim($postValue) == ''){

                    exit('Please fill the required fields.');

                }

            }

        }

        if (!filter_var($this->input->post('email'), FILTER_VALIDATE_EMAIL)) {

            exit('Your email is invalid.');

        }



        $data 			= [];

        $data['company_name'] 		= $post['company_name'];

        $data['website'] 			= $post['website'];

        $data['phone_country_code'] = $post['phone_country'];

        $data['phone_number'] 		= $post['phone_number'];

        $data['phone_ex'] 			= $post['phone_ex'];

        $data['country'] 			= $post['country'];

        $data['state'] 				= $post['state'];

        $data['city'] 				= $post['city'];

        $data['street'] 			= $post['street'];

        $data['company_peoples'] 	= $post['how_many_people'];

        $data['hear_about'] 		= $post['hear_about'];

        $data['other_hear'] 		= isset($post['other_hear']) ? $post['other_hear'] : '';

        $data['contact_full_name'] 	= $post['contact_full_name'];

        $data['email'] 				= $post['email'];

        $data['password'] 			= base64_encode($post['password']);

        $data['contact_country_code']= $post['contact_country'];

        $data['contact_phone'] 		= $post['contact_phone'];



        $domain = trim($post['email']);

        $domain = explode('@',$domain);

        $domain = trim($domain[1]);

        $data['email_domain'] 		= $domain;



        $data_user	= [];

        $data_user['var_email'] 	= trim($post['email']);

        $data_user['var_password'] 	= base64_encode($post['password']);

        $data_user['var_fname'] 	= $post['contact_full_name'];

        $data_user['var_lname'] 	= '';

        $data_user['var_phone'] 	= $post['contact_country'].$post['contact_phone'];

        $data_user['var_country'] 	= 'null';

        $data_user['chr_user_type'] = 'EN';

        $data_user['dt_created_date'] = date('Y-m-d H:i:s');

        $data_user['var_signupip'] 	= $this->getIP();

        $data_user['var_accountid'] = $this->generateAccountId();

        $data_user['profile_pic'] = 'null';

        $data_user['chr_status'] = 'AP';



        $alreadyExist = $this->db->select('int_glcode')->get_where('dz_user',['var_email'=>trim($post['email'])])->result_array();

        if(empty($alreadyExist)){



            $this->db->insert('dz_user', $data_user);

            $lastInsertedId = $this->db->insert_id();

            $data['user_id'] = $lastInsertedId;

            $this->db->insert('entity_detail', $data);

            $this->session->set_userdata('signup_data',$data);

            $this->send_mail(trim($post['email']),'Entity Sign-up',$data);



            echo 'success';

            exit;

        }else{

            echo 'Sorry! You are already signup with this email.';

            exit;

        }

    }

    function checkemail(){

        if (!filter_var($this->input->post('email'), FILTER_VALIDATE_EMAIL)) {

            echo "error";

            exit();

        }

        $explodedEmail = explode('@', $this->input->post('email'));

        $domain = array_pop($explodedEmail);

        $this->db->where('var_email LIKE ', '%'.$domain.'%');

        $query = $this->db->get('dz_user')->num_rows();

        $query = 0;

        if ($query == 0) {

            $domain = trim($this->input->post('email'));

            $domain = explode('@',$domain);

            $domain = trim($domain[1]);



            $this->db->where('domain', $domain);

            $queryStrict = $this->db->get('strict_domains')->num_rows();

            $queryStrict = 0;

            if((int)$queryStrict == 0){

                echo "success";

            }else{

                echo 'strict';

            }

            exit;

        } else {

            echo "error";

            exit;

        }

    }

    private function getIP() {

        $tmp = getenv("HTTP_CLIENT_IP");

        if ( $tmp && !strcasecmp( $tmp, "unknown"))

            return $tmp;



        $tmp = getenv("HTTP_X_FORWARDED_FOR");

        if( $tmp && !strcasecmp( $tmp, "unknown"))

            return $tmp;

        $tmp = getenv("REMOTE_ADDR");

        if($tmp && !strcasecmp($tmp, "unknown"))

            return $tmp;



        return("unknown");

    }



    private function send_mail($to, $subject, $data) {

        $from_email = NOREPLY;

        $to_email = $to;



        //Load email library

        $this->load->library('email');

        $this->email->set_mailtype("html");

        $this->email->from($from_email, SITE_NAME);

        $this->email->to($to_email);

        $this->email->subject($subject);



        $message = $this->load->view('front/entity/signup_email', ['data'=>$data], TRUE);



        $this->email->message($message);

        $this->email->send();

    }



    function thanks(){

        //masterLoginCheck();

        $data['data'] = $this->session->userdata('signup_data');

        //$this->send_mail('adnankhanbs@gmail.com', 'testing', $data['data'] );

        $data['page'] = "front/entity/thankyou";

        $this->load->view(FRONT_LAYOUT, $data);

    }



    function thankYou(){



        $data['data'] = $this->session->userdata('signup_data');

        $data['page'] = "front/entity/sub_thanks";

        $this->load->view(FRONT_LAYOUT, $data);

    }



    function states($country_id){

        $states	= $this->db->get_where('states',['country_id'=>$country_id])->result_array();

        echo json_encode($states);exit;

    }

    function generateAccountId($numno1 = 3, $numno2 = 4) {

        $listno = '01234567';

        return str_shuffle(

            substr(str_shuffle($listno), 0, $numno1) .

            substr(str_shuffle($listno), 0, $numno2)

        );

    }

    function faqs(){

        masterLoginCheck();

        $this->page_name = 'Faq';

        $this->parent_menu = '';



        $data['page'] = "front/entity/Entity_faq";

        $this->load->view(FRONT_LAYOUT, $data);

    }

    function account_info(){

        masterLoginCheck();

        $this->page_name = 'account_info';

        $this->parent_menu = 'MY_ACCOUNT';

        $data['countries'] 		= $this->db->order_by("name", "asc")->get_where('countries')->result_array();

        $data['hear_abouts'] = ['Social Media'=>'Social Media','Another Travel Agent'=>'Another Travel Agent','Media (TV/Radio/Magazine)'=>'Media (TV/Radio/Magazine)','Email'=>'Email','Other'=>'Other'];



        $allSession  = $this->session->all_userdata();

        $this->db->select('dz_user.*, entity_detail.*, users_memberships.endDate,users_memberships.isLifeTime,countries.name as country_name');

        $this->db->from('dz_user');

        $this->db->join('entity_detail', 'entity_detail.user_id= dz_user.int_glcode');

        $this->db->join('countries', 'entity_detail.country = countries.id', 'left');

        $this->db->join('users_memberships', 'dz_user.int_glcode = users_memberships.userId', 'left');

        $this->db->where('dz_user.int_glcode', $allSession['entity_detail']['user_id']);

        $user_info =  $this->db->get();



        $data['user_info'] = (array)$user_info->result()[0];



        $data['how_many_people_arr'] = $this->db->order_by("id", "asc")->get_where('company_people_range')->result_array();

        $data['page'] = "front/entity/account_info";



        $this->load->view(FRONT_LAYOUT, $data);

    }

    function profile_update_post(){

        masterLoginCheck();

        $allSession  = $this->session->all_userdata();



        $requiredArr = ['phone_country','phone_number','city','street','contact_full_name','contact_country','contact_phone'];

        $post = $this->input->post();

        foreach($post as $postKey=>$postValue){

            if(in_array($postKey,$requiredArr)){

                if(trim($postValue) == ''){

                    exit('Please fill the required fields.');

                }

            }

        }



        $data 			= [];



        $data['phone_country_code'] 	= $post['phone_country'];

        $data['phone_number'] 	= $post['phone_number'];

        $data['phone_ex'] 		= $post['phone_ex'];

        $data['state'] 			= $post['state'];

        $data['city'] 			= $post['city'];

        $data['street'] 		= $post['street'];

        $data['company_peoples'] 		= $post['how_many_people'];

        $data['contact_full_name'] = $post['contact_full_name'];

        $data['contact_country_code']= $post['contact_country'];

        $data['contact_phone'] 	= $post['contact_phone'];



        $this->db->where('user_id', $allSession['entity_detail']['user_id']);

        $this->db->update('entity_detail', $data);



        $data_user = [];

        $data_user['var_fname']	= $post['contact_full_name'];

        $data_user['var_lname']	= '';

        $data_user['var_phone']	= $post['contact_country'].$post['contact_phone'];



        $this->db->where('int_glcode', $allSession['entity_detail']['user_id']);

        $this->db->update('dz_user', $data_user);



        /*****Update Session Start****************/

        $entity_detail = $this->db->get_where('entity_detail',['user_id'=>trim($allSession['entity_detail']['user_id'])])->result_array();

        $arr['entity_detail'] = $entity_detail[0];

        $this->session->set_userdata($arr);

        /*****Update Session Start****************/

        echo 'success';

        exit;

    }

    function password_update(){

        masterLoginCheck();

        $this->page_name = 'Change Password';

        $this->parent_menu = 'myaccount';

        $data['js'] = array(

            'front/travel_professional/change_password.js'

        );

        $data['js_plugin'] = array(

            'jquery-validation/dist/jquery.validate.min.js',

            'jquery-validation/dist/additional-methods.min.js',

            'bootstrap-wizard/jquery.bootstrap.wizard.min.js',

            'select2/select2.min.js',

            'bootstrap-datepicker/js/bootstrap-datepicker.js',

            'bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',

            'bootstrap-maxlength/bootstrap-maxlength.min.js'

        );

        $data['css_plugin'] = array(

            'select2/select2-metronic.css',

            'select2/select2.css',

        );

        $data['init'] = array('Change_password.init()');

        $data['page'] = "front/entity/password_update";





        if ($this->input->post()) {



            $oldpass = $this->input->post('old_pass');

            $id = $this->session->userdata['valid_user']['id'];

            $this->db->where('int_glcode', $id);

            $this->db->where('var_password', base64_encode($oldpass));

            $query = $this->db->get('dz_user');



            if ($query->num_rows() == 1) {



                $content = array(

                    'var_password' => base64_encode($this->input->post('new_pass')),

                );

                $this->db->where('int_glcode', $id);

                $row = $this->db->update('dz_user', $content);



                if ($row){

                    $this->db->where('int_glcode', $id);

                    $query2 = $this->db->get('dz_user');

                    $name = $query2->result_array();

                    $this->load->library('email');

                    $this->email->set_mailtype("html");

                    $this->email->from(NOREPLY, SITE_NAME);

                    $this->email->to($name[0]['var_email']);

                    $this->email->subject('Change Password');



                    $mail_body = '<p style="color:#fff;margin-bottom:10px;">Dear  ' . $name[0]['var_fname'] . '</p>';

                    $mail_body .='<p style="color:#fff;margin-bottom:10px;">Thank you for using <b>'.SITE_NAME.'<sup>sm</sup></b>. We are glad to confirm that your password is now successfully changed! Your new password is <b>'.$this->input->post('new_pass').'</b></p>';

                    $mail_body .='<p style="color:#fff;margin-bottom:10px;">Again, thank you for choosing us and please always give us a try before booking your next trip!</p>';

                    $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Sincerely,</br>';

                    $mail_body .= '<p style="color:#fff;margin-bottom:10px;"><b>'.SITE_NAME.'<sup>sm</sup></b><br>

					<small style="font-style: italic;">Where better deals are made for YOU!</small><b><sup>sm</sup></b>

					</p>';

                    $data['mail_body'] = $mail_body;

                    $message = $this->load->view('admin/account/genral_emailtemplate', $data, TRUE);

                    $this->email->message($message);

                    $res = $this->email->send();



                    if ($res){

                        echo "success";

                    }else{

                        echo "email error";

                    }

                }else{

                    echo "error";

                }

            }else{

                echo "error";

            }

            exit;

        }

        $this->load->view(FRONT_LAYOUT, $data);

    }

    function employee_database(){

        masterLoginCheck();

        $allSession  = $this->session->all_userdata();



        $this->page_name = 'employee_database';

        $this->parent_menu = '';



        $data['js_plugin'] = array(

            'jquery-validation/dist/jquery.validate.min.js',

            'jquery-validation/dist/additional-methods.min.js',

            'bootstrap-wizard/jquery.bootstrap.wizard.min.js',

            'select2/select2.min.js',

            'bootstrap-datepicker/js/bootstrap-datepicker.js',

            'bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',

            'bootstrap-maxlength/bootstrap-maxlength.min.js'

        );

        $data['css_plugin'] = array(

            'select2/select2-metronic.css',

            'select2/select2.css',

        );



        $data['page'] = "front/entity/employee_database";



        if ($this->input->post()) {

            $post = $this->input->post();

            if(!isset($post['auth_code']) || trim($post['auth_code']) == ''){

                $data['has_error'] = "Please enter your 7-digit authorization code.";

            }else{

                $this->db->where('int_glcode', $allSession['entity_detail']['user_id']);

                $query = $this->db->get('dz_user');

                $data_query = $query->result();

                $data_query = $data_query[0];

                if(!empty($data_query)){

                    if($data_query->entity_sec_code != $post['auth_code']){

                        $data['has_error'] = "You have entered an invalid code.";

                    }

                    if($data_query->entity_sec_code == $post['auth_code']){

                        $this->session->set_userdata('valid_account',$data_query->var_accountid);

                        redirect('/front/entity/employee_database');

                        exit;

                    }

                }



            }

        }

        if(isset($allSession['valid_account']) && $allSession['valid_account'] != ''){

            $this->db->where('entity_parent_account', $allSession['entity_detail']['user_id']);

            $this->db->where('chr_status', 'A');

            $query_child = $this->db->get('dz_user');

            $data['child_users'] = $query_child;

        }

        $this->load->view(FRONT_LAYOUT, $data);

    }

    function entity_sub_signup($account_id){





        $this->page_name = 'employee_database';

        $this->parent_menu = '';

        $data['account_id'] = $account_id;

        $data['page'] = "front/entity/sub_user_signup";



        $this->db->where('var_accountid', $account_id);

        $query_parent_account = $this->db->get('dz_user');

        $query_parent_account = (array)$query_parent_account->result()[0];

        // check membrship status

        checkUserMembershipStatus($query_parent_account['int_glcode'],array('expiryPage'=>"/user/myaccount/entity_expired"));

        $this->db->where('user_id', $query_parent_account['int_glcode']);

        $entity_detail = $this->db->get('entity_detail');

        $entity_detail = (array)$entity_detail->result()[0];



        $this->db->where('chr_status', "A")->where("chr_user_type","EN")->where("entity_parent_account",$query_parent_account['int_glcode']);

        $totalChild = $this->db->get('dz_user');

        $totalChild = (array)$totalChild->result();

        $totalChild = count($totalChild);

        if($entity_detail['company_peoples'] != -1) {

            if ($totalChild >= $entity_detail['company_peoples']) {

                $data['showForm'] = false;

            } else {

                $data['showForm'] = true;

            }

        }else{

            $data['showForm'] = true;

        }



        $this->load->view(FRONT_LAYOUT, $data);

    }

    function checkemail_sub(){

        if (!filter_var($this->input->post('email'), FILTER_VALIDATE_EMAIL)) {

            echo "error";

            exit();

        }

        $parent_account = $this->input->post('parent_account');

        $email = $this->input->post('email');

        if($parent_account != ''){

            $this->db->where('var_accountid', $parent_account);

            $query_parent_account = $this->db->get('dz_user');

            if($query_parent_account->num_rows() > 0){



                $query_parent_account = (array)$query_parent_account->result()[0];

                $parent_account_email = $query_parent_account['var_email'];

                $parent_account_email = explode('@',$parent_account_email);

                $parent_account_email_domain = $parent_account_email[1];





                $post_email = explode('@',$email);

                $post_email = $post_email[1];



                if($parent_account_email_domain != $post_email){

                    echo "error";

                    exit;

                }else{

                    $this->db->where('var_email', $this->input->post('email'));

                    $query = $this->db->get('dz_user')->num_rows();

                    if ($query == 0) {

                        echo "success";

                        exit;

                    } else {

                        echo "error";

                        exit;

                    }

                }

            }else{

                echo "error";

                exit;

            }

        }

    }

    function signup_post_sub_user(){

        $requiredArr = ['var_fname','var_lname','email','password','c_pasword','account_id'];

        $post = $this->input->post();

        foreach($post as $postKey=>$postValue){

            if(in_array($postKey,$requiredArr)){

                if(trim($postValue) == ''){

                    exit('Please fill the required fields.');

                }

            }

        }





        $this->db->where('var_accountid', $post['account_id']);

        $query_parent_account = $this->db->get('dz_user');

        $query_parent_account = (array)$query_parent_account->result()[0];





        $data_user 			= [];

        $data_user['var_fname'] 		= $post['var_fname'];

        $data_user['var_lname'] 		= $post['var_lname'];

        $data_user['var_email'] 			= $post['email'];

        $data_user['var_password'] 			= base64_encode($post['password']);

        $data_user['var_country'] 	= '';

        $data_user['chr_user_type'] = 'EN';

        $data_user['dt_created_date'] = date('Y-m-d H:i:s');

        $data_user['var_signupip'] 	= $this->getIP();

        $data_user['var_accountid'] =  $this->generateAccountId(4,4);

        $data_user['entity_parent_account'] = $query_parent_account['int_glcode'];

        $data_user['profile_pic'] = 'null';

        $data_user['chr_status'] = 'P';



        $alreadyExist = $this->db->select('int_glcode')->get_where('dz_user',['var_email'=>trim($post['email'])])->result_array();

        if(empty($alreadyExist)){



            $this->db->insert('dz_user', $data_user);

            $lastInsertedId = $this->db->insert_id();



            $data_user['inserted_id'] = $lastInsertedId;

            $this->session->set_userdata('signup_data',$data_user);

            $this->send_mail_sub_entity(trim($post['email']),'Sub Entity Sign-up',$data_user);



            echo 'success';

            exit;

        }else{

            echo 'Sorry! You are already signup with this email.';

            exit;

        }

    }

    private function send_mail_sub_entity($to, $subject, $data) {

        $from_email = NOREPLY;

        $to_email = $to;



        //Load email library

        $this->load->library('email');

        $this->email->set_mailtype("html");

        $this->email->from($from_email, SITE_NAME);

        $this->email->to($to_email);

        $this->email->subject($subject);



        $message = $this->load->view('front/entity/sub_entity_signup_email', ['data'=>$data], TRUE);

        $this->email->message($message);

        $this->email->send();

    }

    function delete_sub_user(){

        masterLoginCheck();

        $post = $this->input->post();

        if(isset($post['uid']) && $post['uid'] != ''){

            $this->db->where('int_glcode', $post['uid']);

            $this->db->delete('dz_user');



            $this->db->where('userId', $post['uid']);

            $this->db->delete('users_memberships');



            exit('success');

        }



    }

    function incentives(){

        masterLoginCheck();

        $this->page_name = 'incentives';

        $data['page'] = "front/entity/incentives";

        $this->load->view(FRONT_LAYOUT, $data);

    }



    function activation($id){

        $key = explode("-",base64_decode($id));

        $id = $key[0];

        $exist = $this->db->select('int_glcode,entity_parent_account')->get_where('dz_user',['int_glcode'=>$id,'chr_status'=>'P'])->result_array();



        if(!empty($exist)){

            // get activation date of master;

            $master = $this->db->select('int_glcode,entity_activate_time,company_peoples,users_memberships.*')->join('users_memberships', 'users_memberships.userId = dz_user.int_glcode')->join('entity_detail', 'entity_detail.user_id= dz_user.int_glcode')->get_where('dz_user',['int_glcode'=>$exist[0]['entity_parent_account'],'chr_status'=>'A'])->result_array();



            if(count($master)>0) {



                $this->db->where('chr_status', "A")->where("chr_user_type","EN")->where("entity_parent_account",$exist[0]['entity_parent_account']);

                $totalChild = $this->db->get('dz_user');

                $totalChild = (array)$totalChild->result();

                $totalChild = count($totalChild);



                $date = date("Y-m-d",strtotime($master[0]['startDate']));

                $date1 = new DateTime(date('Y-m-d'));

                $date2 = new DateTime($date);

                $interval = $date1->diff($date2);

                //$remainingDays = SUB_ENTITY_MAX_DAYS - $interval->d;

                $remainingDays = $master[0]['days'] - $interval->d;

                $rec = array(

                    'userId' => $id,

                    'startDate' => date("Y-m-d H:i:s"),

                    'endDate' => date("Y-m-d H:i:s", strtotime("+" . $remainingDays . " days")),

                    //'endDate' => date("Y-m-d H:i:s", strtotime("+1 days")),

                    'plan' => "",

                    'amountPerMonth' => 0,

                    'stripeInfo' => "",

                    'paid' => 0,

                    'description' => 'Sub entity membership',

                    'status' => 1,

                    'days' => $remainingDays

                );

                if($master[0]['company_peoples'] == -1 || $totalChild < $master[0]['company_peoples']){

                    if($master[0]['company_peoples'] == -1) {

                        $rec['isLifeTime'] = 1;

                        $rec['days'] = -1;

                    }



                    $row = $this->db->insert('users_memberships', $rec);



                    $data['msg'] = 'YOU DID IT! Congratulations, your account is now active and ready to use. <a href="'.site_url().'login"> Click on </a> LOGIN to access your account. See you soon!';



                    $this->db->where('int_glcode', $id);

                    $this->db->update('dz_user', array('is_subscribed'=>1,'chr_status'=>'A','entity_activate_time'=>date('Y-m-d H:i:s')));

                }else if ($totalChild >= $master[0]['company_peoples']) {

                    $data['msg'] = "We are sorry but your account has reached its limit. If you wish to expand your account please contact us.";

                }



            }else{

                $data['msg'] = "Sorry! No or inactive master account";

            }

        }else{

            $data['msg'] = "Sorry! It looks like that this account has been activated already. If you do not know your login information, please try the 'Forgot password' feature at the login window.";

        }

        $data['page'] = "front/entity/link_activation";



        $this->load->view(FRONT_LAYOUT, $data);

    }



}

?>