<?php

class Quote_request extends Front_Controller {

    function __construct() {

        parent::__construct();
        $this->load->model('front/quote_model');
    }
	
	function pdf($id = false , $saveServer = false)
	{
		if(!isLogin())
		{
			redirect("/");
		}
        //regularUser_SubEntity_TP_Check();
		if($id)
		{
			$loginID = $this->session->userdata['valid_user']['id'];
			$res = $this->db->where('id' , $id)->where('uid' , $loginID)->get('book_hotal')->result();
			if(count($res) > 0 || isAdminLogin())
			{
				$this->load->helper('pdf_helper');
				$data = array();
				tcpdf($id , $saveServer);
			}
			else
			{
				redirect('/');
			}
		}
		else
		{
			redirect('/');
		}
		
	}

    function voucher($id = false , $saveServer = false)
    {
        if(!isLogin())
        {
            redirect("/");
        }
        if($id)
        {
            $loginID = $this->session->userdata['valid_user']['id'];
            $res = $this->db->where('id' , $id)->where('uid' , $loginID)->get('book_hotal')->result();
            if(count($res) > 0 || isAdminLogin())
            {
                $user = $this->db->where('int_glcode' , $loginID)->get('dz_user')->row();
                $this->load->helper('pdf_helper');
                $data = array();
                travelProVoucher($id, $user,$saveServer);
            }
            else
            {
                redirect('/');
            }
        }
        else
        {
            redirect('/');
        }

    }
	
	function view_invoice($id)
	{
		$data['page'] = "front/quote_request/printinvo";
        $this->page_name = 'Quote request';
        $this->load->view(FRONT_LAYOUT, $data);
	}
	
    function index() {

        $data['page'] = "front/quote_request/quote_request";
        $this->page_name = 'Quote request';
        $data['js'] = array(
              'affiliate/req_quote.js',
              'custom/components-pickers.js',
        );
        $data['js_plugin'] = array(
            'bootstrap-fileinput/bootstrap-fileinput.js',
            'bootstrap-datepicker/js/bootstrap-datepicker.js',
        );
        $data['css_plugin'] = array(
            'bootstrap-fileinput/bootstrap-fileinput.css',
            'bootstrap-datepicker/css/datepicker.css'
        );
        $data['css'] = array(
        );
            /*if($this->input->post()){
                
             $res = $this->quote_model->add_req_quote($this->input->post());
             
             if($res){
                 echo "success";
                 exit;
             }else{
                 echo "error";
                 exit;
             }
            }*/
        $data['init'] = array(
            'Req_quote.init()',
            'ComponentsPickers.init_datepicker()'
        );
        $this->load->view(FRONT_LAYOUT, $data);
    }
}
?>