<?php

class Home extends Front_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model('front/quote_model');
        $this->load->model('admin/loginlog_model');
        $this->load->model('signup/signup_model');
        //$this->load->model('signup/signup_affiliate');
        $this->load->helper('tourico_api');
        $this->load->model('tourico_activity');
        $this->load->model('tourico_api/destination_model');
        $this->load->model('tourico_cruise');
    }

    function getimageszie($img)
    {
        $img = 'http://hotels.softech.website/public/affilliate_theme/img/logo.png';
        if (@getimagesize($img))
        {
            echo "<pre>";
            print_r(getimagesize($img));
            echo "</pre>";
        }
        else
        {
            echo $img;
        }
    }


    function lp( $hear_about_us = '' ){

        $data['hear_about_us'] = $hear_about_us;
        if($hear_about_us == 'blog'){
            $this->load->view('front/home/lp', $data);
        }
        if($hear_about_us == 'twitter'){
            $data['inner_text'] = 'GET YOUR ANNUAL MEMBERSHIP NOW FOR <b style="color:#bc0005;">55%</b> WHEN YOU FOLLOW US ON TWITTER <span style="text-decoration:underline;display: inline-block;color:#bc0005;text-decoration:underline;">AND</span> USE COUPON CODE: <b style="color:#bc0005;">TWITTER235</b> (Offer expires 12/31/2017).';
            $this->load->view('front/home/lp2', $data);
        }
        if($hear_about_us == 'facebook'){
            $data['inner_text'] = 'GET YOUR ANNUAL MEMBERSHIP NOW FOR <b style="color:#bc0005;"> 55% </b>WHEN YOU LIKE US ON FACEBOOK <span style="text-decoration:underline;display: inline-block;color:#bc0005;">AND</span> USE COUPON CODE: <b style="color:#bc0005;">FACEBOOK921</b> (Offer expires 12/31/2017).';
            $this->load->view('front/home/lp2', $data);
        }
        if($hear_about_us == 'pinterest'){
            $data['inner_text'] = 'GET YOUR ANNUAL MEMBERSHIP NOW FOR <b style="color:#bc0005;"> 55% </b> WHEN YOU FOLLOW US ON PINTEREST <span style="text-decoration:underline;display: inline-block;color:#bc0005;">AND</span> USE COUPON CODE: <b style="color:#bc0005;">PINTEREST642</b> (Offer expires 12/31/2017).';
            $this->load->view('front/home/lp2', $data);
        }
        if($hear_about_us == 'gplus'){
            $data['inner_text'] = 'GET YOUR ANNUAL MEMBERSHIP NOW FOR <b style="color:#bc0005;"> 55% </b> WHEN YOU FOLLOW US ON GOOGLE PLUS <span style="text-decoration:underline;display: inline-block;color:#bc0005">AND</span> USE COUPON CODE: <b style="color:#bc0005;">GPLUS851</b> (Offer expires 12/31/2017).';
            $this->load->view('front/home/lp2', $data);
        }

        if($hear_about_us == 'instagram'){
            $data['inner_text'] = 'GET YOUR ANNUAL MEMBERSHIP NOW FOR <b style="color:#bc0005;"> 55% </b> WHEN YOU FOLLOW US ON INSTAGRAM <span style="text-decoration:underline;display: inline-block;color:#bc0005">AND</span> USE COUPON CODE: <b style="color:#bc0005;">INSTAGRAM388</b> (Offer expires 12/31/2017).';
            $this->load->view('front/home/lp2', $data);
        }
        if($hear_about_us == 'media'){
            $data['inner_text'] = 'GET YOUR ANNUAL MEMBERSHIP NOW FOR <b style="color:#bc0005;"> 55% </b> WHEN YOU USE COUPON CODE: <b style="color:#bc0005;">MEDIA162</b> (Offer expires 12/31/2017).';
            $this->load->view('front/home/lp2', $data);
        }
    }

    function index()
    {
        $allSession  = $this->session->all_userdata();
        //echo '<pre>';print_r($allSession);exit;
        /*if(isset($allSession['travel_professional_detail']) && (int)$allSession['travel_professional_detail']['welcome_show'] == 0){
            redirect("front/travel_professional/congratulation");
            exit;
        }*/
        $data['var_meta_title'] = 'Wholesale Hotels Group – Great travel deals for Corporate Travel and Individuals';
        $data['var_meta_description'] = 'Get WHOLESALE HOTEL inventories and deals worldwide for Corporate Travel, Non-Profit travel, Government travel, Travel Agents and Individual users.';
        $data['var_meta_keyword'] = 'Wholesale Hotels Group, WHotelsGroup.com, wholesale hotel deals, corporate travel, government travel, non-profit travel, travel agency travel, travel agency hotels, wholesale hotels, Online Travel Agency, best OTAs';
        $data['page'] = "front/home/home";
        $this->page_name = 'Home';
        $data['js'] = array(
            'login.js',
            'moment.min.js',
            'front/home.js',
            'affiliate/req_quote.js',
            'custom/components-pickers.js',
            'custom/components-dropdowns.js',
            'front/jquery.bxslider.js',
        );
        $data['js_plugin'] = array(
            'bootstrap-fileinput/bootstrap-fileinput.js',
            // 'bootstrap-datepicker/js/bootstrap-datepicker.js',
            'checkincheckout/bootstrap-datepicker.js',
            'select2/select2.min.js',
            'jquery.bxslider.min.js',
        );
        $data['css_plugin'] = array(
            'bootstrap-fileinput/bootstrap-fileinput.css',
            'bootstrap-datepicker/css/datepicker.css',
            'bootstrap-select/bootstrap-select.min.css',
            'select2/select2.css',
            'select2/select2-metronic.css',
            'jquery-ui/jquery-ui.css',
            'jquery.bxslider.css',
        );
        $data['css'] = array(
        );
        $data['init'] = array(
            'Req_quote.comments_placeholder()',
            'Login.init()',
            'Home.init()',
            'Req_quote.init()',
            'ComponentsPickers.init_datepicker()',
        );

        $hotelSearchAPIResponseProcessObj = new HotelSearchAPIResponseProcess();
        $data['brandList'] = $hotelSearchAPIResponseProcessObj->ProcessBrandList();

        $activity_obj = new Tourico_Activity();
        $data['activities'] = $activity_obj->GetActivitiesNameIdPair();

        $cruise_obj = new Destination_model();
        $cruise_list = $cruise_obj->GetCruiseSuggestionLists();
        $cruise_list_count = count($cruise_list);

        for ($i = 0; $i < $cruise_list_count; $i++) {
            if (0 == $cruise_list[$i]['parent_id']) {
                $cruise_list_parent[] = $cruise_list[$i];
            } else {
                $cruise_list_child[] = $cruise_list[$i];
            }
        }

        $cruise_lines_obj = new Tourico_Cruise();
        $cruise_lines_list = $cruise_lines_obj->GetCruiseLines();
        $data['cruise_line_lists'] = $cruise_lines_list;

        $data['cruise_length'] = $cruise_lines_obj->GetCruiseLength();
        $data['cruise_list_parent'] = $cruise_list_parent;
        $data['cruise_list_child'] = $cruise_list_child;
        $data['cruise_departure_month_options'] = $this->GetCruiseDepartureMonthOptions();

        $data['cruise_list'] = $cruise_list;
        $data['vehicle_types'] = array(
            "1" => "Car",
            "2" => "Van",
            "3" => "SUV",
            "4" => "Convertible",
            "6" => "Station Wagon",
            "7" => "Pickup",
            "9" => "Recreational",
            "10" => "Sport",
            "11" => "Special",
            "12" => "Limo",
            "15" => "4-Wheel Drive"
        );

        $data['car_company_id'] = array(
            "1" => "Budget",
            "2" => "Hertz",
            "3" => "Thrifty",
            "4" => "Enterprise",
            "5" => "National",
            "7" => "Alamo",
            "9" => "Fox",
            "11" => "Dollar",
            "13" => "Sixt",
            "15" => "Avis"
        );

        $data['transmission_type'] = array(
            "1" => "Manual Transmission",
            "2" => "Automatic Transmission"
        );



        if ($this->input->post()) {
            $city = $this->input->post('city');
            $this->db->select('dz_text');
            $this->db->where('var_city', $city);
            $data['dz_text'] = $this->db->get('dz_savingdatabase')->result_array();
            echo json_encode($data['dz_text']);
            exit;
        } else {
            $this->db->select('dz_text');
            $data['dz_text'] = $this->db->get('dz_savingdatabase')->result_array();
        }
        $this->db->select('dz_city.int_glcode,dz_city.var_city,dz_state.var_state,dz_country.var_country');
        $this->db->from('dz_city');
        $this->db->join('dz_state', 'dz_city.fk_state = dz_state.int_glcode', 'left');
        $this->db->join('dz_country', 'dz_city.fk_country = dz_country.int_glcode', 'left');
        $data['city'] = $this->db->get()->result_array();

        $this->db->select('var_hotelname,dz_city.var_city,dz_state.var_state,dz_country.var_country');
        $this->db->from('dz_hotel');
        $this->db->join('dz_city', 'dz_hotel.fk_city = dz_city.int_glcode', 'left');
        $this->db->join('dz_state', 'dz_hotel.fk_state = dz_state.int_glcode', 'left');
        $this->db->join('dz_country', 'dz_hotel.fk_country = dz_country.int_glcode', 'left');
        $data['hotels'] = $this->db->get()->result_array();


        $this->db->select('*');
        $this->db->where('int_glcode', '1');
        $this->db->from('dz_maincounter');
        $data['maincounter'] = $this->db->get()->result_array();
        $this->load->view(FRONT_LAYOUT, $data);
    }

    function searchgetdiscount() {

        if ($this->input->post()) {
            $city = $this->input->post('city');
            $state = explode(',', $city);

            if (count($state) == 4) {
                $finalcity = $state[1];
            }
            if (count($state) == 3) {
                $finalcity = $state[0];
            }
            if (count($state) == 5) {
                $finalcity = $state[2];
            }

            $this->db->select('dz_text,int_glcode,dt_updateddate,int_saved');
            $this->db->where('var_city', $finalcity);
            $this->db->order_by('int_saved', 'desc');
            $city_data = $this->db->get('dz_savingdatabase')->result_array();
            $html = array();

            if (count($city_data) >= 1) {
                $html[] = '<div class="content_box slide" style="background:none; border:none; box-shadow:none;"></div>';
            }

            for ($i = 0; $i < count($city_data); $i++) {
                $strdate = date('Y-m-d', strtotime($city_data[$i]["dt_updateddate"]));
                $today = date('Y-m-d');
                $pastsevenday = date('Y-m-d', strtotime('-7 days'));
                if (($today >= $strdate) && ($strdate > $pastsevenday)) {
                    $html[] = '<div class="content_box slide">
                            ' . $city_data[$i]["dz_text"] . '
                        </div>';
                }
            }
            echo json_encode($html);
            exit;
        }
    }

    function add_quote() {
        $res = $this->quote_model->add_req_quote($this->input->post());
        if ($res) {
            delete_cookie('cookesdata');
            echo "success";
            exit;
        } else {
            echo "error";
            exit;
        }
    }

    function member_signup($account_id) {
        if (!empty($this->session->userdata['valid_user']) || !empty($this->session->userdata['valid_affiliate']))
        {
            redirect("/");
            exit();
        }
        $data['hear_about_us'] = isset($_GET['hear_about_us']) ? $_GET['hear_about_us'] : '';
        $data['page'] = "front/home/member_signup";
        $this->page_name = 'Member Signup';
        $data['js'] = array(
            'custom/form-wizard.js',
            'custom/components-pickers.js',
        );
        $data['var_meta_title'] = 'Wholesale Hotels Group – Great travel deals for Corporate Travel and Individuals';
        $data['var_meta_description'] = 'Get WHOLESALE HOTEL inventories and deals worldwide for Corporate Travel, Non-Profit travel, Government travel, Travel Agents and Individual users.';
        $data['var_meta_keyword'] = 'Wholesale Hotels Group, WHotelsGroup.com, wholesale hotel deals, corporate travel, government travel, non-profit travel, travel agency travel, travel agency hotels, wholesale hotels, Online Travel Agency, best OTAs';


        $data['js_plugin'] = array(
            'jquery-validation/dist/jquery.validate.min.js',
            'jquery-validation/dist/additional-methods.min.js',
            'bootstrap-wizard/jquery.bootstrap.wizard.min.js',
            'select2/select2.min.js',
            'bootstrap-datepicker/js/bootstrap-datepicker.js',
            'bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',
            'bootstrap-maxlength/bootstrap-maxlength.min.js'
        );
        $data['css_plugin'] = array(
            'select2/select2-metronic.css',
            'select2/select2.css',
        );
        $data['css'] = array(
        );
        $data['init'] = array(
            'FormWizard.init()',
            'ComponentsPickers.init()',
        );
        $data['sign_status'] = $this->db->get('dz_singup_coupons')->result_array();

        $this->db->where('var_accountid', $account_id);
        $data['info'] = $this->db->get('dz_user')->result_array();

        $this->db->where('var_signupip', $_SERVER['REMOTE_ADDR']);
        $data['ipaddress'] = $this->db->get('dz_user')->result_array();
        $data['previousUrl'] = basename(($_SERVER['HTTP_REFERER']));
        //var_dump(basename(($_SERVER['HTTP_REFERER'])));exit;
        if (!empty($data['info']))
        {
            $data['parentid'] = $data['info'][0]['int_glcode'];
        }
        else
        {
            $data['parentid'] = 0;
        }

        $this->load->view(FRONT_LAYOUT, $data);
    }

    function sharefacebook($userid)
    {
        $title = "Try this to get the CHEAPEST HOTEL DEALS! Get $20 OFF for your booking as a bonus: <a color='color:red;' href='" . base_url() . "front/home/member_signup/" . $userid . "'>" . $userid . "</a>";
        $img = base_url() . "public/affilliate_theme/img/logo.png";
        $data['title'] = urldecode($title);
        $data['desc'] = urldecode($desc);
        $data['img'] = $img;
        $this->load->view('front/home/share', $data);
    }

    function paymentsharefacebook($userid, $amount) {
        $url1 = base_url();
        $title = "I just saved $" . $amount . " using ".SITE_NAME.".Sign up and get $20 OFF! LINK: " . $url1 . "front/home/member_signup/" . $userid;
        $desc = $url1 . "front/home/member_signup/" . $userid;
        //   $desc = "Get a $20 OFF coupon for your booking as a sign up bonus if you use my link: xxx";
        $img = $url1 . "public/affilliate_theme/img/logo.png";
        $data['title'] = urldecode($title);
        $data['desc'] = urldecode($desc);
        $data['img'] = $img;
        $this->load->view('front/home/share', $data);
    }

    /*function affiliate_signup()
	{
        if (!empty($this->session->userdata['valid_user'])) {
            exit();
        }
        $data['page'] = "front/home/affiliate_signup";
        $this->page_name = 'Affiliate Signup';
        $data['js'] = array(
            'front/affiliate_wizard.js',
            'custom/components-pickers.js',
        );
        $data['js_plugin'] = array(
            'jquery-validation/dist/jquery.validate.min.js',
            'jquery-validation/dist/additional-methods.min.js',
            'bootstrap-wizard/jquery.bootstrap.wizard.min.js',
            'select2/select2.min.js',
            'bootstrap-datepicker/js/bootstrap-datepicker.js',
            'bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',
            'bootstrap-maxlength/bootstrap-maxlength.min.js'
        );
        $data['css_plugin'] = array(
            'select2/select2-metronic.css',
            'select2/select2.css',
        );
        $data['css'] = array(
        );
        $data['init'] = array(
            'Affiliate_wizard.init()',
            'ComponentsPickers.init()',
        );
        $this->load->view(FRONT_LAYOUT, $data);
    }*/

    function signUpResendEmail()
    {
        if($this->input->post('email')) {
            $this->db->where('var_email', $this->input->post('email'));
            $query1 = $this->db->get('dz_user')->row_array();
            if (count($query1) > 0) {
                $code = couponcode();
                $query1['code'] = $code;
                signUpActivationEmail($query1);
                echo "Activation email has been sent successfully";
            } else {
                echo "No email found";
            }
        }else{
            echo "Invalid";
        }
        exit;
    }

    function memberregister()
    {
        $captcha=$this->input->post('g-recaptcha-response');

        if(empty($captcha)){
            echo 'captcha_error';exit;
        }

        $this->db->where('var_email', $_POST['email']);
        $query1 = $this->db->get('dz_user')->num_rows();
        if ($query1 == 0) {
            $result = $this->signup_model->signup_mod($this->input->post());
            if ($result) {



                echo "success";
                exit;
            }else{
                echo "error";
                exit;
            }
        }else {
            echo "You have already Registered !";
        }
    }
    private function send_mail($to, $subject, $data) {
        $from_email = NOREPLY;
        $to_email = $to;

        //Load email library
        $this->load->library('email');
        $this->email->set_mailtype("html");
        $this->email->from($from_email, 'Wholesale Hotels Group');
        $this->email->to($to_email);
        $this->email->subject($subject);

        $message = $this->load->view('front/home/ind_signup_email', ['data'=>$data], TRUE);

        $this->email->message($message);
        $this->email->send();
    }
    /*
    function affiliateregister()
    {
        $data = array();
        parse_str($_POST['data'], $data);
        $data['country_full'] = $_POST['country'];
        $this->db->where('var_email', $data['email']);

        $query = $this->db->get('dz_user')->num_rows();
        if ($query == 0)
        {
            $result = $this->signup_affiliate->signup_mod($data);
            if ($result)
            {
                echo "success";
                exit;
            }
            else
            {
                echo "error";
                exit;
            }
        }
        else
        {
            echo "You have already Registered !";
        }
    }
    */

    function checkemail()
    {
        if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
            echo "error";
            exit();
        }
        $this->db->where('var_email', $_POST['email']);
        $query = $this->db->get('dz_user')->num_rows();
        if ($query == 0) {
            echo "success";
            exit;
        } else {
            echo "error";
            exit;
        }
    }

    function checkusername() {
        $this->db->where('var_username', $_POST['username']);
        $query = $this->db->get('dz_user')->num_rows();
        if ($query == 0) {
            echo "success";
            exit;
        } else {
            echo "error";
            exit;
        }
    }

    function check_smtp()
    {
        $this->load->library('email');
        $this->email->set_mailtype("html");
        $this->email->from('raheel@tradehub.pk', 'Member');
        $this->email->to('raheelshehzad188@gmail.com');
        $this->email->subject('Welcome to '.SITE_NAME.'!');
        $message = 'test';
        $this->email->message($message);
        $sendit = $this->email->send();

        echo $this->email->print_debugger();
        die();
    }

    function login()
    {
        if(is_user_login())
        {
            redirect("/");
            exit;
        }
        $data['page'] = "front/login1";
        $this->page_name = "Login";
        $data['var_meta_title'] = 'Wholesale Hotels Group – Great travel deals for Corporate Travel and Individuals';
        $data['var_meta_description'] = 'Get WHOLESALE HOTEL inventories and deals worldwide for Corporate Travel, Non-Profit travel, Government travel, Travel Agents and Individual users.';
        $data['var_meta_keyword'] = 'Wholesale Hotels Group, WHotelsGroup.com, wholesale hotel deals, corporate travel, government travel, non-profit travel, travel agency travel, travel agency hotels, wholesale hotels, Online Travel Agency, best OTAs';
        $data['js'] = array(
            'login.js'
        );
        $data['js_plugin'] = array(
        );
        $data['css'] = array(
        );
        $data['init'] = array(
            'Login.init()'
        );
        $data['username'] = " ";
        $data['password'] = " ";

        if ($this->input->post()) {

            $this->email = $this->input->post('username');
            $this->password = $this->input->post('password');
            if ($this->email && $this->password) {
                $row = $this->login_check();
                $logid = $this->loginlog_model->add_loginlog($this->input->post());
                $remember = $this->input->post('remeber');
                if ($remember == 'on')
                {
                    $cookie_username = array(
                        'name' => 'username',
                        'value' => $this->input->post('username'),
                        'expire' => '86500',
                        'domain' => $this->input->server(),
                        'path' => '/',
                    );
                    $this->input->set_cookie($cookie_username);
                    $cookie_password = array(
                        'name' => 'password',
                        'value' => $this->password,
                        'expire' => '86500',
                        'domain' => $this->input->server(),
                        'path' => '/',
                    );
                    $this->input->set_cookie($cookie_password);
                }
                else
                {
                    $cookie_username = array(
                        'name' => 'username',
                        'value' => '',
                        'expire' => '0',
                        'domain' => $this->input->server(),
                        'path' => '/',
                    );
                    $cookie_password = array(
                        'name' => 'password',
                        'value' => '',
                        'expire' => '0',
                        'domain' => $this->input->server(),
                        'path' => '/',
                    );
                    delete_cookie($cookie_username);
                    delete_cookie($cookie_password);
                }
                if ($row)
                {
                    $id = $row->int_glcode;
                    $this->db->select('int_logincnt');
                    $this->db->where('int_glcode', $id);
                    $data = $this->db->get('dz_user')->result_array();
                    $cntvalue = $data[0]['int_logincnt'];
                    if ($row->chr_user_type == "AF" && $row->chr_status == "A")
                    {
                        $arr = array(
                            'valid_affiliate' => array('lastactivity' => time(), 'username' => $row->var_username, 'var_email' => $row->var_email, 'id' => $row->int_glcode, 'var_usertype' => $row->chr_user_type));
                        $this->session->set_userdata($arr);
                        echo "f";
                        $data_array = array(
                            'var_status' => 'OK',
                        );
                        $this->db->where('int_glcode', $logid);
                        $this->db->update('dz_login_log', $data_array);
                    }
                    elseif ($row->chr_user_type == "U")
                    {
                        $arr = array(
                            'valid_user' => array('lastactivity' => time(), 'username' => $row->var_username, 'var_email' => $row->var_email, 'id' => $row->int_glcode, 'var_usertype' => $row->chr_user_type,'is_subscribed' => $row->is_subscribed));
                        $this->session->set_userdata($arr);

                        if($row->is_subscribed == 1)
                            echo "u";
                        else if($row->is_subscribed == 0)
                            echo 'enrolling';
                        else if($row->is_subscribed == 2)
                            echo 'verification';
                        else if($row->is_subscribed == 3)
                            echo 'expired';
                        else if($row->is_subscribed == 4)
                            echo 'unverified';

                        $data_array = array(
                            'var_status' => 'OK',
                        );
                        $this->db->where('int_glcode', $logid);
                        $this->db->update('dz_login_log', $data_array);

                        if ($this->input->post('select_city') != "" && $this->input->post('fname') != "") {
                            $value = array(
                                'select_city' => $this->input->post('select_city'),
                                'chackin' => $this->input->post('chackin'),
                                'chackout' => $this->input->post('chackout'),
                                'room' => $this->input->post('room'),
                                'adults' => $this->input->post('adults'),
                                'children' => $this->input->post('children'),
                                'fname' => $this->input->post('fname'),
                                'lname' => $this->input->post('lname'),
                                'hotelname' => $this->input->post('hotelname'),
                                'star' => $this->input->post('star'),
                                'comments' => $this->input->post('comments'),
                                'night' => $this->input->post('night'),
                            );

                            $str = json_encode($value);

                            $cookie_select_city = array(
                                'name' => 'cookesdata',
                                'value' => $str,
                                'expire' => '100',
                                'domain' => $this->input->server(),
                                'path' => '/',
                            );
                            $this->input->set_cookie($cookie_select_city);
                        }
                    }
                    elseif ($row->chr_user_type == "TF" && $row->chr_status == "A")
                    {
                        // set layout in session
                        $this->session->set_userdata('menu_layout',FRONT_LAYOUT);

                        $arr = array(
                            'valid_user' => array('lastactivity' => time(), 'username' => $row->var_username, 'var_email' => $row->var_email, 'id' => $row->int_glcode, 'var_usertype' => $row->chr_user_type));

                        $travel_professional_detail = $this->db->get_where('travel_professional_detail',['user_id'=>trim($row->int_glcode)])->result_array();

                        $arr['travel_professional_detail'] = $travel_professional_detail[0];
                        $this->session->set_userdata($arr);
                        echo "tf";
                        $data_array = array(
                            'var_status' => 'OK',
                        );
                        $this->db->where('int_glcode', $logid);
                        $this->db->update('dz_login_log', $data_array);
                    }
                    elseif ($row->chr_user_type == "EN" && $row->chr_status == "A")
                    {
                        if($row->entity_parent_account == 0) {
                            // set layout in session
                            $this->session->set_userdata('menu_layout', FRONT_LAYOUT);
                        }

                        $arr = array(
                            'valid_user' => array('entity_parent_account'=>$row->entity_parent_account,'lastactivity' => time(), 'username' => $row->var_username, 'var_email' => $row->var_email, 'id' => $row->int_glcode, 'var_usertype' => $row->chr_user_type));

                        $travel_professional_detail = $this->db->get_where('entity_detail',['user_id'=>trim($row->int_glcode)])->result_array();

                        $arr['entity_detail'] = $travel_professional_detail[0];
                        $this->session->set_userdata($arr);
                        if($row->is_subscribed == 1)
                            echo "en";
                        else // else case will be expired for entity
                            echo 'entity_expired';
                        $data_array = array(
                            'var_status' => 'OK',
                        );
                        $this->db->where('int_glcode', $logid);
                        $this->db->update('dz_login_log', $data_array);
                    }
                    else
                    {
                        echo "error";
                    }
                }
                exit();
            }
        } else {
            $this->load->view(LOGIN_LAYOUT, $data);
        }
    }

    function login_check()
    {
        $row = $this->toval->idtorow('var_email', $this->email, 'dz_user');

        if (!empty($row))
        {
            if ($row->chr_status == "A")
            {
                if ($row->var_password == base64_encode($this->password))
                {
                    $id = $row->int_glcode;
                    $login_count = array(
                        'int_logincnt' => '0',
                    );
                    $this->db->where('int_glcode', $id);
                    $this->db->update('dz_user', $login_count);
                    $this->db->where('ip' , $_SERVER['REMOTE_ADDR'])->delete('ip_wrong_request');
                    return $row;

                } else {

                    date_default_timezone_set(AMERICA_LOS_ANGLES_TIME_ZONE);
                    $id = $row->int_glcode;
                    $this->db->where('int_glcode', $id);
                    $query = $this->db->get('dz_user');
                    $data = $query->result_array();
                    $cnt = $this->loginlog_model->addlogincnt1($_SERVER['REMOTE_ADDR']);
                    $count = 5 - $cnt;
                    if ($count <= 0)
                    {
                        $startdate = date('Y-m-d H:i:s');
                        $enddate = date('Y-m-d H:i:s', strtotime('+6 hours'));
                        $ip_ban = array(
                            'fk_user' => $id,
                            'var_ipban' => $_SERVER['REMOTE_ADDR'],
                            'var_banaue' => $data[0]['var_username'],
                            'var_reason' => 'Failed login attempts',
                            'chr_status' => 'B',
                            'var_banby' => 'SYSTEM',
                            'date_banstart_time' => $startdate,
                            'date_banend_time' => $enddate,
                            'var_createddate' => date('Y-m-d H:i:s'),
                            'var_updateddate' => date('Y-m-d H:i:s'),
                        );
                        $this->db->insert('dz_ban_ip_address', $ip_ban);
                        echo "login_last";
                    }
                    else
                    {
                        echo $count;
                    }
                    return FALSE;
                }
            }
            else
            {
                if ($row->chr_status == "P") {
                    echo "pendding";
                    exit();
                }
                if ($row->chr_status == "AP") {
                    echo "adminpendding";
                    exit();
                }
                if (($row->chr_status == "A") && ($row->var_password == base64_encode($this->password)))
                {
                    $this->db->select('int_glcode');
                    $this->db->where('fk_user', $row->int_glcode);
                    $this->db->where('chr_status', 'B');
                    $data1 = $this->db->get('dz_ban_ip_address')->result_array();
                    if (!empty($data1))
                    {
                        echo "banbyadmin";
                    }
                    else
                    {
                        echo "login_last";
                    }
                }
                else
                {
                    echo "login_last";
                }
            }
        }
        else
        {
            echo "login_error";
        }
        exit;
    }

    //Edit by tayyab
    function is_login_ajax() {
        if ($this->input->post()) {
            if (!empty($this->session->userdata['valid_user']))
                echo 'yes';
            else
                echo 'no';
        }
        exit;
    }

    function forgot_password() {

        $this->load->library('email');

        $condition = array('var_email' => $_POST['email']);
        $this->db->select('*')->from('dz_user')->where($condition);

        $query = $this->db->get();

        if ($query->num_rows() != 0) {

            $result_array = $query->result_array();

            $this->email->set_mailtype("html");
            $this->email->from(NOREPLY, SITE_NAME);
            $this->email->to($_POST['email']);
            $this->email->subject('Forgot password');
            $mail_body = '<p style="color:#fff;margin-bottom:10px;">Dear ' . $result_array[0]['var_fname'] . ',</p>';
            $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Thank you for using <strong style="color:#fff;">'.DOMAIN_NAME.'</strong>. We are sending you your current password, please see it below:</p>';
            $mail_body .='<p style="color:#fff;margin-bottom:10px;">Your password is:<b> ' . base64_decode($result_array[0]['var_password']) . '</b></p>';
            $mail_body .='<p style="color:#fff;margin-bottom:10px;">Thank you for choosing us and please always give us a try before booking your next trip so that you can be sure that you made the best booking decision!</p>';
            $mail_body .='<p style="color:#fff;">Sincerely,<br/>';
            $mail_body .= '<p style="color:#fff;margin-bottom:10px;"><b>'.SITE_NAME.'<sup>sm</sup></b><br>
							<small style="font-style: italic;">Where better deals are made for YOU!</small><b><sup>sm</sup></b>
							</p>';
            $data['mail_body'] = $mail_body;

            $data['lastline'] = 'yes';
            $message = $this->load->view('admin/account/genral_emailtemplate', $data, TRUE);

            $this->email->message($message);
            $this->email->send();
            echo 'success';
        } else {
            echo 'error';
        }
        exit;
    }

    function logout() {
        $this->session->unset_userdata('valid_user');
        $this->session->unset_userdata('valid_affiliate');
        $this->session->sess_destroy();
        redirect('front/home');
        exit;
    }

    function adminuserlogin($userid1) {

        $userid = base64_decode($userid1);
        $row = $this->toval->idtorow('var_accountid', $userid, 'dz_user');
        if (empty($row)) {
            $row = $this->toval->idtorow('var_username', $userid, 'dz_user');
            if (empty($row)) {
                $row = $this->toval->idtorow('var_email', $userid, 'dz_user');
            }
        }
        if ($row->chr_user_type == "AF") {
            $arr = array(
                'valid_affiliate' => array('lastactivity' => time(), 'username' => $row->var_username, 'var_email' => $row->var_email, 'id' => $row->int_glcode, 'var_usertype' => $row->chr_user_type));

            $this->session->set_userdata($arr);
            $data['page'] = "affiliate/home/home";
        } elseif ($row->chr_user_type == "U") {
            $arr = array(
                'valid_user' => array('lastactivity' => time(), 'username' => $row->var_username, 'var_email' => $row->var_email, 'id' => $row->int_glcode, 'var_usertype' => $row->chr_user_type));

            $this->session->set_userdata($arr);
            $data['page'] = "user/home/home";
        }

        $this->page_name = 'Home';
        $data['js'] = array(
            'login.js',
            'moment.min.js',
            'front/home.js',
            'front/jquery.slimscroll.min.js',
            'front/jquery.slimscroll.js',
            'affiliate/req_quote.js',
            'custom/components-pickers.js',
            'custom/components-dropdowns.js',
        );
        $data['js_plugin'] = array(
            'bootstrap-fileinput/bootstrap-fileinput.js',
            'checkincheckout/bootstrap-datepicker.js',
            'bootstrap-select/bootstrap-select.min.js',
            'select2/select2.min.js',
        );
        $data['css_plugin'] = array(
            'bootstrap-fileinput/bootstrap-fileinput.css',
            'bootstrap-datepicker/css/datepicker.css',
            'bootstrap-select/bootstrap-select.min.css',
            'select2/select2.css',
            'select2/select2-metronic.css',
            'jquery-ui/jquery-ui.css'
        );
        $data['css'] = array(
        );
        if ($this->input->post()) {
            $res = $this->quote_model->add_req_quote($this->input->post());
            if ($res) {
                echo "success";
                exit;
            } else {
                echo "error";
                exit;
            }
        }
        $data['init'] = array(
            'Login.init()',
            'Home.init()',
            'Req_quote.init()',
            'Req_quote.comments_placeholder()',
            'ComponentsPickers.init_datepicker()',
        );
        $this->db->where('fk_user', $this->userid);
        $this->db->where('var_status', 'A');
        $data['coupons'] = $this->db->get('dz_coupons')->result_array();
        $this->db->select('dz_city.int_glcode,dz_city.var_city,dz_state.var_state,dz_country.var_country');
        $this->db->from('dz_city');
        $this->db->join('dz_state', 'dz_city.fk_state = dz_state.int_glcode', 'left');
        $this->db->join('dz_country', 'dz_city.fk_country = dz_country.int_glcode', 'left');
        $data['city'] = $this->db->get()->result_array();

        if ($row->chr_user_type == "AF") {
            $this->load->view(AFILLIATE_LAYOUT, $data);
        } elseif ($row->chr_user_type == "U") {
            $this->load->view(USER_LAYOUT, $data);
        }
    }

    function activation($userid) {
        $data['page'] = "front/home/activation";
        $this->page_name = 'Login Activation';
        $data['js'] = array(
            'custom/form-wizard.js',
            'custom/components-pickers.js',
        );
        $data['js_plugin'] = array(
            'jquery-validation/dist/jquery.validate.min.js',
            'jquery-validation/dist/additional-methods.min.js',
            'bootstrap-wizard/jquery.bootstrap.wizard.min.js',
            'select2/select2.min.js',
            'bootstrap-datepicker/js/bootstrap-datepicker.js',
            'bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',
            'bootstrap-maxlength/bootstrap-maxlength.min.js'
        );
        $data['css_plugin'] = array(
            'select2/select2-metronic.css',
            'select2/select2.css',
        );
        $data['css'] = array(
        );
        $data['init'] = array(
            'FormWizard.init()',
            'ComponentsPickers.init()',
        );

        $this->db->where('var_accountid', $userid);
        $time = $this->db->get('dz_user')->result_array();
        date_default_timezone_set(AMERICA_LOS_ANGLES_TIME_ZONE);
        $date1 = date_create($time[0]['dt_created_date']);
        $date2 = date_create(date('Y-m-d H:i:s'));
        $diff = date_diff($date1, $date2);

        if(count($time)>0) {
            if ($time[0]['chr_status'] != "A") {

                if ($diff->h < 24) {
                    $data_array = array(
                        'login_status' => 'Active',
                        'chr_status' => 'A',
                    );
                    $this->db->where('var_accountid', $userid);
                    $this->db->update('dz_user', $data_array);

                    $data['message'] = 'Thank you very much for verifying your email address. Please login in order to continue your enrollment.';
                } else {

                    $data['message'] = 'Sorry! Wrong activation link or your activation period has expired. Please check if you typed the activation code properly or try to re-register.';
                }
            } else {
                $data['message'] = 'Sorry! It looks like that this account has been activated already. If you do not know your login information, please try the ‘Forgot password’ feature at the login window.';
            }
        }else{
            $data['message'] = 'Sorry! Wrong activation link or your activation period has expired. Please check if you typed the activation code properly or try to re-register.';
        }


        $this->load->view(FRONT_LAYOUT, $data);
    }

    function add_newsletter1() {

        $list_id = 'fd2d2522bf';
        $email = $this->input->post('email');
        $config = array(
            'apikey' => 'b41d7ac731f7d73f6b04b5c046361aef-us10', // Insert your api key
            'secure' => FALSE   // Optional (defaults to FALSE)
        );
        $this->load->library('MCAPI', $config, 'mail_chimp');
        $result = $this->mail_chimp->listSubscribe($list_id, $email);

        if ($result) {
            echo 'success';
        } else {
            echo 'fail';
        }
    }

    function add_newsletter() {

        require_once 'SimplyCastAPI.php';

        $list_id = '1';
        $email = $this->input->post('email');

        $contect = new \SimplyCast\ContactManager('08fff105e5c3a73b2370554de9fcd8bd7d32a355', '4e76914c8520c0f6e6c61fc9e6edb49e7a639378');
        $dd1 = $contect->getLists();
        $columns = $contect->getColumnsByName(array('email', 'name', 'phone'));
        $contact = array();
        $myContact = array(
            //'corp_system_id' => 'ID0001234',
            'email' => $email,
            'name' => 'News Latter1',
            'phone' => '555',
        );
        foreach ($columns['columns'] as $col) {
            if (array_key_exists($col['name'], $myContact)) {
                $contact[] = array(
                    'id' => $col['id'],
                    'value' => $myContact[$col['name']],
                );
            }
        }
        $listid = array('1');
        $dd = $contect->createContact($contact, $listid);
        if ($dd['contact']['id'] != "") {
            echo 'success';
        } else {
            echo 'fail';
        }
        exit();
    }

    function news_activation($userid) {
        $data['page'] = "front/home/activation";
        $this->page_name = 'NewsLetter Activation';
        $data['js'] = array(
            'custom/form-wizard.js',
            'custom/components-pickers.js',
        );
        $data['js_plugin'] = array(
            'jquery-validation/dist/jquery.validate.min.js',
            'jquery-validation/dist/additional-methods.min.js',
            'bootstrap-wizard/jquery.bootstrap.wizard.min.js',
            'select2/select2.min.js',
            'bootstrap-datepicker/js/bootstrap-datepicker.js',
            'bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',
            'bootstrap-maxlength/bootstrap-maxlength.min.js'
        );
        $data['css_plugin'] = array(
            'select2/select2-metronic.css',
            'select2/select2.css',
        );
        $data['css'] = array(
        );
        $data['init'] = array(
            'FormWizard.init()',
            'ComponentsPickers.init()',
        );

        $this->db->where('int_glcode', $userid);
        $time = $this->db->get('dz_newsletter')->result_array();
        date_default_timezone_set(AMERICA_LOS_ANGLES_TIME_ZONE);
        $date1 = date_create($time[0]['dt_created_date']);
        $date2 = date_create(date('Y-m-d H:i:s'));
        $diff = date_diff($date1, $date2);

        if ($time[0]['var_status'] != "Active") {

            if ($diff->h < 24) {
                $data_array = array(
                    'var_status' => 'Active'
                );
                $this->db->where('int_glcode', $userid);
                $this->db->update('dz_newsletter', $data_array);

                $data['message'] = 'Thank you very much for verifying your email address. Please login in order to continue your enrollment.';
            } else {

                $data['message'] = 'Sorry! Wrong activation link or your activation period has expired. Please check if you typed the activation code properly or try to re-register.';
            }
        } else {
            $data['message'] = 'Sorry! It looks like that this account has been activated already. If you do not know your login information, please try the ‘Forgot password’ feature at the login window.';
        }

        $this->load->view(FRONT_LAYOUT, $data);
    }

    function check_email() {
        $this->db->where('var_email', $_POST['email']);
        $query = $this->db->get('dz_user')->num_rows();
        if ($query == 1) {
            echo "success";
            exit;
        } else {
            echo "error";
            exit;
        }
    }

    function hotelname() {
        if ($this->input->post('hotelname')) {
            $this->db->select('var_category');
            $this->db->like('var_hotel_name', $this->input->post('hotelname'));
            $query = $this->db->get('dz_hotel_list')->result_array();
            echo json_encode($query[0]['var_category']);
        }
    }

    function GetCruiseDepartureMonthOptions() {
        $cruise_options = '<option value="01/0001">All Dates</option>';

        for ($i = 1; $i <= 12; $i++) {
            $curDate = "+" . $i . " month";
            $month_slash_year = date('m/Y', strtotime($curDate));
            $month_space_year = date('M', strtotime($curDate)) . "  " . date('Y', strtotime($curDate));

            $cruise_options .= '<option value=' . $month_slash_year . '>' . $month_space_year . '</option>';
        }

        return $cruise_options;
    }


    function individual_signup($account_id) {
        // print_r($this->session->userdata);
        if (!empty($this->session->userdata['valid_user']) || !empty($this->session->userdata['valid_affiliate']))
        {
            exit();
        }
        $data['hear_about_us'] = isset($_GET['hear_about_us']) ? $_GET['hear_about_us'] : '';
        $data['page'] = "front/home/individual_signup";
        $this->page_name = 'Individual Signup';
        $data['js'] = array(
            'custom/form-wizard.js',
            'custom/components-pickers.js',
        );
        $data['var_meta_title'] = 'Wholesale Hotels Group – Great travel deals for Corporate Travel and Individuals';
        $data['var_meta_description'] = 'Get WHOLESALE HOTEL inventories and deals worldwide for Corporate Travel, Non-Profit travel, Government travel, Travel Agents and Individual users.';
        $data['var_meta_keyword'] = 'Wholesale Hotels Group, WHotelsGroup.com, wholesale hotel deals, corporate travel, government travel, non-profit travel, travel agency travel, travel agency hotels, wholesale hotels, Online Travel Agency, best OTAs';


        $data['js_plugin'] = array(
            'jquery-validation/dist/jquery.validate.min.js',
            'jquery-validation/dist/additional-methods.min.js',
            'bootstrap-wizard/jquery.bootstrap.wizard.min.js',
            'select2/select2.min.js',
            'bootstrap-datepicker/js/bootstrap-datepicker.js',
            'bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',
            'bootstrap-maxlength/bootstrap-maxlength.min.js'
        );
        $data['css_plugin'] = array(
            'select2/select2-metronic.css',
            'select2/select2.css',
        );
        $data['css'] = array(
        );
        $data['init'] = array(
            'FormWizard.init()',
            'ComponentsPickers.init()',
        );
        $data['sign_status'] = $this->db->get('dz_singup_coupons')->result_array();

        $this->db->where('var_accountid', $account_id);
        $data['info'] = $this->db->get('dz_user')->result_array();

        $this->db->where('var_signupip', $_SERVER['REMOTE_ADDR']);
        $data['ipaddress'] = $this->db->get('dz_user')->result_array();

        if (!empty($data['info']))
        {
            $data['parentid'] = $data['info'][0]['int_glcode'];
        }
        else
        {
            $data['parentid'] = 0;
        }

        $this->load->view(FRONT_LAYOUT, $data);
    }

}

?>