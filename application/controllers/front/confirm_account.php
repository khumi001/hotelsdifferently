<?php

class Confirm_account extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function user($id) {
        $data['page'] = "front/home/activation";
        $this->page_name = 'Login Activation';

        $email = base64_decode($id);
        $this->db->where('var_email', $email);
        $this->db->where('chr_status', 'P');
        $query = $this->db->get('dz_user')->row();
        $id = $query->int_glcode;
        date_default_timezone_set(AMERICA_LOS_ANGLES_TIME_ZONE);
        $TotalHours = 0;
        $today = date('Y-m-d H:i:s');
        $start = new DateTime($query->dt_created_date);
        $End = new DateTime($today);
        $interval = $start->diff($End);
        $TotalHours = $interval->h;
        if ($TotalHours < 24) {
            if (!empty($query)) {
                $status = array(
                    'chr_status' => 'AP',
                    'login_status' => 'inactive',
                );
                $this->db->where('int_glcode', $id);
                $this->db->update('dz_user', $status);
                $data['message'] = "Thank you for confirming your email address. Our representative will review your application and will get back to you soon with the decision about your enrollment. Please allow up to 48 hours to verify your enrollment details. Thank you!";
            } else {
                $data['message'] = "Sorry! It looks like that this account has been activated already. If you do not know your login information, please try the <b>Forgot password</b> feature at the login window.";
            }
        } else {
            $data['message'] = "Sorry! Wrong activation link or your activation period has expired. Please check if you typed the activation code properly or try to re-register";
        }
        $this->load->view(FRONT_LAYOUT, $data);
    }

    function mailchimp() {
        $list_id = '9ed637e92c';
        $email = 'abhishekdesai39@gmail.com';
        $config = array(
            'apikey' => 'bdc9ff10ea221c2321b7c5dd1233eaa5-us10', // Insert your api key
            'secure' => FALSE   // Optional (defaults to FALSE)
        );
        $this->load->library('MCAPI', $config, 'mail_chimp');

        if ($this->mail_chimp->listSubscribe($list_id, $email)) {
            echo $email . 'is now subscribed to list with id:' . $list_id;
        }
    }

    function paymentsuccess() {
        $custom = $_POST['custom'];
        $arraydata = explode('/', $custom);
        $replyquotid = $arraydata[0];
        $coupanid = $arraydata[1] ? $arraydata[1] : "";

        $this->paypalsuccess($replyquotid, $coupanid);
    }

    function paypalsuccess($replyquotid, $coupanid = NULL) {

        $id = $replyquotid;
        $this->db->where('int_glcode', $id);
        $query = $this->db->get('dz_rplyquote')->result_array();

        if ($coupanid != "") {
            $data_array3 = array(
                'var_status' => 'U',
            );
            $this->db->where('int_glcode', $coupanid);
            $this->db->update('dz_coupons', $data_array3);

            $data_array = array(
                'quote_status' => 'C',
                'dt_updated_date' => date("Y-m-d H:i:s"),
                'fk_coupon' => $coupanid
            );
            $this->db->where('int_glcode', $query[0]['int_fkquote']);
            $this->db->update('dz_quote', $data_array);

            $data_array1 = array(
                'fk_coupon' => $coupanid
            );
            $this->db->where('int_glcode', $id);
            $this->db->update('dz_rplyquote', $data_array1);
        } else {
            $data_array = array(
                'quote_status' => 'C',
                'dt_updated_date' => date("Y-m-d H:i:s"),
            );
            $this->db->where('int_glcode', $query[0]['int_fkquote']);
            $this->db->update('dz_quote', $data_array);
        }

        $this->db->where('int_glcode', $query[0]['int_fkquote']);
        $quote = $this->db->get('dz_quote')->result_array();

        //payment relase date
        $paymentdate = date("Y-m-d", strtotime(date("Y-m-d", strtotime($quote[0]['var_checkout'])) . " +90 day"));

        $data_array1 = array(
            'chr_status' => 'P',
            'var_updateddate' => date("Y-m-d H:i:s"),
        );
        $this->db->where('int_glcode', $id);
        $this->db->update('dz_rplyquote', $data_array1);

        $query1 = $this->db->select('var_prize')->get_where('dz_rplyquote', array('int_glcode' => $id))->row();
        $actual_amount = $query1->var_prize;
        $paid1 = preg_replace('/\D/', '', $actual_amount) / 100;

        if ($coupanid != "") {
            $paid = $this->getfinalamount1($paid1, $coupanid);
        } else {
            $paid = $paid1;
        }

        // transaction id start
        if ($query[0]['var_transactionid'] == "") {
            
            $this->load->model('admin/quote');
            $random = $this->quote->random_num();

            $data_array1 = array(
                'var_transactionid' => $random,
            );
            $this->db->where('int_glcode', $id);
            $this->db->update('dz_rplyquote', $data_array1);
            $query[0]['var_transactionid'] = $random;

            // cout point
            $get_perent1 = $this->db->select('fk_parent_account,var_email,var_point')->get_where('dz_user', array('int_glcode' => $this->userid))->row();
            $uamount = $get_perent1->var_point + $paid;
            $pointupdate = array(
                'var_point' => $uamount,
            );
            $this->db->where('int_glcode', $this->userid);
            $this->db->update('dz_user', $pointupdate);
            // cout point
        }

        // transaction id end

        $commision_percentage = ($paid * 5) / 100;
        $commision_percentage = number_format((float) $commision_percentage, 2, '.', '');

        $this->db->select('dz_quote.*,
                            dz_rplyquote.var_site1,
                            dz_rplyquote.int_site1_price,
                            dz_rplyquote.var_site2,
                            dz_rplyquote.int_site2_price,
                            dz_rplyquote.var_tripadvisor,
                            dz_rplyquote.var_uniq_quoteid,
                            dz_rplyquote.var_policy,
                            dz_rplyquote.var_comment,
                            dz_rplyquote.var_NOH,
                            dz_rplyquote.var_star,
                            dz_rplyquote.var_room_type,
                            dz_rplyquote.var_address,
                            dz_rplyquote.var_prize,
                            dz_rplyquote.var_transactionid');
        $this->db->from('dz_quote');
        $this->db->join('dz_rplyquote', 'dz_quote.int_glcode=dz_rplyquote.int_fkquote');
        $this->db->where_in('dz_rplyquote.int_glcode', $id);
        $datacontent = $this->db->get()->result_array();

        $this->db->where('int_glcode', $coupanid);
        $coupanidquery = $this->db->get('dz_coupons')->result_array();

        if ($coupanidquery[0]['var_coupan_type'] == 1) {
            $coupanstr = $coupanidquery[0]['var_couponvalue'] . '% OFF';
        }
        if ($coupanidquery[0]['var_coupan_type'] == 2) {
            $coupanstr = '$' . $coupanidquery[0]['var_couponvalue'] . 'OFF';
        }

        // automatic counter
        $this->load->model('front/paymentsuccess');
        $content = array('id' => $query[0]['var_transactionid']);

        $inserdata = $this->paymentsuccess->get_quetes($content);

        $dataarray = array(
            'id' => $query[0]['var_transactionid'],
            'username' => $inserdata['username'],
            'quote' => $inserdata['quote'],
            'highprice' => $inserdata['price'],
            'saveprice' => $inserdata['save'],
            'fkid' => $inserdata['fkid'],
        );

        $this->manualentry_model->insert_data($dataarray);

        $emailFrom = $this->db->get_where('dz_user', array('int_glcode' => $datacontent[0]['var_fkuser']))->result_array();
        //automatic counter

        $this->email->set_mailtype("html");
        $this->email->from(NOREPLY, 'Admin');
        $this->email->to($emailFrom[0]['var_email']);
        $this->email->subject('Payment Success');
        $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Dear Member,</p>';
        $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Thank you for booking with Wholesale Hotels Group!<SUP>sm</SUP> We are delighted that you decided to choose us for your next getaway. </p>';
        $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Your charge will appear on your statement as: <b>PAYPAL*HOTELSDIFFE</b> or <b>Wholesale Hotels Group</b></p>';
        $mail_body .= '<p style="color:#fff;margin-bottom:10px;">As soon as we secure your reservation, we will send you an email notification.</p>';
        $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Once again, here are the details of your reservation: </p>';
        $mail_body .= '<p style="color: #fff; float: left; margin-bottom: 10px; width: 50%;"><b>Check-in</b>: ' . $datacontent[0]['var_checkin'] . '</p>';
        $mail_body .= '<p style="color: #fff; float: left; margin-bottom: 10px; width: 50%;"><b>Check-out</b>: ' . $datacontent[0]['var_checkout'] . '</p>';
        $mail_body .= '<p style="color: #fff; float: left; margin-bottom: 10px; width: 50%;"><b>City</b>: ' . $datacontent[0]['var_city'] . '</p>';
        $mail_body .= '<p style="color: #fff; float: left; margin-bottom: 10px; width: 50%;"><b>QUOTE ID</b>: ' . $datacontent[0]['var_uniq_quoteid'] . '</p>';
        $mail_body .= '<p style="color: #fff; float: left; margin-bottom: 10px; width: 100%;"><b>Name of hotel</b>: ' . $datacontent[0]['var_NOH'] . '</p>';
        $mail_body .= '<p style="color: #fff; float: left; margin-bottom: 10px; width: 100%;"><b>Address</b>: ' . $datacontent[0]['var_address'] . '</p>';
        $mail_body .= '<p style="color: #fff; float: left; margin-bottom: 10px; width: 100%;"><b>Room Type</b>: ' . $datacontent[0]['var_room_type'] . '</p>';
        $mail_body .= '<p style="color: #fff; float: left; margin-bottom: 10px; width: 50%;"><b>Stars</b>: ' . $datacontent[0]['var_star'] . '</p>';
        $mail_body .= '<p style="color: #fff; float: left;  margin-bottom: 10px;  width:100%;"><b>TripAdvisor</b>: ' . $datacontent[0]['var_tripadvisor'] . '</p>';
        $mail_body .= '<p style="color: #fff; float: left; margin-bottom: 10px; width: 33%;"><b>Rooms</b>: ' . $datacontent[0]['var_room'] . '</p>';
        $mail_body .= '<p style="color: #fff; float: left; margin-bottom: 10px; width: 33%;"><b>Adults</b>: ' . $datacontent[0]['var_adult'] . '</p>';
        $mail_body .= '<p style="color: #fff; float: left; margin-bottom: 10px; width: 33%;"><b>Children</b>: ' . $datacontent[0]['var_child'] . '</p><p style="clear: both"></p>';
        $mail_body .= '<p style="color:#fff;margin-bottom:10px;"><b>Cancellation policy</b>: ' . $datacontent[0]['var_policy'] . '</p>';
        $mail_body .= '<p style="color:#fff;margin-bottom:10px;"><b>Comments</b>: ' . $datacontent[0]['var_comment'] . '</p>';
        $mail_body .= '<p style="color:#fff;margin-bottom:10px; display:block; text-align:center;"><b>Quoted price</b>: $' . $datacontent[0]['var_prize'] . '</p>';
        $mail_body .= '<p style="color:#fff;margin-bottom:10px; display:block; text-align:center;"><b>Coupon used</b>: ' . $coupanstr . '</p>';
        $mail_body .= '<p style="color:#fff;margin-bottom:10px; display:block; text-align:center; color:#ff7563"><b>Paid amount</b>: <b>$' . number_format((float) $paid, 2, '.', '') . '</b></p>';
        $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Please do not hesitate to let us know if there is anything we can do to make your stay excellent.</p>';
        $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Sincerely,</br>';
        $mail_body .= '<p style="color:#fff;margin-bottom:10px;"><b>Wholesale Hotels Group<sup>sm</sup></b></p>';



        $data['mail_body'] = $mail_body;
        $message = $this->load->view('admin/account/genral_emailtemplate', $data, TRUE);
        $this->email->message($message);
        $sent = $this->email->send();

        $get_perent = $this->db->select('fk_parent_account,var_email,var_point')->get_where('dz_user', array('int_glcode' => $this->userid))->row();


        if ($get_perent->fk_parent_account != "") {

            $this->db->where('fk_user', $get_perent->fk_parent_account);
            $this->db->where('fk_replayquote', $query[0]['fk_replayquote']);
            $commsion = $this->db->get('dz_commision')->result_array();

            if (empty($commsion)) {
                $data_array2 = array(
                    'fk_user' => $get_perent->fk_parent_account,
                    'fk_quote' => $query[0]['int_fkquote'],
                    'fk_replayquote' => $query[0]['int_glcode'],
                    'chr_status' => 'U',
                    'var_actual_amont' => $paid1,
                    'var_paid_amount' => $paid,
                    'var_commision_amount' => $commision_percentage,
                    'var_releasedate' => $paymentdate,
                    'dt_created_date' => date('Y-m-d H:i:s'),
                    'dt_update_date' => date('Y-m-d H:i:s'),
                );
                $result = $this->db->insert('dz_commision', $data_array2);
            }

            if ($result) {
                $data['page'] = "user/reservation/paypalsuccess";
            } else {
                $data['page'] = "user/reservation/paypalsuccess";
            }
        } else {
            $data['page'] = "user/reservation/paypalsuccess";
        }
        
        // comistion count end

        $this->page_name = 'Payment Page';
        $this->parent_menu = 'reservation';
        $data['js'] = array(
            'user/userpay.js',
            'custom/components-pickers.js',
        );
        $data['js_plugin'] = array(
            'bootstrap-datepicker/js/bootstrap-datepicker.js',
        );
        $data['css_plugin'] = array(
            'bootstrap-datepicker/css/datepicker.css'
        );
        $data['css'] = array(
        );
        $data['init'] = array(
            'Userpay.init()',
            'ComponentsPickers.init_datepicker()'
        );
        $data['transactionid'] = $query[0]['var_transactionid'];
        $this->db->select('dz_quote.*,
            dz_rplyquote.var_site1,
            dz_rplyquote.int_site1_price,
            dz_rplyquote.var_site2,
            dz_rplyquote.int_site2_price,
            dz_rplyquote.var_tripadvisor,
            dz_rplyquote.var_uniq_quoteid,
            dz_rplyquote.var_policy,
            dz_rplyquote.var_NOH,
            dz_rplyquote.var_comment,
            dz_rplyquote.var_star,
            dz_rplyquote.var_prize');
        $this->db->from('dz_quote');
        $this->db->join('dz_rplyquote', 'dz_quote.int_glcode=dz_rplyquote.int_fkquote');
        $this->db->where_in('dz_rplyquote.int_glcode', $id);
        $query = $this->db->get()->result_array();

        $data['datacontent'] = $query;
        $this->db->where('int_glcode', $coupanid);
        $data['coupons'] = $this->db->get('dz_coupons')->result_array();

        $data['paidamount'] = $paid;
        $data['replayquoteid'] = $replayquoteid;

        $data['refer_link'] = $this->db->select('var_accountid')->where('int_glcode', $this->userid)->get('dz_user')->row_array();
        $refer_link = $this->db->select('var_accountid')->where('int_glcode', $this->userid)->get('dz_user')->result_array();
        $url1 = base_url();
        $data['title'] = "I just saved $" . $paid . " using Wholesale Hotels Group. Sign up through my link and get a $20 OFF coupon!!! My link is: " . base_url() . "front/home/member_signup/" . $userid;
        $data['desc'] = "";

        $this->load->view(USER_LAYOUT, $data);
    }

    function getfinalamount1($paid1, $coupanid) {
        $this->db->select('*');
        $this->db->where('int_glcode', $coupanid);
        $coupan = $this->db->get('dz_coupons')->result_array();

        if ($coupan[0]['var_coupan_type'] == 1) {

            $discoutamount = floatval(($coupan[0]['var_couponvalue'] * $paid1) / 100);
            $discoutamount = number_format((float) $discoutamount, 2, '.', '');
            $acutelamount = $paid1 - $discoutamount;
            $acutelamount = number_format((float) $acutelamount, 2, '.', '');
            return $acutelamount;
        }
        if ($coupan[0]['var_coupan_type'] == 2) {
            $acutelamount = $paid1 - $coupan[0]['var_couponvalue'];
            $acutelamount = number_format((float) $acutelamount, 2, '.', '');
            return $acutelamount;
        }
    }

    function paymentreturnsuccess() {
        $data['page'] = "front/home/paymentreturnsuccess";
        $this->page_name = 'Payment Success';

        $data['message'] = "Thank you for booking with <b>Wholesale Hotels Group!<sup>sm</sup></b> We are delighted that you decided to choose us for your next getaway. As soon as we secure your reservation, we will send you an email notification.";

        $this->load->view(FRONT_LAYOUT, $data);
    }

    function applycode() {
        if ($this->input->post()) {
            $this->db->select('int_amount , minspend');
            $this->db->from('dz_promocode');
            $this->db->where('var_promocode', $this->input->post('promocode'));
            $query = $this->db->get()->result_array();

            if (!empty($query)) 
			{
                echo json_encode(array('amount' => $query[0]['int_amount'] , "minspen" => $query[0]['minspend']));
            } 
			else 
			{
                echo json_encode(array('error' => true));
            }
        }
		exit;
    }

    function sendmail_newsletter() {
        $email = $this->input->post('email');
        $demail = base64_encode($email);
        $url = base_url() . 'front/confirm_account/activationnewslatter/' . $demail;

        require_once 'SimplyCastAPI.php';
        $list_id = '1';
        $contect = new \SimplyCast\ContactManager('08fff105e5c3a73b2370554de9fcd8bd7d32a355', '4e76914c8520c0f6e6c61fc9e6edb49e7a639378');
        $GetEmail = $contect->getContactsFromList('1');
        $checkEmail = $this->CheckEmailApi($GetEmail, $email);
        if ($checkEmail == 0) {
            $this->load->library('email');
            $this->email->set_mailtype("html");
            $this->email->from(NOREPLY, 'Member');
            $this->email->to($email);
            $this->email->subject('Welcome to Wholesale Hotels Group!');
            $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Thank you for enrolling in our newsletter. To confirm your newsletter subscription, please activate it by clicking <a style="color: white" href=' . $url . '>HERE</a>.</p>';
            $mail_body .= '<p style="color: #fff;margin-bottom:10px;">Alternatively, you can also copy and paste the following into your browser: <a style="color: white" href=' . $url . '>' . $url . '</a></p>';
            $mail_body .= '<p style="color:#fff;margin-bottom:10px;">If you feel that you received this message by mistake, do nothing and your email address will not be added to our database!</p>';
            $mail_body .= '<p style="color:#fff;margin-bottom:10px;">Thank you for choosing us and please always give us a try before booking your next trip!</p>';
            $mail_body .='<p style="color:#fff;">Sincerely,<br/><strong style="color:#fff;"><a href="www.WHotelsGroup.com" style="color:#fff;">www.WHotelsGroup.com</a></strong></p>';
            $data['mail_body'] = $mail_body;
            $message = $this->load->view('admin/account/genral_emailtemplate', $data, TRUE);

            $this->email->message($message);
            $sendit = $this->email->send();

            if ($sendit) {
                echo 'success';
            } else {
                echo 'fail';
            }
        } else {
            echo 'already';
        }
        exit();
    }

    function activationnewslatter($demail) {

        require_once 'SimplyCastAPI.php';
        $email = base64_decode($demail);
        $list_id = '1';

        $contect = new \SimplyCast\ContactManager('08fff105e5c3a73b2370554de9fcd8bd7d32a355', '4e76914c8520c0f6e6c61fc9e6edb49e7a639378');
        $dd1 = $contect->getLists();
        $columns = $contect->getColumnsByName(array('email', 'name', 'phone'));
        $contact = array();
        $myContact = array(
            'email' => $email,
            'name' => 'News Latter1',
            'phone' => '555',
        );
        foreach ($columns['columns'] as $col) {
            if (array_key_exists($col['name'], $myContact)) {
                $contact[] = array(
                    'id' => $col['id'],
                    'value' => $myContact[$col['name']],
                );
            }
        }
        $GetEmail = $contect->getContactsFromList('1');
        $checkEmail = $this->CheckEmailApi($GetEmail, $email);

        if ($checkEmail == 0) {
            $listid = array('1');
            $contect->createContact($contact, $listid);

            $data['page'] = "front/home/paymentreturnsuccess";
            $this->page_name = 'Newsletter Activation Success';

            //$data['message'] = "<p>Thank you for subscribing to our newsletter! We will provide you with valuable contents about savings, coupons and other valuable information.</p><p>Sincerely, <br/> <b>HotelsDifferently!<sup>sm</sup></b> </p>";
            $data['message'] = "<p>Thank you for subscribing to our newsletter! We will provide you with valuable contents about savings, coupons and tips on how to make the most out of your booking experience with Wholesale Hotels Group. Welcome to the inner circle!</p><p>Sincerely, <br/> <b>Wholesale Hotels Group!<sup>sm</sup></b> <br/> Where better deals are made for YOU!<sup>sm</sup> </p>";
        } else if ($checkEmail == 1) {
            $data['page'] = "front/home/paymentreturnsuccess";
            $this->page_name = 'Already Newsletter Activation';

            $data['message'] = "<p>Sorry, but it seems that you have activated your newsletter subscription process already. </p> <p>Sincerely, <br/> <b>Wholesale Hotels Group!<sup>sm</sup></b> </p>";
        } else {
            $data['page'] = "front/home/paymentreturnsuccess";
            $this->page_name = 'Newsletter Activation Fail';

            $data['message'] = "Thank you for booking with <b>Wholesale Hotels Group!<sup>sm</sup></b> We are delighted that you decided to choose us for your next getaway. As soon as we secure your reservation, we will send you an email notification.";
        }
        $this->load->view(FRONT_LAYOUT, $data);
    }

    function CheckEmailApi($GetEmail, $email) {
        for ($i = 0; $i < count($GetEmail['contacts']); $i++) {
            $Fields[] = $GetEmail['contacts'][$i]['fields'];
        }

        for ($j = 0; $j < count($Fields); $j++) {
            $FeildsValue = array_values($Fields[$j]);
            for ($k = 0; $k < count($FeildsValue); $k++) {
                if ($FeildsValue[$k]['value'] != "") {
                    $value[] = $FeildsValue[$k]['value'];
                }
            }
        }
        if (in_array($email, $value)) {
            $result = 1;
        } else {
            $result = 0;
        }
        return $result;
    }

}

?>