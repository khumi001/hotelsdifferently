<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
if (!defined('BASEPATH')) exit('No direct script access allowed');
class User_Controller extends MY_Controller{
    public $page_name;
    public $parent_menu;
    public $userid;
    
    function __construct() {
        parent::__construct();
        $ipban = $this->toval->ipban();
        if(!empty($ipban))
        {
            redirect(base_url().'ipban', 'location');
        }
	    if ($this->router->fetch_class() != "account" && $this->router->fetch_method() != "login") {
            if (!isset($this->session->userdata['valid_user']) && !isset($this->session->userdata['travel_professional_detail'])) {
                redirect('login');
            }
        }
        $this->page_name = '';
        $this->parent_menu = '';
        $this->userid =  $this->session->userdata['valid_user']['id']; 
        
        date_default_timezone_set(AMERICA_LOS_ANGLES_TIME_ZONE);
//        if(!empty($this->session->userdata['valid_user'])){
//         //  echo  time().'-'.$this->session->userdata['valid_user']['lastactivity'];
//           if(time() - $this->session->userdata['valid_user']['lastactivity'] > 1005) { //subtract new timestamp from the old one
//               $this->session->set_flashdata('item', 'value');
//             //  redirect( base_url().'user/myaccount/info');
//         } else {
//            $arr = array(
//                'valid_user' => array(
//                'lastactivity' => time(),
//                'username' => $this->session->userdata['valid_user']['username'],
//                'var_email' => $this->session->userdata['valid_user']['var_email'],
//                'id' => $this->session->userdata['valid_user']['id'],
//                'var_usertype' => $this->session->userdata['valid_user']['var_usertype']
//            ));
//         $this->session->set_userdata($arr);//set new timestamp
//        }
//       }
        
       //  print_r($this->session->userdata);
       //  exit();
    }
}
?>
