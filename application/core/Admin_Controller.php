<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Admin_controller
 *
 * @author lenova G570
 */
class Admin_Controller extends MY_Controller {

    function __construct() 
	{
        parent::__construct();
        date_default_timezone_set(EST_AMERICA_TIME_ZONE);
        if ($this->router->fetch_class() != "account" && $this->router->fetch_method() != "login") 
		{
            if (!isset($_SESSION['valid_adminuser'])) {
                redirect('admin/login');
            }
        }
      //  print_r($this->session->userdata);
        $blobmenu1 = $_SESSION['valid_adminuser']['menu_details'];
        $this->blobmenuarray1 = unserialize(urldecode($blobmenu1));
    }
}

?>
