<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Front_Controller extends MY_Controller{
    public $page_name;
    function __construct() {
        parent::__construct();
        $ipban = $this->toval->ipban();
       
        if(!empty($ipban))
        {
            redirect(base_url().'ipban/', 'location');
        }
        $this->page_name = '';   
    }
}
?>
