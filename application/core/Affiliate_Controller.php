<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Affiliate_Controller extends MY_Controller {

    public $userid;

    function __construct() {
        parent::__construct();
        $ipban = $this->toval->ipban();
        if(!empty($ipban))
        {
            redirect(base_url().'ipban', 'location');
        }
        if ($this->router->fetch_class() != "account" && $this->router->fetch_method() != "login") {
            if (!isset($this->session->userdata['valid_affiliate'])) {
                redirect('login');
            }
        }
        
        $this->userid =  $this->session->userdata['valid_affiliate']['id'];  
        
        if(time() - $this->session->userdata['valid_affiliate']['lastactivity'] > 2000) { //subtract new timestamp from the old one
             $this->session->sess_destroy();
            header("Location: " . base_url()); //redirect to index.php
            exit;
        } else {
            $arr = array(
                'valid_affiliate' => array(
                'lastactivity' => time(),
                'username' => $this->session->userdata['valid_affiliate']['username'],
                'var_email' => $this->session->userdata['valid_affiliate']['var_email'],
                'id' => $this->session->userdata['valid_affiliate']['id'],
                'var_usertype' => $this->session->userdata['valid_affiliate']['var_usertype']
            ));
         $this->session->set_userdata($arr);//set new timestamp
        }
    }

}

?>
