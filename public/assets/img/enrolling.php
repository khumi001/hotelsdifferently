
<style>
    .enroling-list{
        padding: 0; 
    }    
    .enroling-list > li {
        margin: 5px 0 10px 20px;
    }
    .submit-btn-furture {
      background: rgba(0, 0, 0, 0) linear-gradient(to bottom, #71d1ff 0%, #22b3f4 4%, #198fef 100%) repeat scroll 0 0;
      border: medium none;
      border-radius: 6px;
      box-shadow: 0 1px 3px rgba(0, 0, 0, 0.4);
      color: #fff;
      font-family: "FuturaStd-Book";
      font-size: 16px;
      margin: 0 60px 30px 40px;
      padding: 10px 25px;
      text-shadow: 1px 1px 2px #000;
      text-transform: uppercase;
    }
    .submit-btn-furture {
      background: rgba(0, 0, 0, 0) linear-gradient(to bottom, #71d1ff 0%, #22b3f4 4%, #198fef 100%) repeat scroll 0 0;
      border: medium none;
      border-radius: 6px;
      box-shadow: 0 1px 3px rgba(0, 0, 0, 0.4);
      color: #fff;
      font-family: "FuturaStd-Book";
      font-size: 16px;
      margin: 0 60px 30px 40px;
      padding: 10px 25px;
      text-shadow: 1px 1px 2px #000;
      text-transform: uppercase;
    }
    .form-group.furture-form-style {
      margin: 15px 0 0;
      text-align: center;
    }
    .underli
</style>
<div class="width-row" style="background-color: rgba(255, 255, 255, 0.74) none repeat scroll 0 0;">
    <div class="main_cont">
        <strong class="text-center" style="text-align:center;display:block;margin:0 0 15px;">Thank you for enrolling on HotelsDifferently.com!</strong>
        <div class="" style="margin: 10px 0;">
          <ul class="enroling-list">
                Before we charge you for your annual membership, we would like to make sure that besides the <del>exciting</del> Terms and Conditions, you agree and accept the following:
               <li>In case you pay with <b>Apple Pay</b> or <b>Android Pay</b>, your membership will be activated <b style="text-decoration: underline;">instantly!</b> </li>
               <li>In case you pay with a Debit card or Credit card, your membership will <b>NOT</b> be activated instantly. If you are in a hurry, we advise you <b>NOT</b> to pay for this membership and look for alternative sources. In order to fight fraud, we need to ensure that you are the authorized cardholder and we will conduct a very quick and safe way to authenticate your account. This is both for your and our protection!</li>
               <!-- <li>Upon approval, your payment will be valid for a membership for 365 days.</li> -->
               <li><span style="background-color: yellow;">Your second year membership will auto-renew at <b>$89.99/year</b> and you will have an option at anytime to cancel your membership. </span></li>
               <li><b>REFUND POLICY:</b> There is <b style="color: red;text-decoration: underline;">NO REFUND</b> for your first membership.<ins><b>All sales are final!</b></ins> By paying for your first membership, you duly agree and accept this. For renewals, you are covered by our  
               <b style="color: red;">7-DAY MONEY BACK GUARANTEE!!!  </b>When you get charged for the renewal of your membership, you will have  <b>7 days to request a full refund.</b> </li>
               <li>For <b>EVERY PERSON </b> you refer to <ins>sign up, pay and successfully enroll</ins> on our site, you get <b> 1 MONTH MEMBERSHIP EXTENSION FREE.</b> There is no maximum cap on referred people so if you refer 12 people who signs up, pays <ins>and </ins> successfully enrolls, you get 1-year worth of membership extension from us.</li>
              <!--  <li>At any time <i>(even before you pay for 1 year membership)</i> you will have a chance to pay or upgrade to our <b>LIFETIME PLAN</b>. Should this option interest you, please send us a message via Contact Us menu.</li> -->
               <li>After membership expiration, you will still be able to log back in to retrieve and manage all of your reservations.</li>
          </ul>
        </div>
        <div class="checkbox-list" style="margin-bottom: 10px;">
            <div class="checkbox-inline control-label" style="font-weight: bold; display:inline; vertical-align:middle;">
                <div class="checker" id="uniform-inlineCheckbox21" style="padding: 0 5px 0 ;">
                    <span class=""><input value="option1" name="remember" class="chek_value" id="inlineCheckbox21" type="checkbox"></span>
                </div>I accept and agree to the following conditions as part of my membership enrollment.
        </div>
        <div class="col-md-12 col-sm-12">
            <div class="form-group furture-form-style">
                <button type="button" class="submit-btn-furture " style="background: red;" >Back <i class="fa fa-refresh fa-lg fa-spin loadin_ajax" style="color: #ffffff;display:none;"></i></button>
                <button type="button" class="submit-btn-furture" >Next</button>
            </div>
        </div>
        <p style="margin: 17px 0;"> </p>
        <p style="margin: 0 0 60px;"></p>
        </p>                
    </div>
</div>
