var Log = function() {
    var headle_data_table = function() {
        
        var grid = new Datatable_Apply();
        grid.init({
            src: $("#sample_2"),
            onSuccess: function(grid) {
                // execute some code after table records loaded
            },
            onError: function(grid) {
                // execute some code on network or other general error  
            },
            dataTable: {
                "fnPreDrawCallback": function(oSettings) {
                    $('.group-checkable').attr('checked', false);
                    $('.group-checkable').parents('span').removeClass('checked')
                },
                "aLengthMenu": [
                    [20, 50, 100, 150, -1],
                    [20, 50, 100, 150, "All"] // change per page values here
                ],
                "bDestroy": true,
                "iDisplayLength": 30, // default record count per page
                "bServerSide": true, // server side processing
                "sAjaxSource": baseurl + 'admin/log/log_datatable', // ajax source
                "aaSorting": [[0, "desc"]], // set first column as a default sort by asc
                "aoColumnDefs": [{// define columns sorting options(by default all columns are sortable extept the first checkbox column)
                        'bSortable': false,
                        'aTargets': [-1, -1],
                    }],
                'fnDrawCallback': function() {
                    App.inittooltip(); // reinitialize tooltip
                    ComponentsPickers.init();
                }
            }
        });

    }
    /*var headle_affilatel_table = function() {
        var grid = new Datatable_Apply();
        grid.init({
            src: $("#sample_4"),
            onSuccess: function(grid) {
                // execute some code after table records loaded
            },
            onError: function(grid) {
                // execute some code on network or other general error  
            },
            dataTable: {
                "fnPreDrawCallback": function(oSettings) {
                    $('.group-checkable').attr('checked', false);
                    $('.group-checkable').parents('span').removeClass('checked')
                },
                "aLengthMenu": [
                    [20, 50, 100, 150, -1],
                    [20, 50, 100, 150, "All"] // change per page values here
                ],
                "bDestroy": true,
                "iDisplayLength": 10, // default record count per page
                "bServerSide": true, // server side processing
                "sAjaxSource": baseurl + 'admin/log/affilatel_datatable/', // ajax source
                "aaSorting": [[0, "desc"]], // set first column as a default sort by asc
                "aoColumnDefs": [{// define columns sorting options(by default all columns are sortable extept the first checkbox column)
                        'bSortable': false,
                        'aTargets': [-1, -1],
                    }],
                'fnDrawCallback': function() {
                    App.inittooltip(); // reinitialize tooltip
                    ComponentsPickers.init();
                }
            }
        });

    }*/
    var headle_admin_table = function() {
        var grid = new Datatable_Apply();
        grid.init({
            src: $("#sample_5"),
            onSuccess: function(grid) {
                // execute some code after table records loaded
            },
            onError: function(grid) {
                // execute some code on network or other general error  
            },
            dataTable: {
                "fnPreDrawCallback": function(oSettings) {
                    $('.group-checkable').attr('checked', false);
                    $('.group-checkable').parents('span').removeClass('checked')
                },
                "aLengthMenu": [
                    [20, 50, 100, 150, -1],
                    [20, 50, 100, 150, "All"] // change per page values here
                ],
                "bDestroy": true,
                "iDisplayLength": 10, // default record count per page
                "bServerSide": true, // server side processing
                "sAjaxSource": baseurl + 'admin/log/admin_datatable', // ajax source
                "aaSorting": [[0, "desc"]], // set first column as a default sort by asc
                "aoColumnDefs": [{// define columns sorting options(by default all columns are sortable extept the first checkbox column)
                        'bSortable': false,
                        'aTargets': [-1, -1],
                    }],
                'fnDrawCallback': function() {
                    App.inittooltip(); // reinitialize tooltip
                    ComponentsPickers.init();
                }
            }
        });

    }
    var headle_member_table = function() {
        var grid = new Datatable_Apply();
        grid.init({
            src: $("#sample_3"),
            onSuccess: function(grid) {
				console.log("success");
                // execute some code after table records loaded
            },
            onError: function(grid) {
				console.log("error");
                // execute some code on network or other general error  
            },
            dataTable: {
                "fnPreDrawCallback": function(oSettings) {
                    $('.group-checkable').attr('checked', false);
                    $('.group-checkable').parents('span').removeClass('checked')
                },
                "aLengthMenu": [
                    [20, 50, 100, 150, -1],
                    [20, 50, 100, 150, "All"] // change per page values here
                ],
                "bDestroy": true,
                "iDisplayLength": 10, // default record count per page
                "bServerSide": true, // server side processing
                "sAjaxSource": baseurl + 'admin/log/member_datatable', // ajax source
                "aaSorting": [[0, "desc"]], // set first column as a default sort by asc
                "aoColumnDefs": [{// define columns sorting options(by default all columns are sortable extept the first checkbox column)
                        'bSortable': false,
                        'aTargets': [-1, -1],
                    }],
                'fnDrawCallback': function() {
                    App.inittooltip(); // reinitialize tooltip
                    ComponentsPickers.init();
                }
            }
        });

    }
    var general = function() {
        $('body').on('click', '#logmember', function() {
            $('#mainlog').hide();
            $('#memberlogdiv').show();
            $('#affilatelogdiv').hide();
            $('#adminlogdiv').hide();
        });
        $('body').on('click', '#logaffi', function() {
            $('#mainlog').hide();
            $('#memberlogdiv').hide();
            $('#affilatelogdiv').show();
            $('#adminlogdiv').hide();
        });
        $('body').on('click', '#logadmin', function() {
            $('#mainlog').hide();
            $('#memberlogdiv').hide();
            $('#affilatelogdiv').hide();
            $('#adminlogdiv').show();
        });
    }
    return{
        init: function() {
            headle_data_table();
            //headle_affilatel_table();
            headle_admin_table();
            headle_member_table();
            general();
        }
    };
}();

