var Cancel_reser = function() {
    var headle_tracking_table = function(fistdate, secounddate) {
        if (!fistdate) {
            fistdate = 0;
        }
        if (!secounddate) {
            secounddate = 0;
        }
        var grid = new Datatable_Apply();
        grid.init({
            src: $("#tracking_table"),
            onSuccess: function(grid) {
                // execute some code after table records loaded
            },
            onError: function(grid) {
                // execute some code on network or other general error  
            },
            dataTable: {
                "fnPreDrawCallback": function(oSettings) {
                    $('.group-checkable').attr('checked', false);
                    $('.group-checkable').parents('span').removeClass('checked')
                },
                "aLengthMenu": [
                    [20, 50, 100, 150, -1],
                    [20, 50, 100, 150, "All"] // change per page values here
                ],
                "bDestroy": true,
                "iDisplayLength": 10, // default record count per page
                "bServerSide": true, // server side processing
                "sAjaxSource": baseurl + 'admin/cancel_res/datatable/' + fistdate + '/' + secounddate, // ajax source
                "aaSorting": [[0, "desc"]], // set first column as a default sort by asc
                "aoColumnDefs": [{// define columns sorting options(by default all columns are sortable extept the first checkbox column)
                        'bSortable': false,
                        'aTargets': [-1, -1],
                    }],
                'fnDrawCallback': function() {
                    App.inittooltip(); // reinitialize tooltip
                    ComponentsPickers.init();
                }
            }
        });

        //    var example_range_from_1 = $('#example_range_from_1').val();
        //   var example_range_to_1 = $('#example_range_to_1').val();
        //  alert(example_range_from_1);
        //  alert(fistdate);
        $('.pagination-panel').remove();
        $('#tracking_table_info').remove();
        $('.dataTables_info').remove();
        $('.seperator').html('');
        $('#tracking_table_wrapper .row div:first').removeClass('col-md-8 col-md-6');
        $('#tracking_table_wrapper .row div:first').addClass('col-md-3 col-sm-12');
        $('#tracking_table_wrapper .row div:nth-child(2)').addClass('customeclassadd');
//       $('#tracking_table_wrapper .row div:second').addClass('col-md-6 col-sm-12');  
        var search_html = $('.search_box_custome').clone();
        if (fistdate != 0)
        {
            search_html.find('#example_range_from_1').val(fistdate);
        }
        if (secounddate != 0)
        {
            search_html.find('#example_range_to_1').val(secounddate);
            console.log(search_html.find('#example_range_to_1').val(secounddate));
        }

        $($($('#tracking_table_wrapper').children()[0]).children()[0]).after(search_html)
    }
    var getquote = function() {
         $(".amount").maskMoney({
            prefix: '$',
            allowNegative: true,
            thousands: ',',
            decimal: '.',
            affixesStay: false}
        );
        $('body').on('click', '#conform_list', function() {
            var id = $(this).attr('data-id');

            $.ajax({
                type: 'post',
                url: baseurl + 'admin/pen_res/getconfirm_res',
                data: {"id": id},
                success: function(data) {
                    if (data) {
                        $('.quoterequest').html(data);
                        $('select').select();
                    }
                }
            });
        });
        $('body').on('click', '.revart', function() {
            var id = $(this).attr('data-id');

            $.ajax({
                type: 'post',
                url: baseurl + 'admin/cancel_res/revart',
                data: {"id": id},
                success: function(output) {
                    if (output == 'success') {
                                Toastr.init('success', 'Weldone..', 'Coupon is Generated...');
                                handle_click();
                                setTimeout(function() {
                                    window.location.href = baseurl + 'admin/cancel_res'
                                }, 2000)

                            } else {
                                Toastr.init('error', 'Oops..', 'Something Will worng...');
                            }
                }
            });
        });
        $('body').on('click', '.okbutton', function() {
            var id = $(this).attr('data-id');

            $('.replyquotes').val(id);
        });
    }
    var add_chargeback = function() {
       
        var form = $('#add_paid');
        var error = $('.alert-danger', form);
        var success = $('.alert-success', form);

        success.hide();
        error.show();
        $(form).validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
//                amount: {
//                    required: true,
//                    number: true
//                },
//                comment: {
//                    required: true
//                }

            },
            invalidHandler: function(event, validator) { //display error alert on form submit              
                success.hide();
                error.show();
                //App.scrollTo(error, -200);
            },
            errorPlacement: function(error, element) { // render error placement for each input type
                var icon = $(element).parent('.input-icon').children('i');
                icon.removeClass('fa-check').addClass("fa-warning");
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error'); // set success class to the control group                    
                icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
            },
            highlight: function(element) { // hightlight error inputs
                $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            unhighlight: function(element) { // revert the change done by hightlight
                $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            success: function(label, element) {
                var icon = $(element).parent('.input-icon').children('i');
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                icon.removeClass("fa-warning").addClass("fa-check");
            },
            submitHandler: function(form) {
           var formdata = $(form).serialize();
                $.ajax({
                    type: 'post',
                    url: baseurl + 'admin/chargeback/canclerefund',
                    data: formdata,
                    success: function(output) {
                       
                             if (output == 'success') {
                                Toastr.init('success', 'Weldone..', 'Coupon is Generated...');
                                handle_click();
                                setTimeout(function() {
                                    window.location.href = baseurl + 'admin/cancel_res'
                                }, 2000)

                            } else {
                                Toastr.init('error', 'Oops..', 'Something Will worng...');
                            }
                        
                    }
                });
            }
        });

    }
      var genrel = function() {
        $('body').on('change', '#example_range_from_1', function() {

            var from = $(this).val();
            var to = $(this).parent().next().next().find('input').val();
            //  var to = $('#example_range_to_1').val();

            headle_tracking_table(from, to);
        })
        $('body').on('change', '#example_range_to_1', function() {
            var to = $(this).val();
            var from = $(this).parent().prev().prev().find('input').val();
            //    console.log(from);
            //   console.log(to);
            //  alert(from);
            //   alert(to);
            headle_tracking_table(from, to);
        })
        $('body').on('click', '.resetbutton', function() {
            headle_tracking_table();

        });
    }
    return{
        init: function() {
            headle_tracking_table();
            getquote();
            add_chargeback();
            genrel();

        }
    };
}();