var Affiliatedata = function() {
    var get_affiliatereq = function() {
        $('#btn').click(function(){
            $('#firstname1').addClass('hidden');
            $('#lastname1').addClass('hidden');
            $('#email1').addClass('hidden');
            $('#companyname1').addClass('hidden');
            $('#title1').addClass('hidden');
            $('#country1').addClass('hidden');
            $('#state1').addClass('hidden');
            $('#zip1').addClass('hidden');
            $('#address1').addClass('hidden');
            $('#address21').addClass('hidden');
            $('#phone1').addClass('hidden');
            $('#newsletter1').addClass('hidden');
            $('#wesiteurl1').addClass('hidden');
            $('#sitedesc1').addClass('hidden');
            $('#visitors1').addClass('hidden');
            $('.paymentmethod1').addClass('hidden');
            $('#ppfullname1').addClass('hidden');
            $('#ppphone1').addClass('hidden');
            $('#ppcity1').addClass('hidden');
            $('#ppstate1').addClass('hidden');
            $('#ppcountry1').addClass('hidden');
            $('.ppamount1').addClass('hidden');
            $('#paypalemail1').addClass('hidden');
            
             $('#firstname').removeClass('hidden');
            $('#lastname').removeClass('hidden');
            $('#email').removeClass('hidden');
            $('#companyname').removeClass('hidden');
            $('#title').removeClass('hidden');
            $('#country').removeClass('hidden');
            $('#state').removeClass('hidden');
            $('#zip').removeClass('hidden');
            $('#address').removeClass('hidden');
            $('#address2').removeClass('hidden');
            $('#phone').removeClass('hidden');
            $('#newsletter').removeClass('hidden');
            $('#wesiteurl').removeClass('hidden');
            $('#sitedesc').removeClass('hidden');
            $('#visitors').removeClass('hidden');
            $('.paymentmethod').removeClass('hidden');
            $('#ppfullname').removeClass('hidden');
            $('#ppphone').removeClass('hidden');
            $('#ppcity').removeClass('hidden');
            $('#ppstate').removeClass('hidden');
            $('#ppcountry').removeClass('hidden');
            $('.ppamount').removeClass('hidden');
            $('#paypalemail').removeClass('hidden');
            
            
            $('#btn').addClass('hidden');
            $('#btn_affdb').removeClass('hidden');
        })
        
        $('.editbtn').on('click',function(){
        
            $('#firstname').attr("readonly", false);
            $('#lastname').attr("readonly", false);
            $('#email').attr("readonly", false);
            $('#companyname').attr("readonly", false);
            $('#title').attr("readonly", false);
            $('#country').attr("readonly", false);
            $('#state').attr("readonly", false);
            $('#zip').attr("readonly", false);
            $('#address').attr("readonly", false);
            $('#phone').attr("readonly", false);
            $('#newsletter').attr("readonly", false);
            $('#wesiteurl').attr("readonly", false);
            $('#sitedesc').attr("readonly", false);
            $('#visitor').attr("readonly", false);
            $('#visitors').attr("readonly", false);
            $('#paymentmethod').attr("readonly", false);
            $('#btn_affdb').attr("disabled",false);
        });
    
        $('.btn_affiliatereq').on('click', function(e){
           
            var id = $(this).attr('data-id');
            $('.westranunion').css('display','none');
            $('.paypal').css('display','none');
            $.ajax({
                type: 'post',
                url: baseurl + 'admin/affiliaterequest/getaffiliatereq1',
                data: {"id": id},
                success: function(data) {
                   
                    var output = JSON.parse(data);
                    $('#req_id').val(output[0].int_glcode)
                    $('#firstname').val(output[0].var_fname)
                    $('#lastname').val(output[0].var_lname);
                    $('#email').val(output[0].var_email);
                    $('#companyname').val(output[0].var_company);
                    $('#title').val(output[0].var_title);
                    $('#country').val(output[0].var_country);
                    $('#state').val(output[0].var_state);
                    $('#zip').val(output[0].var_zip);
                    $('#address').val(output[0].var_address1);
                    $('#address2').val(output[0].var_address2);
                    $('#phone').val(output[0].var_phone);
                    $('#newsletter').val(output[0].var_newsletter);
                    $('#wesiteurl').val(output[0].var_websiteurl);
                    $('#sitedesc').val(output[0].var_sitedescription);
                    $('#visitors').val(output[0].var_visitors);
                    $('.paymentmethod').val(output[0].var_payment);
                    $('#ppfullname').val(output[0].var_fullname);
                    $('#ppphone').val(output[0].var_phonenumber);
                    $('#ppcity').val(output[0].var_city);
                    $('#ppstate').val(output[0].var_state);
                    $('#ppcountry').val(output[0].var_country1);
                    $('.ppamount').val(output[0].var_money);
                    $('#paypalemail').val(output[0].var_paypalemail);
                  //  $('#req_id').val(output[0].int_glcode);
                    
                    
                    $('#firstname1').html(output[0].var_fname)
                    $('#lastname1').html(output[0].var_lname);
                    $('#email1').html(output[0].var_email);
                    $('#companyname1').html(output[0].var_company);
                    $('#title1').html(output[0].var_title);
                    $('#country1').html(output[0].var_country);
                    $('#state1').html(output[0].var_state);
                    $('#zip1').html(output[0].var_zip);
                    $('#address1').html(output[0].var_address1);
                    $('#address21').html(output[0].var_address2);
                    $('#phone1').html(output[0].var_phone);
                    $('#newsletter1').html(output[0].var_newsletter);
                    $('#wesiteurl1').html(output[0].var_websiteurl);
                    $('#sitedesc1').html(output[0].var_sitedescription);
                    $('#visitors1').html(output[0].var_visitors);
                    $('.paymentmethod1').html(output[0].var_payment);
                    $('#ppfullname1').html(output[0].var_fullname);
                    $('#ppphone1').html(output[0].var_phonenumber);
                    $('#ppcity1').html(output[0].var_city);
                    $('#ppstate1').html(output[0].var_state);
                    $('#ppcountry1').html(output[0].var_country1);
                    $('.ppamount1').html(output[0].var_money);
                    $('#paypalemail1').html(output[0].var_paypalemail);
                    
                    if(output[0].var_payment == 'westerrn_union')
                    {
                        $('.westranunion').css('display','block');
                    }
                    else
                    {
                        $('.paypal').css('display','block');
                    }
                }
            });

        });
    }
       
    var changestatusA = function() {
        $('#approve').on('click', function()
        {
            var id = $('#req_id').val();
            $.ajax({
                type: 'post',
                url: baseurl + 'admin/affiliaterequest/approvestatus',
                data: {'id': id},
                success: function(output) {
                    if (output == 'success')
                    {
                        Toastr.init('success', 'Weldone..', 'Affiliate Requerest status is Approved..');
                        $('#myModal_autocomplete').modal('hide');
                           handle_click();
                    }
                    else if (output == 'error')
                    {
                        Toastr.init('error', 'Oops..', 'Affiliate Requerest status not Approved');
                    }
                }
            });
        });
    }

    var changestatusR = function() {
        $('#reject').on('click', function()
        {
            var id = $('#req_id').val();

            $.ajax({
                type: 'post',
                url: baseurl + 'admin/affiliaterequest/rejectstatus',
                data: {'id': id},
                success: function(output) {
                    if (output == 'success')
                    {
                        Toastr.init('success', 'Weldone..', 'Affiliate Requerest status is Rejected..');
                        $('#myModal_autocomplete').modal('hide');
                        handle_click();
                    }
                    else if (output == 'error')
                    {
                        Toastr.init('error', 'Oops..', 'Affiliate Requerest status not Rejected');
                    }
                }
            });
        });
    }

    var updateaffiliatedb = function() {
        $('#btn_affdb').on('click', function() {
              //  alert('hiii');
            $.ajax({
                type: 'post',
                url: baseurl + 'admin/affidatabase/updateaffiliate',
                data: $('#affiliate_database').serialize(),
                success: function(output) {
                    if (output == 'success')
                    {
                        Toastr.init('success', 'Weldone..', 'Affiliate Information Updated Successfully.');
                        $('#myModal_autocomplete').modal('hide');
                        handle_click();
                    }
                    else if (output == 'error')
                    {
                        Toastr.init('error', 'Oops..', 'Affiliate Information Not Updated...');
                    }
                }
            });
        });
    }

    var add_affiliatedb = function() {
        var form = $('#add_affiliate_database');
        var error = $('.alert-danger', form);
        var success = $('.alert-success', form);
        success.hide();
        error.show();
        form.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                firstname: {
                  required: true,
                },
                lastname: {
                    required: true,
                },
                email: {
                    required: true,
                    email: true,
                },
                companyname: {
                    required: true,
                },
                title: {
                    required: true,
                },
                country: {
                    required: true,
                },
                state: {
                    required: true,
                },
                newsletter: {
                    required: true,
                },
                phone: {
                    required: true,
                },
                zip: {
                    required: true,
                    number: true,
                },
                wesiteurl: {
                    required: true,
                },
                sitedesc: {
                    required: true,
                },
                visitors: {
                    required: true,
                }

            },
            invalidHandler: function(event, validator) { //display error alert on form submit              
                success.hide();
                error.show();
                App.scrollTo(error, -200);
            },
            errorPlacement: function(error, element) { // render error placement for each input type
                var icon = $(element).parent('.input-icon').children('i');
                icon.removeClass('fa-check').addClass("fa-warning");
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error'); // set success class to the control group                    
                icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
            },
            highlight: function(element) { // hightlight error inputs
                $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            unhighlight: function(element) { // revert the change done by hightlight
                $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            success: function(label, element) {
                var icon = $(element).parent('.input-icon').children('i');
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                icon.removeClass("fa-warning").addClass("fa-check");
            },
            submitHandler: function(form) {
                var url = baseurl + "admin/affidatabase/addaffiliate";
                var data = $(form).serialize();
                ajaxcall(url, data, function(output) {
                    if (output == 'success') {
                        Toastr.init('success', 'Welldone..', 'Affiliate added successfully..');
                        $('#add_affiliate_detail').modal('hide');
                    } else {
                        Toastr.init('error', 'Oops..', 'Quote not added...');
                    }
                });
            }
        });
        //apply validation on select2 dropdown value change, this only needed for chosen dropdown integration.
        $('.select2me', form).change(function() {
            form.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
        });
        //});
    }

    return {
        init: function() {
            get_affiliatereq();
            changestatusA();
            changestatusR();
            updateaffiliatedb();
            add_affiliatedb();
        }
    }
}();