var affi = function(){
    var headle_tracking_table= function(fistdate, secounddate) {        
         if (!fistdate) {
            fistdate = 0;
        }
        if (!secounddate) {
            secounddate = 0;
        }
        var grid = new Datatable_Apply();
        grid.init({
                src: $("#tracking_table"),
                onSuccess: function(grid) {
                    // execute some code after table records loaded
                },
                onError: function(grid) {
                    // execute some code on network or other general error  
                },
                dataTable: { 
                "fnPreDrawCallback": function( oSettings ) {
                    $('.group-checkable').attr('checked',false);
                    $('.group-checkable').parents('span').removeClass('checked')
                    },
                    "aLengthMenu": [
                        [20, 50, 100, 150, -1],
                        [20, 50, 100, 150, "All"] // change per page values here
                    ],
                    
                    "bDestroy":true,                    
                    "iDisplayLength": 20, // default record count per page
                    "bServerSide": true, // server side processing
                    "sAjaxSource": baseurl+'admin/affitracker/datatable/'+fistdate+'/'+secounddate, // ajax source
                    "aaSorting": [[ 1, "asc" ]], // set first column as a default sort by asc
                    "aoColumnDefs":[{  // define columns sorting options(by default all columns are sortable extept the first checkbox column)
                        'bSortable' : false,
                        'aTargets' : [3,4,5,6],
                        
                    }],
                    'fnDrawCallback': function() {
                         App.inittooltip(); // reinitialize tooltip
                         ComponentsPickers.init();
                        }
                    }                                      
            });
       
       
       $('.pagination-panel').remove();
       $('#tracking_table_info').remove();
       $('.dataTables_info').remove();
       $('.seperator').html('');
       $('#tracking_table_wrapper .row div:first').removeClass('col-md-8 col-md-4');
       $('#tracking_table_wrapper .row div:first').addClass('col-md-3 col-sm-12');       
      //  var search_html = $('.search_box_custome').clone();
         var search_html = $('.search_box_custome').clone();
        if(fistdate != 0)
        {
            search_html.find('#example_range_from_1').val(fistdate);
        }
        if(secounddate != 0)
        {
            search_html.find('#example_range_to_1').val(secounddate);
        }
        $($($('#tracking_table_wrapper').children()[0]).children()[0]).after(search_html)
    }
    var genrel = function(){
        $('body').on('change','#example_range_from_1',function(){
           
            var from = $(this).val();
            var to = $(this).parent().next().next().find('input').val();
         
            headle_tracking_table(from,to);
        })
        $('body').on('change','#example_range_to_1',function(){
            var to = $(this).val();
            var from = $(this).parent().prev().prev().find('input').val();
            headle_tracking_table(from,to);
        })
    }
     var handle_manage = function(){
       // alert('ghghjg')
        $('#tracking_table_length').hide();
        
         $('body').on('click','.resetbutton',function(){
           headle_tracking_table();
         });
    }
    
    return {
        init:function(){
            headle_tracking_table();
            genrel();
            handle_manage();
        }
    }
}();
 