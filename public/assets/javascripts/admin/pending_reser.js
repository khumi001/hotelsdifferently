var hotel_array = [];
var countvalue = 1;

var Quote = function() {

    var quote_fill_val = function() {

        $('.quote_val').click(function() {
            var id = $(this).attr('data-id');
            $("#fk_quote").val(id);
            $('.quote').html('');
            $('#rplyquote')[0].reset();
            $('#rplyquote').children().removeClass('.has-error');
            $('#rplyquote').children().removeClass('.has-success');
            $('.col-md-5.valid_quot .select2-chosen').html('--Select--');
            $('.col-md-7.valid_quot .select2-chosen').html('--Select Star--');
            $('.col-md-77.valid_quot .select2-chosen').html('--Select Policy--');
            $("input").each(function() {
                $(this).parent().removeClass('has-error');
                $(this).parent().removeClass('has-success');
            });
            countvalue = 1;
            $("select").each(function() {
                $(this).parent().removeClass('has-error');
                $(this).parent().removeClass('has-success');
            });
            $.ajax({
                type: 'post',
//                url: baseurl + "admin/quoterequests/getquoteval",
                data: {"id": id},
                success: function(output) {
                    var data = JSON.parse(output);
                    $('.city').val(data[0].var_city);
                    $('.point').val(data[0].var_points);
                    $('.accountid').val(data[0].var_accountid);
                    $('.requesteddate').val(data[0].dt_reuested_date)
//                    $('.indate').val(data[0].var_checkin);
//                    $('.outcheck').val(data[0].var_checkout);
                    $('.indate').val(data[0].var_checkin);
                    $('.outcheck').val(data[0].var_checkout);
                    $('.username').val(data[0].var_username);
                    $('.rooms').val(data[0].var_room);
                    $('.adults').val(data[0].var_adult);
                    $('.children').val(data[0].var_child);
                    $('.res_name').val(data[0].var_nameofreservation);
                    $('.hotelname').val(data[0].var_hotelname);
                    $('.star').val(data[0].var_rating);
                    $('.comment').val(data[0].var_comments);


                    var city = $('.city').val();
                    $.ajax({
                        type: 'post',
                        url: baseurl + 'admin/quoterequests/get_hotel',
                        data: {cityvalue: city},
                        success: function(output) {
                            var data = JSON.parse(output);
                            var i;
                            hotel_array = [];
                            for (i = 0; i < data.length; i++) {
                                hotel_array.push(data[i].var_hotelname);
                            }
                            $(".select_hotel").autocomplete({
                                source: hotel_array
                            });
                        }
                    });

                }

            });

        });

        $('body').on('click', '.resetbutton', function() {
            headle_tracking_table();

        });
        $('body').on('click', '.paid_list', function() {
            var src = $(this).attr('src');
            var amount = $(this).attr('srcint');
            
            $('#Source').val(src);
            $('#Amount').val(amount);
            //$('#add_paid')[0].reset();
        });
        
        var cancelquote = 0
        $('body').on('click', '.cancle_list', function() {
            cancelquote++;
            var url = baseurl + "admin/pen_res/cancelquote";
            var id = $(this).attr('data-id');
            var name = $(this).attr('name');
            var email = $(this).attr('email');
            var data = {replayid: id,name:name,email:email};
//            console.log(cancelquote); return false;
            if(cancelquote == 1){
                ajaxcall(url, data, function(output) {
                if (output == 'success') {
                    Toastr.init('success', 'Welldone..', 'Quote added successfully..');
                    setTimeout(function() {
                        location.href = baseurl + "admin/pen_res";
                    }, 3000)
                    //$('#myModal_autocomplete').modal('hide');
                    //   handle_click();
                }
                else if (output == 'user exits') {
                    Toastr.init('warning', 'Warning..', 'Email Already Exits');
                }
                else if (output == 'error') {
                    Toastr.init('error', 'Oops..', 'Quote not added...');
                }
            });
            }
           
        });
    }

    var rplyquote = function() {
        // alert('ll');
        //    $("#rplyquote").validate();

        $('.sendquote').on('click', function() {
            var flag2 = 1;
            $("input.requered_true").each(function() {

                var valuesite = $(this).val();

                if (valuesite == "")
                {
                    $(this).parent().addClass('has-error');
                    flag2 = 0;

                }
            });

            $("select.requered_true").each(function() {

                var valuesite = $(this).val();

                if (valuesite == "")
                {
                    $(this).parent().addClass('has-error');
                    flag2 = 0;

                }
            });
            console.log(flag2);
            $('.flag2').val(flag2);
            var form = $('#rplyquote');
            var error = $('.alert-danger', form);
            var success = $('.alert-success', form);
            success.hide();
            error.show();
            form.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",
                rules: {
//                    'pricequote[]': {
//                        required: true,
//                        number: true
//                    },
//                    'hotel[]': {
//                        required: true,
//                    },
//                    'policy[]': {
//                        required: true,
//                    },
//                    'k_site[]': {
//                        required: true,
//                        number: true,
//                    },
//                    'w_site[]': {
//                        required: true,
//                    },
//                    'f_site[]': {
//                        required: true,
//                    },;
//                    's_site[]': {
//                        required: true,
//                        number: true,
//                    },
//                    'tripadvisor[]': {
//                        required: true,
//                    },
//                    'src[]': {
//                        required: true,
//                    },
//                    's_src[]': {
//                        required: true,
//                    },
//                    'rating[]': {
//                        required: true,
//                    }
                },
                invalidHandler: function(event, validator) { //display error alert on form submit              
                    success.hide();
                    error.show();
                    App.scrollTo(error, -200);
                },
                errorPlacement: function(error, element) { // render error placement for each input type
                    var icon = $(element).parent('.input-icon').children('i');
                    icon.removeClass('fa-check').addClass("fa-warning");
                    $(element).closest('.valid_quot').removeClass('has-success').addClass('has-error'); // set success class to the control group                    
                    icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});

                },
                highlight: function(element) { // hightlight error inputs
                    $(element)
                            .closest('.valid_quot').addClass('has-error'); // set error class to the control group
//                    console.log('kk');
                },
                unhighlight: function(element) { // revert the change done by hightlight
                    $(element)
                            .closest('.valid_quot').removeClass('has-error'); // set error class to the control group
                },
                success: function(label, element) {
                    var icon = $(element).parent('.input-icon').children('i');
                    $(element).closest('.valid_quot').removeClass('has-error').addClass('has-success'); // set success class to the control group
                    icon.removeClass("fa-warning").addClass("fa-check");
                },
                submitHandler: function(form) {
                    var url = baseurl + "admin/quoterequests/rplyquote";
                    var data = $(form).serialize();

                    flag2 = $('.flag2').val();
                    console.log(flag2);
                    $(".ksite").each(function() {
                        var valuesite = $(this).val();
                        var valuesite1 = valuesite.replace(/\,/g, '');
                        // console.log(ss);
                        if (parseFloat(valuesite1) < 20.00)
                        {

                            flag2 = 0;
                        }

                    });
                    $('.flag2').val(flag2);

                    setTimeout(function() {
                        flag2 = $('.flag2').val();
                        if (flag2 == 1)
                        {
                            console.log(flag2);
                            ajaxcall(url, data, function(output) {
                                if (output == 'success') {
                                    Toastr.init('success', 'Welldone..', 'Quote added successfully..');
                                    setTimeout(function() {
                                        location.href = baseurl + "admin/quoterequests";
                                    }, 3000)
                                    //$('#myModal_autocomplete').modal('hide');
                                    //   handle_click();
                                }
                                else if (output == 'user exits') {
                                    Toastr.init('warning', 'Warning..', 'Email Already Exits');
                                }
                                else if (output == 'error') {
                                    Toastr.init('error', 'Oops..', 'Quote not added...');
                                }
                            });
                        }
                    }, 1000)



                }
            });
            // $(".requered_true").rules("add", {required: true});
            //apply validation on select2 dropdown value change, this only needed for chosen dropdown integration.
            $('.select2me', form).change(function() {
                form.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
            });
        });
    }

    var headle_tracking_table = function(fistdate, secounddate) {
        if (!fistdate) {
            fistdate = 0;
        }
        if (!secounddate) {
            secounddate = 0;
        }
        var grid = new Datatable_Apply();
        grid.init({
            src: $("#tracking_table"),
            onSuccess: function(grid) {
                // execute some code after table records loaded
            },
            onError: function(grid) {
                // execute some code on network or other general error  
            },
            dataTable: {
                "fnPreDrawCallback": function(oSettings) {
                    $('.group-checkable').attr('checked', false);
                    $('.group-checkable').parents('span').removeClass('checked')
                },
                "aLengthMenu": [
                    [20, 50, 100, 150, -1],
                    [20, 50, 100, 150, "All"] // change per page values here
                ],
                "bDestroy": true,
                "iDisplayLength": 20, // default record count per page
                "bServerSide": true, // server side processing
                "sAjaxSource": baseurl + 'admin/pen_res/datatable/' + fistdate + '/' + secounddate, // ajax source
                "aaSorting": [[0, "desc"]], // set first column as a default sort by asc
                "aoColumnDefs": [{// define columns sorting options(by default all columns are sortable extept the first checkbox column)
                        'bSortable': false,
                        'aTargets': [-1, -1],
                    }],
                'fnDrawCallback': function() {
                    App.inittooltip(); // reinitialize tooltip
                    ComponentsPickers.init();
                }
            }
        });

        //    var example_range_from_1 = $('#example_range_from_1').val();
        //   var example_range_to_1 = $('#example_range_to_1').val();
        //  alert(example_range_from_1);
        //  alert(fistdate);
        $('.pagination-panel').remove();
        $('#tracking_table_info').remove();
        $('.dataTables_info').remove();
        $('.seperator').html('');
        $('#tracking_table_wrapper .row div:first').removeClass('col-md-8 col-md-6');
        $('#tracking_table_wrapper .row div:first').addClass('col-md-3 col-sm-12');
        $('#tracking_table_wrapper .row div:nth-child(2)').addClass('customeclassadd');
//       $('#tracking_table_wrapper .row div:second').addClass('col-md-6 col-sm-12');  
        var search_html = $('.search_box_custome').clone();
        if (fistdate != 0)
        {
            search_html.find('#example_range_from_1').val(fistdate);
        }
        if (secounddate != 0)
        {
            search_html.find('#example_range_to_1').val(secounddate);
        }

        $($($('#tracking_table_wrapper').children()[0]).children()[0]).after(search_html)
    }

    var genrel = function() {
        $('body').on('change', '#example_range_from_1', function() {

            var from = $(this).val();
            var to = $(this).parent().next().next().find('input').val();
            //  var to = $('#example_range_to_1').val();

            headle_tracking_table(from, to);
        })
        $('body').on('change', '#example_range_to_1', function() {
            var to = $(this).val();
            var from = $(this).parent().prev().prev().find('input').val();
            //    console.log(from);
            //   console.log(to);
            //  alert(from);
            //   alert(to);
            headle_tracking_table(from, to);
        })
    }
    
    var getquote = function() {

        $('body').on('change', '.ksite', function() {
            var valuesite = $(this).val();
            var valuesite1 = valuesite.replace(/\,/g, '');
//            var value = valuesite.substring(1);
            var siteval = $(this);

            if (parseFloat(valuesite1) >= '20.00') {
//                alert(valuesite)
//                console.log(parseFloat(value));
                siteval.parent().parent().parent().parent().find('.site1').addClass('has-success');
                siteval.parent().parent().parent().parent().find('.site1').removeClass('has-error');
            } else {
//                alert(valuesite);
                siteval.parent().parent().parent().parent().find('.site1').addClass('has-error');
//                console.log(siteval.parent().parent().parent().parent().find('.site1').addClass('has-error'));
                siteval.parent().parent().parent().parent().find('.site1').removeClass('has-success');
            }
        });
        $('body').on('click', '#conform_list', function() {
            var id = $(this).attr('data-id');

            $.ajax({
                type: 'post',
                url: baseurl + 'admin/pen_res/getconfirm_res',
                data: {"id": id},
                success: function(data) {
                    if (data) {
                        $('.quoterequest').html(data);
                        $('select').select();
                    }
                }
            });
        });
        $('body').on('click', '#paid_list', function() {
            var id = $(this).attr('data-id');
            var values = $('.idvalues').val(id);

        });

    }
    
    var handle_manage = function() {
//        alert('ghghjg')
        $('#tracking_table_length').hide();
    }
//    $('#minusquote').click(function() {
//        $('.quote').html('');
//        countvalue = 1;
//    });
    var add_paid = function() {
        $(".amount").maskMoney({
            prefix: '$',
            allowNegative: true,
            thousands: ',',
            decimal: '.',
            affixesStay: false}
        );
        var form = $('#add_paid');
        var error = $('.alert-danger', form);
        var success = $('.alert-success', form);
        
        success.hide();
        error.show();
        $(form).validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                source: {
                    required: true
                },
                amount: {
                    required: true,
                    number: true
                },
                source_confirmation: {
                    required: true
                }
            },
            invalidHandler: function(event, validator) { //display error alert on form submit              
                success.hide();
                error.show();
                //App.scrollTo(error, -200);
            },
            errorPlacement: function(error, element) { // render error placement for each input type
                var icon = $(element).parent('.input-icon').children('i');
                icon.removeClass('fa-check').addClass("fa-warning");
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error'); // set success class to the control group                    
                icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
            },
            highlight: function(element) { // hightlight error inputs
                $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            unhighlight: function(element) { // revert the change done by hightlight
                $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            success: function(label, element) {
                $('.form-group').removeClass('has-success has-error');
                var icon = $(element).parent('.input-icon').children('i');
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                icon.removeClass("fa-warning").addClass("fa-check");
            },
            submitHandler: function(form) {
                var url = baseurl + "admin/pen_res/add_pending";
                var data = $(form).serialize();
                ajaxcall(url, data, function(output) {
                     
                    if (output == 'success') {
                        Toastr.init('success', 'Welldone..', 'Pending reservations Update successfully..');
                        $('#myModal_paid').modal('hide');
                        setTimeout(function(){
                            window.location.href = baseurl+'admin/pen_res';
                        },2000);
                        $(form)[0].reset();
                    } else {
                        Toastr.init('error', 'Oops..', 'Quote not added...');
                    }
                });
            }
        });
        //apply validation on select2 dropdown value change, this only needed for chosen dropdown integration.
        $('.select2me', form).change(function() {
            form.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
        });
        //});
    }
    return{
        init: function() {
            quote_fill_val();
            rplyquote();
            getquote();
            headle_tracking_table();
            genrel();
            handle_manage();
            add_paid();
        }
    };
}();


$.ui.autocomplete.prototype._renderItem = function(ul, item) {
    item.label = item.label.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(this.term) + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "<span style='background-color:#ff9'>$1</span>");
    //     console.log(item.label);
    return $("<li></li>")
            .data("item.autocomplete", item)
            .append("<a>" + item.label + "</a>")
            .appendTo(ul);
};