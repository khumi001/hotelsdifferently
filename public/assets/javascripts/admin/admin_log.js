
var Adminlog = function () {
    var amchart = function () {
        $('#sample_2').dataTable({
             "aaSorting": [[ 2, "desc" ]]
        });
        jQuery('#sample_2_wrapper .dataTables_filter input').addClass("form-control input-large input-inline"); // modify table search input
        jQuery('#sample_2_wrapper .dataTables_length select').addClass("form-control input-xsmall"); // modify table per page dropdown
        jQuery('#sample_2_wrapper .dataTables_length select').select2(); // initialize select2 dropdown
    }
    return{
        init: function () {
            amchart();
        }
    };
}();