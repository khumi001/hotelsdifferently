var Datamanage = function() {
    
    var handle_records = function() {

        var grid = new Datatable_Apply();
        grid.init({
            src: $("#hotellist1"),
            onSuccess: function(grid) {
                // execute some code after table records loaded
            },
            onError: function(grid) {
                // execute some code on network or other general error  
            },
            dataTable: {
                "fnPreDrawCallback": function(oSettings) {
                    $('.group-checkable').attr('checked', false);
                    $('.group-checkable').parents('span').removeClass('checked')
                },
                "aLengthMenu": [
                    [20, 50, 100, 150, 200],
                    [20, 50, 100, 150, 200] // change per page values here
                ],
                "bDestroy": true,
                "iDisplayLength": 50, // default record count per page
                "bServerSide": true, // server side processing
                "sAjaxSource": baseurl + 'admin/datamanage/hotellist_datatable', // ajax source
                "aaSorting": [[5, "desc"]] // set first column as a default sort by asc
            }
        });
    }
    
    
    var add = function() {
        $('.countryadd').click(function() {
            var country = $('.country').val();
            $.ajax({
                type: 'Post',
                url: baseurl + 'admin/datamanage/addcountry',
                data: {country: country},
                success: function(output) {
                    if (output == 'success') {
                        Toastr.init('success', 'Welldone..', 'Country Add successfully..');
                        handle_click();
                    } else {
                        Toastr.init('error', 'Oops..', 'Something Will Worng..');
                    }
                }
            })
        })
        $('.stateadd').click(function() {
            var state = $('.state').val();
//            var countryid = $('.country_check:checked').attr('data-id');
            var countryid = $( ".list_box" ).find('.country_id').attr('data-id');
        //    alert(countryid);
            $.ajax({
                type: 'Post',
                url: baseurl + 'admin/datamanage/addstate',
                data: {state: state, fk_contry: countryid},
                success: function(output) {
                    if (output == 'success') {
                        Toastr.init('success', 'Welldone..', 'State Add successfully..');
                        handle_click();
                    } else {
                        Toastr.init('error', 'Oops..', 'Something Will Worng..');
                    }
                }
            })
        })
        $('.cityadd').click(function() {
            var city = $('.city').val();
//            var countryid = $('.country_check:checked').attr('data-id');
             var countryid = $( ".list_box" ).find('.country_id').attr('data-id');
             var stateid = $( ".list_box" ).find('.state_id').attr('data-id');
//            var stateid = $('.state_check:checked').attr('data-id');
            $.ajax({
                type: 'Post',
                url: baseurl + 'admin/datamanage/addcity',
                data: {city: city, fk_state: stateid, fk_contry: countryid},
                success: function(output) {
                    if (output == 'success') {
                        Toastr.init('success', 'Welldone..', 'City Add successfully..');
                        handle_click();
                    } else {
                        Toastr.init('error', 'Oops..', 'Something Will Worng..');
                    }
                }
            })
        })
        $('.hoteladd').click(function() {
            var hotel = $('.hotel').val();
//            var countryid = $('.country_check:checked').attr('data-id');
//            var stateid = $('.state_check:checked').attr('data-id');
//            var cityid = $('.city_check:checked').attr('data-id');
            var countryid = $( ".list_box" ).find('.country_id').attr('data-id');
             var stateid = $( ".list_box" ).find('.state_id').attr('data-id');
             var cityid = $( ".list_box" ).find('.city_id').attr('data-id');
            $.ajax({
                type: 'Post',
                url: baseurl + 'admin/datamanage/addhotel',
                data: {hotel: hotel, fk_contry: countryid, fk_state: stateid, fk_city: cityid},
                success: function(output) {
                    if (output == 'success') {
                        Toastr.init('success', 'Welldone..', 'Hotel Add successfully..');
                        handle_click();
                    } else {
                        Toastr.init('error', 'Oops..', 'Something Will Worng..');
                    }
                }
            })
        })
    }

    var delete_data = function() {
        $('.countrydelete').click(function() {
//            var id = $('input:radio[name=country]:checked').attr('data-id');
            var id = $( ".list_box" ).find('.country_id').attr('data-id');
            $.ajax({
                type: 'Post',
                url: baseurl + 'admin/datamanage/deletecountry',
                data: {id: id},
                success: function(output) {
                    if (output == 'success') {
                        Toastr.init('success', 'Welldone..', 'Country Delete successfully..');
                        handle_click();
                    } else {
                        Toastr.init('error', 'Oops..', 'Something Will Worng..');
                    }
                }
            })
        })
        $('.statedelete').click(function() {
//            var id = $('input:radio[name=state]:checked').attr('data-id');
            var id = $( ".list_box" ).find('.state_id').attr('data-id');
            $.ajax({
                type: 'Post',
                url: baseurl + 'admin/datamanage/deletestate',
                data: {id: id},
                success: function(output) {
                    if (output == 'success') {
                        Toastr.init('success', 'Welldone..', 'State Delete successfully..');
                        handle_click();
                    } else {
                        Toastr.init('error', 'Oops..', 'Something Will Worng..');
                    }
                }
            })
        })
        $('.citydelete').click(function() {
//            var id = $('input:radio[name=city]:checked').attr('data-id');
            var id = $( ".list_box" ).find('.city_id').attr('data-id');
            $.ajax({
                type: 'Post',
                url: baseurl + 'admin/datamanage/deletecity',
                data: {id: id},
                success: function(output) {
                    if (output == 'success') {
                        Toastr.init('success', 'Welldone..', 'City Delete successfully..');
                        handle_click();
                    } else {
                        Toastr.init('error', 'Oops..', 'Something Will Worng..');
                    }
                }
            })
        })
        $('.hoteldelete').click(function() {
//            var id = $('input:radio[name=hotel]:checked').attr('data-id');
            var id = $( ".list_box" ).find('.hotel_id').attr('data-id');
            $('#btndelete').click(function() {
                $.ajax({
                    type: 'Post',
                    url: baseurl + 'admin/datamanage/deletehotel',
                    data: {id: id},
                    success: function(output) {
                        if (output == 'success') {
                            Toastr.init('success', 'Welldone..', 'Hotel Delete successfully..');
                            setTimeout(function() {
                                location.reload();
                            }, 1000);
                        } else {
                            Toastr.init('error', 'Oops..', 'Something Will Worng..');
                        }
                    }
                })
            })
        })
    }

    var get_data = function() {

        $('.country_sel').click(function() {
            $('.country_sel').removeClass('selection');
             $('.country_sel').removeClass('country_id');
            $(this).addClass('selection');
            $(this).addClass('country_id');
            var country_id = $(this).attr('data-id');
            var value = $(this).text();
            $('.country').val(value);
            $('.countryedit').css('display', 'inline-block');
            $('.countryadd').css('display', 'none');
            $('.state').removeAttr('disabled');
            //  alert(value);
            $.ajax({
                type: 'Post',
                url: baseurl + 'admin/datamanage/get_state',
                data: {countryid: country_id},
                success: function(output) {
                    console.log(output);
                    if (output == "Null") {
                        $('.state_list').html('<div class="list_box radio-list"></div>');
                        $('.city_list').html('<div class="list_box radio-list"></div>');
                        $('.hotel_list').html('<div class="list_box radio-list"></div>');
                        $('.state').val('');
                        $('.city').val('');
                        $('.hotel').val('');
                        $('.stateedit').css('display', 'none');
                        $('.stateadd').css('display', 'inline-block');
                        $('.cityedit').css('display', 'none');
                        $('.cityadd').css('display', 'inline-block');
                        $('.hoteledit').css('display', 'none');
                        $('.hoteladd').css('display', 'inline-block');
                    } else {
                        $('.state_list').html(output);
                    }
                },
            });
        });

        $("body").on('click', '.state_sel', function() {
            var country_id = $(".list_box").find('.country_id').attr('data-id');
            $('.state_sel').removeClass('selection');
            $(this).addClass('selection');
            $(this).addClass('state_id');
            var id = $(this).attr('data-id');
            var value = $(this).text();

            $('.state').val(value);
            $('.stateedit').css('display', 'inline-block');
            $('.stateadd').css('display', 'none');
            $('.city').removeAttr('disabled');
            //alert(value);
            $.ajax({
                type: 'Post',
                url: baseurl + 'admin/datamanage/get_city',
                data: {countryid: country_id, stateid: id},
                success: function(output) {
                    //   console.log(output);
                    $('.city_list').html(output);
                }
            });
        });

        $("body").on('click', '.city_sel', function() {
            var country_id = $(".list_box").find('.country_id').attr('data-id');
            var state_id = $(".list_box").find('.state_id').attr('data-id');
            $('.city_sel').removeClass('selection');
            $(this).addClass('selection');
            $(this).addClass('city_id');
            var id = $(this).attr('data-id');
            var value = $(this).text();

            $('.city').val(value);
            $('.cityedit').css('display', 'inline-block');
            $('.cityadd').css('display', 'none');
            $('.hotel').removeAttr('disabled');
            
            //alert(value);
            $.ajax({
                type: 'Post',
                url: baseurl + 'admin/datamanage/get_hotel',
                data: {countryid: country_id, stateid: state_id, cityid: id},
                success: function(output) {
                    //   console.log(output);
                    $('.hotel_list').html(output);
                }
            });
        });
        $("body").on('click', '.hotel_sel', function() {

            $('.hotel_sel').removeClass('selection');
            $(this).addClass('selection');
            $(this).addClass('hotel_id');
            var id = $(this).attr('data-id');
            var value = $(this).text();
            $('.hotel').val(value);
            $('.hoteledit').css('display', 'inline-block');
            $('.hoteladd').css('display', 'none');
        });
//        $("input:radio[name=country]").click(function() {
//            var id = $(this).attr('data-id');
//            var value = $(this).val();
//            $('.country').val(value);
//            $('.countryedit').css('display', 'inline-block');
//            $('.countryadd').css('display', 'none');
//            $('.state').removeAttr('disabled');
//            $.ajax({
//                type: 'Post',
//                url: baseurl + 'admin/datamanage/get_state',
//                data: {countryid: id},
//                success: function(output) {
//                    console.log(output);
//                    if (output == "Null") {
//                        $('.state_list').html("<div class='radio-list list_box'></div>");
//                        $('.city_list').html("<div class='radio-list list_box'></div>");
//                        $('.hotel_list').html("<div class='radio-list list_box'></div>");
//                        $('.state').val('');
//                        $('.city').val('');
//                        $('.hotel').val('');
//                        $('.stateedit').css('display', 'none');
//                        $('.stateadd').css('display', 'inline-block');
//                        $('.cityedit').css('display', 'none');
//                        $('.cityadd').css('display', 'inline-block');
//                        $('.hoteledit').css('display', 'none');
//                        $('.hoteladd').css('display', 'inline-block');
//                    } else {
//                        $('.state_list').html(output);
//                        var test = $("input[type=checkbox]:not(.toggle, .make-switch), input[type=radio]:not(.toggle, .star, .make-switch)");
//                        if (test.size() > 0) {
//                            test.each(function() {
//                                if ($(this).parents(".checker").size() == 0) {
//                                    $(this).show();
//                                    $(this).uniform();
//                                }
//                            });
//                        }
//                    }
//                },
//            });
//        });

        $("body").on('click', 'input:radio[name=state]', function() {
            alert('hi')
            var id = $(this).attr('data-id');
            var countryid = $('.country_check:checked').attr('data-id');
            var value = $(this).val();
            $('.state').val(value);
            $('.stateedit').css('display', 'inline-block');
            $('.stateadd').css('display', 'none');
            $('.city').removeAttr('disabled');
            $.ajax({
                type: 'Post',
                url: baseurl + 'admin/datamanage/get_city',
                data: {countryid: countryid, stateid: id},
                success: function(output) {
                    $('.city_list').html(output);
                    var test = $("input[type=checkbox]:not(.toggle, .make-switch), input[type=radio]:not(.toggle, .star, .make-switch)");
                    if (test.size() > 0) {
                        test.each(function() {
                            if ($(this).parents(".checker").size() == 0) {
                                $(this).show();
                                $(this).uniform();
                            }
                        });
                    }
                },
            })
        });

        $("body").on('click', 'input:radio[name=city]', function() {
            var id = $(this).attr('data-id');
            var countryid = $('.country_check:checked').attr('data-id');
            var stateid = $('.state_check:checked').attr('data-id');
            var value = $(this).val();
            $('.city').val(value);
            $('.cityedit').css('display', 'inline-block');
            $('.cityadd').css('display', 'none');
            $('.hotel').removeAttr('disabled');
            $.ajax({
                type: 'Post',
                url: baseurl + 'admin/datamanage/get_hotel',
                data: {countryid: countryid, stateid: stateid, cityid: id},
                success: function(output) {
                    $('.hotel_list').html(output);
                    var test = $("input[type=checkbox]:not(.toggle, .make-switch), input[type=radio]:not(.toggle, .star, .make-switch)");
                    if (test.size() > 0) {
                        test.each(function() {
                            if ($(this).parents(".checker").size() == 0) {
                                $(this).show();
                                $(this).uniform();
                            }
                        });
                    }
                },
            })
        });


        $("body").on('click', 'input:radio[name=hotel]', function() {
            var id = $(this).attr('data-id');
            var value = $(this).val();
            $('.hotel').val(value);
            $('.hoteledit').css('display', 'inline-block');
            $('.hoteladd').css('display', 'none');
        });
    }

    var edit_data = function() {
        $('.countryedit').click(function() {
            var country = $('.country').val();
            var id = $(".list_box").find('.country_id').attr('data-id');
            $.ajax({
                type: 'Post',
                url: baseurl + 'admin/datamanage/updatecountry',
                data: {country: country, id: id},
                success: function(output) {
                    if (output == 'success') {
                        Toastr.init('success', 'Welldone..', 'Country Edit successfully..');
                        handle_click();
                    } else {
                        Toastr.init('error', 'Oops..', 'Something Will Worng..');
                    }
                },
            });
        });
        $('.stateedit').click(function() {
            var state = $('.state').val();
//            var id = $('input:radio[name=state]:checked').attr('data-id');
            var id = $(".list_box").find('.state_id').attr('data-id');
            $.ajax({
                type: 'Post',
                url: baseurl + 'admin/datamanage/updatestate',
                data: {state: state, id: id},
                success: function(output) {
                    if (output == 'success') {
                        Toastr.init('success', 'Welldone..', 'State Edit successfully..');
                        handle_click();
                    } else {
                        Toastr.init('error', 'Oops..', 'Something Will Worng..');
                    }
                },
            });
        });
        $('.cityedit').click(function() {
            var city = $('.city').val();
//            var id = $('input:radio[name=city]:checked').attr('data-id');
            var id = $(".list_box").find('.city_id').attr('data-id');
            $.ajax({
                type: 'Post',
                url: baseurl + 'admin/datamanage/updatecity',
                data: {city: city, id: id},
                success: function(output) {
                    if (output == 'success') {
                        Toastr.init('success', 'Welldone..', 'City Edit successfully..');
                        handle_click();
                    } else {
                        Toastr.init('error', 'Oops..', 'Something Will Worng..');
                    }
                },
            });
        });
        $('.hoteledit').click(function() {
            var hotel = $('.hotel').val();
//            var id = $('input:radio[name=hotel]:checked').attr('data-id');
            var id = $(".list_box").find('.hotel_id').attr('data-id');
            $.ajax({
                type: 'Post',
                url: baseurl + 'admin/datamanage/updatehotel',
                data: {hotel: hotel, id: id},
                success: function(output) {
                    if (output == 'success') {
                        Toastr.init('success', 'Welldone..', 'Hotel Edit successfully..');
                        handle_click();
                    } else {
                        Toastr.init('error', 'Oops..', 'Something Will Worng..');
                    }
                },
            });
        });
    }

    return{
        init: function() {
            handle_records();
        },
        init_add: function() {
            
            add();
        },
        int_delete: function() {
            delete_data();
        },
        int_get: function() {
            get_data();
        },
        int_edit: function() {
            edit_data();
        },
    };
}();


