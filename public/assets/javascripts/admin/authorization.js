var id = 0;
var Authorization = function() {
    var giveauthorization = function() {
        $('body').on('click', '.btn_authorization', function() {
            $('#deleteModal').modal('show');
            id = $(this).attr('data-id');
        });

        $('body').on('click', '#deleteButton', function() {
            if($('#deletePassword').val() == "bUw#kef5ate$U8a2" ){
                $('#deleteModal').modal('hide');
                $('#btn_submit').attr('data-id', id);
                $.ajax({
                    type: 'post',
                    url: baseurl + 'admin/authorization/getauthorizemenu',
                    data: {id: id},
                    success: function(output) {
                        $('#lll').html(output);
                        $('#mymodal_autho').modal('show');
                        //         var data = JSON.parse(output);
                        //         console.log(data);
//                     console.log(data.length);
//                    for (var i = 0; i < data.length; i++) {
//                        var checkBox = "<input type='checkbox'   value='" + data[i].ID + "'/>" + data[i].Name + "<br/>";
//                        $(checkBox).appendTo('#modifiersDiv');
//                    }
                    }
                });
            }else{
                alert('Sorry! Password is wrong.');
            }
        });

        $('#btn_submit').click(function() {
            var id = $(this).attr('data-id');

            $.ajax({
                type: 'post',
                url: baseurl + 'admin/authorization/checkmenu',
                data: $('#authorization').serialize() + '&id=' + id,
                success: function(output) {
                    if (output == 'success')
                    {
                        Toastr.init('success', 'Weldone..', 'Authorization successfully done..');
                        $('#mymodal_autho').modal('hide');
//                        handle_click();
                    }
                    else if (output == 'error')
                    {
                        Toastr.init('error', 'Oops..', 'Authorization not done');
                    }

                }
            });
        });

        // begin second table
        $('#sample_2').dataTable({
            "aLengthMenu": [
                [25, 50, 100, -1],
                [25, 50, 100, "All"] // change per page values here
            ],
            // set the initial value
            "iDisplayLength": 25,
            "sPaginationType": "bootstrap",
            "oLanguage": {
                "sLengthMenu": "_MENU_ records",
                "oPaginate": {
                    "sPrevious": "Prev",
                    "sNext": "Next"
                }
            },
            "aaSorting": [[3, "asc"]]
        });

        jQuery('#sample_2 .group-checkable').change(function () {
            var set = jQuery(this).attr("data-set");
            var checked = jQuery(this).is(":checked");
            jQuery(set).each(function () {
                if (checked) {
                    $(this).attr("checked", true);
                } else {
                    $(this).attr("checked", false);
                }
            });
            jQuery.uniform.update(set);
        });

        jQuery('#sample_2_wrapper .dataTables_filter input').addClass("form-control input-large input-inline"); // modify table search input
        jQuery('#sample_2_wrapper .dataTables_length select').addClass("form-control input-xsmall"); // modify table per page dropdown
        jQuery('#sample_2_wrapper .dataTables_length select').select2(); // initialize select2 dropdown

    }

    var general = function(){

        $('body').on('click','.btn_delete_user',function(){
            var email = $(this).attr('data-email');
            var id = $(this).attr('data-id');
            $('.user_email').html('<h5>Are you sure you want to delete '+ email +'?</h5>');
            $('#btndelete').attr('data-id',id);
        });

        $('#btndelete').on('click',function(){
            var user_id = $(this).attr('data-id');
            $.ajax({
                type:'post',
                url:baseurl + 'admin/authorization/deleteuser',
                data:{id:user_id},
                success:function(output){

                    if (output == 'success')
                    {
                        Toastr.init('success', 'Weldone..', 'Successfully Delete..');
                        $('#mymodal_delete').modal('hide');
                        setTimeout(function(){
                            location.reload();
                        },500);

//                        handle_click();
                    }
                    else if (output == 'error')
                    {
                        Toastr.init('error', 'Oops..', 'Some thing will be wrong');
                    }
                }
            })
        })
    }
    return{init: function() {
        giveauthorization();
        general();
    }
    };
}();