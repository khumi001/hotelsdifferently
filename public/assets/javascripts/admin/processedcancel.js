var Processedcancel = function() {
   
    var headle_tracking_table = function(fistdate, secounddate) {
        if (!fistdate) {
            fistdate = 0;
        }
        if (!secounddate) {
            secounddate = 0;
        }
        var grid = new Datatable_Apply();
        grid.init({
            src: $("#tracking_table"),
            onSuccess: function(grid) {
                // execute some code after table records loaded
            },
            onError: function(grid) {
                // execute some code on network or other general error  
            },
            dataTable: {
                "fnPreDrawCallback": function(oSettings) {
                    $('.group-checkable').attr('checked', false);
                    $('.group-checkable').parents('span').removeClass('checked')
                },
                "aLengthMenu": [
                    [20, 50, 100, 150, -1],
                    [20, 50, 100, 150, "All"] // change per page values here
                ],
                "bDestroy": true,
                "iDisplayLength": 10, // default record count per page
                "bServerSide": true, // server side processing
                "sAjaxSource": baseurl + 'admin/processedcancel/datatable/' + fistdate + '/' + secounddate, // ajax source
                "aaSorting": [[0, "desc"]], // set first column as a default sort by asc
                "aoColumnDefs": [{// define columns sorting options(by default all columns are sortable extept the first checkbox column)
                        'bSortable': false,
                        'aTargets': [-1, -1],
                    }],
                'fnDrawCallback': function() {
                    App.inittooltip(); // reinitialize tooltip
                    ComponentsPickers.init();
                }
            }
        });

        //    var example_range_from_1 = $('#example_range_from_1').val();
        //   var example_range_to_1 = $('#example_range_to_1').val();
        //  alert(example_range_from_1);
        //  alert(fistdate);
        $('.pagination-panel').remove();
        $('#tracking_table_info').remove();
        $('.dataTables_info').remove();
        $('.seperator').html('');
        $('#tracking_table_wrapper .row div:first').removeClass('col-md-8 col-md-6');
        $('#tracking_table_wrapper .row div:first').addClass('col-md-3 col-sm-12');
        $('#tracking_table_wrapper .row div:nth-child(2)').addClass('customeclassadd');
//       $('#tracking_table_wrapper .row div:second').addClass('col-md-6 col-sm-12');  
        var search_html = $('.search_box_custome').clone();
        if (fistdate != 0)
        {
            search_html.find('#example_range_from_1').val(fistdate);
            console.log(search_html.find('#example_range_from_1').val(fistdate));
        }
        if (secounddate != 0)
        {
            search_html.find('#example_range_to_1').val(secounddate);
        }

        $($($('#tracking_table_wrapper').children()[0]).children()[0]).after(search_html)
    }
    
      var genrel = function() {
        $('body').on('change', '#example_range_from_1', function() {
            var from = $(this).val();
            var to = $(this).parent().next().next().find('input').val();
            //  var to = $('#example_range_to_1').val();
            headle_tracking_table(from, to);
        })
        $('body').on('change', '#example_range_to_1', function() {
            var to = $(this).val();
            var from = $(this).parent().prev().prev().find('input').val();
            //    console.log(from);
            //   console.log(to);
//              alert(from);
//               alert(to);
            headle_tracking_table(from, to);
        })
        $('body').on('click', '.resetbutton', function() {
            headle_tracking_table();

        });
        $('body').on('click', '.sentquote', function() {
            var id = $(this).attr('data-id');
             var userid = $(this).attr('proccesuser-id');

            $.ajax({
                type: 'post',
                url: baseurl + 'admin/pen_res/getconfirm_res',
                data: {"id": id,'userid':userid},
                success: function(data) {
                    if (data) {
                        $('.quoterequest').html(data);
                        $('select').select();
                    }
                }
            });
        });
    }
   
    return{
        init: function() {
            headle_tracking_table();
            genrel();

        }
    };
}();