
var Static = function() {

    var headle_tracking_table = function(fistdate, secounddate) {
        if (!fistdate) {
            fistdate = 0;
        }
        if (!secounddate) {
            secounddate = 0;
        }
        var grid = new Datatable_Apply();
        grid.init({
            src: $("#tracking_table"),
            onSuccess: function(grid) {
                // execute some code after table records loaded
            },
            onError: function(grid) {
                // execute some code on network or other general error  
            },
            dataTable: {
                "fnPreDrawCallback": function(oSettings) {
                    $('.group-checkable').attr('checked', false);
                    $('.group-checkable').parents('span').removeClass('checked')
                },
                "aLengthMenu": [
                    [20, 50, 100, 150, -1],
                    [20, 50, 100, 150, "All"] // change per page values here
                ],
                "bDestroy": true,
                "iDisplayLength": 10, // default record count per page
                "bServerSide": true, // server side processing
                "sAjaxSource": baseurl + 'admin/statistics/datatable/' + fistdate + '/' + secounddate, // ajax source
//                    "sAjaxSource": baseurl+'admin/conf_res/datatable/'+fistdate+'/'+secounddate, // ajax source
                "aaSorting": [[1,'asc']], // set first column as a default sort by asc
                "aoColumnDefs": [{// define columns sorting options(by default all columns are sortable extept the first checkbox column)
                        'bSortable': false,
                        'aTargets': [-1, -1],
                    }],
//                oTable.fnSort( [ [0,'desc'] ] );  
                'fnDrawCallback': function() {
                    App.inittooltip(); // reinitialize tooltip
                    ComponentsPickers.init();
                }
            }
        });


        $('.pagination-panel').remove();
        $('#tracking_table_info').remove();
        $('#tracking_table_length').remove();
        $('.dataTables_info').remove();
        $('.seperator').html('');
        $('#tracking_table_wrapper .row div:first').removeClass('col-md-8 col-md-4');
        $('#tracking_table_wrapper .row div:first').addClass('col-md-3 col-sm-12 hidden');
        var search_html = $('.search_box_custome').clone();
        //  var search_html = $('.search_box_custome').clone();
        if (fistdate != 0)
        {
            search_html.find('#example_range_from_1').val(fistdate);
        }
        if (secounddate != 0)
        {
            search_html.find('#example_range_to_1').val(secounddate);
        }
        $($($('#tracking_table_wrapper').children()[0]).children()[0]).after(search_html);
      //  $('#search_box_custome').css('margin-right','200');
    }

    var genrel = function() {
        $('body').on('change', '#example_range_from_1', function() {

            var from = $(this).val();
            // var to = $('#example_range_to_1').val();
            var to = $(this).parent().next().next().find('input').val();
            headle_tracking_table(from, to);
        })
        $('body').on('change', '#example_range_to_1', function() {
            var to = $(this).val();
            // var from = $('#example_range_from_1').val();
            var from = $(this).parent().prev().prev().find('input').val();
            headle_tracking_table(from, to);
        })
        $('body').on('click', '.resetbutton', function() {
           // alert('kk');
              headle_tracking_table();
        })
    }

    var amchart = function() {
//        $.ajax({
//            type: 'post',
//            url: baseurl + 'admin/statistics/chartdata',
//            data: {},
//            success: function(data) {
//                console.log(data);
//                var output = JSON.parse(data);
//                var chart = AmCharts.makeChart("chartdiv", {
//                    "type": "pie",
//                    "theme": "light",
//                    "dataProvider": output,
//                    "valueField": "litres",
//                    "titleField": "country",
//                    "export": {
//                        "enabled": true,
//                        "libs": {
//                            "path": "http://www.amcharts.com/lib/3/plugins/export/libs/"
//                        }
//                    }
//                });
//            }
//        });

   $('#sample_2').dataTable({
                "aLengthMenu": [
                    [25, 50, 100, -1],
                    [25, 50, 100, "All"] // change per page values here
                ],
                // set the initial value
                "iDisplayLength": 25,
                "sPaginationType": "bootstrap",
                "oLanguage": {
                    "sLengthMenu": "_MENU_ records",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                },
                "aaSorting": [[1, "desc"]]
            });

            jQuery('#sample_2 .group-checkable').change(function () {
                var set = jQuery(this).attr("data-set");
                var checked = jQuery(this).is(":checked");
                jQuery(set).each(function () {
                    if (checked) {
                        $(this).attr("checked", true);
                    } else {
                        $(this).attr("checked", false);
                    }
                });
                jQuery.uniform.update(set);
            });

            jQuery('#sample_2_wrapper .dataTables_filter input').addClass("form-control input-large input-inline"); // modify table search input
            jQuery('#sample_2_wrapper .dataTables_length select').addClass("form-control input-xsmall"); // modify table per page dropdown
            jQuery('#sample_2_wrapper .dataTables_length select').select2(); // initialize select2 dropdown


    }
    return{
        init: function() {
            headle_tracking_table();
            genrel();
            amchart();
        }
    };
}();


$.ui.autocomplete.prototype._renderItem = function(ul, item) {
    item.label = item.label.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(this.term) + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "<span style='background-color:#ff9'>$1</span>");
    //     console.log(item.label);
    return $("<li></li>")
            .data("item.autocomplete", item)
            .append("<a>" + item.label + "</a>")
            .appendTo(ul);
};