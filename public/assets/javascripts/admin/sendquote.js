var Sendquote = function () {
    
    var sendquote_tabel = function(){
        var grid = new Datatable_Apply();
        grid.init({
            src: $("#sendquote1"),
            onSuccess: function(grid) {
                // execute some code after table records loaded
            },
            onError: function(grid) {
                // execute some code on network or other general error  
            },
            dataTable: {
                "fnPreDrawCallback": function(oSettings) {
                    $('.group-checkable').attr('checked', false);
                    $('.group-checkable').parents('span').removeClass('checked')
                },
                "aLengthMenu": [
                    [20, 50, 100, 150, -1],
                    [20, 50, 100, 150, "All"] // change per page values here
                ],
                
                "bDestroy": true,
                "iDisplayLength": 15, // default record count per page
                "bServerSide": true, // server side processing
                "sAjaxSource": baseurl + 'admin/sentquotes/sendquote_datatable', // ajax source
                "aoColumnDefs" : [{  // define columns sorting options(by default all columns are sortable extept the first checkbox column)
                        'bSortable' : true,
                        'aTargets' : [ 0 ]
                    }],
                "aaSorting": [[0, "desc"]], // set first column as a default sort by asc
            }
        });         
    }
    
    var general = function (){
        $('body').on('click', '#sentquote', function () {
            var id = $(this).attr('data-id');
            $.ajax({
                type: 'post',
                url: baseurl + 'admin/sentquotes/getrequestquote',
                data: {"id": id},
                success: function (data) {
                    if (data) {
                        $('.quoterequest').html(data);
                    }
                }
            });
        });
    }
    return{
        init_table: function () {
            sendquote_tabel();
            general();
        }
    };
}();
