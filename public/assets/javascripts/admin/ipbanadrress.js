var IPban = function (){
    
    $('body').on('click','.radiobtn',function(){
        var radio_value = $(this).val();
        if(radio_value == 'option1'){
            $("#ban_by_acc").prop('disabled',true); 
            $("#ban_by_ip").prop('disabled',false); 
            $('.form-group').removeClass('has-error');
        }else{
            $("#ban_by_ip").prop('disabled',true);
            $("#ban_by_acc").prop('disabled',false); 
            $('.form-group').removeClass('has-error');
        }
    });
    var addipban = function(){
       
       $('.submit').click(function(){
            var radio_value = $('.radiobtn:checked').val();
            var ban_by_ip = $('#ban_by_ip').val();
            var ban_time = $('.bantime').val();
            var ban_by_acc = $('#ban_by_acc').val();
            var reason = $('.reason').val();
            var form = $('#addipban');
            if(radio_value == "option1")
            {
               if(ban_by_ip == "")
               {
                   $('#ban_by_ip').parent().parent().parent().addClass('has-error');
               }
               else{
                    $('#ban_by_ip').parent().parent().parent().removeClass('has-error');
               }
               if(reason == ""){
                   $('.reason').parent().parent().addClass('has-error');
               }
               else{
                   $('.reason').parent().parent().removeClass('has-error');
               }
               if(ban_time == ""){
                   $('.bantime').parent().parent().addClass('has-error');
               }else{
                    $('.bantime').parent().parent().removeClass('has-error');
               }
               if(ban_by_ip != "" && reason != "" && ban_time !="")
               {
                   var url = baseurl+"admin/ipaddressban/add";
                    var data =  $( form ).serialize() ;
                    ajaxcall(url,data,function(output){
                        if(output == 'success'){
                            Toastr.init('success','Welldone..','IP Address baned successfully..'); 
                            handle_click();
                        }
                        else if(output == 'usernotexits'){
                             Toastr.init('warning','Warning..','Email,Username, AccountID Is Not Exits');
                             handle_click();
                        }
                        else if(output == 'error'){
                                Toastr.init('error','Oops..','Something Will Worng...'); 
                                handle_click();
                        }                        
                    });
               }
            }
            if(radio_value == "option2")
            {
               if(ban_by_acc == "")
               {
                   $('#ban_by_acc').parent().parent().parent().addClass('has-error');
               }
               else{
                    $('#ban_by_acc').parent().parent().parent().removeClass('has-error');
               }
               if(reason == ""){
                   $('.reason').parent().parent().addClass('has-error');
               }
               else{
                   $('.reason').parent().parent().removeClass('has-error');
               }
               if(ban_time == ""){
                   $('.bantime').parent().parent().addClass('has-error');
               }else{
                    $('.bantime').parent().parent().removeClass('has-error');
               }
               if(ban_by_acc != "" && reason != "" && ban_time != "")
               {
                   var url = baseurl+"admin/ipaddressban/add";
                    var data =  $( form ).serialize() ;
                    ajaxcall(url,data,function(output){
                        if(output == 'success'){
                            Toastr.init('success','Welldone..','IP Address baned successfully..'); 
                            handle_click();
                        }
                        else if(output == 'usernotexits'){
                             Toastr.init('warning','Warning..','Email,Username, AccountID Is Not Exits');
                             handle_click();
                        }
                        else if(output == 'error'){
                                Toastr.init('error','Oops..','Something Will Worng...');     
                                handle_click();
                        }                        
                    });
               }
            }
            return false;
       });
//        var form = $('#addipban');
//        var error = $('.alert-danger', form);
//        var success = $('.alert-success', form);
//       $.validator.addMethod('IP4Checker', function(value) {
//            var ip = /^(\d|[1-9]\d|1\d\d|2([0-4]\d|5[0-5]))\.(\d|[1-9]\d|1\d\d|2([0-4]\d|5[0-5]))\.(\d|[1-9]\d|1\d\d|2([0-4]\d|5[0-5]))\.(\d|[1-9]\d|1\d\d|2([0-4]\d|5[0-5]))$/;
//            return value.match(ip);
//        }, 'Invalid IP address');
//        success.hide();
//        error.show();
//        form.validate({
//            errorElement: 'span', //default input error message container
//            errorClass: 'help-block', // default input error message class
//            focusInvalid: false, // do not focus the last invalid input
//            ignore: "",
//            
//            rules: {
//                 ban_by_ip: {
//                        IP4Checker: true,
//                    },               
//                reason:{
//                    required: true,
//                }
//            },
//            invalidHandler: function(event, validator) { //display error alert on form submit              
//                success.hide();
//                error.show();
//                App.scrollTo(error, -200);
//            },
//            errorPlacement: function(error, element) { // render error placement for each input type
//                var icon = $(element).parent('.input-icon').children('i');
//                icon.removeClass('fa-check').addClass("fa-warning");
//                $(element).closest('.form-group').removeClass('has-success').addClass('has-error'); // set success class to the control group                    
//                icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
//            },
//            highlight: function(element) { // hightlight error inputs
//                $(element)
//                        .closest('.form-group').addClass('has-error'); // set error class to the control group
//            },
//            unhighlight: function(element) { // revert the change done by hightlight
//                $(element)
//                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
//            },
//            success: function(label, element) {
//                var icon = $(element).parent('.input-icon').children('i');
//                $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
//                icon.removeClass("fa-warning").addClass("fa-check");
//            },
//            submitHandler: function(form) {
//                    var url = baseurl+"admin/ipaddressban/add";
//                    var data =  $( form ).serialize() ;
//                    ajaxcall(url,data,function(output){
//                        if(output == 'success'){
//                            Toastr.init('success','Welldone..','IP Address baned successfully..'); 
//                        }
//                        else if(output == 'usernotexits'){
//                             Toastr.init('warning','Warning..','Email,Username, AccountID Is Not Exits');
//                        }
//                        else if(output == 'error'){
//                                Toastr.init('error','Oops..','Something Will Worng...');                       
//                        }                        
//                    });
//                }
//            });
         }
    return{
        init:function(){
            addipban();
        }
    };
}();


