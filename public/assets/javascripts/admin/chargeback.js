var affiliate_id = '';

var Chargeback = function() {
    $('.chargeview').click(function() {
        affiliate_id = $(this).attr('id');

    });

    $('body').on('change', '.amount', function() {
        var value = $(this).val();
        if(parseFloat(value) > 0)
        {
            $.ajax({
            type: 'Post',
            url: baseurl + 'admin/chargeback/get_amount',
            data: {amount: value, id: affiliate_id},
            success: function(output) {
                if (output == 'success') {
                    $('.amount').closest('.form-group').addClass('has-success');
                    $('.amount').closest('.form-group').removeClass('has-error');
                } else {
                    $('.amount').val('');
                    $('.amount').closest('.form-group').removeClass('has-success');
                    $('.amount').closest('.form-group').addClass('has-error');

                }
            }
        });
        }
        else{
             $('.amount').val('');
        }
        
    })

    var add_chargeback = function() {
        $(".amount").maskMoney({
            prefix: '$',
            allowNegative: true,
            thousands: ',',
            decimal: '.',
            affixesStay: false}
        );
        var form = $('#add_quoterequests');
        var error = $('.alert-danger', form);
        var success = $('.alert-success', form);

        success.hide();
        error.show();
        $(form).validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                amount: {
                    required: true,
                    number: true
                },
                comment: {
                    required: true
                }

            },
            invalidHandler: function(event, validator) { //display error alert on form submit              
                success.hide();
                error.show();
                //App.scrollTo(error, -200);
            },
            errorPlacement: function(error, element) { // render error placement for each input type
                var icon = $(element).parent('.input-icon').children('i');
                icon.removeClass('fa-check').addClass("fa-warning");
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error'); // set success class to the control group                    
                icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
            },
            highlight: function(element) { // hightlight error inputs
                $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            unhighlight: function(element) { // revert the change done by hightlight
                $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            success: function(label, element) {
                var icon = $(element).parent('.input-icon').children('i');
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                icon.removeClass("fa-warning").addClass("fa-check");
            },
            submitHandler: function(form) {
//                var id = $('.chargeview').attr('id');
                var formdata = $(form).serialize();
                formdata += "&affiliate=" + affiliate_id;

                $.ajax({
                    type: 'post',
                    url: baseurl + 'admin/chargeback/get_affilate',
                    data: formdata,
                    success: function(output) {
                        var amount = $('.amount').val();
                        var comment = $('.comment').val();
                        var data = JSON.parse(output);
                        var html = 'Are you sure that you want to issue a chargeback to Affiliate ID ' + data[0].var_accountid + ' account in the amount of $' + amount + '?'
                        $('.chrgeback_html').html(html);
                        $('#chargeback_modal').modal('show');
                    }
                });
            }
        });

    }

    var click_btn = function() {
        $('body').on('click', '#btn', function() {
            var amount = $('.amount').val();
            var comment = $('.comment').val();
            $.ajax({
                type: 'Post',
                url: baseurl + 'admin/chargeback/add_data',
                data: {affiliate: affiliate_id, amount: amount, comment: comment},
                success: function(output) {
                    if (output == "sucess") {
                        Toastr.init('success', 'Chargeback Add successfully', '');
                            $('#chargeback_modal').modal('hide');
                            $("#myModal_autocomplete").modal('hide');
                            $('#add_quoterequests')[0].reset();
                    } else {
                        Toastr.init('error', 'Oops ....', 'Somthing Rong');
                    }
                }
            });
        });
    }

    return {
        init: function() {
            add_chargeback();
            click_btn();
        },
    };

}();