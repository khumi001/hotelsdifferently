$(document).ready(function(){
    $(".updateHotelPrice").click(function(){
        var hotelId = $(this).attr("data-hotel-id");
        var hotelName = $(this).attr("data-hotel-name");
        var hotelImageUrl = $(this).attr("data-hotel-image-url");
        var touricoDestinationName = $(this).attr("data-hotel-location-name");
        
        $("#hotelName").val( hotelName );
        $("#hotelId").val( hotelId );
        $("#hotelImageUrl").val( hotelImageUrl );
        $("#touricoDestinationName").val( touricoDestinationName );
        
        $("#hotelPriceSetModal").modal({"show":true});
    });
    
    $("#savePercentile").click(function(){
        $("#setPercentilePriceForm").serialize();
        
        $.post( baseurl+ "admin/booking_api/UpdateHotelPriceAjax", $("#setPercentilePriceForm").serialize() ).done(function( data ) {
            var josnData = JSON.parse(data);
            var hotelId = josnData.tourico_hotel_id;
            var multiplierInPercentage = josnData.multiplier_in_percentage;
            $("#percentile-"+hotelId).html( multiplierInPercentage+"%" );
            
            $("#hotelPriceSetModal").modal('hide');
            $("#profitPercentage").val("");
        });
        //return false;
    });
});