$(document).ready(function()
{
    $('#hotelSearchResultList').dataTable( {
        "iDisplayLength": 50,
        "dom": '<"toolbar">frtip'
    } );
    
    $('#hotelSearchResultList_length').parent().removeClass('col-md-6');
    $('#hotelSearchResultList_length').parent().addClass('col-md-7');
    
    $('#hotelSearchResultList_filter').parent().removeClass('col-md-6');
    $('#hotelSearchResultList_filter').parent().addClass('col-md-5');
    var BCTemplateHtml = $('#BCTemplate').html();
    var bindedElement = $(BCTemplateHtml);
//    bindedElement.find("#setAllHotelPrice").click(function(){
//        console.log('heheheh');
//         $.post( baseurl+ "admin/booking_api/UpdateAllHotelPriceAjax", $("#setAllHotelPriceForm").serialize() ).done(function( data ) {
//                var josnData = JSON.parse(data);
//                var all_hotel_multiplier = josnData.all_hotel_multiplier;
//                var hotel_id_list = josnData.hotel_id_list;
//                for(var index=0;index<hotel_id_list.length;index++)
//                {
//                    var aHotelId = hotel_id_list[index];
//                    $("#percentile-"+aHotelId).html( all_hotel_multiplier+"%" );
//                }
//        });
//    });
    $("#hotelSearchResultList_length").append(bindedElement);
    
    
    $('#hotelSearchResultList tbody').on('click', '.updateHotelPrice', function()
    {
        var hotelId = $(this).attr("data-hotel-id");
        var hotelName = $(this).attr("data-hotel-name");
        var hotelImageUrl = $(this).attr("data-hotel-image-url");
        var hotelCurrentMultiplier = $(this).attr("data-hotel-current-multiplier");
        
        $("#hotelName").val( hotelName );
        $("#hotelId").val( hotelId );
        $("#hotelImageUrl").val( hotelImageUrl );
        $("#profitPercentage").val( hotelCurrentMultiplier );

        $("#hotelPriceSetModal").modal({"show":true});
    });
    
    $("#setAllHotelPrice").click(function(){
         $.blockUI();
         $.post( baseurl+ "admin/booking_api/UpdateAllHotelPriceAjax", $("#setAllHotelPriceForm").serialize() ).done(function( data ) {
                $.unblockUI();
                var josnData = JSON.parse(data);
                var all_hotel_multiplier = josnData.all_hotel_multiplier;
                var hotel_id_list = josnData.hotel_id_list;
                for(var index=0;index<hotel_id_list.length;index++)
                {
                    var aHotelId = hotel_id_list[index];
                    $("#percentile-"+aHotelId).html( all_hotel_multiplier+"%" );
                }
        });
    });
    
    $("#savePercentile").click(function(){
        $("#setPercentilePriceForm").serialize();
        
        $.post( baseurl+ "admin/booking_api/UpdateHotelPriceAjax", $("#setPercentilePriceForm").serialize() ).done(function( data ) {
            var josnData = JSON.parse(data);
            var hotelId = josnData.tourico_hotel_id;
            var multiplierInPercentage = josnData.multiplier_in_percentage;
            $("#percentile-"+hotelId).html( multiplierInPercentage+"%" );
            
            $("#hotelPriceSetModal").modal('hide');
            $("#profitPercentage").val("");
        });
        //return false;
    });
});