$(document).ready(function(){
    $(".updateCruisePrice").click(function(){
        var cruiseId = $(this).attr("data-cruise-id");
        var cruiseName = $(this).attr("data-cruise-name");
        var shipImageUrl = $(this).attr("data-ship-image-url");
        var shipName = $(this).attr("data-ship-name");
        var touricoDestinationName = $(this).attr("tourico_destination_name");
        
        $("#cruiseName").val(cruiseName);
        $("#shipName").val(shipName);
        $("#cruiseId").val( cruiseId );
        $("#shipImageUrl").val( shipImageUrl );
        $("#touricoDestinationName").val( touricoDestinationName );
        
        $("#cruisePriceSetModal").modal({"show":true});
    });
    
    $("#savePercentile").click(function(){
        //$("#setPercentilePriceForm").serialize();
        
        $.post( baseurl+ "admin/Booking_api/UpdateCruisePriceAjax", $("#setPercentilePriceForm").serialize() ).done(function( data ) {
            var josnData = JSON.parse(data);
            var cruiseId = josnData.tourico_cruise_id;
            var multiplierInPercentage = josnData.multiplier_in_percentage;
            $("#percentile-"+cruiseId).html( multiplierInPercentage+"%" );
            
            $("#hotelPriceSetModal").modal({"show":false});
            $("#profitPercentage").val("");
        });
        //return false;
    });
});