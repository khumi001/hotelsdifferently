$(document).ready(function(){
    $(".updateCarLocationPercentile").click(function(){
        var carLocationId = $(this).attr("data-destination-id");
        
        $("#carLocationId").val( carLocationId );
        
        $("#carPriceSetModal").modal({"show":true});
    });
    
    $("#savePercentile").click(function(){
        $("#setPercentilePriceForm").serialize();
        
        $.post( baseurl+ "admin/Booking_api/AddUpdateLocationPriceAjax", $("#setPercentilePriceForm").serialize() ).done(function( data ) {
            var josnData = JSON.parse(data);
            var destinationId = josnData.tourico_destination_id;
            var multiplierInPercentage = josnData.multiplier_in_percentage;
            $("#percentile-"+destinationId).html( multiplierInPercentage+"%" );
            
            $("#carPriceSetModal").modal({"show":false});
            $("#profitPercentage").val("");
        });
        //return false;
    });
});