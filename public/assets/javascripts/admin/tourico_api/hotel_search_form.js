jQuery(document).ready(function(){
            jQuery( "#locationName" ).autocomplete({
                source: function(request, response) 
                    {
                        jQuery.getJSON(baseurl+ "get_destination_suggestion", { term: jQuery("#locationName").val() }, 
                              response);
                    },
                minLength: 3,
                select: function( event, ui ) 
                {
                    $("#locationId").val(ui.item.id);
                }
            });
    });
	$("#priceper").maskMoney({prefix: '', allowNegative: true, thousands: ',', decimal: '.', affixesStay: false});
	