var ind_val = [];
var Coupon = function() {
    $('body').on('click', '.status', function() {
        var id = $(this).attr('data-id');
        $.ajax({
            type: 'post',
            url: baseurl + 'admin/coupons/update_status',
            data: {id: id},
            success: function(data) {
                handle_click();
            }
        });
    });
    $("input[type='checkbox']").click(function() {
        if ($(this).is(':checked')) {
            var id = $(this).val();
            ind_val.push(id)
        } else {
            var id = $(this).val();
            if ($.inArray(id, ind_val) != '-1') {
                ind_val.splice($.inArray(id, ind_val), 1);
            }
        }
    });
    $('body').on('click', '.coup_off', function() {
        var value = $(this).val();
        var status = $(this).attr('data-status');
        
        if (status == 'individual') {
              var status =  ind_val;
        }
        if (value == 4) {
            $('.coupanoff4').removeAttr('disabled');
            $('.exdate2').removeAttr('disabled');
            $('.coupanoff3').attr('disabled', 'disabled');
            $('.coupanoff2').attr('disabled', 'disabled');
            $('.coupanoff1').attr('disabled', 'disabled');
            $('.exdate1').attr('disabled', 'disabled');
        }
        else if (value == 3) {
            $('.coupanoff3').removeAttr('disabled');
            $('.exdate2').removeAttr('disabled');
            $('.coupanoff4').attr('disabled', 'disabled');
            $('.coupanoff2').attr('disabled', 'disabled');
            $('.coupanoff1').attr('disabled', 'disabled');
            $('.exdate1').attr('disabled', 'disabled');
        }
        else if (value == 2) {
            $('.coupanoff2').removeAttr('disabled');
            $('.exdate1').removeAttr('disabled');
            $('.coupanoff3').attr('disabled', 'disabled');
            $('.coupanoff4').attr('disabled', 'disabled');
            $('.coupanoff1').attr('disabled', 'disabled');
            $('.exdate2').attr('disabled', 'disabled');
        }
        else if (value == 1) {
            $('.coupanoff1').removeAttr('disabled');
            $('.exdate1').removeAttr('disabled');
            $('.coupanoff2').attr('disabled', 'disabled');
            $('.coupanoff3').attr('disabled', 'disabled');
            $('.coupanoff4').attr('disabled', 'disabled');
            $('.exdate2').attr('disabled', 'disabled');
        }
       
        $('.dataaction').click(function() {
            var coupanoff1 = $('.coupanoff1').val();
            var coupanoff2 = $('.coupanoff2').val();
            var coupanoff3 = $('.coupanoff3').val();
            var coupanoff4 = $('.coupanoff4').val();
           $.ajax({
              type:'post',
              url:baseurl +'admin/coupons/coupon_generate',
              data:{'status':status,coupanoff1:coupanoff1,coupanoff2:coupanoff2,coupanoff3:coupanoff3,coupanoff4:coupanoff4},
              success:function(output){
                if(output == 'success'){
                 Toastr.init('success', 'Weldone..', 'Coupon is Generated...');
                 handle_click();
                }else{
                     Toastr.init('error', 'Oops..', 'Something Will worng...');
                 }         
              }
           });
            return false;
        })
       
    })

    $('body').on('click', '.bootstrap-switch', function() {
        var value = $('.change_coupan').val();
        $.ajax({
            type: 'post',
            url: baseurl + 'admin/coupons/singup_status',
            data: {status: value},
            success: function(output) {

            }
        });
    });
//    var coupon_generate = function() {
//        
//            var form = $('#coupongen');
//            var error1 = $('.alert-danger', form);
//            var success1 = $('.alert-success', form);    
//            form.validate({
//            errorElement: 'span', //default input error message container
//            errorClass: 'help-block', // default input error message class
//            focusInvalid: false, // do not focus the last invalid input
//            ignore: "",    
//            rules: {
//                expdate: {
//                  required:true,  
//                
//                },
//                off_expdate: {
//                    required: true,
//
//                },
//            },
//                invalidHandler: function (event, validator) { //display error alert on form submit              
//                    success1.hide();
//                    error1.show();
//                    App.scrollTo(error1, -200);
//                },
//
//                highlight: function (element) { // hightlight error inputs
//                    $(element)
//                        .closest('.form-group').addClass('has-error'); // set error class to the control group
//                },
//
//                unhighlight: function (element) { // revert the change done by hightlight
//                    $(element)
//                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
//                },
//
//                success: function (label) {
//                    label
//                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
//                },
//
//                submitHandler: function (form) {
//                    var url = baseurl+"admin/coupons/coupon_generate";
//                    var data =  $( form ).serialize() ;
//                    
//                    ajaxcall(url,data,function(output){
//                        if(output == 'success')
//                        {
//                            Toastr.init('success','Weldone..','Coupon is Generated...'); 
//                            handle_click();
//                        }
//                        else if(output == 'error')
//                        {
//                            Toastr.init('error','Oops..','Coupon is not Generated...');                       
//                        }                        
//                    });
//                }
//            });
//    }
    return{
        init: function() {
            //coupon_generate();
        }
    };
}();
