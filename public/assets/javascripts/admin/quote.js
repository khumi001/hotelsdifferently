var hotel_array = [];
var countvalue = 1;

var Quote = function () {
    var addquote = function () {

        var form = $('#add_quote');
        var error = $('.alert-danger', form);
        var success = $('.alert-success', form);
        success.hide();
        error.show();
        form.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                username: {
                    required: true,
                },
                firstname: {
                    required: true,
                },
                lastname: {
                    required: true,
                },
                address: {
                    required: true,
                },
                dob: {
                    required: true,
                },
                email: {
                    required: true,
                    email: true
                },
                password: {
                    required: true,
                }
            },
            invalidHandler: function (event, validator) { //display error alert on form submit              
                success.hide();
                error.show();
                App.scrollTo(error, -200);
            },
            errorPlacement: function (error, element) { // render error placement for each input type
                var icon = $(element).parent('.input-icon').children('i');
                icon.removeClass('fa-check').addClass("fa-warning");
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error'); // set success class to the control group                    
                icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
            },
            highlight: function (element) { // hightlight error inputs
                $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            success: function (label, element) {
                var icon = $(element).parent('.input-icon').children('i');
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                icon.removeClass("fa-warning").addClass("fa-check");
            },
            submitHandler: function (form) {
                // alert('useradd');
                var url = baseurl + "admin/quoterequests/add";
                var data = $(form).serialize();
                ajaxcall(url, data, function (output) {
                    if (output == 'success') {
                        Toastr.init('success', 'Welldone..', 'Quote added successfully..');
                        handle_click();
                    }
                    else if (output == 'user exits') {
                        Toastr.init('warning', 'Warning..', 'Email Already Exits');
                    }
                    else if (output == 'error') {
                        Toastr.init('error', 'Oops..', 'Quote not added...');
                    }
                });
            }
        });

        $('.select2me', form).change(function () {
            form.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
        });

        $(".price").maskMoney({prefix: '$', allowNegative: true, thousands: ',', decimal: '.', affixesStay: false});
        $(".ksite").maskMoney({prefix: '$', allowNegative: true, thousands: ',', decimal: '.', affixesStay: false});
        $(".ssite").maskMoney({prefix: '$', allowNegative: true, thousands: ',', decimal: '.', affixesStay: false});
        $(".ssrc").maskMoney({prefix: '$', allowNegative: true, thousands: ',', decimal: '.', affixesStay: false});
    }

    var quote_fill_val = function () {
        $('.quote_val').click(function () {
            var id = $(this).attr('data-id');
            var resubmit_id = $(this).attr('data-resubmit-id');
            var status = $(this).attr('data-status');
            if (status == 1) {
                var type = "M"
            } else {
                var type = ""
            }
            $('.parentaccount').val(type);
            $("#fk_quote").val(id);
            $('.quote').html('');
            $('.resubmitid').val(resubmit_id);
            $('#rplyquote')[0].reset();
            $('#rplyquote').children().removeClass('.has-error');
            $('#rplyquote').children().removeClass('.has-success');
            $('.col-md-5.valid_quot .select2-chosen').html('--Select--');
            $('.col-md-7.valid_quot .select2-chosen').html('--Select Star--');
            $('.col-md-77.valid_quot .select2-chosen').html('--Select Policy--');
            $("input").each(function () {
                $(this).parent().removeClass('has-error');
                $(this).parent().removeClass('has-success');
            });
            countvalue = 1;
            $("select").each(function () {
                $(this).parent().removeClass('has-error');
                $(this).parent().removeClass('has-success');
            });
            $.ajax({
                type: 'post',
                url: baseurl + "admin/quoterequests/getquoteval",
                data: {"id": id, "resubmit_id": resubmit_id},
                success: function (output) {
                    var data = JSON.parse(output);

                    if (resubmit_id != '') {

                        $('.city').val(data[0].var_city);
                        $('.point').val(data[0].var_points);
                        $('.accountid').val(data[0].var_accountid);
                        $('.requesteddate').val(data[0].dt_reuested_date);
                        $('.indate').val(data[0].var_checkin);
                        $('.outcheck').val(data[0].var_checkout);
                        $('.username').val(data[0].var_username);
                        $('.rooms').val(data[0].var_room);
                        $('.adults').val(data[0].var_adult);
                        $('.children').val(data[0].var_child);
                        $('.res_name').val(data[0].var_nameofreservation);
                        $('.hotelname').val(data[0].var_hotelname);
                        $('.star').val(data[0].var_rating);
                        $('.comment').val(data[0].var_comments);
                        $('.parentaccount').val(data[0].parentaccount);
                        $('.coupan').val(data[0].coupan);
                        if (data[0].parentaccount == "A" && data[0].coupan == "C")
                        {
                            $('.afflatehascoupanclass').removeClass('hidden');
                        }
                        else if (data[0].parentaccount == "A") {
                            $('.afflateclass').removeClass('hidden');
                        }
                        else if (data[0].coupan == "C") {
                            $('.coupanclass').removeClass('hidden');
                        }
                        else {
                            $('.afflatehascoupanclass').addClass('hidden');
                            $('.afflateclass').addClass('hidden');
                            $('.coupanclass').addClass('hidden');
                        }
                        $('h4').html('Quote # ' + data[0].var_uniq_quoteid);
                        $('.select_hotel').val(data[0].var_NOH);
                        $('.select_hotel_name').parent().find('.select2-chosen').html(data[0].var_site1);
                        $('.select_hotel_name').parent().find("option[value='" + data[0].var_site1 + "']").attr('selected', true);
                        $('.rating_star_hotel').parent().find('.select2-chosen').html(data[0].var_star);
                        $('.rating_star_hotel').parent().find("option[value='" + data[0].var_star + "']").attr('selected', true);
                        $('.ksite').val(data[0].int_site1_price);
                        $('.f_site').val(data[0].var_site2);
                        $('.ssite').val(data[0].int_site2_price);
                        $('.src').val(data[0].var_src);
                        $('.ssrc').val(data[0].int_src_price);
                        $('.price').val(data[0].var_prize);
                        $('.hotel_adviser').val(data[0].var_tripadvisor);
                        $('.hotel_comments').val(data[0].var_comment);
                        $('.room_type').val(data[0].var_room_type);
                        $('.hotel_cancelation').val(data[0].var_policy);
                    } else {

                        $('.city').val(data[0].var_city);
                        $('.point').val(data[0].var_points);
                        $('.accountid').val(data[0].var_accountid);
                        $('.requesteddate').val(data[0].dt_reuested_date);
                        $('.indate').val(data[0].var_checkin);
                        $('.outcheck').val(data[0].var_checkout);
                        $('.username').val(data[0].var_username);
                        $('.rooms').val(data[0].var_room);
                        $('.adults').val(data[0].var_adult);
                        $('.children').val(data[0].var_child);
                        $('.res_name').val(data[0].var_nameofreservation);
                        $('.hotelname').val(data[0].var_hotelname)

                        $('.star').val(data[0].var_rating);
                        $('.comment').val(data[0].var_comments);
                        $('.parentaccount').val(data[0].parentaccount);
                        $('.coupan').val(data[0].coupan);
                        if (data[0].parentaccount == "A" && data[0].coupan == "C")
                        {
                            $('.afflatehascoupanclass').removeClass('hidden');
                        }
                        else if (data[0].parentaccount == "A") {
                            $('.afflateclass').removeClass('hidden');
                        }
                        else if (data[0].coupan == "C") {
                            $('.coupanclass').removeClass('hidden');
                        }
                        else {
                            $('.afflatehascoupanclass').addClass('hidden');
                            $('.afflateclass').addClass('hidden');
                            $('.coupanclass').addClass('hidden');
                        }

                    }
                    var city = $('.city').val();
                    
                    $('body').on('click', '.popupclose', function () {
                        $('.afflatehascoupanclass').addClass('hidden');
                        $('.afflateclass').addClass('hidden');
                        $('.coupanclass').addClass('hidden');
                    });
                }
            });
        });
        
        $('body').on('focus','.select_hotel',function(){
            var that=$(this);
            $(this).autocomplete({
                source: function (request, response) {
                    var value = that.val();
                    hotel_array = [];
                    
                    $.ajax({
                        type: 'post',
                        url: baseurl + 'admin/quoterequests/get_hotel',
                        data: {cityvalue: value},
                        success: function (output) {
                            var data = JSON.parse(output);
                            var i;

                            for (i = 0; i < data.length; i++) {
                                if ($.inArray(data[i].var_hotel_name + ',' + data[i].var_city + ',' + data[i].var_state + ',' + data[i].var_country, hotel_array) == -1) {
                                    hotel_array.push(data[i].var_hotel_name + ',' + data[i].var_city + ',' + data[i].var_state + ',' + data[i].var_country);
                                }
                            }
                            response(hotel_array);

                        },
                        error: function (data) {
                            $('input.suggest-user').removeClass('ui-autocomplete-loading');
                        }
                    });
                    
                },

            });
        });
        
        $('body').on('change','.select_hotel',function(){
            
            var value = $(this).val();
            var this_pos = $(this).attr('data-position');
            var this_sss = $(this);
            $.ajax({
                type:'post',
                url:baseurl + 'admin/quoterequests/address_star',
                data:{'value':value},
                success:function(output){
                    var data = JSON.parse(output);
                    if (this_pos == '1') {
                        var find_pos = $('.quote1');
                    } else {
                        var find_pos = $(this_sss).parents('.quote2');
                    }
                    $(find_pos).find('.address').val(data[0].var_address);
                    $(find_pos).find('.rating_star_hotel').parent().find('.select2-chosen').html(data[0].var_category);
                    $(find_pos).find('.rating_star_hotel').parent().find("option[value='" + data[0].var_category + "']").attr('selected', true);
                    
                }
            })
        })
    }

    var rplyquote = function () {

        var countsend = 0;
        $('.sendquote').on('click', function () {
            countsend++;
            var flag2 = 1;
            $("input.requered_true").each(function () {

                var valuesite = $(this).val();

                if (valuesite == "")
                {
                    $(this).parent().addClass('has-error');
                    flag2 = 0;
                    countsend = 0;
                }
            });

            $("select.requered_true").each(function () {

                var valuesite = $(this).val();

                if (valuesite == "")
                {
                    $(this).parent().addClass('has-error');
                    flag2 = 0;
                    countsend = 0;
                }
            });

            $('.flag2').val(flag2);
            var form = $('#rplyquote');
            var error = $('.alert-danger', form);
            var success = $('.alert-success', form);
            success.hide();
            error.show();
            form.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",
                rules: {
                    'pricequote[]': {
                        required: true,
                        number: true
                    },
                    'hotel[]': {
                        required: true,
                    },
                    'policy[]': {
                        required: true,
                    },
                    'k_site[]': {
                        required: true,
                        number: true,
                    },
                    'w_site[]': {
                        required: true,
                    },
                    'room_type[]': {
                        required: true,
                    },
                    'src[]': {
                        required: true,
                    },
                    's_src[]': {
                        required: true,
                    },
                    'rating[]': {
                        required: true,
                    }
                },
                invalidHandler: function (event, validator) { //display error alert on form submit              
                    success.hide();
                    error.show();
                    countsend = 0;
                    App.scrollTo(error, -200);
                },
                errorPlacement: function (error, element) { // render error placement for each input type
                    var icon = $(element).parent('.input-icon').children('i');
                    icon.removeClass('fa-check').addClass("fa-warning");
                    $(element).closest('.valid_quot').removeClass('has-success').addClass('has-error');
                    icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});

                },
                highlight: function (element) { // hightlight error inputs
                    $(element)
                            .closest('.valid_quot').addClass('has-error'); // set error class to the control group
                },
                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                            .closest('.valid_quot').removeClass('has-error'); // set error class to the control group
                },
                success: function (label, element) {
                    var icon = $(element).parent('.input-icon').children('i');
                    $(element).closest('.valid_quot').removeClass('has-error').addClass('has-success'); // set success class to the control group
                    icon.removeClass("fa-warning").addClass("fa-check");
                },
                submitHandler: function (form) {
//                    console.log(countsend); return false;
                    if (countsend == 1) {
                        var url = baseurl + "admin/quoterequests/rplyquote";
                        var data = $(form).serialize();

                        flag2 = $('.flag2').val();

                        $(".ksite").each(function () {
                            var valuesite = $(this).val();
                            var valuesite1 = valuesite.replace(/\,/g, '');
                            // console.log(ss);
                            if (parseFloat(valuesite1) < 20.00)
                            {

                                flag2 = 0;
                            }

                        });
                        $('.flag2').val(flag2);

                        setTimeout(function () {
                            flag2 = $('.flag2').val();
                            if (flag2 == 1)
                            {
                                ajaxcall(url, data, function (output) {
                                    if (output == 'success') {
                                        Toastr.init('success', 'Welldone..', 'Quote added successfully..');
                                        setTimeout(function () {
                                            location.href = baseurl + "admin/quoterequests";
                                        }, 1000);
                                    }
                                    else if (output == 'user exits') {
                                        Toastr.init('warning', 'Warning..', 'Email Already Exits');
                                    }
                                    else if (output == 'error') {
                                        Toastr.init('error', 'Oops..', 'Quote not added...');
                                    }
                                });
                            }
                        }, 1000)
                    }
                }
            });
            $('.select2me', form).change(function () {
                form.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
            });
        });
    }
    
    var set = '0';
    
    $('body').on('change', '.src_price', function () {
        var value = $(this).val();
        var this_pos = $(this).attr('data-position');
        var this_sss = $(this);
        $.ajax({
            type: 'post',
            url: baseurl + 'admin/quoterequests/getprice',
            data: {srcvalue: value},
            success: function (output) {
                if (this_pos == '1') {
                    var find_pos = $('.quote1');
                } else {
                    var find_pos = $(this_sss).parents('.quote2');
                }
                var html = '<span style="">Minimum $' + output + '</span>';
                $(find_pos).find('.price_tooltips').append(html);
                var pricevalue = $(find_pos).find('.price').val().replace(/\,/g, '');

                if (parseFloat(pricevalue) < parseFloat(output)) {
                    $(find_pos).find('.price_tooltips').addClass('tooltips');
                    $(find_pos).find('.price_tooltips').addClass('has-error');

                } else {
                    $(find_pos).find('.price_tooltips').removeClass('tooltips');
                    $(find_pos).find('.price_tooltips').removeClass('has-error');
                }
            }
        })
        set = '0';
        $('.src_price').each(function () {
            var this_pos = $(this).attr('data-position');
            var this_sss = $(this);
            if (this_pos == '1') {
                var find_pos = $('.quote1');
            } else {
                var find_pos = $(this_sss).parents('.quote2');
            }

            var pricevalue = $(find_pos).find('.price').val().replace(/\,/g, '');
            var toatal = (parseFloat($(this).val().replace(/\,/g, '')) / 100 * 10);
            var this_val = toatal + parseFloat($(this).val().replace(/\,/g, ''));

            if (parseFloat(pricevalue) < parseFloat(this_val)) {

                set = '0';
                return false;
            } else {
                set = '1';
            }
        });

        if (set == '0') {
            $('.sendquote').attr('disabled', 'disabled');
        } else {
            $('.sendquote').removeAttr('disabled');
        }
    });

    $('body').on('change', '.price', function () {
        var pval = $(this).val();
        var pval1 = pval.replace(/\,/g, '');
        var srcval = $(this).parent().parent().parent().parent().next().next().find('.src_price').val();
        var dd = $(this);
        var set1 = '0';

        if (srcval != "")
        {
            $.ajax({
                type: 'post',
                url: baseurl + 'admin/quoterequests/getprice',
                data: {srcvalue: srcval},
                success: function (output) {
                    if (parseFloat(pval1) >= parseFloat(output)) {
                        dd.parent().parent().parent().parent().find('.price_tooltips span').css('display', 'none');
                        dd.parent().parent().parent().parent().find('.price_tooltips').removeClass('has-error');
                    } else {
                        dd.parent().parent().parent().parent().find('.price_tooltips').addClass('tooltips');
                        dd.parent().parent().parent().parent().find('.price_tooltips').addClass('has-error');
                        dd.parent().parent().parent().parent().find('.price_tooltips span').css('display', 'block');
                    }

                }
            });
        }


        $('.price').each(function () {
            var pval = $(this).val().replace(/\,/g, '');
            var srcval = $(this).parent().parent().parent().parent().next().next().find('.src_price').val().replace(/\,/g, '');

            if (pval != "" && srcval != "") {
                var toatal = (parseFloat(srcval.replace(/\,/g, '')) / 100 * 10);
                var this_val = toatal + parseFloat(srcval.replace(/\,/g, ''));
                if (parseFloat(pval) < parseFloat(this_val)) {
                    set1 = '0';
                    return false;
                } else {
                    set1 = '1';
                }
            }
        });

        if (set1 == '0') {
            $('.sendquote').attr('disabled', 'disabled');
        } else {
            $('.sendquote').removeAttr('disabled');
        }
    });

    $('body').on('change', 'input.requered_true', function () {
        var valuesite = $(this).val();
        if (valuesite != "")
        {
            $(this).parent().removeClass('has-error');
            $(this).parent().addClass('has-success');
        }
        else
        {
            $(this).parent().parent().addClass('has-error');
            $(this).parent().parent().removeClass('has-success');
        }
    });

    $('body').on('change', 'select.requered_true', function () {
        var valuesite = $(this).val();

        if (valuesite != "")
        {
            $(this).parent().removeClass('has-error');
            $(this).parent().addClass('has-success');
        }
        else
        {
            $(this).parent().addClass('has-error');
            $(this).parent().removeClass('has-success');
        }
    });

    $('body').on('change', '.commanPrice', function () {

        var this_pos = $(this).attr('data-position');
        var this_sss = $(this);

        if (this_pos == '1') {
            var find_position = $('.quote1');
        } else {
            var find_position = $(this_sss).parents('.quote2');
        }

        var site1 = $(find_position).find('.ksite').val();
        var site2 = $(find_position).find('.ssite').val();
        var srcvalue = $(find_position).find('.src_price').val();

        var site1 = site1.replace(/\,/g, '');
        var site2 = site2.replace(/\,/g, '');
        var srcvalue = srcvalue.replace(/\,/g, '');

        var accounttype = $(find_position).parent().parent().find('.parentaccount').val();

        if (accounttype == 'A') {
            var typevalue = 1.1;
        } else {
            var typevalue = 1.05;
        }

        if (site1 != '' && site2 != '') {
            if (Number(site1) < Number(site2)) {
                var site = site1;
            } else {
                var site = site2;
            }
        } else if (site1 != '' && site2 == '') {
            var site = site1;
        } else if (site1 == '' && site2 != '') {
            var site = site2;
        } else {
            var site = '';
        }
        if (site != '' && srcvalue != '') {
            var priceval = ((parseFloat(site) - parseFloat(srcvalue)) / 2);
            var totalamount = ((parseFloat(priceval) + parseFloat(srcvalue)) * parseFloat(typevalue));
            $(find_position).find('.price').val(totalamount.toFixed(2));
        }
    });

    var getquote = function () {

        $('body').on('change', '.ksite', function () {
            var valuesite = $(this).val();
            var valuesite1 = valuesite.replace(/\,/g, '');
            var siteval = $(this);

            if (parseFloat(valuesite1) >= '20.00') {
                siteval.parent().parent().parent().parent().find('.site1').addClass('has-success');
                siteval.parent().parent().parent().parent().find('.site1').removeClass('has-error');
            } else {
                siteval.parent().parent().parent().parent().find('.site1').addClass('has-error');
                siteval.parent().parent().parent().parent().find('.site1').removeClass('has-success');
            }
        });
    }
    
    $('#addquote').click(function () {
        countvalue++;
        var city = $('.city').val();
        var indate = $('.indate').val();
        var outcheck = $('.outcheck').val();
        var rooms = $('.rooms').val();
        var adults = $('.adults').val();
        var children = $('.children').val();
        var res_name = $('.res_name').val();
        var tabindex = $('.tabindex').val();

        $.ajax({
            type: 'post',
            url: baseurl + 'admin/quoterequests/frmaddquote',
            data: {count: countvalue, tabindex: tabindex, city: city, indate: indate, outcheck: outcheck, rooms: rooms, adults: adults, children: children, res_name: res_name},
            success: function (data1) {
                if (data1) {
                    $('.quote').append(data1);
                    $(".select_hotel").autocomplete({
                        source: hotel_array
                    });

                    $('.date-picker').datepicker();
                    $('.site-1' + countvalue + ' select').select2();
                    $('.site-2' + countvalue + ' select').select2();
                    $(".price").maskMoney({prefix: '$', allowNegative: true, thousands: ',', decimal: '.', affixesStay: false});
                    $(".ksite").maskMoney({prefix: '$', allowNegative: true, thousands: ',', decimal: '.', affixesStay: false});
                    $(".ssite").maskMoney({prefix: '$', allowNegative: true, thousands: ',', decimal: '.', affixesStay: false});
                    $(".ssrc").maskMoney({prefix: '$', allowNegative: true, thousands: ',', decimal: '.', affixesStay: false});
                    tabindex = parseInt(tabindex) + parseInt(12);
                    $('.tabindex').val(tabindex);
                    var tabindex1 = parseInt(tabindex) + parseInt(13);
                    var tabindex2 = parseInt(tabindex) + parseInt(14);
                    $('#addquote').attr('tabindex', tabindex1);
                    $('#btn').attr('tabindex', tabindex2);
                }
            }
        });

//        $('#minusquote').removeClass('hidden');

    });

    $('body').on('click', '.copyquote', function () {

        var that = $(this)
        var count = $(this).attr('data-id');

        if (count == 2) {
            var hotel = that.parent().parent().parent().find('.select_hotel').val();
            var hotelname = that.parent().parent().parent().find('.select_hotel_name').val();
            var rating = that.parent().parent().parent().find('.rating_star_hotel .select2-chosen').html();
            var hotel_comments = that.parent().parent().parent().find('.hotel_comments').val();
            var hotel_adviser = that.parent().parent().parent().find('.hotel_adviser').val();
            var hotel_cancelation = that.parent().parent().parent().find('.hotel_cancelation').val();
            var src = that.parent().parent().parent().find('.src').val();
            var f_site = that.parent().parent().parent().find('.f_site').val();
            var address = that.parent().parent().parent().find('.address').val();
        } else {
            var hotel = that.parent().prev().prev().find('.select_hotel').val();
            var hotelname = that.parent().prev().prev().find('.select_hotel_name').val();
            var rating = that.parent().prev().prev().find('.rating_star_hotel .select2-chosen').html();
            var hotel_comments = that.parent().prev().prev().find('.hotel_comments').val();
            var hotel_adviser = that.parent().prev().prev().find('.hotel_adviser').val();
            var hotel_cancelation = that.parent().prev().prev().find('.hotel_cancelation').val();
            var src = that.parent().prev().prev().find('.src').val();
            var f_site = that.parent().prev().prev().find('.f_site').val();
            var address = that.parent().prev().prev().find('.address').val();
        }

        that.parent('div').find('.select_hotel').val(hotel);
        that.parent('div').find('.site-1' + count + ' .select2-chosen').html(hotelname);
        that.parent('div').find('.site-2' + count + ' .select2-chosen').html(rating);
        that.parent('div').find('.site-1' + count + ' select').find("option[value='" + hotelname + "']").attr('selected', true);
        that.parent('div').find('.site-2' + count + ' select').find("option[value='" + rating + "']").attr('selected', true);
        that.parent('div').find('.select2-chosen').val(rating).attr('selected', true);
        that.parent('div').find('.hotel_comments').val(hotel_comments);
        that.parent('div').find('.hotel_adviser').val(hotel_adviser);
        that.parent('div').find('.hotel_cancelation').val(hotel_cancelation);
        that.parent('div').find('.src').val(src);
        that.parent('div').find('.f_site').val(f_site);
        that.parent('div').find('.address').val(address);

    })
        
    $('body').on('click','.minusquote',function(){
         var that = $(this);
         var count = $(this).attr('data-id');
         that.parent('div').html('');
         countvalue = count-1;
    })

    var handle_delete = function () {

        $('body').on('click', '.delete_id', function () {
            var id = $('.delete_id').attr('data-id');
            $('.pop_delete_id').attr('data-id', id);
        });
        $('body').on('click', '#delete_quets', function () {

            var pop_id = $(this).attr('data-id');
            var url = baseurl + "admin/quoterequests/delete";
            var data = {'id': pop_id};
            ajaxcall(url, data, function (output) {
                if (output == 'success') {
                    Toastr.init('success', 'Welldone..', 'Quote added successfully..');
                    $('#delete_modal').hide();
                    setTimeout(function () {
                        window.location.href = baseurl + "admin/quoterequests";
                    }, 500);
                }
                else {
                    Toastr.init('error', 'Oops..', 'Quote not added...');
                }
            });
        });
    }

    var tablemanaged = function () {
        $('#quoterequests').dataTable({
            "aLengthMenu": [
                [25, 50, 100, -1],
                [25, 50, 100, "All"] // change per page values here
            ],
            // set the initial value
            "iDisplayLength": 25,
            "sPaginationType": "bootstrap",
            "oLanguage": {
                "sLengthMenu": "_MENU_ records",
                "oPaginate": {
                    "sPrevious": "Prev",
                    "sNext": "Next"
                }
            },
            "aaSorting": [[1, "desc"]]
        });

        jQuery('#quoterequests .group-checkable').change(function () {
            var set = jQuery(this).attr("data-set");
            var checked = jQuery(this).is(":checked");
            jQuery(set).each(function () {
                if (checked) {
                    $(this).attr("checked", true);
                } else {
                    $(this).attr("checked", false);
                }
            });
            jQuery.uniform.update(set);
        });

        jQuery('#quoterequests_wrapper .dataTables_filter input').addClass("form-control input-large input-inline"); // modify table search input
        jQuery('#quoterequests_wrapper .dataTables_length select').addClass("form-control input-xsmall"); // modify table per page dropdown
        jQuery('#quoterequests_wrapper .dataTables_length select').select2(); // initialize select2 dropdown
    }
    
    return{
        init: function () {
            addquote();
            quote_fill_val();
            rplyquote();
            getquote();
            handle_delete();
            tablemanaged();
        },
    };
}();


$.ui.autocomplete.prototype._renderItem = function (ul, item) {
    item.label = item.label.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(this.term) + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "<span style='background-color:#ff9'>$1</span>");
    return $("<li></li>")
            .data("item.autocomplete", item)
            .append("<a>" + item.label + "</a>")
            .appendTo(ul);
};