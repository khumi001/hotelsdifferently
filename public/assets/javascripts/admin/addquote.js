var Addquote = function () {
   
    var Quote_info = function() {
//        alert('second')
            var form = $('#addquotes');
//            alert(form)
            var error1 = $('.alert-danger', form);
            var success1 = $('.alert-success', form);    
            form.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",    
            rules: {
                city: {
                     required:true,  
                
                },
                point: {
                    required: true,

                },
                checkin: {
                    required: true,
                },
                accountid: {
                    required: true,
                    
                },
                checkout: {
                    required: true,
                    
                },
                username: {
                  required:true,  
                
                },
                room: {
                    required: true,

                },
                requestdate: {
                    required: true,
                },
                adult: {
                    required: true,
                    
                },
                children: {
                    required: true,
                    
                },
                reservname: {
                    required: true,

                },
                hotelname: {
                    required: true,
                },
                stars: {
                    required: true,
                    
                },
                 CommentS: {
                    required: true,
                    
                },
              
            },
                invalidHandler: function (event, validator) { //display error alert on form submit              
                    success1.hide();
                    error1.show();
                    App.scrollTo(error1, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function (form) {
                    var url = baseurl+"admin/quoterequests/addquote";
                    var data =  $( form ).serialize() ;
                    
                    ajaxcall(url,data,function(output){
                        if(output == 'success')
                        {
                            Toastr.init('success','Weldone..','Profile Is Updated..');  
                        }
                        else if(output == 'error')
                        {
                            Toastr.init('error','Oops..','Profile is not Updated..');                       
                        }                        
                    });
                }
            });
            

    }
     return{
        init:function(){
            Quote_info();
        }
    };
    }();