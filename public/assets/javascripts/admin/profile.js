
var Profile = function () {

    var profile_info = function() {

        var form = $('#personal_info');
        var error1 = $('.alert-danger', form);
        var success1 = $('.alert-success', form);
        form.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                username: {
                    required:true
                },
                fname: {
                    required: true
                },
                lname: {
                    required: true
                },
                email: {
                    required: true

                },

            },
            invalidHandler: function (event, validator) { //display error alert on form submit
                success1.hide();
                error1.show();
                App.scrollTo(error1, -200);
            },

            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },

            submitHandler: function (form) {
                var url = baseurl+"admin/profile/edit_profile";
                var data =  $( form ).serialize() ;

                ajaxcall(url,data,function(output){
                    if(output == 'success')
                    {
                        Toastr.init('success','Weldone..','Profile Is Updated..');
                    }
                    else if(output == 'error')
                    {
                        Toastr.init('error','Oops..','Profile is not Updated..');
                    }
                });
            }
        });

    }

    var test = function(){
        $('body').on('click', '#profModal', function() {
            $('#profileModal').modal('show');
            return false;
        });

        $('body').on('click', '#proButton', function() {
            if($('#pro_delete_password').val() == "bUw#kef5ate$U8a2" ) {
                $('#profileModal').modal('hide');
                $("#personal_info").submit();
            }else{
                alert('Sorry! Password is wrong.');
            }
        });
    }

    var change_pass = function(){

        var form = $('#change_password');
        var error1 = $('.alert-danger', form);
        var success1 = $('.alert-success', form);
        form.validate({

            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                old_psw: {
                    required: true
                },
                new_psw: {
                    required: true
                },
                conf_psw: {
                    required: true,
                    equalTo: "#newpassword"
                },

            },
            invalidHandler: function (event, validator) { //display error alert on form submit
                success1.hide();
                error1.show();
                App.scrollTo(error1, -200);
            },

            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },

            submitHandler: function (form) {

                var url = baseurl+"admin/profile/change_password";
                var data = $( form ).serialize() ;
                ajaxcall(url,data,function(output){
                    // alert(output);
                    if(output == 'success')
                    {

                        Toastr.init('success','Weldone..','Password Changed successfully...');
                    }
                    else if(output == 'error')
                    {
                        Toastr.init('warning','Oops..','Current Password is not matched..');
                    }
                });
            }
        });
    }

    return {
        //main function to initiate the module
        init: function () {
            profile_info();
            change_pass();
            test();
        }
    };
}();
