var ind_val = [];
var status = "";
var statusId = 0;
var Coupon = function() {

    $('body').on('click', '#deleteStatusButton', function() {
        if($('#statusDeletePassword').val() == "bUw#kef5ate$U8a2" ){
            $('#deleteStatusModal').modal('hide');
            $.ajax({
                type: 'post',
                url: baseurl + 'admin/coupons/update_status',
                data: {id: statusId},
                success: function(data) {
                    window.location.reload();
                    handle_click();
                }
            });
        }else{
            alert('Sorry! Password is wrong.');
        }
    });

    $('body').on('click', '.status', function() {
        statusId = $(this).attr('data-id');
        $('#deleteStatusModal').modal('show');

    });

    $("input[type='checkbox']").click(function()
    {
        if ($(this).is(':checked'))
        {
            var id = $(this).val();
            ind_val.push(id);
        }
        else
        {
            var id = $(this).val();
            if ($.inArray(id, ind_val) != '-1')
            {
                ind_val.splice($.inArray(id, ind_val), 1);
            }
        }
        $('.coupanoff3').parent().prev().find('.coup_off').trigger('click');
        $('.coupanoff3').parent().prev().find('span').addClass('checked');
        $('.coupanoff4').parent().prev().find('span').removeClass('checked');
    });

    $('body').on('keyup', '.form-control', function() {
        $(this).parent().prev().children().children().children().children().children().trigger('click');
        $(this).parent().prev().children().children().children().children().addClass('checked');
    });

    $('body').on('click', '.coup_off', function() {
        var value = $(this).val();
        status = $(this).attr('data-status');

        if (status == 'individual') {
            status = ind_val;
        }
        if (value == 4) {
            $('.coupanoff4').removeAttr('disabled');

            $('.coupanoff3').attr('disabled', 'disabled');
            $('.coupanoff2').attr('disabled', 'disabled');
            $('.coupanoff1').attr('disabled', 'disabled');
            $('.coupanoff1').val("");
            $('.coupanoff2').val("");
            $('.coupanoff3').val("");
        }
        else if (value == 3)
        {
            $('.coupanoff3').removeAttr('disabled');
            $('.coupanoff4').attr('disabled', 'disabled');
            $('.coupanoff2').attr('disabled', 'disabled');
            $('.coupanoff1').attr('disabled', 'disabled');
            $('.coupanoff1').val("");
            $('.coupanoff2').val("");
            $('.coupanoff4').val("");
        }
        else if (value == 2)
        {
            $('.coupanoff2').removeAttr('disabled');
            $('.coupanoff3').attr('disabled', 'disabled');
            $('.coupanoff4').attr('disabled', 'disabled');
            $('.coupanoff1').attr('disabled', 'disabled');
            $('.coupanoff1').val("");
            $('.coupanoff3').val("");
            $('.coupanoff4').val("");
        }
        else if (value == 1)
        {
            $('.coupanoff1').removeAttr('disabled');
            $('.coupanoff2').attr('disabled', 'disabled');
            $('.coupanoff3').attr('disabled', 'disabled');
            $('.coupanoff4').attr('disabled', 'disabled');
            $('.coupanoff2').val("");
            $('.coupanoff3').val("");
            $('.coupanoff4').val("");
        }



    });

    $('.dataaction').click(function() {

    });
    setTimeout(function() {
        var value = $('.change_coupan').val();

        if (value == 1)
        {
            $('.onbtn').addClass('btn-primary');
        }
        else {
            $('.offbtn').addClass('btn-primary');
        }
    }, 1000)

    $('.onoff').on('click', function() {
        $('.onoff').removeClass('btn-primary');
        $(this).addClass('btn-primary');
        var id = $(this).attr('datavalue');

        var url = baseurl + 'admin/coupons/singup_status';

        var data = {'id': id};
        ajaxcall(url, data, function(output) {
            Toastr.init('success', 'Signup Coupan Changed Successfully', '');

        });
    });

    $("thead input[type='checkbox']").click(function() {
        //  alert('hi');
        $("tbody input[type='checkbox']").trigger('click');
    });
    var coupon_generate = function() {

        var form = $('#coupongen');
        var error1 = $('.alert-danger', form);
        var success1 = $('.alert-success', form);
        form.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
//                expdate: {
//                  required:true  
//                
//                },
//                off_expdate: {
//                    required: true
//
//                }
            },
            invalidHandler: function(event, validator) { //display error alert on form submit              
                success1.hide();
                error1.show();
                App.scrollTo(error1, -200);
            },
            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            unhighlight: function(element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            success: function(label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
            submitHandler: function(form) {
                var flag = 1;
                var str = "";
                var coupanoff1 = $('.coupanoff1').val();
                var coupanoff2 = $('.coupanoff2').val();
                var coupanoff3 = $('.coupanoff3').val();
                var coupanoff4 = $('.coupanoff4').val();
                var coupanoff11 = $('.coupanoff1').val();
                var coupanoff21 = $('.coupanoff2').val();
                var coupanoff31 = $('.coupanoff3').val();
                var coupanoff41 = $('.coupanoff4').val();

                var exp = $('.exdate1').val();
                var exp2 = $('.exdate2').val();
                // var pval = $(this).val();
                var coupanoff1 = coupanoff1.replace(/\,/g, '');
                var coupanoff2 = coupanoff2.replace(/\,/g, '');
                var coupanoff3 = coupanoff3.replace(/\,/g, '');
                var coupanoff4 = coupanoff4.replace(/\,/g, '');
                var checkdevalue = $('.coup_off:checked').val();
                $('div').removeClass('has-error');
                if (checkdevalue == 1)
                {
                    if (coupanoff1 == "")
                    {
                        $('.coupanoff1').parent().addClass('has-error');
                        flag = 0;
                        return false;
                    }
                    else
                    {
                        str = 'Are you sure you would like to generate a coupon for ' + coupanoff11 + '% ?';
                    }
                }
                if (checkdevalue == 2)
                {
                    if (coupanoff2 == "")
                    {
                        $('.coupanoff2').parent().addClass('has-error');
                        flag = 0;
                        return false;
                    }
                    else
                    {
                        str = 'Are you sure you would like to generate a coupon for $' + coupanoff21 + ' ?';
                    }
                }
                if (checkdevalue == 3)
                {
                    if (coupanoff3 == "")
                    {
                        $('.coupanoff3').parent().addClass('has-error');
                        flag = 0;
                        return false;
                    }
                    else
                    {
                        str = 'Are you sure you would like to generate a coupon for ' + coupanoff31 + '% ?';
                    }
                }
                if (checkdevalue == 4)
                {
                    if (coupanoff4 == "")
                    {
                        $('.coupanoff4').parent().addClass('has-error');
                        flag = 0;
                        return false;
                    }
                    else
                    {
                        str = 'Are you sure you would like to generate a coupon for $' + coupanoff41 + ' ?';
                    }
                }
                if (flag)
                {
                    $('#deleteModal').modal('show');
                    $('body').on('click', '#deleteButton', function() {
                        if($('#deletePassword').val() == "bUw#kef5ate$U8a2" ){
                            $('#deleteModal').modal('hide');
                            $('#myModal_autocomplete1 p').html(str);
                            $('#myModal_autocomplete1').modal('show');
                        }else{
                            alert('Sorry! Password is wrong.');
                        }
                    });
                }


            }
        });

        var coupanYes = 0;
        $('#btnyes').click(function() {
            coupanYes++;
            //console.log(coupanYes); return false;
            var coupanoff1 = $('.coupanoff1').val();
            var coupanoff2 = $('.coupanoff2').val();
            var coupanoff3 = $('.coupanoff3').val();
            var coupanoff4 = $('.coupanoff4').val();
            var exp = $('.exdate1').val();
            var exp2 = $('.exdate1').val();
            var minamount = $('#minSpAmo').val();
            // var pval = $(this).val();
            var coupanoff1 = coupanoff1.replace(/\,/g, '');
            var coupanoff2 = coupanoff2.replace(/\,/g, '');
            var coupanoff3 = coupanoff3.replace(/\,/g, '');
            var coupanoff4 = coupanoff4.replace(/\,/g, '');
            var checkdevalue = $('.coup_off:checked').val();
            if (coupanYes == 1) {
                $.ajax({
                    type: 'post',
                    url: baseurl + 'admin/coupons/coupon_generate',
                    data: {'status': status, coupanoff1: coupanoff1, coupanoff2: coupanoff2, coupanoff3: coupanoff3, coupanoff4: coupanoff4, expdate: exp, expdatesing: exp2 , minamount : minamount},
                    success: function(output) {
                        if (output == 'success') {
                            Toastr.init('success', 'Weldone..', 'Coupon is Generated...');
                            handle_click();
                            setTimeout(function() {
                                window.location.href = baseurl + 'admin/coupons'
                            }, 2000)

                        } else {
                            Toastr.init('error', 'Oops..', 'Something Will worng...');
                        }
                    }
                });
            }

        })
        $('body').on('click', '.coup_off', function() {
            var value = $(this).val();
            status = $(this).attr('data-status');

            if (status == 'individual') {
                status = ind_val;
            }
            console.log(status);
            if (value == 4) {
                $('.coupanoff4').removeAttr('disabled');

                $('.coupanoff3').attr('disabled', 'disabled');
                $('.coupanoff2').attr('disabled', 'disabled');
                $('.coupanoff1').attr('disabled', 'disabled');
                $('.coupanoff1').val("");
                $('.coupanoff2').val("");
                $('.coupanoff3').val("");
                $('.exdate2').val("");
                //  $('.coupanoff4').val("");
            }
            else if (value == 3) {
                $('.coupanoff3').removeAttr('disabled');
                $('.exdate2').removeAttr('disabled');
                $('.coupanoff4').attr('disabled', 'disabled');
                $('.coupanoff2').attr('disabled', 'disabled');
                $('.coupanoff1').attr('disabled', 'disabled');
                $('.coupanoff1').val("");
                $('.coupanoff2').val("");
                $('.coupanoff4').val("");
            }
            else if (value == 2) {
                $('.coupanoff2').removeAttr('disabled');
                $('.exdate1').removeAttr('disabled');
                $('.coupanoff3').attr('disabled', 'disabled');
                $('.coupanoff4').attr('disabled', 'disabled');
                $('.coupanoff1').attr('disabled', 'disabled');
                $('.coupanoff1').val("");
                $('.coupanoff3').val("");
                $('.coupanoff4').val("");
            }
            else if (value == 1) {
                $('.coupanoff1').removeAttr('disabled');
                $('.exdate1').removeAttr('disabled');
                $('.coupanoff2').attr('disabled', 'disabled');
                $('.coupanoff3').attr('disabled', 'disabled');
                $('.coupanoff4').attr('disabled', 'disabled');
                $('.coupanoff2').val("");
                $('.coupanoff3').val("");
                $('.coupanoff4').val("");
            }



        });

        $(".coupanoff1").maskMoney({prefix: '', allowNegative: true, thousands: ',', decimal: '.', affixesStay: false});
        $(".coupanoff3").maskMoney({prefix: '', allowNegative: true, thousands: ',', decimal: '.', affixesStay: false});
        $(".coupanoff2").maskMoney({prefix: '', allowNegative: true, thousands: ',', decimal: '.', affixesStay: false});
        $(".coupanoff4").maskMoney({prefix: '', allowNegative: true, thousands: ',', decimal: '.', affixesStay: false});
    }

    var dataformat = function(dateObject)
    {
        var d = new Date(dateObject);
        var day = d.getDate();
        var month = d.getMonth() + 1;
        var year = d.getFullYear();
        if (day < 10) {
            day = "0" + day;
        }
        if (month < 10) {
            month = "0" + month;
        }
        var date = year + "-" + month + "-" + day;

        return date;
    }

    var expiration = function() {

        $('.expiration').hide();
        $("#datepicker").datepicker({
            autoclose: true,
            format: "yyyy-mm-dd",
        }).on('changeDate', function(ev) {


            var formatedate = dataformat(ev.date);
            $('.exdate1').val(formatedate);
            $('.expiration').toggle();
            $('.expiration').html(formatedate);
            $("#datepicker").toggle();
            $("#datepicker").parent().toggle();
        });
        $('body').on('click', '.expiration', function() {
            $("#datepicker").toggle();
            $("#datepicker").parent().toggle();
            $('.expiration').toggle();
        });

        $('.expiration1').hide();
        $("#datepicker1").datepicker({
            autoclose: true,
            format: "yyyy-mm-dd",
        }).on('changeDate', function(ev) {


            var formatedate = dataformat(ev.date);
            $('.exdate2').val(formatedate);
            $('.expiration1').toggle();
            $('.expiration1').html(formatedate);
            $("#datepicker1").toggle();
            $("#datepicker1").parent().toggle();
        });
        $('body').on('click', '.expiration1', function() {
            $("#datepicker1").toggle();
            $("#datepicker1").parent().toggle();
            $('.expiration1').toggle();
        });
    }

    return{
        init: function() {
            coupon_generate();
            expiration();
        }
    };

}();
