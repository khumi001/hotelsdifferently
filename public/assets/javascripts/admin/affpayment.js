var Affpayment = function() {
//    alert('hello payment add');
    var payment = function() {
        
        $('.btnid').on("click",function(){
            var payid=$(this).attr('user-id');
            $('#payid').val(payid);
            
            var amount=$(this).attr('amount');
            $('#amount').val(amount);
            
             var charjbackid = $(this).attr('charjbackid');
            // alert(charjbackid);
            $('#charjbackid').val(charjbackid)
             var comssionid = $(this).attr('comssionid');
            $('#comssionid').val(comssionid)
        });
        
        var form = $('#payment');
        var error = $('.alert-danger', form);
        var success = $('.alert-success', form);
        success.hide();
        error.show();
        form.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                ptransaction: {
                    required: true,
                }
            },
            invalidHandler: function(event, validator) { //display error alert on form submit              
                success.hide();
                error.show();
                App.scrollTo(error, -200);
            },
            errorPlacement: function(error, element) { // render error placement for each input type
                var icon = $(element).parent('.input-icon').children('i');
                icon.removeClass('fa-check').addClass("fa-warning");
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error'); // set success class to the control group                    
                icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
            },
            highlight: function(element) { // hightlight error inputs
                $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            unhighlight: function(element) { // revert the change done by hightlight
                $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            success: function(label, element) {
                var icon = $(element).parent('.input-icon').children('i');
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                icon.removeClass("fa-warning").addClass("fa-check");
            },
            submitHandler: function(form) {
                var formdata = $(form).serialize();
//                $('body').on('click', '.btnsubmit', function() {
//                    var id = $(this).attr('data-id');
                    
                    $.ajax({
                        type: 'post',
                        url: baseurl + 'admin/affipayment/add_payment',
                        data: formdata,
                        success: function(data) {
                            if (data == 'success') {
                                Toastr.init('success', 'Welldone..', 'Afflient payment added successfully..');
                                $('#mymodal_payment').modal('hide');
                                handle_click();
                                window.location.href = baseurl+"admin/affipayment";
                            }
                            else {
                                Toastr.init('error', 'Oops..', 'Sumthing Will Worng...');
                            }
                        }
                    });
            }
        });
        //apply validation on select2 dropdown value change, this only needed for chosen dropdown integration.
        $('.select2me', form).change(function() {
            form.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
        });
        //});
    }
    return{
        init: function() {
            payment();
        }
    };
}();


