var Saving = function() {

    var getQuets = function() {

        var form = $('#quetsId');
        var error = $('.alert-danger', form);
        var success = $('.alert-success', form);
        success.hide();
        error.show();
        $(form).validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                promocode: {
                    required: true,
                },
                amount: {
                    required: true,
                },
            },
            invalidHandler: function(event, validator) { //display error alert on form submit              
                success.hide();
                error.show();
                //App.scrollTo(error, -200);
            },
            errorPlacement: function(error, element) { // render error placement for each input type
              //  var icon = $(element).parent('.input-icon').children('i');
              //  icon.removeClass('fa-check').addClass("fa-warning");
               // $(element).closest('.form-group').removeClass('has-success').addClass('has-error'); // set success class to the control group                    
               // icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
            },
            highlight: function(element) { // hightlight error inputs
                $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            unhighlight: function(element) { // revert the change done by hightlight
                $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            success: function(label, element) {
                var icon = $(element).parent('.input-icon').children('i');
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                icon.removeClass("fa-warning").addClass("fa-check");
            },
            submitHandler: function(form) {

                var formdata = $(form).serialize();
                $.ajax({
                    type: 'post',
                    url: baseurl + 'sss',
                    data: formdata,
                    success: function(output) {

                        if (output == "sucess") {
                            $(form)[0].reset();
                            Toastr.init('success', 'get Quets Add successfully', '');
                           
                        }
                        else {
                            Toastr.init('error', 'Oops ....', 'Something Will be Worng');
                        }
                    }
                });
                return false;
            }
            
        });
        return false;
    }

    var get_saving = function()
    {
        $('#quote').change(function() {
            var id = $('#quote').val();
            var lengthid=id.length;
            if(lengthid>3){
               $.ajax({
                type: 'post',
                url: baseurl + 'admin/manualentry/searchQuets',
                data: {id: id},
                success: function(data) {
                    var output = JSON.parse(data);
                   // console.log(output.int_glcode);
                    $('#quetesUser').html(output.username);
                    $('#quetsfk').html(output.quote);
                    $('#high_price').html(output.price);
                    $('#saverp').html(output.save);
                    $('#fk_id').val(output.fkid);
                }
            }); 
            }
            
        });
        
        $('body').on('click','#quets_ok',function(){
           var promocode = $('#promocode').val();
           var amount = $('#amount').val();
           var minspend = $('#minspend').val();
           
           if(amount != "" && promocode != "" && minspend != "")
           {
              $.ajax({
                        type: 'post',
                        url: baseurl + 'admin/manualentry/addpromocode',
                        data: {promocode:promocode,amount:amount , minspend : minspend},
                        success: function(data) {
                         if(data == 'success'){
                             Toastr.init('success', 'Saveing database add successfully', '');
                             $('#quote').val("");
                             setTimeout(function(){
                                 window.location.href = baseurl+"admin/manualentry";
                             },1000)
                             
                         }else{
                              Toastr.init('error', 'Oops ....', 'Something Will be worng');
                         }   
                      }
                }); 
           }
           else
		   {
                Toastr.init('error', 'Oops ....', 'Something Will be worng');
           }
            

    });
    
    }
    
     var general = function(){
        $('.deletepromocode').on('click',function(){
           var id= $(this).attr('data-id'); 
          $('#btndelete').attr('data-id',id);
        });
        
        $('#btndelete').on('click',function(){
           var id = $(this).attr('data-id');
           $.ajax({
              type:'post',
              data:{'id':id},
              url : baseurl + 'admin/manualentry/delete_promo',
              success : function(output){
                if (output == 'success') {
                    $('#delete-model').modal('hide');
                    Toastr.init('success', 'Promocode Delete successfully', '');
                    setTimeout(function () {
                        window.location.href = baseurl + "admin/manualentry";
                    }, 1000)
                  } else {
                    Toastr.init('error', 'Oops ....', 'Something Will be worng');
                 }   
              }
           });
        });
    }
    return {
        init: function() {
            get_saving();
            general();
        },
    };

}();
$("#minspend").maskMoney({prefix: '', allowNegative: true, thousands: ',', decimal: '.', affixesStay: false});









