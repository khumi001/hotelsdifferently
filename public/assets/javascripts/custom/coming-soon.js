var ComingSoon = function () {

    return {
        //main function to initiate the module
        init: function () {

            $.backstretch([
              baseurl+"public/assets/img/bg/1.jpg",
             // baseurl+"public/assets/img/bg/2.jpg",
             // baseurl+"public/assets/img/bg/3.jpg",
              //baseurl+"public/assets/img/bg/4.jpg"
              ], {
                fade: 1000,
                duration: 10000
          });

            var austDay = new Date();
            $('#defaultCountdown').countdown({until: austDay});
            $('#year').text(austDay.getFullYear());
            setInterval(function(){
             //   alert(userid);
               $.ajax({
                            type: 'post',
                            url: baseurl+"admin/account/timecall/"+userid,
                            data: {'datime':austDay},
                            success: function(output) {
                              //  console.log(output);
                               var obj = $.parseJSON(output);
                               $('.day').html(obj.days);
                               $('.hours').html(obj.houres);
                                $('.minutes').html(obj.minutes);
                                 $('.second').html(obj.seconds);
                                }
                            });
            },1000);
        }

    };

}();