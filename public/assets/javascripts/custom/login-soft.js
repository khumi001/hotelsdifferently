var Login = function () {

	var handleLogin = function() {
		$('.login-form').validate({
	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            rules: {
	                username: {
	                    required: true,
//                            email:true
	                },
	                password: {
	                    required: true
	                },
	                remember: {
	                    required: false
	                },
                       
	            },

	            messages: {
	                username: {
	                    required: "Username is required."
	                },
	                password: {
	                    required: "Password is required."
	                }
                        
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit   
                        $('.alert-danger').children('span').html('Enter any username and password.');
	                $('.alert-danger', $('.login-form')).show();
	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.form-group').addClass('has-error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.form-group').removeClass('has-error');
	                label.remove();
	            },

	            errorPlacement: function (error, element) {
	                error.insertAfter(element.closest('.input-icon'));
	            },

	            submitHandler: function (form) {
                        //alert('hii');
                       var username=$("#user").val();
                       var password=$("#pass").val();
                       var remeber = $("#remember").val();
                       
                       $.ajax({
                        type: "POST",
                        url: baseurl+"admin/account/login",
                        data: {"username": username, "password": password,"remeber" : remeber},

                        success: function(response)
						{
                            //alert(response)
                            if(response == 'error')
                            {
                                $('.alert-danger').children('span').html('Please Enter Valid Email add or Password..');
                                $('.alert-danger', $('.login-form')).show();
                                $('#user').parent().parent().addClass('has-error');
                                $('#pass').parent().parent().addClass('has-error');
                               
                            }
                            else if(response == 'last_error')
                            {
								location.href = "/";
                                /*$('.alert-danger').children('span').html('You are banned for 6 houres.');
                                $('.alert-danger', $('.login-form')).show();
                                $('#user').parent().parent().addClass('has-error');
                                $('#pass').parent().parent().addClass('has-error');*/
                               
                            }
                            else if(response == 'baned')
                            {
                                $('.alert-danger').children('span').html('You are banned for 6 houres.');
                                $('.alert-danger', $('.login-form')).show();
                                $('#user').parent().parent().addClass('has-error');
                                $('#pass').parent().parent().addClass('has-error');
                               
                            }
							else if(response == 'authy_error')
							{
								$('.alert-danger').children('span').html('Authy Error!');
                                $('.alert-danger', $('.login-form')).show();
                                $('#user').parent().parent().addClass('has-error');
                                $('#pass').parent().parent().addClass('has-error');
							}
                            else if($.isNumeric(response))
                            {
                                //$('.alert-danger').children('span').html('Email address and/or Password does not match.You have '+response+' attamp left.');
								//$('.alert-danger', $('.login-form')).show();
								$('#user').parent().parent().addClass('has-error');
                                $('#pass').parent().parent().addClass('has-error');
                            }
							else if(response == 'notexist')
							{
								$('#user').parent().parent().addClass('has-error');
                                $('#pass').parent().parent().addClass('has-error');
							}
                            else
                            {
                                location.href = baseurl+response;
                            }
						}
                        });          
	            }
	        });

	        $('.login-form input').keypress(function (e) {
	            if (e.which == 13) {
	                if ($('.login-form').validate().form()) {
	                    $('.login-form').submit();
	                }
	                return false;
	            }
	        });
	}

	var handleForgetPassword = function () {
		$('.forget-form').validate({
	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",
	            rules: {
	                email: {
	                    required: true,
	                    email: true
	                }
	            },

	            messages: {
	                email: {
	                    required: "Email is required."
	                }
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit   

	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.form-group').addClass('has-error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.form-group').removeClass('has-error');
	                label.remove();
	            },

	            errorPlacement: function (error, element) {
	                error.insertAfter(element.closest('.input-icon'));
	            },

	            submitHandler: function (form) {
	               var forgetemail =$('#forgetemail').val();
                        $.ajax({
                        type: "POST",
                        url: baseurl+"admin/account/forgot_password",
                        data: {"f_pass": "T","forgetemail": forgetemail},

                        success: function(response){
                            location.href = baseurl+'admin/login';
                         }           
                        }); 
	            }
	        });

	        $('.forget-form input').keypress(function (e) {
	            if (e.which == 13) {
	                if ($('.forget-form').validate().form()) {
	                    $('.forget-form').submit();
	                }
	                return false;
	            }
	        });

	        jQuery('#forget-password').click(function () {
	            jQuery('.login-form').hide();
	            jQuery('.forget-form').show();
	        });

	        jQuery('#back-btn').click(function () {
	            jQuery('.login-form').show();
	            jQuery('.forget-form').hide();
	        });

	}

	var handleRegister = function () {
               
		function format(state) {
            if (!state.id) return state.text; // optgroup
            return "<img class='flag' src='http://localhost/mobex/public/assets/img/flags/" + state.id.toLowerCase() + ".png'/>&nbsp;&nbsp;" + state.text;
        }



//         $('.register-form').validate({
//	            errorElement: 'span', //default input error message container
//	            errorClass: 'help-block', // default input error message class
//	            focusInvalid: false, // do not focus the last invalid input
//	            ignore: "",
//	            rules: {
//	                
//                        compnay: {
//	                    required: true
//	                },
//                        bussiness: {
//	                    required: true
//	                },
//                        employess: {
//	                    required: true,
//                             number: true,
//	                },
//                        state: {
//	                    required: true
//	                },
//                        zip: {
//	                    required: true,
//                             number: true,
//	                },
//                        firstname: {
//	                    required: true
//	                },
//                        lastname:{
//	                    required: true
//	                },
//                        phone:{
//	                    required: true,
//                            number: true,
//                           
//	                },
//                        ext:{
//	                    required: true
//	                },
//                        cellphone:{
//	                    required: true,
//                            number: true,
//	                },
//                       date:{
//	                    required: true,
//                            date: true,
//	                },
//	                email: {
//	                    required: true,
//	                    email: true
//	                },
//	                address: {
//	                    required: true
//	                },
//                        companyname: {
//	                    required: true
//	                },
//	                city: {
//	                    required: true
//	                },
//	                country: {
//	                    required: true
//	                },
//                        usertype :{
//                            requierd:true
//                        },
//	                username: {
//	                    required: true,
//                            email: true
//	                },
//	                password: {
//	                    required: true,
//	                },
//	                rpassword: {
//	                    equalTo: "#register_password"
//	                },
//
//	                tnc: {
//	                    required: true
//	                }
//	            },
//
//	            messages: { // custom messages for radio buttons and checkboxes
//	                tnc: {
//	                    required: "Please accept TNC first."
//	                }
//	            },
//
//	            invalidHandler: function (event, validator) { //display error alert on form submit   
//
//	            },
//
//	            highlight: function (element) { // hightlight error inputs
//	                $(element)
//	                    .closest('.form-group').addClass('has-error'); // set error class to the control group
//	            },
//
//	            success: function (label) {
//	                label.closest('.form-group').removeClass('has-error');
//	                label.remove();
//	            },
//
//	            errorPlacement: function (error, element) {
//	                if (element.attr("name") == "tnc") { // insert checkbox errors after the container                  
//	                    error.insertAfter($('#register_tnc_error'));
//	                } else if (element.closest('.input-icon').size() === 1) {
//	                    error.insertAfter(element.closest('.input-icon'));
//	                } else {
//	                	error.insertAfter(element);
//	                }
//	            },
//
//	            submitHandler: function (form) {
//                            $.ajax({
//                             type: "post",
//                             url: baseurl+"admin/account/sign_up",
//                             data: ('#ragistration').serialize(),
//                             success: function(response){
//                                //alert(data);        
//                                 if(response=="success")
//                                {
//                                    Toastr.init('success','Weldone..','Registration Is Successfully..');  
//                                }
//                                else if(response == 'error')
//                                {
//                                    Toastr.init('error','Oops..','Registration is not Successfully..');                       
//                                }  
//                                else if(response == 'failure')
//                                {
//                                    Toastr.init('error','Oops..','Email already exist');   
//                                } 
//                              }           
//                             }); 
//	            }
//	        });
                    $('.register-form').validate({
	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",
	            rules: {
	                
                        compnay : {
	                    required: true
	                },
	                useremail: {
	                    required: true,
	                    email: true
	                },
	                password: {
	                    required: true
	                },
	                rpassword: {
	                    equalTo: "#register_password"
	                },
	                firstname: {
	                    required: true
	                },

	                lastname: {
	                    required: true
	                },
	                companyname: {
	                    required: true
	                },
	                address: {
	                    required: true
	                },

	                city: {
	                    required: true
	                }
	            },

	            messages: { // custom messages for radio buttons and checkboxes
	                tnc: {
	                    required: "Please accept TNC first."
	                }
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit   

	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.form-group').addClass('has-error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.form-group').removeClass('has-error');
	                label.remove();
	            },

	            errorPlacement: function (error, element) {
	                if (element.attr("name") == "tnc") { // insert checkbox errors after the container                  
	                    error.insertAfter($('#register_tnc_error'));
	                } else if (element.closest('.input-icon').size() === 1) {
	                    error.insertAfter(element.closest('.input-icon'));
	                } else {
	                	error.insertAfter(element);
	                }
	            },

	            submitHandler: function () {
                       var data_value =  $('.register-form').serialize();
	               $.ajax({
                        type: "POST",
                        url: baseurl+"admin/account/sign_up",
                        data: data_value,
                        success: function(response){//alert(response)
                           if(response =='success'){
                                Toastr.init('success','Weldone!','Register succsessfully !!');
                                setTimeout(function(){
                                    location.reload();
                                },1000);
                           }else if(response =='user exits')
                                Toastr.init('error','Opps..','Email Already Exits');
                                
                            else{
                                Toastr.init('warning','Opps..','Registeration Error');
                           }
                         }           
                        });
	            }
	        });

			$('.register-form input').keypress(function (e) {
	            if (e.which == 13) {
	                if ($('.register-form').validate().form()) {
	                    $('.register-form').submit();
	                }
	                return false;
	            }
	        });

	        jQuery('#register-btn').click(function () {
	            jQuery('.login-form').hide();
	            jQuery('.register-form').show();
	        });

	        jQuery('#register-back-btn').click(function () {
	            jQuery('.login-form').show();
	            jQuery('.register-form').hide();
	        });
	}
    
    return {
        //main function to initiate the module
        init: function () {
        	
            handleLogin();
            handleForgetPassword();
            handleRegister();
        }

    };

}();