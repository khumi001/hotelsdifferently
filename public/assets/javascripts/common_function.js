function ajaxcall(url, data, callback)
{
    App.startPageLoading();
    $.ajax({
        type: 'POST',
        url: url,
        data: data,
        success: function(result) {
            App.stopPageLoading();
            callback(result);
        }
    })
}
function gritter(title, text, sticky, time)
{
    $.gritter.add({
        // (string | mandatory) the heading of the notification
        title: title,
        // (string | mandatory) the text inside the notification
        text: text,
        // (string | optional) the image to display on the left
//                    image1: './assets/img/avatar1.jpg',
        // (bool | optional) if you want it to fade out on its own or just sit there
        sticky: sticky,
        // (int | optional) the time you want it to be alive for before fading out
        time: time,
        // (string | optional) the class name you want to apply to that specific message
        class_name: 'my-sticky-class'
    });

}

function CKupdate() {
    for (instance in CKEDITOR.instances)
        CKEDITOR.instances[instance].updateElement();
}
var Toastr = function() {

    return {
        //main function to initiate the module
        init: function(type, title, message) {
            message = "";
            if(type == 'success')
            {
                title = 'SUCCESS!';
            }
            else
            {
                title = 'ERROR!';
            }
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "positionClass": "toast-top-right",
                "onclick": null,
                "showDuration": "1000",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
            toastr[type](message, title);
        },
        clear: function() {
            toastr.clear();
        }

    };

}();


var delete_data = function() {

    var delete_select = function(tb_name, url, success)
    {
        var checked_length = $(".checkboxes:checked").length;
        var id_array = new Array();

        $('.checkboxes:checked').each(function() {
            id_array.push($(this).attr('value'));
        });
        if (checked_length != 0)
        {
            data = {"tb_name": tb_name, "id_array": id_array};
            ajaxcall(url, data, function(output) {
                if (output == 'success')
                {
                    $('.checkboxes:checked').each(function() {
                        $(this).parents('tr').hide();
                        $(this).prop('checked', false);
                    });
                    Toastr.init('success', 'Weldone..!!', success);
                    $('.close').trigger('click');
                }
                else if (output == 'error')
                {
                    Toastr.init('error', 'oops..!!', 'Selected Item Do not deleted');
                }
                else
                {
                    Toastr.init('error', 'oops..!!', 'Something will be wrong ');
                }
            });
        }
        else
        {
            Toastr.init('warning', 'Opps..', 'Please Select Atleast One Checkbox');
            $('.close').trigger('click');
//            gritter('Opps..','Please Select Atleast One Checkbox',false,'1000');
        }

    }
    return {
        init: function(tb_name, url, success) {
            delete_select(tb_name, url, success);
        }
    }

}();


var check_checkbox = function() {

    return {
        init: function() {
            var checked_length = $(".checkboxes:checked").length;
            var id_array = new Array();

            $('.checkboxes:checked').each(function() {
                id_array.push($(this).attr('value'));
            });
            if (checked_length != 0)
            {
                return id_array;
            }
            else
            {
                Toastr.init('warning', 'Opps..', 'Please Select Atleast One Checkbox');
                return false;
            }
        }
    }

}();


if ("pushState" in history) {
//    $('body').on('click', 'a', function() {
//        var refresh_check = $(this).attr('refresh');
//        var href = $(this).attr('href');
//        var target = $(this).attr('target');
//        var datarel = $(this).attr('data-rel');
//        if(href == 'javascript:;' || href == '#'){
//            return false;
//        }
//        if (refresh_check == '1') {
//            if (href != undefined) {
//                location.href = href;
//            }
//            return false;
//        }
//        if($(this).hasClass('leave-page')){
//            $('input[name=beforeunload]').val('0');
//        }
//        var validate = $('input[name=beforeunload]').val();
//        if (typeof validate !== 'undefined' && validate == '1' && target != '_blank' && datarel != 'fancybox-button') {
//            $("#confirm_leave_page_modal").modal('show');
//            $('.leave-page').attr('href',href);
//            return false;
//        }
//        
//        if (href != '' && href != undefined) {
//            if (href.search('#') == '-1' && href != undefined && href != 'javascript:;' && target != '_blank' && datarel != 'fancybox-button') {
//                window.history.pushState({}, '', href);
//                ajax_load(href);
//                return false;
//            }
//        }
//        
//    });

    function url_infor(href, split_from, capitize) {
        if (href) {
            var url_parts = href.split(split_from + '/');
            if (capitize == '1') {
                var url = url_parts[1].charAt(0).toUpperCase() + url_parts[1].slice(1);
                return url;
            } else {
                var url = url_parts[1];
                return url;
            }
        }
    }
    function update_sidebar(element) {
        var trgt = window.location.href;
//                 //update the sidebar                
        $('.page-sidebar-menu li a').each(function() {
            if ($(this).attr('href').search(trgt) != '-1') {
                if ($(this).attr('href') == trgt)
                {
                    $('.page-sidebar-menu li a').parent().removeClass('active');
                    $('.page-sidebar-menu li a').parent().removeClass('start');
                    $(this).parent().addClass('active');

                    if ($(this).parent('li').parent('ul').parent('li').hasClass('open')) {
                        $(this).parent('li').parent('ul').parent('li').addClass('active')
                    }
                }

            }
        });
    }
    function ajax_load(href) {
        update_sidebar();
        App.startPageLoading();
        $.get(href, function(output) {
            //alert(output);
            var page_title = $(output).filter('title').text();
            var page_content = $(output).find('#result_container').html();
            document.title = page_title;
            $("#result_container").html(page_content);
            App.initAjax();

//            var controller = url_infor(href, 'admin', '0');
//            var js = url_infor(href, 'admin', '1');
//                    $.getScript(baseurl + 'public/assets/javascripts/admin/' + controller + '.js', function() {
//
//                    setTimeout(function() {
            var header_css = $(output).filter('#header-css')
            var footer_jsplugin = $(output).filter('#footer-jsplugin');
            var footer_js = $(output).filter('#footer-js');
            var footer_init = $(output).filter('#footer-init');
            $('#header-css').html(header_css);
            $('#footer-jsplugin').html(footer_jsplugin);
            $('#footer-js').html(footer_js);
            setTimeout(function() {
                $('#footer-init').html(footer_init);
                App.stopPageLoading();
            }, 500);
           

//                        App.init();
//                        if(js == 'Payment'){
//                           Payment.list_init();
//                        }else if(js == 'Order'){
//                           Order.list_init(); 
//                        }else if(js == 'Clients'){
//                            Clients.list_init();
//                        }else if(js == 'Profile'){
//                            Profile.init();
//                        }else if(js == 'Product'){
//                            Product.list_init();
//                        }else if(js == 'Packages'){
//                            Packages.list_init();
//                        }
//                    }, 500);
//                });  
        });
      //   App.stopPageLoading();
      
    }
    $(window).bind('popstate', function() {
        var trgt = window.location.href;
        ajax_load(trgt);
    });
}

var handle_url = function() {


    return {
        init: function(url) {
            $('.page-sidebar-menu li a').each(function() {
                if ($(this).attr('href').search(url) != '-1') {
                    $(this).trigger('click');
                }
            });
        }
    }

}();   

var handle_click = function(){
    if($('.page-sidebar-menu').children('li.active').hasClass('open')){
        $('.page-sidebar-menu').children('li.active').children('ul').children('li.active').trigger('click');
    }else{
        $('.page-sidebar-menu').children('li.active').children('a').trigger('click');                                
    }
}