var firstColumnChangeRoom = "md-4";
var secondColumnChangeRoom = "md-4";
var thirdColumnChangeRoom = "md-12";
var clearFixDivStartChangeRoom = "";
var clearFixDivEndChangeRoom = "";
$(document).ready(
    function()
    {
        jQuery(".rooms").change(
            function()
            {
                //debugger;
                var hotelRooms = $('.rooms :selected').val();
                var hotelChildren = $('.numberOfChild :selected').val();
				for(i=1; i < 9; i++)
                {
					var rID = '#room'+i;
					$(rID).html('');
				}
				i = 0;
                var rooms_html = "";
                for(i=0; i<hotelRooms; i++)
                {
                    thirdColumnChangeRoom = 'sm-4';
                    firstColumnChangeRoom = 'sm-3';
                    secondColumnChangeRoom = 'sm-4';
                    clearFixDivStartChangeRoom = '<div class="clearfix">';
                    clearFixDivEndChangeRoom = '</div>';

                    room_index = i + 1;
                    //rooms_html +=     '<div class="col-md-4">'  +
                    rooms_html += clearFixDivStartChangeRoom;
                    rooms_html +=     '<div class="col-' + firstColumnChangeRoom +'" style="margin-right:20px;">'  +
                    '<p style="margin: 36px 0px 0px 0px;text-align: right">Room' + room_index + '</p>' +
                    '</div>' +
                    //'<div class="col-md-4">' +
                    '<div class="col-' + secondColumnChangeRoom + '">' +
                    '<div class="form-group">' +
                    //'<label class="control-label col-md-12">Adults</label>' +
                    '<label class="control-label col-' + thirdColumnChangeRoom + '">Adults</label>' +
                    //'<div class="">' +
                    '<div class="' + thirdColumnChangeRoom + '">' +
                    '<select class="select2me form-control input-xlarge rooms" name="numberOfAdults[]" id="numberOfAdults" style="text-align: center;">';

                    for(room_index=0; room_index<=6; room_index++)
                    {
                        if(room_index == 2)
                        {
                            rooms_html += "<option value='" + room_index + "' selected>" + room_index + "</option>";
                        }
                        else
                        {
                            rooms_html += "<option value='" + room_index + "'>" + room_index + "</option>";
                        }

                    }

                    rooms_html += '</select>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '<div class="col-sm-4">' +
                    '<div class="form-group">' +
                    '<label class="control-label col-sm-12">Children</label>' +
                    '<div class="">' +
                    '<select class="select2me form-control input-xlarge numberOfChild" name="numberOfChild[]" id="numberOfChild" style="text-align: center;">';


                    for(room_index=0; room_index<=3; room_index++)
                    {
                        for_room = i + 1;
                        rooms_html += "<option for_room='" + for_room + "' value='" + room_index + "'>" + room_index + "</option>";
                    }

                    rooms_html += '</select>' +
                    '</div>' +
                    '</div>' +
                    '</div>';
                    rooms_html += clearFixDivEndChangeRoom;
                }
                $("#hotel_rooms_id").html(rooms_html);

                /* debugger;

                 childRow = "";
                 for(i=1; i<=hotelRooms; i++)
                 {
                 childRow +='<div class="clearfix">' +
                 '<div class="col-sm-3">' +
                 '<p style="margin: 36px 0px 0px 0px;text-align: right;">Room' + i + ':</p>' +
                 '</div>';

                 for(j=1; j<=hotelChildren; j++)
                 {
                 childRow +=  '<div class="col-sm-3">' +
                 '<div class="form-group">' +
                 '<label class="control-label col-sm-12">Child' + j + '</label>' +
                 '<div class="col-sm-12">' +
                 '<select class="select2me form-control input-xlarge rooms" name="numberOfAdults" id="numberOfAdults" style="text-align: center;">'+
                 getChildAgesOptions(20) +
                 '</select>' +
                 '</div>' +
                 '</div>' +
                 '</div>';

                 }
                 childRow += '</div>';
                 }

                 $("#childRows").html(childRow);

                 console.log(childRow);

                 //alert($(this).val());
                 //alert($('.hotelRooms :selected').val());
                 //alert($('.hotelChildren :selected').val());
                 */
            }
        )

        function getChildAgesOptions(maxAge)
        {
            var optionHtml = "<option value='1'>1</option>";

            for(k=2; k<maxAge; k++)
            {
                optionHtml += "<option value='" + k + "'>" + k + "</option>";
            }

            return optionHtml;
        }
    }
);
