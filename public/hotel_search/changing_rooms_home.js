$(document).ready(function () {
    jQuery(".rooms").change(function () {
        var hotelRooms = $('.rooms :selected').val();
        var hotelChildren = $('.numberOfChild :selected').val();
        for (i = 1; i < 9; i++) {
            var rID = '#room' + i;
            $(rID).html('');
        }
        i = 0;
        var rooms_html = "";
        var col_offset = "col-sm-offset-4";
        for (i = 0; i < hotelRooms; i++) {
            room_index = i + 1;
            rooms_html += '<div class="clearfix">';
            if (i > 0) {
                rooms_html += '<div class="col-sm-2  col-sm-offset-4">';
            }
            else {
                rooms_html += '<div class="col-sm-2">';
            }
            rooms_html += '<p style="margin: 36px 0px 0px 0px;text-align: right;">Room ' + room_index + ':</p>' + '</div>' + '<div class="col-sm-3">' + '<div class="form-group">' + '<label class="control-label col-sm-12">Adults</label>' + '<div class="col-sm-12">' + '<select class="select2me form-control input-xlarge numberOfAdults" name="numberOfAdults[]" id="numberOfAdults" style="text-align: center;">';
            for (room_index = 0; room_index <= 6; room_index++) {
                if (room_index == 2) {
                    rooms_html += "<option value='" + room_index + "' selected>" + room_index + "</option>";
                }
                else {
                    rooms_html += "<option value='" + room_index + "'>" + room_index + "</option>";
                }
            }
            rooms_html += '</select>' + '</div>' + '</div>' + '</div>' + '<div class="col-sm-3">' + '<div class="form-group">' + '<label class="control-label col-md-12">Children</label>' + '<div class="col-md-12">' + '<select class="select2me form-control input-xlarge numberOfChild" name="numberOfChild[]" id="numberOfChild" style="text-align: center;">';
            for (room_index = 0; room_index <= 3; room_index++) {
                for_room = i + 1;
                rooms_html += "<option for_room='" + for_room + "' value='" + room_index + "'>" + room_index + "</option>";
            }
            rooms_html += '</select>' + '</div>' + '</div>' + '</div>' + '</div>';
        }
        $("#hotel_rooms_id").html(rooms_html);
    })
    function getChildAgesOptions(maxAge) {
        var optionHtml = "<option value='1'>1</option>";
        for (k = 2; k < maxAge; k++) {
            optionHtml += "<option value='" + k + "'>" + k + "</option>";
        }
        return optionHtml;
    }
});