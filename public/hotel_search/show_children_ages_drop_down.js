var divRoomLabel = 2;
var firstColumn = "sm-4";
var secondColumn = "sm-12";
$(document).ready(
    function()
    {
        $("#hotel_rooms_id").on("change" , ".numberOfChild",
            function()
            {
                //debugger;
                child_num = $(this).attr("value");
                for_room = $("option:selected" , this).attr("for_room");
                room_index = "#room" + for_room;

                child_num = parseInt(child_num);

                var child_ages_html = "";
                if(divRoomLabel == 1)
                {
                    child_ages_html += '<div class="col-sm-3">' +
                    '<p style="margin: 36px 0px 0px 0px;text-align: right;">Room ' + for_room + '</p>' +
                    '</div>';
                }
				if(child_num == 0)
				{
					
				}
				else
				{
					$(".chAges").hide();
					$(".chAges").find('.children_ages_heading').remove();
					$(".chAges").prepend("<h4 class='children_ages_heading' style='margin: 5px 0px 5px 15px;'>Childrens Ages:</h4>");
				}
				
				if(child_num != 0)
				{
					child_ages_html += '<div class="col-sm-12">';
							child_ages_html += 'Room '+for_room;
					child_ages_html += '</div>';
				}
				
                for(i=1; i<=child_num; i++)
                {

                    //child_ages_html += '<div class="col-sm-4">' +
					
                    child_ages_html += '<div class="col-' + firstColumn + '">' +
					'<label class="control-label">Child ' + i +'</label>' +
                    '<div class="form-group">' +
                    //'<label class="control-label col-md-12">Child ' + i +'</label>' +
                    
                    '<select class="select2me form-control input-xlarge rooms" name="childAge[]" id="childAge" style="text-align: center;">';

                    for(j=1; j<=17; j++)
                    {
                        child_ages_html += "<option value='" + j + "'>" + j + "</option>";
                    }

                    child_ages_html += '</select>'+
                    '</div>' +
                    '</div>';


                }
                if(divRoomLabel == 1)
                {
                    child_ages_html += '</div>';
                }
                $(room_index).html(child_ages_html);
                //$(".chAges").hide();
                $('.chAges').css('display','block');

                if(child_num == 0)
                {
                    //$(".chAges").css('display' , 'none');
                }

            }
        );
    }
);