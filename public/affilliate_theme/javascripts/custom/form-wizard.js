var FormWizard = function () {
    jQuery.validator.addMethod("letter", function(value, element) {
        //return this.optional(element) || /^[a-zA-Z_@!#\$\^%&*()+=\-\[\]\\\';,\.\/\{\}\|\":<>\? ]+$/i.test(value);
        return this.optional(element) || /^[a-zA-Z ]+$/i.test(value);
    }, "Only Alphabet allow");

    return {
        //main function to initiate the module
        init: function () {
            if (!jQuery().bootstrapWizard) {
                return;
            }

            function format(state) {
                if (!state.id) return state.text; // optgroup
                return "<img class='flag' src="+ baseurl +"public/assets/img/flags/" + state.id.toLowerCase() + ".png>&nbsp;&nbsp;" + state.text;
            }
            // var promocode = function(){

            $('body').on('click', '.applybutton', function() {
                var promocode = $('.promocode').val();

                $.ajax({
                    type: 'post',
                    url: baseurl + "front/confirm_account/applycode",
                    data: {"promocode": promocode},
                    success: function(output) {
                        output = JSON.parse(output);
                        if(output.error == true)
                        {
                            setTimeout(function(){
                                $('.successline').removeClass('hidden');
                                $('#invalid-promo').modal('show');
                                $('.promocode').val('');
                            },100);
                        }
                        else
                        {
                            $('#show_promo_txt').show();
                            $('#show_promo_txt').html(' *You will receive this discount on your first booking once the total amount reaches at least $'+output.minspen+'. ');
                            $('.successline').removeClass('hidden');
                            $('.promocodearea').addClass('hidden');
                            $('.successtext').html('CONGRATULATIONS!!! $'+output.amount+' DISCOUNT ON YOUR FIRST BOOKING!<sup>*</sup>');
                        }

                    }
                });
            });
            // }
            $("#country_list").select2({
                placeholder: "Select",
                allowClear: true,
                formatResult: format,
                formatSelection: format,
                escapeMarkup: function (m) {
                    return m;
                }
            });

            var form = $('#submit_form');
            var error = $('.alert-danger', form);
            var success = $('.alert-success', form);

            form.validate({
                doNotHideMessage: false, //this option enables to show the error/success messages on tab switch.
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                rules: {
                    //account

                    /* addded*/
                    username: {
                        required: true,
                        minlength: 4,
                        maxlength: 15,

                    },
                    accountidname: {
                        required: true,
                    },
                    checkemail: {
                        required: true,
                    },
                    firstname: {
                        required: true,
                        letter:true,
                        maxlength: 20,
                    },
                    lastname: {
                        required: true,
                        letter:true,
                        maxlength: 20,
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        minlength: 8,
                        required: true,
                        pwcheck: true,

                    },
                    rpassword: {
                        minlength: 8,
                        required: true,
                        equalTo: "#submit_form_password",
                        pwcheck: true,
                    },
                    phone1: {
                        //  required: true,
                        maxlength: 4,
                        minlength: 1,
                        number:true
                    },
                    phone2: {
                        // required: true,
                        maxlength: 12,
                        minlength: 1,
                        number:true
                    },
                    phone3: {
                        //  required: true,
                        maxlength: 6,
                        minlength: 0,
                        number:true
                    },
                    dob:{
                        required: true,
                    },
                    country: {
                        required: true
                    },
                    newsletter: {
                        required: true
                    },
//                     remember:{
//                        required: true,
//                    },            
                    policy: {
                        required: true,
                        minlength: 1
                    },
                    hear_text: {
                        required: true,
                        minlength: 4
                    },
                },

                messages: { // custom messages for radio buttons and checkboxes
                    'payment[]': {
                        required: "Please select at least one option",
                        minlength: jQuery.format("Please select at least one option")
                    },
                    username: {
                        required: " ",
                        minlength: " ",
                        maxlength: " ",
                        alphanumeric: " ",
                        //    usernamecheck: " "
                    },
                    firstname: {
                        required: " ",
                        letter:" ",
                        maxlength: " ",
                    },
                    lastname: {
                        required: " ",
                        letter:" ",
                        maxlength: " ",
                    },
                    email: {
                        required: " ",
                        email: " "
                    },
                    password: {
                        minlength: " ",
                        required: " ",
                        pwcheck: " ",
                    },
                    rpassword: {
                        minlength: " ",
                        required: " ",
                        equalTo: " ",
                        pwcheck: " ",
                    },
                    phone1: {
                        // required: " ",
                        maxlength: " ",
                        minlength: " ",
                        number:" "
                    },
                    phone2: {
                        //    required: " ",
                        maxlength: " ",
                        minlength: " ",
                        number:" "
                    },
                    phone3: {
                        // required: " ",
                        maxlength: " ",
                        minlength: " ",
                        number:" "
                    },
                    dob:{
                        required: " ",
                    },
                    country: {
                        required: " "
                    },
                    newsletter: {
                        required: " "
                    },
                    policy: {
                        required: " ",
                        minlength: " "
                    },
                    hear_text: {
                        required: " ",
                        minlength: " "
                    }
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    if (element.attr("name") == "gender") { // for uniform radio buttons, insert the after the given container
                        error.insertAfter("#form_gender_error");
                    } else if (element.attr("name") == "payment[]") { // for uniform radio buttons, insert the after the given container
                        error.insertAfter("#form_payment_error");
                    } else {
                        error.insertAfter(element); // for other inputs, just perform default behavior
                    }
                    if (element.attr("name") == "policy") { // insert checkbox errors after the container
                        error.insertAfter($('#register_tnc_error'));
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit   
                    success.hide();
                    error.show();
                    App.scrollTo(error, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    if (label.attr("for") == "gender" || label.attr("for") == "payment[]") { // for checkboxes and radio buttons, no need to show OK icon
                        label
                            .closest('.form-group').removeClass('has-error').addClass('has-success');
                        label.remove(); // remove error label here
                    } else { // display success icon for other inputs
                        label
                            .addClass('valid') // mark the current input as valid and display OK icon
                            .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                    }
                },
                submitHandler: function (form) {

                    var url = baseurl+"front/home/memberregister";
                    var data =  $(form).serialize();
                    // alert('kk');
//                        ajaxcall(url,data,function(){   
//                           setTimeout(function(){
//                            //   location.href = baseurl;
//                           },2000); 
//                          
//                        });
                    $.ajax({
                        type: 'post',
                        url: baseurl+"front/home/memberregister",
                        data: data,
                        success: function(output) {
                            if(output == 'success')
                            {
                                // alert('dd');
                                success.show();
                                $('#success_registration').modal('show');
                                error.hide();
                            }else if(output == 'captcha_error'){
                                $('#alert-captcha').show();
                                $('#alert-captcha').focus();

                            }
                        }
                    });

                    //add here some ajax code to submit your form or just call form.submit() if you want to submit the form without ajax
                }

            });

            $.validator.addMethod("pwcheck", function(value) {
                return /^[A-Za-z0-9\d=!\-@._*]*$/.test(value) // consists of only these
                && /[a-z]/.test(value) // has a lowercase letter
                && /\d/.test(value) // has a digit
            });
            $.validator.addMethod("usernamecheck", function(value) {
                return /^[A-Za-z0-9\d=!\-@._*]*$/.test(value) // consists of only these
                && /[a-z]/.test(value) // has a lowercase letter
                && /\d/.test(value) // has a digit
            });
            $('.clickhome').click(function(){
                location.href = baseurl;
            });


            function validateEmail(email) {
                var re = /\S+@\S+\.\S+/;
                return re.test(email);
            }

            $("#email").focusout(function() {
                var email = $(this).val();
                var confirm_email = validateEmail(email);
                if (confirm_email){
                    $.ajax({
                        type: 'post',
                        url: baseurl + "front/home/checkemail",
                        data: {"email": email},
                        success: function(output) {
                            if(output == 'success')
                            {
                                $('#email_message').parents('.form-group').removeClass('has-error');
                                $('#email_message').parents('.form-group').addClass('has-success');
                                $('#email_message').css('display','none');
                                $('#checkemail').val('found');
                                $('.email1').removeClass('tooltips');
                            }
                            else
                            {
                                $('#email_message').parents('.form-group').removeClass('has-success');
                                $('#email_message').parents('.form-group').addClass('has-error')
                                $('.email1').addClass('tooltips');
                                $('#email_message').css('display','block');
                                $('#checkemail').val('');
                            }
                        }
                    });
                }else{
                    $('#email_message').parents('.form-group').removeClass('has-error');
                }

            });
            $("#username").focusout(function() {
                var username = $(this).val();
                $.ajax({
                    type: 'post',
                    url: baseurl + "front/home/checkusername",
                    data: {"username": username},
                    success: function(output) {
                        if (output == 'success')
                        {

                            $('#user_massage').parents('.form-group').removeClass('has-error');
                            $('#user_massage').css('display', 'none');
                            $('#accountid').val('found');
                            $('.username1').removeClass('tooltips');

                        }
                        else
                        {
                            $('.username1').addClass('tooltips');
                            $('#user_massage').parents('.form-group').removeClass('has-success');
                            $('#user_massage').parents('.form-group').addClass('has-error');
                            $('#user_massage').css('display', 'block');
                            $('#accountid').val('');
                        }
                    }
                });
            });

            $(".firstname").keypress(function(event) { return isNumber(event) });
            $(".lastname").keypress(function(event) { return isNumber(event) });
            function isNumber(evt) {

                var charCode = (evt.which) ? evt.which : event.keyCode
                if ((charCode != 46 || $(this).val().indexOf('.') != -1) && (charCode < 48 || charCode > 57))
                    return true;

                return false;
            }

            $("#username").keypress(function(event) { return isSymbol(event) });
            function isSymbol(evt) {

                var charCode = (evt.which) ? evt.which : event.keyCode
                // alert(charCode);
                if ((charCode > 31 && charCode < 48) || (charCode > 57 && charCode< 65) || (charCode > 90 && charCode < 97) || (charCode > 122 && charCode < 127)){
                    return false;
                }
                return true;
            }
            $('#inlineCheckbox21').click(function(){
                var thisCheck = $(this);
                if ( thisCheck.is(':checked') ) {
                    if (captchacorrect == true) {
                        $('input[type="submit"]').removeAttr('disabled');
                        $('input[type="submit"]').addClass('green_btn')
                        $('input[type="submit"]').removeClass('red_btn')
                    }

                }else{
                    $('input[type="submit"]').prop("disabled", true);
                    $('input[type="submit"]').addClass('red_btn')
                    $('input[type="submit"]').removeClass('green_btn')

                }
            });
            var displayConfirm = function() {
                $('#tab4 .form-control-static', form).each(function(){
                    var input = $('[name="'+$(this).attr("data-display")+'"]', form);
                    if (input.is(":radio")) {
                        input = $('[name="'+$(this).attr("data-display")+'"]:checked', form);
                    }
                    if (input.is(":text") || input.is("textarea")) {
                        $(this).html(input.val());
                    } else if (input.is("select")) {
                        $(this).html(input.find('option:selected').text());
                    } else if (input.is(":radio") && input.is(":checked")) {
                        $(this).html(input.attr("data-title"));
                    } else if ($(this).attr("data-display") == 'payment') {
                        var payment = [];
                        $('[name="payment[]"]').each(function(){
                            payment.push($(this).attr('data-title'));
                        });
                        $(this).html(payment.join("<br>"));
                    }
                });
            }

            var handleTitle = function(tab, navigation, index) {
                var total = navigation.find('li').length;
                var current = index + 1;
                // set wizard title
                $('.step-title', $('#form_wizard_1')).text('Step ' + (index + 1) + ' of ' + total);
                // set done steps
                jQuery('li', $('#form_wizard_1')).removeClass("done");
                var li_list = navigation.find('li');
                for (var i = 0; i < index; i++) {
                    jQuery(li_list[i]).addClass("done");
                }

                if (current == 1) {
                    $('#form_wizard_1').find('.button-previous').hide();
                } else {
                    $('#form_wizard_1').find('.button-previous').show();
                }

                if (current >= total) {
                    $('#form_wizard_1').find('.button-next').hide();
                    $('#form_wizard_1').find('#reset_form').hide();
                    $('#form_wizard_1').find('.button-submit').show();
                    displayConfirm();
                } else {
                    $('#form_wizard_1').find('.button-next').show();
                    $('#form_wizard_1').find('#reset_form').hide();
                    $('#form_wizard_1').find('.button-previous').show();
                    $('#form_wizard_1').find('.button-submit').hide();
                }
                App.scrollTo($('.page-title'));
            }

            // default form wizard
            $('#form_wizard_1').bootstrapWizard({
                'nextSelector': '.button-next',
                'previousSelector': '.button-previous',
                onTabClick: function (tab, navigation, index, clickedIndex) {
                    success.hide();
                    error.hide();
                    if (form.valid() == false) {
                        return false;
                    }
                    handleTitle(tab, navigation, clickedIndex);
                },
                onNext: function (tab, navigation, index) {
                    success.hide();
                    error.hide();
                    var account = $('#accountid').val();
                    var checkemail = $('#checkemail').val();
                    var flag = 0;
                    $('.disscounttext').hide();
                    if (form.valid() == false) {
                        return false;
                    }
                    if (account == "") {
                        $('#accountid').parent().parent().removeClass('has-success');
                        $('#accountid').parent().parent().addClass('has-error');
                        flag = 1;
                    }
                    if (checkemail == "") {
                        $('#checkemail').parent().parent().removeClass('has-success');
                        $('#checkemail').parent().parent().addClass('has-error');
                        flag = 1;
                    }
                    if(flag)
                    {
                        return false;
                    }
                    handleTitle(tab, navigation, index);
                },
                onPrevious: function (tab, navigation, index) {
                    success.hide();
                    error.hide();

                    handleTitle(tab, navigation, index);
                },
                onTabShow: function (tab, navigation, index) {
                    var total = navigation.find('li').length;
                    var current = index + 1;
                    var $percent = (current / total) * 100;
                    $('#form_wizard_1').find('.progress-bar').css({
                        width: $percent + '%'
                    });
                }
            });

            $('#form_wizard_1').find('.button-previous').hide();
            $('#form_wizard_1 .button-submit').click(function () {
            }).hide();
        }

    };

}();