
$(function() {
    
    $(".srRating").rating({
        disabled:true,
        size:'xs',
        showClear:false,
        hoverEnabled:false,
        starCaptions:{
                1: '1 Star Hotel',
                2: '2 Star Hotel',
                3: '3 Star Hotel',
                4: '4 Star Hotel',
                5: '5 Star Hotel'
        },
        starCaptionClasses: function(val) {
            if (val == 0) {
               return 'capText';
            }
            else if (val < 3) {
                return 'capText';
            } 
            else {
                return 'capText';
            }
        }
       
    });

    $(".srRating2").rating({

        disabled:true,
        size:'xs',
        showClear:false,
        hoverEnabled:false,
        glyphicon:false,
        ratingClass:'rating-fa',
        starCaptions:{
                1: 'Poor',
                2: 'Poor',
                3: 'Good',
                4: 'Good',
                5: 'Great'
        },
        starCaptionClasses: function(val) {
            if (val == 0) {
               return 'hidden';
            }
            else if (val < 3) {
                return 'hidden';
            } 
            else {
                return 'hidden';
            }
        }
       
    });


    $("#ex2").slider({});


    $( ".range1_cruise" ).slider ({
        range: true,
        min: min_price_slider,
        max: max_price_slider,
        values: [ min_price_slider, max_price_slider ],
        slide: function( event, ui ) {
            $( "#amount_cruise" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
            min_price_slider = ui.values[ 0 ];
            max_price_slider = ui.values[ 1 ];

            ShowCruiseDetails(cruise_list);

        }
    });

    $( "#amount_cruise" ).val( "$" + $( ".range1_cruise" ).slider( "values", 0 ) +
    " - $" + $( ".range1_cruise" ).slider( "values", 1 ) );



});

jQuery(document).ready(function($) {

    /*//$('#slPicture').carousel({
   //     interval: 5000
    //});

    //Handles the carousel thumbnails
    $('[id^=carousel-selector-]').click(function () {
    var id_selector = $(this).attr("id");
    try {
            var id = /-(\d+)$/.exec(id_selector)[1];
            console.log(id_selector, id);
            jQuery('#slPicture').carousel(parseInt(id));
        } catch (e) {
            console.log('Regex failed!', e);
        }
    });
    // When the carousel slides, auto update the text
    $('#slPicture').on('slid.bs.carousel', function (e) {
         var id = $('.item.active').data('slide-number');
        $('#carousel-text').html($('#slide-content-'+id).html());
    });*/
});


$(function() {
   // $('#myWizard').wizard();
});