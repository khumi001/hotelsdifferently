$(document).ready(function (){
    $(".map_tab_clicked").click(function (){
        initializeMapForSingleHotel(global_portArray, global_map_name);
    });
});

$(function() {
    $(".srRating").rating({
        disabled: true,
        size: 'xs',
        showClear: false,
        hoverEnabled: false,
        starCaptions: {
            1: '1 Star Hotel',
            2: '2 Star Hotel',
            3: '3 Star Hotel',
            4: '4 Star Hotel',
            5: '5 Star Hotel'
        },
        starCaptionClasses: function (val) {
            if (val == 0)
            {
                return 'capText';
            }
            else if (val < 3)
            {
                return 'capText';
            }
            else
            {
                return 'capText';
            }
        }
    });
    $(".srRating2").rating({
        disabled:true,
        size:'xs',
        showClear:false,
        hoverEnabled:false,
        glyphicon:false,
        ratingClass:'rating-fa',
        starCaptions:{
            1: 'Poor',
            2: 'Poor',
            3: 'Good',
            4: 'Good',
            5: 'Great'
        },
        starCaptionClasses: function(val) {
            if (val == 0) {
                return 'hidden';
            }
            else if (val < 3) {
                return 'hidden';
            }
            else {
                return 'hidden';
            }
        }
    });
    $("#ex2").slider({});
    var callTimer = null;
    $( ".range1" ).slider ({
        range: true,
        min: min_price_slider,
        max: max_price_slider,
        values: [ min_price_slider, max_price_slider ],
        change:function(){
            if(callTimer){
                clearTimeout(callTimer);
            }
            callTimer = setTimeout(searchHotelResult(1), 2000);
        },
        slide: function( event, ui ) {
            $( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
            min_price_slider = ui.values[ 0 ];
            max_price_slider = ui.values[ 1 ];
        }
    });

    $( "#amount" ).val( "$" + $( ".range1" ).slider( "values", 0 ) +
    " - $" + $( ".range1" ).slider( "values", 1 ) );

    $( ".range2" ).slider ({
        range: true,
        min: min_dest_slider,
        max: max_dest_slider,
        values: [ min_dest_slider, max_dest_slider ],
        slide: function( event, ui ) {
            $( "#miles" ).val( ui.values[ 0 ]+" mile" + " - " + ui.values[ 1 ]+" miles" );
            min_dest_slider = ui.values[ 0 ];
            max_dest_slider = ui.values[ 1 ];

            showHotelSearchResults();

        }
    });

    $( "#miles" ).val( $( ".range2" ).slider( "values", 0 ) +" mile"+
    " - " + $( ".range2" ).slider( "values", 1 ) + " miles" );
});

jQuery(document).ready(function($) {

    $('#myCarousel').carousel({
        interval: 5000
    });

    //Handles the carousel thumbnails
    $(document).on('click','[id^=carousel-selector-]',function () {
        var id_selector = $(this).attr("id");
        try {
            var id = /-(\d+)$/.exec(id_selector)[1];
            console.log(id_selector, id);
            jQuery('#myCarousel').carousel(parseInt(id));
        } catch (e) {
            console.log('Regex failed!', e);
        }
    });
    // When the carousel slides, auto update the text
    $('#myCarousel').on('slid.bs.carousel', function (e) {
        var id = $('.item.active').data('slide-number');
        $('#carousel-text').html($('#slide-content-'+id).html());
    });

    /*$('#slPicture').carousel({
     interval: 5000
     });
     //Handles the carousel thumbnails
     $('[id^=carousel-selector-]').click(function () {
     var id_selector = $(this).attr("id");
     try {
     var id = /-(\d+)$/.exec(id_selector)[1];
     console.log(id_selector, id);
     jQuery('#slPicture').carousel(parseInt(id));
     } catch (e) {
     console.log('Regex failed!', e);
     }
     });
     // When the carousel slides, auto update the text
     $('#slPicture').on('slid.bs.carousel', function (e) {
     var id = $('.item.active').data('slide-number');
     $('#carousel-text').html($('#slide-content-'+id).html());
     });*/
});



////////////////////////////////////////////////////////
function popuoBookNow(id)
{
    $('#'+id).submit();
}
$(document).ready(function (){
    $(".hotel-rate-and-policy a").click(function (event) {
        $("#popover").addClass('hide')
    });
    $('#content').on('click', '.hotel_search_details', function (event)
    {
        var hotel_id2 = $(this).attr('hotel_id');
        var hotel_type = $(this).attr('hotel-type');
        var tab_clicked = $(this).attr('tab_name');
        var book_now_url = $(this).attr('modal_book_now_button');
        var per_night_fee = $(this).attr('min_price');
        var form_id = $(this).attr('form-id');
        hotel_zip = "";
        hotel_city = "";
        hotel_country = "";
        hotel_state = "";
        hotel_location_name = "";
        hotel_zip = $('#hotelthumb'+hotel_id2).attr('hotel_location_zip');

        hotel_city = $('#hotelthumb'+hotel_id2).attr('hotel_location_city');
        hotel_country = $('#hotelthumb'+hotel_id2).attr('hotel_location_country');
        hotel_state = $('#hotelthumb'+hotel_id2).attr('hotel_location_state');
        hotel_location_name = $('#hotelthumb'+hotel_id2).attr('hotel_location_name');
        var btnonbooknow = "popuoBookNow('"+form_id+"0');"
        var book_now_section;
        book_now_section = '<h4 style="margin:5px 0px; color: #EC2A26;">TOTAL</h4>' +
        '<h4>$' + per_night_fee + '</h4>' +
        '<a class="btn btn-danger btn-sm popuoBookNow" onclick="'+btnonbooknow+'"  href="javascript:void(0)">Book now!</a>';
        $("#modal_book_now").html(book_now_section);
        if (tab_clicked == "information")
        {
            $("#hotel_search_information_tab").addClass("active");
            $("#hotel_search_amenities_tab").removeClass("active");
            $("#hotel_search_map_tab").removeClass("active");
            $("#hotel_search_pictures_tab").removeClass("active");
            $("#information").addClass("active in");
            $("#ame").removeClass("active in");
            $("#pictures").removeClass("active in");
            $("#map").removeClass("active in");
        }
        else if (tab_clicked == "amenities")
        {
            $("#hotel_search_amenities_tab").addClass("active");
            $("#hotel_search_information_tab").removeClass("active");
            $("#hotel_search_map_tab").removeClass("active");
            $("#hotel_search_pictures_tab").removeClass("active");
            $("#information").removeClass("active in");
            $("#ame").addClass("active in");
            $("#pictures").removeClass("active in");
            $("#map").removeClass("active in");
        }
        else if (tab_clicked == "map")
        {
            $("#hotel_search_map_tab").addClass("active");
            $("#hotel_search_amenities_tab").removeClass("active");
            $("#hotel_search_information_tab").removeClass("active");
            $("#hotel_search_pictures_tab").removeClass("active");
            $("#information").removeClass("active in");
            $("#ame").removeClass("active in");
            $("#pictures").removeClass("active in");
            $("#map").addClass("active in");
        }
        else if (tab_clicked == "picture")
        {
            $("#hotel_search_pictures_tab").addClass("active");
            $("#hotel_search_map_tab").removeClass("active");
            $("#hotel_search_amenities_tab").removeClass("active");
            $("#hotel_search_information_tab").removeClass("active");
            $("#information").removeClass("active in");
            $("#ame").removeClass("active in");
            $("#pictures").addClass("active in");
            $("#map").removeClass("active in");
        }
        $(".loader").show();
        jQuery.post(hotel_search_id_ajax_path, {hotel_type:hotel_type,hotel_id: hotel_id2}).done(function (data) {
            $(".loader").hide();
            var jsonData = $.parseJSON(data);
            var hotel_by_id = jsonData.hotel_desc_by_id;
            hotel_thumb_url = '<a class="pull-left" href="' + $('#hotelthumb'+hotel_id2).attr('src') + '">' +
            '<img class="media-object img-responsive thumbnail mh145" src="' + $('#hotelthumb'+hotel_id2).attr('src') + '" alt="image">' +
            '</a>';
            $("#hotel_thumbnail_html").html(hotel_thumb_url);
            hotel_by_id_map_coordinates = [];
            hotel_by_id_map_coordinates[0] = [];
            hotel_by_id_map_coordinates[0]['longitude'] = $('#hotelthumb'+hotel_id2).attr('longitude');
            hotel_by_id_map_coordinates[0]['latitude']  = $('#hotelthumb'+hotel_id2).attr('latitude');
            hotel_by_id_map_coordinates[0]['port_name'] = $('#hotelthumb'+hotel_id2).attr('hotelName');
            initializeMapForSingleHotel(hotel_by_id_map_coordinates, "map");
            // Information tab
            var location = hotel_by_id.location_desc;
            var facilities = hotel_by_id.facilities_desc;
            var sports_entertainment = hotel_by_id.sports_entertainment_desc;
            var payments = hotel_by_id.payment_desc;
            var rooms = hotel_by_id.room_desc;
            var meals = hotel_by_id.meals_desc;
            var info_tab = "";
            var information_column_counter = 0;
            if ("" != location)
            {
                info_tab += '<h4 class="info-title">Location</h4>' +
                '<ul class="list-unstyled">' +
                '<li>' + location + '</li>' +
                '</ul>';
                information_column_counter++;
            }
            if ("" != facilities)
            {
                info_tab += '<h4 class="info-title">Facilities</h4>' +
                '<ul class="list-unstyled">' +
                '<li>' + facilities + '</li>' +
                '</ul>';
                information_column_counter++;
            }
            if ("" != sports_entertainment)
            {
                info_tab += '<h4 class="info-title">Sports/Entertainment</h4>' +
                '<ul class="list-unstyled">' +
                '<li>' + sports_entertainment + '</li>' +
                '</ul>';
                information_column_counter++;
            }
            if (information_column_counter == 3)
            {
                $("#information_tab_1").html(info_tab);
                info_tab = "";
            }
            if ("" != payments)
            {
                info_tab += '<h4 class="info-title">Payment</h4>' +
                '<ul class="list-unstyled">' +
                '<li>' + payments + '</li>' +
                '</ul>';
            }
            if (information_column_counter == 3)
            {
                $("#information_tab_1").html(info_tab);
                info_tab = "";
            }
            if ("" != rooms)
            {
                info_tab += '<h4 class="info-title">Rooms</h4>' +
                '<ul class="list-unstyled">' +
                '<li>' + rooms + '</li>' +
                '</ul>';
            }
            if (information_column_counter == 3)
            {
                $("#information_tab_1").html(info_tab);
                info_tab = "";
            }
            if ("" != meals)
            {
                info_tab += '<h4 class="info-title">Facilities</h4>' +
                '<ul class="list-unstyled">' +
                '<li>' + meals + '</li>' +
                '</ul>';
            }
            if (information_column_counter > 3)
            {
                $("#information_tab_2").html(info_tab);
                info_tab = "";
            }
            else
            {
                $("#information_tab_1").html(info_tab);
                info_tab = "";
            }
            var amenities_arr = hotel_by_id.aminities;
            var images_arr = hotel_by_id.images;
            var amenities_html = "";
            for (i = 0; i < amenities_arr.length; i++)
            {
                amenities_html += '<li>' + amenities_arr[i] + '</li>';
            }
            $("#hotel_by_id_aminities").html(amenities_html);
            var images_path = "";
            var slide_switcher_path = "";
            if (images_arr.length > 0)
            {
                images_path = '<div class="active item" data-slide-number="0"><img src="' + images_arr[0] + '"></div>';
                slide_switcher_path += '<li class="col-sm-2">' +
                '<a class="thumbnail" id="carousel-selector-0"><img src="' + images_arr[0] + '"></a>' +
                '</li>';
                for (i = 1; i < images_arr.length; i++)
                {
                    images_path += '<div class="item" data-slide-number="' + i + '"><img src="' + images_arr[i] + '"></div>';
                    slide_switcher_path += '<li class="col-sm-2">' +
                    '<a class="thumbnail" id="carousel-selector-' + i + '"><img src="' + images_arr[i] + '"></a>' +
                    '</li>';
                }


            }else{
                images_path = '<div class="active item" data-slide-number="0"><img class="img-responsive" src="http://www.touricoholidays.com/en/Modules/customizable/images/photo-not-available-small.gif" alt="default image"></div>';
            }

            $("#image_slider").html(images_path);
            $("#all_slider_images").html(slide_switcher_path);
            // add this class to carousal after loading images in order to start carosal
            $("#myCarousel").addClass("carousel slide");

            var hotel_name = hotel_by_id.hotelName;
            $("#hotel_name").html('<h4>' + hotel_name + '</h4>');
            var star_level = hotel_by_id.star_level;
            $("#hotel_rating").html('<input type="text" name="rating" id="inputRating" class="srRating hidden tt form-control hide" value="' + star_level + '">');
            $(".srRating").rating({
                disabled: true,
                size: 'xs',
                showClear: false,
                hoverEnabled: false,
                starCaptions: {
                    1: '1 Star Hotel',
                    2: '2 Star Hotel',
                    3: '3 Star Hotel',
                    4: '4 Star Hotel',
                    5: '5 Star Hotel'
                },
                starCaptionClasses: function (val) {
                    if (val == 0)
                    {
                        return 'capText';
                    }
                    else if (val < 3)
                    {
                        return 'capText';
                    }
                    else
                    {
                        return 'capText';
                    }
                }
            });
            var address = hotel_by_id.address;
            address = '<strong>' + hotel_by_id.address + '</strong><br>' + hotel_location_name + '<br>' +
            '' + hotel_city + ' ' + hotel_state + ' ' + hotel_country + '  ' + hotel_zip + '<br>';
            $("#hotel_address").html(address);
            var hotel_phone = hotel_by_id.hotel_phone;
            $("#hotel_phone").html(hotel_phone);
            $('#pop').modal({
                show: true
            });
        });
    });
    $(window).scroll(function () {
        if ($(window).scrollTop() + $(window).height() > $('.footer_scroll').offset().top) {
            searchHotelResult();
        }
    });
    $(".hotel_star_ratings").click(
        function ()
        {
            searchHotelResult(1);
        }
    );
    $(".hotel_search_select_all").click(
        function ()
        {
            $(".hotel_star_ratings").each(
                function ()
                {
                    $('.hotel_star_ratings').prop('checked', true);
                }
            );
            searchHotelResult(1);
        }
    );
    $(".search_by_hotel_name_button").click(
        function ()
        {
            searchHotelResult(1);
        }
    );
    $(".hotel_brand_select_all").click(
        function ()
        {
            $(".hotel_brand_checkbox").each(
                function ()
                {
                    $('.hotel_brand_checkbox').prop('checked', true);
                }
            );
            searchHotelResult(1);
        }
    );
    $(".hotel_brand_clear_all").click(
        function ()
        {
            $(".hotel_brand_checkbox").each(
                function ()
                {
                    $('.hotel_brand_checkbox').prop('checked', false);
                }
            );
            searchHotelResult(1);
        }
    );
    $(".hotel_brand_checkbox").click(function (){
        searchHotelResult(1);
    });

    // locations filter
    $(".hotel_location_select_all").click(
        function ()
        {
            alert(1);
            $(".hotel_search_location_chkbox").each(
                function ()
                {
                    $('.hotel_search_location_chkbox').prop('checked', true);
                }
            );
            searchHotelResult(1);
        }
    );
    $(".hotel_location_clear_all").click(
        function ()
        {
            $(".hotel_search_location_chkbox").each(
                function ()
                {
                    $('.hotel_search_location_chkbox').prop('checked', false);
                }
            );
            searchHotelResult(1);
        }
    );
    $(".hotel_search_location_chkbox").click(function (){
        searchHotelResult(1);
    });
});
var ajaxCheck = true;
function searchHotelResult(filter = 0)
{
    var currentLoadedHotel = currentPage * perPageHotel;
    if((totalHotel > currentLoadedHotel && ajaxCheck == true) || filter)
    {
        ajaxCheck = false;
        $(".loader").show();
        if(filter)
        {
            currentPage = 0;
        }
        var starsFilter = [];
        $('.hotel_star_ratings').each(function(){
            if ($(this).attr("checked"))
            {
                starsFilter.push($(this).val());
            }
        });
        var brandFilter = [];
        $('.hotel_brand_checkbox').each(function(){
            if ($(this).attr("checked"))
            {
                brandFilter.push($(this).val());
            }
        });
        var locationFilter = [];
        $('.hotel_search_location_chkbox').each(function(){
            if ($(this).attr("checked"))
            {
                locationFilter.push($(this).val());
            }
        });

        var search_hotel_name = $(".search_by_hotel_name_text_box").val();
        var ajaxData = {locationId:locationId,locationName:locationName,numberOfAdults:numberOfAdults,numberOfChild:numberOfChild,from:from,to:to,roomNo: roomNo ,uniqueSession: uniqueSession , currentPage : currentPage , min_price: min_price_slider , max_price : max_price_slider , locations:locationFilter,stars : starsFilter , hotel_name : search_hotel_name , brand : brandFilter};
        jQuery.post(searchFilterAndMoreResult, ajaxData).done(function (data) {
            $(".loader").hide();
            var jsonData = $.parseJSON(data);
            if(jsonData.status == 1)
            {
                currentPage = jsonData.nextPage;
                totalHotel = jsonData.totalRec;
                if(totalHotel > 0)
                {
                    $('#total_hotels_found_id').html('<p>We found <b style="font-size: 15px;">'+totalHotel+'</b> hotels for you!</p>');
                }
                else
                {
                    $('#total_hotels_found_id').html('<p>No result found!</p>');
                }
                if(filter)
                {
                    $('.sResult').html(jsonData.html);
                }
                else
                {
                    $('.sResult').append(jsonData.html);
                }
                $(".srRating").rating({
                    disabled: true,
                    size: 'xs',
                    showClear: false,
                    hoverEnabled: false,
                    starCaptions: {
                        1: '1 Star Hotel',
                        2: '2 Star Hotel',
                        3: '3 Star Hotel',
                        4: '4 Star Hotel',
                        5: '5 Star Hotel'
                    },
                    starCaptionClasses: function (val) {
                        if (val == 0)
                        {
                            return 'capText';
                        }
                        else if (val < 3)
                        {
                            return 'capText';
                        }
                        else
                        {
                            return 'capText';
                        }
                    }
                });
            }
            else if(jsonData.status == 0)
            {

            }
            else
            {
                console.log("Error in page loading");
            }
            ajaxCheck = true;
        });
    }
}

var global_portArray;
var global_map_name;
function initializeMapForSingleHotel(portArray, mapElementId)
{
    global_portArray = portArray;
    global_map_name = mapElementId;
    var bounds = new google.maps.LatLngBounds();
    var infowindow2 = new google.maps.InfoWindow();
    var totalLongitude = 0;
    var totalLattitude = 0;
    for (var index = 0; index < portArray.length; index++)
    {
        portArray[index].longitude
        totalLongitude += parseFloat(portArray[index].longitude);
        totalLattitude += parseFloat(portArray[index].latitude);
    }
    var midLongitude = totalLongitude / portArray.length;
    var midLatitude = totalLattitude / portArray.length;
    var mapCanvas = document.getElementById(mapElementId);
    var mapOptions ={
        center: {lat: midLatitude, lng: midLongitude},
        zoom: 12
    }
    map = new google.maps.Map(mapCanvas, mapOptions);
    google.maps.event.addListenerOnce(map, 'idle', function () {
        google.maps.event.trigger(map, "resize");
        map.setCenter(new google.maps.LatLng(midLatitude, midLongitude));
    });
    var marker = [];
    for (var index = 0; index < portArray.length; index++) {
        marker[index] = new google.maps.Marker({
            position: {lat: parseFloat(portArray[index].latitude), lng: parseFloat(portArray[index].longitude)},
            map: map,
            title: portArray[index].port_name,
            index2: index
        });
        google.maps.event.addListener(marker[index], 'click', (function (index) {
            return function () {
                infowindow[index].open(map, marker[index]);
            }

        })(index));
    }
}
function decodeHTMLEntities(text) {
    var entities = [
        ['amp', '&'],
        ['apos', '\''],
        ['#x27', '\''],
        ['#x2F', '/'],
        ['#39', '\''],
        ['#47', '/'],
        ['lt', '<'],
        ['gt', '>'],
        ['nbsp', ' '],
        ['quot', '"']
    ];

    for (var i = 0, max = entities.length; i < max; ++i)
        text = text.replace(new RegExp('&'+entities[i][0]+';', 'g'), entities[i][1]);

    return text;
}
function rateAndPolicyFunc(thisclick)
{
    var pos = $(thisclick).offset();
    $("#popover").removeClass('hide').offset({top: pos.top - $("#popover").height(), left: pos.left - ($(thisclick).width() * 3)});
    first_index = parseInt($(thisclick).attr("first_index"));
    second_index = parseInt($(thisclick).attr("second_index"));

    hotel_id = $(thisclick).attr("hotelId");
    minR = $(thisclick).attr("minR");
    room_type_id = $(thisclick).attr("RoomTypeId");
    //$(".loader").show();
    $("#cancellation_policy_text").html(" ");
    if($(thisclick).attr("HotelType") == 'TL'){
        $.post(base_url + "ajax/TL_HotelPolicy", {
            room_type_id: room_type_id,minR:minR
        }).done(function (data) {
            //$(".loader").hide();
            $("#cancellation_policy_text").html(data);
        });
    }else {
        $.post(base_url + "ajax/getCancellationPoliciesBeforeReservation", {
            hotel_id: hotel_id,
            room_type_id: room_type_id,
            check_in: check_in,
            check_out: check_out
        }).done(function (data) {
            var jsonData = JSON.parse(data.trim());
            policy_messages = "";
            cancellation_policies_list = jsonData.cancellation_policies_list;
            for (policy_index = 0; policy_index < cancellation_policies_list.length; policy_index++) {
                policy_messages += cancellation_policies_list[policy_index].message;
                policy_messages += "</br></br>";
            }
            //$(".loader").hide();
            $("#cancellation_policy_text").html(policy_messages);
        });
    }
    var findindex = hotel_id+""+first_index+""+second_index;
    occupancy_lists = dynamic_occupancy_lists[findindex];//hotel_search_results[first_index]['room_type_list'][second_index]['occupancy_list'];
    facilities = $(thisclick).attr("facilities");//hotel_search_results[first_index]['room_type_list'][second_index]['facilities'];
    facility_str = "";
    $("facility_lists").html(facility_str);
    sort_occupancy_list = [];
    for (var prop in occupancy_lists)
    {
        sort_occupancy_list.push([prop, occupancy_lists[prop]['room_sequnce']]);
    }
    sort_occupancy_list.sort(
        function (x, y)
        {
            return parseInt(x[1]) - parseInt(y[1]);
        }
    );
    header = "<th></th><th>Total</th>";
    var totDays = JSON.parse(jsonDays);
    for (ocu_index = 0; ocu_index < 1; ocu_index++)
    {
        prop = sort_occupancy_list[ocu_index][0];
        availability_list = occupancy_lists[prop]['available_list'];
        per_day_price_list = availability_list[0]['per_day_price'];
        var numbeday = 0;
        if(totDays.length > 7)
        {
            numbeday = 7;
        }
        else
        {
            numbeday = totDays.length;
        }
        for (per_day_index = 0; per_day_index < numbeday; per_day_index++)
        {
            header += '<th>' + totDays[per_day_index] + '</th>';
        }
    }
    $("#rate_policies_header").html(header);
    col = "";
    for (ocu_index = 0; ocu_index < sort_occupancy_list.length; ocu_index++)
    {
        prop = sort_occupancy_list[ocu_index][0];
        if(totDays.length > 7)
        {
            col += '<tr><td colspan='+9+'><b>'+prop+'</b></td></tr>';
        }
        else
        {
            col += '<tr><td colspan='+(totDays.length + 2)+'><b>'+prop+'</b></td></tr>';
        }
        availability_list = occupancy_lists[prop]['available_list'];
        bedding_text = "";
        for (available_index = 0; available_index < availability_list.length; available_index++)
        {
            bedding = availability_list[available_index]['bedding'];
            bedding_arr = bedding.split(",");
            if(bedding_arr[1] == 0){
                bedding_text += bedding_arr[0] + " Adults";
            }else {
                bedding_text += bedding_arr[0] + " Adults<br>" + bedding_arr[1] + " Bed </br>";
            }
            var toweek = totDays.length / 7;
            toweek = parseInt(toweek);
            var rowgap = 0;
            if(totDays.length % 7 == 0)
            {
                rowgap = toweek;
            }
            else
            {
                rowgap = toweek + 1;
            }
            col += '<td rowspan='+(rowgap)+'>' +bedding_text + '</td>';
            bedding_text = "";
            bedding = availability_list[available_index]['bedding'];
            bedding_arr = bedding.split(",");
            col += '<td rowspan='+(rowgap)+'>$' + parseFloat(availability_list[available_index]['total_price_inc_tax']).toFixed(2) + '</td>';
            per_day_price_list = availability_list[available_index]['per_day_price_inc_tax'];
            per_day_availability_list = availability_list[available_index]['per_day_availability'];
            ib = 1;
            for (per_day_index = 0; per_day_index < per_day_price_list.length; per_day_index++)
            {
                availability_text = "";
                if (per_day_availability_list[per_day_index] == 'true')
                {
                    availability_text = "Available";
                }
                else
                {
                    availability_text = "On Request";
                }

                if(per_day_index / 7 == ib)
                {
                    col += "</tr><tr>";
                    ib++;
                }
                col += "<td align='center'>";
                col += availability_text+'<br>$' + parseFloat(per_day_price_list[per_day_index]).toFixed(2);
                col += "</td>";
            }
            col = '<tr>' + col + '</tr>';
        }
    }
    $("#rate_policies_body").html(col);
}
$(document).ready(function (){
    var checkin = $('#check_in').datepicker({
        onRender: function (date) {
            if (date.valueOf() < now.valueOf())
            {
                return 'disabled';
            }
            else if (date.valueOf() == now.valueOf())
            {
                return 'pluseone';
            }
            else
            {
                return '';
            }
        }
    }).on('changeDate', function (ev)
    {
        var newDate = new Date(ev.date);

        newDate.setDate(newDate.getDate() + 1);
        $('#check_out').val("")
        checkout.setValue(newDate);

        $(this).datepicker('hide');
        var check_in = $('#check_in').val();
        var check_out = $('#check_out').val();
        check_in = moment($('#check_in').val());
        check_out = moment($('#check_out').val());

        var diff = check_out.diff(check_in, 'days');
        $('.nights').text(diff);
        $('.nights').val(diff);
        $('#check_out')[0].focus();
    }).data('datepicker');
    var checkout = $('#check_out').datepicker({
        onRender: function (date) {
            if (date.valueOf() < now.valueOf())
            {
                return 'disabled';
            } else if (date.valueOf() == now.valueOf())
            {
                return 'pluseone';
            } else
            {
                return '';
            }
        }
    }).on('changeDate', function (ev) {
        checkout.hide();
        var check_in = $('#check_in').val();
        var check_out = $('#check_out').val();
        if (check_in != '' && check_out != '') {
            check_in = moment($('#check_in').val());
            check_out = moment($('#check_out').val());
            var diff = check_out.diff(check_in, 'days');
            $('.nights').text(diff);
            $('.nights').val(diff);
        }

    }).data('datepicker');
});