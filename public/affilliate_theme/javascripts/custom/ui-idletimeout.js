var UIIdleTimeout = function () {

    return {

        //main function to initiate the module
        init: function () {

            // cache a reference to the countdown element so we don't have to query the DOM for it on each ping.
            var $countdown;

            $('body').append('\
<div class="modal fade" id="idle-timeout-dialog" data-backdrop="static">\n\
<div class="modal-dialog modal-small" style="width:400px;">\n\
<div class="modal-content">\n\
<div class="modal-header">\n\
<h4 class="modal-title">Your session is about to expire.</h4>\n\
</div>\n\
<div class="modal-body">\n\
<p><i class="fa fa-warning"></i>You will be logged out due to inactivity in  <span id="idle-timeout-counter"></span> seconds.</p>\n\
<p>Do you want to continue your session?</p>\n\
</div>\n\
<div class="modal-footer" style="text-align:center;">\n\
<button id="idle-timeout-dialog-logout" type="button" class="btn red">No, I logout</button>\n\
<button id="idle-timeout-dialog-keepalive" type="button" class="btn btn-primary" data-dismiss="modal">Yes, keep working</button>\n\
</div>\n\
</div>\n\
</div>\n\
</div>');
                    
            // start the idle timer plugin
            $.idleTimeout('#idle-timeout-dialog', '.modal-content button:last', {
                idleAfter: 900, // 5 seconds
                timeout: 90000000, //30 seconds to timeout
                pollingInterval: 90, // 5 seconds
                keepAliveURL: baseurl,
                serverResponseEquals: 'OK',
                onTimeout: function(){
                    window.location = baseurl+'user/home/noactivity';
                },
                onIdle: function(){
                    $('#idle-timeout-dialog').modal('show');
                    $countdown = $('#idle-timeout-counter');

                    $('#idle-timeout-dialog-keepalive').on('click', function () { 
                        $('#idle-timeout-dialog').modal('hide');
                    });

                    $('#idle-timeout-dialog-logout').on('click', function () { 
                        $('#idle-timeout-dialog').modal('hide');
                        $.idleTimeout.options.onTimeout.call(this);
                    });
                },
                onCountdown: function(counter){
                    $countdown.html(counter); // update the counter
                }
            });
            
        }

    };

}();