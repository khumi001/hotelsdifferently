var Login = function () {
    var handleLogin = function () {
        var form = $('.login-form');
        var error = $('.alert-danger', form);
        var success = $('.alert-success', form);
        form.validate({
            errorElement: 'span',
            errorClass: 'help-block',
            focusInvalid: false,
            rules: {username: {required: true, email: true}, password: {required: true}, remember: {required: false},},
            messages: {username: {required: " ", email: " "}, password: {required: " "}},
            errorPlacement: function (error, element) {
                if (element.attr("name") == "gender") {
                    error.insertAfter("#form_gender_error");
                } else if (element.attr("name") == "payment[]") {
                    error.insertAfter("#form_payment_error");
                } else {
                    error.insertAfter(element);
                }
                if (element.attr("name") == "policy") {
                    error.insertAfter($('#register_tnc_error'));
                }
            },
            invalidHandler: function (event, validator) {
                success.hide();
                error.show();
            },
            highlight: function (element) {
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            success: function (label) {
                if (label.attr("for") == "gender" || label.attr("for") == "payment[]") {
                    label.closest('.form-group').removeClass('has-error').addClass('has-success');
                    label.remove();
                } else {
                    label.addClass('valid').closest('.form-group').removeClass('has-error').addClass('has-success');
                }
            },
            submitHandler: function (form) {
                var username = $("#user").val();
                var password = $("#pass").val();
                var remeber = $("#remember").val();
                $.ajax({
                    type: "POST",
                    url: baseurl + "front/home/login",
                    data: {"username": username, "password": password, "remeber": remeber},
                    success: function (response) {
                        if (response == 'f') {
                            location.href = baseurl + 'affiliate/home';
                        }
                        else if (response == 'tf') {
                            location.href = baseurl;
                        }
                        else if (response == 'en') {
                            window.location = baseurl;
                        }
                        else if (response == 'u') {
                            location.href = baseurl + 'user/home';
                        }
                        else if (response == 'enrolling') {
                            window.location = baseurl + 'user/myaccount/enrolling';
                        }
                        else if (response == 'verification') {
                            window.location = baseurl + 'user/myaccount/verification';
                        }
                        else if (response == 'expired') {
                            window.location = baseurl + 'user/myaccount/expired';
                        }
                        else if (response == 'unverified') {
                            window.location = baseurl + 'user/myaccount/unverified';
                        }
                        else if (response == 'entity_expired') {
                            window.location = baseurl + 'user/myaccount/entity_expired';
                        }
                        else if (response == 'banbyadmin') {
                            $('#login').show();
                            $('#login').children('span').html('Your account has been banned!');
                            $('#login', $('.login-form')).show();
                        }
                        else if (response == 'login_error') {
                            $('#user').parent().parent().removeClass('has-success');
                            $('#user').parent().parent().addClass('has-error');
                            $('#pass').parent().parent().removeClass('has-success');
                            $('#pass').parent().parent().addClass('has-error');
                        }
                        else if (response == 'login_last') {
                            $('#login').children('span').html('Your IP address has been suspended due to too many failed login attempts. Please try again 6 hours later.');
                            $('#login', $('.login-form')).show();
                        } else if (response == 'pendding') {
                            $('#login').children('span').html('Please Activate your account first. <a id="signUpResendEmail" href="javascript:;">Resend email</a>');
                            $('#login', $('.login-form')).show();
                        } else if (response == 'adminpendding') {
                            $('#login').children('span').html('Your account must be approved by the Admin before you are able to login. We will send you a confirmation email as soon as it was successfully processed.');
                            $('#login', $('.login-form')).show();
                        } else if ($.isNumeric(response)) {
                            $('#login').children('span').html('Email address and/or Password does not match. You have ' + response + ' attempts left. Please try again!');
                            $('#login', $('.login-form')).show();
                        }
                    }
                });
            }
        });
        $('.login-form input').keypress(function (e) {
            if (e.which == 13) {
                if ($('.login-form').validate().form()) {
                    $('.login-form').submit();
                }
                return false;
            }
        });
        var email = $('#user').val();
        var kk = validateEmail(email);
        if (kk) {
            $('.listing_btn').attr('disabled', false);
        }
        else {
            $('.listing_btn').attr('disabled', true);
        }
        $('#user').keypress(function (e) {
            var email = $(this).val();
            var kk = validateEmail(email);
            if (kk) {
                $('.listing_btn').attr('disabled', false);
            }
            else {
                $('.listing_btn').attr('disabled', true);
            }
        });
        $('#user').focusout(function (e) {
            var email = $(this).val();
            var kk = validateEmail(email);
            if (kk) {
                $('.listing_btn').attr('disabled', false);
            }
            else {
                $('.listing_btn').attr('disabled', true);
            }
        });
        jQuery('#user').on('input propertychange paste', function () {
            setTimeout(function () {
                var email = $('#user').val();
                var kk = validateEmail(email);
                if (kk) {
                    $('.listing_btn').attr('disabled', false);
                    $('.listing_btn').addClass('disablelld');
                }
                else {
                    $('.listing_btn').attr('disabled', true);
                }
            }, 100);
        });
        $('#user').focusin(function (e) {
            var email = $(this).val();
            var kk = validateEmail(email);
            if (kk) {
                $('.listing_btn').attr('disabled', false);
            }
            else {
                $('.listing_btn').attr('disabled', true);
            }
        });
        $('#user').on('paste', function () {
            setTimeout(function () {
                var email = $('#user').val();
                var kk = validateEmail(email);
                if (kk) {
                    $('.listing_btn').attr('disabled', false);
                    $('.listing_btn').addClass('disablelld');
                }
                else {
                    $('.listing_btn').attr('disabled', true);
                }
            }, 100);
        });
        function validateEmail(email) {
            var re = /\S+@\S+\.\S+/;
            return re.test(email);
        }

        $('#remember').click(function () {
            if ($('#remember').is(':checked')) {
                $(this).val('on');
            } else {
                $(this).val('off');
            }
        });
    }
    var cookie = function () {
        $('.login').click(function () {
            var username = $("#user").val();
            var password = $("#pass").val();
            var remeber = $("#remember1").val();
            var select_city = $("#select_city").val();
            var chackin = $("#check_in").val();
            var chackout = $("#check_out").val();
            var check_in = '';
            var check_out = '';
            check_in = moment($('#check_in').val());
            check_out = moment($('#check_out').val());
            if (check_in != null && check_out != null) {
                var night = check_out.diff(check_in, 'days');
                $('.nights').text(night);
                $('.nights').val(night);
            }
            var room = $("#room").val();
            var adults = $("#adults").val();
            var children = $("#children").val();
            var fname = $("#fname").val();
            var lname = $("#lname").val();
            var hotelname = $("#select_hotel").val();
            var star = $("#star").val();
            var comments = $("#comments").val();
            if (select_city != "" && fname != "") {
                var data1 = {
                    "username": username,
                    "password": password,
                    "remeber": remeber,
                    "night": night,
                    "select_city": select_city,
                    "chackin": chackin,
                    "chackout": chackout,
                    "room": room,
                    "adults": adults,
                    "children": children,
                    "fname": fname,
                    "lname": lname,
                    "hotelname": hotelname,
                    "star": star,
                    "comments": comments
                };
            }
            else {
                var data1 = {"username": username, "password": password, "remeber": remeber}
            }

            $.ajax({
                type: "POST", url: baseurl + "front/home/login", data: data1, success: function (response) {
                    if (response == 'f') {
                        location.href = baseurl + 'affiliate/home';
                    }
                    else if (response == 'tf') {
                        location.href = baseurl + 'affiliate/home';
                    }
                    else if (response == 'en') {
                        location.href = baseurl + 'affiliate/home';
                    }
                    else if (response == 'u') {
                        location.href = baseurl + 'user/home';
                    }
                    else if (response == 'banbyadmin') {
                        location.href = baseurl + 'admin/account/ipban/' + username;
                    }
                    else if (response == 'login_error') {
                        $('#user').parent().parent().removeClass('has-success');
                        $('#user').parent().parent().addClass('has-error');
                        $('#pass').parent().parent().removeClass('has-success');
                        $('#pass').parent().parent().addClass('has-error');
                    }
                    else if (response == 'login_last') {
                        $('#login').children('span').html('Your IP address has been suspended due to too many failed login attempts. Please try again 6 hours later.');
                        $('#login', $('.login-form')).show();
                    } else if (response == 'pendding') {
                        $('#login').children('span').html('Please Activate your account first.');
                        $('#login', $('.login-form')).show();
                    } else if (response == 'adminpendding') {
                        $('#login').children('span').html('Your account must be approved by the Admin before you are able to login. We will send you a confirmation email as soon as it was successfully processed.');
                        $('#login', $('.login-form')).show();
                    } else if ($.isNumeric(response)) {
                        $('#login').children('span').html('Email address and/or Password does not match. You have ' + response + ' attempts left. Please try again!');
                        $('#login', $('.login-form')).show();
                    }
                }
            });
            return false;
        });
    }
    var handleForgetPassword = function () {
        $('.forget-form').validate({
            errorElement: 'span',
            errorClass: 'help-block',
            focusInvalid: false,
            ignore: "",
            rules: {email: {required: true, email: true}},
            messages: {email: {required: " ", email: " "}},
            invalidHandler: function (event, validator) {
            },
            highlight: function (element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
            errorPlacement: function (error, element) {
                error.insertAfter(element.closest('.input-icon'));
            },
            submitHandler: function (form) {
                var forgetemail = $('#forgetemail').val();
                $.ajax({
                    type: "POST",
                    url: baseurl + "front/home/forgot_password",
                    data: {"email": forgetemail},
                    success: function (output) {
                        if (output == 'success') {
                            $('#success_registration').modal('show');
                            handle_click();
                        }
                        else if (output == 'error') {
                            $('#error_registration').modal('show');
                        }
                    }
                });
            }
        });
        $('.forget-form input').keypress(function (e) {
            if (e.which == 13) {
                if ($('.forget-form').validate().form()) {
                    $('.forget-form').submit();
                }
                return false;
            }
        });
        jQuery('#forget-password').click(function () {
            jQuery('.forget-form').show();
        });
        jQuery('#back-btn').click(function () {
            jQuery('.forget-form').hide();
        });
    }
    return {
        init: function () {
            cookie();
            handleLogin();
            handleForgetPassword();
        }
    };
}();