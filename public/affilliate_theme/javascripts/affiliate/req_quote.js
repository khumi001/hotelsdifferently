var Req_quote=function(){var req_quote=function(){var select_hotel=$("#select_hotel").val();var star_rating=$(".star_rating").val();var form=$('#quote_request');var error1=$('.alert-danger',form);var success1=$('.alert-success',form);form.validate({errorElement:'span',errorClass:'help-block',focusInvalid:false,ignore:"",rules:{},messages:{city:{required:" ",},from:{required:" ",},top:{required:" ",},rooms:{required:" ",},adults:{required:" ",},children:{required:" ",},nor_fname:{required:" ",},nor_lname:{required:" ",},},invalidHandler:function(event,validator){success1.hide();error1.show();App.scrollTo(error1,-200);},highlight:function(element){$(element).closest('.form-group').addClass('has-error');if(select_hotel==""&&star_rating=="")
{}},unhighlight:function(element){$(element).closest('.form-group').removeClass('has-error');},success:function(label){label.closest('.form-group').removeClass('has-error');},});$('#user').keypress(function(e){var email=$(this).val();var kk=validateEmail(email);if(kk)
{$('.login-form .listing_btn').attr('disabled',false);}
else
{$('.login-form .listing_btn').attr('disabled',true);}});function validateEmail(email){var re=/\S+@\S+\.\S+/;return re.test(email);}}
var textarea_count=function(){$('#count_char').keyup(function(){var left=100-$(this).val().length;if(left<0){left=0;}
$('#counter').text(left+' Characters left');});}
var textarea_placeholder=function(){var comment=$('#comments').val();var select_hotel=$('#select_hotel').val();var select_city=$('#select_city').val();if(comment==""){var html="We will quote a price for a basic room unless a specific view or suite type is specified here\n\nTIP: The more specific you get, the faster we can get back to you with a quote. For example: Quote request for the Wraparound Terrace suite at the Cosmopolitan in Las Vegas will give you a much faster turnaround time than a quote request for a hotel in Las Vegas that is 3 star or better.";}
else
{var html=comment;}
$('#comments').keyup(function(){var value=$(this).val();if(value!="")
{$('.commentvalue').val('1');}
else
{$('.commentvalue').val('0');}});$('#comments').attr('value',html);$('#comments').css('color','#000');$('#comments').focus(function(){if($(this).val()===html&&$('.commentvalue').val()!=1){$(this).attr('value','');$(this).css('color','#000');}});$('#comments').blur(function(){if($(this).val()===''){$(this).attr('value',html);$(this).css('color','#000');}});if(select_city!="")
{if(select_hotel=='')
{$('.select_hotel').attr('disabled',true);}
else{$('.star_rating').attr('disabled',true);}}}
return{init:function(){$('.data-picker').datepicker({autoclose:true});req_quote();textarea_count();},comments_placeholder:function(){textarea_placeholder();}};}();