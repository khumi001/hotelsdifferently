var Account = function() {
    
    var edit_account = function() {

        var form = $('#edit_account');
        var error1 = $('.alert-danger', form);
        var success1 = $('.alert-success', form);
        form.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                email: {
                    required: true,
                     email: true
                },
                address: {
                    required: true,
                },
                city: {
                    required: true,
                },
                country: {
                    required: true,
                },
                 phone1: {
//                        required: true,
                        maxlength: 4,
                        minlength: 1,
                        number: true
                    },
                    phone2: {
//                        required: true,
                        maxlength: 12,
                        minlength: 1,
                        number: true
                    },
                    phone3: {
//                        required: true,
                        maxlength: 6,
                        minlength: 0,
                        number: true
                    },
                news_letter: {
                    required: true,
                    maxlength: 40
                },
                company1: {
                    required: true,
                    maxlength: 40
                },
                title1: {
                    required: true,
                    maxlength: 20,
                },
//                state: {
//                    required: true,
//                },
                zipcode: {
                    required: true,
                },
                web_url: {
                    required: true,
                    maxlength: 50
                },
                site_desc: {
                    required: true,
                    maxlength: 250
                },
                visitors: {
                    required: true,
                },
            },
            messages: {
                email: {
                    required: ' ',
                     email: " "
                },
                address: {
                    required: " ",
                },
                city: {
                    required: " ",
                },
                country: {
                    required: " ",
                },
                 phone1: {
//                        required: true,
                        maxlength: " ",
                        minlength: " ",
                        number: " "
                    },
                    phone2: {
//                        required: true,
                        maxlength: " ",
                        minlength: " ",
                        number: " "
                    },
                    phone3: {
//                        required: true,
                        maxlength: " ",
                        minlength: " ",
                        number: " "
                    },
                news_letter: {
                    required: " ",
                    maxlength: " "
                },
                company1: {
                    required: " ",
                    maxlength: " "
                },
                title1: {
                    required: " ",
                    maxlength: " ",
                },
                state: {
                    required: " ",
                },
                zipcode: {
                    required: " ",
                },
                web_url: {
                    required: " ",
                    maxlength: " "
                },
                site_desc: {
                    required: " ",
                    maxlength: " "
                },
                visitors: {
                    required: " ",
                },
            },
            invalidHandler: function(event, validator) { //display error alert on form submit              
                success1.hide();
                error1.show();
                App.scrollTo(error1, -200);
            },
            highlight: function(element) { // hightlight error inputs
                $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            unhighlight: function(element) { // revert the change done by hightlight
                $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            success: function(label) {
                label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
            submitHandler: function(form) {

                var url = baseurl + "affiliate/account_info";
                var data = $(form).serialize();
                ajaxcall(url, data, function(output) {

                    if (output == 'success')
                    {
                            $("#req_success").modal('show');
//                        Toastr.init('success', 'Welldone..', 'Account Detail Updated Successfully..');
                    }
                    else if(output == 'user_exit')
                        {
                             Toastr.init('warning','Warning..','Email Already Exits');
                            
                        }
                    else if (output == 'error')
                    {
                        Toastr.init('error', 'Oops..', 'Account Detail not Updated...');
                    }
                });
            }
        });
        setTimeout(function(){
            var country_list = $("#s2id_autogen1 .select2-chosen").html();
       // alert(country_list);
        if(country_list == "Canada")
        {
            $('.canada').removeClass('hidden');
        }
        if(country_list == "United States")
        {
             $('.US').removeClass('hidden');
        }
        },1000)
        
        $('.country_list').change(function(){
             var country_list = $(this).val();
            // $("#myinput").rules("add", "required");
            
        $('#state_id').rules('remove');
        $('#state_id1').rules('remove');
            if(country_list == "Canada")
            {
                $("#state_id1").rules("add", "required");
                $('.canada').removeClass('hidden');
            }else if(country_list == "United States")
            {
                $("#state_id").rules("add", "required");
                 $('.US').removeClass('hidden');
            }
            else{
                $('.canada').addClass('hidden');
                $('.US').addClass('hidden');
            }
        });
    }

    var change_pass = function() {

        var form = $('#change_pass');
        var error1 = $('.alert-danger', form);
        var success1 = $('.alert-success', form);
        form.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                old_pass: {
                    required: true,
                },
                new_pass: {
                    required: true,
                    minlength: 8,
                },
                confirm_pass: {
                    required: true,
                    equalTo: "#new_password"
                },
            },
             messages: {
                old_pass: {
                    required: " ",
                },
                new_pass: {
                    required: " ",
                    minlength: " ",
                },
                confirm_pass: {
                    required: " ",
                    equalTo: " "
                },
            },
            invalidHandler: function(event, validator) { //display error alert on form submit              
                success1.hide();
                error1.show();
                App.scrollTo(error1, -200);
            },
            highlight: function(element) { // hightlight error inputs
                $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            unhighlight: function(element) { // revert the change done by hightlight
                $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            success: function(label) {
                label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
            submitHandler: function(form) {

                var url = baseurl + "affiliate/account_info/change_pass";
                var data = $(form).serialize()
                ajaxcall(url, data, function(output) {

                    if (output == 'success')
                    {
                        Toastr.init('success', 'Welldone..', 'Update Password Successfully..');
                    }
                    else if (output == 'error')
                    {
                        Toastr.init('error', 'Oops..', 'Password not Matched...');
                    }
                });
            }
        });
    }

    var textarea_count = function() {

        $('#count_char').keyup(function() {
            var left = 200 - $(this).val().length;
            if (left < 0) {
                left = 0;
            }
            $('#counter').text(left + ' Characters left');
        });

    }
    
   

   
    return {
        //main function to initiate the module
        init: function() {
           
            
            edit_account();
            change_pass();
            textarea_count();
        }
    };
}();
