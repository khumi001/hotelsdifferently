var Account = function() {

    var edit_account = function() {

        var form = $('#edit_account');
        var error1 = $('.alert-danger', form);
        var success1 = $('.alert-success', form);
        form.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                email: {
                    required: true,
                },
                address: {
                    required: true,
                },
                city: {
                    required: true,
                },
                country: {
                    required: true,
                },
                phone_number: {
                    //  required: true,
                    number: true,
                    //  maxlength: 10
                },
                news_letter: {
                    required: true,
                    maxlength: 40
                },
                company1: {
                    required: true,
                    maxlength: 40
                },
                title1: {
                    required: true,
                    maxlength: 20,
                },
                state: {
                    required: true,
                },
                zipcode: {
                    required: true,
                },
                web_url: {
                    required: true,
                    maxlength: 50
                },
                site_desc: {
                    required: true,
                    maxlength: 250
                },
                visitors: {
                    required: true,
                },
            },
            messages: {
                email: {
                    required: ' ',
                },
                address: {
                    required: " ",
                },
                city: {
                    required: " ",
                },
                country: {
                    required: " ",
                },
                phone_number: {
                    //  required: true,
                    number: " ",
                    //  maxlength: 10
                },
                news_letter: {
                    required: " ",
                    maxlength: " "
                },
                company1: {
                    required: " ",
                    maxlength: " "
                },
                title1: {
                    required: " ",
                    maxlength: " ",
                },
                state: {
                    required: " ",
                },
                zipcode: {
                    required: " ",
                },
                web_url: {
                    required: " ",
                    maxlength: " "
                },
                site_desc: {
                    required: " ",
                    maxlength: " "
                },
                visitors: {
                    required: " ",
                },
            },
            invalidHandler: function(event, validator) { //display error alert on form submit              
                success1.hide();
                error1.show();
                App.scrollTo(error1, -200);
            },
            highlight: function(element) { // hightlight error inputs
                $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            unhighlight: function(element) { // revert the change done by hightlight
                $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            success: function(label) {
                label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
            submitHandler: function(form) {

                var url = baseurl + "affiliate/account_info";
                var data = $(form).serialize();
                ajaxcall(url, data, function(output) {

                    if (output == 'success')
                    {
                        $("#req_success").modal('show');
//                        Toastr.init('success', 'Welldone..', 'Account Detail Updated Successfully..');
                    }
                    else if (output == 'user_exit')
                    {
                        Toastr.init('warning', 'Warning..', 'Email Already Exits');

                    }
                    else if (output == 'error')
                    {
                        Toastr.init('error', 'Oops..', 'Account Detail not Updated...');
                    }
                });
            }
        });
    }

    var change_pass = function() {

        var form = $('#change_pass');
        var error1 = $('.alert-danger', form);
        var success1 = $('.alert-success', form);
        form.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                old_pass: {
                    required: true,
                },
                new_pass: {
                    required: true,
                    minlength: 8,
                },
                confirm_pass: {
                    required: true,
                    equalTo: "#new_password"
                },
            },
            messages: {
                old_pass: {
                    required: " ",
                },
                new_pass: {
                    required: " ",
                    minlength: " ",
                },
                confirm_pass: {
                    required: " ",
                    equalTo: " "
                },
            },
            invalidHandler: function(event, validator) { //display error alert on form submit              
                success1.hide();
                error1.show();
                App.scrollTo(error1, -200);
            },
            highlight: function(element) { // hightlight error inputs
                $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            unhighlight: function(element) { // revert the change done by hightlight
                $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            success: function(label) {
                label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
            submitHandler: function(form) {

                var url = baseurl + "affiliate/account_info/change_pass";
                var data = $(form).serialize()
                ajaxcall(url, data, function(output) {

                    if (output == 'success')
                    {
                        Toastr.init('success', 'Welldone..', 'Update Password Successfully..');
                    }
                    else if (output == 'error')
                    {
                        Toastr.init('error', 'Oops..', 'Password not Matched...');
                    }
                });
            }
        });
    }

    var textarea_count = function() {

        $('#count_char').keyup(function() {
            var left = 200 - $(this).val().length;
            if (left < 0) {
                left = 0;
            }
            $('#counter').text(left + ' Characters left');
        });

    }

    var edit_payout_prefrance = function() {
        var form = $('#edit_account');
        var form1 = $('#edit_account1');
        var error1 = $('.alert-danger', form);
        var success1 = $('.alert-success', form);
        form.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                email: {
                    required: true,
                },
                full_name: {
                    maxlength: 20,
                },
                phone_number: {
                    //  required: true,
                    number: true,
                    maxlength: 10
                },
            },
            messages: {
                email: {
                    required: ' ',
                },
                phone_number: {
                    //  required: true,
                    number: " ",
                    //  maxlength: 10
                },
            },
            invalidHandler: function(event, validator) { //display error alert on form submit              
                success1.hide();
                error1.show();
                App.scrollTo(error1, -200);
            },
            highlight: function(element) { // hightlight error inputs
                $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            unhighlight: function(element) { // revert the change done by hightlight
                $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            success: function(label) {
                label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
            submitHandler: function(form) {

                var url = baseurl + "affiliate/payout_prefrence/payout_prefrance";
                var data = $(form).serialize();

                ajaxcall(url, data, function(output) {

                    if (output == 'success') {
                        $("#req_success").modal('show');
//                        Toastr.init('success', 'Welldone..', 'Account Detail Updated Successfully..');
                    } else {
                        Toastr.init('error', 'Oops..', 'Account Detail not Updated...');
                    }
                });
            }
        });

    }

    var edit_paymentprefrence = function() {
        $(".price").maskMoney({prefix: '$', allowNegative: true, thousands: ',', decimal: '.', affixesStay: false});
        var form = $('#edit_paymentinfo');
        var error1 = $('.alert-danger', form);
        var success1 = $('.alert-success', form);
        form.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                phone1: {
                    //  required: true,
                    maxlength: 4,
                    minlength: 1,
                    number: true
                },
                phone2: {
                    // required: true,
                    maxlength: 12,
                    minlength: 1,
                    number: true
                },
                phone3: {
                    //  required: true,
                    maxlength: 6,
                    minlength: 0,
                    number: true
                },
                method_payment: {
                    required: true,
                },
                send_money: {
                    required: true,
                },
                email: {
                    required: true,
                    email: true
                },
                phone_no:{
                    maxlength: 12,
                }
            },
            messages: {
                phone1: {
                    //  required: true,
                    maxlength: " ",
                    minlength: " ",
                    number: " "
                },
                phone2: {
                    // required: true,
                    maxlength: " ",
                    minlength: " ",
                    number: " "
                },
                phone3: {
                    //  required: true,
                    maxlength: " ",
                    minlength: " ",
                    number: " "
                },
                pay_email: {
                    required: " ",
                    email: " ",
                },
                full_name: {
                    required: " ",
                },
                phone_no: {
                    required: " ",
                    number: " ",
                     maxlength: " ",
                },
                city: {
                    required: " ",
                },
                state: {
                    required: " ",
                },
                country: {
                    required: " ",
                },
                method_payment: {
                    required: " ",
                    maxlength: " "
                },
                send_money: {
                    required: " ",
                    maxlength: " "
                },
                email: {
                    required: " ",
                    email: " "
                },
            },
            invalidHandler: function(event, validator) { //display error alert on form submit              
                success1.hide();
                error1.show();
                App.scrollTo(error1, -200);
            },
            highlight: function(element) { // hightlight error inputs
                $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            unhighlight: function(element) { // revert the change done by hightlight
                $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            success: function(label) {
                label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
            submitHandler: function(form) {

                var url = baseurl + "affiliate/payout_prefrence/paymentinfo";
                var data = $(form).serialize();
                ajaxcall(url, data, function(output) {

                    if (output == 'success')
                    {
                        $("#req_success").modal('show');
//                        Toastr.init('success', 'Welldone..', 'Account Detail Updated Successfully..');
                    }
                    else if (output == 'user_exit')
                    {
                        Toastr.init('warning', 'Warning..', 'Email Already Exits');

                    }
                    else if (output == 'error')
                    {
                        Toastr.init('error', 'Oops..', 'Account Detail not Updated...');
                    }
                });
            }
        });
        setTimeout(function() {
            var method_payment = $('#method_payment').val();
            //  alert(method_payment);
            if (method_payment == 'westerrn_union')
            {
                var country_list = $('#country_list').val();

                $('#myinput').rules('remove');
                $("#full_name").rules("add", "required");
                $("#phone_no").rules("add", "required");
                $("#phone_no").rules("add", "number");
                $("#city").rules("add", "required");
                if (country_list == 'United States') {
                    $("#state").rules("add", "required");
                }

                $("#country_list").rules("add", "required");
            }
            else {
                $("#myinput").rules("add", "required");
                $("#myinput").rules("add", "email");
                $("#full_name").rules('remove');
                $("#phone_no").rules('remove');
                $("#city").rules('remove');
                $("#state").rules('remove');
                $("#country_list").rules('remove');
            }
        }, 1000)

        $('#method_payment').change(function() {
            $('#edit_paymentinfo input').val('');
            $('#country_list option[value=""]').attr('selected', 'selected');
            $('#state option[value=""]').attr('selected', 'selected')
            $("div").removeClass("has-error");
            var method_payment = $(this).val();
            var country_list = $('#country_list').val();
            if (method_payment == 'westerrn_union')
            {
                $('#myinput').rules('remove');
                $("#full_name").rules("add", "required");
                $("#phone_no").rules("add", "required");
                $("#phone_no").rules("add", "number");
                if (country_list == 'United States') {
                    $("#state").rules("add", "required");
                    
                }
                $("#city").rules("add", "required");
              //  $("#state").rules("add", "required");
                $("#country_list").rules("add", "required");
            }
            else {
                $("#myinput").rules("add", "required");
                $("#myinput").rules("add", "email");
                $("#full_name").rules('remove');
                $("#phone_no").rules('remove');
                $("#city").rules('remove');
                $("#state").rules('remove');
                $("#country_list").rules('remove');
            }
        });
        $("#full_name").keypress(function(event) {
            return isNumber(event)
        });
        $("#phone_no").keypress(function(event) {
            
            return isLatter(event)
        });
        function isLatter(evt) {
            
            var charCode = (evt.which) ? evt.which : event.keyCode
            
            if ((charCode >= 97 && charCode <= 122) || (charCode >= 65 && charCode <= 90)){
                return false;
            }
            return true;
           // return true;
        }
        function isNumber(evt) {

            var charCode = (evt.which) ? evt.which : event.keyCode
            if ((charCode != 46 || $(this).val().indexOf('.') != -1) && (charCode < 48 || charCode > 57))
                return true;

            return false;
        }
        $("#city").keypress(function(event) {
            return isNumber(event)
        });
        $("#state").keypress(function(event) {
            return isNumber(event)
        });
        setTimeout(function() {
            var country_list = $('#country_list').val();
            if (country_list == "United States")
            {
                $('.US').removeClass('hidden');
            }
        }, 1000)
        $('#country_list').change(function() {
            var country_list = $(this).val();
            if (country_list == "United States")
            {
                $('.US').removeClass('hidden');
                 $("#state").rules("add", "required");
            } else
            {
                $('.US').addClass('hidden');
                $("#state").rules('remove');
            }
        })
        //$('#myinput').rules('remove');
    }
    return {
        //main function to initiate the module
        init: function() {
            edit_payout_prefrance();
            edit_paymentprefrence();
            edit_account();
            change_pass();
            textarea_count();
        }
    };
}();
