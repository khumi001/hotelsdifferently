var from = '';
var to = '';
var from1 = '';
var to1 = '';
var html_content = $('.filter-box').html();
var html_content1 = $('.filter-box1').html();
$('.filter-box').remove();
$('.filter-box1').remove();

var Statistics = function() {
    
    var handle_records = function() {
        
        var grid = new Datatable_Apply();
        grid.init({
            src: $("#pending_transaction"),
            onSuccess: function(grid) {
                // execute some code after table records loaded
            },
            onError: function(grid) {
                // execute some code on network or other general error  
            },
            dataTable: {
                "fnPreDrawCallback": function(oSettings) {
                    $('.group-checkable').attr('checked', false);
                    $('.group-checkable').parents('span').removeClass('checked');
                     general();
                },
                "aLengthMenu": [
                    [25, 50, 100, 150, -1],
                    [25, 50, 100, 150, "All"] // change per page values here
                ],
                "sDom" : "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r><'table-scrollable't><'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>r>>", // datatable layout                        
                "bDestroy": true,
                'fnServerData': function(sSource, aoData, fnCallback)
                {
                   aoData.push(
                        {"name": "from", "value": from},
                        {"name": "to", "value": to}
                    );
                    $.ajax
                            ({
                                'dataType': 'json',
                                'type': 'POST',
                                'url': sSource,
                                'data': aoData,
                                'success': fnCallback
                            });

                },
                "fnFooterCallback": function(nRow, aaData, iStart, iEnd, aiDisplay) {
                    var total = 0;
                    for (var i = 0; i < aaData.length; i++) {
                        var comm = aaData[i][3].replace(/[^\d.]/g, '');
                        total += comm * 1;

                    }
                    var amount_total = 0;
                    for (var i = 0; i < aaData.length; i++) {
                        var amount = aaData[i][2].replace(/[^\d.]/g, '');
                        amount_total += amount * 1;

                    }
//          var page = 0;
//                        for (var i = iStart; i < iEnd; i++) {
//                            page += aaData[aiDisplay[i]][0] * 1;
//                        }

                    /* Modify the footer row to match what we want */
                    var nCells = nRow.getElementsByTagName('th');
                    //  nCells[1].innerHTML = parseInt(page);
                 //   var number = parseFloat(amount_total).toFixed(2);
                  //       number = $.formatNumber(number, {format:"#,###.00", locale:"us"});
                //    nCells[2].innerHTML = '$' + parseFloat(amount_total).toFixed(2);
              // var ss =  addThousandsSeparator( parseFloat(amount_total).toFixed(2));
                    nCells[2].innerHTML = '$' + addThousandsSeparator( parseFloat(amount_total).toFixed(2));
                    nCells[3].innerHTML = '$' + addThousandsSeparator(parseFloat(total).toFixed(2));
                },
                "iDisplayLength": 25, // default record count per page
                "bServerSide": true, // server side processing
                "sAjaxSource": baseurl + 'affiliate/statistics/statistics_datatable/', // ajax source
                "aaSorting": [[1, "desc"]], // set first column as a default sort by asc
                "oLanguage": {// language settings
                    "sProcessing": '<img src="' + baseurl + 'public/assets/img/loading-spinner-grey.gif"/><span>&nbsp;&nbsp;Loading...</span>',
                    "sLengthMenu": "<span class='seperator'>|</span>View _MENU_ records",
                    "sInfo": "<span class='seperator'>|</span>Found total _TOTAL_ records",
                    "sInfoEmpty": "",
                    "sGroupActions": "_TOTAL_ records selected:  ",
                    "sAjaxRequestGeneralError": "Could not complete request. Please check your internet connection",
                    "sEmptyTable": "",
                   "sZeroRecords": 'You have no pending transactions.',
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next",
                        "sPage": "Page",
                        "sPageOf": "of"
                    }
                },
            }

        });
        var oTable = grid.getDataTable();
    }
    
    var handele_charjback = function() {
        
        var grid = new Datatable_Apply();
        grid.init({
            src: $("#charjback_transaction"),
            onSuccess: function(grid) {
                // execute some code after table records loaded
            },
            onError: function(grid) {
                // execute some code on network or other general error  
            },
            dataTable: {
                "fnPreDrawCallback": function(oSettings) {
                    $('.group-checkable').attr('checked', false);
                    $('.group-checkable').parents('span').removeClass('checked');
                     general();
                },
                "aLengthMenu": [
                    [25, 50, 100, 150, -1],
                    [25, 50, 100, 150, "All"] // change per page values here
                ],
                "sDom" : "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r><'table-scrollable't><'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>r>>", // datatable layout                                
                "bDestroy": true,
                'fnServerData': function(sSource, aoData, fnCallback)
                {
                   aoData.push(
                        {"name": "from1", "value": from1},
                        {"name": "to1", "value": to1}
                    );
                    $.ajax
                            ({
                                'dataType': 'json',
                                'type': 'POST',
                                'url': sSource,
                                'data': aoData,
                                'success': fnCallback
                            });

                },
                "fnFooterCallback": function(nRow, aaData, iStart, iEnd, aiDisplay) {
                    var total = 0;
                    for (var i = 0; i < aaData.length; i++) {
                        var comm = aaData[i][3].replace(/[^\d.]/g, '');
                        total += comm * 1;

                    }
                    var amount_total = 0;
                    for (var i = 0; i < aaData.length; i++) {
                        var amount = aaData[i][2].replace(/[^\d.]/g, '');
                        amount_total += amount * 1;

                    }
//          var page = 0;
//                        for (var i = iStart; i < iEnd; i++) {
//                            page += aaData[aiDisplay[i]][0] * 1;
//                        }

                    /* Modify the footer row to match what we want */
                    var nCells = nRow.getElementsByTagName('th');
                    //  nCells[1].innerHTML = parseInt(page);
                    nCells[2].innerHTML = '$' + parseFloat(amount_total).toFixed(2);
                 //   nCells[3].innerHTML = '$' + parseFloat(total).toFixed(2);
                },
                "iDisplayLength": 25, // default record count per page
                "bServerSide": true, // server side processing
                "sAjaxSource": baseurl + 'affiliate/statistics/charjback_datatable/', // ajax source
                "aaSorting": [[1, "desc"]], // set first column as a default sort by asc
                 "oLanguage": {// language settings
                    "sProcessing": '<img src="' + baseurl + 'public/assets/img/loading-spinner-grey.gif"/><span>&nbsp;&nbsp;Loading...</span>',
                    "sLengthMenu": "<span class='seperator'>|</span>View _MENU_ records",
                    "sInfo": "<span class='seperator'>|</span>Found total _TOTAL_ records",
                    "sInfoEmpty": "",
                    "sGroupActions": "_TOTAL_ records selected:  ",
                    "sAjaxRequestGeneralError": "Could not complete request. Please check your internet connection",
                    "sEmptyTable": "",
                   "sZeroRecords": 'You have no chargebacks.',
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next",
                        "sPage": "Page",
                        "sPageOf": "of"
                    }
                },
            }

        });
        var oTable = grid.getDataTable();
    }
    
    var general = function() {
        $('#pending_transaction_wrapper').children(':nth-child(1)').children(':nth-child(1)').html(html_content);
        $('#charjback_transaction_wrapper').children(':nth-child(1)').children(':nth-child(1)').html(html_content1);
        $('.date-picker').datepicker({            
            autoclose: true
        });
         $('#from').val(from);
         $('#to').val(to);
         
         $('#from1').val(from1);
         $('#to1').val(to1);
       var selector =  $('#pending_transactions_wrapper').children(':nth-child(3)').children(':nth-child(1)');
                       $(selector).removeClass('col-md-8');
                       $(selector).addClass('col-md-12 text-center');
      var selector1 =  $('#charjback_transaction_wrapper').children(':nth-child(3)').children(':nth-child(1)');
                       $(selector1).removeClass('col-md-8');
                       $(selector1).addClass('col-md-12 text-center');
    }
    
    var general_handle_filter = function() {
        $('body').on('change', '.date-filter', function(e) {
            from = $('#from').val();
            to = $('#to').val();                        
          //  if((from == '' && to == '') || (from != '' && to != '')){
                handle_records();
          //  }            
        });
        $('body').on('change', '.date-filter1', function(e) {
            from1 = $('#from1').val();
            to1 = $('#to1').val();                        
          //  if((from == '' && to == '') || (from != '' && to != '')){
                handele_charjback();
          //  }            
        });
    }
    
    var resetbutton = function() {
        $('body').on('click', '.reset1', function(e) {
                $('.date-filter').val("");
                from = "";
                to = "";
              setTimeout(function(){
                   handle_records();
              },1000)
               
             
        });
        $('body').on('click', '.reset2', function(e) {
                $('.date-filter1').val("");
                from1 = "";
                to1 = "";
              setTimeout(function(){
                   handele_charjback();
              },1000)
               
             
        });
    }
    
    function addThousandsSeparator(input) {
        var output = input
        if (parseFloat(input)) {
            input = new String(input); // so you can perform string operations
            var parts = input.split("."); // remove the decimal part
            parts[0] = parts[0].split("").reverse().join("").replace(/(\d{3})(?!$)/g, "$1,").split("").reverse().join("");
            output = parts.join(".");
        }

        return output;
    }
    
    return {
        init: function() {
            handle_records();
            general();
            general_handle_filter();
            handele_charjback();
            resetbutton();
        },
    };

}();