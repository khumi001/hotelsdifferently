var Payout_history = function() {

    var handle_records = function() {

        var grid = new Datatable_Apply();
        grid.init({
            src: $("#payout_history"),
            onSuccess: function(grid) {
                // execute some code after table records loaded
            },
            onError: function(grid) {
                // execute some code on network or other general error  
            },
            dataTable: {
                "fnPreDrawCallback": function(oSettings) {
                    $('.group-checkable').attr('checked', false);
                    $('.group-checkable').parents('span').removeClass('checked')
                },
                "aLengthMenu": [
                    [25, 50, 100, 150, -1],
                    [25, 50, 100, 150, "All"] // change per page values here
                ],
                "bDestroy": true,
                'fnServerData': function(sSource, aoData, fnCallback)
                {
//                    aoData.push(
//                            {"name": "from_date", "value": $("#from").val()},
//                    {"name": "to_date", "value": $("#to").val()},
//                    {"name": "today_data", "value": $('#filter_by_daily').val()},
//                    {"name": "month", "value": $('#month').val()},
//                    {"name": "quarter", "value": $('#quarter').val()}
//
////                    null,
////                    {"name": "today_data", "value": ""}
////                    
//                    );
                    $.ajax
                            ({
                                'dataType': 'json',
                                'type': 'POST',
                                'url': sSource,
                                'data': aoData,
                                'success': fnCallback
                            });

                },
                "fnFooterCallback": function(nRow, aaData, iStart, iEnd, aiDisplay) {
                    var total = 0;
                    for (var i = 0; i < aaData.length; i++) {
                        var sales = aaData[i][2].replace(/[^\d.]/g, '');
                        total += sales * 1;

                    }
                    var amount_total = 0;
                    for (var i = 0; i < aaData.length; i++) {
                        var amount = aaData[i][3].replace(/[^\d.]/g, '');
                        amount_total += amount * 1;

                    }
//          var page = 0;
//                        for (var i = iStart; i < iEnd; i++) {
//                            page += aaData[aiDisplay[i]][0] * 1;
//                        }

                    /* Modify the footer row to match what we want */
                    var nCells = nRow.getElementsByTagName('th');
                    //  nCells[1].innerHTML = parseInt(page);
                  //  nCells[2].innerHTML = '$' + parseFloat(total).toFixed(2);
                    nCells[3].innerHTML = '$' + parseFloat(amount_total).toFixed(2);
                },
                "iDisplayLength": 25, // default record count per page
                "bServerSide": true, // server side processing
                "sAjaxSource": baseurl + 'affiliate/payout_history/payout_history_datatable/', // ajax source
                "aaSorting": [[1, "desc"]] // set first column as a default sort by asc

            }

        });
        var oTable = grid.getDataTable();
//        $('body').on('click', '#date_filter_ajax', function(e) {
//            if (e.handled !== true) {
//
//                var from = $('#from').val();
//                var to = $('#to').val();
//
//                if (from != '') {
//                    $('#from').val(from);
//                }
//                else if (to != '') {
//                    $('#to').val(to);
//                }
//
////                alert(from);
//                oTable.fnDraw();
//                e.handled = true;
//            }
//        });
    }
    
     var general = function() {
        var html_content = $('.filter-box').html();
        $('#payout_history_wrapper').children(':nth-child(1)').children(':nth-child(1)').html(html_content);
        $('.date-picker').datepicker({            
            autoclose: true
        });
        var selector =  $('#payout_history_wrapper').children(':nth-child(3)').children(':nth-child(1)');
                       $(selector).removeClass('col-md-8');
                       $(selector).addClass('col-md-12 text-center');
    }
    
    return {
        init: function() {
            handle_records();
            general();
        },
    };

}();
