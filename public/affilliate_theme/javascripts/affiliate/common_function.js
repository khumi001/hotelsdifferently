var Toastr = function() {

    return {
        //main function to initiate the module
        init: function(type, title, message) {
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "positionClass": "toast-top-right",
                "onclick": null,
                "showDuration": "1000",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
            toastr[type](message, title);
        },
        clear: function() {
            toastr.clear();
        }

    };

}();

var handle_click = function(){
    if($('.page-sidebar-menu').children('li.active').hasClass('open')){
        $('.page-sidebar-menu').children('li.active').children('ul').children('li.active').trigger('click');
    }else{
        $('.page-sidebar-menu').children('li.active').children('a').trigger('click');                                
    }
}