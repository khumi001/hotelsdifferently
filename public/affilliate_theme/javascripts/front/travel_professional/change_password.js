
var Change_password = function() {
jQuery.validator.addMethod("letter", function(value, element) {
	return this.optional(element) || /^[a-zA-Z_@!#\$\^%&*()+=\-\[\]\\\';,\.\/\{\}\|\":<>\? ]+$/i.test(value);
}, "Only Alphabet allow");

    
     var change_pass = function(){
		
		var form = $('#edit_pass');
		var error = $('.alert-danger', form);
		var success = $('.alert-success', form);
		form.validate({
			doNotHideMessage: false, //this option enables to show the error/success messages on tab switch.
			errorElement: 'span', //default input error message container
			errorClass: 'help-block', // default input error message class
			focusInvalid: false, // do not focus the last invalid input
			rules: {
			   old_pass: {
					required: true
				},
				new_pass: {
					required: true,
					pwcheck: true,
					notEqualTo: '#old_pass',
				},
				confirm_pass: {
					required: true,
					equalTo: "#newpassword",
					pwcheck: true,
				},
			},
		
			messages: { // custom messages for radio buttons and checkboxes
			   old_pass: {
					required: " "
				},
				new_pass: {
					required: " ",
					notEqualTo : "",
					pwcheck:" "
				},
				confirm_pass: {
					required: " ",
					equalTo: " ",
					pwcheck:" "
				},
			},
		
			errorPlacement: function (error, element) { // render error placement for each input type
				if (element.attr("name") == "gender") { // for uniform radio buttons, insert the after the given container
					error.insertAfter("#form_gender_error");
				} else if (element.attr("name") == "payment[]") { // for uniform radio buttons, insert the after the given container
					error.insertAfter("#form_payment_error");
				} else {
					error.insertAfter(element); // for other inputs, just perform default behavior
				}       
				 if (element.attr("name") == "policy") { // insert checkbox errors after the container                  
					error.insertAfter($('#register_tnc_error'));
			 } 
			},
		
			invalidHandler: function (event, validator) { //display error alert on form submit   
				success.hide();
				error.show();
				App.scrollTo(error, -200);
			},
		
			highlight: function (element) { // hightlight error inputs
				$(element)
					.closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
			},
		
			unhighlight: function (element) { // revert the change done by hightlight
				$(element)
					.closest('.form-group').removeClass('has-error'); // set error class to the control group
			},
		
			success: function (label) {
				if (label.attr("for") == "gender" || label.attr("for") == "payment[]") { // for checkboxes and radio buttons, no need to show OK icon
					label
						.closest('.form-group').removeClass('has-error').addClass('has-success');
					label.remove(); // remove error label here
				} else { // display success icon for other inputs
					label
						.addClass('valid') // mark the current input as valid and display OK icon
					.closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
				}
			},
			submitHandler: function (form) {
		
				var form = $('#edit_pass').serialize();
				$('#loading').show();
			$.ajax({
				type: 'POST',
				url: baseurl + 'front/travel_professional/password_update',
				data: form,
				success: function(output) {
					$('#loading').hide();
					if (output == 'success' || output=='email error')
					{
						 $("#myModal_autocomplete").modal('show');
						 $('#edit_pass')[0].reset();
					}
					else if (output == 'error')
					{
						$("#myModal_autocomplete1").modal('show');
					}
				}
			});
		
					//add here some ajax code to submit your form or just call form.submit() if you want to submit the form without ajax
			}  
			
		});
		$.validator.addMethod("pwcheck", function(value) {
			return /^[A-Za-z0-9\d=!\-@._*]*$/.test(value) // consists of only these
				&& /[a-z]/.test(value) // has a lowercase letter
				&& /\d/.test(value) // has a digit
		 });
		 
		$.validator.addMethod("notEqualTo",
			function(value, element, param) {
				var notEqual = true;
				$("#error_msg").hide();
				value = $.trim(value);
				if (value == $.trim($(param).val())) 
				{ 
					notEqual = false; 
					$("#error_msg").show();
					$("#error_msg").html("Current password and New password could not be same!");
				}
				return notEqual;
			}
		);
    }
    
    return {
        init: function() {
            change_pass();
        },
    };

}();
