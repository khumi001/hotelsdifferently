var Affiliate_wizard = function() {
   jQuery.validator.addMethod("letter", function(value, element) {
	return this.optional(element) || /^[a-zA-Z_@!#\$\^%&*()+=\-\[\]\\\';,\.\/\{\}\|\":<>\? ]+$/i.test(value);
}, "Only Alphabet allow");
    return {
        //main function to initiate the module
        init: function() {
            if (!jQuery().bootstrapWizard) {
                return;
            }

            function format(state) {
                if (!state.id)
                    return state.text; // optgroup
                return "<img class='flag' src=" + baseurl + "public/assets/img/flags/" + state.id.toLowerCase() + ".png>&nbsp;&nbsp;" + state.text;
            }

            $("#country_list").select2({
                placeholder: "Select Country",
                allowClear: true,
                formatResult: format,
                formatSelection: format,
                escapeMarkup: function(m) {
                    return m;
                }
            });
            $("#state_id").select2({
                placeholder: "Select State"
            });

            var form = $('#submit_form');
            var error = $('.alert-danger', form);
            var success = $('.alert-success', form);
//             $('.form-control').maxlength({
//                limitReachedClass: "label label-danger",
//            })
            form.validate({
                doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                rules: {
                    firstname: {
                        required: true,
                        maxlength: 20,
                    },
                    lastname: {
                        required: true,
                        maxlength: 20,
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    checkemail: {
                        required: true,
                    },
                    password: {
                        minlength: 8,
                        required: true,
                        pwcheck: true,
                    },
                    rpassword: {
                        minlength: 8,
                        required: true,
                        equalTo: "#submit_form_password"
                    },
                    company: {
                        maxlength: 40,
                        required: true,
                    },
                    title: {
                        maxlength: 20,
                        required: true,
                    },
                    country: {
                        required: true
                    },
                    address: {
                        required: true,
                        maxlength: 50
                    },
                    address2: {
//                        required: true,
//                        maxlength: 50
                    },
                    city: {
                        required: true
                    },
                    state: {
                        required: true
                    },
                    zipcode: {
                        required: true,
                        maxlength: 10
                    },
                     phone1: {
//                        required: true,
                        maxlength: 4,
                        minlength: 1,
                        number: true
                    },
                    phone2: {
//                        required: true,
                        maxlength: 12,
                        minlength: 1,
                        number: true
                    },
                    phone3: {
//                        required: true,
                        maxlength: 6,
                        minlength: 0,
                        number: true
                    },
                    dob: {
                        required: true,
                    },
                    newsletter: {
                        required: true,
                        maxlength: 40
                    },
                    website_url: {
                        required: true,
                        maxlength: 50
                    },
                    site_desc: {
                        required: true,
                        maxlength: 250
                    },
                    visitors: {
                        required: true,
                    },
                    payment_method: {
                        required: true,
                    },
                    policy: {
                        required: true,
                        minlength: 1
                    },
                    select11: {
                        required: true,
                    },
                },
                messages: {// custom messages for radio buttons and checkboxes
                    'payment[]': {
                        required: "Please select at least one option",
                        minlength: jQuery.format("Please select at least one option")
                    },
                    firstname: {
                        required: " ",
                        maxlength: " ",
                    },
                    lastname: {
                        required: " ",
                        maxlength: " ",
                    },
                    email: {
                        required: " ",
                        email: " "
                    },
                    password: {
                        minlength: " ",
                        required: " ",
                        pwcheck: " ",
                    },
                    rpassword: {
                        minlength: " ",
                        required: " ",
                        equalTo: " "
                    },
                    company: {
                        maxlength: " ",
                        required: " ",
                    },
                    title: {
                        maxlength: " ",
                        required: " ",
                    },
                    country: {
                        required: " "
                    },
                    address: {
                        required: " ",
                        maxlength: " "
                    },
                    address2: {
                        required: " ",
                        maxlength: " "
                    },
                    city: {
                        required: " "
                    },
                    state: {
                        required: " "
                    },
                    zipcode: {
                        required: " ",
                        maxlength: " "
                    },
                    phone1: {
                       // required: " ",
                        maxlength: " ",
                        minlength: " ",
                        number:" "
                    },
                    phone2: {
                   //    required: " ",
                        maxlength: " ",
                        minlength: " ",
                        number:" "
                    },
                    phone3: {
                       // required: " ",
                        maxlength: " ",
                        minlength: " ",
                        number:" "
                    },
                    dob: {
                        required: " ",
                    },
                    newsletter: {
                        required: " ",
                        maxlength: " "
                    },
                    website_url: {
                        required: " ",
                        maxlength: " "
                    },
                    site_desc: {
                        required: " ",
                        maxlength: " "
                    },
                    visitors: {
                        required: " ",
                    },
                    payment_method: {
                        required: " ",
                    },
                    policy: {
                        required: " ",
                        minlength: " "
                    },
                    select11: {
                        required: " ",
                    },
                },
                errorPlacement: function(error, element) { // render error placement for each input type
                    if (element.attr("name") == "gender") { // for uniform radio buttons, insert the after the given container
                        error.insertAfter("#form_gender_error");
                    } else if (element.attr("name") == "payment[]") { // for uniform radio buttons, insert the after the given container
                        error.insertAfter("#form_payment_error");
                    } else {
                        error.insertAfter(element); // for other inputs, just perform default behavior
                    }
                    if (element.attr("name") == "policy") { // insert checkbox errors after the container                  
                        error.insertAfter($('#register_tnc_error'));
                    }
                },
                invalidHandler: function(event, validator) { //display error alert on form submit   
                    success.hide();
                    error.show();
                    App.scrollTo(error, -200);
                },
                highlight: function(element) { // hightlight error inputs
                    $(element)
                            .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
                },
                unhighlight: function(element) { // revert the change done by hightlight
                    $(element)
                            .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },
                success: function(label) {
                    if (label.attr("for") == "gender" || label.attr("for") == "payment[]") { // for checkboxes and radio buttons, no need to show OK icon
                        label
                                .closest('.form-group').removeClass('has-error').addClass('has-success');
                        label.remove(); // remove error label here
                    } else { // display success icon for other inputs
                        label
                                .addClass('valid') // mark the current input as valid and display OK icon
                                .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                    }
                },
                    submitHandler: function(form) {

                    var url = baseurl + "front/home/affiliateregister";
                    var country = $('#country_list :selected').text();
                    var data1 = $(form).serialize();
                    var data={'data':data1,'country':country};
                  //  App.startPageLoading();
                        $.ajax({
                            type: 'POST',
                            url: url,
                            data: data,
                            success: function(result) {
                            //    App.stopPageLoading();
                                if(result == 'success')
                                {
                                    $('#success_registration').modal('show');
                                }
                            }
                        })
                    success.show();
                    error.hide();
                }

            });
            $(".firstname").keypress(function(event) { return isNumber(event) });
           function isNumber(evt) {
               
                var charCode = (evt.which) ? evt.which : event.keyCode                
                if ((charCode != 46 || $(this).val().indexOf('.') != -1) && (charCode < 48 || charCode > 57))
                    return true;

                return false;
            }
            $.validator.addMethod("pwcheck", function(value) {
                return /^[A-Za-z0-9\d=!\-@._*]*$/.test(value) // consists of only these
                        && /[a-z]/.test(value) // has a lowercase letter
                        && /\d/.test(value) // has a digit
            });
               function validateEmail(email) {
                var re = /\S+@\S+\.\S+/;
                return re.test(email);
            }
            
            $("#email").focusout(function() {
                var email = $(this).val();
                var confirm_email = validateEmail(email);
                if(confirm_email){
                     $.ajax({
                    type: 'post',
                    url: baseurl + "front/home/checkemail",
                    data: {"email": email},
                    success: function(output) {
                        if (output == 'success')
                        {
                            $('#email_massage').parents('.form-group').removeClass('has-error');
                            $('#email_massage').css('display', 'none');
                            $('#checkemail').val('found');
                            $('.email1').removeClass('tooltips');
                        }
                        else
                        {
                            
                            $('#email_message').parents('.form-group').removeClass('has-success');
                            $('#email_message').parents('.form-group').addClass('has-error')
                            $('.email1').addClass('tooltips');
                            $('#email_massage').css('display', 'block');
                            $('#checkemail').val('');
                        }
                    }
                });
                }else{
                    $('#email_message').parents('.form-group').addClass('has-error')
                }
               
            });
            $('#inlineCheckbox21').click(function() {
                var thisCheck = $(this);
                if (thisCheck.is(':checked')) {
                    $('input[type="submit"]').removeAttr('disabled');
                    $('input[type="submit"]').addClass('green_btn')
                    $('input[type="submit"]').removeClass('red_btn')

                } else {
                    $('input[type="submit"]').prop("disabled", true);
                    $('input[type="submit"]').addClass('red_btn')
                    $('input[type="submit"]').removeClass('green_btn')

                }
            });
            $('.clickhome').click(function(){
                location.href = baseurl;
            });
            $('body').on('change','.country_list',function(){
               var value = $(this).val();
               if(value == 'CA'){
                   $('.canada').removeClass('hidden');
               }else if(value == 'US'){
                   $('.canada').addClass('hidden');
                   $('.US').removeClass('hidden');
                   
               }else{
                    $('.US').addClass('hidden');
                    $('.canada').addClass('hidden');
               }
            });
            var displayConfirm = function() {
                $('#tab4 .form-control-static', form).each(function() {
                    var input = $('[name="' + $(this).attr("data-display") + '"]', form);
                    if (input.is(":radio")) {
                        input = $('[name="' + $(this).attr("data-display") + '"]:checked', form);
                    }
                    if (input.is(":text") || input.is("textarea")) {
                        $(this).html(input.val());
                    } else if (input.is("select")) {
                        $(this).html(input.find('option:selected').text());
                    } else if (input.is(":radio") && input.is(":checked")) {
                        $(this).html(input.attr("data-title"));
                    } else if ($(this).attr("data-display") == 'payment') {
                        var payment = [];
                        $('[name="payment[]"]').each(function() {
                            payment.push($(this).attr('data-title'));
                        });
                        $(this).html(payment.join("<br>"));
                    }
                });
            }

            var handleTitle = function(tab, navigation, index) {
                var total = navigation.find('li').length;
                var current = index + 1;
                // set wizard title
                $('.step-title', $('#form_wizard_1')).text('Step ' + (index + 1) + ' of ' + total);
                // set done steps
                jQuery('li', $('#form_wizard_1')).removeClass("done");
                var li_list = navigation.find('li');
                for (var i = 0; i < index; i++) {
                    jQuery(li_list[i]).addClass("done");
                }

                if (current == 1) {
                    $('#form_wizard_1').find('.button-previous').hide();
                } else {
                    $('#form_wizard_1').find('.button-previous').show();
                }

                if (current >= total) {
                    $('#form_wizard_1').find('.button-next').hide();
                    $('#form_wizard_1').find('#reset_form').hide();
                    $('#form_wizard_1').find('.button-submit').show();
                    displayConfirm();
                } else {
                    $('#form_wizard_1').find('.button-next').show();
                    $('#form_wizard_1').find('#reset_form').hide();
                    $('#form_wizard_1').find('.button-previous').show();
                    $('#form_wizard_1').find('.button-submit').hide();
                }
                App.scrollTo($('.page-title'));
            }

            // default form wizard
            $('#form_wizard_1').bootstrapWizard({
                'nextSelector': '.button-next',
                'previousSelector': '.button-previous',
                onTabClick: function(tab, navigation, index, clickedIndex) {
                    success.hide();
                    error.hide();
                    if (form.valid() == false) {
                        return false;
                    }
                    handleTitle(tab, navigation, clickedIndex);
                },
                onNext: function(tab, navigation, index) {
                    success.hide();
                    error.hide();
                    var account = $('#accountid').val();
                    var checkemail = $('#checkemail').val();
                    var flag = 0;
                    if (form.valid() == false) {
                        return false;
                    }
                   
                    if (account == "") {
                        $('#accountid').parent().parent().removeClass('has-success');
                         $('#accountid').parent().parent().addClass('has-error');
                        flag = 1;
                    }
                    if (checkemail == "") {
                        $('#checkemail').parent().parent().removeClass('has-success');
                        $('#checkemail').parent().parent().addClass('has-error');
                        flag = 1;
                    }
                    if(flag)
                    {
                        return false;
                    }
                    handleTitle(tab, navigation, index);
                },
                onPrevious: function(tab, navigation, index) {
                    success.hide();
                    error.hide();

                    handleTitle(tab, navigation, index);
                },
                onTabShow: function(tab, navigation, index) {
                    var total = navigation.find('li').length;
                    var current = index + 1;
                    var $percent = (current / total) * 100;
                    $('#form_wizard_1').find('.progress-bar').css({
                        width: $percent + '%'
                    });
                }
            });

            $('#form_wizard_1').find('.button-previous').hide();
            $('#form_wizard_1 .button-submit').click(function() {
                // alert('Finished! Hope you like it :)');                
            }).hide();
        }

    };

}();