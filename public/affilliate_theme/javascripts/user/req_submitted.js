var from = '';
var to = '';
var html_content = $('.filter-box').html();

function  confirm_reservation_datatable(status){
	$('.filter-box').removeClass('hide');
    $('#request_submitted').DataTable( {
		"ajax": {
			"url": baseurl + 'user/reservation/req_submitted_datatable/'+status,
			"type": "POST",
			"data" : {from:$('#from').val() , to:$('#to').val()}
		},
		"destroy": true,
		"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) 
		{
			if(aData[7] == 'Approved')
			{
				for(var i = 0 ; i < aData.length ; i++)
				{
					$('td:eq('+i+')', nRow).attr('style', 'color:#fff ; background:green;');
				}
			}
			else if(aData[7] == 'Declined')
			{
				for(var i = 0 ; i < aData.length ; i++)
				{
					$('td:eq('+i+')', nRow).attr('style', 'color:#fff ; background:red;');
				}
			}
			else if(aData[7] == 'Pending')
			{
				for(var i = 0 ; i < aData.length ; i++)
				{
					$('td:eq('+i+')', nRow).attr('style', 'color:#fff ; background:gray;');
				}
			}
			return nRow;
		},
		"order": [[ 0, "asc" ]]
    });
}
$('body').on('change', '.filter', function() {
	$('.filter').removeAttr('checked');
	$(this).attr('checked', 'checked');
	var status = $('input[name="filter"]:checked').val();
	html_content = jQuery.fn.outerHTML = function(s) {
        return s
            ? this.before(s).remove()
            : jQuery(".filter-box1").append(this.eq(0).clone()).html();
    };//$('.filter-box1').outerHTML();
	confirm_reservation_datatable(status);
});
$('#from').change(function(){
	confirm_reservation_datatable(status);
});
$('#to').change(function(){
	confirm_reservation_datatable(status);
});


