var Coupons = function() {

    $(function() {
        $("#filter1").prop("checked", true);
    });

    var handle_records = function(status) {
        status = $('input[name=filter]:checked').val();

        if (!status || status == 'ALL') {
            status = '';
        }
        var grid = new Datatable_Apply();
        var dataempty = "";
        if (!status || status == 'ALL') {
            dataempty = 'You have no coupons yet.';
        }
        if (status == 'A') {
            dataempty = 'You do not have any active coupons.';
        }
        if (status == 'U') {
            dataempty = 'You have not used any coupons yet.';
        }
        if (status == 'E') {
            dataempty = 'You do not have any expired coupons.';
        }
		var hit_url = '';
		if(status == '')
		{
			hit_url = baseurl + 'user/myaccount/coupons_datatable';
		}
		else
		{
			hit_url = baseurl + 'user/myaccount/coupons_datatable/' + status;
		}
        grid.init({
            src: $("#coupons"),
            onSuccess: function(grid) {
                // execute some code after table records loaded
            },
            onError: function(grid) {
                // execute some code on network or other general error  
            },
            dataTable: {
                "fnPreDrawCallback": function(oSettings) {
                    $('.group-checkable').attr('checked', false);
                    $('.group-checkable').parents('span').removeClass('checked')
                },
                "aLengthMenu": [
                    [25, 50, 100, 150, -1],
                    [25, 50, 100, 150, "All"] // change per page values here
                ],
                "bDestroy": true,
                'fnServerData': function(sSource, aoData, fnCallback)
                {
                    $.ajax
                            ({
                                'dataType': 'json',
                                'type': 'POST',
                                'url': sSource,
                                'data': aoData,
                                'success': fnCallback
                            });

                },
                "iDisplayLength": 25, // default record count per page
                "bServerSide": true, // server side processing
                "sAjaxSource": hit_url, // ajax source
                "aaSorting": [[1, "desc"]], // set first column as a default sort by asc
                "oLanguage": {// language settings
                    "sProcessing": '<img src="' + baseurl + 'public/assets/img/loading-spinner-grey.gif"/><span>&nbsp;&nbsp;Loading...</span>',
                    "sLengthMenu": "<span class='seperator'>|</span>View _MENU_ records",
                    "sInfo": "<span class='seperator'>|</span>Found total _TOTAL_ records",
                    "sInfoEmpty": "",
                    "sGroupActions": "_TOTAL_ records selected:  ",
                    "sAjaxRequestGeneralError": "Could not complete request. Please check your internet connection",
                    "sEmptyTable": "",
                    "sZeroRecords": dataempty,
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next",
                        "sPage": "Page",
                        "sPageOf": "of"
                    }
                },
            }

        });
        var oTable = grid.getDataTable();
//        $('body').on('click', '#date_filter_ajax', function(e) {
//            if (e.handled !== true) {
//
//                var from = $('#from').val();
//                var to = $('#to').val();
//
//                if (from != '') {
//                    $('#from').val(from);
//                }
//                else if (to != '') {
//                    $('#to').val(to);
//                }
//
////                alert(from);
//                oTable.fnDraw();
//                e.handled = true;
//            }
//        });

    }


    $("body").on("change", ".filter", function() {
        var status = $('input[name="filter"]:checked').val();
        handle_records(status);
        general();
    });

    var general = function() {
        var html_content = $('.filter-box').html();
        $('#coupons_wrapper').children(':nth-child(1)').children(':nth-child(1)').hide();
        $('#coupons_wrapper').children(':nth-child(1)').children(':nth-child(2)').hide();


    }
//    var selectallcheckbox = function() {
//        setTimeout(function() {
//            $('#filter').parent().addClass('checked');
//        }, 1000);
//    }
    return {
        init: function() {
            handle_records();
            general();
//            selectallcheckbox();
        },
    };

}();
