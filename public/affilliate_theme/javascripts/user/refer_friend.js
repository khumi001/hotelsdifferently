var from = '';
var to = '';
var html_content = $('.filter-box').html();
$('.filter-box').remove();
var Refer_friend = function() {

    var handle_records = function() {

        var grid = new Datatable_Apply();
        grid.init({
            src: $("#refer_friend"),
            onSuccess: function(grid) {
                // execute some code after table records loaded
            },
            onError: function(grid) {
                // execute some code on network or other general error  
            },
            dataTable: {
                "fnPreDrawCallback": function(oSettings) {
                    $('.group-checkable').attr('checked', false);
                    $('.group-checkable').parents('span').removeClass('checked');
                    general();
                },
                "aLengthMenu": [
                    [25, 50, 100, 150, -1],
                    [25, 50, 100, 150, "All"] // change per page values here
                ],
                "bDestroy": true,
                'fnServerData': function(sSource, aoData, fnCallback)
                {
                    aoData.push(
                            {"name": "from", "value": from},
                    {"name": "to", "value": to}
                    );
                    $.ajax
                            ({
                                'dataType': 'json',
                                'type': 'POST',
                                'url': sSource,
                                'data': aoData,
                                'success': fnCallback
                            });
                },
                "iDisplayLength": 25, // default record count per page
                "bServerSide": true, // server side processing
                "sAjaxSource": baseurl + 'user/myaccount/refer_friend_datatable/', // ajax source
                "aaSorting": [[1, "desc"]], // set first column as a default sort by asc
                "oLanguage": {// language settings
                    "sProcessing": '<img src="' + baseurl + 'public/assets/img/loading-spinner-grey.gif"/><span>&nbsp;&nbsp;Loading...</span>',
                    "sLengthMenu": "<span class='seperator'>|</span>View _MENU_ records",
                    "sInfo": "<span class='seperator'>|</span>Found total _TOTAL_ records",
                    "sInfoEmpty": "",
                    "sGroupActions": "_TOTAL_ records selected:  ",
                    "sAjaxRequestGeneralError": "Could not complete request. Please check your internet connection",
                    "sEmptyTable": "",
                    "sZeroRecords": "Nothing here yet. Your referred friend has yet to make his first booking with us.",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next",
                        "sPage": "Page",
                        "sPageOf": "of"
                    }
                },
                  "fnFooterCallback": function(nRow, aaData, iStart, iEnd, aiDisplay) {
                    var total = 0;
                    for (var i = 0; i < aaData.length; i++) {
                        var sales = aaData[i][2].replace(/[^\d.]/g, '');
                        total += sales * 1;

                    }
                    var amount_total = 0;
                    for (var i = 0; i < aaData.length; i++) {
                        var amount = aaData[i][3].replace(/[^\d.]/g, '');
                        amount_total += amount * 1;

                    }
          var page = 0;
                        for (var i = iStart; i < iEnd; i++) {
                            page += aaData[aiDisplay[i]][0] * 1;
                        }

                    /* Modify the footer row to match what we want */
                    var nCells = nRow.getElementsByTagName('th');
                    var minuesamount = 0;
                    var minuesamount1 = 0;
                   // console.log('dd');
                   setTimeout(function(){
                         $('.linethrow').each(function(){
                        
                        minuesamount = $(this).html();
                      //  console.log(minuesamount);
                        amount_total =  parseFloat(amount_total) - parseFloat(minuesamount);
                        //minuesamount += parseFloat(minuesamount);
                    });
                     $('.linethrow1').each(function(){
                        
                        minuesamount1 = $(this).html();
                      //  console.log(minuesamount);
                        total =  parseFloat(total) - parseFloat(minuesamount1);
                        //minuesamount += parseFloat(minuesamount);
                    })
                    //  nCells[1].innerHTML = parseInt(page);
                    nCells[2].innerHTML = '$' + parseFloat(total).toFixed(2);
                    nCells[3].innerHTML = '$' + parseFloat(amount_total).toFixed(2);
                   },2000);
                   
                },
            }
           
        });
        setTimeout(function(){
             $('.linethrow').parent().parent().addClass('linethrowtr');
        },2000);
        
        var oTable = grid.getDataTable();
    }

    var general = function() {        
        $('#refer_friend_wrapper').children(':nth-child(1)').children(':nth-child(1)').html(html_content);        
        $('.date-picker').datepicker({
            autoclose: true
        });
        $('#from').val(from);
        $('#to').val(to);
        var selector = $('#refer_friend_wrapper').children(':nth-child(3)').children(':nth-child(1)');
        $(selector).removeClass('col-md-8');
        $(selector).addClass('col-md-12 text-center');
    }
    var general_handle_filter = function() {
        $('body').on('change', '.date_filter', function(e) {
            from = $('#from').val();
            to = $('#to').val();            
            if (from == '' || to == '') {
                return false;
            }
            handle_records();
        });
    }
    
    var referfriend = function() {
        
        var form = $('.referfriend');
        var error = $('.alert-danger', form);
        var success = $('.alert-success', form);
        form.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                txtsignup_link: {
                    required: true,
                }
            },
             messages: {
                txtsignup_link: {
                    required: " ",
                    email: " "
                }

            },
            errorPlacement: function(error, element) { // render error placement for each input type
                if (element.attr("name") == "gender") { // for uniform radio buttons, insert the after the given container
                    error.insertAfter("#form_gender_error");
                } else if (element.attr("name") == "payment[]") { // for uniform radio buttons, insert the after the given container
                    error.insertAfter("#form_payment_error");
                } else {
                    error.insertAfter(element); // for other inputs, just perform default behavior
                }
                if (element.attr("name") == "policy") { // insert checkbox errors after the container                  
                    error.insertAfter($('#register_tnc_error'));
                }
            },
            invalidHandler: function(event, validator) { //display error alert on form submit   
                success.hide();
                error.show();
                //  App.scrollTo(error, -200);
            },
            highlight: function(element) { // hightlight error inputs
                $(element)
                        .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
            },
            unhighlight: function(element) { // revert the change done by hightlight
                $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            submitHandler: function(form) {                
                var referemail = $("#contact_list").val(); 
                $.ajax({
                    type: "POST",
                    url: baseurl + "user/myaccount/refer_friend",
                    data: {"referemail": referemail},
                    success: function(response) {
                        if (response == 'success'){
                            Toastr.init('success', '', 'Your invitation has been sent!');
                            setTimeout(function(){
                                window.location.href = baseurl+"user/myaccount/refer_friend";
                            },1000)
                        }else {
                            Toastr.init('error', '', 'Your invitation has not been sent!');
                            setTimeout(function(){
                                window.location.href = baseurl+"user/myaccount/refer_friend";
                            },1000)
                        }                        
                    }
                });
            }
        });    
    }    
    return {
        init: function() {
            //handle_records();
            general();
            general_handle_filter();
            referfriend();
        },
    };

}();
