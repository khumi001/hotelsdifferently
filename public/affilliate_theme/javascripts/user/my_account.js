var My_account = function() {
    $('#autoRenewal').click(function(){
        if($(this).is(":checked")){
            alert('Your membership will now renew automatically')
        }else{
            alert("Your membership will NOT renew automatically. Should you wish to renew your membership, please switch it back to ON or renew it manually after your subscription expiration date.")
        }
    });

    jQuery.validator.addMethod("letter", function(value, element) {
        return this.optional(element) || /^[a-zA-Z_@!#\$\^%&*()+=\-\[\]\\\';,\.\/\{\}\|\":<>\? ]+$/i.test(value);
    }, "Only Alphabet allow");
    var edit_info = function() {

        var form = $('#edit_info');
        var error = $('.alert-danger', form);
        var success = $('.alert-success', form);

        form.validate({
            doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: true, // do not focus the last invalid input
            rules: {
                /*fname: {
                 required: true,
                 letter:true,
                 maxlength: 20,
                 },
                 lname: {
                 required: true,
                 letter:true,
                 maxlength: 20,
                 },*/
                phone1: {
                    //  required: true,
                    maxlength: 4,
                    minlength: 0,
                    number:true
                },
                phone2: {
                    //    required: true,
                    maxlength: 12,
                    minlength: 0,
                    number:true
                },
                phone3: {
                    //  required: true,
                    maxlength: 6,
                    minlength: 0,
                    number:true
                },
                /*country: {
                 required: true,
                 },
                 newsletter: {
                 required: true,
                 }*/
            },
            messages: { // custom messages for radio buttons and checkboxes

                /*fname: {
                 required: " ",
                 letter:" ",
                 maxlength: " ",
                 },
                 lname: {
                 required: " ",
                 letter:" ",
                 maxlength: " ",
                 },*/
                phone1: {
                    //  required: " ",
                    maxlength: " ",
                    minlength: " ",
                    number:" "
                },
                phone2: {
                    //   required: " ",
                    maxlength: " ",
                    minlength: " ",
                    number:" "
                },
                phone3: {
                    maxlength: " ",
                    minlength: " ",
                    number:" "
                },

            },
            invalidHandler: function(event, validator) { //display error alert on form submit
                //success.hide();
                //error.show();
                App.scrollTo(error, -200);
            },

            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
            },
            /*unhighlight: function(element) { // revert the change done by hightlight
             $(element)
             .closest('.form-group').removeClass('has-error'); // set error class to the control group
             },*/
            success: function(label) {
                /*if (label.attr("for") == "gender" || label.attr("for") == "payment[]") { // for checkboxes and radio buttons, no need to show OK icon
                 label
                 .closest('.form-group').removeClass('has-error').addClass('has-success');
                 label.remove(); // remove error label here
                 } else { // display success icon for other inputs
                 label
                 .addClass('valid') // mark the current input as valid and display OK icon
                 .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                 }*/
            },

            submitHandler: function(form) {
                var url = baseurl + "user/myaccount/info";
                var data = $(form).serialize();
                ajaxcall(url, data, function(output) {
                    if (output == 'success')
                    {
                        $("#myModal_autocomplete").modal('show');

//                        Toastr.init('success', 'Weldone..', 'Infromation Update successfully..');
                        handle_click();
                    }
                    else if (output == 'error')
                    {
                        $("#myModal_autocomplete1").modal('show');
                        // Toastr.init('error', 'Oops..', 'Infromation not Update..');
                    }
                });
                return false;
            }
        });
        $(".fname").keypress(function(event) { return isNumber(event) });
        function isNumber(evt) {

            var charCode = (evt.which) ? evt.which : event.keyCode
            if ((charCode != 46 || $(this).val().indexOf('.') != -1) && (charCode < 48 || charCode > 57))
                return true;

            return false;
        }
    }
    $('body').on('click','.clickme',function(){
        location.reload();
    });

    var change_pass = function(){

        var form = $('#edit_pass');
        var error = $('.alert-danger', form);
        var success = $('.alert-success', form);
        form.validate({
            doNotHideMessage: false, //this option enables to show the error/success messages on tab switch.
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                old_pass: {
                    required: true,
                    pwcheck: true,
                },
                new_pass: {
                    required: true,
                    pwcheck: true,
                    notEqualTo: '#old_pass',
                },
                confirm_pass: {
                    required: true,
                    equalTo: "#newpassword",
                    pwcheck: true,
                },
            },

            messages: { // custom messages for radio buttons and checkboxes
                old_pass: {
                    required: " ",
                    pwcheck:" "
                },
                new_pass: {
                    required: " ",
                    notEqualTo : "",
                    pwcheck:" "
                },
                confirm_pass: {
                    required: " ",
                    equalTo: " ",
                    pwcheck:" "
                },
            },

            errorPlacement: function (error, element) { // render error placement for each input type
                if (element.attr("name") == "gender") { // for uniform radio buttons, insert the after the given container
                    error.insertAfter("#form_gender_error");
                } else if (element.attr("name") == "payment[]") { // for uniform radio buttons, insert the after the given container
                    error.insertAfter("#form_payment_error");
                } else {
                    error.insertAfter(element); // for other inputs, just perform default behavior
                }
                if (element.attr("name") == "policy") { // insert checkbox errors after the container
                    error.insertAfter($('#register_tnc_error'));
                }
            },

            invalidHandler: function (event, validator) { //display error alert on form submit
                success.hide();
                error.show();
                App.scrollTo(error, -200);
            },

            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                if (label.attr("for") == "gender" || label.attr("for") == "payment[]") { // for checkboxes and radio buttons, no need to show OK icon
                    label
                        .closest('.form-group').removeClass('has-error').addClass('has-success');
                    label.remove(); // remove error label here
                } else { // display success icon for other inputs
                    label
                        .addClass('valid') // mark the current input as valid and display OK icon
                        .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                }
            },
            submitHandler: function (form) {

                var form = $('#edit_pass').serialize();
                $('#loading').show();
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'user/myaccount/change_pass',
                    data: form,
                    success: function(output) {
                        $('#loading').hide();
                        if (output == 'success')
                        {
                            $("#myModal_autocomplete").modal('show');
                            $('#edit_pass')[0].reset();
                        }
                        else if (output == 'error')
                        {
                            $("#myModal_autocomplete1").modal('show');
                        }
                    }
                });

                //add here some ajax code to submit your form or just call form.submit() if you want to submit the form without ajax
            }

        });
        $.validator.addMethod("pwcheck", function(value) {
            return /^[A-Za-z0-9\d=!\-@._*]*$/.test(value) // consists of only these
            && /[a-z]/.test(value) // has a lowercase letter
            && /\d/.test(value) // has a digit
        });

        $.validator.addMethod("notEqualTo",
            function(value, element, param) {
                var notEqual = true;
                $("#error_msg").hide();
                value = $.trim(value);
                if (value == $.trim($(param).val()))
                {
                    notEqual = false;
                    $("#error_msg").show();
                    $("#error_msg").html("Current password and New password could not be same!");
                }
                return notEqual;
            }
        );

//                $('.oldpass').bind('paste', function (e) {
//                   e.preventDefault();
//                });
//                $('.newpass').bind('copy', function (e) {
//                   e.preventDefault();
//                });
//                $('.confirmpass').bind('paste', function (e) {
//                   e.preventDefault();
//                });
    }

    var change_email = function(){

        var form = $('#chnge_email');
        var error = $('.alert-danger', form);
        var success = $('.alert-success', form);
        form.validate({
            doNotHideMessage: false, //this option enables to show the error/success messages on tab switch.
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                //account

                /* addded*/
                email: {
                    required: true,
                    email: true,
                },
                new_email: {
                    required: true,
                    email: true,
                },
                confirm_email: {
                    required: true,
                    email: true,
                    equalTo: '#newemail',
                },
            },

            messages: { // custom messages for radio buttons and checkboxes
                email: {
                    required: " ",
                    email:" "
                },
                new_email: {
                    required: " ",
                    email:" "
                },
                confirm_email: {
                    required: " ",
                    email: " ",
                    equalTo: " ",
                },
            },

            errorPlacement: function (error, element) { // render error placement for each input type
                if (element.attr("name") == "gender") { // for uniform radio buttons, insert the after the given container
                    error.insertAfter("#form_gender_error");
                } else if (element.attr("name") == "payment[]") { // for uniform radio buttons, insert the after the given container
                    error.insertAfter("#form_payment_error");
                } else {
                    error.insertAfter(element); // for other inputs, just perform default behavior
                }
                if (element.attr("name") == "policy") { // insert checkbox errors after the container
                    error.insertAfter($('#register_tnc_error'));
                }
            },

            invalidHandler: function (event, validator) { //display error alert on form submit
                success.hide();
                error.show();
                App.scrollTo(error, -200);
            },

            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                if (label.attr("for") == "gender" || label.attr("for") == "payment[]") { // for checkboxes and radio buttons, no need to show OK icon
                    label
                        .closest('.form-group').removeClass('has-error').addClass('has-success');
                    label.remove(); // remove error label here
                } else { // display success icon for other inputs
                    label
                        .addClass('valid') // mark the current input as valid and display OK icon
                        .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                }
            },
            submitHandler: function (form) {

                var form = $('#chnge_email').serialize();
                $('#loading').show();
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'user/myaccount/change_email',
                    data: form,
                    success: function(output) {
                        $('#loading').hide();
                        if (output == 'success')
                        {
                            $("#myModal_autocomplete").modal('show');
                            $('#chnge_email')[0].reset();
                        }
                        else if (output == 'error'){
                            Toastr.init('error', 'Your current email address does NOT match our records. Please try again!');
                        }else if(output == 'exits'){

                            $("#exits_modal").modal('show');
                        }

                    }
                });
            }

        });
    }
    return {
        init: function() {
            edit_info();
            change_pass();
            change_email();
        },
    };

}();
