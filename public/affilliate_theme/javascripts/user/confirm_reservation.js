var from = '';
var to = '';
var html_content = $('.filter-box').html();
//$('.filter-box').remove();

function cancelBooking(id)
{
	$('#can_model_close').click();
	$('#resDiv').hide();
	$('#loading').show();
	$.ajax({
		type: 'post',
		url: baseurl + 'front/tourico_api/cancelBookingAjax/'+id,
		data: {'id': id},
		success: function(output) { //alert(output)
			var data_array = JSON.parse(output);
			$('#loading').hide();
			$('#resDiv').show();
			if(data_array.status == "success")
			{
				html = '<div class="alert alert-success">'+data_array.msg+'</div>';
				$('.bookCanId'+data_array.id).html('<span style="color:red;">CANCELLED</span>');
				$('#confirmcancelpopupbtn').click();
			}
			else
			{
				html = '<div class="alert alert-danger">'+data_array.error+'</div>';
                $('#confirmcancelpopupbtn').click();
                $("#confirmcancelpopup .modal-body p").html(html);
			}
			$('#resDiv').html(html);
		}
	});
}

function getCancelationFee(id)
{
	$('#resDiv').hide();
	$('#loading').show();
	$('#resDiv').html('<div class="alert alert-info">Please wait---------------</div>');
	$.ajax({
		type: 'post',
		url: baseurl + 'front/tourico_api/GetCancellationPolicyAfterReserationPage/'+id,
		data: {'id': id},
		success: function(output) { //alert(output)
			var data_array = JSON.parse(output);
			$('#callnow').click();
			$('#loading').hide();
			if(data_array.status == "success")
			{
				/*roomprice = $('#roomprice'+id).val();
				console.log(data_array.fee);
				roomprice = parseFloat(data_array.fee);
				roomprice = roomprice.toFixed(2);
				coupon_str = '';
				coupon = parseFloat(data_array.coupon);
				if(coupon > 0)
				{
					coupon_str = ' + $'+coupon+' coupon';
				}
				html = '<p>Are you sure that you would like to cancel your reservation? Once you cancel your reservation it cannot be reversed. <br>Your refund will be <b>“$'+roomprice+" "+data_array.currency+coupon_str+'”</b> if you cancel the reservation now.</p>';*/
                html = data_array.message;
				footer = '<button type="button" class="btn btn-danger" onclick="cancelBooking('+id+')" >Yes</button><button type="button" class="btn btn-success" data-dismiss="modal">NO</button>';
			}
			else
			{
				html = '<div class="alert alert-danger">'+data_array.error+'</div>';
				footer = '<button type="button" class="btn btn-default" data-dismiss="modal">OK</button>';
			}
			$('#can_model_body').html(html);
			$('#can_model_footer').html(footer);
		}
	});
}

function cancelAdminReser(id)
{
	$('#admincancelpopup1-close').click();
	$('#resDiv').hide();
	$('#loading').show();
	$.ajax({
		type: 'post',
		url: baseurl + 'front/tourico_api/canceladminBookingAjax/'+id,
		data: {'id': id},
		success: function(output) 
		{ 
			var data_array = JSON.parse(output);
			$('#loading').hide();
			$('#resDiv').show();
			if(data_array.status == "success")
			{
				html = '<div class="alert alert-success">'+data_array.msg+'</div>';
				$('.bookCanId'+data_array.id).html('<span style="color:red;">CANCELLED</span>');
				$('#admincancelpopupbtn').click();
			}
			else
			{
				html = '<div class="alert alert-danger">'+data_array.error+'</div>';
			}
			$('#resDiv').html(html);
		}
	});
	
}

function adminCancelationalert(id)
{
	$('#admincancelpopup1-yes').attr('onclick' , 'cancelAdminReser("'+id+'")');
	html = $('#admincantext'+id).html();
	$('#admincancelpopupbtn1').click();
	$('#admincanconfinumber').text($('#confimationo'+id).text());
	$('#canadmindynamicadata').html(html);
}



function  confirm_reservation_datatable(status){
	$('.filter-box').removeClass('hide');
    $('#confirm_reservation').DataTable( {
		"ajax": {
			"url": baseurl + 'user/reservation/conf_reservation_datatable/'+status,
			"type": "POST",
			"data" : {from:$('#from').val() , to:$('#to').val()}
		},
		"destroy": true,
        "aaSorting": [[0, "desc"]]
    });
}
$('body').on('change', '.filter', function() {
	$('.filter').removeAttr('checked');
	$(this).attr('checked', 'checked');
	var status = $('input[name="filter"]:checked').val();
	html_content = $('.filter-box1').outerHTML();
	confirm_reservation_datatable(status);
});
$('#from').change(function(){
	confirm_reservation_datatable(status);
});
$('#to').change(function(){
	confirm_reservation_datatable(status);
});

/*var Confirm_reservation = function() {
	
    var handle_records = function(status) {

        if (!status || status == '') 
		{
            status = '';
        }
        var grid = new Datatable_Apply();
        var dataempty = "";
        if (!status || status == 'ALL') 
		{
            dataempty = 'You do not have any confirmed reservations yet.';
        }
        if (status == 'OUTDATED') 
		{
            dataempty = "You don't have any out dated reservations.";
        }
        if (status == 'ACTIVE') 
		{
            dataempty = "You don't have any active reservations.";
        }
        if (status == 'CANCEL') 
		{
            dataempty = "You don't have any cancelled reservations.";
        }

        var grid = new Datatable_Apply();
        grid.init({
            src: $("#confirm_reservation"),
            onSuccess: function(grid) {
				console.log('success', grid);
                // execute some code after table records loaded
            },
            onError: function(grid) {
				console.log('my check', grid);
                // execute some code on network or other general error  
            },
            dataTable: {
                "fnPreDrawCallback": function(oSettings) {
                    $('.group-checkable').attr('checked', false);
                    $('.group-checkable').parents('span').removeClass('checked');
                    general();
                },
                "aLengthMenu": [
                    [10, 20, 40, 60, -1],
                    [10, 20, 40, 60, "All"] // change per page values here
                ],
                "bDestroy": true,
                'fnServerData': function(sSource, aoData, fnCallback)
                {
                    aoData.push(
                            {"name": "from", "value": from},
                    {"name": "to", "value": to}
                    );
                    $.ajax
                            ({
                                'dataType': 'json',
                                'type': 'POST',
                                'url': sSource,
                                'data': aoData,
                                'success': fnCallback
                            });

                },
                "iDisplayLength": 10, // default record count per page
                "bServerSide": true, // server side processing
                "sAjaxSource": baseurl + 'user/reservation/conf_reservation_datatable/' + status, // ajax source
                "aaSorting": [[5, "desc"]], // set first column as a default sort by asc
                "oLanguage": {// language settings
                    "sProcessing": '<img src="' + baseurl + 'public/assets/img/loading-spinner-grey.gif"/><span>&nbsp;&nbsp;Loading...</span>',
                    "sLengthMenu": "<span class='seperator'>|</span>View _MENU_ records",
                    "sInfo": "<span class='seperator'>|</span>Found total _TOTAL_ records",
                    "sInfoEmpty": "",
                    "sGroupActions": "_TOTAL_ records selected:  ",
                    "sAjaxRequestGeneralError": "Could not complete request. Please check your internet connection",
                    "sEmptyTable": "",
                    "sZeroRecords": dataempty,
                    "oPaginate": 
					{
                        "sPrevious": "Prev",
                        "sNext": "Next",
                        "sPage": "Page",
                        "sPageOf": "of"
                    }
                },
            }

        });
        var oTable = grid.getDataTable();


    }
    $('body').on('change', '.filter', function() {

        $('.filter').removeAttr('checked');
        $(this).attr('checked', 'checked');
        var status = $('input[name="filter"]:checked').val();
        html_content = $('.filter-box1').outerHTML();
        handle_records(status);
    });
    
   $('body').on('click', '.cansel_modal', function() {

        var id = $(this).attr('data-id');
        $('.cancelpolicy').html("");
        $.ajax({
            type: 'post',
            url: baseurl + 'user/reservation/getcancletionpolicy',
            data: {'id': id},
            success: function(output) { //alert(output)
               var data_array = JSON.parse(output);
                $(".city").html(data_array[0].var_city);
                $(".checkin").html(data_array[0].var_checkin);
                $(".checkout").html(data_array[0].var_checkout);
                $(".rooms").html(data_array[0].var_room);
                $(".adults").html(data_array[0].var_adult);
                $(".star").html(data_array[0].var_star);
                $(".tripadvisor").html(data_array[0].var_tripadvisor);
                $(".children").html(data_array[0].var_child);
                $(".comments").html(data_array[0].var_comment);
                $(".nameofhotel").html(data_array[0].var_NOH);
                $(".policy").html(data_array[0].var_policy);
            }
        });
    });
    
    $("body").on("click", ".couponview", function() {
        var id = $(this).attr('id');

        $.ajax({
            type: 'post',
            url: baseurl + 'user/reservation/get_confirmreply',
            data: {'id': id},
            success: function(output) {
                var data_array = JSON.parse(output);
                $(".city").html(data_array[0].var_city);
                $(".checkin").html(data_array[0].var_checkin);
                $(".checkout").html(data_array[0].var_checkout);
                $(".rooms").html(data_array[0].var_room);
                $(".adults").html(data_array[0].var_adult);
                $(".children").html(data_array[0].var_child);
                $(".comments").html(data_array[0].var_comment);
                $(".accountid").html(data_array[0].var_city);
                $(".username").html(data_array[0].var_city);
                $(".nameofhotel").html(data_array[0].var_NOH);
                $(".policy").html(data_array[0].var_policy);
                $(".src").html(data_array[0].var_src);
                if (data_array[0].var_site2 != "")
                {
                    $(".site2name").html(data_array[0].var_site2);
                    $(".site2price").html(data_array[0].int_site2_price);
                }
                else
                {
                    $(".site2name").html(" ");
                    $(".site2price").html(" ");
                }
                if (data_array[0].int_site2_price != 0)
                {
                    $('.site2').parent().parent().removeClass('hidden');
                }
                else 
				{
                    $('.site2').parent().parent().addClass('hidden');
                }
                if(data_array[0].chr_status == "C")
                {
                    $('.cancel').removeClass('hidden');
                    $('.canceltime').html(data_array[0].date_usercanceltime);
                    $('.notcancel').addClass('hidden');
                }
                else
				{
                    $('.cancel').addClass('hidden');
                    $('.notcancel').removeClass('hidden');
                }
                $(".youaresaving").html('$' + data_array[0].savingamount);
                $(".site1name").html(data_array[0].var_site1);
                $(".site1price").html(data_array[0].int_site1_price);
                $(".tripadvisor").html(data_array[0].var_tripadvisor);
                $(".quoteid").html(data_array[0].var_transactionid);
                $(".yourquote").html('$' + data_array[0].var_prize);
                $(".stars").html(data_array[0].var_star);
            }
        });
    });
    var general = function() {
        $('#confirm_reservation_wrapper').children(':nth-child(1)').children(':nth-child(1)').html(html_content);
        $('.date-picker').datepicker({
            autoclose: true
        });
        $('#from').val(from);
        $('#to').val(to);
        var selector = $('#confirm_reservation_wrapper').children(':nth-child(3)').children(':nth-child(1)');
        $(selector).removeClass('col-md-8');
        $(selector).addClass('col-md-12 text-center');
    }

    var general_handle_filter = function() {
        $('body').on('change', '.date-filter', function(e) {
            from = $('#from').val();
            to = $('#to').val();
            if ((from == '' && to == '') || (from != '' && to != '')) {
                handle_records();
            }
            handle_records();
        });
    }
    var handle_cansel_conformation = function()
    {
        $('body').on('click', '#cansel_reservation', function() {
            var id = $(this).attr('data-id');
            $('.sayyes').attr('data-id', id);
        });
         $('body').on('click', '.sayyes', function() {
                var id = $(this).attr('data-id');
                var data = {'id': id}
                $.ajax({
                    type: 'post',
                    url: baseurl + "user/reservation/cansel_reservation",
                    data: data,
                    success: function(output) {
                        if (output == 'success')
                        {
                            window.location.href = baseurl + 'user/reservation/confirm_reservation';
                        }
                        else
                        {

                        }
                    }

                });
            });
    }
    var handlkeCity = function() 
	{
        $('body').on('hover', '.hoverShow', function() {
            $(this).children('.hoverShowDiv').css("display", "inline-block");
        });
        $('body').on('mouseout', '.hoverShow', function() {
            $(this).children('.hoverShowDiv').css("display", "none");
        });
    }
    return {
        init: function() 
		{
            handle_records('ALL');
            general_handle_filter();
            general();
            handle_cansel_conformation();
            handlkeCity();  
        },
    };
}();*/

