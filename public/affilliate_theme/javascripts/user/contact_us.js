var Contact_us = function () {
  $('body').on('click','.clickme',function(){
        location.reload(); 
    });
 var send_mail = function() {
		$('#contacterror').hide();

        var form = $('#send_mail');
        var error1 = $('.alert-danger', form);
        var success1 = $('.alert-success', form);
        form.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                email_add: {
                    required: true,
                },
                topic: {
                    required: true,
                },
                response: {
                    required: true,
                },
                message: {
                    required: true,
                    maxlength: 1000,
                },
            }, messages: { // custom messages for radio buttons and checkboxes
                    
                   email_add: {
                    required: " ",
                },
                topic: {
                    required: " ",
                },
                response: {
                    required: " ",
                },
                message: {
                    required: " ",
                    maxlength: " ",
                },
                   
                },
            invalidHandler: function(event, validator) { //display error alert on form submit              
                success1.hide();
                error1.show();
                App.scrollTo(error1, -200);
            },
            highlight: function(element) { // hightlight error inputs
                $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            unhighlight: function(element) { // revert the change done by hightlight
                $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            success: function(label) {
                label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
            submitHandler: function(form) {

                var url = baseurl + "user/contactus";
                var data = $(form).serialize();
                ajaxcall(url, data, function(output) {

                    if (output == 'success')
                    {
						$('#contacterror').hide();
                        $('#contact_modal').modal('show');
                         $(form)[0].reset();
                        //Toastr.init('success', 'Welldone..', 'Your Mail sent successfully..');   
                    }
                    else 
                    {
                        $('#contacterror').show();
                        $('#contacterror').html(output);
                        //Toastr.init('error', 'Oops..', 'Your Mail not sent...');
                    }
                });
            }
        });
    }
   
 var textarea_count = function(){
     
      $('#count_char').keyup(function () {
            var left = 1000 - $(this).val().length;
            if (left < 0) {
                left = 0;
            }
            $('#counter').text(left + ' Characters left');
            
});
     
 }
    
    return {
        //main function to initiate the module
        init: function () {    
            textarea_count();
            send_mail();
        }
    };
}();
