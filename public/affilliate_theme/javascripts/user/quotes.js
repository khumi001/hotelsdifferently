var from = '';
var to = '';
var html_content = $('.filter-box').html();
$('.filter-box').remove();

var Quotes = function() {

    var handle_records = function(status) {
        if (!status || status == 'ALL') {
            status = '';
        }
        var grid = new Datatable_Apply();
        var dataempty = "";
        if (!status || status == 'ALL') {
            dataempty = 'You have no quotes yet.';
        }
        if (status == 'A') {
            dataempty = 'Nothing here. You do not have any active quotes.';
        }
        if (status == 'E') {
            dataempty = 'Nothing here. You do not have any expired quotes.';
        }

        grid.init({
            src: $("#quotes"),
            onSuccess: function(grid) {
                // execute some code after table records loaded
            },
            onError: function(grid) {
                // execute some code on network or other general error  
            },
            dataTable: {
                "fnPreDrawCallback": function(oSettings) {
                    $('.group-checkable').attr('checked', false);
                    $('.group-checkable').parents('span').removeClass('checked');
                    general();
                },
                "aLengthMenu": [
                    [25, 50, 100, 150, -1],
                    [25, 50, 100, 150, "All"] // change per page values here
                ],
                "bDestroy": true,
                'fnServerData': function(sSource, aoData, fnCallback)
                {
                    aoData.push(
                            {"name": "from", "value": from},
                    {"name": "to", "value": to}
                    );
                    $.ajax
                            ({
                                'dataType': 'json',
                                'type': 'POST',
                                'url': sSource,
                                'data': aoData,
                                'success': fnCallback
                            });
//                            setTimeout(function(){
//                                $('.quotecolor').parent().parent().children().addClass('redbg');
//                                $('.quotecolor').parent().removeClass('redbg');
//                                //console.log($('.quotecolor').parent().last().removeClass('redbg'));
//                            },1000)

                },
                "iDisplayLength": 25, // default record count per page
                "bServerSide": true, // server side processing
                "sAjaxSource": baseurl + 'user/reservation/quotes_datatable/' + status, // ajax source
                "aaSorting": [[1, "desc"]], // set first column as a default sort by asc
                "oLanguage": {// language settings
                    "sProcessing": '<img src="' + baseurl + 'public/assets/img/loading-spinner-grey.gif"/><span>&nbsp;&nbsp;Loading...</span>',
                    "sLengthMenu": "<span class='seperator'>|</span>View _MENU_ records",
                    "sInfo": "<span class='seperator'>|</span>Found total _TOTAL_ records",
                    "sInfoEmpty": "",
                    "sGroupActions": "_TOTAL_ records selected:  ",
                    "sAjaxRequestGeneralError": "Could not complete request. Please check your internet connection",
                    "sEmptyTable": "",
                    "sZeroRecords": dataempty,
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next",
                        "sPage": "Page",
                        "sPageOf": "of"
                    }
                },
            }
        });
        var oTable = grid.getDataTable();
        $("body").on("click", ".couponview", function() {
            var id = $(this).attr('id');

            $.ajax({
                type: 'post',
                url: baseurl + 'user/reservation/get_reply',
                data: {'id': id},
                success: function(output) {
                    var data_array = JSON.parse(output);
                    
                    var comment = data_array[0].var_comment;
                    comment = comment.replace(/(?:\r\n|\r|\n)/g, '<br />');
                    var policy = data_array[0].var_policy
                    policy = policy.replace(/(?:\r\n|\r|\n)/g, '<br />');
                     
                    $(".city").html(data_array[0].var_city);
                    $(".checkin").html(data_array[0].var_checkin);
                    $(".checkout").html(data_array[0].var_checkout);
                    $(".rooms").html(data_array[0].var_room);
                    $(".adults").html(data_array[0].var_adult);
                    $(".children").html(data_array[0].var_child);
                    $(".comments").html(comment);
                    $(".accountid").html(data_array[0].var_city);
                    $(".roomtype").html(data_array[0].var_room_type);
                    $(".address").html(data_array[0].var_address);
                    $(".username").html(data_array[0].var_city);
                    $(".nameofhotel").html(data_array[0].var_NOH);
                    $(".policy").html(policy);
                    $(".src").html(data_array[0].var_src);
                    $(".site2name").parent().parent().parent().show();
                    if (data_array[0].var_site2 != "")
                    {
                        $(".site2name").html(data_array[0].var_site2);
                        $(".site2price").html("$" + data_array[0].int_site2_price);
                    }
                    else
                    {
                        $(".site2name").parent().parent().parent().hide();
                        $(".site2name").html(" ");
                        $(".site2price").html(" ");
                    }
                    $(".youaresaving").html('$' + data_array[0].savingamount);
                    $(".site1name").html(data_array[0].var_site1);
                    $(".site1price").html(data_array[0].int_site1_price);
                    $(".tripadvisor").html(data_array[0].var_tripadvisor);
                    $(".quoteid").html(data_array[0].var_uniq_quoteid);
                    $(".yourquote").html('$' + data_array[0].var_prize);
                    $(".stars").html(data_array[0].var_star);
                }
            });
        });
//        $('body').on('click', '.couponview', function() {
//            var id = $(this).attr('id');
//            $('.pay_queoteReq').click(function() {
//                $.ajax({
//                    type: 'post',
//                    url: baseurl + 'user/reservation/quotesstatus',
//                    data: {'id': id},
//                    success: function(output) {
//                        if (output == 'success') {
//                            Toastr.init('success', 'Welldone..', 'Quote Pay Successfully..');
//                        } else {
//                            Toastr.init('error', 'Oops..', 'Quote is not Updated...');
//                        }
//                    }
//                });
//            });
//        });
//        $('body').on('click', '.pay_quote', function() {
//            var id = $(this).attr('id');
//            $.ajax({
//                type: 'post',
//                url: baseurl + 'user/reservation/quotesstatus',
//                data: {'id': id},
//                success: function(output) {
//                    if (output == 'success') {
//                        Toastr.init('success', 'Welldone..', 'Quote Pay Successfully..');
//                    } else {
//                        Toastr.init('error', 'Oops..', 'Quote is not Updated...');
//                    }
//                }
//            });
//
//        });
        $('body').on('click', '.resubmit', function() {
            var id = $(this).attr('id');
            $('.quote_val').attr('data-replyid',id);
            //  $('#resubmit_modal').modal('hide');
            $.ajax({
                type: 'post',
                url: baseurl + 'user/reservation/resubmit',
                data: {'id': id},
                success: function(output) {
                    if (output == 'success') {


                    }
                }
            });
        });

        $('.resubmitokbutton').click(function() {
            setTimeout(function() {
                $('#resubmit_modal').modal('hide');
                window.location.href = baseurl + 'user/reservation/quotes';
            }, 1000);

        })
    }

    $('body').on('change', '.filter', function() {
        $('.filter').removeAttr('checked');
        $(this).attr('checked', 'checked');
        var status = $('input[name="filter"]:checked').val();
        html_content = $('.filter-box1').outerHTML();
        handle_records(status);
    });

    var general = function() {
//         console.log(html_content);
//         if(typeof $('.filter-box1').outerHTML() !== 'undefined'){
//             html_content = $('.filter-box1').outerHTML();
//         }         
        $('#quotes_wrapper').children(':nth-child(1)').children(':nth-child(1)').html(html_content);
        $('.date-picker').datepicker({
            autoclose: true
        });
        $('#from').val(from);
        $('#to').val(to);
        var selector = $('#quotes_wrapper').children(':nth-child(3)').children(':nth-child(1)');
        $(selector).removeClass('col-md-8');
        $(selector).addClass('col-md-12 text-center');
        $('#reset-btn').click(function() {
            from = '';
            to = '';
            html_content = $('.filter-box1').outerHTML();
            handle_records();
        })

    }
    var general_handle_filter = function() {
        $('body').on('change', '.date-filter', function(e) {

            from = $('#from').val();
            to = $('#to').val();
            if ((from == '' && to == '') || (from != '' && to != '')) {
                html_content = $('.filter-box1').outerHTML();
                handle_records();
            }

        });
    }
    var handlkeCity = function() {
        $('body').on('hover', '.hoverShow', function() {
            $(this).children('.hoverShowDiv').css("display", "inline-block");
        });
        $('body').on('mouseout', '.hoverShow', function() {
            $(this).children('.hoverShowDiv').css("display", "none");
        });
    }

    return {
        init: function() {
            handle_records('A');
            general_handle_filter();
            general();
            handlkeCity();
        },
    };

}();
